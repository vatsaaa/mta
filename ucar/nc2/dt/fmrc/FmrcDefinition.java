// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.fmrc;

import java.util.Set;
import java.util.Arrays;
import java.util.HashSet;
import org.slf4j.LoggerFactory;
import ucar.nc2.dataset.CoordinateAxis1D;
import java.util.Map;
import java.io.FileOutputStream;
import ucar.unidata.util.StringUtil;
import java.util.HashMap;
import ucar.nc2.util.DiskCache2;
import java.util.StringTokenizer;
import java.util.ArrayList;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.File;
import org.jdom.Content;
import java.util.Collections;
import org.jdom.Element;
import org.jdom.Document;
import java.io.IOException;
import java.io.OutputStream;
import org.jdom.output.XMLOutputter;
import org.jdom.output.Format;
import java.util.Iterator;
import java.util.Date;
import java.util.TimeZone;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.util.List;
import org.slf4j.Logger;
import ucar.nc2.dt.fmr.FmrcCoordSys;

public class FmrcDefinition implements FmrcCoordSys
{
    private static Logger log;
    private List<VertTimeCoord> vertTimeCoords;
    private List<ForecastModelRunInventory.TimeCoord> timeCoords;
    private List<ForecastModelRunInventory.EnsCoord> ensCoords;
    private List<RunSeq> runSequences;
    private String name;
    private String suffixFilter;
    private int runseq_num;
    private Calendar cal;
    static boolean showState;
    public static String[] fmrcDatasets;
    public static String[] fmrcDatasets_41;
    private static String fmrcDefinitionDir;
    private static String[] fmrcDefinitionFiles;
    private static String[] exampleFiles;
    
    public FmrcDefinition() {
        this.runseq_num = 0;
        (this.cal = new GregorianCalendar()).setTimeZone(TimeZone.getTimeZone("UTC"));
    }
    
    public String getSuffixFilter() {
        return this.suffixFilter;
    }
    
    public List<RunSeq> getRunSequences() {
        return this.runSequences;
    }
    
    public boolean hasVariable(final String searchName) {
        return this.findGridByName(searchName) != null;
    }
    
    public VertCoord findVertCoordForVariable(final String searchName) {
        final Grid grid = this.findGridByName(searchName);
        return (grid.vtc == null) ? null : grid.vtc.vc;
    }
    
    public TimeCoord findTimeCoordForVariable(final String searchName, final Date runTime) {
        for (final RunSeq runSeq : this.runSequences) {
            final Grid grid = runSeq.findGrid(searchName);
            if (null != grid) {
                final ForecastModelRunInventory.TimeCoord from = runSeq.findTimeCoordByRuntime(runTime);
                return new ForecastModelRunInventory.TimeCoord(runSeq.num, from);
            }
        }
        return null;
    }
    
    private ForecastModelRunInventory.TimeCoord findTimeCoord(final String id) {
        for (final ForecastModelRunInventory.TimeCoord tc : this.timeCoords) {
            if (tc.getId().equals(id)) {
                return tc;
            }
        }
        return null;
    }
    
    public RunSeq findSeqForVariable(final String name) {
        for (final RunSeq runSeq : this.runSequences) {
            if (runSeq.findGrid(name) != null) {
                return runSeq;
            }
        }
        return null;
    }
    
    Grid findGridByName(final String name) {
        for (final RunSeq runSeq : this.runSequences) {
            final Grid grid = runSeq.findGrid(name);
            if (null != grid) {
                return grid;
            }
        }
        return null;
    }
    
    VertTimeCoord findVertCoord(final String id) {
        if (id == null) {
            return null;
        }
        for (final VertTimeCoord vc : this.vertTimeCoords) {
            if (vc.getId().equals(id)) {
                return vc;
            }
        }
        return null;
    }
    
    VertTimeCoord findVertCoordByName(final String name) {
        for (final VertTimeCoord vc : this.vertTimeCoords) {
            if (vc.getName().equals(name)) {
                return vc;
            }
        }
        return null;
    }
    
    boolean replaceVertCoord(final ForecastModelRunInventory.VertCoord vc) {
        for (final VertTimeCoord vtc : this.vertTimeCoords) {
            if (vtc.getName().equals(vc.getName())) {
                vtc.vc.values1 = vc.values1;
                vtc.vc.values2 = vc.values2;
                vtc.vc.setId(vc.getId());
                vtc.vc.setUnits(vc.getUnits());
                return true;
            }
        }
        this.vertTimeCoords.add(new VertTimeCoord(vc));
        return false;
    }
    
    private ForecastModelRunInventory.EnsCoord findEnsCoord(final String id) {
        if (id == null) {
            return null;
        }
        for (final ForecastModelRunInventory.EnsCoord ec : this.ensCoords) {
            if (ec.getId().equals(id)) {
                return ec;
            }
        }
        return null;
    }
    
    public String writeDefinitionXML() {
        final XMLOutputter fmt = new XMLOutputter(Format.getPrettyFormat());
        return fmt.outputString(this.makeDefinitionXML());
    }
    
    public void writeDefinitionXML(final OutputStream os) throws IOException {
        final XMLOutputter fmt = new XMLOutputter(Format.getPrettyFormat());
        fmt.output(this.makeDefinitionXML(), os);
    }
    
    public Document makeDefinitionXML() {
        final Element rootElem = new Element("fmrcDefinition");
        final Document doc = new Document(rootElem);
        if (this.name != null) {
            rootElem.setAttribute("dataset", this.name);
        }
        if (null != this.suffixFilter) {
            rootElem.setAttribute("suffixFilter", this.suffixFilter);
        }
        Collections.sort(this.ensCoords);
        for (final ForecastModelRunInventory.EnsCoord ec : this.ensCoords) {
            final Element ecElem = new Element("ensCoord");
            rootElem.addContent(ecElem);
            ecElem.setAttribute("id", ec.getId());
            ecElem.setAttribute("name", ec.getName());
            ecElem.setAttribute("product_definition", Integer.toString(ec.getPDN()));
            final StringBuilder sbuff = new StringBuilder();
            final int[] ensTypes = ec.getEnsTypes();
            for (int j = 0; j < ensTypes.length; ++j) {
                if (j > 0) {
                    sbuff.append(" ");
                }
                sbuff.append(Integer.toString(ensTypes[j]));
            }
            ecElem.addContent(sbuff.toString());
        }
        Collections.sort(this.vertTimeCoords);
        for (final VertTimeCoord vtc : this.vertTimeCoords) {
            final ForecastModelRunInventory.VertCoord vc = vtc.vc;
            final Element vcElem = new Element("vertCoord");
            rootElem.addContent(vcElem);
            vcElem.setAttribute("id", vc.getId());
            vcElem.setAttribute("name", vc.getName());
            if (null != vc.getUnits()) {
                vcElem.setAttribute("units", vc.getUnits());
            }
            final StringBuilder sbuff2 = new StringBuilder();
            final double[] values1 = vc.getValues1();
            final double[] values2 = vc.getValues2();
            for (int i = 0; i < values1.length; ++i) {
                if (i > 0) {
                    sbuff2.append(" ");
                }
                sbuff2.append(Double.toString(values1[i]));
                if (values2 != null) {
                    sbuff2.append(",");
                    sbuff2.append(Double.toString(values2[i]));
                }
            }
            vcElem.addContent(sbuff2.toString());
        }
        Collections.sort(this.timeCoords);
        for (final ForecastModelRunInventory.TimeCoord tc : this.timeCoords) {
            final Element offsetElem = new Element("offsetHours");
            rootElem.addContent(offsetElem);
            offsetElem.setAttribute("id", tc.getId());
            final StringBuilder sbuff = new StringBuilder();
            final double[] offset = tc.getOffsetHours();
            for (int j = 0; j < offset.length; ++j) {
                if (j > 0) {
                    sbuff.append(" ");
                }
                sbuff.append(Double.toString(offset[j]));
            }
            offsetElem.addContent(sbuff.toString());
        }
        for (final RunSeq runSeq : this.runSequences) {
            final Element seqElem = new Element("runSequence");
            rootElem.addContent(seqElem);
            if (runSeq.isAll) {
                seqElem.setAttribute("allUseSeq", runSeq.allUseOffset.getId());
            }
            else {
                for (final Run run : runSeq.runs) {
                    final Element runElem = new Element("run");
                    seqElem.addContent(runElem);
                    runElem.setAttribute("runHour", Double.toString(run.runHour));
                    runElem.setAttribute("offsetHourSeq", run.tc.getId());
                }
            }
            for (final Grid grid : runSeq.vars) {
                final Element varElem = new Element("variable");
                seqElem.addContent(varElem);
                varElem.setAttribute("name", grid.name);
                if (grid.ec != null) {
                    varElem.setAttribute("ensCoord", grid.ec.getId());
                }
                if (grid.vtc != null) {
                    varElem.setAttribute("vertCoord", grid.vtc.getId());
                    if (grid.vtc.restrictList == null) {
                        continue;
                    }
                    final Iterator iter = grid.vtc.restrictList.iterator();
                    while (iter.hasNext()) {
                        final Element vtElem = new Element("vertCoord");
                        varElem.addContent(vtElem);
                        vtElem.setAttribute("restrict", iter.next());
                        vtElem.setText(iter.next());
                    }
                }
            }
        }
        return doc;
    }
    
    public boolean readDefinitionXML(final String xmlLocation) throws IOException {
        final File xml = new File(xmlLocation);
        if (!xml.exists()) {
            return false;
        }
        final InputStream is = new BufferedInputStream(new FileInputStream(xmlLocation));
        Document doc;
        try {
            final SAXBuilder builder = new SAXBuilder();
            doc = builder.build(is);
        }
        catch (JDOMException e) {
            throw new IOException(e.getMessage());
        }
        final Element rootElem = doc.getRootElement();
        this.name = rootElem.getAttributeValue("name");
        this.suffixFilter = rootElem.getAttributeValue("suffixFilter");
        this.ensCoords = new ArrayList<ForecastModelRunInventory.EnsCoord>();
        final List<Element> eList = (List<Element>)rootElem.getChildren("ensCoord");
        for (final Element ecElem : eList) {
            final ForecastModelRunInventory.EnsCoord ec = new ForecastModelRunInventory.EnsCoord();
            ec.setId(ecElem.getAttributeValue("id"));
            ec.setName(ecElem.getAttributeValue("name"));
            ec.setPDN(Integer.parseInt(ecElem.getAttributeValue("product_definition")));
            final String values = ecElem.getText();
            final StringTokenizer stoke = new StringTokenizer(values);
            final int n = stoke.countTokens();
            ec.setNEnsembles(n);
            final int[] ensType = new int[n];
            int count = 0;
            while (stoke.hasMoreTokens()) {
                final String toke = stoke.nextToken();
                final int pos = toke.indexOf(44);
                if (pos < 0) {
                    ensType[count] = Integer.parseInt(toke);
                }
                ++count;
            }
            ec.setEnsTypes(ensType);
            this.ensCoords.add(ec);
        }
        this.vertTimeCoords = new ArrayList<VertTimeCoord>();
        final List<Element> vList = (List<Element>)rootElem.getChildren("vertCoord");
        for (final Element vcElem : vList) {
            final ForecastModelRunInventory.VertCoord vc = new ForecastModelRunInventory.VertCoord();
            vc.setId(vcElem.getAttributeValue("id"));
            vc.setName(vcElem.getAttributeValue("name"));
            vc.setUnits(vcElem.getAttributeValue("units"));
            final String values2 = vcElem.getText();
            final StringTokenizer stoke2 = new StringTokenizer(values2);
            final int n2 = stoke2.countTokens();
            final double[] values3 = new double[n2];
            double[] values4 = null;
            int count2 = 0;
            while (stoke2.hasMoreTokens()) {
                final String toke2 = stoke2.nextToken();
                final int pos2 = toke2.indexOf(44);
                if (pos2 < 0) {
                    values3[count2] = Double.parseDouble(toke2);
                }
                else {
                    if (values4 == null) {
                        values4 = new double[n2];
                    }
                    final String val1 = toke2.substring(0, pos2);
                    final String val2 = toke2.substring(pos2 + 1);
                    values3[count2] = Double.parseDouble(val1);
                    values4[count2] = Double.parseDouble(val2);
                }
                ++count2;
            }
            vc.setValues1(values3);
            if (values4 != null) {
                vc.setValues2(values4);
            }
            final VertTimeCoord vtc = new VertTimeCoord(vc);
            this.vertTimeCoords.add(vtc);
        }
        this.timeCoords = new ArrayList<ForecastModelRunInventory.TimeCoord>();
        final List<Element> tList = (List<Element>)rootElem.getChildren("offsetHours");
        for (final Element timeElem : tList) {
            final ForecastModelRunInventory.TimeCoord tc = new ForecastModelRunInventory.TimeCoord();
            this.timeCoords.add(tc);
            tc.setId(timeElem.getAttributeValue("id"));
            final String values5 = timeElem.getText();
            final StringTokenizer stoke3 = new StringTokenizer(values5);
            final int n3 = stoke3.countTokens();
            final double[] offset = new double[n3];
            int count2 = 0;
            while (stoke3.hasMoreTokens()) {
                offset[count2++] = Double.parseDouble(stoke3.nextToken());
            }
            tc.setOffsetHours(offset);
        }
        this.runSequences = new ArrayList<RunSeq>();
        final List<Element> runseqList = (List<Element>)rootElem.getChildren("runSequence");
        for (final Element runseqElem : runseqList) {
            final String allUseId = runseqElem.getAttributeValue("allUseSeq");
            RunSeq rseq;
            if (allUseId != null) {
                rseq = new RunSeq(allUseId);
            }
            else {
                final List<Run> runs = new ArrayList<Run>();
                final List<Element> runList = (List<Element>)runseqElem.getChildren("run");
                for (final Element runElem : runList) {
                    final String id = runElem.getAttributeValue("offsetHourSeq");
                    final ForecastModelRunInventory.TimeCoord tc2 = this.findTimeCoord(id);
                    final String hour = runElem.getAttributeValue("runHour");
                    final Run run = new Run(tc2, Double.parseDouble(hour));
                    runs.add(run);
                }
                rseq = new RunSeq(runs);
            }
            this.runSequences.add(rseq);
            final List<Element> varList = (List<Element>)runseqElem.getChildren("variable");
            for (final Element varElem : varList) {
                final String name = varElem.getAttributeValue("name");
                final Grid grid = new Grid(name);
                rseq.vars.add(grid);
                grid.ec = this.findEnsCoord(varElem.getAttributeValue("ensCoord"));
                grid.vtc = this.findVertCoord(varElem.getAttributeValue("vertCoord"));
                final List<Element> rList = (List<Element>)varElem.getChildren("vertTimeCoord");
                if (rList.size() > 0) {
                    grid.vtc = new VertTimeCoord(grid.vtc.vc, rseq);
                    for (final Element vtElem : rList) {
                        final String vertCoords = vtElem.getAttributeValue("restrict");
                        final String timeCoords = vtElem.getText();
                        grid.vtc.addRestriction(vertCoords, timeCoords);
                    }
                }
            }
            Collections.sort((List<Comparable>)rseq.vars);
        }
        return true;
    }
    
    public void makeFromCollectionInventory(final FmrcInventory fmrc) {
        this.name = fmrc.getName();
        this.timeCoords = fmrc.getTimeCoords();
        this.ensCoords = fmrc.getEnsCoords();
        this.vertTimeCoords = new ArrayList<VertTimeCoord>();
        for (int i = 0; i < fmrc.getVertCoords().size(); ++i) {
            final ForecastModelRunInventory.VertCoord vc = fmrc.getVertCoords().get(i);
            this.vertTimeCoords.add(new VertTimeCoord(vc));
        }
        this.runSequences = new ArrayList<RunSeq>();
        final List<FmrcInventory.RunSeq> seqs = fmrc.getRunSequences();
        for (final FmrcInventory.RunSeq invSeq : seqs) {
            boolean isAll = true;
            ForecastModelRunInventory.TimeCoord oneTc = null;
            for (int j = 0; j < invSeq.runs.size(); ++j) {
                final FmrcInventory.Run run = invSeq.runs.get(j);
                if (j == 0) {
                    oneTc = run.tc;
                }
                else if (oneTc != run.tc) {
                    isAll = false;
                }
            }
            RunSeq runSeq;
            if (isAll) {
                runSeq = new RunSeq(oneTc.getId());
            }
            else {
                final List<Run> runs = new ArrayList<Run>();
                for (final FmrcInventory.Run invRun : invSeq.runs) {
                    final Run run2 = new Run(invRun.tc, this.getHour(invRun.runTime));
                    runs.add(run2);
                }
                runSeq = new RunSeq(runs);
            }
            this.runSequences.add(runSeq);
            final List<FmrcInventory.UberGrid> vars = invSeq.getVariables();
            for (final FmrcInventory.UberGrid uv : vars) {
                final Grid grid = new Grid(uv.getName());
                runSeq.vars.add(grid);
                if (uv.vertCoordUnion != null) {
                    grid.vtc = new VertTimeCoord(uv.vertCoordUnion);
                }
                if (uv.ensCoordUnion != null) {
                    grid.ec = uv.ensCoordUnion;
                }
            }
        }
        Collections.sort(this.vertTimeCoords);
    }
    
    public void addVertCoordsFromCollectionInventory(final FmrcInventory fmrc) {
        this.vertTimeCoords = new ArrayList<VertTimeCoord>();
        for (int i = 0; i < fmrc.getVertCoords().size(); ++i) {
            final ForecastModelRunInventory.VertCoord vc = fmrc.getVertCoords().get(i);
            this.vertTimeCoords.add(new VertTimeCoord(vc));
        }
        final List<FmrcInventory.RunSeq> seqs = fmrc.getRunSequences();
        for (final FmrcInventory.RunSeq invSeq : seqs) {
            final List<FmrcInventory.UberGrid> vars = invSeq.getVariables();
            for (final FmrcInventory.UberGrid uv : vars) {
                if (uv.vertCoordUnion != null) {
                    final String sname = uv.getName();
                    final Grid grid = this.findGridByName(sname);
                    grid.vtc = new VertTimeCoord(uv.vertCoordUnion);
                }
            }
        }
    }
    
    private double getHour(final Date d) {
        this.cal.setTime(d);
        final int hour = this.cal.get(11);
        final double min = this.cal.get(12);
        return hour + min / 60.0;
    }
    
    static void convertIds(final String datasetName, final String defName) throws IOException {
        System.out.println(datasetName);
        final ForecastModelRunInventory fmrInv = ForecastModelRunInventory.open(null, datasetName, 2, true);
        final FmrcDefinition fmrDef = new FmrcDefinition();
        fmrDef.readDefinitionXML(defName);
        boolean changed = false;
        final Map<String, Grid> hash = new HashMap<String, Grid>();
        for (final RunSeq runSeq : fmrDef.runSequences) {
            for (final Grid gridDef : runSeq.vars) {
                final String munged = StringUtil.replace(gridDef.name, '_', "");
                hash.put(munged, gridDef);
            }
        }
        final List<ForecastModelRunInventory.TimeCoord> fmrInvTimeCoords = fmrInv.getTimeCoords();
        for (final ForecastModelRunInventory.TimeCoord tc : fmrInvTimeCoords) {
            final List<ForecastModelRunInventory.Grid> fmrInvGrids = tc.getGrids();
            for (final ForecastModelRunInventory.Grid invGrid : fmrInvGrids) {
                final Grid defGrid = fmrDef.findGridByName(invGrid.name);
                if (null == defGrid) {
                    String munged2 = StringUtil.replace(invGrid.name, "-", "");
                    munged2 = StringUtil.replace(munged2, "_", "");
                    final Grid gridDefnew = hash.get(munged2);
                    if (gridDefnew != null) {
                        System.out.println(" replace " + gridDefnew.name + " with " + invGrid.name);
                        gridDefnew.name = invGrid.name;
                        changed = false;
                    }
                    else {
                        System.out.println("*** cant find replacement for grid= " + invGrid.name + " in the definition");
                    }
                }
            }
        }
        if (changed) {
            final int pos = defName.lastIndexOf("/");
            final String newDef = defName.substring(0, pos) + "/new/" + defName.substring(pos);
            final FileOutputStream fout = new FileOutputStream(newDef);
            fmrDef.writeDefinitionXML(fout);
        }
    }
    
    static void convert(final String datasetName, final String defName) throws IOException {
        System.out.println(datasetName);
        final ForecastModelRunInventory fmrInv = ForecastModelRunInventory.open(null, datasetName, 2, true);
        final FmrcDefinition fmrDef = new FmrcDefinition();
        fmrDef.readDefinitionXML(defName);
        final List<ForecastModelRunInventory.VertCoord> vcList = fmrInv.getVertCoords();
        for (final ForecastModelRunInventory.VertCoord vc : vcList) {
            final CoordinateAxis1D axis = vc.axis;
            if (axis == null) {
                System.out.println("*** No Axis " + vc.getName());
            }
            else {
                if (FmrcDefinition.showState) {
                    System.out.print(" " + vc.getName() + " contig= " + axis.isContiguous());
                }
                final boolean ok = fmrDef.replaceVertCoord(vc);
                if (!FmrcDefinition.showState) {
                    continue;
                }
                System.out.println(" = " + ok);
            }
        }
        Collections.sort(fmrDef.vertTimeCoords);
        for (final RunSeq runSeq : fmrDef.runSequences) {
            for (final Grid gridDef : runSeq.vars) {
                final ForecastModelRunInventory.Grid gridInv = fmrInv.findGrid(gridDef.name);
                if (gridInv == null) {
                    System.out.println("*** cant find def grid= " + gridDef.name + " in the inventory ");
                }
                else {
                    if (gridInv.vc == null) {
                        continue;
                    }
                    final VertTimeCoord new_vtc = fmrDef.findVertCoordByName(gridInv.vc.getName());
                    if (new_vtc == null) {
                        System.out.println("*** cant find VertCoord= " + gridInv.vc.getName());
                    }
                    else {
                        gridDef.vtc = new_vtc;
                        if (!FmrcDefinition.showState) {
                            continue;
                        }
                        System.out.println(" ok= " + gridDef.name);
                    }
                }
            }
        }
        final int pos = defName.lastIndexOf("/");
        final String newDef = defName.substring(0, pos) + "/new/" + defName.substring(pos);
        final FileOutputStream fout = new FileOutputStream(newDef);
        fmrDef.writeDefinitionXML(fout);
        final List<ForecastModelRunInventory.TimeCoord> fmrInvTimeCoords = fmrInv.getTimeCoords();
        for (final ForecastModelRunInventory.TimeCoord tc : fmrInvTimeCoords) {
            final List<ForecastModelRunInventory.Grid> fmrInvGrids = tc.getGrids();
            for (final ForecastModelRunInventory.Grid invGrid : fmrInvGrids) {
                final Grid defGrid = fmrDef.findGridByName(invGrid.name);
                if (null == defGrid) {
                    System.out.println("*** cant find inv grid= " + invGrid.name + " in the definition");
                }
                else {
                    final ForecastModelRunInventory.VertCoord inv_vc = invGrid.vc;
                    if (inv_vc == null && defGrid.vtc == null) {
                        continue;
                    }
                    if (inv_vc != null && defGrid.vtc == null) {
                        System.out.println("*** mismatch " + invGrid.name + " VertCoord: inv= " + inv_vc.getSize() + ", no def ");
                    }
                    else if (inv_vc == null && defGrid.vtc != null) {
                        final ForecastModelRunInventory.VertCoord def_vc = defGrid.vtc.vc;
                        System.out.println("*** mismatch " + invGrid.name + " VertCoord: def= " + def_vc.getSize() + ", no inv ");
                    }
                    else {
                        final ForecastModelRunInventory.VertCoord def_vc = defGrid.vtc.vc;
                        if (inv_vc.getSize() == def_vc.getSize()) {
                            continue;
                        }
                        System.out.println("*** mismatch " + invGrid.name + " VertCoord size: inv= " + inv_vc.getSize() + ", def = " + def_vc.getSize());
                    }
                }
            }
        }
    }
    
    static void showVertCoords(final String datasetName, final String defName) throws IOException {
        System.out.println("--------------------------------------");
        System.out.println(defName);
        final FmrcDefinition fmrDef = new FmrcDefinition();
        fmrDef.readDefinitionXML(defName);
        System.out.println(datasetName);
        final ForecastModelRunInventory fmrInv = ForecastModelRunInventory.open(null, datasetName, 2, true);
        final List<ForecastModelRunInventory.VertCoord> vcList = fmrInv.getVertCoords();
        for (final ForecastModelRunInventory.VertCoord vc : vcList) {
            final CoordinateAxis1D axis = vc.axis;
            if (axis == null) {
                System.out.println(" No Axis " + vc.getName());
            }
            else if (axis.isInterval()) {
                System.out.println(" Layer " + vc.getName() + " contig= " + axis.isContiguous());
                findGridsForVertCoord(fmrDef, vc);
            }
            final boolean got = fmrDef.findVertCoordByName(vc.getName()) != null;
            if (!got) {
                System.out.println(" ***NOT " + vc.getName());
            }
        }
    }
    
    static void findGridsForVertCoord(final FmrcDefinition fmrDef, final ForecastModelRunInventory.VertCoord vc) {
        for (final RunSeq runSeq : fmrDef.runSequences) {
            for (final Grid grid : runSeq.vars) {
                if (grid.vtc != null && grid.vtc.vc == vc) {
                    final List restrictList = grid.vtc.restrictList;
                    if (restrictList == null || restrictList.size() <= 0) {
                        continue;
                    }
                    System.out.println(" TimeVertCoord refers to this vertical coordinate");
                }
            }
        }
    }
    
    public static String[] getDefinitionFiles() {
        if (FmrcDefinition.fmrcDefinitionFiles == null) {
            FmrcDefinition.fmrcDefinitionFiles = new String[FmrcDefinition.fmrcDatasets.length];
            int count = 0;
            for (final String ds : FmrcDefinition.fmrcDatasets) {
                FmrcDefinition.fmrcDefinitionFiles[count++] = FmrcDefinition.fmrcDefinitionDir + StringUtil.replace(ds, '/', "-") + ".fmrcDefinition.xml";
            }
        }
        return FmrcDefinition.fmrcDefinitionFiles;
    }
    
    public static void main(final String[] args) throws IOException {
        for (int i = 0; i < FmrcDefinition.exampleFiles.length; i += 2) {
            convertIds(FmrcDefinition.exampleFiles[i], FmrcDefinition.exampleFiles[i + 1]);
        }
    }
    
    static {
        FmrcDefinition.log = LoggerFactory.getLogger(FmrcDefinition.class);
        FmrcDefinition.showState = false;
        FmrcDefinition.fmrcDatasets = new String[] { "NCEP/GFS/Alaska_191km", "NCEP/GFS/CONUS_80km", "NCEP/GFS/CONUS_95km", "NCEP/GFS/CONUS_191km", "NCEP/GFS/Global_0p5deg", "NCEP/GFS/Global_onedeg", "NCEP/GFS/Global_2p5deg", "NCEP/GFS/Hawaii_160km", "NCEP/GFS/N_Hemisphere_381km", "NCEP/GFS/Puerto_Rico_191km", "NCEP/NAM/Alaska_11km", "NCEP/NAM/Alaska_22km", "NCEP/NAM/Alaska_45km/noaaport", "NCEP/NAM/Alaska_45km/conduit", "NCEP/NAM/Alaska_95km", "NCEP/NAM/CONUS_12km/conduit", "NCEP/NAM/CONUS_20km/surface", "NCEP/NAM/CONUS_20km/selectsurface", "NCEP/NAM/CONUS_20km/noaaport", "NCEP/NAM/CONUS_40km/conduit", "NCEP/NAM/CONUS_80km", "NCEP/NAM/Polar_90km", "NCEP/RUC2/CONUS_20km/surface", "NCEP/RUC2/CONUS_20km/pressure", "NCEP/RUC2/CONUS_20km/hybrid", "NCEP/RUC2/CONUS_40km", "NCEP/RUC/CONUS_80km", "NCEP/DGEX/CONUS_12km", "NCEP/DGEX/Alaska_12km", "NCEP/SREF/CONUS_40km/ensprod", "NCEP/SREF/CONUS_40km/ensprod_biasc", "NCEP/SREF/Alaska_45km/ensprod", "NCEP/SREF/PacificNE_0p4/ensprod", "NCEP/NDFD/CONUS_5km", "NCEP/WW3/Alaskan_4minute", "NCEP/WW3/Alaskan_10minute", "NCEP/WW3/Atlantic_4minute", "NCEP/WW3/Atlantic_10minute", "NCEP/WW3/EasternPacific_10minute", "NCEP/WW3/Global_30minute", "NCEP/WW3/WestCoast_4minute", "NCEP/WW3/WestCoast_10minute" };
        FmrcDefinition.fmrcDatasets_41 = new String[] { "NCEP/GFS/Alaska_191km", "NCEP/GFS/CONUS_80km", "NCEP/GFS/CONUS_95km", "NCEP/GFS/CONUS_191km", "NCEP/GFS/Global_0p5deg", "NCEP/GFS/Global_onedeg", "NCEP/GFS/Global_2p5deg", "NCEP/GFS/Hawaii_160km", "NCEP/GFS/N_Hemisphere_381km", "NCEP/GFS/Puerto_Rico_191km", "NCEP/NAM/Alaska_11km", "NCEP/NAM/Alaska_22km", "NCEP/NAM/Alaska_45km/noaaport", "NCEP/NAM/Alaska_45km/conduit", "NCEP/NAM/Alaska_95km", "NCEP/NAM/CONUS_12km/conduit", "NCEP/NAM/CONUS_20km/surface", "NCEP/NAM/CONUS_20km/selectsurface", "NCEP/NAM/CONUS_20km/noaaport", "NCEP/NAM/CONUS_40km/conduit", "NCEP/NAM/CONUS_80km", "NCEP/NAM/Polar_90km", "NCEP/RUC2/CONUS_20km/surface", "NCEP/RUC2/CONUS_20km/pressure", "NCEP/RUC2/CONUS_20km/hybrid", "NCEP/RUC2/CONUS_40km", "NCEP/RUC/CONUS_80km", "NCEP/DGEX/CONUS_12km", "NCEP/DGEX/Alaska_12km", "NCEP/NDFD/CONUS_5km" };
        FmrcDefinition.fmrcDefinitionDir = "C:/dev/tds/thredds/tds/src/main/webapp/WEB-INF/altContent/idd/thredds/modelInventory/";
        FmrcDefinition.exampleFiles = new String[] { "R:/testdata/motherlode/grid/RUC2_CONUS_20km_surface_20060825_1400.grib2", "R:/testdata/motherlode/grid/modelDefs/NCEP-RUC2-CONUS_20km-surface.fmrcDefinition.xml" };
    }
    
    public static class Grid implements Comparable
    {
        private String name;
        private VertTimeCoord vtc;
        private ForecastModelRunInventory.EnsCoord ec;
        
        Grid(final String name) {
            this.vtc = null;
            this.ec = null;
            this.name = name;
        }
        
        public EnsCoord getEnsTimeCoord() {
            return this.ec;
        }
        
        public VertTimeCoord getVertTimeCoord() {
            return this.vtc;
        }
        
        public int countVertCoords(final double offsetHour) {
            return (this.vtc == null) ? 1 : this.vtc.countVertCoords(offsetHour);
        }
        
        public double[] getVertCoords(final double hourOffset) {
            if (this.vtc == null) {
                final double[] result = { -0.0 };
                return result;
            }
            return this.vtc.getVertCoords(hourOffset);
        }
        
        public int compareTo(final Object o) {
            final Grid other = (Grid)o;
            return this.name.compareTo(other.name);
        }
    }
    
    class Run
    {
        double runHour;
        ForecastModelRunInventory.TimeCoord tc;
        
        Run(final ForecastModelRunInventory.TimeCoord tc, final double runHour) {
            this.tc = tc;
            this.runHour = runHour;
        }
    }
    
    public class RunSeq
    {
        private boolean isAll;
        private ForecastModelRunInventory.TimeCoord allUseOffset;
        private List<Run> runs;
        private List<Grid> vars;
        private int num;
        
        RunSeq(final String id) {
            this.isAll = false;
            this.runs = new ArrayList<Run>();
            this.vars = new ArrayList<Grid>();
            this.num = 0;
            this.isAll = true;
            this.allUseOffset = FmrcDefinition.this.findTimeCoord(id);
            this.num = FmrcDefinition.this.runseq_num++;
        }
        
        RunSeq(final List<Run> runs) {
            this.isAll = false;
            this.runs = new ArrayList<Run>();
            this.vars = new ArrayList<Grid>();
            this.num = 0;
            this.runs = runs;
            this.num = FmrcDefinition.this.runseq_num++;
            int matchIndex = 0;
            final Run last = runs.get(runs.size() - 1);
            double runHour = last.runHour;
            while (runHour < 24.0) {
                final Run match = runs.get(matchIndex);
                final Run next = runs.get(matchIndex + 1);
                final double incr = next.runHour - match.runHour;
                if (incr <= 0.0) {
                    break;
                }
                runHour += incr;
                runs.add(new Run(next.tc, runHour));
                ++matchIndex;
            }
        }
        
        public String getName() {
            return (this.num == 0) ? "time" : ("time" + this.num);
        }
        
        public ForecastModelRunInventory.TimeCoord findTimeCoordByRuntime(final Date runTime) {
            if (this.isAll) {
                return this.allUseOffset;
            }
            final double hour = FmrcDefinition.this.getHour(runTime);
            final Run run = this.findRun(hour);
            if (run == null) {
                return null;
            }
            return run.tc;
        }
        
        Run findRun(final double hour) {
            for (final Run run : this.runs) {
                if (run.runHour == hour) {
                    return run;
                }
            }
            return null;
        }
        
        public Grid findGrid(final String name) {
            if (name == null) {
                return null;
            }
            for (final Grid grid : this.vars) {
                if (name.equals(grid.name)) {
                    return grid;
                }
            }
            return null;
        }
    }
    
    class VertTimeCoord implements Comparable
    {
        ForecastModelRunInventory.VertCoord vc;
        ForecastModelRunInventory.TimeCoord tc;
        int ntimes;
        int nverts;
        double[][] vcForTimeIndex;
        List<String> restrictList;
        
        VertTimeCoord(final ForecastModelRunInventory.VertCoord vc) {
            this.vc = vc;
            this.ntimes = ((this.tc == null) ? 1 : this.tc.getOffsetHours().length);
            this.nverts = vc.getValues1().length;
        }
        
        VertTimeCoord(final ForecastModelRunInventory.VertCoord vc, final RunSeq runSeq) {
            if (runSeq.isAll) {
                this.tc = runSeq.allUseOffset;
            }
            else {
                final Set<Double> valueSet = new HashSet<Double>();
                for (final Run run : runSeq.runs) {
                    this.addValues(valueSet, run.tc.getOffsetHours());
                }
                final List<Double> valueList = Arrays.asList((Double[])valueSet.toArray((T[])new Double[valueSet.size()]));
                Collections.sort(valueList);
                final double[] values = new double[valueList.size()];
                for (int i = 0; i < valueList.size(); ++i) {
                    values[i] = valueList.get(i);
                }
                (this.tc = new ForecastModelRunInventory.TimeCoord()).setOffsetHours(values);
                this.tc.setId("union");
            }
            this.vc = vc;
            this.ntimes = ((this.tc == null) ? 1 : this.tc.getOffsetHours().length);
            this.nverts = vc.getValues1().length;
        }
        
        private void addValues(final Set<Double> valueSet, final double[] values) {
            for (final double value : values) {
                valueSet.add(value);
            }
        }
        
        String getId() {
            return this.vc.getId();
        }
        
        String getName() {
            return this.vc.getName();
        }
        
        void addRestriction(final String vertCoordsString, final String timeCoords) {
            StringTokenizer stoker = new StringTokenizer(vertCoordsString, " ,");
            final int n = stoker.countTokens();
            final double[] vertCoords = new double[n];
            int count = 0;
            while (stoker.hasMoreTokens()) {
                vertCoords[count++] = Double.parseDouble(stoker.nextToken());
            }
            if (this.vcForTimeIndex == null) {
                this.restrictList = new ArrayList<String>();
                this.vcForTimeIndex = new double[this.ntimes][];
                for (int i = 0; i < this.vcForTimeIndex.length; ++i) {
                    this.vcForTimeIndex[i] = this.vc.getValues1();
                }
            }
            this.restrictList.add(vertCoordsString);
            this.restrictList.add(timeCoords);
            stoker = new StringTokenizer(timeCoords, " ,");
            while (stoker.hasMoreTokens()) {
                final double hour = Double.parseDouble(stoker.nextToken());
                final int index = this.tc.findIndex(hour);
                if (index < 0) {
                    FmrcDefinition.log.error("hour Offset" + hour + " not found in TimeCoord " + this.tc.getId());
                }
                this.vcForTimeIndex[index] = vertCoords;
            }
        }
        
        double[] getVertCoords(final double offsetHour) {
            if (this.tc == null || null == this.vcForTimeIndex) {
                return this.vc.getValues1();
            }
            final int index = this.tc.findIndex(offsetHour);
            if (index < 0) {
                return new double[0];
            }
            return this.vcForTimeIndex[index];
        }
        
        int countVertCoords(final double offsetHour) {
            if (this.tc == null || null == this.vcForTimeIndex) {
                return this.vc.getValues1().length;
            }
            final int index = this.tc.findIndex(offsetHour);
            if (index < 0) {
                return 0;
            }
            return this.vcForTimeIndex[index].length;
        }
        
        public int compareTo(final Object o) {
            final VertTimeCoord other = (VertTimeCoord)o;
            return this.getName().compareTo(other.getName());
        }
    }
}
