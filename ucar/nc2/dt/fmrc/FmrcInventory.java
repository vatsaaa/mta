// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.fmrc;

import thredds.catalog.InvCatalogRef;
import thredds.catalog.InvAccess;
import thredds.catalog.ServiceType;
import thredds.catalog.InvDataset;
import ucar.nc2.util.Misc;
import org.slf4j.LoggerFactory;
import ucar.nc2.util.CancelTask;
import thredds.catalog.crawl.CatalogCrawler;
import ucar.unidata.util.StringUtil;
import java.io.FileOutputStream;
import java.io.File;
import ucar.nc2.util.DiskCache2;
import org.jdom.Content;
import org.jdom.Element;
import org.jdom.Document;
import java.io.OutputStream;
import org.jdom.output.XMLOutputter;
import org.jdom.output.Format;
import java.util.Arrays;
import java.util.Collections;
import java.util.Collection;
import java.util.Iterator;
import java.io.IOException;
import java.util.TimeZone;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.HashMap;
import java.util.ArrayList;
import ucar.nc2.units.DateFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import java.util.Map;
import java.util.List;
import java.text.SimpleDateFormat;
import org.slf4j.Logger;

public class FmrcInventory
{
    private static Logger log;
    private static SimpleDateFormat dateFormatShort;
    private static SimpleDateFormat dateFormat;
    private static boolean debug;
    private String name;
    private int tc_seqno;
    private List<ForecastModelRunInventory.TimeCoord> timeCoords;
    private int ec_seqno;
    private List<ForecastModelRunInventory.EnsCoord> ensCoords;
    private int vc_seqno;
    private List<ForecastModelRunInventory.VertCoord> vertCoords;
    private int run_seqno;
    private List<RunSeq> runSequences;
    private Map<String, UberGrid> uvHash;
    private List<UberGrid> varList;
    private Set<Date> runTimeHash;
    private List<Date> runTimeList;
    private Set<Double> offsetHash;
    private List<Double> offsetList;
    private Set<Date> forecastTimeHash;
    private List<Date> forecastTimeList;
    private String fmrcDefinitionDir;
    private FmrcDefinition definition;
    private TimeMatrixDataset tmAll;
    private Calendar cal;
    private DateFormatter formatter;
    private static boolean debugTiming;
    
    FmrcInventory(final String fmrcDefinitionDir, final String name) throws IOException {
        this.tc_seqno = 0;
        this.timeCoords = new ArrayList<ForecastModelRunInventory.TimeCoord>();
        this.ec_seqno = 0;
        this.ensCoords = new ArrayList<ForecastModelRunInventory.EnsCoord>();
        this.vc_seqno = 0;
        this.vertCoords = new ArrayList<ForecastModelRunInventory.VertCoord>();
        this.run_seqno = 0;
        this.runSequences = new ArrayList<RunSeq>();
        this.uvHash = new HashMap<String, UberGrid>();
        this.runTimeHash = new HashSet<Date>();
        this.offsetHash = new HashSet<Double>();
        this.forecastTimeHash = new HashSet<Date>();
        this.definition = null;
        this.cal = new GregorianCalendar();
        this.formatter = new DateFormatter();
        this.fmrcDefinitionDir = fmrcDefinitionDir;
        final int pos = name.indexOf(".fmrcDefinition.xml");
        if (pos > 0) {
            this.name = name.substring(0, pos);
        }
        else {
            this.name = name;
        }
        final FmrcDefinition fmrc = new FmrcDefinition();
        if (fmrc.readDefinitionXML(this.getDefinitionPath())) {
            this.definition = fmrc;
        }
        else {
            FmrcInventory.log.warn("FmrcCollection has no Definition " + this.getDefinitionPath());
        }
        this.cal.setTimeZone(TimeZone.getTimeZone("UTC"));
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getDefinitionPath() {
        if (this.fmrcDefinitionDir != null) {
            return this.fmrcDefinitionDir + this.name + ".fmrcDefinition.xml";
        }
        return "./" + this.name + ".fmrcDefinition.xml";
    }
    
    public List<ForecastModelRunInventory.TimeCoord> getTimeCoords() {
        return this.timeCoords;
    }
    
    public List<RunSeq> getRunSequences() {
        return this.runSequences;
    }
    
    public List<ForecastModelRunInventory.EnsCoord> getEnsCoords() {
        return this.ensCoords;
    }
    
    public List<ForecastModelRunInventory.VertCoord> getVertCoords() {
        return this.vertCoords;
    }
    
    public String getSuffixFilter() {
        return (this.definition == null) ? null : this.definition.getSuffixFilter();
    }
    
    public FmrcDefinition getDefinition() {
        return this.definition;
    }
    
    void addRun(final ForecastModelRunInventory fmr) {
        if (FmrcInventory.debug) {
            System.out.println(" Adding ForecastModelRun " + fmr.getRunDateString());
        }
        this.runTimeHash.add(fmr.getRunDate());
        for (final ForecastModelRunInventory.TimeCoord tc : fmr.getTimeCoords()) {
            ForecastModelRunInventory.TimeCoord tcUse = this.findTime(tc);
            if (tcUse == null) {
                this.timeCoords.add(tc);
                tcUse = tc;
                tc.setId(Integer.toString(this.tc_seqno));
                ++this.tc_seqno;
            }
            final Run run = new Run(fmr.getRunDate(), tcUse);
            for (final double offset : tcUse.getOffsetHours()) {
                final Date fcDate = this.addHour(fmr.getRunDate(), offset);
                final Inventory inv = new Inventory(fmr.getRunDate(), fcDate, offset);
                run.invList.add(inv);
                this.forecastTimeHash.add(fcDate);
                this.offsetHash.add(offset);
            }
            for (final ForecastModelRunInventory.Grid grid : tc.getGrids()) {
                UberGrid uv = this.uvHash.get(grid.name);
                if (uv == null) {
                    if (this.definition != null && null == this.definition.findSeqForVariable(grid.name)) {
                        FmrcInventory.log.warn("FmrcCollection Definition " + this.name + " does not contain variable " + grid.name);
                        continue;
                    }
                    uv = new UberGrid(grid.name);
                    this.uvHash.put(grid.name, uv);
                }
                uv.addRun(run, grid);
            }
        }
    }
    
    void finish() {
        Collections.sort(this.varList = new ArrayList<UberGrid>(this.uvHash.values()));
        Collections.sort(this.runTimeList = Arrays.asList((Date[])this.runTimeHash.toArray((T[])new Date[this.runTimeHash.size()])));
        Collections.sort(this.forecastTimeList = Arrays.asList((Date[])this.forecastTimeHash.toArray((T[])new Date[this.forecastTimeHash.size()])));
        Collections.sort(this.offsetList = Arrays.asList((Double[])this.offsetHash.toArray((T[])new Double[this.offsetHash.size()])));
        for (final UberGrid uv : this.varList) {
            uv.finish();
            (uv.seq = this.findRunSequence(uv.runs)).addVariable(uv);
        }
    }
    
    private UberGrid findVar(final String varName) {
        for (final UberGrid uv : this.varList) {
            if (uv.name.equals(varName)) {
                return uv;
            }
        }
        return null;
    }
    
    private RunSeq findRunSequence(final List<RunExpected> runs) {
        for (final RunSeq seq : this.runSequences) {
            if (seq.equalsData(runs)) {
                return seq;
            }
        }
        final RunSeq seq2 = new RunSeq(runs);
        this.runSequences.add(seq2);
        return seq2;
    }
    
    private ForecastModelRunInventory.TimeCoord findTime(final ForecastModelRunInventory.TimeCoord want) {
        for (final ForecastModelRunInventory.TimeCoord tc : this.timeCoords) {
            if (want.equalsData(tc)) {
                return tc;
            }
        }
        return null;
    }
    
    private ForecastModelRunInventory.EnsCoord findEnsCoord(final ForecastModelRunInventory.EnsCoord want) {
        for (final ForecastModelRunInventory.EnsCoord ec : this.ensCoords) {
            if (want.equalsData(ec)) {
                return ec;
            }
        }
        return null;
    }
    
    private ForecastModelRunInventory.VertCoord findVertCoord(final ForecastModelRunInventory.VertCoord want) {
        for (final ForecastModelRunInventory.VertCoord vc : this.vertCoords) {
            if (want.equalsData(vc)) {
                return vc;
            }
        }
        return null;
    }
    
    private Date addHour(final Date d, final double hour) {
        this.cal.setTime(d);
        final int ihour = (int)hour;
        final int imin = (int)(hour - ihour) * 60;
        this.cal.add(11, ihour);
        this.cal.add(12, imin);
        return this.cal.getTime();
    }
    
    private double getOffsetHour(final Date run, final Date forecast) {
        final double diff = (double)(forecast.getTime() - run.getTime());
        return diff / 1000.0 / 60.0 / 60.0;
    }
    
    public String writeMatrixXML(final String varName) {
        final XMLOutputter fmt = new XMLOutputter(Format.getPrettyFormat());
        if (varName == null) {
            return fmt.outputString(this.makeMatrixDocument());
        }
        return fmt.outputString(this.makeMatrixDocument(varName));
    }
    
    public void writeMatrixXML(final String varName, final OutputStream os) throws IOException {
        final XMLOutputter fmt = new XMLOutputter(Format.getPrettyFormat());
        if (varName == null) {
            fmt.output(this.makeMatrixDocument(), os);
        }
        else {
            fmt.output(this.makeMatrixDocument(varName), os);
        }
    }
    
    public Document makeMatrixDocument() {
        if (this.tmAll == null) {
            this.tmAll = new TimeMatrixDataset();
        }
        final Element rootElem = new Element("forecastModelRunCollectionInventory");
        final Document doc = new Document(rootElem);
        rootElem.setAttribute("dataset", this.name);
        for (final Double offset : this.offsetList) {
            final Element offsetElem = new Element("offsetTime");
            rootElem.addContent(offsetElem);
            offsetElem.setAttribute("hours", offset.toString());
        }
        for (final UberGrid uv : this.varList) {
            final Element varElem = new Element("variable");
            rootElem.addContent(varElem);
            varElem.setAttribute("name", uv.name);
            this.addCountPercent(uv.countInv, uv.countExpected, varElem, false);
        }
        for (int i = this.runTimeList.size() - 1; i >= 0; --i) {
            final Element runElem = new Element("run");
            rootElem.addContent(runElem);
            final Date runTime = this.runTimeList.get(i);
            runElem.setAttribute("date", FmrcInventory.dateFormat.format(runTime));
            this.addCountPercent(this.tmAll.countTotalRunInv[i], this.tmAll.expectedTotalRun[i], runElem, true);
            for (int k = 0; k < this.offsetList.size(); ++k) {
                final Element offsetElem2 = new Element("offset");
                runElem.addContent(offsetElem2);
                final Double offset2 = this.offsetList.get(k);
                offsetElem2.setAttribute("hours", offset2.toString());
                this.addCountPercent(this.tmAll.countOffsetInv[i][k], this.tmAll.expectedOffset[i][k], offsetElem2, false);
            }
        }
        for (int j = this.forecastTimeList.size() - 1; j >= 0; --j) {
            final Element fcElem = new Element("forecastTime");
            rootElem.addContent(fcElem);
            final Date ftime = this.forecastTimeList.get(j);
            fcElem.setAttribute("date", FmrcInventory.dateFormat.format(ftime));
            for (int l = this.runTimeList.size() - 1; l >= 0; --l) {
                final Element rtElem = new Element("runTime");
                fcElem.addContent(rtElem);
                this.addCountPercent(this.tmAll.countInv[j][l], this.tmAll.expected[j][l], rtElem, false);
            }
        }
        return doc;
    }
    
    private void addCountPercent(final int have, final int want, final Element elem, final boolean always) {
        if ((have == want || want == 0) && have != 0) {
            elem.setAttribute("count", Integer.toString(have));
            if (always) {
                elem.setAttribute("percent", "100");
            }
        }
        else if (want != 0) {
            final int percent = (int)(100.0 * have / want);
            elem.setAttribute("count", have + "/" + want);
            elem.setAttribute("percent", Integer.toString(percent));
        }
    }
    
    public Document makeMatrixDocument(final String varName) {
        final UberGrid uv = this.findVar(varName);
        if (uv == null) {
            throw new IllegalArgumentException("No variable named = " + varName);
        }
        final Element rootElem = new Element("forecastModelRunCollectionInventory");
        final Document doc = new Document(rootElem);
        rootElem.setAttribute("dataset", this.name);
        rootElem.setAttribute("variable", uv.name);
        for (int k = 0; k < this.offsetList.size(); ++k) {
            final Element offsetElem = new Element("offsetTime");
            rootElem.addContent(offsetElem);
            final Double offset = this.offsetList.get(k);
            offsetElem.setAttribute("hour", offset.toString());
        }
        for (int i = this.runTimeList.size() - 1; i >= 0; --i) {
            final Element runElem = new Element("run");
            rootElem.addContent(runElem);
            final Date runTime = this.runTimeList.get(i);
            runElem.setAttribute("date", FmrcInventory.dateFormat.format(runTime));
            final RunExpected rune = uv.findRun(runTime);
            for (final Double offset2 : this.offsetList) {
                final Element offsetElem2 = new Element("offset");
                runElem.addContent(offsetElem2);
                final double hourOffset = offset2;
                offsetElem2.setAttribute("hour", offset2.toString());
                final int missing = rune.countInventory(hourOffset);
                final int expected = rune.countExpected(hourOffset);
                this.addCountPercent(missing, expected, offsetElem2, false);
            }
        }
        for (int k = this.forecastTimeList.size() - 1; k >= 0; --k) {
            final Element fcElem = new Element("forecastTime");
            rootElem.addContent(fcElem);
            final Date forecastTime = this.forecastTimeList.get(k);
            fcElem.setAttribute("date", FmrcInventory.dateFormat.format(forecastTime));
            for (int j = this.runTimeList.size() - 1; j >= 0; --j) {
                final Element rtElem = new Element("runTime");
                fcElem.addContent(rtElem);
                final Date runTime2 = this.runTimeList.get(j);
                final RunExpected rune2 = uv.findRun(runTime2);
                final double hourOffset = this.getOffsetHour(runTime2, forecastTime);
                final int missing = rune2.countInventory(hourOffset);
                final int expected = rune2.countExpected(hourOffset);
                this.addCountPercent(missing, expected, rtElem, false);
            }
        }
        return doc;
    }
    
    public String showOffsetHour(final String varName, final String offsetHour) {
        final UberGrid uv = this.findVar(varName);
        if (uv == null) {
            return "No variable named = " + varName;
        }
        final double hour = Double.parseDouble(offsetHour);
        final StringBuilder sbuff = new StringBuilder();
        sbuff.append("Inventory for ").append(varName).append(" for offset hour= ").append(offsetHour).append("\n");
        for (final RunExpected rune : uv.runs) {
            final double[] vcoords = rune.grid.getVertCoords(hour);
            sbuff.append(" Run ");
            sbuff.append(this.formatter.toDateTimeString(rune.run.runTime));
            sbuff.append(": ");
            for (int j = 0; j < vcoords.length; ++j) {
                if (j > 0) {
                    sbuff.append(",");
                }
                sbuff.append(vcoords[j]);
            }
            sbuff.append("\n");
        }
        sbuff.append("\nExpected for ").append(varName).append(" for offset hour= ").append(offsetHour).append("\n");
        for (final RunExpected rune : uv.runs) {
            final double[] vcoords = rune.expectedGrid.getVertCoords(hour);
            sbuff.append(" Run ");
            sbuff.append(this.formatter.toDateTimeString(rune.run.runTime));
            sbuff.append(": ");
            for (int j = 0; j < vcoords.length; ++j) {
                if (j > 0) {
                    sbuff.append(",");
                }
                sbuff.append(vcoords[j]);
            }
            sbuff.append("\n");
        }
        return sbuff.toString();
    }
    
    public static FmrcInventory makeFromDirectory(final String fmrcDefinitionPath, final String collectionName, final DiskCache2 fmr_cache, final String dirName, String suffix, final int mode) throws Exception {
        final long startTime = System.currentTimeMillis();
        final FmrcInventory fmrCollection = new FmrcInventory(fmrcDefinitionPath, collectionName);
        if (fmrCollection.getSuffixFilter() != null) {
            suffix = fmrCollection.getSuffixFilter();
        }
        final File dir = new File(dirName);
        final File[] files = dir.listFiles();
        if (null == files) {
            return null;
        }
        for (final File file : files) {
            if (file.getPath().endsWith(suffix)) {
                final ForecastModelRunInventory fmr = ForecastModelRunInventory.open(fmr_cache, file.getPath(), mode, true);
                if (null != fmr) {
                    fmrCollection.addRun(fmr);
                }
            }
        }
        fmrCollection.finish();
        if (FmrcInventory.debugTiming) {
            final long took = System.currentTimeMillis() - startTime;
            System.out.println("that took = " + took + " msecs");
        }
        return fmrCollection;
    }
    
    public static void main2(final String[] args) throws Exception {
        final String dir = "nam/c20s";
        final FmrcInventory fmrc = makeFromDirectory("R:/testdata/motherlode/grid/inv/new/", "NCEP-NAM-CONUS_20km-surface", null, "C:/data/grib/" + dir, "grib1", 2);
        FmrcDefinition def = fmrc.getDefinition();
        if (null != def) {
            System.out.println("current definition = " + fmrc.getDefinitionPath());
            System.out.println(def.writeDefinitionXML());
        }
        else {
            System.out.println("write definition to " + fmrc.getDefinitionPath());
            def = new FmrcDefinition();
            def.makeFromCollectionInventory(fmrc);
            final FileOutputStream fos = new FileOutputStream(fmrc.getDefinitionPath());
            System.out.println(def.writeDefinitionXML());
            def.writeDefinitionXML(fos);
        }
        final String varName = "Temperature";
        System.out.println(fmrc.writeMatrixXML(varName));
        final FileOutputStream fos2 = new FileOutputStream("C:/data/grib/" + dir + "/fmrcMatrix.xml");
        fmrc.writeMatrixXML(varName, fos2);
        System.out.println(fmrc.writeMatrixXML(null));
        final FileOutputStream fos3 = new FileOutputStream("C:/data/grib/" + dir + "/fmrcMatrixAll.xml");
        fmrc.writeMatrixXML(null, fos3);
        System.out.println(fmrc.showOffsetHour(varName, "7.0"));
    }
    
    public static void main(final String[] args) throws Exception {
        for (final String cat : FmrcDefinition.fmrcDatasets) {
            if (cat.contains("/RUC")) {
                doOne(cat, 72);
            }
            else {
                doOne(cat, 12);
            }
        }
    }
    
    public static void doOne(final String cat, final int n) throws Exception {
        final String server = "http://motherlode.ucar.edu:8080/thredds/catalog/fmrc/";
        final String writeDir = "D:/temp/modelDef/";
        new File(writeDir).mkdirs();
        final String catName = server + cat + "/files/catalog.xml";
        final FmrcInventory fmrCollection = makeFromCatalog(null, catName, catName, n, 2);
        final String writeFile = writeDir + StringUtil.replace(cat, "/", "-") + ".fmrcDefinition.xml";
        System.out.println("write definition to " + writeFile);
        final FmrcDefinition def = new FmrcDefinition();
        def.makeFromCollectionInventory(fmrCollection);
        final FileOutputStream fos = new FileOutputStream(writeFile);
        def.writeDefinitionXML(fos);
        fos.close();
    }
    
    public static void writeDefinitionFromCatalog(final String catURL, final String collectionName, final int maxDatasets) throws Exception {
        final FmrcInventory fmrCollection = makeFromCatalog(catURL, collectionName, maxDatasets, 1);
        System.out.println("write definition to " + fmrCollection.getDefinitionPath());
        final FmrcDefinition def = new FmrcDefinition();
        def.makeFromCollectionInventory(fmrCollection);
        final FileOutputStream fos = new FileOutputStream(fmrCollection.getDefinitionPath());
        def.writeDefinitionXML(fos);
    }
    
    public static FmrcInventory makeFromCatalog(final String catURL, final String collectionName, final int maxDatasets, final int mode) throws Exception {
        final DiskCache2 cache = new DiskCache2("fmrcInventory/", true, 0, -1);
        return makeFromCatalog(cache, catURL, collectionName, maxDatasets, mode);
    }
    
    public static FmrcInventory makeFromCatalog(final DiskCache2 cache, final String catURL, final String collectionName, final int maxDatasets, final int mode) throws Exception {
        final String fmrcDefinitionPath = (cache == null) ? null : (cache.getRootDirectory() + "/defs/");
        System.out.println("***makeFromCatalog " + catURL);
        final long startTime = System.currentTimeMillis();
        final FmrcInventory fmrCollection = new FmrcInventory(fmrcDefinitionPath, collectionName);
        final CatalogCrawler crawler = new CatalogCrawler(1, false, new MyListener(fmrCollection, maxDatasets, mode, cache));
        crawler.crawl(catURL, null, System.out, null);
        fmrCollection.finish();
        if (FmrcInventory.debugTiming) {
            final long took = System.currentTimeMillis() - startTime;
            System.out.println("that took = " + took + " msecs");
        }
        return fmrCollection;
    }
    
    public static void main4(final String[] args) throws Exception {
        final String dir = "nam/conus80";
        final FmrcInventory fmrc = makeFromDirectory("C:/temp", "NCEP-NAM-CONUS_80km", null, "C:/data/grib/" + dir, "grib1", 2);
    }
    
    static {
        FmrcInventory.log = LoggerFactory.getLogger(FmrcInventory.class);
        FmrcInventory.dateFormatShort = new SimpleDateFormat("MM-dd HH.mm");
        FmrcInventory.dateFormat = new SimpleDateFormat("yyyy-MM-dd HH.mm'Z'");
        FmrcInventory.debug = false;
        FmrcInventory.dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        FmrcInventory.dateFormatShort.setTimeZone(TimeZone.getTimeZone("GMT"));
        FmrcInventory.debugTiming = false;
    }
    
    private class Inventory
    {
        Date forecastTime;
        Date runTime;
        double hourOffset;
        
        Inventory(final Date runTime, final Date forecastTime, final double hourOffset) {
            this.runTime = runTime;
            this.hourOffset = hourOffset;
            this.forecastTime = forecastTime;
        }
    }
    
    static class Run implements Comparable
    {
        ForecastModelRunInventory.TimeCoord tc;
        List<Inventory> invList;
        Date runTime;
        
        Run(final Date runTime, final ForecastModelRunInventory.TimeCoord tc) {
            this.runTime = runTime;
            this.tc = tc;
            this.invList = new ArrayList<Inventory>();
        }
        
        public int compareTo(final Object o) {
            final Run other = (Run)o;
            return this.runTime.compareTo(other.runTime);
        }
        
        public boolean equalsData(final Run orun) {
            if (this.invList.size() != orun.invList.size()) {
                return false;
            }
            for (int i = 0; i < this.invList.size(); ++i) {
                final Inventory inv = this.invList.get(i);
                final Inventory oinv = orun.invList.get(i);
                if (inv.hourOffset != oinv.hourOffset) {
                    return false;
                }
            }
            return true;
        }
        
        double[] getOffsetHours() {
            if (this.tc != null) {
                return this.tc.getOffsetHours();
            }
            final double[] result = new double[this.invList.size()];
            for (int i = 0; i < this.invList.size(); ++i) {
                final Inventory inv = this.invList.get(i);
                result[i] = inv.hourOffset;
            }
            return result;
        }
    }
    
    class RunSeq
    {
        List<Run> runs;
        List<UberGrid> vars;
        String name;
        
        RunSeq(final List<RunExpected> runs) {
            this.vars = new ArrayList<UberGrid>();
            this.runs = new ArrayList<Run>();
            for (final RunExpected rune : runs) {
                this.runs.add(rune.run);
            }
            this.name = "RunSeq" + FmrcInventory.this.run_seqno;
            FmrcInventory.this.run_seqno++;
        }
        
        boolean equalsData(final List<RunExpected> oruns) {
            if (this.runs.size() != oruns.size()) {
                return false;
            }
            for (int i = 0; i < this.runs.size(); ++i) {
                final Run run = this.runs.get(i);
                final RunExpected orune = oruns.get(i);
                if (!run.runTime.equals(orune.run.runTime)) {
                    return false;
                }
                if (!run.equalsData(orune.run)) {
                    return false;
                }
            }
            return true;
        }
        
        void addVariable(final UberGrid uv) {
            this.vars.add(uv);
        }
        
        List<UberGrid> getVariables() {
            return this.vars;
        }
    }
    
    class UberGrid implements Comparable
    {
        String name;
        List<RunExpected> runs;
        ForecastModelRunInventory.VertCoord vertCoordUnion;
        ForecastModelRunInventory.EnsCoord ensCoordUnion;
        int countInv;
        int countExpected;
        RunSeq seq;
        FmrcDefinition.RunSeq expectedSeq;
        FmrcDefinition.Grid expectedGrid;
        
        UberGrid(final String name) {
            this.runs = new ArrayList<RunExpected>();
            this.vertCoordUnion = null;
            this.ensCoordUnion = null;
            this.name = name;
            if (FmrcInventory.this.definition != null) {
                this.expectedSeq = FmrcInventory.this.definition.findSeqForVariable(name);
                this.expectedGrid = this.expectedSeq.findGrid(name);
            }
        }
        
        String getName() {
            return this.name;
        }
        
        void addRun(final Run run, final ForecastModelRunInventory.Grid grid) {
            final ForecastModelRunInventory.TimeCoord xtc = (this.expectedSeq == null) ? null : this.expectedSeq.findTimeCoordByRuntime(run.runTime);
            final RunExpected rune = new RunExpected(run, xtc, grid, this.expectedGrid);
            this.runs.add(rune);
            if (rune.expected != null) {
                for (final double offset : rune.expected.getOffsetHours()) {
                    final Date fcDate = FmrcInventory.this.addHour(run.runTime, offset);
                    FmrcInventory.this.forecastTimeHash.add(fcDate);
                    FmrcInventory.this.offsetHash.add(offset);
                }
            }
        }
        
        void finish() {
            Collections.sort(this.runs);
            final List<ForecastModelRunInventory.EnsCoord> eextendList = new ArrayList<ForecastModelRunInventory.EnsCoord>();
            ForecastModelRunInventory.EnsCoord ec_union = null;
            for (final RunExpected rune : this.runs) {
                final ForecastModelRunInventory.EnsCoord ec = rune.grid.ec;
                if (ec == null) {
                    continue;
                }
                if (ec_union == null) {
                    ec_union = new ForecastModelRunInventory.EnsCoord(ec);
                }
                else {
                    if (ec_union.equalsData(ec)) {
                        continue;
                    }
                    eextendList.add(ec);
                }
            }
            if (ec_union != null) {
                this.normalize(ec_union, eextendList);
                this.ensCoordUnion = FmrcInventory.this.findEnsCoord(ec_union);
                if (this.ensCoordUnion == null) {
                    FmrcInventory.this.ensCoords.add(ec_union);
                    (this.ensCoordUnion = ec_union).setId(Integer.toString(FmrcInventory.this.ec_seqno));
                    FmrcInventory.this.ec_seqno++;
                }
            }
            final List<ForecastModelRunInventory.VertCoord> extendList = new ArrayList<ForecastModelRunInventory.VertCoord>();
            ForecastModelRunInventory.VertCoord vc_union = null;
            for (final RunExpected rune2 : this.runs) {
                final ForecastModelRunInventory.VertCoord vc = rune2.grid.vc;
                if (vc == null) {
                    continue;
                }
                if (vc_union == null) {
                    vc_union = new ForecastModelRunInventory.VertCoord(vc);
                }
                else {
                    if (vc_union.equalsData(vc)) {
                        continue;
                    }
                    extendList.add(vc);
                }
            }
            if (vc_union != null) {
                this.normalize(vc_union, extendList);
                this.vertCoordUnion = FmrcInventory.this.findVertCoord(vc_union);
                if (this.vertCoordUnion == null) {
                    FmrcInventory.this.vertCoords.add(vc_union);
                    (this.vertCoordUnion = vc_union).setId(Integer.toString(FmrcInventory.this.vc_seqno));
                    FmrcInventory.this.vc_seqno++;
                }
            }
        }
        
        public void normalize(ForecastModelRunInventory.EnsCoord result, final List<ForecastModelRunInventory.EnsCoord> ecList) {
            final List<ForecastModelRunInventory.EnsCoord> extra = new ArrayList<ForecastModelRunInventory.EnsCoord>();
            for (final ForecastModelRunInventory.EnsCoord ec : ecList) {
                if (!result.equalsData(ec)) {
                    extra.add(ec);
                }
            }
            if (extra.size() == 0) {
                return;
            }
            for (final ForecastModelRunInventory.EnsCoord ec : extra) {
                if (ec.getNEnsembles() < result.getNEnsembles()) {
                    continue;
                }
                result = ec;
            }
        }
        
        public void normalize(final ForecastModelRunInventory.VertCoord result, final List<ForecastModelRunInventory.VertCoord> vcList) {
            final Set<LevelCoord> valueSet = new HashSet<LevelCoord>();
            this.addValues(valueSet, result.getValues1(), result.getValues2());
            for (final ForecastModelRunInventory.VertCoord vc : vcList) {
                this.addValues(valueSet, vc.getValues1(), vc.getValues2());
            }
            final List<LevelCoord> valueList = Arrays.asList((LevelCoord[])valueSet.toArray((T[])new LevelCoord[valueSet.size()]));
            Collections.sort(valueList);
            final double[] values1 = new double[valueList.size()];
            final double[] values2 = new double[valueList.size()];
            boolean has_values2 = false;
            for (int i = 0; i < valueList.size(); ++i) {
                final LevelCoord lc = valueList.get(i);
                values1[i] = lc.value1;
                values2[i] = lc.value2;
                if (lc.value2 != 0.0) {
                    has_values2 = true;
                }
            }
            result.setValues1(values1);
            if (has_values2) {
                result.setValues2(values2);
            }
        }
        
        private void addValues(final Set<LevelCoord> valueSet, final double[] values1, final double[] values2) {
            for (int i = 0; i < values1.length; ++i) {
                final double val2 = (values2 == null) ? 0.0 : values2[i];
                valueSet.add(new LevelCoord(values1[i], val2));
            }
        }
        
        public int compareTo(final Object o) {
            final UberGrid uv = (UberGrid)o;
            return this.name.compareTo(uv.name);
        }
        
        RunExpected findRun(final Date runTime) {
            for (final RunExpected rune : this.runs) {
                if (runTime.equals(rune.run.runTime)) {
                    return rune;
                }
            }
            return null;
        }
    }
    
    class LevelCoord implements Comparable
    {
        double mid;
        double value1;
        double value2;
        
        LevelCoord(final double value1, final double value2) {
            this.value1 = value1;
            this.value2 = value2;
            this.mid = ((value2 == 0.0) ? value1 : ((value1 + value2) / 2.0));
        }
        
        public int compareTo(final Object o) {
            final LevelCoord other = (LevelCoord)o;
            if (this.mid < other.mid) {
                return -1;
            }
            if (this.mid > other.mid) {
                return 1;
            }
            return 0;
        }
        
        @Override
        public boolean equals(final Object oo) {
            if (this == oo) {
                return true;
            }
            if (!(oo instanceof LevelCoord)) {
                return false;
            }
            final LevelCoord other = (LevelCoord)oo;
            return Misc.closeEnough(this.value1, other.value1) && Misc.closeEnough(this.value2, other.value2);
        }
        
        @Override
        public int hashCode() {
            return (int)(this.value1 * 100000.0 + this.value2 * 100.0);
        }
    }
    
    class RunExpected implements Comparable
    {
        Run run;
        ForecastModelRunInventory.Grid grid;
        ForecastModelRunInventory.TimeCoord expected;
        FmrcDefinition.Grid expectedGrid;
        
        RunExpected(final Run run, final ForecastModelRunInventory.TimeCoord expected, final ForecastModelRunInventory.Grid grid, final FmrcDefinition.Grid expectedGrid) {
            this.run = run;
            this.expected = expected;
            this.grid = grid;
            this.expectedGrid = expectedGrid;
        }
        
        public int compareTo(final Object o) {
            final RunExpected other = (RunExpected)o;
            return this.run.runTime.compareTo(other.run.runTime);
        }
        
        int countInventory(final double hourOffset) {
            final boolean hasExpected = this.expected != null && this.expected.findIndex(hourOffset) >= 0;
            return hasExpected ? this.grid.countInventory(hourOffset) : 0;
        }
        
        int countExpected(final double hourOffset) {
            if (this.expected != null) {
                final boolean hasExpected = this.expected.findIndex(hourOffset) >= 0;
                return hasExpected ? this.expectedGrid.countVertCoords(hourOffset) : 0;
            }
            return this.grid.countTotal();
        }
    }
    
    private class TimeMatrixDataset
    {
        private int ntimes;
        private int nruns;
        private int noffsets;
        private short[][] countInv;
        private short[][] expected;
        private short[][] countOffsetInv;
        private short[][] expectedOffset;
        private int[] countTotalRunInv;
        private int[] expectedTotalRun;
        
        TimeMatrixDataset() {
            this.nruns = FmrcInventory.this.runTimeList.size();
            this.ntimes = FmrcInventory.this.forecastTimeList.size();
            this.noffsets = FmrcInventory.this.offsetList.size();
            this.countInv = new short[this.ntimes][this.nruns];
            this.expected = new short[this.ntimes][this.nruns];
            this.countOffsetInv = new short[this.nruns][this.noffsets];
            this.expectedOffset = new short[this.nruns][this.noffsets];
            for (final UberGrid uv : FmrcInventory.this.varList) {
                this.addInventory(uv);
            }
            this.countTotalRunInv = new int[this.nruns];
            this.expectedTotalRun = new int[this.nruns];
            for (int i = 0; i < this.nruns; ++i) {
                for (int j = 0; j < this.noffsets; ++j) {
                    final int[] countTotalRunInv = this.countTotalRunInv;
                    final int n = i;
                    countTotalRunInv[n] += this.countOffsetInv[i][j];
                    final int[] expectedTotalRun = this.expectedTotalRun;
                    final int n2 = i;
                    expectedTotalRun[n2] += this.expectedOffset[i][j];
                }
            }
        }
        
        void addInventory(final UberGrid uv) {
            uv.countInv = 0;
            uv.countExpected = 0;
            for (int runIndex = 0; runIndex < FmrcInventory.this.runTimeList.size(); ++runIndex) {
                final Date runTime = FmrcInventory.this.runTimeList.get(runIndex);
                final RunExpected rune = uv.findRun(runTime);
                if (rune != null) {
                    for (int offsetIndex = 0; offsetIndex < FmrcInventory.this.offsetList.size(); ++offsetIndex) {
                        final double hourOffset = FmrcInventory.this.offsetList.get(offsetIndex);
                        final int invCount = rune.countInventory(hourOffset);
                        final int expectedCount = rune.countExpected(hourOffset);
                        final Date forecastTime = FmrcInventory.this.addHour(runTime, hourOffset);
                        final int forecastIndex = this.findForecastIndex(forecastTime);
                        if (forecastIndex < 0) {
                            FmrcInventory.log.debug("No Forecast for runTime=" + FmrcInventory.this.formatter.toDateTimeString(runTime) + " OffsetHour=" + hourOffset + " dataset=" + FmrcInventory.this.name);
                        }
                        else {
                            final short[] array = this.countInv[forecastIndex];
                            final int n = runIndex;
                            array[n] += (short)invCount;
                            final short[] array2 = this.expected[forecastIndex];
                            final int n2 = runIndex;
                            array2[n2] += (short)expectedCount;
                        }
                        final short[] array3 = this.countOffsetInv[runIndex];
                        final int n3 = offsetIndex;
                        array3[n3] += (short)invCount;
                        final short[] array4 = this.expectedOffset[runIndex];
                        final int n4 = offsetIndex;
                        array4[n4] += (short)expectedCount;
                        uv.countInv += invCount;
                        uv.countExpected += expectedCount;
                    }
                }
            }
        }
        
        int findRunIndex(final Date runTime) {
            for (int i = 0; i < FmrcInventory.this.runTimeList.size(); ++i) {
                final Date d = FmrcInventory.this.runTimeList.get(i);
                if (d.equals(runTime)) {
                    return i;
                }
            }
            return -1;
        }
        
        int findForecastIndex(final Date forecastTime) {
            for (int i = 0; i < FmrcInventory.this.forecastTimeList.size(); ++i) {
                final Date d = FmrcInventory.this.forecastTimeList.get(i);
                if (d.equals(forecastTime)) {
                    return i;
                }
            }
            return -1;
        }
        
        int findOffsetIndex(final double offsetHour) {
            for (int i = 0; i < FmrcInventory.this.offsetList.size(); ++i) {
                if (offsetHour == FmrcInventory.this.offsetList.get(i)) {
                    return i;
                }
            }
            return -1;
        }
    }
    
    private static class MyListener implements CatalogCrawler.Listener
    {
        FmrcInventory fmrCollection;
        DiskCache2 cache;
        int maxDatasets;
        int mode;
        int count;
        boolean first;
        
        MyListener(final FmrcInventory fmrCollection, final int maxDatasets, final int mode, final DiskCache2 cache) {
            this.first = true;
            this.fmrCollection = fmrCollection;
            this.maxDatasets = maxDatasets;
            this.mode = mode;
            this.count = 0;
            this.cache = cache;
        }
        
        public void getDataset(final InvDataset dd, final Object context) {
            if (this.count > this.maxDatasets && this.maxDatasets > 0) {
                return;
            }
            final InvAccess access = dd.getAccess(ServiceType.OPENDAP);
            if (access == null) {
                System.out.println(" no opendap access");
                return;
            }
            if (this.first) {
                System.out.println(" skip " + access.getStandardUrlName());
                this.first = false;
                return;
            }
            ++this.count;
            System.out.println(" access " + access.getStandardUrlName());
            try {
                final ForecastModelRunInventory fmr = ForecastModelRunInventory.open(this.cache, access.getStandardUrlName(), this.mode, false);
                if (null != fmr) {
                    this.fmrCollection.addRun(fmr);
                    fmr.releaseDataset();
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        
        public boolean getCatalogRef(final InvCatalogRef dd, final Object context) {
            return true;
        }
    }
}
