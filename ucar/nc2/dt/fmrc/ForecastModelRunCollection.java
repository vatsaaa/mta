// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.fmrc;

import ucar.nc2.dt.GridDataset;
import java.io.IOException;
import ucar.nc2.dataset.NetcdfDataset;
import java.util.Date;
import java.util.List;

public interface ForecastModelRunCollection
{
    List<Date> getRunDates();
    
    NetcdfDataset getRunTimeDataset(final Date p0) throws IOException;
    
    List<Date> getForecastDates();
    
    NetcdfDataset getForecastTimeDataset(final Date p0) throws IOException;
    
    List<Double> getForecastOffsets();
    
    NetcdfDataset getForecastOffsetDataset(final double p0) throws IOException;
    
    NetcdfDataset getBestTimeSeries() throws IOException;
    
    NetcdfDataset getFmrcDataset();
    
    GridDataset getGridDataset();
    
    boolean sync() throws IOException;
    
    void close() throws IOException;
}
