// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.fmrc;

import ucar.ma2.InvalidRangeException;
import ucar.ma2.Range;
import ucar.ma2.Section;
import org.slf4j.LoggerFactory;
import ucar.nc2.NCdumpW;
import java.io.OutputStream;
import java.io.PrintWriter;
import ucar.ma2.ArrayObject;
import ucar.nc2.constants.AxisType;
import ucar.ma2.Array;
import ucar.nc2.Structure;
import ucar.ma2.DataType;
import ucar.ma2.ArrayDouble;
import java.util.Formatter;
import java.util.EnumSet;
import ucar.nc2.dataset.VariableDS;
import ucar.nc2.Dimension;
import ucar.nc2.Variable;
import ucar.nc2.Group;
import ucar.nc2.Attribute;
import ucar.nc2.units.DateFormatter;
import ucar.nc2.dataset.CoordinateAxis1DTime;
import ucar.nc2.dt.GridCoordSystem;
import java.util.Iterator;
import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Arrays;
import ucar.nc2.dt.GridDatatype;
import ucar.nc2.dataset.CoordinateAxis;
import java.util.HashSet;
import java.util.HashMap;
import java.io.IOException;
import ucar.nc2.util.CancelTask;
import java.util.Set;
import java.util.Map;
import java.util.List;
import java.util.Date;
import ucar.nc2.dt.GridDataset;
import ucar.nc2.dataset.NetcdfDataset;
import org.slf4j.Logger;

public class FmrcImpl implements ForecastModelRunCollection
{
    private static Logger logger;
    private static final String BEST = "best";
    private static final String RUN = "run";
    private static final String FORECAST = "forecast";
    private static final String OFFSET = "offset";
    private NetcdfDataset ncd_2dtime;
    private GridDataset gds;
    private Date baseDate;
    private String runtimeDimName;
    private List<Gridset> gridsets;
    private Map<String, Gridset> gridHash;
    private Set<String> coordSet;
    private List<Date> runtimes;
    private List<Date> forecasts;
    private List<Double> offsets;
    
    public FmrcImpl(final String filename) throws IOException {
        this(NetcdfDataset.acquireDataset(filename, null));
    }
    
    public FmrcImpl(final NetcdfDataset ncd) throws IOException {
        this.init(ncd);
    }
    
    public boolean sync() throws IOException {
        final boolean changed = this.ncd_2dtime.sync();
        if (changed) {
            if (FmrcImpl.logger.isDebugEnabled()) {
                FmrcImpl.logger.debug("ncd_2dtime changed, reinit Fmrc " + this.ncd_2dtime.getLocation());
            }
            this.init(this.ncd_2dtime);
        }
        return changed;
    }
    
    public GridDataset getGridDataset() {
        return this.gds;
    }
    
    public void close() throws IOException {
        this.gds.close();
    }
    
    private void init(final NetcdfDataset ncd) throws IOException {
        this.ncd_2dtime = ncd;
        this.gridHash = new HashMap<String, Gridset>();
        this.coordSet = new HashSet<String>();
        this.runtimes = null;
        this.gds = new ucar.nc2.dt.grid.GridDataset(ncd);
        final List<GridDatatype> grids = this.gds.getGrids();
        if (grids.size() == 0) {
            throw new IllegalArgumentException("no grids");
        }
        final HashMap<CoordinateAxis, Gridset> timeAxisHash = new HashMap<CoordinateAxis, Gridset>();
        for (final GridDatatype grid : grids) {
            final GridCoordSystem gcs = grid.getCoordinateSystem();
            final CoordinateAxis timeAxis = gcs.getTimeAxis();
            if (timeAxis != null) {
                Gridset gset = timeAxisHash.get(timeAxis);
                if (gset == null) {
                    gset = new Gridset(timeAxis, gcs);
                    timeAxisHash.put(timeAxis, gset);
                    this.coordSet.add(timeAxis.getName());
                }
                gset.gridList.add(grid);
                this.gridHash.put(grid.getName(), gset);
            }
            if (this.runtimes == null && gcs.getRunTimeAxis() != null) {
                final CoordinateAxis1DTime runtimeCoord = gcs.getRunTimeAxis();
                final Date[] runDates = runtimeCoord.getTimeDates();
                this.baseDate = runDates[0];
                this.runtimes = Arrays.asList(runDates);
                this.runtimeDimName = runtimeCoord.getDimension(0).getName();
                this.coordSet.add(runtimeCoord.getName());
            }
        }
        if (this.runtimes == null) {
            throw new IllegalArgumentException("no runtime dimension");
        }
        final HashSet<Date> forecastSet = new HashSet<Date>();
        final HashSet<Double> offsetSet = new HashSet<Double>();
        this.gridsets = new ArrayList<Gridset>(timeAxisHash.values());
        for (final Gridset gridset : this.gridsets) {
            for (int run = 0; run < this.runtimes.size(); ++run) {
                final Date runDate = this.runtimes.get(run);
                final CoordinateAxis1DTime timeCoordRun = gridset.gcs.getTimeAxisForRun(run);
                final Date[] arr$;
                final Date[] forecastDates = arr$ = timeCoordRun.getTimeDates();
                for (final Date forecastDate : arr$) {
                    forecastSet.add(forecastDate);
                    final double hourOffset = this.getOffsetHour(runDate, forecastDate);
                    offsetSet.add(hourOffset);
                }
            }
        }
        Collections.sort(this.forecasts = Arrays.asList((Date[])forecastSet.toArray((T[])new Date[forecastSet.size()])));
        Collections.sort(this.offsets = Arrays.asList((Double[])offsetSet.toArray((T[])new Double[offsetSet.size()])));
        for (final Gridset gridset : this.gridsets) {
            gridset.generateInventory();
        }
    }
    
    private double getOffsetHour(final Date run, final Date forecast) {
        final double diff = (double)(forecast.getTime() - run.getTime());
        return diff / 1000.0 / 60.0 / 60.0;
    }
    
    public List<Date> getRunDates() {
        return this.runtimes;
    }
    
    public NetcdfDataset getRunTimeDataset(final Date wantRuntime) throws IOException {
        if (wantRuntime == null) {
            return null;
        }
        if (!this.runtimes.contains(wantRuntime)) {
            return null;
        }
        final DateFormatter df = new DateFormatter();
        final String runTimeString = df.toDateTimeStringISO(wantRuntime);
        final NetcdfDataset ncd = this.createDataset(new RuntimeInvGetter(wantRuntime), "run", runTimeString);
        ncd.addAttribute(null, new Attribute("_CoordinateModelRunDate", runTimeString));
        ncd.finish();
        return ncd;
    }
    
    public List<Date> getForecastDates() {
        return this.forecasts;
    }
    
    public NetcdfDataset getForecastTimeDataset(final Date forecastTime) throws IOException {
        if (forecastTime == null) {
            return null;
        }
        if (!this.forecasts.contains(forecastTime)) {
            return null;
        }
        final DateFormatter df = new DateFormatter();
        final String name = df.toDateTimeStringISO(forecastTime);
        return this.createDataset(new ForecastInvGetter(forecastTime), "forecast", name);
    }
    
    public List<Double> getForecastOffsets() {
        return this.offsets;
    }
    
    public NetcdfDataset getForecastOffsetDataset(final double hours) throws IOException {
        if (!this.offsets.contains(new Double(hours))) {
            return null;
        }
        return this.createDataset(new OffsetInvGetter(hours), "offset", Double.toString(hours));
    }
    
    public NetcdfDataset getBestTimeSeries() throws IOException {
        return this.createDataset(new InventoryGetter() {
            public List<Inventory> get(final Gridset gridset) {
                return gridset.bestList;
            }
        }, "best", null);
    }
    
    public NetcdfDataset getFmrcDataset() {
        return this.ncd_2dtime;
    }
    
    private String makeLocation(final String type, final String name) {
        if (name != null) {
            return this.ncd_2dtime.getLocation() + "/" + type + "-" + name + ".ncd";
        }
        return this.ncd_2dtime.getLocation() + "/" + type + ".ncd";
    }
    
    private NetcdfDataset createDataset(final InventoryGetter invGetter, final String type, final String name) throws IOException {
        final NetcdfDataset newds = new NetcdfDataset();
        newds.setLocation(this.makeLocation(type, name));
        final Group src = this.ncd_2dtime.getRootGroup();
        final Group target = newds.getRootGroup();
        for (final Attribute a : src.getAttributes()) {
            target.addAttribute(a);
        }
        final String oldHistory = this.ncd_2dtime.findAttValueIgnoreCase(null, "history", null);
        final String newHistory = "Synthetic dataset from TDS fmrc (" + type + ") aggregation, original data from " + this.ncd_2dtime.getLocation();
        final String history = (oldHistory != null) ? (oldHistory + "; " + newHistory) : newHistory;
        target.addAttribute(new Attribute("history", history));
        final DateFormatter df = new DateFormatter();
        target.addAttribute(new Attribute("_CoordinateModelBaseDate", df.toDateTimeStringISO(this.baseDate)));
        for (final Dimension d : src.getDimensions()) {
            target.addDimension(new Dimension(d.getName(), d));
        }
        for (final Gridset gridset : this.gridsets) {
            final List<Inventory> invList = invGetter.get(gridset);
            if (invList == null) {
                continue;
            }
            this.addTime3Coordinates(newds, gridset, invList, type);
            for (final GridDatatype grid : gridset.gridList) {
                final Variable orgVar = this.ncd_2dtime.findVariable(grid.getNameEscaped());
                final VariableDS v = new VariableDS(target, orgVar, false);
                v.clearCoordinateSystems();
                v.setDimensions(gridset.makeDimensions(v.getDimensions()));
                v.remove(v.findAttribute("_CoordinateAxes"));
                v.remove(v.findAttribute("coordinates"));
                v.remove(v.findAttribute("_CoordinateAxes"));
                final String coords = this.makeCoordinatesAttribute(grid.getCoordinateSystem(), gridset.timeDimName);
                v.addAttribute(new Attribute("coordinates", coords));
                target.addVariable(v);
            }
        }
        for (final Variable v2 : src.getVariables()) {
            if (null == this.gridHash.get(v2.getName()) && !this.coordSet.contains(v2.getName())) {
                final VariableDS vds = new VariableDS(newds.getRootGroup(), v2, false);
                vds.clearCoordinateSystems();
                vds.remove(vds.findAttribute("coordinates"));
                target.addVariable(vds);
            }
        }
        newds.finish();
        newds.enhance(EnumSet.of(NetcdfDataset.Enhance.CoordSystems));
        return newds;
    }
    
    private String makeCoordinatesAttribute(final GridCoordSystem gcs, final String timeDimName) {
        final Formatter sb = new Formatter();
        if (gcs.getXHorizAxis() != null) {
            sb.format("%s ", gcs.getXHorizAxis().getName());
        }
        if (gcs.getYHorizAxis() != null) {
            sb.format("%s ", gcs.getYHorizAxis().getName());
        }
        if (gcs.getVerticalAxis() != null) {
            sb.format("%s ", gcs.getVerticalAxis().getName());
        }
        sb.format("%s ", timeDimName);
        return sb.toString();
    }
    
    private void addTime3Coordinates(final NetcdfDataset newds, final Gridset gridset, final List<Inventory> invList, final String type) {
        final DateFormatter formatter = new DateFormatter();
        final boolean useRun = type.equals("forecast");
        final int n = invList.size();
        final String dimName = gridset.timeDimName;
        final Group g = newds.getRootGroup();
        g.remove(g.findDimension(dimName));
        g.addDimension(new Dimension(dimName, n));
        ArrayDouble.D1 offsetData = new ArrayDouble.D1(n);
        for (int i = 0; i < n; ++i) {
            final Inventory inv = invList.get(i);
            final double offsetHour = this.getOffsetHour(this.baseDate, useRun ? inv.runTime : inv.forecastTime);
            offsetData.set(i, offsetHour);
        }
        final String typeName = useRun ? "run" : "forecast";
        String desc = typeName + " time coordinate";
        final VariableDS timeCoordinate = new VariableDS(newds, g, null, dimName, DataType.DOUBLE, dimName, "hours since " + formatter.toDateTimeStringISO(this.baseDate), desc);
        timeCoordinate.setCachedData(offsetData, true);
        timeCoordinate.addAttribute(new Attribute("long_name", desc));
        timeCoordinate.addAttribute(new Attribute("standard_name", useRun ? "forecast_reference_time" : "time"));
        timeCoordinate.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Time.toString()));
        newds.addVariable(g, timeCoordinate);
        final ArrayObject.D1 runData = new ArrayObject.D1(String.class, n);
        for (int j = 0; j < n; ++j) {
            final Inventory inv2 = invList.get(j);
            runData.set(j, formatter.toDateTimeStringISO(inv2.runTime));
        }
        desc = "model run dates for coordinate = " + dimName;
        final VariableDS runtimeCoordinate = new VariableDS(newds, newds.getRootGroup(), null, dimName + "_run", DataType.STRING, dimName, null, desc);
        runtimeCoordinate.setCachedData(runData, true);
        runtimeCoordinate.addAttribute(new Attribute("long_name", desc));
        runtimeCoordinate.addAttribute(new Attribute("standard_name", "forecast_reference_time"));
        runtimeCoordinate.addAttribute(new Attribute("_CoordinateAxisType", AxisType.RunTime.toString()));
        newds.addVariable(newds.getRootGroup(), runtimeCoordinate);
        offsetData = new ArrayDouble.D1(n);
        for (int k = 0; k < n; ++k) {
            final Inventory inv3 = invList.get(k);
            offsetData.set(k, inv3.hourOffset);
        }
        desc = "hour offset from start of run for coordinate = " + dimName;
        final VariableDS offsetCoordinate = new VariableDS(newds, newds.getRootGroup(), null, dimName + "_offset", DataType.DOUBLE, dimName, null, desc);
        offsetCoordinate.setCachedData(offsetData, true);
        offsetCoordinate.addAttribute(new Attribute("long_name", desc));
        offsetCoordinate.addAttribute(new Attribute("units", "hour"));
        offsetCoordinate.addAttribute(new Attribute("standard_name", "forecast_period"));
        newds.addVariable(newds.getRootGroup(), offsetCoordinate);
    }
    
    public void dump(final Formatter f) throws IOException {
        for (final Gridset gridset : this.gridsets) {
            f.format("===========================%n", new Object[0]);
            gridset.dump(f);
        }
    }
    
    static void test(final String location, final String timeVarName) throws IOException {
        final FmrcImpl fmrc = new FmrcImpl(location);
        System.out.println("Fmrc for dataset= " + location);
        final NetcdfDataset fmrcd = fmrc.getFmrcDataset();
        final Variable time = fmrcd.findVariable(timeVarName);
        final Array data = time.read();
        NCdumpW.printArray(data, "2D time", new PrintWriter(System.out), null);
        fmrc.dump(new Formatter(System.out));
    }
    
    static void testSync(final String location, final String timeVarName) throws IOException, InterruptedException {
        final FmrcImpl fmrc = new FmrcImpl(location);
        System.out.println("Fmrc for dataset= " + location);
        final NetcdfDataset fmrcd = fmrc.getFmrcDataset();
        final Variable time = fmrcd.findVariable(timeVarName);
        Array data = time.read();
        NCdumpW.printArray(data, "2D time", new PrintWriter(System.out), null);
        fmrc.dump(new Formatter(System.out));
        final boolean changed = fmrc.sync();
        if (changed) {
            System.out.println("========== Sync =================");
            data = time.read();
            NCdumpW.printArray(data, "2D time", new PrintWriter(System.out), null);
            fmrc.dump(new Formatter(System.out));
        }
    }
    
    public static void main(final String[] args) throws Exception {
        test("D:/test/signell/test.ncml", "ocean_time");
    }
    
    static {
        FmrcImpl.logger = LoggerFactory.getLogger(FmrcImpl.class);
    }
    
    private class Gridset
    {
        List<GridDatatype> gridList;
        GridCoordSystem gcs;
        CoordinateAxis timeAxis;
        String timeDimName;
        HashMap<Date, List<Inventory>> runMap;
        HashMap<Date, List<Inventory>> timeMap;
        HashMap<Double, List<Inventory>> offsetMap;
        List<Inventory> bestList;
        
        Gridset(final CoordinateAxis timeAxis, final GridCoordSystem gcs) {
            this.gridList = new ArrayList<GridDatatype>();
            this.runMap = new HashMap<Date, List<Inventory>>();
            this.timeMap = new HashMap<Date, List<Inventory>>();
            this.offsetMap = new HashMap<Double, List<Inventory>>();
            this.bestList = new ArrayList<Inventory>();
            this.gcs = gcs;
            this.timeAxis = timeAxis;
            this.timeDimName = timeAxis.getDimension(1).getName();
        }
        
        String makeDimensions(final List<Dimension> dims) {
            final StringBuilder sbuff = new StringBuilder();
            sbuff.append(this.timeDimName);
            for (final Dimension d : dims) {
                if (!d.getName().equals(FmrcImpl.this.runtimeDimName)) {
                    if (d.getName().equals(this.timeDimName)) {
                        continue;
                    }
                    sbuff.append(" ").append(d.getName());
                }
            }
            return sbuff.toString();
        }
        
        void generateInventory() {
            final HashMap<Date, Inventory> bestMap = new HashMap<Date, Inventory>();
            for (int nruns = FmrcImpl.this.runtimes.size(), run = 0; run < nruns; ++run) {
                final Date runDate = FmrcImpl.this.runtimes.get(run);
                final List<Inventory> runList = new ArrayList<Inventory>();
                this.runMap.put(runDate, runList);
                final CoordinateAxis1DTime timeCoordRun = this.gcs.getTimeAxisForRun(run);
                final Date[] forecastDates = timeCoordRun.getTimeDates();
                for (int time = 0; time < forecastDates.length; ++time) {
                    final Date forecastDate = forecastDates[time];
                    final double hourOffset = FmrcImpl.this.getOffsetHour(runDate, forecastDate);
                    final Inventory inv = new Inventory(runDate, forecastDate, hourOffset, run, time);
                    runList.add(inv);
                    bestMap.put(forecastDate, inv);
                    List<Inventory> offsetList = this.offsetMap.get(hourOffset);
                    if (offsetList == null) {
                        offsetList = new ArrayList<Inventory>();
                        this.offsetMap.put(hourOffset, offsetList);
                    }
                    offsetList.add(inv);
                    List<Inventory> timeList = this.timeMap.get(forecastDate);
                    if (timeList == null) {
                        timeList = new ArrayList<Inventory>();
                        this.timeMap.put(forecastDate, timeList);
                    }
                    timeList.add(inv);
                }
            }
            Collections.sort(this.bestList = new ArrayList<Inventory>(bestMap.values()));
        }
        
        void dump(final Formatter f) throws IOException {
            final DateFormatter df = new DateFormatter();
            f.format("Gridset timeDimName= %s%n grids= %n", this.timeDimName);
            for (final GridDatatype grid : this.gridList) {
                f.format("  %s%n", grid.getName());
            }
            f.format("%nRun Dates= %s%n", FmrcImpl.this.runtimes.size());
            for (final Date date : FmrcImpl.this.runtimes) {
                f.format(" %s (", df.toDateTimeString(date));
                final List<Inventory> list = this.runMap.get(date);
                if (list == null) {
                    f.format(" none", new Object[0]);
                }
                else {
                    for (final Inventory inv : list) {
                        f.format(" %s", inv.hourOffset);
                    }
                }
                f.format(") %n", new Object[0]);
            }
            f.format("%nForecast Dates= %d %n", FmrcImpl.this.forecasts.size());
            for (final Date date : FmrcImpl.this.forecasts) {
                f.format(" %s(", df.toDateTimeString(date));
                final List<Inventory> list = this.timeMap.get(date);
                if (list == null) {
                    f.format(" none", new Object[0]);
                }
                else {
                    for (final Inventory inv : list) {
                        f.format(" %d/%f", inv.run, inv.hourOffset);
                    }
                }
                f.format(")%n", new Object[0]);
            }
            f.format("\nForecast Hours= %d%n", FmrcImpl.this.offsets.size());
            for (final Double hour : FmrcImpl.this.offsets) {
                final List<Inventory> offsetList = this.offsetMap.get(hour);
                f.format(" %s: (", hour);
                if (offsetList == null) {
                    f.format(" none", new Object[0]);
                }
                else {
                    for (int j = 0; j < offsetList.size(); ++j) {
                        final Inventory inv = offsetList.get(j);
                        if (j > 0) {
                            System.out.print(", ");
                        }
                        f.format("%d/%s", inv.run, df.toDateTimeStringISO(inv.runTime));
                    }
                }
                f.format(")%n", new Object[0]);
            }
            f.format("\nBest Forecast = %d%n", this.bestList.size());
            for (final Inventory inv2 : this.bestList) {
                f.format(" %s (run=%s) offset=%f%n", df.toDateTimeStringISO(inv2.forecastTime), df.toDateTimeStringISO(inv2.runTime), inv2.hourOffset);
            }
        }
    }
    
    private class Inventory implements Comparable
    {
        Date forecastTime;
        Date runTime;
        double hourOffset;
        int run;
        int time;
        
        Inventory(final Date runTime, final Date forecastTime, final double hourOffset, final int run, final int time) {
            this.runTime = runTime;
            this.hourOffset = hourOffset;
            this.forecastTime = forecastTime;
            this.run = run;
            this.time = time;
        }
        
        public int compareTo(final Object o) {
            final Inventory other = (Inventory)o;
            return this.forecastTime.compareTo(other.forecastTime);
        }
    }
    
    private class RuntimeInvGetter implements InventoryGetter
    {
        Date wantRuntime;
        
        RuntimeInvGetter(final Date wantRuntime) {
            this.wantRuntime = wantRuntime;
        }
        
        public List<Inventory> get(final Gridset gridset) {
            return gridset.runMap.get(this.wantRuntime);
        }
    }
    
    private class ForecastInvGetter implements InventoryGetter
    {
        Date forecastTime;
        
        ForecastInvGetter(final Date forecastTime) {
            this.forecastTime = forecastTime;
        }
        
        public List<Inventory> get(final Gridset gridset) {
            return gridset.timeMap.get(this.forecastTime);
        }
    }
    
    private class OffsetInvGetter implements InventoryGetter
    {
        Double hours;
        
        OffsetInvGetter(final double hours) {
            this.hours = hours;
        }
        
        public List<Inventory> get(final Gridset gridset) {
            return gridset.offsetMap.get(this.hours);
        }
    }
    
    private class Subsetter
    {
        List<Inventory> invList;
        Variable mainv;
        
        Subsetter(final List<Inventory> invList, final Variable mainv) {
            this.invList = invList;
            this.mainv = mainv;
        }
        
        public Array reallyRead(final CancelTask cancelTask) throws IOException {
            final Variable orgVar = FmrcImpl.this.ncd_2dtime.findVariable(this.mainv.getNameEscaped());
            final int[] orgVarShape = orgVar.getShape();
            final int rank = orgVar.getRank() - 1;
            final int[] varShape = new int[rank];
            varShape[0] = this.invList.size();
            System.arraycopy(orgVarShape, 2, varShape, 1, rank - 1);
            final Array allData = Array.factory(this.mainv.getDataType(), varShape);
            int destPos = 0;
            final Section section = new Section(orgVar.getRanges());
            for (final Inventory inv : this.invList) {
                Array varData;
                try {
                    section.setRange(0, new Range(inv.run, inv.run));
                    section.setRange(1, new Range(inv.time, inv.time));
                    varData = orgVar.read(section);
                }
                catch (InvalidRangeException e) {
                    FmrcImpl.logger.error("read failed", e);
                    throw new IllegalStateException(e.getMessage());
                }
                Array.arraycopy(varData, 0, allData, destPos, (int)varData.getSize());
                destPos += (int)varData.getSize();
                if (cancelTask != null && cancelTask.isCancel()) {
                    return null;
                }
            }
            return allData;
        }
        
        public Array reallyRead(final Section section, final CancelTask cancelTask) throws IOException, InvalidRangeException {
            final long size = section.computeSize();
            if (size == this.mainv.getSize()) {
                return this.reallyRead(cancelTask);
            }
            final Variable orgVar = FmrcImpl.this.ncd_2dtime.findVariable(this.mainv.getNameEscaped());
            final Array sectionData = Array.factory(this.mainv.getDataType(), section.getShape());
            int destPos = 0;
            final Range timeRange = section.getRange(0);
            final List<Range> allSection = new ArrayList<Range>(section.getRanges());
            allSection.add(0, null);
            final Range.Iterator iter = timeRange.getIterator();
            while (iter.hasNext()) {
                final int index = iter.next();
                final Inventory inv = this.invList.get(index);
                Array varData;
                try {
                    allSection.set(0, new Range(inv.run, inv.run));
                    allSection.set(1, new Range(inv.time, inv.time));
                    varData = orgVar.read(allSection);
                }
                catch (InvalidRangeException e) {
                    FmrcImpl.logger.error("readSection failed", e);
                    throw new IllegalStateException("read failed", e);
                }
                Array.arraycopy(varData, 0, sectionData, destPos, (int)varData.getSize());
                destPos += (int)varData.getSize();
                if (cancelTask != null && cancelTask.isCancel()) {
                    return null;
                }
            }
            return sectionData;
        }
    }
    
    private interface InventoryGetter
    {
        List<Inventory> get(final Gridset p0);
    }
}
