// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.fmrc;

import ucar.nc2.util.DiskCache2;
import ucar.nc2.util.Misc;
import java.util.Iterator;
import java.util.List;
import ucar.nc2.units.DateFormatter;
import java.util.Collections;
import java.util.Date;
import java.util.ArrayList;
import java.io.PrintStream;

public class FmrcReport
{
    private boolean debug;
    
    public FmrcReport() {
        this.debug = false;
    }
    
    public void report(final FmrcInventory fmrc, final PrintStream out, final boolean showMissing) {
        out.println("ForecastModelRunCollection " + fmrc.getDefinitionPath());
        final FmrcDefinition def = fmrc.getDefinition();
        if (null == def) {
            out.println(" No Definition");
            return;
        }
        final StringBuilder sbuff = new StringBuilder();
        final List<ErrMessage> errs = new ArrayList<ErrMessage>();
        for (final FmrcInventory.RunSeq haveSeq : fmrc.getRunSequences()) {
            final List<FmrcInventory.UberGrid> vars = haveSeq.getVariables();
            for (final FmrcInventory.UberGrid uv : vars) {
                final String sname = uv.getName();
                final FmrcDefinition.Grid g = def.findGridByName(sname);
                if (g == null) {
                    errs.add(new ErrMessage(new Date(0L), uv.name, "Extra Variable (not in definition)", ""));
                }
                else {
                    if (this.debug) {
                        System.out.println(uv.name);
                    }
                    for (final FmrcInventory.RunExpected rune : uv.runs) {
                        final ForecastModelRunInventory.TimeCoord haveTc = rune.run.tc;
                        final ForecastModelRunInventory.TimeCoord wantTc = rune.expected;
                        if (this.debug) {
                            System.out.println(" " + rune.run.runTime);
                        }
                        if (rune.expected == null) {
                            errs.add(new ErrMessage(rune.run.runTime, uv.name, "Extra Variable (not in definition)", ""));
                        }
                        else {
                            sbuff.setLength(0);
                            if (showMissing && this.findMissing(haveTc.getOffsetHours(), wantTc.getOffsetHours(), sbuff)) {
                                errs.add(new ErrMessage(rune.run.runTime, uv.name, "Missing All Grids at Offset hour:", sbuff.toString()));
                            }
                            sbuff.setLength(0);
                            if (this.findMissing(wantTc.getOffsetHours(), haveTc.getOffsetHours(), sbuff)) {
                                errs.add(new ErrMessage(rune.run.runTime, uv.name, "Extra Grid at Offset hour:", sbuff.toString()));
                            }
                            if (showMissing) {
                                sbuff.setLength(0);
                                boolean haveErrs = false;
                                for (final double offset : haveTc.getOffsetHours()) {
                                    final double[] wantVc = this.normalize(rune.expectedGrid.getVertCoords(offset));
                                    final double[] haveVc = this.normalize(rune.grid.getVertCoords(offset));
                                    if (this.findMissing(haveVc, wantVc, sbuff)) {
                                        haveErrs = true;
                                        sbuff.append("(").append(offset).append(") ");
                                    }
                                }
                                if (haveErrs) {
                                    errs.add(new ErrMessage(rune.run.runTime, uv.name, "Missing Some Grids:", sbuff.toString()));
                                }
                            }
                            sbuff.setLength(0);
                            boolean haveErrs = false;
                            for (final double offset : haveTc.getOffsetHours()) {
                                if (this.debug) {
                                    System.out.println("  " + offset);
                                }
                                final double[] wantVc = this.normalize(rune.expectedGrid.getVertCoords(offset));
                                final double[] haveVc = this.normalize(rune.grid.getVertCoords(offset));
                                if (this.findMissing(wantVc, haveVc, sbuff)) {
                                    haveErrs = true;
                                    sbuff.append("(").append(offset).append(") ");
                                }
                            }
                            if (!haveErrs) {
                                continue;
                            }
                            errs.add(new ErrMessage(rune.run.runTime, uv.name, "Extra Grids:", sbuff.toString()));
                        }
                    }
                }
            }
        }
        Collections.sort(errs);
        Date currentDate = null;
        String currentType = null;
        final DateFormatter formatter = new DateFormatter();
        for (final ErrMessage err : errs) {
            if (err.runDate != currentDate) {
                if (currentDate != null) {
                    out.println();
                }
                out.println(" Run " + formatter.toDateTimeString(err.runDate));
                currentDate = err.runDate;
                currentType = null;
            }
            if (!err.type.equals(currentType)) {
                out.println("  " + err.type);
                currentType = err.type;
            }
            out.println("   " + err.varName + ": " + err.message);
        }
        out.println();
    }
    
    double[] normalize(double[] v) {
        int countNans = 0;
        for (int i = 0; i < v.length; ++i) {
            if (Double.isNaN(v[i])) {
                ++countNans;
            }
        }
        if (countNans > 0) {
            final double[] vnew = new double[v.length - countNans];
            int count = 0;
            for (int j = 0; j < v.length; ++j) {
                if (!Double.isNaN(v[j])) {
                    vnew[count++] = v[j];
                }
            }
            v = vnew;
        }
        if (v.length < 2) {
            return v;
        }
        if (v[0] < v[1]) {
            return v;
        }
        final double[] v2 = new double[v.length];
        for (int k = 0; k < v.length; ++k) {
            v2[v.length - k - 1] = v[k];
        }
        return v2;
    }
    
    boolean findMissing(final double[] test, final double[] standard, final StringBuilder sbuff) {
        int countTest = 0;
        int countStandard = 0;
        boolean errs = false;
        while (countTest < test.length && countStandard < standard.length) {
            if (Double.isNaN(standard[countStandard])) {
                ++countStandard;
            }
            else if (Double.isNaN(test[countTest])) {
                ++countTest;
            }
            else if (Misc.closeEnough(standard[countStandard], test[countTest])) {
                ++countTest;
                ++countStandard;
            }
            else if (standard[countStandard] < test[countTest]) {
                sbuff.append(" ").append(standard[countStandard]);
                errs = true;
                ++countStandard;
            }
            else {
                if (standard[countStandard] <= test[countTest]) {
                    continue;
                }
                ++countTest;
            }
        }
        while (countStandard < standard.length) {
            sbuff.append(" ").append(standard[countStandard]);
            errs = true;
            ++countStandard;
        }
        return errs;
    }
    
    static void doit(final String dir) throws Exception {
        final FmrcInventory fmrc = FmrcInventory.makeFromDirectory("/local/robb/data/NAM_CONUS_12km/", "test", null, "/local/robb/data/NAM_CONUS_12km", "grib2", 1);
        final FmrcReport report = new FmrcReport();
        report.report(fmrc, System.out, true);
    }
    
    public static void main(final String[] args) throws Exception {
        doit("ruc/c20p");
    }
    
    private class ErrMessage implements Comparable
    {
        Date runDate;
        String varName;
        String type;
        String message;
        
        ErrMessage(final Date runDate, final String varName, final String type, final String message) {
            this.runDate = runDate;
            this.varName = varName;
            this.type = type;
            this.message = message;
        }
        
        public int compareTo(final Object o) {
            final ErrMessage om = (ErrMessage)o;
            int result = this.runDate.compareTo(om.runDate);
            if (result != 0) {
                return result;
            }
            result = this.type.compareTo(om.type);
            if (result != 0) {
                return result;
            }
            result = this.varName.compareTo(om.varName);
            return result;
        }
    }
}
