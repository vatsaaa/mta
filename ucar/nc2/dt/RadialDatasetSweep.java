// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt;

import java.util.Date;
import java.io.IOException;
import ucar.nc2.Variable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Collection;
import ucar.nc2.VariableSimpleIF;
import java.util.List;
import ucar.nc2.units.DateUnit;
import ucar.unidata.geoloc.EarthLocation;
import ucar.nc2.ft.FeatureDataset;

public interface RadialDatasetSweep extends TypedDataset, FeatureDataset
{
    public static final String LevelII = "Level II";
    public static final String UF = "Universal Format";
    
    String getRadarID();
    
    String getRadarName();
    
    String getDataFormat();
    
    Type getCommonType();
    
    EarthLocation getCommonOrigin();
    
    DateUnit getTimeUnits();
    
    boolean isStationary();
    
    void clearDatasetMemory();
    
    boolean isVolume();
    
    List<VariableSimpleIF> getDataVariables();
    
    public static final class Type
    {
        public static final Type NONE;
        private static List<Type> members;
        private String name;
        
        private Type(final String s) {
            this.name = s;
            Type.members.add(this);
        }
        
        public static Collection getAllTypes() {
            return Type.members;
        }
        
        public static Type getType(final String name) {
            if (name == null) {
                return null;
            }
            for (final Type m : Type.members) {
                if (m.name.equalsIgnoreCase(name)) {
                    return m;
                }
            }
            return null;
        }
        
        @Override
        public String toString() {
            return this.name;
        }
        
        static {
            NONE = new Type("");
            Type.members = new ArrayList<Type>(20);
        }
    }
    
    public interface Sweep
    {
        Type getType();
        
        Variable getsweepVar();
        
        int getRadialNumber();
        
        int getGateNumber();
        
        float getBeamWidth();
        
        float getNyquistFrequency();
        
        float getRangeToFirstGate();
        
        float getGateSize();
        
        float[] readData() throws IOException;
        
        float[] readData(final int p0) throws IOException;
        
        float getElevation(final int p0) throws IOException;
        
        float[] getElevation() throws IOException;
        
        float getMeanElevation();
        
        float getAzimuth(final int p0) throws IOException;
        
        float[] getAzimuth() throws IOException;
        
        float getMeanAzimuth();
        
        EarthLocation getOrigin(final int p0);
        
        float getTime(final int p0) throws IOException;
        
        Date getStartingTime();
        
        Date getEndingTime();
        
        int getSweepIndex();
        
        void clearSweepMemory();
    }
    
    public interface RadialVariable extends VariableSimpleIF
    {
        int getNumSweeps();
        
        Sweep getSweep(final int p0);
        
        float[] readAllData() throws IOException;
        
        void clearVariableMemory();
    }
}
