// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.grid;

import ucar.nc2.dataset.CoordinateAxis1D;
import java.io.IOException;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.ma2.Array;
import ucar.nc2.dt.GridCoordSystem;
import java.util.Iterator;
import java.util.Collections;
import java.util.Arrays;
import ucar.nc2.dataset.CoordinateAxis1DTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Date;
import ucar.nc2.dt.GridDatatype;
import java.util.List;

public class GridAsPointDataset
{
    private List<GridDatatype> grids;
    private List<Date> dates;
    
    public GridAsPointDataset(final List<GridDatatype> grids) {
        this.grids = grids;
        final HashSet<Date> dateHash = new HashSet<Date>();
        final List<CoordinateAxis1DTime> timeAxes = new ArrayList<CoordinateAxis1DTime>();
        for (final GridDatatype grid : grids) {
            final GridCoordSystem gcs = grid.getCoordinateSystem();
            final CoordinateAxis1DTime timeAxis = gcs.getTimeAxis1D();
            if (timeAxis != null && !timeAxes.contains(timeAxis)) {
                timeAxes.add(timeAxis);
                final Date[] arr$;
                final Date[] timeDates = arr$ = timeAxis.getTimeDates();
                for (final Date timeDate : arr$) {
                    dateHash.add(timeDate);
                }
            }
        }
        Collections.sort(this.dates = Arrays.asList((Date[])dateHash.toArray((T[])new Date[dateHash.size()])));
    }
    
    public List<Date> getDates() {
        return this.dates;
    }
    
    public boolean hasTime(final GridDatatype grid, final Date date) {
        final GridCoordSystem gcs = grid.getCoordinateSystem();
        final CoordinateAxis1DTime timeAxis = gcs.getTimeAxis1D();
        return timeAxis != null && timeAxis.hasTime(date);
    }
    
    public double getMissingValue(final GridDatatype grid) {
        return Double.NaN;
    }
    
    public Point readData(final GridDatatype grid, final Date date, final double lat, final double lon) throws IOException {
        final GridCoordSystem gcs = grid.getCoordinateSystem();
        final CoordinateAxis1DTime timeAxis = gcs.getTimeAxis1D();
        final int tidx = timeAxis.findTimeIndexFromDate(date);
        final int[] xy = gcs.findXYindexFromLatLonBounded(lat, lon, null);
        final Array data = grid.readDataSlice(tidx, -1, xy[1], xy[0]);
        final LatLonPoint latlon = gcs.getLatLon(xy[0], xy[1]);
        final Point p = new Point();
        p.lat = latlon.getLatitude();
        p.lon = latlon.getLongitude();
        p.dataValue = data.getDouble(data.getIndex());
        return p;
    }
    
    public boolean hasVert(final GridDatatype grid, final double zCoord) {
        final GridCoordSystem gcs = grid.getCoordinateSystem();
        final CoordinateAxis1D zAxis = gcs.getVerticalAxis();
        return zAxis != null && zAxis.findCoordElement(zCoord) >= 0;
    }
    
    public Point readData(final GridDatatype grid, final Date date, final double zCoord, final double lat, final double lon) throws IOException {
        final GridCoordSystem gcs = grid.getCoordinateSystem();
        final CoordinateAxis1DTime timeAxis = gcs.getTimeAxis1D();
        final int tidx = timeAxis.findTimeIndexFromDate(date);
        final CoordinateAxis1D zAxis = gcs.getVerticalAxis();
        final int zidx = zAxis.findCoordElement(zCoord);
        final int[] xy = gcs.findXYindexFromLatLon(lat, lon, null);
        final Array data = grid.readDataSlice(tidx, zidx, xy[1], xy[0]);
        final LatLonPoint latlon = gcs.getLatLon(xy[0], xy[1]);
        final Point p = new Point();
        p.lat = latlon.getLatitude();
        p.lon = latlon.getLongitude();
        p.z = zAxis.getCoordValue(zidx);
        p.dataValue = data.getDouble(data.getIndex());
        return p;
    }
    
    public class Point
    {
        public double lat;
        public double lon;
        public double z;
        public double dataValue;
    }
}
