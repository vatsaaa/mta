// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.grid;

import org.slf4j.LoggerFactory;
import ucar.nc2.units.DateFormatter;
import ucar.unidata.geoloc.projection.sat.MSGnavigation;
import ucar.unidata.geoloc.projection.VerticalPerspectiveView;
import java.util.StringTokenizer;
import ucar.nc2.units.TimeUnit;
import ucar.nc2.units.DateUnit;
import ucar.nc2.units.DateRange;
import ucar.nc2.util.NamedAnything;
import ucar.unidata.geoloc.ProjectionPoint;
import ucar.unidata.geoloc.Projection;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.ProjectionPointImpl;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.ma2.IndexIterator;
import ucar.ma2.Array;
import ucar.ma2.DataType;
import ucar.ma2.Section;
import ucar.nc2.dataset.CoordinateAxis2D;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Range;
import java.util.Collection;
import java.util.Comparator;
import java.util.Collections;
import ucar.nc2.dataset.CoordinateTransform;
import java.io.IOException;
import ucar.nc2.dataset.VariableDS;
import ucar.unidata.geoloc.projection.RotatedLatLon;
import java.util.Iterator;
import ucar.nc2.constants.AxisType;
import java.util.ArrayList;
import ucar.nc2.Variable;
import ucar.nc2.units.SimpleUnit;
import ucar.unidata.geoloc.projection.RotatedPole;
import ucar.nc2.dataset.VariableEnhanced;
import java.util.Formatter;
import ucar.unidata.geoloc.LatLonRect;
import ucar.unidata.geoloc.ProjectionRect;
import java.util.Date;
import ucar.nc2.util.NamedObject;
import java.util.List;
import ucar.nc2.Dimension;
import ucar.unidata.geoloc.vertical.VerticalTransform;
import ucar.nc2.dataset.VerticalCT;
import ucar.nc2.dataset.CoordinateAxis1DTime;
import ucar.nc2.dataset.CoordinateAxis1D;
import ucar.nc2.dataset.CoordinateAxis;
import ucar.unidata.geoloc.ProjectionImpl;
import org.slf4j.Logger;
import ucar.nc2.dt.GridCoordSystem;
import ucar.nc2.dataset.CoordinateSystem;

public class GridCoordSys extends CoordinateSystem implements GridCoordSystem
{
    private static Logger log;
    private ProjectionImpl proj;
    private GridCoordinate2D g2d;
    private CoordinateAxis horizXaxis;
    private CoordinateAxis horizYaxis;
    private CoordinateAxis1D vertZaxis;
    private CoordinateAxis1D ensembleAxis;
    private CoordinateAxis1DTime timeTaxis;
    private CoordinateAxis1DTime runTimeAxis;
    private VerticalCT vCT;
    private VerticalTransform vt;
    private Dimension timeDim;
    private boolean isDate;
    private boolean isLatLon;
    private List<NamedObject> levels;
    private List<NamedObject> times;
    private Date[] timeDates;
    private CoordinateAxis1DTime[] timeAxisForRun;
    private ProjectionRect mapArea;
    private LatLonRect llbb;
    
    public static boolean isGridCoordSys(final Formatter sbuff, final CoordinateSystem cs, final VariableEnhanced v) {
        if (cs.getRankDomain() < 2) {
            if (sbuff != null) {
                sbuff.format("%s: domain rank < 2%n", cs.getName());
            }
            return false;
        }
        if (!cs.isLatLon()) {
            if (cs.getXaxis() == null || cs.getYaxis() == null) {
                if (sbuff != null) {
                    sbuff.format("%s: NO Lat,Lon or X,Y axis%n", cs.getName());
                }
                return false;
            }
            if (null == cs.getProjection()) {
                if (sbuff != null) {
                    sbuff.format("%s: NO projection found%n", cs.getName());
                }
                return false;
            }
        }
        CoordinateAxis xaxis;
        CoordinateAxis yaxis;
        if (cs.isGeoXY()) {
            xaxis = cs.getXaxis();
            yaxis = cs.getYaxis();
            final ProjectionImpl p = cs.getProjection();
            if (!(p instanceof RotatedPole)) {
                if (!SimpleUnit.kmUnit.isCompatible(xaxis.getUnitsString()) && sbuff != null) {
                    sbuff.format("%s: X axis units are not convertible to km%n", cs.getName());
                }
                if (!SimpleUnit.kmUnit.isCompatible(yaxis.getUnitsString()) && sbuff != null) {
                    sbuff.format("%s: Y axis units are not convertible to km%n", cs.getName());
                }
            }
        }
        else {
            xaxis = cs.getLonAxis();
            yaxis = cs.getLatAxis();
        }
        if (xaxis.getRank() > 2 || yaxis.getRank() > 2) {
            if (sbuff != null) {
                sbuff.format("%s: X or Y axis rank must be <= 2%n", cs.getName());
            }
            return false;
        }
        final List<Dimension> xyDomain = CoordinateSystem.makeDomain(new CoordinateAxis[] { xaxis, yaxis });
        if (xyDomain.size() < 2) {
            if (sbuff != null) {
                sbuff.format("%s: X and Y axis must have 2 or more dimensions%n", cs.getName());
            }
            return false;
        }
        final List<CoordinateAxis> testAxis = new ArrayList<CoordinateAxis>();
        testAxis.add(xaxis);
        testAxis.add(yaxis);
        CoordinateAxis z = cs.getHeightAxis();
        if (z == null || !(z instanceof CoordinateAxis1D)) {
            z = cs.getPressureAxis();
        }
        if (z == null || !(z instanceof CoordinateAxis1D)) {
            z = cs.getZaxis();
        }
        if (z != null && !(z instanceof CoordinateAxis1D)) {
            if (sbuff != null) {
                sbuff.format("%s: Z axis must be 1D%n", cs.getName());
            }
            return false;
        }
        if (z != null) {
            testAxis.add(z);
        }
        final CoordinateAxis t = cs.getTaxis();
        final CoordinateAxis rt = cs.findAxis(AxisType.RunTime);
        if (rt != null && !(rt instanceof CoordinateAxis1D)) {
            if (sbuff != null) {
                sbuff.format("%s: RunTime axis must be 1D%n", cs.getName());
            }
            return false;
        }
        if (t != null && !(t instanceof CoordinateAxis1D) && t.getRank() != 0) {
            if (rt == null) {
                if (sbuff != null) {
                    sbuff.format("%s: T axis must be 1D%n", cs.getName());
                }
                return false;
            }
            if (t.getRank() != 2) {
                if (sbuff != null) {
                    sbuff.format("%s: Time axis must be 2D when used with RunTime dimension%n", cs.getName());
                }
                return false;
            }
            final CoordinateAxis1D rt1D = (CoordinateAxis1D)rt;
            if (!rt1D.getDimension(0).equals(t.getDimension(0))) {
                if (sbuff != null) {
                    sbuff.format("%s: Time axis must use RunTime dimension%n", cs.getName());
                }
                return false;
            }
        }
        if (t != null) {
            testAxis.add(t);
        }
        else if (rt != null) {
            testAxis.add(rt);
        }
        final CoordinateAxis ens = cs.getEnsembleAxis();
        if (ens != null) {
            testAxis.add(ens);
        }
        if (v != null) {
            final List<Dimension> testDomain = new ArrayList<Dimension>();
            for (final CoordinateAxis axis : testAxis) {
                for (final Dimension dim : axis.getDimensions()) {
                    if (!testDomain.contains(dim)) {
                        testDomain.add(dim);
                    }
                }
            }
            if (!CoordinateSystem.isSubset(v.getDimensionsAll(), testDomain)) {
                if (sbuff != null) {
                    sbuff.format(" NOT complete\n", new Object[0]);
                }
                return false;
            }
        }
        return true;
    }
    
    public static GridCoordSys makeGridCoordSys(final Formatter sbuff, final CoordinateSystem cs, final VariableEnhanced v) {
        if (sbuff != null) {
            sbuff.format(" ", new Object[0]);
            v.getNameAndDimensions(sbuff, false, true);
            sbuff.format(" check CS %s: ", cs.getName());
        }
        if (isGridCoordSys(sbuff, cs, v)) {
            final GridCoordSys gcs = new GridCoordSys(cs, sbuff);
            if (sbuff != null) {
                sbuff.format(" OK\n", new Object[0]);
            }
            return gcs;
        }
        return null;
    }
    
    public GridCoordSys(final CoordinateSystem cs, final Formatter sbuff) {
        this.isDate = false;
        this.isLatLon = false;
        this.levels = null;
        this.times = null;
        this.timeDates = null;
        this.mapArea = null;
        this.llbb = null;
        this.ds = cs.getNetcdfDataset();
        if (cs.isGeoXY()) {
            final CoordinateAxis xaxis = cs.getXaxis();
            this.xAxis = xaxis;
            this.horizXaxis = xaxis;
            final CoordinateAxis yaxis = cs.getYaxis();
            this.yAxis = yaxis;
            this.horizYaxis = yaxis;
            final ProjectionImpl p = cs.getProjection();
            if (!(p instanceof RotatedPole) && !(p instanceof RotatedLatLon)) {
                this.horizXaxis = this.convertUnits(this.horizXaxis);
                this.horizYaxis = this.convertUnits(this.horizYaxis);
            }
        }
        else {
            if (!cs.isLatLon()) {
                throw new IllegalArgumentException("CoordinateSystem is not geoReferencing");
            }
            final CoordinateAxis lonAxis = cs.getLonAxis();
            this.lonAxis = lonAxis;
            this.horizXaxis = lonAxis;
            final CoordinateAxis latAxis = cs.getLatAxis();
            this.latAxis = latAxis;
            this.horizYaxis = latAxis;
            this.isLatLon = true;
        }
        this.coordAxes.add(this.horizXaxis);
        this.coordAxes.add(this.horizYaxis);
        final ProjectionImpl projOrig = cs.getProjection();
        if (projOrig != null) {
            (this.proj = projOrig.constructCopy()).setDefaultMapArea(this.getBoundingBox());
        }
        final CoordinateAxis heightAxis = cs.getHeightAxis();
        this.hAxis = heightAxis;
        CoordinateAxis z_oneD = heightAxis;
        if (z_oneD == null || !(z_oneD instanceof CoordinateAxis1D)) {
            final CoordinateAxis pressureAxis = cs.getPressureAxis();
            this.pAxis = pressureAxis;
            z_oneD = pressureAxis;
        }
        if (z_oneD == null || !(z_oneD instanceof CoordinateAxis1D)) {
            final CoordinateAxis zaxis = cs.getZaxis();
            this.zAxis = zaxis;
            z_oneD = zaxis;
        }
        if (z_oneD != null && !(z_oneD instanceof CoordinateAxis1D)) {
            z_oneD = null;
        }
        CoordinateAxis z_best = this.hAxis;
        if (this.pAxis != null && (z_best == null || z_best.getRank() <= this.pAxis.getRank())) {
            z_best = this.pAxis;
        }
        if (this.zAxis != null && (z_best == null || z_best.getRank() <= this.zAxis.getRank())) {
            z_best = this.zAxis;
        }
        if (z_oneD == null && z_best != null && sbuff != null) {
            sbuff.format("GridCoordSys needs a 1D Coordinate, instead has %s%n", z_best.getNameAndDimensions());
        }
        if (z_oneD != null) {
            this.vertZaxis = (CoordinateAxis1D)z_oneD;
            this.coordAxes.add(this.vertZaxis);
        }
        else {
            final CoordinateAxis hAxis = null;
            this.zAxis = hAxis;
            this.pAxis = hAxis;
            this.hAxis = hAxis;
        }
        final CoordinateAxis t = cs.getTaxis();
        if (t != null) {
            if (t instanceof CoordinateAxis1D) {
                try {
                    if (t instanceof CoordinateAxis1DTime) {
                        this.timeTaxis = (CoordinateAxis1DTime)t;
                    }
                    else {
                        this.timeTaxis = CoordinateAxis1DTime.factory(this.ds, t, sbuff);
                    }
                    this.tAxis = this.timeTaxis;
                    this.coordAxes.add(this.timeTaxis);
                    this.timeDim = t.getDimension(0);
                }
                catch (Exception e) {
                    if (sbuff != null) {
                        sbuff.format("Error reading time coord= %s err= %s\n", t.getName(), e.getMessage());
                    }
                }
            }
            else {
                this.tAxis = t;
                this.timeTaxis = null;
                this.coordAxes.add(t);
            }
        }
        this.ensembleAxis = (CoordinateAxis1D)cs.findAxis(AxisType.Ensemble);
        if (null != this.ensembleAxis) {
            this.coordAxes.add(this.ensembleAxis);
        }
        final CoordinateAxis1D rtAxis = (CoordinateAxis1D)cs.findAxis(AxisType.RunTime);
        if (null != rtAxis) {
            try {
                if (rtAxis instanceof CoordinateAxis1DTime) {
                    this.runTimeAxis = (CoordinateAxis1DTime)rtAxis;
                }
                else {
                    this.runTimeAxis = CoordinateAxis1DTime.factory(this.ds, rtAxis, sbuff);
                }
                this.coordAxes.add(this.runTimeAxis);
            }
            catch (IOException e2) {
                if (sbuff != null) {
                    sbuff.format("Error reading runtime coord= %s err= %s\n", t.getName(), e2.getMessage());
                }
            }
        }
        final List<CoordinateTransform> list = cs.getCoordinateTransforms();
        for (final CoordinateTransform ct : list) {
            if (ct instanceof VerticalCT) {
                this.vCT = (VerticalCT)ct;
                break;
            }
        }
        Collections.sort(this.coordAxes, new CoordinateAxis.AxisComparator());
        this.name = CoordinateSystem.makeName(this.coordAxes);
        this.coordTrans = new ArrayList<CoordinateTransform>(cs.getCoordinateTransforms());
        for (final CoordinateAxis axis : this.coordAxes) {
            final List<Dimension> dims = axis.getDimensions();
            for (final Dimension dim : dims) {
                if (!this.domain.contains(dim)) {
                    this.domain.add(dim);
                }
            }
        }
    }
    
    public GridCoordSys(final GridCoordSys from, final Range t_range, final Range z_range, final Range y_range, final Range x_range) throws InvalidRangeException {
        this(from, null, null, t_range, z_range, y_range, x_range);
    }
    
    public GridCoordSys(final GridCoordSys from, final Range rt_range, final Range e_range, final Range t_range, final Range z_range, final Range y_range, final Range x_range) throws InvalidRangeException {
        this.isDate = false;
        this.isLatLon = false;
        this.levels = null;
        this.times = null;
        this.timeDates = null;
        this.mapArea = null;
        this.llbb = null;
        final CoordinateAxis xaxis = from.getXHorizAxis();
        final CoordinateAxis yaxis = from.getYHorizAxis();
        if (xaxis instanceof CoordinateAxis1D && yaxis instanceof CoordinateAxis1D) {
            final CoordinateAxis1D xaxis2 = (CoordinateAxis1D)xaxis;
            final CoordinateAxis1D yaxis2 = (CoordinateAxis1D)yaxis;
            this.horizXaxis = ((x_range == null) ? xaxis2 : xaxis2.section(x_range));
            this.horizYaxis = ((y_range == null) ? yaxis : yaxis2.section(y_range));
        }
        else {
            if (!(xaxis instanceof CoordinateAxis2D) || !(yaxis instanceof CoordinateAxis2D) || !from.isLatLon()) {
                throw new IllegalArgumentException("must be 1D or 2D/LatLon ");
            }
            final CoordinateAxis2D lon_axis = (CoordinateAxis2D)xaxis;
            final CoordinateAxis2D lat_axis = (CoordinateAxis2D)yaxis;
            this.horizXaxis = lon_axis.section(y_range, x_range);
            this.horizYaxis = lat_axis.section(y_range, x_range);
        }
        if (from.isGeoXY()) {
            this.xAxis = this.horizXaxis;
            this.yAxis = this.horizYaxis;
        }
        else {
            this.lonAxis = this.horizXaxis;
            this.latAxis = this.horizYaxis;
            this.isLatLon = true;
        }
        this.coordAxes.add(this.horizXaxis);
        this.coordAxes.add(this.horizYaxis);
        final ProjectionImpl projOrig = from.getProjection();
        if (projOrig != null) {
            (this.proj = projOrig.constructCopy()).setDefaultMapArea(this.getBoundingBox());
        }
        final CoordinateAxis1D zaxis = from.getVerticalAxis();
        if (zaxis != null) {
            this.vertZaxis = ((z_range == null) ? zaxis : zaxis.section(z_range));
            this.coordAxes.add(this.vertZaxis);
        }
        if (from.getVerticalCT() != null) {
            final VerticalTransform vtFrom = from.getVerticalTransform();
            if (vtFrom != null) {
                this.vt = vtFrom.subset(t_range, z_range, y_range, x_range);
            }
            this.vCT = from.getVerticalCT();
        }
        final CoordinateAxis1DTime rtaxis = from.getRunTimeAxis();
        if (rtaxis != null) {
            this.runTimeAxis = (CoordinateAxis1DTime)((rt_range == null) ? rtaxis : rtaxis.section(rt_range));
            this.coordAxes.add(this.runTimeAxis);
        }
        final CoordinateAxis1D eaxis = from.getEnsembleAxis();
        if (eaxis != null) {
            this.ensembleAxis = ((e_range == null) ? eaxis : eaxis.section(e_range));
            this.coordAxes.add(this.ensembleAxis);
        }
        final CoordinateAxis taxis = from.getTimeAxis();
        if (taxis != null) {
            if (taxis instanceof CoordinateAxis1DTime) {
                final CoordinateAxis1DTime taxis1D = (CoordinateAxis1DTime)taxis;
                final CoordinateAxis1DTime coordinateAxis1DTime = (CoordinateAxis1DTime)((t_range == null) ? taxis1D : taxis1D.section(t_range));
                this.timeTaxis = coordinateAxis1DTime;
                this.tAxis = coordinateAxis1DTime;
                this.coordAxes.add(this.timeTaxis);
                this.timeDim = this.timeTaxis.getDimension(0);
            }
            else {
                if (rt_range == null && t_range == null) {
                    this.tAxis = taxis;
                }
                else {
                    final Section timeSection = new Section().appendRange(rt_range).appendRange(t_range);
                    this.tAxis = (CoordinateAxis)taxis.section(timeSection);
                }
                this.coordAxes.add(this.tAxis);
            }
        }
        Collections.sort(this.coordAxes, new CoordinateAxis.AxisComparator());
        this.name = CoordinateSystem.makeName(this.coordAxes);
        this.coordTrans = new ArrayList<CoordinateTransform>(from.getCoordinateTransforms());
        for (final CoordinateAxis axis : this.coordAxes) {
            final List<Dimension> dims = axis.getDimensions();
            for (final Dimension dim : dims) {
                dim.setShared(true);
                if (!this.domain.contains(dim)) {
                    this.domain.add(dim);
                }
            }
        }
    }
    
    private CoordinateAxis convertUnits(final CoordinateAxis axis) {
        final String units = axis.getUnitsString();
        final SimpleUnit axisUnit = SimpleUnit.factory(units);
        double factor;
        try {
            factor = axisUnit.convertTo(1.0, SimpleUnit.kmUnit);
        }
        catch (IllegalArgumentException e) {
            GridCoordSys.log.warn("convertUnits failed", e);
            return axis;
        }
        if (factor == 1.0) {
            return axis;
        }
        Array data;
        try {
            data = axis.read();
        }
        catch (IOException e2) {
            GridCoordSys.log.warn("convertUnits read failed", e2);
            return axis;
        }
        final DataType dtype = axis.getDataType();
        if (dtype.isFloatingPoint()) {
            final IndexIterator ii = data.getIndexIterator();
            while (ii.hasNext()) {
                ii.setDoubleCurrent(factor * ii.getDoubleNext());
            }
            final CoordinateAxis newAxis = axis.copyNoCache();
            newAxis.setCachedData(data, false);
            newAxis.setUnitsString("km");
            return newAxis;
        }
        final Array newData = Array.factory(DataType.DOUBLE, axis.getShape());
        final IndexIterator newi = newData.getIndexIterator();
        final IndexIterator ii2 = data.getIndexIterator();
        while (ii2.hasNext() && newi.hasNext()) {
            newi.setDoubleNext(factor * ii2.getDoubleNext());
        }
        final CoordinateAxis newAxis2 = axis.copyNoCache();
        newAxis2.setDataType(DataType.DOUBLE);
        newAxis2.setCachedData(newData, false);
        newAxis2.setUnitsString("km");
        return newAxis2;
    }
    
    public VerticalTransform getVerticalTransform() {
        return this.vt;
    }
    
    public VerticalCT getVerticalCT() {
        return this.vCT;
    }
    
    void makeVerticalTransform(final GridDataset gds, final Formatter parseInfo) {
        if (this.vt != null) {
            return;
        }
        if (this.vCT == null) {
            return;
        }
        this.vt = this.vCT.makeVerticalTransform(gds.getNetcdfDataset(), this.timeDim);
        if (this.vt == null) {
            if (parseInfo != null) {
                parseInfo.format("  - ERR can't make VerticalTransform = %s\n", this.vCT.getVerticalTransformType());
            }
        }
        else if (parseInfo != null) {
            parseInfo.format("  - VerticalTransform = %s\n", this.vCT.getVerticalTransformType());
        }
    }
    
    public CoordinateAxis getXHorizAxis() {
        return this.horizXaxis;
    }
    
    public CoordinateAxis getYHorizAxis() {
        return this.horizYaxis;
    }
    
    public CoordinateAxis1D getVerticalAxis() {
        return this.vertZaxis;
    }
    
    public CoordinateAxis getTimeAxis() {
        return this.tAxis;
    }
    
    public CoordinateAxis1DTime getTimeAxis1D() {
        return this.timeTaxis;
    }
    
    public CoordinateAxis1DTime getRunTimeAxis() {
        return this.runTimeAxis;
    }
    
    @Override
    public CoordinateAxis1D getEnsembleAxis() {
        return this.ensembleAxis;
    }
    
    @Override
    public ProjectionImpl getProjection() {
        return this.proj;
    }
    
    public void setProjectionBoundingBox() {
        if (this.proj != null) {
            this.proj.setDefaultMapArea(this.getBoundingBox());
        }
    }
    
    public List<NamedObject> getLevels() {
        if (this.levels == null) {
            this.makeLevels();
        }
        return this.levels;
    }
    
    public List<NamedObject> getTimes() {
        if (this.times == null) {
            this.makeTimes();
        }
        return this.times;
    }
    
    public Date[] getTimeDates() {
        if (this.timeDates == null) {
            this.makeTimes();
        }
        return this.timeDates;
    }
    
    @Override
    public boolean isLatLon() {
        return this.isLatLon;
    }
    
    public boolean isDate() {
        if (this.timeDates == null) {
            this.makeTimes();
        }
        return this.isDate;
    }
    
    public boolean isZPositive() {
        if (this.vertZaxis == null) {
            return false;
        }
        if (this.vertZaxis.getPositive() != null) {
            return this.vertZaxis.getPositive().equalsIgnoreCase("up");
        }
        return this.vertZaxis.getAxisType() == AxisType.Height || this.vertZaxis.getAxisType() != AxisType.Pressure;
    }
    
    public boolean isRegularSpatial() {
        return this.isRegularSpatial(this.getXHorizAxis()) && this.isRegularSpatial(this.getYHorizAxis());
    }
    
    private boolean isRegularSpatial(final CoordinateAxis axis) {
        return axis == null || (axis instanceof CoordinateAxis1D && ((CoordinateAxis1D)axis).isRegular());
    }
    
    public int[] findXYindexFromCoord(final double x_coord, final double y_coord, int[] result) {
        if (result == null) {
            result = new int[2];
        }
        if (this.horizXaxis instanceof CoordinateAxis1D && this.horizYaxis instanceof CoordinateAxis1D) {
            result[0] = ((CoordinateAxis1D)this.horizXaxis).findCoordElement(x_coord);
            result[1] = ((CoordinateAxis1D)this.horizYaxis).findCoordElement(y_coord);
            return result;
        }
        if (this.horizXaxis instanceof CoordinateAxis2D && this.horizYaxis instanceof CoordinateAxis2D) {
            if (this.g2d == null) {
                this.g2d = new GridCoordinate2D((CoordinateAxis2D)this.horizYaxis, (CoordinateAxis2D)this.horizXaxis);
            }
            final int[] result2 = new int[2];
            final boolean found = this.g2d.findCoordElement(y_coord, x_coord, result2);
            if (found) {
                result[0] = result2[1];
                result[1] = result2[0];
            }
            else {
                result[1] = (result[0] = -1);
            }
            return result;
        }
        throw new IllegalStateException("GridCoordSystem.findXYindexFromCoord");
    }
    
    public int[] findXYindexFromCoordBounded(final double x_coord, final double y_coord, int[] result) {
        if (result == null) {
            result = new int[2];
        }
        if (this.horizXaxis instanceof CoordinateAxis1D && this.horizYaxis instanceof CoordinateAxis1D) {
            result[0] = ((CoordinateAxis1D)this.horizXaxis).findCoordElementBounded(x_coord);
            result[1] = ((CoordinateAxis1D)this.horizYaxis).findCoordElementBounded(y_coord);
            return result;
        }
        if (this.horizXaxis instanceof CoordinateAxis2D && this.horizYaxis instanceof CoordinateAxis2D) {
            if (this.g2d == null) {
                this.g2d = new GridCoordinate2D((CoordinateAxis2D)this.horizYaxis, (CoordinateAxis2D)this.horizXaxis);
            }
            final int[] result2 = new int[2];
            this.g2d.findCoordElement(y_coord, x_coord, result2);
            result[0] = result2[1];
            result[1] = result2[0];
            return result;
        }
        throw new IllegalStateException("GridCoordSystem.findXYindexFromCoord");
    }
    
    public int[] findXYindexFromLatLon(final double lat, final double lon, final int[] result) {
        final Projection dataProjection = this.getProjection();
        final ProjectionPoint pp = dataProjection.latLonToProj(new LatLonPointImpl(lat, lon), new ProjectionPointImpl());
        return this.findXYindexFromCoord(pp.getX(), pp.getY(), result);
    }
    
    public int[] findXYindexFromLatLonBounded(final double lat, final double lon, final int[] result) {
        final Projection dataProjection = this.getProjection();
        final ProjectionPoint pp = dataProjection.latLonToProj(new LatLonPointImpl(lat, lon), new ProjectionPointImpl());
        return this.findXYindexFromCoordBounded(pp.getX(), pp.getY(), result);
    }
    
    @Deprecated
    public int[] findXYCoordElement(final double x_coord, final double y_coord, final int[] result) {
        return this.findXYindexFromCoord(x_coord, y_coord, result);
    }
    
    public int findTimeIndexFromDate(final Date d) {
        if (this.timeDates == null) {
            this.makeTimes();
        }
        if (!this.isDate) {
            throw new UnsupportedOperationException("GridCoordSys: no time index");
        }
        int n;
        long m;
        int index;
        for (n = (int)this.timeTaxis.getSize(), m = d.getTime(), index = 0; index < n && m >= this.timeDates[index].getTime(); ++index) {}
        return index - 1;
    }
    
    public String getLevelName(final int index) {
        if (this.vertZaxis == null || index < 0 || index >= this.vertZaxis.getSize()) {
            throw new IllegalArgumentException("getLevelName = " + index);
        }
        if (this.levels == null) {
            this.makeLevels();
        }
        final NamedAnything name = this.levels.get(index);
        return name.getName();
    }
    
    public int getLevelIndex(final String name) {
        if (this.vertZaxis == null || name == null) {
            return -1;
        }
        if (this.levels == null) {
            this.makeLevels();
        }
        for (int i = 0; i < this.levels.size(); ++i) {
            final NamedAnything level = this.levels.get(i);
            if (level.getName().trim().equals(name)) {
                return i;
            }
        }
        return -1;
    }
    
    public String getTimeName(final int index) {
        if (this.timeTaxis == null || index < 0 || index >= this.timeTaxis.getSize()) {
            throw new IllegalArgumentException("getTimeName = " + index);
        }
        if (this.times == null) {
            this.makeTimes();
        }
        final NamedAnything name = this.times.get(index);
        return name.getName();
    }
    
    public int getTimeIndex(final String name) {
        if (this.timeTaxis == null || name == null) {
            return -1;
        }
        if (this.times == null) {
            this.makeTimes();
        }
        for (int i = 0; i < this.times.size(); ++i) {
            final NamedAnything time = this.times.get(i);
            if (time.getName().trim().equals(name)) {
                return i;
            }
        }
        return -1;
    }
    
    public DateRange getDateRange() {
        if (this.timeDates == null) {
            this.makeTimes();
        }
        if (this.isDate) {
            final Date[] dates = this.getTimeDates();
            return new DateRange(dates[0], dates[dates.length - 1]);
        }
        return null;
    }
    
    @Override
    public boolean hasTimeAxis() {
        return this.tAxis != null;
    }
    
    public boolean hasTimeAxis1D() {
        return this.timeTaxis != null;
    }
    
    public CoordinateAxis1DTime getTimeAxisForRun(final int run_index) {
        if (!this.hasTimeAxis() || this.hasTimeAxis1D()) {
            return null;
        }
        final int nruns = (int)this.runTimeAxis.getSize();
        if (run_index < 0 || run_index >= nruns) {
            throw new IllegalArgumentException("getTimeAxisForRun index out of bounds= " + run_index);
        }
        if (this.timeAxisForRun == null) {
            this.timeAxisForRun = new CoordinateAxis1DTime[nruns];
        }
        if (this.timeAxisForRun[run_index] == null) {
            this.timeAxisForRun[run_index] = this.makeTimeAxisForRun(run_index);
        }
        return this.timeAxisForRun[run_index];
    }
    
    private CoordinateAxis1DTime makeTimeAxisForRun(final int run_index) {
        try {
            final VariableDS section = (VariableDS)this.tAxis.slice(0, run_index);
            return CoordinateAxis1DTime.factory(this.ds, section, null);
        }
        catch (InvalidRangeException e) {
            e.printStackTrace();
        }
        catch (IOException e2) {
            e2.printStackTrace();
        }
        return null;
    }
    
    public DateUnit getDateUnit() throws Exception {
        final String tUnits = this.getTimeAxis().getUnitsString();
        return new DateUnit(tUnits);
    }
    
    public TimeUnit getTimeResolution() throws Exception {
        if (!this.isRegular()) {
            return null;
        }
        final CoordinateAxis1DTime taxis = (CoordinateAxis1DTime)this.getTimeAxis();
        final String tUnits = taxis.getUnitsString();
        final StringTokenizer stoker = new StringTokenizer(tUnits);
        final double tResolution = taxis.getIncrement();
        return new TimeUnit(tResolution, stoker.nextToken());
    }
    
    public ProjectionRect getBoundingBox() {
        if (this.mapArea == null) {
            if (this.horizXaxis == null || !this.horizXaxis.isNumeric() || this.horizYaxis == null || !this.horizYaxis.isNumeric()) {
                return null;
            }
            if (!(this.horizXaxis instanceof CoordinateAxis1D) || !(this.horizYaxis instanceof CoordinateAxis1D)) {
                this.mapArea = new ProjectionRect(this.horizXaxis.getMinValue(), this.horizYaxis.getMinValue(), this.horizXaxis.getMaxValue(), this.horizYaxis.getMaxValue());
            }
            else {
                final CoordinateAxis1D xaxis1 = (CoordinateAxis1D)this.horizXaxis;
                final CoordinateAxis1D yaxis1 = (CoordinateAxis1D)this.horizYaxis;
                this.mapArea = new ProjectionRect(xaxis1.getCoordEdge(0), yaxis1.getCoordEdge(0), xaxis1.getCoordEdge((int)xaxis1.getSize()), yaxis1.getCoordEdge((int)yaxis1.getSize()));
            }
        }
        return this.mapArea;
    }
    
    public LatLonPoint getLatLon(final int xindex, final int yindex) {
        double x;
        if (this.horizXaxis instanceof CoordinateAxis1D) {
            final CoordinateAxis1D horiz1D = (CoordinateAxis1D)this.horizXaxis;
            x = horiz1D.getCoordValue(xindex);
        }
        else {
            final CoordinateAxis2D horiz2D = (CoordinateAxis2D)this.horizXaxis;
            x = horiz2D.getCoordValue(xindex, yindex);
        }
        double y;
        if (this.horizYaxis instanceof CoordinateAxis1D) {
            final CoordinateAxis1D horiz1D = (CoordinateAxis1D)this.horizYaxis;
            y = horiz1D.getCoordValue(yindex);
        }
        else {
            final CoordinateAxis2D horiz2D = (CoordinateAxis2D)this.horizYaxis;
            y = horiz2D.getCoordValue(xindex, yindex);
        }
        return this.isLatLon() ? new LatLonPointImpl(y, x) : this.getLatLon(x, y);
    }
    
    public LatLonPoint getLatLon(final double xcoord, final double ycoord) {
        final Projection dataProjection = this.getProjection();
        return dataProjection.projToLatLon(new ProjectionPointImpl(xcoord, ycoord), new LatLonPointImpl());
    }
    
    public LatLonRect getLatLonBoundingBox() {
        if (this.llbb == null) {
            if (this.isLatLon()) {
                final double startLat = this.horizYaxis.getMinValue();
                final double startLon = this.horizXaxis.getMinValue();
                final double deltaLat = this.horizYaxis.getMaxValue() - startLat;
                final double deltaLon = this.horizXaxis.getMaxValue() - startLon;
                final LatLonPoint llpt = new LatLonPointImpl(startLat, startLon);
                this.llbb = new LatLonRect(llpt, deltaLat, deltaLon);
            }
            else {
                final Projection dataProjection = this.getProjection();
                final ProjectionRect bb = this.getBoundingBox();
                final LatLonPointImpl llpt2 = (LatLonPointImpl)dataProjection.projToLatLon(bb.getLowerLeftPoint(), new LatLonPointImpl());
                final LatLonPointImpl lrpt = (LatLonPointImpl)dataProjection.projToLatLon(bb.getLowerRightPoint(), new LatLonPointImpl());
                final LatLonPointImpl urpt = (LatLonPointImpl)dataProjection.projToLatLon(bb.getUpperRightPoint(), new LatLonPointImpl());
                final LatLonPointImpl ulpt = (LatLonPointImpl)dataProjection.projToLatLon(bb.getUpperLeftPoint(), new LatLonPointImpl());
                boolean includesNorthPole = false;
                int[] resultNP = new int[2];
                resultNP = this.findXYindexFromLatLon(90.0, 0.0, null);
                if (resultNP[0] != -1 && resultNP[1] != -1) {
                    includesNorthPole = true;
                }
                boolean includesSouthPole = false;
                int[] resultSP = new int[2];
                resultSP = this.findXYindexFromLatLon(-90.0, 0.0, null);
                if (resultSP[0] != -1 && resultSP[1] != -1) {
                    includesSouthPole = true;
                }
                if (includesNorthPole && !includesSouthPole) {
                    (this.llbb = new LatLonRect(llpt2, new LatLonPointImpl(90.0, 0.0))).extend(lrpt);
                    this.llbb.extend(urpt);
                    this.llbb.extend(ulpt);
                }
                else if (includesSouthPole && !includesNorthPole) {
                    (this.llbb = new LatLonRect(llpt2, new LatLonPointImpl(-90.0, -180.0))).extend(lrpt);
                    this.llbb.extend(urpt);
                    this.llbb.extend(ulpt);
                }
                else {
                    final double latMin = Math.min(llpt2.getLatitude(), lrpt.getLatitude());
                    final double latMax = Math.max(ulpt.getLatitude(), urpt.getLatitude());
                    final double lonMin = this.getMinOrMaxLon(llpt2.getLongitude(), ulpt.getLongitude(), true);
                    final double lonMax = this.getMinOrMaxLon(lrpt.getLongitude(), urpt.getLongitude(), false);
                    llpt2.set(latMin, lonMin);
                    urpt.set(latMax, lonMax);
                    this.llbb = new LatLonRect(llpt2, urpt);
                }
            }
        }
        return this.llbb;
    }
    
    private double getMinOrMaxLon(double lon1, double lon2, final boolean wantMin) {
        final double midpoint = (lon1 + lon2) / 2.0;
        lon1 = LatLonPointImpl.lonNormal(lon1, midpoint);
        lon2 = LatLonPointImpl.lonNormal(lon2, midpoint);
        return wantMin ? Math.min(lon1, lon2) : Math.max(lon1, lon2);
    }
    
    @Deprecated
    public List<Range> getLatLonBoundingBox(final LatLonRect rect) throws InvalidRangeException {
        return this.getRangesFromLatLonRect(rect);
    }
    
    public List<Range> getRangesFromLatLonRect(LatLonRect rect) throws InvalidRangeException {
        final ProjectionImpl proj = this.getProjection();
        if (proj != null && !(proj instanceof VerticalPerspectiveView) && !(proj instanceof MSGnavigation)) {
            final LatLonRect bb = this.getLatLonBoundingBox();
            rect = bb.intersect(rect);
            if (null == rect) {
                throw new InvalidRangeException("Request Bounding box does not intersect Grid");
            }
        }
        final CoordinateAxis xaxis = this.getXHorizAxis();
        final CoordinateAxis yaxis = this.getYHorizAxis();
        double minx;
        double miny;
        double maxx;
        double maxy;
        if (this.isLatLon()) {
            final LatLonPointImpl llpt = rect.getLowerLeftPoint();
            final LatLonPointImpl urpt = rect.getUpperRightPoint();
            final LatLonPointImpl lrpt = rect.getLowerRightPoint();
            final LatLonPointImpl ulpt = rect.getUpperLeftPoint();
            minx = this.getMinOrMaxLon(llpt.getLongitude(), ulpt.getLongitude(), true);
            miny = Math.min(llpt.getLatitude(), lrpt.getLatitude());
            maxx = this.getMinOrMaxLon(urpt.getLongitude(), lrpt.getLongitude(), false);
            maxy = Math.min(ulpt.getLatitude(), urpt.getLatitude());
            final double minLon = xaxis.getMinValue();
            minx = LatLonPointImpl.lonNormalFrom(minx, minLon);
            maxx = LatLonPointImpl.lonNormalFrom(maxx, minLon);
        }
        else {
            final ProjectionRect prect = this.getProjection().latLonToProjBB(rect);
            minx = prect.getMinPoint().getX();
            miny = prect.getMinPoint().getY();
            maxx = prect.getMaxPoint().getX();
            maxy = prect.getMaxPoint().getY();
        }
        if (xaxis instanceof CoordinateAxis1D && yaxis instanceof CoordinateAxis1D) {
            final CoordinateAxis1D xaxis2 = (CoordinateAxis1D)xaxis;
            final CoordinateAxis1D yaxis2 = (CoordinateAxis1D)yaxis;
            final int minxIndex = xaxis2.findCoordElementBounded(minx);
            final int minyIndex = yaxis2.findCoordElementBounded(miny);
            final int maxxIndex = xaxis2.findCoordElementBounded(maxx);
            final int maxyIndex = yaxis2.findCoordElementBounded(maxy);
            final List<Range> list = new ArrayList<Range>();
            list.add(new Range(Math.min(minyIndex, maxyIndex), Math.max(minyIndex, maxyIndex)));
            list.add(new Range(Math.min(minxIndex, maxxIndex), Math.max(minxIndex, maxxIndex)));
            return list;
        }
        if (xaxis instanceof CoordinateAxis2D && yaxis instanceof CoordinateAxis2D && this.isLatLon()) {
            final CoordinateAxis2D lon_axis = (CoordinateAxis2D)xaxis;
            final CoordinateAxis2D lat_axis = (CoordinateAxis2D)yaxis;
            final int[] shape = lon_axis.getShape();
            final int nj = shape[0];
            final int ni = shape[1];
            int mini = Integer.MAX_VALUE;
            int minj = Integer.MAX_VALUE;
            int maxi = -1;
            int maxj = -1;
            for (int j = 0; j < nj; ++j) {
                for (int i = 0; i < ni; ++i) {
                    final double lat = lat_axis.getCoordValue(j, i);
                    final double lon = lon_axis.getCoordValue(j, i);
                    if (lat >= miny && lat <= maxy && lon >= minx && lon <= maxx) {
                        if (i > maxi) {
                            maxi = i;
                        }
                        if (i < mini) {
                            mini = i;
                        }
                        if (j > maxj) {
                            maxj = j;
                        }
                        if (j < minj) {
                            minj = j;
                        }
                    }
                }
            }
            if (mini > maxi || minj > maxj) {
                mini = 0;
                minj = 0;
                maxi = -1;
                maxj = -1;
            }
            final ArrayList<Range> list2 = new ArrayList<Range>();
            list2.add(new Range(minj, maxj));
            list2.add(new Range(mini, maxi));
            return list2;
        }
        throw new IllegalArgumentException("must be 1D or 2D/LatLon ");
    }
    
    @Override
    public String toString() {
        final Formatter buff = new Formatter();
        buff.format("(%s) ", this.getName());
        if (this.runTimeAxis != null) {
            buff.format("rt=%s,", this.runTimeAxis.getName());
        }
        if (this.ensembleAxis != null) {
            buff.format("ens=%s,", this.ensembleAxis.getName());
        }
        if (this.timeTaxis != null) {
            buff.format("t=%s,", this.timeTaxis.getName());
        }
        if (this.vertZaxis != null) {
            buff.format("z=%s,", this.vertZaxis.getName());
        }
        if (this.horizYaxis != null) {
            buff.format("y=%s,", this.horizYaxis.getName());
        }
        if (this.horizXaxis != null) {
            buff.format("x=%s,", this.horizXaxis.getName());
        }
        if (this.proj != null) {
            buff.format("  Projection: %s %s", this.proj.getName(), this.proj.getClassName());
        }
        return buff.toString();
    }
    
    private void makeLevels() {
        if (this.vertZaxis == null) {
            this.levels = new ArrayList<NamedObject>(0);
            return;
        }
        final int n = (int)this.vertZaxis.getSize();
        this.levels = new ArrayList<NamedObject>(n);
        for (int i = 0; i < n; ++i) {
            this.levels.add(new NamedAnything(this.vertZaxis.getCoordName(i), this.vertZaxis.getUnitsString()));
        }
    }
    
    private void makeTimes() {
        if (this.timeTaxis == null || this.timeTaxis.getSize() == 0L) {
            this.times = new ArrayList<NamedObject>(0);
            this.timeDates = new Date[0];
            this.isDate = false;
            return;
        }
        final int n = (int)this.timeTaxis.getSize();
        this.timeDates = new Date[n];
        this.times = new ArrayList<NamedObject>(n);
        try {
            DateUnit du = null;
            final String units = this.timeTaxis.getUnitsString();
            if (units != null) {
                du = new DateUnit(units);
            }
            final DateFormatter formatter = new DateFormatter();
            for (int i = 0; i < n; ++i) {
                final Date d = du.makeDate(this.timeTaxis.getCoordValue(i));
                String name = formatter.toDateTimeString(d);
                if (name == null) {
                    name = Double.toString(this.timeTaxis.getCoordValue(i));
                }
                this.times.add(new NamedAnything(name, "date/time"));
                this.timeDates[i] = d;
            }
            this.isDate = true;
        }
        catch (Exception e) {
            if (this.timeTaxis.getDataType() == DataType.STRING || this.timeTaxis.getDataType() == DataType.CHAR) {
                this.isDate = true;
                final DateFormatter formatter2 = new DateFormatter();
                for (int j = 0; j < n; ++j) {
                    final String coordValue = this.timeTaxis.getCoordName(j);
                    final Date d2 = formatter2.getISODate(coordValue);
                    if (d2 == null) {
                        this.isDate = false;
                        this.times.add(new NamedAnything(coordValue, this.timeTaxis.getUnitsString()));
                    }
                    else {
                        this.times.add(new NamedAnything(formatter2.toDateTimeString(d2), "date/time"));
                        this.timeDates[j] = d2;
                    }
                }
                return;
            }
            for (int k = 0; k < n; ++k) {
                this.times.add(new NamedAnything(this.timeTaxis.getCoordName(k), this.timeTaxis.getUnitsString()));
            }
        }
    }
    
    static {
        GridCoordSys.log = LoggerFactory.getLogger(GridCoordSys.class);
    }
}
