// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.grid;

import org.slf4j.LoggerFactory;
import java.io.IOException;
import ucar.nc2.dataset.CoordinateAxis;
import ucar.nc2.dt.GridCoordSystem;
import ucar.ma2.Array;
import ucar.ma2.MAMath;
import ucar.ma2.ArrayDouble;
import ucar.nc2.dataset.CoordinateAxis2D;
import org.slf4j.Logger;

public class GridCoordinate2D
{
    private static boolean debug;
    private static Logger log;
    private CoordinateAxis2D latCoord;
    private CoordinateAxis2D lonCoord;
    private ArrayDouble.D2 latEdge;
    private ArrayDouble.D2 lonEdge;
    private MAMath.MinMax latMinMax;
    private MAMath.MinMax lonMinMax;
    int nrows;
    int ncols;
    
    GridCoordinate2D(final CoordinateAxis2D latCoord, final CoordinateAxis2D lonCoord) {
        this.latCoord = latCoord;
        this.lonCoord = lonCoord;
        assert latCoord.getRank() == 2;
        assert lonCoord.getRank() == 2;
        final int[] shape = latCoord.getShape();
        this.nrows = shape[0];
        this.ncols = shape[1];
    }
    
    private void findBounds() {
        if (this.lonMinMax != null) {
            return;
        }
        this.lonEdge = CoordinateAxis2D.makeXEdges(this.lonCoord.getMidpoints());
        this.latEdge = CoordinateAxis2D.makeYEdges(this.latCoord.getMidpoints());
        this.latMinMax = MAMath.getMinMax(this.latEdge);
        this.lonMinMax = MAMath.getMinMax(this.lonEdge);
        if (GridCoordinate2D.debug) {
            System.out.printf("Bounds (%d %d): lat= (%f,%f) lon = (%f,%f) %n", this.nrows, this.ncols, this.latMinMax.min, this.latMinMax.max, this.lonMinMax.min, this.lonMinMax.max);
        }
    }
    
    public boolean findCoordElementForce(final double wantLat, final double wantLon, final int[] rectIndex) {
        this.findBounds();
        if (wantLat < this.latMinMax.min) {
            return false;
        }
        if (wantLat > this.latMinMax.max) {
            return false;
        }
        if (wantLon < this.lonMinMax.min) {
            return false;
        }
        if (wantLon > this.lonMinMax.max) {
            return false;
        }
        final boolean saveDebug = GridCoordinate2D.debug;
        GridCoordinate2D.debug = false;
        for (int row = 0; row < this.nrows; ++row) {
            for (int col = 0; col < this.ncols; ++col) {
                rectIndex[0] = row;
                rectIndex[1] = col;
                if (this.contains(wantLat, wantLon, rectIndex)) {
                    GridCoordinate2D.debug = saveDebug;
                    return true;
                }
            }
        }
        return false;
    }
    
    public boolean findCoordElement(final double wantLat, final double wantLon, final int[] rectIndex) {
        return this.findCoordElementNoForce(wantLat, wantLon, rectIndex);
    }
    
    public boolean findCoordElementNoForce(final double wantLat, final double wantLon, final int[] rectIndex) {
        this.findBounds();
        if (wantLat < this.latMinMax.min) {
            return false;
        }
        if (wantLat > this.latMinMax.max) {
            return false;
        }
        if (wantLon < this.lonMinMax.min) {
            return false;
        }
        if (wantLon > this.lonMinMax.max) {
            return false;
        }
        final double gradientLat = (this.latMinMax.max - this.latMinMax.min) / this.nrows;
        final double gradientLon = (this.lonMinMax.max - this.lonMinMax.min) / this.ncols;
        final double diffLat = wantLat - this.latMinMax.min;
        final double diffLon = wantLon - this.lonMinMax.min;
        rectIndex[0] = (int)Math.round(diffLat / gradientLat);
        rectIndex[1] = (int)Math.round(diffLon / gradientLon);
        int count = 0;
        do {
            ++count;
            if (GridCoordinate2D.debug) {
                System.out.printf("%nIteration %d %n", count);
            }
            if (this.contains(wantLat, wantLon, rectIndex)) {
                return true;
            }
            if (!this.jump2(wantLat, wantLon, rectIndex)) {
                return false;
            }
        } while (count <= 10);
        return this.incr(wantLat, wantLon, rectIndex);
    }
    
    private boolean containsOld(final double wantLat, final double wantLon, final int[] rectIndex) {
        rectIndex[0] = Math.max(Math.min(rectIndex[0], this.nrows - 1), 0);
        rectIndex[1] = Math.max(Math.min(rectIndex[1], this.ncols - 1), 0);
        final int row = rectIndex[0];
        final int col = rectIndex[1];
        if (GridCoordinate2D.debug) {
            System.out.printf(" (%d,%d) contains (%f,%f) in (lat=%f %f) (lon=%f %f) ?%n", rectIndex[0], rectIndex[1], wantLat, wantLon, this.latEdge.get(row, col), this.latEdge.get(row + 1, col), this.lonEdge.get(row, col), this.lonEdge.get(row, col + 1));
        }
        return wantLat >= this.latEdge.get(row, col) && wantLat <= this.latEdge.get(row + 1, col) && wantLon >= this.lonEdge.get(row, col) && wantLon <= this.lonEdge.get(row, col + 1);
    }
    
    private boolean contains(final double wantLat, final double wantLon, final int[] rectIndex) {
        rectIndex[0] = Math.max(Math.min(rectIndex[0], this.nrows - 1), 0);
        rectIndex[1] = Math.max(Math.min(rectIndex[1], this.ncols - 1), 0);
        final int row = rectIndex[0];
        final int col = rectIndex[1];
        final double x1 = this.lonEdge.get(row, col);
        final double y1 = this.latEdge.get(row, col);
        final double x2 = this.lonEdge.get(row, col + 1);
        final double y2 = this.latEdge.get(row, col + 1);
        final double x3 = this.lonEdge.get(row + 1, col + 1);
        final double y3 = this.latEdge.get(row + 1, col + 1);
        final double x4 = this.lonEdge.get(row + 1, col);
        final double y4 = this.latEdge.get(row + 1, col);
        final boolean sign = this.detIsPositive(x1, y1, x2, y2, wantLon, wantLat);
        return sign == this.detIsPositive(x2, y2, x3, y3, wantLon, wantLat) && sign == this.detIsPositive(x3, y3, x4, y4, wantLon, wantLat) && sign == this.detIsPositive(x4, y4, x1, y1, wantLon, wantLat);
    }
    
    private boolean detIsPositive(final double x0, final double y0, final double x1, final double y1, final double x2, final double y2) {
        final double det = x1 * y2 - y1 * x2 - x0 * y2 + y0 * x2 + x0 * y1 - y0 * x1;
        if (det == 0.0) {
            System.out.printf("determinate = 0%n", new Object[0]);
        }
        return det > 0.0;
    }
    
    private boolean jumpOld(final double wantLat, final double wantLon, final int[] rectIndex) {
        final int row = Math.max(Math.min(rectIndex[0], this.nrows - 1), 0);
        final int col = Math.max(Math.min(rectIndex[1], this.ncols - 1), 0);
        final double gradientLat = this.latEdge.get(row + 1, col) - this.latEdge.get(row, col);
        final double gradientLon = this.lonEdge.get(row, col + 1) - this.lonEdge.get(row, col);
        final double diffLat = wantLat - this.latEdge.get(row, col);
        final double diffLon = wantLon - this.lonEdge.get(row, col);
        final int drow = (int)Math.round(diffLat / gradientLat);
        final int dcol = (int)Math.round(diffLon / gradientLon);
        if (GridCoordinate2D.debug) {
            System.out.printf("   jump from %d %d (grad=%f %f) (diff=%f %f) (delta=%d %d)", row, col, gradientLat, gradientLon, diffLat, diffLon, drow, dcol);
        }
        if (drow == 0 && dcol == 0) {
            if (GridCoordinate2D.debug) {
                System.out.printf("%n   incr:", new Object[0]);
            }
            return this.incr(wantLat, wantLon, rectIndex);
        }
        rectIndex[0] = Math.max(Math.min(row + drow, this.nrows - 1), 0);
        rectIndex[1] = Math.max(Math.min(col + dcol, this.ncols - 1), 0);
        if (GridCoordinate2D.debug) {
            System.out.printf(" to (%d %d)%n", rectIndex[0], rectIndex[1]);
        }
        return row != rectIndex[0] || col != rectIndex[1];
    }
    
    private boolean jump2(final double wantLat, final double wantLon, final int[] rectIndex) {
        final int row = Math.max(Math.min(rectIndex[0], this.nrows - 1), 0);
        final int col = Math.max(Math.min(rectIndex[1], this.ncols - 1), 0);
        final double dlatdy = this.latEdge.get(row + 1, col) - this.latEdge.get(row, col);
        final double dlatdx = this.latEdge.get(row, col + 1) - this.latEdge.get(row, col);
        final double dlondx = this.lonEdge.get(row, col + 1) - this.lonEdge.get(row, col);
        final double dlondy = this.lonEdge.get(row + 1, col) - this.lonEdge.get(row, col);
        final double diffLat = wantLat - this.latEdge.get(row, col);
        final double diffLon = wantLon - this.lonEdge.get(row, col);
        final double dx = (diffLon - dlondy * diffLat / dlatdy) / (dlondx - dlatdx * dlondy / dlatdy);
        final double dy = (diffLat - dlatdx * dx) / dlatdy;
        if (GridCoordinate2D.debug) {
            System.out.printf("   jump from %d %d (dlondx=%f dlondy=%f dlatdx=%f dlatdy=%f) (diffLat,Lon=%f %f) (deltalat,Lon=%f %f)", row, col, dlondx, dlondy, dlatdx, dlatdy, diffLat, diffLon, dy, dx);
        }
        final int drow = (int)Math.round(dy);
        final int dcol = (int)Math.round(dx);
        if (drow == 0 && dcol == 0) {
            if (GridCoordinate2D.debug) {
                System.out.printf("%n   incr:", new Object[0]);
            }
            return this.incr(wantLat, wantLon, rectIndex);
        }
        rectIndex[0] = Math.max(Math.min(row + drow, this.nrows - 1), 0);
        rectIndex[1] = Math.max(Math.min(col + dcol, this.ncols - 1), 0);
        if (GridCoordinate2D.debug) {
            System.out.printf(" to (%d %d)%n", rectIndex[0], rectIndex[1]);
        }
        return row != rectIndex[0] || col != rectIndex[1];
    }
    
    private boolean incr(final double wantLat, final double wantLon, final int[] rectIndex) {
        final int row = rectIndex[0];
        final int col = rectIndex[1];
        final double diffLat = wantLat - this.latEdge.get(row, col);
        final double diffLon = wantLon - this.lonEdge.get(row, col);
        if (Math.abs(diffLat) > Math.abs(diffLon)) {
            rectIndex[0] = row + ((diffLat > 0.0) ? 1 : -1);
            if (this.contains(wantLat, wantLon, rectIndex)) {
                return true;
            }
            rectIndex[1] = col + ((diffLon > 0.0) ? 1 : -1);
            if (this.contains(wantLat, wantLon, rectIndex)) {
                return true;
            }
        }
        else {
            rectIndex[1] = col + ((diffLon > 0.0) ? 1 : -1);
            if (this.contains(wantLat, wantLon, rectIndex)) {
                return true;
            }
            rectIndex[0] = row + ((diffLat > 0.0) ? 1 : -1);
            if (this.contains(wantLat, wantLon, rectIndex)) {
                return true;
            }
        }
        rectIndex[0] = row;
        rectIndex[1] = col;
        return this.box9(wantLat, wantLon, rectIndex);
    }
    
    private boolean box9(final double wantLat, final double wantLon, final int[] rectIndex) {
        final int row = rectIndex[0];
        final int minrow = Math.max(row - 1, 0);
        final int maxrow = Math.min(row + 1, this.nrows);
        final int col = rectIndex[1];
        final int mincol = Math.max(col - 1, 0);
        final int maxcol = Math.min(col + 1, this.ncols);
        if (GridCoordinate2D.debug) {
            System.out.printf("%n   box9:", new Object[0]);
        }
        for (int i = minrow; i <= maxrow; ++i) {
            for (int j = mincol; j <= maxcol; ++j) {
                rectIndex[0] = i;
                rectIndex[1] = j;
                if (this.contains(wantLat, wantLon, rectIndex)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    private static void doOne(final GridCoordinate2D g2d, final double wantLat, final double wantLon) {
        final int[] result = new int[2];
        if (g2d.findCoordElementForce(wantLat, wantLon, result)) {
            System.out.printf("Brute (%f %f) == (%d %d) %n", wantLat, wantLon, result[0], result[1]);
            if (g2d.findCoordElement(wantLat, wantLon, result)) {
                System.out.printf("(%f %f) == (%d %d) %n", wantLat, wantLon, result[0], result[1]);
            }
            else {
                System.out.printf("(%f %f) FAIL %n", wantLat, wantLon);
            }
            System.out.printf("----------------------------------------%n", new Object[0]);
            return;
        }
        System.out.printf("Brute (%f %f) FAIL", wantLat, wantLon);
    }
    
    public static void test1() throws IOException {
        final String filename = "D:/work/asaScience/EGM200_3.ncml";
        final GridDataset gds = GridDataset.open(filename);
        final GeoGrid grid = gds.findGridByName("u_wind");
        final GridCoordSystem gcs = grid.getCoordinateSystem();
        final CoordinateAxis lonAxis = gcs.getXHorizAxis();
        assert lonAxis instanceof CoordinateAxis2D;
        final CoordinateAxis latAxis = gcs.getYHorizAxis();
        assert latAxis instanceof CoordinateAxis2D;
        final GridCoordinate2D g2d = new GridCoordinate2D((CoordinateAxis2D)latAxis, (CoordinateAxis2D)lonAxis);
        doOne(g2d, 35.0, -6.0);
        doOne(g2d, 34.667302, -5.008376);
        doOne(g2d, 34.667303, -6.39424);
        doOne(g2d, 36.6346, -5.0084);
        doOne(g2d, 36.6346, -6.39424);
        gds.close();
    }
    
    public static void test2() throws IOException {
        final String filename = "C:/data/fmrc/apex_fmrc/Run_20091025_0000.nc";
        final GridDataset gds = GridDataset.open(filename);
        final GeoGrid grid = gds.findGridByName("temp");
        final GridCoordSystem gcs = grid.getCoordinateSystem();
        final CoordinateAxis lonAxis = gcs.getXHorizAxis();
        assert lonAxis instanceof CoordinateAxis2D;
        final CoordinateAxis latAxis = gcs.getYHorizAxis();
        assert latAxis instanceof CoordinateAxis2D;
        final GridCoordinate2D g2d = new GridCoordinate2D((CoordinateAxis2D)latAxis, (CoordinateAxis2D)lonAxis);
        doOne(g2d, 40.166959, -73.954234);
        gds.close();
    }
    
    public static void test3() throws IOException {
        final String filename = "/data/testdata/cdmUnitTest/fmrc/rtofs/ofs.20091122/ofs_atl.t00z.F024.grb.grib2";
        final GridDataset gds = GridDataset.open(filename);
        final GeoGrid grid = gds.findGridByName("Sea_Surface_Height_Relative_to_Geoid");
        final GridCoordSystem gcs = grid.getCoordinateSystem();
        final CoordinateAxis lonAxis = gcs.getXHorizAxis();
        assert lonAxis instanceof CoordinateAxis2D;
        final CoordinateAxis latAxis = gcs.getYHorizAxis();
        assert latAxis instanceof CoordinateAxis2D;
        final GridCoordinate2D g2d = new GridCoordinate2D((CoordinateAxis2D)latAxis, (CoordinateAxis2D)lonAxis);
        doOne(g2d, -15.554099426977835, -0.7742870290336263);
        gds.close();
    }
    
    public static void main(final String[] args) throws IOException {
        test3();
    }
    
    static {
        GridCoordinate2D.debug = false;
        GridCoordinate2D.log = LoggerFactory.getLogger(GridCoordinate2D.class);
    }
}
