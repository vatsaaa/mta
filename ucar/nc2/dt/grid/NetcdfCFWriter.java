// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.grid;

import org.slf4j.LoggerFactory;
import java.text.ParseException;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.nc2.units.DateFormatter;
import ucar.ma2.Array;
import ucar.unidata.geoloc.ProjectionPoint;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.ProjectionPointImpl;
import ucar.ma2.DataType;
import ucar.nc2.Structure;
import ucar.nc2.Dimension;
import ucar.nc2.dataset.transform.AbstractCoordTransBuilder;
import ucar.nc2.dataset.ProjectionCT;
import ucar.nc2.Group;
import ucar.nc2.NetcdfFileWriteable;
import ucar.unidata.geoloc.Projection;
import ucar.nc2.dataset.CoordinateAxis1DTime;
import ucar.nc2.dt.GridCoordSystem;
import ucar.nc2.dt.GridDatatype;
import java.util.Iterator;
import ucar.nc2.constants.AxisType;
import ucar.nc2.dataset.TransformType;
import ucar.nc2.NetcdfFile;
import ucar.unidata.geoloc.projection.LatLonProjection;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.nc2.dataset.CoordinateAxis;
import ucar.nc2.Variable;
import java.util.ArrayList;
import java.util.Date;
import ucar.nc2.Attribute;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.FileWriter;
import ucar.ma2.InvalidRangeException;
import java.io.IOException;
import ucar.ma2.Range;
import ucar.nc2.units.DateRange;
import ucar.unidata.geoloc.LatLonRect;
import java.util.List;
import ucar.nc2.dt.GridDataset;
import org.slf4j.Logger;

public class NetcdfCFWriter
{
    private static long maxSize;
    private static Logger log;
    
    public void makeFile(final String location, final GridDataset gds, final List<String> gridList, final LatLonRect llbb, final DateRange range, final boolean addLatLon, final int horizStride, final int stride_z, final int stride_time) throws IOException, InvalidRangeException {
        this.makeFile(location, gds, gridList, llbb, horizStride, null, range, stride_time, addLatLon);
    }
    
    public void makeFile(final String location, final GridDataset gds, final List<String> gridList, final LatLonRect llbb, final int horizStride, final Range zRange, final DateRange dateRange, final int stride_time, boolean addLatLon) throws IOException, InvalidRangeException {
        final FileWriter writer = new FileWriter(location, false);
        final NetcdfDataset ncd = (NetcdfDataset)gds.getNetcdfFile();
        for (final Attribute att : gds.getGlobalAttributes()) {
            writer.writeGlobalAttribute(att);
        }
        writer.writeGlobalAttribute(new Attribute("Conventions", "CF-1.0"));
        writer.writeGlobalAttribute(new Attribute("History", "Translated to CF-1.0 Conventions by Netcdf-Java CDM (NetcdfCFWriter)\nOriginal Dataset = " + gds.getLocationURI() + "; Translation Date = " + new Date()));
        final ArrayList<Variable> varList = new ArrayList<Variable>();
        final ArrayList<String> varNameList = new ArrayList<String>();
        final ArrayList<CoordinateAxis> axisList = new ArrayList<CoordinateAxis>();
        long total_size = 0L;
        for (final String gridName : gridList) {
            if (varNameList.contains(gridName)) {
                continue;
            }
            varNameList.add(gridName);
            GridDatatype grid = gds.findGridDatatype(gridName);
            final GridCoordSystem gcsOrg = grid.getCoordinateSystem();
            final CoordinateAxis1DTime timeAxis = gcsOrg.getTimeAxis1D();
            Range timeRange = null;
            if (dateRange != null && timeAxis != null) {
                final int startIndex = timeAxis.findTimeIndexFromDate(dateRange.getStart().getDate());
                final int endIndex = timeAxis.findTimeIndexFromDate(dateRange.getEnd().getDate());
                if (startIndex < 0) {
                    throw new InvalidRangeException("start time=" + dateRange.getStart().getDate() + " must be >= " + timeAxis.getTimeDate(0));
                }
                if (endIndex < 0) {
                    throw new InvalidRangeException("end time=" + dateRange.getEnd().getDate() + " must be >= " + timeAxis.getTimeDate(0));
                }
                timeRange = new Range(startIndex, endIndex);
            }
            if (null != timeRange || zRange != null || llbb != null || horizStride > 1) {
                grid = grid.makeSubset(timeRange, zRange, llbb, 1, horizStride, horizStride);
            }
            final Variable gridV = grid.getVariable();
            varList.add(gridV);
            total_size += gridV.getSize() * gridV.getElementSize();
            final GridCoordSystem gcs = grid.getCoordinateSystem();
            for (final CoordinateAxis axis : gcs.getCoordinateAxes()) {
                if (!varNameList.contains(axis.getName())) {
                    varNameList.add(axis.getName());
                    varList.add(axis);
                    axisList.add(axis);
                }
            }
            for (final CoordinateTransform ct : gcs.getCoordinateTransforms()) {
                final Variable v = ncd.findVariable(ct.getName());
                if (!varNameList.contains(ct.getName()) && null != v) {
                    varNameList.add(ct.getName());
                    varList.add(v);
                }
            }
            if (!addLatLon) {
                continue;
            }
            final Projection proj = gcs.getProjection();
            if (null == proj || proj instanceof LatLonProjection) {
                continue;
            }
            this.addLatLon2D(ncd, varList, proj, gcs.getXHorizAxis(), gcs.getYHorizAxis());
            addLatLon = false;
        }
        if (total_size > NetcdfCFWriter.maxSize) {
            NetcdfCFWriter.log.info("Reject request size = {} Mbytes", total_size);
            throw new IllegalArgumentException("Request too big=" + total_size + " Mbytes, max=" + NetcdfCFWriter.maxSize);
        }
        writer.writeVariables(varList);
        final NetcdfFileWriteable ncfile = writer.getNetcdf();
        final Group root = ncfile.getRootGroup();
        for (final String gridName2 : gridList) {
            final GridDatatype grid2 = gds.findGridDatatype(gridName2);
            final Variable newV = root.findVariable(gridName2);
            if (newV == null) {
                NetcdfCFWriter.log.warn("NetcdfCFWriter cant find " + gridName2 + " in gds " + gds.getLocationURI());
            }
            else {
                final StringBuilder sbuff = new StringBuilder();
                final GridCoordSystem gcs = grid2.getCoordinateSystem();
                for (final Variable axis2 : gcs.getCoordinateAxes()) {
                    sbuff.append(axis2.getName()).append(" ");
                }
                if (addLatLon) {
                    sbuff.append("lat lon");
                }
                newV.addAttribute(new Attribute("coordinates", sbuff.toString()));
                for (final CoordinateTransform ct : gcs.getCoordinateTransforms()) {
                    final Variable v = ncd.findVariable(ct.getName());
                    if (ct.getTransformType() == TransformType.Projection) {
                        newV.addAttribute(new Attribute("grid_mapping", v.getName()));
                    }
                }
            }
        }
        for (final CoordinateAxis axis3 : axisList) {
            final Variable newV2 = root.findVariable(axis3.getShortName());
            if ((axis3.getAxisType() == AxisType.Height || axis3.getAxisType() == AxisType.Pressure || axis3.getAxisType() == AxisType.GeoZ) && null != axis3.getPositive()) {
                newV2.addAttribute(new Attribute("positive", axis3.getPositive()));
            }
            if (axis3.getAxisType() == AxisType.Lat) {
                newV2.addAttribute(new Attribute("units", "degrees_north"));
                newV2.addAttribute(new Attribute("standard_name", "latitude"));
            }
            if (axis3.getAxisType() == AxisType.Lon) {
                newV2.addAttribute(new Attribute("units", "degrees_east"));
                newV2.addAttribute(new Attribute("standard_name", "longitude"));
            }
            if (axis3.getAxisType() == AxisType.GeoX) {
                newV2.addAttribute(new Attribute("standard_name", "projection_x_coordinate"));
            }
            if (axis3.getAxisType() == AxisType.GeoY) {
                newV2.addAttribute(new Attribute("standard_name", "projection_y_coordinate"));
            }
        }
        final List<Variable> ctvList = new ArrayList<Variable>();
        for (final GridDataset.Gridset gridSet : gds.getGridsets()) {
            final GridCoordSystem gcs2 = gridSet.getGeoCoordSystem();
            final ProjectionCT pct = gcs2.getProjectionCT();
            if (pct != null) {
                final Variable v2 = root.findVariable(pct.getName());
                if (v2 == null || ctvList.contains(v2)) {
                    continue;
                }
                this.convertProjectionCTV((NetcdfDataset)gds.getNetcdfFile(), v2);
                ctvList.add(v2);
            }
        }
        writer.finish();
    }
    
    private void convertProjectionCTV(final NetcdfDataset ds, final Variable ctv) {
        final Attribute att = ctv.findAttribute("_CoordinateTransformType");
        if (null != att && att.getStringValue().equals("Projection")) {
            final Attribute east = ctv.findAttribute("false_easting");
            final Attribute north = ctv.findAttribute("false_northing");
            if (null != east || null != north) {
                final double scalef = AbstractCoordTransBuilder.getFalseEastingScaleFactor(ds, ctv);
                if (scalef != 1.0) {
                    this.convertAttribute(ctv, east, scalef);
                    this.convertAttribute(ctv, north, scalef);
                }
            }
        }
    }
    
    private void convertAttribute(final Variable ctv, final Attribute att, final double scalef) {
        if (att == null) {
            return;
        }
        final double val = scalef * att.getNumericValue().doubleValue();
        ctv.addAttribute(new Attribute(att.getName(), val));
    }
    
    private void addLatLon2D(final NetcdfFile ncfile, final List<Variable> varList, final Projection proj, final CoordinateAxis xaxis, final CoordinateAxis yaxis) throws IOException {
        final double[] xData = (double[])xaxis.read().get1DJavaArray(Double.TYPE);
        final double[] yData = (double[])yaxis.read().get1DJavaArray(Double.TYPE);
        final List<Dimension> dims = new ArrayList<Dimension>();
        dims.add(yaxis.getDimension(0));
        dims.add(xaxis.getDimension(0));
        final Variable latVar = new Variable(ncfile, null, null, "lat");
        latVar.setDataType(DataType.DOUBLE);
        latVar.setDimensions(dims);
        latVar.addAttribute(new Attribute("units", "degrees_north"));
        latVar.addAttribute(new Attribute("long_name", "latitude coordinate"));
        latVar.addAttribute(new Attribute("standard_name", "latitude"));
        latVar.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lat.toString()));
        final Variable lonVar = new Variable(ncfile, null, null, "lon");
        lonVar.setDataType(DataType.DOUBLE);
        lonVar.setDimensions(dims);
        lonVar.addAttribute(new Attribute("units", "degrees_east"));
        lonVar.addAttribute(new Attribute("long_name", "longitude coordinate"));
        lonVar.addAttribute(new Attribute("standard_name", "longitude"));
        lonVar.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lon.toString()));
        final int nx = xData.length;
        final int ny = yData.length;
        final ProjectionPointImpl projPoint = new ProjectionPointImpl();
        final LatLonPointImpl latlonPoint = new LatLonPointImpl();
        final double[] latData = new double[nx * ny];
        final double[] lonData = new double[nx * ny];
        for (int i = 0; i < ny; ++i) {
            for (int j = 0; j < nx; ++j) {
                projPoint.setLocation(xData[j], yData[i]);
                proj.projToLatLon(projPoint, latlonPoint);
                latData[i * nx + j] = latlonPoint.getLatitude();
                lonData[i * nx + j] = latlonPoint.getLongitude();
            }
        }
        final Array latDataArray = Array.factory(DataType.DOUBLE, new int[] { ny, nx }, latData);
        latVar.setCachedData(latDataArray, false);
        final Array lonDataArray = Array.factory(DataType.DOUBLE, new int[] { ny, nx }, lonData);
        lonVar.setCachedData(lonDataArray, false);
        varList.add(latVar);
        varList.add(lonVar);
    }
    
    public static void test1() throws IOException, InvalidRangeException, ParseException {
        final String fileIn = "C:/data/ncmodels/NAM_CONUS_80km_20051206_0000.nc";
        final String fileOut = "C:/temp/cf3.nc";
        final GridDataset gds = ucar.nc2.dt.grid.GridDataset.open(fileIn);
        final NetcdfCFWriter writer = new NetcdfCFWriter();
        final List<String> gridList = new ArrayList<String>();
        gridList.add("RH");
        gridList.add("T");
        final DateFormatter format = new DateFormatter();
        final Date start = format.isoDateTimeFormat("2005-12-06T18:00:00Z");
        final Date end = format.isoDateTimeFormat("2005-12-07T18:00:00Z");
        writer.makeFile(fileOut, gds, gridList, new LatLonRect(new LatLonPointImpl(37.0, -109.0), 400.0, 7.0), new DateRange(start, end), true, 1, 1, 1);
    }
    
    public static void main(final String[] args) throws IOException, InvalidRangeException, ParseException {
        final String fileIn = "dods://motherlode.ucar.edu/repository/entry/show/output:data.opendap/entryid:c41a3a26-57e5-4b15-b8b1-a8762b6f02c7/dodsC/entry";
        final String fileOut = "C:/temp/testCF.nc";
        final GridDataset gds = ucar.nc2.dt.grid.GridDataset.open(fileIn);
        final NetcdfCFWriter writer = new NetcdfCFWriter();
        final List<String> gridList = new ArrayList<String>();
        gridList.add("Z_sfc");
        final DateFormatter format = new DateFormatter();
        final Date start = format.isoDateTimeFormat("2003-06-01T03:00:00Z");
        final Date end = format.isoDateTimeFormat("2004-01-01T00:00:00Z");
        writer.makeFile(fileOut, gds, gridList, null, new DateRange(start, end), true, 1, 1, 1);
    }
    
    static {
        NetcdfCFWriter.maxSize = 2000000000L;
        NetcdfCFWriter.log = LoggerFactory.getLogger(NetcdfCFWriter.class);
    }
}
