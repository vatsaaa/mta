// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.grid;

import org.slf4j.LoggerFactory;
import ucar.unidata.util.Format;
import ucar.nc2.dataset.ProjectionCT;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.nc2.dataset.CoordTransBuilder;
import ucar.nc2.FileWriter;
import ucar.ma2.Section;
import java.util.Arrays;
import ucar.ma2.InvalidRangeException;
import ucar.unidata.geoloc.LatLonRect;
import ucar.ma2.Range;
import java.util.Iterator;
import java.io.IOException;
import ucar.ma2.IndexIterator;
import ucar.ma2.MAMath;
import ucar.ma2.Array;
import ucar.unidata.geoloc.ProjectionImpl;
import ucar.nc2.dt.GridCoordSystem;
import ucar.ma2.DataType;
import ucar.nc2.Variable;
import ucar.nc2.Attribute;
import java.util.Collection;
import ucar.nc2.dataset.CoordinateAxis;
import java.util.ArrayList;
import ucar.nc2.dataset.CoordinateAxis1D;
import ucar.nc2.Dimension;
import java.util.List;
import ucar.nc2.dataset.VariableDS;
import org.slf4j.Logger;
import ucar.nc2.dt.GridDatatype;
import ucar.nc2.util.NamedObject;

public class GeoGrid implements NamedObject, GridDatatype
{
    private static Logger log;
    private GridDataset dataset;
    private GridCoordSys gcs;
    private VariableDS vs;
    private int xDimOrgIndex;
    private int yDimOrgIndex;
    private int zDimOrgIndex;
    private int tDimOrgIndex;
    private int eDimOrgIndex;
    private int rtDimOrgIndex;
    private int xDimNewIndex;
    private int yDimNewIndex;
    private int zDimNewIndex;
    private int tDimNewIndex;
    private int eDimNewIndex;
    private int rtDimNewIndex;
    private List<Dimension> mydims;
    private boolean debugArrayShape;
    private int hashCode;
    
    public GeoGrid(final GridDataset dataset, final VariableDS dsvar, final GridCoordSys gcs) {
        this.xDimOrgIndex = -1;
        this.yDimOrgIndex = -1;
        this.zDimOrgIndex = -1;
        this.tDimOrgIndex = -1;
        this.eDimOrgIndex = -1;
        this.rtDimOrgIndex = -1;
        this.xDimNewIndex = -1;
        this.yDimNewIndex = -1;
        this.zDimNewIndex = -1;
        this.tDimNewIndex = -1;
        this.eDimNewIndex = -1;
        this.rtDimNewIndex = -1;
        this.debugArrayShape = false;
        this.hashCode = 0;
        this.dataset = dataset;
        this.vs = dsvar;
        this.gcs = gcs;
        final CoordinateAxis xaxis = gcs.getXHorizAxis();
        if (xaxis instanceof CoordinateAxis1D) {
            this.xDimOrgIndex = this.findDimension(gcs.getXHorizAxis().getDimension(0));
            this.yDimOrgIndex = this.findDimension(gcs.getYHorizAxis().getDimension(0));
        }
        else {
            this.yDimOrgIndex = this.findDimension(gcs.getXHorizAxis().getDimension(0));
            this.xDimOrgIndex = this.findDimension(gcs.getXHorizAxis().getDimension(1));
        }
        if (gcs.getVerticalAxis() != null) {
            this.zDimOrgIndex = this.findDimension(gcs.getVerticalAxis().getDimension(0));
        }
        if (gcs.getTimeAxis() != null) {
            if (gcs.getTimeAxis1D() != null) {
                this.tDimOrgIndex = this.findDimension(gcs.getTimeAxis1D().getDimension(0));
            }
            else {
                this.tDimOrgIndex = this.findDimension(gcs.getTimeAxis().getDimension(1));
            }
        }
        if (gcs.getEnsembleAxis() != null) {
            this.eDimOrgIndex = this.findDimension(gcs.getEnsembleAxis().getDimension(0));
        }
        if (gcs.getRunTimeAxis() != null) {
            this.rtDimOrgIndex = this.findDimension(gcs.getRunTimeAxis().getDimension(0));
        }
        int count = 0;
        this.mydims = new ArrayList<Dimension>();
        if (this.rtDimOrgIndex >= 0 && this.rtDimOrgIndex != this.tDimOrgIndex) {
            this.mydims.add(dsvar.getDimension(this.rtDimOrgIndex));
            this.rtDimNewIndex = count++;
        }
        if (this.eDimOrgIndex >= 0) {
            this.mydims.add(dsvar.getDimension(this.eDimOrgIndex));
            this.eDimNewIndex = count++;
        }
        if (this.tDimOrgIndex >= 0) {
            this.mydims.add(dsvar.getDimension(this.tDimOrgIndex));
            this.tDimNewIndex = count++;
        }
        if (this.zDimOrgIndex >= 0) {
            this.mydims.add(dsvar.getDimension(this.zDimOrgIndex));
            this.zDimNewIndex = count++;
        }
        if (this.yDimOrgIndex >= 0) {
            this.mydims.add(dsvar.getDimension(this.yDimOrgIndex));
            this.yDimNewIndex = count++;
        }
        if (this.xDimOrgIndex >= 0) {
            this.mydims.add(dsvar.getDimension(this.xDimOrgIndex));
            this.xDimNewIndex = count;
        }
    }
    
    private int findDimension(final Dimension want) {
        final List dims = this.vs.getDimensions();
        for (int i = 0; i < dims.size(); ++i) {
            final Dimension d = dims.get(i);
            if (d.equals(want)) {
                return i;
            }
        }
        return -1;
    }
    
    public List<Dimension> getDimensions() {
        return new ArrayList<Dimension>(this.mydims);
    }
    
    public Dimension getDimension(final int i) {
        if (i < 0 || i >= this.mydims.size()) {
            return null;
        }
        return this.mydims.get(i);
    }
    
    public Dimension getTimeDimension() {
        return (this.tDimNewIndex < 0) ? null : this.getDimension(this.tDimNewIndex);
    }
    
    public Dimension getZDimension() {
        return (this.zDimNewIndex < 0) ? null : this.getDimension(this.zDimNewIndex);
    }
    
    public Dimension getYDimension() {
        return (this.yDimNewIndex < 0) ? null : this.getDimension(this.yDimNewIndex);
    }
    
    public Dimension getXDimension() {
        return (this.xDimNewIndex < 0) ? null : this.getDimension(this.xDimNewIndex);
    }
    
    public Dimension getEnsembleDimension() {
        return (this.eDimNewIndex < 0) ? null : this.getDimension(this.eDimNewIndex);
    }
    
    public Dimension getRunTimeDimension() {
        return (this.rtDimNewIndex < 0) ? null : this.getDimension(this.rtDimNewIndex);
    }
    
    public int getTimeDimensionIndex() {
        return this.tDimNewIndex;
    }
    
    public int getZDimensionIndex() {
        return this.zDimNewIndex;
    }
    
    public int getYDimensionIndex() {
        return this.yDimNewIndex;
    }
    
    public int getXDimensionIndex() {
        return this.xDimNewIndex;
    }
    
    public int getEnsembleDimensionIndex() {
        return this.eDimNewIndex;
    }
    
    public int getRunTimeDimensionIndex() {
        return this.rtDimNewIndex;
    }
    
    public Attribute findAttributeIgnoreCase(final String name) {
        return this.vs.findAttributeIgnoreCase(name);
    }
    
    public String findAttValueIgnoreCase(final String attName, final String defaultValue) {
        return this.dataset.getNetcdfDataset().findAttValueIgnoreCase(this.vs, attName, defaultValue);
    }
    
    public int getRank() {
        return this.vs.getRank();
    }
    
    public int[] getShape() {
        final int[] shape = new int[this.mydims.size()];
        for (int i = 0; i < this.mydims.size(); ++i) {
            final Dimension d = this.mydims.get(i);
            shape[i] = d.getLength();
        }
        return shape;
    }
    
    public DataType getDataType() {
        return this.vs.getDataType();
    }
    
    public List<Attribute> getAttributes() {
        return this.vs.getAttributes();
    }
    
    public VariableDS getVariable() {
        return this.vs;
    }
    
    public String getName() {
        return this.vs.getName();
    }
    
    public String getNameEscaped() {
        return this.vs.getNameEscaped();
    }
    
    public GridCoordSystem getCoordinateSystem() {
        return this.gcs;
    }
    
    public ProjectionImpl getProjection() {
        return this.gcs.getProjection();
    }
    
    public List<NamedObject> getLevels() {
        return this.gcs.getLevels();
    }
    
    public List<NamedObject> getTimes() {
        return this.gcs.getTimes();
    }
    
    public String getDescription() {
        return this.vs.getDescription();
    }
    
    public String getUnitsString() {
        final String units = this.vs.getUnitsString();
        return (units == null) ? "" : units;
    }
    
    @Deprecated
    public String getUnitString() {
        return this.getUnitsString();
    }
    
    public boolean hasMissingData() {
        return this.vs.hasMissing();
    }
    
    public boolean isMissingData(final double val) {
        return this.vs.isMissing(val);
    }
    
    public float[] setMissingToNaN(final float[] values) {
        if (!this.vs.hasMissing()) {
            return values;
        }
        for (int length = values.length, i = 0; i < length; ++i) {
            final double value = values[i];
            if (this.vs.isMissing(value)) {
                values[i] = Float.NaN;
            }
        }
        return values;
    }
    
    public MAMath.MinMax getMinMaxSkipMissingData(final Array a) {
        if (!this.hasMissingData()) {
            return MAMath.getMinMax(a);
        }
        final IndexIterator iter = a.getIndexIterator();
        double max = -1.7976931348623157E308;
        double min = Double.MAX_VALUE;
        while (iter.hasNext()) {
            final double val = iter.getDoubleNext();
            if (this.isMissingData(val)) {
                continue;
            }
            if (val > max) {
                max = val;
            }
            if (val >= min) {
                continue;
            }
            min = val;
        }
        return new MAMath.MinMax(min, max);
    }
    
    public Array readVolumeData(final int t) throws IOException {
        return this.readDataSlice(t, -1, -1, -1);
    }
    
    public Array readYXData(final int t, final int z) throws IOException {
        return this.readDataSlice(t, z, -1, -1);
    }
    
    public Array readZYData(final int t, final int x) throws IOException {
        return this.readDataSlice(t, -1, -1, x);
    }
    
    @Deprecated
    public Array getDataSlice(final int t, final int z, final int y, final int x) throws IOException {
        return this.readDataSlice(t, z, y, x);
    }
    
    public Array readDataSlice(final int t, final int z, final int y, final int x) throws IOException {
        return this.readDataSlice(0, 0, t, z, y, x);
    }
    
    public Array readDataSlice(final int rt, final int e, final int t, final int z, final int y, final int x) throws IOException {
        final int rank = this.vs.getRank();
        final int[] start = new int[rank];
        final int[] shape = new int[rank];
        for (int i = 0; i < rank; ++i) {
            start[i] = 0;
            shape[i] = 1;
        }
        final Dimension xdim = this.getXDimension();
        final Dimension ydim = this.getYDimension();
        final Dimension zdim = this.getZDimension();
        final Dimension tdim = this.getTimeDimension();
        final Dimension edim = this.getEnsembleDimension();
        final Dimension rtdim = this.getRunTimeDimension();
        if (rtdim != null) {
            if (rt >= 0 && rt < rtdim.getLength()) {
                start[this.rtDimOrgIndex] = rt;
            }
            else {
                shape[this.rtDimOrgIndex] = rtdim.getLength();
            }
        }
        if (edim != null) {
            if (e >= 0 && e < edim.getLength()) {
                start[this.eDimOrgIndex] = e;
            }
            else {
                shape[this.eDimOrgIndex] = edim.getLength();
            }
        }
        if (tdim != null) {
            if (t >= 0 && t < tdim.getLength()) {
                start[this.tDimOrgIndex] = t;
            }
            else {
                shape[this.tDimOrgIndex] = tdim.getLength();
            }
        }
        if (zdim != null) {
            if (z >= 0 && z < zdim.getLength()) {
                start[this.zDimOrgIndex] = z;
            }
            else {
                shape[this.zDimOrgIndex] = zdim.getLength();
            }
        }
        if (ydim != null) {
            if (y >= 0 && y < ydim.getLength()) {
                start[this.yDimOrgIndex] = y;
            }
            else {
                shape[this.yDimOrgIndex] = ydim.getLength();
            }
        }
        if (xdim != null) {
            if (x >= 0 && x < xdim.getLength()) {
                start[this.xDimOrgIndex] = x;
            }
            else {
                shape[this.xDimOrgIndex] = xdim.getLength();
            }
        }
        if (this.debugArrayShape) {
            System.out.println("read shape from org variable = ");
            for (int j = 0; j < rank; ++j) {
                System.out.println("   start = " + start[j] + " shape = " + shape[j] + " name = " + this.vs.getDimension(j).getName());
            }
        }
        Array dataVolume;
        try {
            dataVolume = this.vs.read(start, shape);
        }
        catch (Exception ex) {
            GeoGrid.log.error("GeoGrid.getdataSlice() on dataset " + this.getName() + " " + this.dataset.getLocation(), ex);
            throw new IOException(ex.getMessage());
        }
        final List<Dimension> oldDims = new ArrayList<Dimension>(this.vs.getDimensions());
        final int[] permuteIndex = new int[dataVolume.getRank()];
        int count = 0;
        if (oldDims.contains(rtdim)) {
            permuteIndex[count++] = oldDims.indexOf(rtdim);
        }
        if (oldDims.contains(edim)) {
            permuteIndex[count++] = oldDims.indexOf(edim);
        }
        if (oldDims.contains(tdim)) {
            permuteIndex[count++] = oldDims.indexOf(tdim);
        }
        if (oldDims.contains(zdim)) {
            permuteIndex[count++] = oldDims.indexOf(zdim);
        }
        if (oldDims.contains(ydim)) {
            permuteIndex[count++] = oldDims.indexOf(ydim);
        }
        if (oldDims.contains(xdim)) {
            permuteIndex[count] = oldDims.indexOf(xdim);
        }
        if (this.debugArrayShape) {
            System.out.println("oldDims = ");
            for (final Dimension oldDim : oldDims) {
                System.out.println("   oldDim = " + oldDim.getName());
            }
            System.out.println("permute dims = ");
            for (final int aPermuteIndex : permuteIndex) {
                System.out.println("   oldDim index = " + aPermuteIndex);
            }
        }
        boolean needPermute = false;
        for (int k = 0; k < permuteIndex.length; ++k) {
            if (k != permuteIndex[k]) {
                needPermute = true;
            }
        }
        if (needPermute) {
            dataVolume = dataVolume.permute(permuteIndex);
        }
        count = 0;
        if (rtdim != null) {
            if (rt >= 0) {
                dataVolume = dataVolume.reduce(count);
            }
            else {
                ++count;
            }
        }
        if (edim != null) {
            if (e >= 0) {
                dataVolume = dataVolume.reduce(count);
            }
            else {
                ++count;
            }
        }
        if (tdim != null) {
            if (t >= 0) {
                dataVolume = dataVolume.reduce(count);
            }
            else {
                ++count;
            }
        }
        if (zdim != null) {
            if (z >= 0) {
                dataVolume = dataVolume.reduce(count);
            }
            else {
                ++count;
            }
        }
        if (ydim != null) {
            if (y >= 0) {
                dataVolume = dataVolume.reduce(count);
            }
            else {
                ++count;
            }
        }
        if (xdim != null && x >= 0) {
            dataVolume = dataVolume.reduce(count);
        }
        return dataVolume;
    }
    
    public GeoGrid subset(final Range t_range, Range z_range, final LatLonRect bbox, final int z_stride, final int y_stride, final int x_stride) throws InvalidRangeException {
        if (z_range == null && z_stride > 1) {
            final Dimension zdim = this.getZDimension();
            if (zdim != null) {
                z_range = new Range(0, zdim.getLength() - 1, z_stride);
            }
        }
        Range y_range = null;
        Range x_range = null;
        if (bbox != null) {
            final List yx_ranges = this.gcs.getRangesFromLatLonRect(bbox);
            y_range = yx_ranges.get(0);
            x_range = yx_ranges.get(1);
        }
        if (y_stride > 1) {
            if (y_range == null) {
                final Dimension ydim = this.getYDimension();
                y_range = new Range(0, ydim.getLength() - 1, y_stride);
            }
            else {
                y_range = new Range(y_range.first(), y_range.last(), y_stride);
            }
        }
        if (x_stride > 1) {
            if (x_range == null) {
                final Dimension xdim = this.getXDimension();
                x_range = new Range(0, xdim.getLength() - 1, x_stride);
            }
            else {
                x_range = new Range(x_range.first(), x_range.last(), x_stride);
            }
        }
        return this.subset(t_range, z_range, y_range, x_range);
    }
    
    public GridDatatype makeSubset(final Range t_range, final Range z_range, final LatLonRect bbox, final int z_stride, final int y_stride, final int x_stride) throws InvalidRangeException {
        return this.subset(t_range, z_range, bbox, z_stride, y_stride, x_stride);
    }
    
    public GeoGrid subset(final Range t_range, final Range z_range, final Range y_range, final Range x_range) throws InvalidRangeException {
        return (GeoGrid)this.makeSubset(null, null, t_range, z_range, y_range, x_range);
    }
    
    public GridDatatype makeSubset(final Range rt_range, final Range e_range, final Range t_range, final Range z_range, final Range y_range, final Range x_range) throws InvalidRangeException {
        final int rank = this.getRank();
        final Range[] ranges = new Range[rank];
        if (null != this.getXDimension()) {
            ranges[this.xDimOrgIndex] = x_range;
        }
        if (null != this.getYDimension()) {
            ranges[this.yDimOrgIndex] = y_range;
        }
        if (null != this.getZDimension()) {
            ranges[this.zDimOrgIndex] = z_range;
        }
        if (null != this.getTimeDimension()) {
            ranges[this.tDimOrgIndex] = t_range;
        }
        if (null != this.getRunTimeDimension()) {
            ranges[this.rtDimOrgIndex] = rt_range;
        }
        if (null != this.getEnsembleDimension()) {
            ranges[this.eDimOrgIndex] = e_range;
        }
        final List<Range> rangesList = Arrays.asList(ranges);
        final VariableDS v_section = (VariableDS)this.vs.section(new Section(rangesList));
        final List<Dimension> dims = v_section.getDimensions();
        for (final Dimension dim : dims) {
            dim.setShared(true);
        }
        final GridCoordSys gcs_section = new GridCoordSys(this.gcs, rt_range, e_range, t_range, z_range, y_range, x_range);
        return new GeoGrid(this.dataset, v_section, gcs_section);
    }
    
    public void writeFile(final String filename) throws IOException {
        final FileWriter writer = new FileWriter(filename, true);
        final List<Variable> varList = new ArrayList<Variable>();
        varList.add(this.vs);
        for (final CoordinateAxis axis : this.gcs.getCoordinateAxes()) {
            varList.add(axis);
        }
        writer.writeVariables(varList);
        final ProjectionCT projCT = this.gcs.getProjectionCT();
        if (projCT != null) {
            final VariableDS v = CoordTransBuilder.makeDummyTransformVariable(this.dataset.getNetcdfDataset(), projCT);
            v.addAttribute(new Attribute("_CoordinateAxisTypes", "GeoX GeoY"));
            writer.writeVariable(v);
        }
        final String location = this.dataset.getNetcdfDataset().getLocation();
        writer.writeGlobalAttribute(new Attribute("History", "GeoGrid extracted from dataset " + location));
        writer.writeGlobalAttribute(new Attribute("Convention", "_Coordinates"));
        writer.finish();
    }
    
    @Override
    public boolean equals(final Object oo) {
        if (this == oo) {
            return true;
        }
        if (!(oo instanceof GeoGrid)) {
            return false;
        }
        final GeoGrid d = (GeoGrid)oo;
        return this.getName().equals(d.getName()) && this.getCoordinateSystem().equals(d.getCoordinateSystem());
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int result = 17;
            result = 37 * result + this.getName().hashCode();
            result = 37 * result + this.getCoordinateSystem().hashCode();
            this.hashCode = result;
        }
        return this.hashCode;
    }
    
    @Override
    public String toString() {
        return this.getName();
    }
    
    public String getInfo() {
        final StringBuilder buf = new StringBuilder(200);
        buf.setLength(0);
        buf.append(this.getName());
        Format.tab(buf, 30, true);
        buf.append(this.getUnitsString());
        Format.tab(buf, 60, true);
        buf.append(this.hasMissingData());
        Format.tab(buf, 66, true);
        buf.append(this.getDescription());
        return buf.toString();
    }
    
    public int compareTo(final GridDatatype g) {
        return this.getName().compareTo(g.getName());
    }
    
    static {
        GeoGrid.log = LoggerFactory.getLogger(GeoGrid.class);
    }
}
