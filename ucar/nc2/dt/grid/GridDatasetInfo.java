// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.grid;

import java.io.FileOutputStream;
import ucar.nc2.Dimension;
import ucar.unidata.util.Parameter;
import ucar.unidata.geoloc.LatLonPoint;
import java.util.Set;
import java.util.Arrays;
import java.util.HashSet;
import ucar.nc2.constants.AxisType;
import ucar.ma2.DataType;
import ucar.nc2.dataset.VariableEnhanced;
import org.jdom.Namespace;
import ucar.nc2.ncml.NcMLWriter;
import ucar.nc2.Attribute;
import ucar.nc2.dt.GridCoordSystem;
import ucar.nc2.dt.GridDatatype;
import java.util.Date;
import ucar.unidata.geoloc.LatLonRect;
import java.util.List;
import java.util.Iterator;
import ucar.nc2.units.DateFormatter;
import ucar.nc2.dataset.CoordinateTransform;
import java.util.Comparator;
import java.util.Collections;
import org.jdom.Content;
import ucar.nc2.dataset.CoordinateAxis;
import org.jdom.Element;
import java.io.IOException;
import java.io.OutputStream;
import org.jdom.output.XMLOutputter;
import org.jdom.output.Format;
import org.jdom.Document;
import ucar.nc2.dt.GridDataset;

public class GridDatasetInfo
{
    private GridDataset gds;
    private String path;
    
    public GridDatasetInfo(final GridDataset gds, final String path) {
        this.gds = gds;
        this.path = path;
    }
    
    public String writeXML(final Document doc) {
        final XMLOutputter fmt = new XMLOutputter(Format.getPrettyFormat());
        return fmt.outputString(doc);
    }
    
    public void writeXML(final Document doc, final OutputStream os) throws IOException {
        final XMLOutputter fmt = new XMLOutputter(Format.getPrettyFormat());
        fmt.output(doc, os);
    }
    
    public Document makeDatasetDescription() {
        final Element rootElem = new Element("gridDataset");
        final Document doc = new Document(rootElem);
        rootElem.setAttribute("location", this.gds.getLocationURI());
        if (null != this.path) {
            rootElem.setAttribute("path", this.path);
        }
        for (final CoordinateAxis axis : this.getCoordAxes(this.gds)) {
            rootElem.addContent(this.writeAxis(axis));
        }
        final List<GridDataset.Gridset> gridSets = this.gds.getGridsets();
        Collections.sort(gridSets, new GridSetComparator());
        for (final GridDataset.Gridset gridset : gridSets) {
            rootElem.addContent(this.writeGridSet(gridset));
        }
        for (final CoordinateTransform ct : this.getCoordTransforms(this.gds)) {
            rootElem.addContent(this.writeCoordTransform(ct));
        }
        final LatLonRect bb = this.gds.getBoundingBox();
        if (bb != null) {
            rootElem.addContent(this.writeBoundingBox(bb));
        }
        final Date start = this.gds.getStartDate();
        final Date end = this.gds.getEndDate();
        if (start != null && end != null) {
            final DateFormatter format = new DateFormatter();
            final Element dateRange = new Element("TimeSpan");
            dateRange.addContent(new Element("begin").addContent(format.toDateTimeStringISO(start)));
            dateRange.addContent(new Element("end").addContent(format.toDateTimeStringISO(end)));
            rootElem.addContent(dateRange);
        }
        final Element elem = new Element("AcceptList");
        elem.addContent(new Element("accept").addContent("xml"));
        elem.addContent(new Element("accept").addContent("csv"));
        elem.addContent(new Element("accept").addContent("netcdf"));
        rootElem.addContent(elem);
        return doc;
    }
    
    public Document makeGridForm() {
        final Element rootElem = new Element("gridForm");
        final Document doc = new Document(rootElem);
        rootElem.setAttribute("location", this.gds.getLocationURI());
        if (null != this.path) {
            rootElem.setAttribute("path", this.path);
        }
        final List<GridDatatype> grids = this.gds.getGrids();
        Collections.sort(grids, new GridComparator());
        CoordinateAxis currentTime = null;
        CoordinateAxis currentVert = null;
        Element timeElem = null;
        Element vertElem = null;
        for (int i = 0; i < grids.size(); ++i) {
            final GeoGrid grid = grids.get(i);
            final GridCoordSystem gcs = grid.getCoordinateSystem();
            final CoordinateAxis time = gcs.getTimeAxis();
            final CoordinateAxis vert = gcs.getVerticalAxis();
            boolean newTime;
            if (i == 0 || !this.compareAxis(time, currentTime)) {
                timeElem = new Element("timeSet");
                rootElem.addContent(timeElem);
                final Element timeAxisElement = this.writeAxis2(time, "time");
                if (timeAxisElement != null) {
                    timeElem.addContent(timeAxisElement);
                }
                currentTime = time;
                newTime = true;
            }
            else {
                newTime = false;
            }
            if (newTime || !this.compareAxis(vert, currentVert)) {
                vertElem = new Element("vertSet");
                timeElem.addContent(vertElem);
                final Element vertAxisElement = this.writeAxis2(vert, "vert");
                if (vertAxisElement != null) {
                    vertElem.addContent(vertAxisElement);
                }
                currentVert = vert;
            }
            vertElem.addContent(this.writeGrid(grid));
        }
        final LatLonRect bb = this.gds.getBoundingBox();
        if (bb != null) {
            rootElem.addContent(this.writeBoundingBox(bb));
        }
        final Date start = this.gds.getStartDate();
        final Date end = this.gds.getEndDate();
        if (start != null && end != null) {
            final DateFormatter format = new DateFormatter();
            final Element dateRange = new Element("TimeSpan");
            dateRange.addContent(new Element("begin").addContent(format.toDateTimeStringISO(start)));
            dateRange.addContent(new Element("end").addContent(format.toDateTimeStringISO(end)));
            rootElem.addContent(dateRange);
        }
        final Element elem = new Element("AcceptList");
        elem.addContent(new Element("accept").addContent("xml"));
        elem.addContent(new Element("accept").addContent("csv"));
        elem.addContent(new Element("accept").addContent("netcdf"));
        rootElem.addContent(elem);
        return doc;
    }
    
    private Element writeAxis2(final CoordinateAxis axis, final String name) {
        if (axis == null) {
            return null;
        }
        final Element varElem = new Element(name);
        varElem.setAttribute("name", axis.getName());
        varElem.setAttribute("shape", this.getShapeString(axis.getShape()));
        final DataType dt = axis.getDataType();
        varElem.setAttribute("type", dt.toString());
        final AxisType axisType = axis.getAxisType();
        if (null != axisType) {
            varElem.setAttribute("axisType", axisType.toString());
        }
        for (final Attribute att : axis.getAttributes()) {
            varElem.addContent(NcMLWriter.writeAttribute(att, "attribute", null));
        }
        final Element values = NcMLWriter.writeValues(axis, null, false);
        values.setAttribute("npts", Long.toString(axis.getSize()));
        varElem.addContent(values);
        return varElem;
    }
    
    private boolean compareAxis(final CoordinateAxis axis1, final CoordinateAxis axis2) {
        return axis1 == axis2 || (axis1 != null && axis2 != null && axis1.equals(axis2));
    }
    
    private List<CoordinateAxis> getCoordAxes(final GridDataset gds) {
        final Set<CoordinateAxis> axesHash = new HashSet<CoordinateAxis>();
        for (final GridDataset.Gridset gridset : gds.getGridsets()) {
            final GridCoordSystem gcs = gridset.getGeoCoordSystem();
            for (final CoordinateAxis axe : gcs.getCoordinateAxes()) {
                axesHash.add(axe);
            }
        }
        final List<CoordinateAxis> list = Arrays.asList((CoordinateAxis[])axesHash.toArray((T[])new CoordinateAxis[axesHash.size()]));
        Collections.sort(list);
        return list;
    }
    
    private List<CoordinateTransform> getCoordTransforms(final GridDataset gds) {
        final Set<CoordinateTransform> ctHash = new HashSet<CoordinateTransform>();
        for (final GridDataset.Gridset gridset : gds.getGridsets()) {
            final GridCoordSystem gcs = gridset.getGeoCoordSystem();
            for (final CoordinateTransform axe : gcs.getCoordinateTransforms()) {
                ctHash.add(axe);
            }
        }
        final List<CoordinateTransform> list = Arrays.asList((CoordinateTransform[])ctHash.toArray((T[])new CoordinateTransform[ctHash.size()]));
        Collections.sort(list);
        return list;
    }
    
    private Element writeAxis(final CoordinateAxis axis) {
        final Element varElem = new Element("axis");
        varElem.setAttribute("name", axis.getName());
        varElem.setAttribute("shape", this.getShapeString(axis.getShape()));
        final DataType dt = axis.getDataType();
        varElem.setAttribute("type", dt.toString());
        final AxisType axisType = axis.getAxisType();
        if (null != axisType) {
            varElem.setAttribute("axisType", axisType.toString());
        }
        for (final Attribute att : axis.getAttributes()) {
            varElem.addContent(NcMLWriter.writeAttribute(att, "attribute", null));
        }
        if (axis.getRank() == 1) {
            final Element values = NcMLWriter.writeValues(axis, null, true);
            varElem.addContent(values);
        }
        return varElem;
    }
    
    private String getShapeString(final int[] shape) {
        final StringBuilder buf = new StringBuilder();
        for (int i = 0; i < shape.length; ++i) {
            if (i != 0) {
                buf.append(" ");
            }
            buf.append(shape[i]);
        }
        return buf.toString();
    }
    
    private Element writeBoundingBox(final LatLonRect bb) {
        final Element bbElem = new Element("LatLonBox");
        final LatLonPoint llpt = bb.getLowerLeftPoint();
        final LatLonPoint urpt = bb.getUpperRightPoint();
        bbElem.addContent(new Element("west").addContent(ucar.unidata.util.Format.dfrac(llpt.getLongitude(), 4)));
        bbElem.addContent(new Element("east").addContent(ucar.unidata.util.Format.dfrac(urpt.getLongitude(), 4)));
        bbElem.addContent(new Element("south").addContent(ucar.unidata.util.Format.dfrac(llpt.getLatitude(), 4)));
        bbElem.addContent(new Element("north").addContent(ucar.unidata.util.Format.dfrac(urpt.getLatitude(), 4)));
        return bbElem;
    }
    
    private Element writeGridSet(final GridDataset.Gridset gridset) {
        final Element csElem = new Element("gridSet");
        final GridCoordSystem cs = gridset.getGeoCoordSystem();
        csElem.setAttribute("name", cs.getName());
        for (final CoordinateAxis axis : cs.getCoordinateAxes()) {
            final Element axisElem = new Element("axisRef");
            axisElem.setAttribute("name", axis.getName());
            csElem.addContent(axisElem);
        }
        for (final CoordinateTransform ct : cs.getCoordinateTransforms()) {
            final Element elem = new Element("coordTransRef");
            elem.setAttribute("name", ct.getName());
            csElem.addContent(elem);
        }
        final List<GridDatatype> grids = gridset.getGrids();
        Collections.sort(grids);
        for (final GridDatatype grid : grids) {
            csElem.addContent(this.writeGrid(grid));
        }
        return csElem;
    }
    
    private Element writeCoordTransform(final CoordinateTransform ct) {
        final Element ctElem = new Element("coordTransform");
        ctElem.setAttribute("name", ct.getName());
        ctElem.setAttribute("transformType", ct.getTransformType().toString());
        for (final Parameter param : ct.getParameters()) {
            final Element pElem = new Element("parameter");
            pElem.setAttribute("name", param.getName());
            pElem.setAttribute("value", param.getStringValue());
            ctElem.addContent(pElem);
        }
        return ctElem;
    }
    
    private Element writeGrid(final GridDatatype grid) {
        final Element varElem = new Element("grid");
        varElem.setAttribute("name", grid.getName());
        final StringBuilder buff = new StringBuilder();
        final List dims = grid.getDimensions();
        for (int i = 0; i < dims.size(); ++i) {
            final Dimension dim = dims.get(i);
            if (i > 0) {
                buff.append(" ");
            }
            if (dim.isShared()) {
                buff.append(dim.getName());
            }
            else {
                buff.append(dim.getLength());
            }
        }
        if (buff.length() > 0) {
            varElem.setAttribute("shape", buff.toString());
        }
        final DataType dt = grid.getDataType();
        if (dt != null) {
            varElem.setAttribute("type", dt.toString());
        }
        for (final Attribute att : grid.getAttributes()) {
            varElem.addContent(NcMLWriter.writeAttribute(att, "attribute", null));
        }
        return varElem;
    }
    
    public static void main(final String[] args) throws IOException {
        final String url = "C:/data/NAM_CONUS_12km_20060227_1200.grib2";
        final GridDataset ncd = ucar.nc2.dt.grid.GridDataset.open(url);
        final GridDatasetInfo info = new GridDatasetInfo(ncd, null);
        final FileOutputStream fos2 = new FileOutputStream("C:/TEMP/gridInfo.xml");
        info.writeXML(info.makeGridForm(), fos2);
        fos2.close();
        final String infoString = info.writeXML(info.makeGridForm());
        System.out.println(infoString);
    }
    
    private class GridComparator implements Comparator<GridDatatype>
    {
        public int compare(final GridDatatype grid1, final GridDatatype grid2) {
            final GridCoordSystem gcs1 = grid1.getCoordinateSystem();
            final GridCoordSystem gcs2 = grid2.getCoordinateSystem();
            final CoordinateAxis time1 = gcs1.getTimeAxis();
            final CoordinateAxis time2 = gcs2.getTimeAxis();
            int ret = this.compareAxis(time1, time2);
            if (ret != 0) {
                return ret;
            }
            final CoordinateAxis vert1 = gcs1.getVerticalAxis();
            final CoordinateAxis vert2 = gcs2.getVerticalAxis();
            ret = this.compareAxis(vert1, vert2);
            if (ret != 0) {
                return ret;
            }
            return grid1.getName().compareTo(grid2.getName());
        }
        
        private int compareAxis(final CoordinateAxis axis1, final CoordinateAxis axis2) {
            if (axis1 == axis2) {
                return 0;
            }
            if (axis1 == null) {
                return -1;
            }
            if (axis2 == null) {
                return 1;
            }
            return axis1.getName().compareTo(axis2.getName());
        }
    }
    
    private class GridSetComparator implements Comparator<GridDataset.Gridset>
    {
        public int compare(final GridDataset.Gridset gridset1, final GridDataset.Gridset gridset2) {
            final GridCoordSystem cs1 = gridset1.getGeoCoordSystem();
            final GridCoordSystem cs2 = gridset2.getGeoCoordSystem();
            if (cs1.getDomain().size() != cs2.getDomain().size()) {
                return cs1.getDomain().size() - cs2.getDomain().size();
            }
            return cs1.getName().compareTo(cs2.getName());
        }
    }
}
