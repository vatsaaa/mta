// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.grid;

import ucar.nc2.util.cache.FileCacheable;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.dataset.CoordinateAxis;
import ucar.nc2.dataset.NetcdfDatasetInfo;
import java.util.Collection;
import ucar.nc2.NetcdfFile;
import ucar.nc2.dt.GridDatatype;
import ucar.nc2.VariableSimpleIF;
import ucar.nc2.Attribute;
import java.util.Date;
import ucar.nc2.dt.GridCoordSystem;
import ucar.nc2.dataset.VariableDS;
import ucar.nc2.dataset.CoordinateSystem;
import ucar.nc2.dataset.StructureDS;
import java.util.Iterator;
import java.util.List;
import ucar.nc2.dataset.VariableEnhanced;
import ucar.nc2.Variable;
import java.util.HashMap;
import java.util.Formatter;
import ucar.nc2.util.CancelTask;
import ucar.nc2.util.cache.FileFactory;
import java.util.Set;
import java.io.IOException;
import ucar.nc2.util.cache.FileCache;
import ucar.nc2.units.DateRange;
import ucar.unidata.geoloc.LatLonRect;
import java.util.Map;
import java.util.ArrayList;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.ft.FeatureDataset;

public class GridDataset implements ucar.nc2.dt.GridDataset, FeatureDataset
{
    private NetcdfDataset ds;
    private ArrayList<GeoGrid> grids;
    private Map<String, Gridset> gridsetHash;
    private LatLonRect llbbMax;
    private DateRange dateRangeMax;
    protected FileCache fileCache;
    
    public static GridDataset open(final String location) throws IOException {
        return open(location, NetcdfDataset.getDefaultEnhanceMode());
    }
    
    public static GridDataset open(final String location, final Set<NetcdfDataset.Enhance> enhanceMode) throws IOException {
        final NetcdfDataset ds = NetcdfDataset.acquireDataset(null, location, enhanceMode, -1, null, null);
        return new GridDataset(ds);
    }
    
    public GridDataset(final NetcdfDataset ds) throws IOException {
        this(ds, null);
    }
    
    public GridDataset(final NetcdfDataset ds, final Formatter parseInfo) throws IOException {
        this.grids = new ArrayList<GeoGrid>();
        this.gridsetHash = new HashMap<String, Gridset>();
        this.llbbMax = null;
        this.dateRangeMax = null;
        (this.ds = ds).enhance(NetcdfDataset.getDefaultEnhanceMode());
        if (parseInfo != null) {
            parseInfo.format("GridDataset look for GeoGrids\n", new Object[0]);
        }
        final List<Variable> vars = ds.getVariables();
        for (final Variable var : vars) {
            final VariableEnhanced varDS = (VariableEnhanced)var;
            this.constructCoordinateSystems(ds, varDS, parseInfo);
        }
    }
    
    private void constructCoordinateSystems(final NetcdfDataset ds, final VariableEnhanced v, final Formatter parseInfo) {
        if (v instanceof StructureDS) {
            final StructureDS s = (StructureDS)v;
            final List<Variable> members = s.getVariables();
            for (final Variable nested : members) {
                this.constructCoordinateSystems(ds, (VariableEnhanced)nested, parseInfo);
            }
        }
        else {
            GridCoordSys gcs = null;
            final List<CoordinateSystem> csys = v.getCoordinateSystems();
            for (final CoordinateSystem cs : csys) {
                final GridCoordSys gcsTry = GridCoordSys.makeGridCoordSys(parseInfo, cs, v);
                if (gcsTry != null) {
                    gcs = gcsTry;
                    if (gcsTry.isProductSet()) {
                        break;
                    }
                    continue;
                }
            }
            if (gcs != null) {
                this.addGeoGrid((VariableDS)v, gcs, parseInfo);
            }
        }
    }
    
    private void makeRanges() {
        for (final ucar.nc2.dt.GridDataset.Gridset gset : this.getGridsets()) {
            final GridCoordSystem gcs = gset.getGeoCoordSystem();
            final LatLonRect llbb = gcs.getLatLonBoundingBox();
            if (this.llbbMax == null) {
                this.llbbMax = llbb;
            }
            else {
                this.llbbMax.extend(llbb);
            }
            final DateRange dateRange = gcs.getDateRange();
            if (dateRange != null) {
                if (this.dateRangeMax == null) {
                    this.dateRangeMax = dateRange;
                }
                else {
                    this.dateRangeMax.extend(dateRange);
                }
            }
        }
    }
    
    public String getTitle() {
        final String title = this.ds.findAttValueIgnoreCase(null, "title", null);
        return (title == null) ? this.getName() : title;
    }
    
    public String getDescription() {
        String desc = this.ds.findAttValueIgnoreCase(null, "description", null);
        if (desc == null) {
            desc = this.ds.findAttValueIgnoreCase(null, "history", null);
        }
        return (desc == null) ? this.getName() : desc;
    }
    
    public String getLocation() {
        return this.ds.getLocation();
    }
    
    public String getLocationURI() {
        return this.ds.getLocation();
    }
    
    public Date getStartDate() {
        if (this.dateRangeMax == null) {
            this.makeRanges();
        }
        return (this.dateRangeMax == null) ? null : this.dateRangeMax.getStart().getDate();
    }
    
    public Date getEndDate() {
        if (this.dateRangeMax == null) {
            this.makeRanges();
        }
        return (this.dateRangeMax == null) ? null : this.dateRangeMax.getEnd().getDate();
    }
    
    public LatLonRect getBoundingBox() {
        if (this.llbbMax == null) {
            this.makeRanges();
        }
        return this.llbbMax;
    }
    
    public void calcBounds() throws IOException {
    }
    
    public List<Attribute> getGlobalAttributes() {
        return this.ds.getGlobalAttributes();
    }
    
    public Attribute findGlobalAttributeIgnoreCase(final String name) {
        return this.ds.findGlobalAttributeIgnoreCase(name);
    }
    
    public List<VariableSimpleIF> getDataVariables() {
        final List<VariableSimpleIF> result = new ArrayList<VariableSimpleIF>(this.grids.size());
        for (final GridDatatype grid : this.getGrids()) {
            if (grid.getVariable() != null) {
                result.add(grid.getVariable());
            }
        }
        return result;
    }
    
    public VariableSimpleIF getDataVariable(final String shortName) {
        return this.ds.findTopVariable(shortName);
    }
    
    public NetcdfFile getNetcdfFile() {
        return this.ds;
    }
    
    private void addGeoGrid(final VariableDS varDS, final GridCoordSys gcs, final Formatter parseInfo) {
        Gridset gridset;
        if (null == (gridset = this.gridsetHash.get(gcs.getName()))) {
            gridset = new Gridset(gcs);
            this.gridsetHash.put(gcs.getName(), gridset);
            if (parseInfo != null) {
                parseInfo.format(" -make new GridCoordSys= %s\n", gcs.getName());
            }
            gcs.makeVerticalTransform(this, parseInfo);
        }
        final GeoGrid geogrid = new GeoGrid(this, varDS, gridset.gcc);
        this.grids.add(geogrid);
        gridset.add(geogrid);
    }
    
    public String getName() {
        return this.ds.getLocation();
    }
    
    public NetcdfDataset getNetcdfDataset() {
        return this.ds;
    }
    
    public List<GridDatatype> getGrids() {
        return new ArrayList<GridDatatype>(this.grids);
    }
    
    public GridDatatype findGridDatatype(final String name) {
        return this.findGridByName(name);
    }
    
    public List<ucar.nc2.dt.GridDataset.Gridset> getGridsets() {
        return new ArrayList<ucar.nc2.dt.GridDataset.Gridset>(this.gridsetHash.values());
    }
    
    public GeoGrid findGridByName(final String name) {
        for (final GeoGrid ggi : this.grids) {
            if (name.equals(ggi.getName())) {
                return ggi;
            }
        }
        return null;
    }
    
    public String getDetailInfo() {
        final Formatter buff = new Formatter();
        this.getDetailInfo(buff);
        return buff.toString();
    }
    
    public void getDetailInfo(final Formatter buff) {
        this.getInfo(buff);
        buff.format("\n\n----------------------------------------------------\n", new Object[0]);
        NetcdfDatasetInfo info = null;
        try {
            info = new NetcdfDatasetInfo(this.ds.getLocation());
            buff.format("%s", info.getParseInfo());
        }
        catch (IOException e) {
            buff.format("NetcdfDatasetInfo failed", new Object[0]);
        }
        finally {
            if (info != null) {
                try {
                    info.close();
                }
                catch (IOException ex) {}
            }
        }
        buff.format("\n\n----------------------------------------------------\n", new Object[0]);
        buff.format("%s", this.ds.toString());
        buff.format("\n\n----------------------------------------------------\n", new Object[0]);
    }
    
    private void getInfo(final Formatter buf) {
        int countGridset = 0;
        for (final Gridset gs : this.gridsetHash.values()) {
            final GridCoordSystem gcs = gs.getGeoCoordSystem();
            buf.format("%nGridset %d  coordSys=%s", countGridset, gcs);
            buf.format(" LLbb=%s ", gcs.getLatLonBoundingBox());
            if (gcs.getProjection() != null && !gcs.getProjection().isLatLon()) {
                buf.format(" bb= %s", gcs.getBoundingBox());
            }
            buf.format("%n", new Object[0]);
            buf.format("Name__________________________Unit__________________________hasMissing_Description%n", new Object[0]);
            for (final GridDatatype grid : gs.getGrids()) {
                buf.format("%s%n", grid.getInfo());
            }
            ++countGridset;
            buf.format("%n", new Object[0]);
        }
        buf.format("\nGeoReferencing Coordinate Axes\n", new Object[0]);
        buf.format("Name__________________________Units_______________Type______Description\n", new Object[0]);
        for (final CoordinateAxis axis : this.ds.getCoordinateAxes()) {
            if (axis.getAxisType() == null) {
                continue;
            }
            axis.getInfo(buf);
            buf.format("\n", new Object[0]);
        }
    }
    
    public FeatureType getFeatureType() {
        return FeatureType.GRID;
    }
    
    public DateRange getDateRange() {
        if (this.dateRangeMax == null) {
            this.makeRanges();
        }
        return this.dateRangeMax;
    }
    
    public String getImplementationName() {
        return this.ds.getConventionUsed();
    }
    
    public synchronized void close() throws IOException {
        if (this.fileCache != null) {
            this.fileCache.release(this);
        }
        else {
            try {
                if (this.ds != null) {
                    this.ds.close();
                }
            }
            finally {
                this.ds = null;
            }
        }
    }
    
    public boolean sync() throws IOException {
        return this.ds != null && this.ds.sync();
    }
    
    public void setFileCache(final FileCache fileCache) {
        this.fileCache = fileCache;
    }
    
    @Deprecated
    public static GridDataset factory(final String netcdfFileURI) throws IOException {
        return open(netcdfFileURI);
    }
    
    private static GridDataset openGridDataset(final String filename) throws IOException {
        final NetcdfFile ncfile = NetcdfDataset.acquireFile(filename, null);
        if (ncfile == null) {
            return null;
        }
        NetcdfDataset ncd;
        if (ncfile instanceof NetcdfDataset) {
            ncd = (NetcdfDataset)ncfile;
            ncd.enhance();
        }
        else {
            ncd = new NetcdfDataset(ncfile, true);
        }
        return new GridDataset(ncd);
    }
    
    public static void main(final String[] arg) {
        final String defaultFilename = "R:/testdata/grid/netcdf/cf/mississippi.nc";
        final String filename = (arg.length > 0) ? arg[0] : defaultFilename;
        try {
            GridDataset gridDs = openGridDataset(filename);
            final String outFilename = "C:/data/writeGrid.nc";
            final GeoGrid gg = gridDs.findGridByName("latent");
            assert gg != null;
            gg.writeFile(outFilename);
            gridDs = open(outFilename);
            System.out.println(gridDs.getDetailInfo());
        }
        catch (Exception ioe) {
            ioe.printStackTrace();
        }
    }
    
    public class Gridset implements ucar.nc2.dt.GridDataset.Gridset
    {
        private GridCoordSys gcc;
        private List<GridDatatype> grids;
        
        private Gridset(final GridCoordSys gcc) {
            this.grids = new ArrayList<GridDatatype>();
            this.gcc = gcc;
        }
        
        private void add(final GeoGrid grid) {
            this.grids.add(grid);
        }
        
        public List<GridDatatype> getGrids() {
            return this.grids;
        }
        
        public GridCoordSystem getGeoCoordSystem() {
            return this.gcc;
        }
        
        @Deprecated
        public GridCoordSys getGeoCoordSys() {
            return this.gcc;
        }
    }
}
