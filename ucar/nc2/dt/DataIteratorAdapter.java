// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt;

import java.io.IOException;
import java.util.Iterator;

public class DataIteratorAdapter implements DataIterator
{
    private Iterator iter;
    
    public DataIteratorAdapter(final Iterator iter) {
        this.iter = iter;
    }
    
    public boolean hasNext() {
        return this.iter.hasNext();
    }
    
    public Object nextData() throws IOException {
        return this.iter.next();
    }
    
    public Object next() {
        return this.iter.next();
    }
    
    public void remove() {
        throw new UnsupportedOperationException();
    }
}
