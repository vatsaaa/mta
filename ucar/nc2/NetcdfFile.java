// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2;

import org.slf4j.LoggerFactory;
import java.util.Formatter;
import ucar.ma2.StructureDataIterator;
import java.nio.channels.WritableByteChannel;
import ucar.nc2.iosp.IospHelper;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Array;
import ucar.ma2.Section;
import java.util.Collections;
import ucar.ma2.DataType;
import java.io.StringWriter;
import java.io.Writer;
import java.io.PrintWriter;
import java.io.OutputStreamWriter;
import java.util.Collection;
import java.util.StringTokenizer;
import ucar.nc2.iosp.netcdf3.SPFactory;
import java.net.URL;
import java.net.URI;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.channels.FileLock;
import java.util.zip.GZIPInputStream;
import ucar.unidata.io.bzip2.CBZip2InputStream;
import java.util.zip.ZipInputStream;
import java.io.OutputStream;
import java.io.InputStream;
import ucar.unidata.io.UncompressInputStream;
import java.io.FileOutputStream;
import java.io.File;
import java.nio.channels.OverlappingFileLockException;
import java.io.FileInputStream;
import ucar.nc2.util.DiskCache;
import ucar.unidata.io.InMemoryRandomAccessFile;
import ucar.nc2.util.IO;
import ucar.unidata.io.http.HTTPRandomAccessFile;
import java.util.Iterator;
import ucar.unidata.io.RandomAccessFile;
import java.io.IOException;
import ucar.nc2.util.CancelTask;
import ucar.nc2.iosp.netcdf3.N3iosp;
import ucar.nc2.iosp.netcdf3.N3header;
import ucar.nc2.util.DebugFlags;
import ucar.unidata.util.StringUtil;
import java.util.List;
import ucar.nc2.util.cache.FileCache;
import ucar.nc2.iosp.IOServiceProvider;
import java.util.ArrayList;
import org.slf4j.Logger;
import ucar.nc2.util.cache.FileCacheable;

public class NetcdfFile implements FileCacheable
{
    public static final String IOSP_MESSAGE_ADD_RECORD_STRUCTURE = "AddRecordStructure";
    public static final String IOSP_MESSAGE_CONVERT_RECORD_STRUCTURE = "ConvertRecordStructure";
    public static final String IOSP_MESSAGE_REMOVE_RECORD_STRUCTURE = "RemoveRecordStructure";
    public static final String IOSP_MESSAGE_RANDOM_ACCESS_FILE = "RandomAccessFile";
    private static Logger log;
    private static int default_buffersize;
    private static ArrayList<IOServiceProvider> registeredProviders;
    protected static boolean debugSPI;
    protected static boolean debugCompress;
    protected static boolean showRequest;
    static boolean debugStructureIterator;
    static boolean loadWarnings;
    private static boolean userLoads;
    public static final String reserved = " .!*'();:@&=+$,/?%#[]";
    protected String location;
    protected String id;
    protected String title;
    protected String cacheName;
    protected Group rootGroup;
    protected boolean unlocked;
    private boolean immutable;
    protected FileCache cache;
    protected IOServiceProvider spi;
    protected List<Variable> variables;
    protected List<Dimension> dimensions;
    protected List<Attribute> gattributes;
    
    public static String escapeName(final String vname) {
        return StringUtil.escape2(vname, " .!*'();:@&=+$,/?%#[]");
    }
    
    public static String unescapeName(final String vname) {
        return StringUtil.unescape(vname);
    }
    
    public static void registerIOProvider(final String className) throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        final Class ioClass = NetcdfFile.class.getClassLoader().loadClass(className);
        registerIOProvider(ioClass);
    }
    
    public static void registerIOProvider(final Class iospClass) throws IllegalAccessException, InstantiationException {
        final IOServiceProvider spi = iospClass.newInstance();
        if (NetcdfFile.userLoads) {
            NetcdfFile.registeredProviders.add(0, spi);
        }
        else {
            NetcdfFile.registeredProviders.add(spi);
        }
    }
    
    public static void setDebugFlags(final DebugFlags debugFlag) {
        NetcdfFile.debugSPI = debugFlag.isSet("NetcdfFile/debugSPI");
        NetcdfFile.debugCompress = debugFlag.isSet("NetcdfFile/debugCompress");
        NetcdfFile.debugStructureIterator = debugFlag.isSet("NetcdfFile/structureIterator");
        N3header.disallowFileTruncation = debugFlag.isSet("NetcdfFile/disallowFileTruncation");
        N3header.debugHeaderSize = debugFlag.isSet("NetcdfFile/debugHeaderSize");
        NetcdfFile.showRequest = debugFlag.isSet("NetcdfFile/showRequest");
    }
    
    public static void setProperty(final String name, final String value) {
        N3iosp.setProperty(name, value);
    }
    
    public static NetcdfFile open(final String location) throws IOException {
        return open(location, null);
    }
    
    public static NetcdfFile open(final String location, final CancelTask cancelTask) throws IOException {
        return open(location, -1, cancelTask);
    }
    
    public static NetcdfFile open(final String location, final int buffer_size, final CancelTask cancelTask) throws IOException {
        return open(location, buffer_size, cancelTask, null);
    }
    
    public static NetcdfFile open(final String location, final int buffer_size, final CancelTask cancelTask, final Object iospMessage) throws IOException {
        final RandomAccessFile raf = getRaf(location, buffer_size);
        try {
            return open(raf, location, cancelTask, iospMessage);
        }
        catch (IOException ioe) {
            raf.close();
            throw ioe;
        }
    }
    
    public static boolean canOpen(final String location) throws IOException {
        RandomAccessFile raf = null;
        try {
            raf = getRaf(location, -1);
            return raf != null && canOpen(raf);
        }
        finally {
            if (raf != null) {
                raf.close();
            }
        }
    }
    
    private static boolean canOpen(final RandomAccessFile raf) throws IOException {
        if (N3header.isValidFile(raf)) {
            return true;
        }
        for (final IOServiceProvider registeredSpi : NetcdfFile.registeredProviders) {
            if (registeredSpi.isValidFile(raf)) {
                return true;
            }
        }
        return false;
    }
    
    public static NetcdfFile open(final String location, final String iospClassName, int bufferSize, final CancelTask cancelTask, final Object iospMessage) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
        final Class iospClass = NetcdfFile.class.getClassLoader().loadClass(iospClassName);
        final IOServiceProvider spi = iospClass.newInstance();
        if (iospMessage != null) {
            spi.sendIospMessage(iospMessage);
        }
        String uriString = location.trim();
        if (uriString.startsWith("file://")) {
            uriString = uriString.substring(7);
        }
        else if (uriString.startsWith("file:")) {
            uriString = uriString.substring(5);
        }
        uriString = StringUtil.replace(uriString, '\\', "/");
        if (bufferSize <= 0) {
            bufferSize = NetcdfFile.default_buffersize;
        }
        final RandomAccessFile raf = new RandomAccessFile(uriString, "r", bufferSize);
        final NetcdfFile result = new NetcdfFile(spi, raf, location, cancelTask);
        if (iospMessage != null) {
            spi.sendIospMessage(iospMessage);
        }
        return result;
    }
    
    private static RandomAccessFile getRaf(final String location, int buffer_size) throws IOException {
        String uriString = location.trim();
        if (buffer_size <= 0) {
            buffer_size = NetcdfFile.default_buffersize;
        }
        RandomAccessFile raf;
        if (uriString.startsWith("http:")) {
            raf = new HTTPRandomAccessFile(uriString);
        }
        else if (uriString.startsWith("nodods:")) {
            uriString = "http" + uriString.substring(6);
            raf = new HTTPRandomAccessFile(uriString);
        }
        else if (uriString.startsWith("slurp:")) {
            uriString = "http" + uriString.substring(5);
            final byte[] contents = IO.readURLContentsToByteArray(uriString);
            raf = new InMemoryRandomAccessFile(uriString, contents);
        }
        else {
            uriString = StringUtil.replace(uriString, '\\', "/");
            if (uriString.startsWith("file:")) {
                uriString = uriString.substring(5);
            }
            String uncompressedFileName = null;
            try {
                uncompressedFileName = makeUncompressed(uriString);
            }
            catch (Exception e) {
                NetcdfFile.log.warn("Failed to uncompress " + uriString + " err= " + e.getMessage() + "; try as a regular file.");
            }
            if (uncompressedFileName != null) {
                raf = new RandomAccessFile(uncompressedFileName, "r", buffer_size);
            }
            else {
                raf = new RandomAccessFile(uriString, "r", buffer_size);
            }
        }
        return raf;
    }
    
    private static String makeUncompressed(final String filename) throws Exception {
        final int pos = filename.lastIndexOf(".");
        if (pos < 0) {
            return null;
        }
        final String suffix = filename.substring(pos + 1);
        final String uncompressedFilename = filename.substring(0, pos);
        if (!suffix.equalsIgnoreCase("Z") && !suffix.equalsIgnoreCase("zip") && !suffix.equalsIgnoreCase("gzip") && !suffix.equalsIgnoreCase("gz") && !suffix.equalsIgnoreCase("bz2")) {
            return null;
        }
        final File uncompressedFile = DiskCache.getFileStandardPolicy(uncompressedFilename);
        if (uncompressedFile.exists() && uncompressedFile.length() > 0L) {
            FileInputStream stream = null;
            FileLock lock = null;
            try {
                stream = new FileInputStream(uncompressedFile);
            Label_0151:
                while (true) {
                    try {
                        lock = stream.getChannel().lock(0L, 1L, true);
                    }
                    catch (OverlappingFileLockException oe) {
                        try {
                            Thread.sleep(100L);
                        }
                        catch (InterruptedException e2) {
                            break Label_0151;
                        }
                        continue;
                    }
                    break;
                }
                if (NetcdfFile.debugCompress) {
                    System.out.println("found uncompressed " + uncompressedFile + " for " + filename);
                }
                return uncompressedFile.getPath();
            }
            finally {
                if (lock != null) {
                    lock.release();
                }
                if (stream != null) {
                    stream.close();
                }
            }
        }
        final File file = new File(filename);
        if (!file.exists()) {
            return null;
        }
        InputStream in = null;
        FileOutputStream fout = new FileOutputStream(uncompressedFile);
        FileLock lock2 = null;
        while (true) {
            try {
                lock2 = fout.getChannel().lock(0L, 1L, false);
            }
            catch (OverlappingFileLockException oe2) {
                try {
                    Thread.sleep(100L);
                }
                catch (InterruptedException ex) {}
                continue;
            }
            break;
        }
        try {
            if (suffix.equalsIgnoreCase("Z")) {
                in = new UncompressInputStream(new FileInputStream(filename));
                copy(in, fout, 100000);
                if (NetcdfFile.debugCompress) {
                    System.out.println("uncompressed " + filename + " to " + uncompressedFile);
                }
            }
            else if (suffix.equalsIgnoreCase("zip")) {
                in = new ZipInputStream(new FileInputStream(filename));
                copy(in, fout, 100000);
                if (NetcdfFile.debugCompress) {
                    System.out.println("unzipped " + filename + " to " + uncompressedFile);
                }
            }
            else if (suffix.equalsIgnoreCase("bz2")) {
                in = new CBZip2InputStream(new FileInputStream(filename), true);
                copy(in, fout, 100000);
                if (NetcdfFile.debugCompress) {
                    System.out.println("unbzipped " + filename + " to " + uncompressedFile);
                }
            }
            else if (suffix.equalsIgnoreCase("gzip") || suffix.equalsIgnoreCase("gz")) {
                in = new GZIPInputStream(new FileInputStream(filename));
                copy(in, fout, 100000);
                if (NetcdfFile.debugCompress) {
                    System.out.println("ungzipped " + filename + " to " + uncompressedFile);
                }
            }
        }
        catch (Exception e) {
            if (fout != null) {
                fout.close();
            }
            fout = null;
            if (uncompressedFile.exists() && !uncompressedFile.delete()) {
                NetcdfFile.log.warn("failed to delete uncompressed file (IOException)" + uncompressedFile);
            }
            throw e;
        }
        finally {
            if (lock2 != null) {
                lock2.release();
            }
            if (in != null) {
                in.close();
            }
            if (fout != null) {
                fout.close();
            }
        }
        return uncompressedFile.getPath();
    }
    
    private static void copy(final InputStream in, final OutputStream out, final int bufferSize) throws IOException {
        final byte[] buffer = new byte[bufferSize];
        while (true) {
            final int bytesRead = in.read(buffer);
            if (bytesRead == -1) {
                break;
            }
            out.write(buffer, 0, bytesRead);
        }
    }
    
    public static NetcdfFile openInMemory(final String name, final byte[] data, final String iospClassName) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        final InMemoryRandomAccessFile raf = new InMemoryRandomAccessFile(name, data);
        final Class iospClass = NetcdfFile.class.getClassLoader().loadClass(iospClassName);
        final IOServiceProvider spi = iospClass.newInstance();
        return new NetcdfFile(spi, raf, name, null);
    }
    
    public static NetcdfFile openInMemory(final String name, final byte[] data) throws IOException {
        final InMemoryRandomAccessFile raf = new InMemoryRandomAccessFile(name, data);
        return open(raf, name, null, null);
    }
    
    public static NetcdfFile openInMemory(final String filename) throws IOException {
        final File file = new File(filename);
        final ByteArrayOutputStream bos = new ByteArrayOutputStream((int)file.length());
        final InputStream in = new BufferedInputStream(new FileInputStream(filename));
        IO.copy(in, bos);
        return openInMemory(filename, bos.toByteArray());
    }
    
    public static NetcdfFile openInMemory(final URI uri) throws IOException {
        final URL url = uri.toURL();
        final byte[] contents = IO.readContentsToByteArray(url.openStream());
        return openInMemory(uri.toString(), contents);
    }
    
    private static NetcdfFile open(final RandomAccessFile raf, final String location, final CancelTask cancelTask, final Object iospMessage) throws IOException {
        IOServiceProvider spi = null;
        if (NetcdfFile.debugSPI) {
            System.out.println("NetcdfFile try to open = " + location);
        }
        if (N3header.isValidFile(raf)) {
            spi = SPFactory.getServiceProvider();
        }
        else {
            for (final IOServiceProvider registeredSpi : NetcdfFile.registeredProviders) {
                if (NetcdfFile.debugSPI) {
                    System.out.println(" try iosp = " + registeredSpi.getClass().getName());
                }
                if (registeredSpi.isValidFile(raf)) {
                    final Class c = registeredSpi.getClass();
                    try {
                        spi = c.newInstance();
                        break;
                    }
                    catch (InstantiationException e2) {
                        throw new IOException("IOServiceProvider " + c.getName() + "must have no-arg constructor.");
                    }
                    catch (IllegalAccessException e) {
                        throw new IOException("IOServiceProvider " + c.getName() + " IllegalAccessException: " + e.getMessage());
                    }
                }
            }
        }
        if (spi == null) {
            raf.close();
            throw new IOException("Cant read " + location + ": not a valid CDM file.");
        }
        if (iospMessage != null) {
            spi.sendIospMessage(iospMessage);
        }
        if (NetcdfFile.log.isDebugEnabled()) {
            NetcdfFile.log.debug("Using IOSP " + spi.getClass().getName());
        }
        final NetcdfFile result = new NetcdfFile(spi, raf, location, cancelTask);
        if (iospMessage != null) {
            spi.sendIospMessage(iospMessage);
        }
        return result;
    }
    
    public synchronized boolean isUnlocked() {
        return this.unlocked;
    }
    
    public synchronized void close() throws IOException {
        if (this.cache != null) {
            this.unlocked = true;
            this.cache.release(this);
        }
        else {
            try {
                if (null != this.spi) {
                    this.spi.close();
                }
            }
            finally {
                this.spi = null;
            }
        }
    }
    
    public void setFileCache(final FileCache cache) {
        this.cache = cache;
    }
    
    public String getCacheName() {
        return this.cacheName;
    }
    
    protected void setCacheName(final String cacheName) {
        this.cacheName = cacheName;
    }
    
    public String getLocation() {
        return this.location;
    }
    
    public String getId() {
        return this.id;
    }
    
    public String getTitle() {
        return this.title;
    }
    
    public Group getRootGroup() {
        return this.rootGroup;
    }
    
    public List<Variable> getVariables() {
        return this.variables;
    }
    
    public Variable findTopVariable(final String name) {
        if (name == null) {
            return null;
        }
        for (final Variable v : this.variables) {
            if (name.equals(v.getName())) {
                return v;
            }
        }
        return null;
    }
    
    public Group findGroup(final String fullNameEscaped) {
        if (fullNameEscaped == null || fullNameEscaped.length() == 0) {
            return this.rootGroup;
        }
        Group g = this.rootGroup;
        final String[] arr$;
        final String[] groupNames = arr$ = fullNameEscaped.split("/");
        for (final String groupName : arr$) {
            g = g.findGroup(groupName);
            if (g == null) {
                return null;
            }
        }
        return g;
    }
    
    public Variable findVariable(final String fullNameEscaped) {
        if (fullNameEscaped == null || fullNameEscaped.length() == 0) {
            return null;
        }
        Group g = this.rootGroup;
        String vars = fullNameEscaped;
        final int pos = fullNameEscaped.lastIndexOf("/");
        if (pos >= 0) {
            final String groups = fullNameEscaped.substring(0, pos);
            vars = fullNameEscaped.substring(pos + 1);
            final StringTokenizer stoke = new StringTokenizer(groups, "/");
            while (stoke.hasMoreTokens()) {
                final String token = stoke.nextToken();
                g = g.findGroup(token);
                if (g == null) {
                    return null;
                }
            }
        }
        final StringTokenizer stoke2 = new StringTokenizer(vars, ".");
        if (!stoke2.hasMoreTokens()) {
            return null;
        }
        final String varShortName = unescapeName(stoke2.nextToken());
        Variable v = g.findVariable(varShortName);
        if (v == null) {
            return null;
        }
        while (stoke2.hasMoreTokens()) {
            if (!(v instanceof Structure)) {
                return null;
            }
            final String name = unescapeName(stoke2.nextToken());
            v = ((Structure)v).findVariable(name);
            if (v == null) {
                return null;
            }
        }
        return v;
    }
    
    public List<Dimension> getDimensions() {
        return this.immutable ? this.dimensions : new ArrayList<Dimension>(this.dimensions);
    }
    
    public Dimension findDimension(final String name) {
        if (name == null) {
            return null;
        }
        for (final Dimension d : this.dimensions) {
            if (name.equals(d.getName())) {
                return d;
            }
        }
        return null;
    }
    
    public boolean hasUnlimitedDimension() {
        return this.getUnlimitedDimension() != null;
    }
    
    public Dimension getUnlimitedDimension() {
        for (final Dimension d : this.dimensions) {
            if (d.isUnlimited()) {
                return d;
            }
        }
        return null;
    }
    
    public List<Attribute> getGlobalAttributes() {
        return this.immutable ? this.gattributes : new ArrayList<Attribute>(this.gattributes);
    }
    
    public Attribute findGlobalAttribute(final String name) {
        for (final Attribute a : this.gattributes) {
            if (name.equals(a.getName())) {
                return a;
            }
        }
        return null;
    }
    
    public Attribute findGlobalAttributeIgnoreCase(final String name) {
        for (final Attribute a : this.gattributes) {
            if (name.equalsIgnoreCase(a.getName())) {
                return a;
            }
        }
        return null;
    }
    
    public String findAttValueIgnoreCase(final Variable v, final String attName, final String defaultValue) {
        String attValue = null;
        Attribute att;
        if (v == null) {
            att = this.rootGroup.findAttributeIgnoreCase(attName);
        }
        else {
            att = v.findAttributeIgnoreCase(attName);
        }
        if (att != null && att.isString()) {
            attValue = att.getStringValue();
        }
        if (null == attValue) {
            attValue = defaultValue;
        }
        return attValue;
    }
    
    public double readAttributeDouble(final Variable v, final String attName, final double defValue) {
        Attribute att;
        if (v == null) {
            att = this.rootGroup.findAttributeIgnoreCase(attName);
        }
        else {
            att = v.findAttributeIgnoreCase(attName);
        }
        if (att == null) {
            return defValue;
        }
        if (att.isString()) {
            return Double.parseDouble(att.getStringValue());
        }
        return att.getNumericValue().doubleValue();
    }
    
    public int readAttributeInteger(final Variable v, final String attName, final int defValue) {
        Attribute att;
        if (v == null) {
            att = this.rootGroup.findAttributeIgnoreCase(attName);
        }
        else {
            att = v.findAttributeIgnoreCase(attName);
        }
        if (att == null) {
            return defValue;
        }
        if (att.isString()) {
            return Integer.parseInt(att.getStringValue());
        }
        return att.getNumericValue().intValue();
    }
    
    public void writeCDL(final OutputStream out, final boolean strict) {
        final PrintWriter pw = new PrintWriter(new OutputStreamWriter(out));
        this.toStringStart(pw, strict);
        this.toStringEnd(pw);
        pw.flush();
    }
    
    public void writeCDL(final PrintWriter pw, final boolean strict) {
        this.toStringStart(pw, strict);
        this.toStringEnd(pw);
        pw.flush();
    }
    
    @Override
    public String toString() {
        final StringWriter writer = new StringWriter(50000);
        this.writeCDL(new PrintWriter(writer), false);
        return writer.toString();
    }
    
    protected void toStringStart(final PrintWriter pw, final boolean strict) {
        String name = this.getLocation();
        if (strict) {
            int pos = name.lastIndexOf(47);
            if (pos < 0) {
                pos = name.lastIndexOf(92);
            }
            if (pos >= 0) {
                name = name.substring(pos + 1);
            }
            if (name.endsWith(".nc")) {
                name = name.substring(0, name.length() - 3);
            }
            if (name.endsWith(".cdl")) {
                name = name.substring(0, name.length() - 4);
            }
        }
        pw.print("netcdf " + name + " {\n");
        this.rootGroup.writeCDL(pw, "", strict);
    }
    
    protected void toStringEnd(final PrintWriter pw) {
        pw.print("}\n");
    }
    
    public void writeNcML(final OutputStream os, final String uri) throws IOException {
        NCdumpW.writeNcML(this, new OutputStreamWriter(os), false, uri);
    }
    
    public void writeNcML(final Writer writer, final String uri) throws IOException {
        NCdumpW.writeNcML(this, writer, false, uri);
    }
    
    public boolean syncExtend() throws IOException {
        this.unlocked = false;
        return this.spi != null && this.spi.syncExtend();
    }
    
    public boolean sync() throws IOException {
        this.unlocked = false;
        return this.spi != null && this.spi.sync();
    }
    
    @Deprecated
    public NetcdfFile(final String filename) throws IOException {
        this.rootGroup = this.makeRootGroup();
        this.unlocked = false;
        this.immutable = false;
        this.location = filename;
        final RandomAccessFile raf = new RandomAccessFile(filename, "r");
        (this.spi = SPFactory.getServiceProvider()).open(raf, this, null);
        this.finish();
    }
    
    @Deprecated
    public NetcdfFile(final URL url) throws IOException {
        this.rootGroup = this.makeRootGroup();
        this.unlocked = false;
        this.immutable = false;
        this.location = url.toString();
        final RandomAccessFile raf = new HTTPRandomAccessFile(this.location);
        (this.spi = SPFactory.getServiceProvider()).open(raf, this, null);
        this.finish();
    }
    
    protected NetcdfFile(final String iospClassName, final String iospParam, final String location, final int buffer_size, final CancelTask cancelTask) throws IOException, IllegalAccessException, InstantiationException, ClassNotFoundException {
        this.rootGroup = this.makeRootGroup();
        this.unlocked = false;
        this.immutable = false;
        final Class iospClass = this.getClass().getClassLoader().loadClass(iospClassName);
        this.spi = iospClass.newInstance();
        if (NetcdfFile.debugSPI) {
            System.out.println("NetcdfFile uses iosp = " + this.spi.getClass().getName());
        }
        this.spi.sendIospMessage(iospParam);
        this.location = location;
        final RandomAccessFile raf = getRaf(location, buffer_size);
        try {
            this.spi.open(raf, this, cancelTask);
            this.finish();
        }
        catch (IOException e) {
            try {
                this.spi.close();
            }
            catch (Throwable t2) {}
            try {
                raf.close();
            }
            catch (Throwable t3) {}
            this.spi = null;
            throw e;
        }
        catch (RuntimeException e2) {
            try {
                this.spi.close();
            }
            catch (Throwable t4) {}
            try {
                raf.close();
            }
            catch (Throwable t5) {}
            this.spi = null;
            throw e2;
        }
        catch (Throwable t) {
            try {
                this.spi.close();
            }
            catch (Throwable t6) {}
            try {
                raf.close();
            }
            catch (Throwable t7) {}
            this.spi = null;
            throw new RuntimeException(t);
        }
        if (this.id == null) {
            this.setId(this.findAttValueIgnoreCase(null, "_Id", null));
        }
        if (this.title == null) {
            this.setId(this.findAttValueIgnoreCase(null, "_Title", null));
        }
    }
    
    protected NetcdfFile(final IOServiceProvider spi, final RandomAccessFile raf, final String location, final CancelTask cancelTask) throws IOException {
        this.rootGroup = this.makeRootGroup();
        this.unlocked = false;
        this.immutable = false;
        this.spi = spi;
        this.location = location;
        if (NetcdfFile.debugSPI) {
            System.out.println("NetcdfFile uses iosp = " + spi.getClass().getName());
        }
        try {
            spi.open(raf, this, cancelTask);
        }
        catch (IOException e) {
            try {
                spi.close();
            }
            catch (Throwable t2) {}
            try {
                raf.close();
            }
            catch (Throwable t3) {}
            this.spi = null;
            throw e;
        }
        catch (RuntimeException e2) {
            try {
                spi.close();
            }
            catch (Throwable t4) {}
            try {
                raf.close();
            }
            catch (Throwable t5) {}
            this.spi = null;
            throw e2;
        }
        catch (Throwable t) {
            try {
                spi.close();
            }
            catch (Throwable t6) {}
            try {
                raf.close();
            }
            catch (Throwable t7) {}
            this.spi = null;
            throw new RuntimeException(t);
        }
        if (this.id == null) {
            this.setId(this.findAttValueIgnoreCase(null, "_Id", null));
        }
        if (this.title == null) {
            this.setId(this.findAttValueIgnoreCase(null, "_Title", null));
        }
        this.finish();
    }
    
    protected NetcdfFile() {
        this.rootGroup = this.makeRootGroup();
        this.unlocked = false;
        this.immutable = false;
    }
    
    protected NetcdfFile(final NetcdfFile ncfile) {
        this.rootGroup = this.makeRootGroup();
        this.unlocked = false;
        this.immutable = false;
        this.location = ncfile.getLocation();
        this.id = ncfile.getId();
        this.title = ncfile.getTitle();
        this.spi = ncfile.spi;
    }
    
    public Attribute addAttribute(Group parent, final Attribute att) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        if (parent == null) {
            parent = this.rootGroup;
        }
        parent.addAttribute(att);
        return att;
    }
    
    public Group addGroup(Group parent, final Group g) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        if (parent == null) {
            parent = this.rootGroup;
        }
        parent.addGroup(g);
        return g;
    }
    
    public Dimension addDimension(Group parent, final Dimension d) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        if (parent == null) {
            parent = this.rootGroup;
        }
        parent.addDimension(d);
        return d;
    }
    
    public boolean removeDimension(Group g, final String dimName) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        if (g == null) {
            g = this.rootGroup;
        }
        return g.removeDimension(dimName);
    }
    
    public Variable addVariable(Group g, final Variable v) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        if (g == null) {
            g = this.rootGroup;
        }
        if (v != null) {
            g.addVariable(v);
        }
        return v;
    }
    
    public Variable addVariable(Group g, final String shortName, final DataType dtype, final String dims) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        if (g == null) {
            g = this.rootGroup;
        }
        final Variable v = new Variable(this, g, null, shortName);
        v.setDataType(dtype);
        v.setDimensions(dims);
        g.addVariable(v);
        return v;
    }
    
    public Variable addStringVariable(Group g, final String shortName, final String dims, final int strlen) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        if (g == null) {
            g = this.rootGroup;
        }
        final String dimName = shortName + "_strlen";
        this.addDimension(g, new Dimension(dimName, strlen));
        final Variable v = new Variable(this, g, null, shortName);
        v.setDataType(DataType.CHAR);
        v.setDimensions(dims + " " + dimName);
        g.addVariable(v);
        return v;
    }
    
    public boolean removeVariable(Group g, final String varName) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        if (g == null) {
            g = this.rootGroup;
        }
        return g.removeVariable(varName);
    }
    
    public Attribute addVariableAttribute(final Variable v, final Attribute att) {
        return v.addAttribute(att);
    }
    
    public Object sendIospMessage(final Object message) {
        if (null == message) {
            return null;
        }
        if (message == "AddRecordStructure") {
            final Variable v = this.rootGroup.findVariable("record");
            final boolean gotit = v != null && v instanceof Structure;
            return gotit || this.makeRecordStructure();
        }
        if (message == "RemoveRecordStructure") {
            final Variable v = this.rootGroup.findVariable("record");
            final boolean gotit = v != null && v instanceof Structure;
            if (gotit) {
                this.rootGroup.remove(v);
                this.variables.remove(v);
                this.removeRecordStructure();
            }
            return gotit;
        }
        if (this.spi != null) {
            return this.spi.sendIospMessage(message);
        }
        return null;
    }
    
    protected Boolean makeRecordStructure() {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        Boolean didit = false;
        if (this.spi != null && this.spi instanceof N3iosp && this.hasUnlimitedDimension()) {
            didit = (Boolean)this.spi.sendIospMessage("AddRecordStructure");
        }
        return didit;
    }
    
    protected Boolean removeRecordStructure() {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        Boolean didit = false;
        if (this.spi != null && this.spi instanceof N3iosp) {
            didit = (Boolean)this.spi.sendIospMessage("RemoveRecordStructure");
        }
        return didit;
    }
    
    public void setId(final String id) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        this.id = id;
    }
    
    public void setTitle(final String title) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        this.title = title;
    }
    
    public void setLocation(final String location) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        this.location = location;
    }
    
    public NetcdfFile setImmutable() {
        if (this.immutable) {
            return this;
        }
        this.immutable = true;
        this.setImmutable(this.rootGroup);
        this.variables = Collections.unmodifiableList((List<? extends Variable>)this.variables);
        this.dimensions = Collections.unmodifiableList((List<? extends Dimension>)this.dimensions);
        this.gattributes = Collections.unmodifiableList((List<? extends Attribute>)this.gattributes);
        return this;
    }
    
    private void setImmutable(final Group g) {
        for (final Variable v : g.variables) {
            v.setImmutable();
        }
        for (final Dimension d : g.dimensions) {
            d.setImmutable();
        }
        for (final Group nested : g.getGroups()) {
            this.setImmutable(nested);
        }
        g.setImmutable();
    }
    
    public void empty() {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        this.variables = new ArrayList<Variable>();
        this.gattributes = new ArrayList<Attribute>();
        this.dimensions = new ArrayList<Dimension>();
        this.rootGroup = this.makeRootGroup();
    }
    
    protected Group makeRootGroup() {
        final Group root = new Group(this, null, "");
        root.parent = null;
        return root;
    }
    
    public void finish() {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        this.variables = new ArrayList<Variable>();
        this.dimensions = new ArrayList<Dimension>();
        this.gattributes = new ArrayList<Attribute>();
        this.finishGroup(this.rootGroup);
    }
    
    private void finishGroup(final Group g) {
        this.variables.addAll(g.variables);
        for (final Attribute oldAtt : g.attributes) {
            if (g == this.rootGroup) {
                this.gattributes.add(oldAtt);
            }
            else {
                final String newName = this.makeFullNameWithString(g, oldAtt.getName());
                this.gattributes.add(new Attribute(newName, oldAtt));
            }
        }
        for (final Dimension oldDim : g.dimensions) {
            if (oldDim.isShared()) {
                if (g == this.rootGroup) {
                    this.dimensions.add(oldDim);
                }
                else {
                    final String newName = this.makeFullNameWithString(g, oldDim.getName());
                    this.dimensions.add(new Dimension(newName, oldDim));
                }
            }
        }
        final List<Group> groups = g.getGroups();
        for (final Group nested : groups) {
            this.finishGroup(nested);
        }
    }
    
    protected String makeFullNameWithString(final Group parent, final String name) {
        final StringBuilder sbuff = new StringBuilder();
        appendGroupName(sbuff, parent);
        sbuff.append(name);
        return sbuff.toString();
    }
    
    protected static String makeFullName(final Group parent, final Variable v) {
        if ((parent == null || parent.isRoot()) && !v.isMemberOfStructure()) {
            return v.getShortName();
        }
        final StringBuilder sbuff = new StringBuilder();
        appendGroupName(sbuff, parent);
        appendStructureName(sbuff, v);
        return sbuff.toString();
    }
    
    private static void appendGroupName(final StringBuilder sbuff, final Group g) {
        final boolean isRoot = g.getParentGroup() == null;
        if (isRoot) {
            return;
        }
        if (g.getParentGroup() != null) {
            appendGroupName(sbuff, g.getParentGroup());
        }
        sbuff.append(g.getShortName());
        sbuff.append("/");
    }
    
    private static void appendStructureName(final StringBuilder sbuff, final Variable v) {
        if (v.isMemberOfStructure()) {
            appendStructureName(sbuff, v.getParentStructure());
            sbuff.append(".");
        }
        sbuff.append(v.getShortName());
    }
    
    protected static String makeFullNameEscaped(final Group parent, final Variable v) {
        final StringBuilder sbuff = new StringBuilder();
        appendGroupNameEscaped(sbuff, parent);
        appendStructureNameEscaped(sbuff, v);
        return sbuff.toString();
    }
    
    private static void appendGroupNameEscaped(final StringBuilder sbuff, final Group g) {
        final boolean isRoot = g.getParentGroup() == null;
        if (isRoot) {
            return;
        }
        if (g.getParentGroup() != null) {
            appendGroupNameEscaped(sbuff, g.getParentGroup());
        }
        sbuff.append(escapeName(g.getShortName()));
        sbuff.append("/");
    }
    
    private static void appendStructureNameEscaped(final StringBuilder sbuff, final Variable v) {
        if (v.isMemberOfStructure()) {
            appendStructureNameEscaped(sbuff, v.getParentStructure());
            sbuff.append(".");
        }
        sbuff.append(escapeName(v.getShortName()));
    }
    
    protected Array readData(final Variable v, final Section ranges) throws IOException, InvalidRangeException {
        if (NetcdfFile.showRequest) {
            System.out.println("Data request for variable: " + v.getName() + " section= " + ranges);
        }
        if (this.unlocked) {
            final String info = this.cache.getInfo(this);
            throw new IllegalStateException("File is unlocked - cannot use\n" + info);
        }
        if (this.spi == null) {
            throw new IOException("BAD: missing spi: " + v.getName());
        }
        final Array result = this.spi.readData(v, ranges);
        result.setUnsigned(v.isUnsigned());
        return result;
    }
    
    public Array readSection(final String variableSection) throws IOException, InvalidRangeException {
        if (this.unlocked) {
            throw new IllegalStateException("File is unlocked - cannot use");
        }
        final ParsedSectionSpec cer = ParsedSectionSpec.parseVariableSection(this, variableSection);
        if (cer.child == null) {
            final Array result = cer.v.read(cer.section);
            result.setUnsigned(cer.v.isUnsigned());
            return result;
        }
        if (this.spi == null) {
            return IospHelper.readSection(cer);
        }
        return this.spi.readSection(cer);
    }
    
    protected long readToByteChannel(final Variable v, final Section section, final WritableByteChannel wbc) throws IOException, InvalidRangeException {
        if (this.unlocked) {
            throw new IllegalStateException("File is unlocked - cannot use");
        }
        if (this.spi == null || v.hasCachedData()) {
            return IospHelper.copyToByteChannel(v.read(section), wbc);
        }
        return this.spi.readToByteChannel(v, section, wbc);
    }
    
    protected StructureDataIterator getStructureIterator(final Structure s, final int bufferSize) throws IOException {
        return this.spi.getStructureIterator(s, bufferSize);
    }
    
    public List<Array> readArrays(final List<Variable> variables) throws IOException {
        final List<Array> result = new ArrayList<Array>();
        for (final Variable variable : variables) {
            result.add(variable.read());
        }
        return result;
    }
    
    @Deprecated
    public Array read(final String variableSection, final boolean flatten) throws IOException, InvalidRangeException {
        if (!flatten) {
            throw new UnsupportedOperationException("NetdfFile.read(String variableSection, boolean flatten=false)");
        }
        return this.readSection(variableSection);
    }
    
    protected String toStringDebug(final Object o) {
        return (this.spi == null) ? "" : this.spi.toStringDebug(o);
    }
    
    public String getDetailInfo() {
        final Formatter f = new Formatter();
        this.getDetailInfo(f);
        return f.toString();
    }
    
    public void getDetailInfo(final Formatter f) {
        f.format("NetcdfFile location= %s%n", this.getLocation());
        f.format("  title= %s%n", this.getTitle());
        f.format("  id= %s%n", this.getId());
        f.format("  fileType= %s%n", this.getFileTypeId());
        f.format("  fileDesc= %s%n", this.getFileTypeDescription());
        f.format("  class= %s%n", this.getClass().getName());
        if (this.spi == null) {
            f.format("  has no IOSP%n", new Object[0]);
        }
        else {
            f.format("  iosp= %s%n%n", this.spi.getClass());
            f.format(this.spi.getDetailInfo(), new Object[0]);
        }
        this.showCached(f);
        this.showProxies(f);
    }
    
    protected void showCached(final Formatter f) {
        int maxNameLen = 8;
        for (final Variable v : this.getVariables()) {
            maxNameLen = Math.max(maxNameLen, v.getShortName().length());
        }
        long total = 0L;
        f.format("%n%-" + maxNameLen + "s isCaching  size     cachedSize (bytes) %n", "Variable");
        for (final Variable v2 : this.getVariables()) {
            f.format(" %-" + maxNameLen + "s %5s %8d ", v2.getShortName(), v2.isCaching(), v2.getSize() * v2.getElementSize());
            if (v2.hasCachedData()) {
                Array data = null;
                try {
                    data = v2.read();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
                final long size = data.getSizeBytes();
                f.format(" %8d", size);
                total += size;
            }
            f.format("%n", new Object[0]);
        }
        f.format(" %" + maxNameLen + "s                  --------%n", " ");
        f.format(" %" + maxNameLen + "s                 %8d Kb total cached%n", " ", total / 1000L);
    }
    
    protected void showProxies(final Formatter f) {
        int maxNameLen = 8;
        boolean hasProxy = false;
        for (final Variable v : this.getVariables()) {
            if (v.proxyReader != v) {
                hasProxy = true;
            }
            maxNameLen = Math.max(maxNameLen, v.getShortName().length());
        }
        if (!hasProxy) {
            return;
        }
        f.format("%n%-" + maxNameLen + "s  proxyReader   Variable.Class %n", "Variable");
        for (final Variable v : this.getVariables()) {
            if (v.proxyReader != v) {
                f.format(" %-" + maxNameLen + "s  %s %s%n", v.getShortName(), v.proxyReader.getClass().getName(), v.getClass().getName());
            }
        }
        f.format("%n", new Object[0]);
    }
    
    public IOServiceProvider getIosp() {
        return this.spi;
    }
    
    public String getFileTypeId() {
        if (this.spi != null) {
            return this.spi.getFileTypeId();
        }
        return "N/A";
    }
    
    public String getFileTypeDescription() {
        if (this.spi != null) {
            return this.spi.getFileTypeDescription();
        }
        return "N/A";
    }
    
    public String getFileTypeVersion() {
        if (this.spi != null) {
            return this.spi.getFileTypeVersion();
        }
        return "N/A";
    }
    
    public static void main(final String[] arg) throws Exception {
        final int wide = 20;
        final Formatter f = new Formatter(System.out);
        f.format(" %" + wide + "s %n", "test");
        f.format(" %20s %n", "asiuasdipuasiud");
    }
    
    static {
        NetcdfFile.log = LoggerFactory.getLogger(NetcdfFile.class);
        NetcdfFile.default_buffersize = 8092;
        NetcdfFile.registeredProviders = new ArrayList<IOServiceProvider>();
        NetcdfFile.debugSPI = false;
        NetcdfFile.debugCompress = false;
        NetcdfFile.showRequest = false;
        NetcdfFile.debugStructureIterator = false;
        NetcdfFile.loadWarnings = false;
        NetcdfFile.userLoads = false;
        try {
            registerIOProvider("ucar.nc2.stream.NcStreamIosp");
        }
        catch (Throwable e) {
            if (NetcdfFile.loadWarnings) {
                NetcdfFile.log.info("Cant load class: " + e);
            }
        }
        try {
            registerIOProvider("ucar.nc2.iosp.hdf5.H5iosp");
        }
        catch (Throwable e) {
            if (NetcdfFile.loadWarnings) {
                NetcdfFile.log.info("Cant load class: " + e);
            }
        }
        try {
            registerIOProvider("ucar.nc2.iosp.hdf4.H4iosp");
        }
        catch (Throwable e) {
            if (NetcdfFile.loadWarnings) {
                NetcdfFile.log.info("Cant load class: " + e);
            }
        }
        try {
            NetcdfFile.class.getClassLoader().loadClass("ucar.grib.grib2.Grib2Input");
            registerIOProvider("ucar.nc2.iosp.grib.GribGridServiceProvider");
        }
        catch (Throwable e) {
            if (NetcdfFile.loadWarnings) {
                NetcdfFile.log.info("Cant load class: " + e);
            }
        }
        try {
            NetcdfFile.class.getClassLoader().loadClass("ucar.grib.grib2.Grib2Input");
            NetcdfFile.class.getClassLoader().loadClass("visad.util.Trace");
            registerIOProvider("ucar.nc2.iosp.gempak.GempakGridServiceProvider");
        }
        catch (Throwable e) {
            if (NetcdfFile.loadWarnings) {
                NetcdfFile.log.info("Cant load class: " + e);
            }
        }
        try {
            final URL url = NetcdfFile.class.getResource("/resources/bufrTables/local/tablelookup.csv");
            if (null != url) {
                registerIOProvider("ucar.nc2.iosp.bufr.BufrIosp");
            }
        }
        catch (Throwable e) {
            if (NetcdfFile.loadWarnings) {
                NetcdfFile.log.info("Cant load resource: " + e);
            }
        }
        try {
            NetcdfFile.class.getClassLoader().loadClass("edu.wisc.ssec.mcidas.AreaFile");
            registerIOProvider("ucar.nc2.iosp.mcidas.AreaServiceProvider");
        }
        catch (Throwable e) {
            if (NetcdfFile.loadWarnings) {
                NetcdfFile.log.info("Cant load class: " + e);
            }
        }
        try {
            registerIOProvider("ucar.nc2.iosp.nexrad2.Nexrad2IOServiceProvider");
        }
        catch (Throwable e) {
            if (NetcdfFile.loadWarnings) {
                NetcdfFile.log.info("Cant load class: " + e);
            }
        }
        try {
            registerIOProvider("ucar.nc2.iosp.nids.Nidsiosp");
        }
        catch (Throwable e) {
            if (NetcdfFile.loadWarnings) {
                NetcdfFile.log.info("Cant load class: " + e);
            }
        }
        try {
            registerIOProvider("ucar.nc2.iosp.nowrad.NOWRadiosp");
        }
        catch (Throwable e) {
            if (NetcdfFile.loadWarnings) {
                NetcdfFile.log.info("Cant load class: " + e);
            }
        }
        try {
            registerIOProvider("ucar.nc2.iosp.dorade.Doradeiosp");
        }
        catch (Throwable e) {
            if (NetcdfFile.loadWarnings) {
                NetcdfFile.log.info("Cant load class: " + e);
            }
        }
        try {
            registerIOProvider("ucar.nc2.iosp.dmsp.DMSPiosp");
        }
        catch (Throwable e) {
            if (NetcdfFile.loadWarnings) {
                NetcdfFile.log.info("Cant load class: " + e);
            }
        }
        try {
            registerIOProvider("ucar.nc2.iosp.cinrad.Cinrad2IOServiceProvider");
        }
        catch (Throwable e) {
            if (NetcdfFile.loadWarnings) {
                NetcdfFile.log.info("Cant load class: " + e);
            }
        }
        try {
            registerIOProvider("ucar.nc2.iosp.misc.GtopoIosp");
        }
        catch (Throwable e) {
            if (NetcdfFile.loadWarnings) {
                NetcdfFile.log.info("Cant load class: " + e);
            }
        }
        try {
            registerIOProvider("ucar.nc2.iosp.misc.NmcObsLegacy");
        }
        catch (Throwable e) {
            if (NetcdfFile.loadWarnings) {
                NetcdfFile.log.info("Cant load class: " + e);
            }
        }
        try {
            registerIOProvider("ucar.nc2.iosp.gini.Giniiosp");
        }
        catch (Throwable e) {
            if (NetcdfFile.loadWarnings) {
                NetcdfFile.log.info("Cant load class: " + e);
            }
        }
        try {
            registerIOProvider("ucar.nc2.iosp.uf.UFiosp");
        }
        catch (Throwable e) {
            if (NetcdfFile.loadWarnings) {
                NetcdfFile.log.info("Cant load class: " + e);
            }
        }
        try {
            registerIOProvider("ucar.nc2.iosp.misc.Uspln");
        }
        catch (Throwable e) {
            if (NetcdfFile.loadWarnings) {
                NetcdfFile.log.info("Cant load class: " + e);
            }
        }
        try {
            registerIOProvider("ucar.nc2.iosp.misc.Nldn");
        }
        catch (Throwable e) {
            if (NetcdfFile.loadWarnings) {
                NetcdfFile.log.info("Cant load class: " + e);
            }
        }
        try {
            registerIOProvider("ucar.nc2.iosp.fysat.Fysatiosp");
        }
        catch (Throwable e) {
            if (NetcdfFile.loadWarnings) {
                NetcdfFile.log.info("Cant load class: " + e);
            }
        }
        try {
            NetcdfFile.class.getClassLoader().loadClass("visad.util.Trace");
            registerIOProvider("ucar.nc2.iosp.gempak.GempakSurfaceIOSP");
        }
        catch (Throwable e) {
            if (NetcdfFile.loadWarnings) {
                NetcdfFile.log.info("Cant load class: " + e);
            }
        }
        try {
            NetcdfFile.class.getClassLoader().loadClass("visad.util.Trace");
            registerIOProvider("ucar.nc2.iosp.gempak.GempakSoundingIOSP");
        }
        catch (Throwable e) {
            if (NetcdfFile.loadWarnings) {
                NetcdfFile.log.info("Cant load class: " + e);
            }
        }
        try {
            registerIOProvider("ucar.nc2.iosp.uamiv.UAMIVServiceProvider");
        }
        catch (Throwable e) {
            if (NetcdfFile.loadWarnings) {
                NetcdfFile.log.info("Cant load class: " + e);
            }
        }
        try {
            NetcdfFile.class.getClassLoader().loadClass("edu.wisc.ssec.mcidas.GridDirectory");
            registerIOProvider("ucar.nc2.iosp.mcidas.McIDASGridServiceProvider");
        }
        catch (Throwable e) {
            if (NetcdfFile.loadWarnings) {
                NetcdfFile.log.info("Cant load class: " + e);
            }
        }
        NetcdfFile.userLoads = true;
    }
}
