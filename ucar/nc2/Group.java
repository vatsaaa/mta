// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2;

import java.util.Collections;
import java.util.ArrayList;
import ucar.ma2.DataType;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

public class Group
{
    protected NetcdfFile ncfile;
    protected Group parent;
    protected String shortName;
    protected List<Variable> variables;
    protected List<Dimension> dimensions;
    protected List<Group> groups;
    protected List<Attribute> attributes;
    protected List<EnumTypedef> enumTypedefs;
    private boolean immutable;
    private int hashCode;
    
    public String getName() {
        return (this.parent == null || this.parent == this.ncfile.getRootGroup()) ? this.shortName : (this.parent.getName() + "/" + this.shortName);
    }
    
    public boolean isRoot() {
        return this.parent == null;
    }
    
    public String getShortName() {
        return this.shortName;
    }
    
    public Group getParentGroup() {
        return this.parent;
    }
    
    public List<Variable> getVariables() {
        return this.variables;
    }
    
    public Variable findVariable(final String varShortName) {
        if (varShortName == null) {
            return null;
        }
        for (final Variable v : this.variables) {
            if (varShortName.equals(v.getShortName())) {
                return v;
            }
        }
        return null;
    }
    
    public Variable findVariableOrInParent(final String varShortName) {
        if (varShortName == null) {
            return null;
        }
        Variable v = this.findVariable(varShortName);
        if (v == null && this.parent != null) {
            v = this.parent.findVariableOrInParent(varShortName);
        }
        return v;
    }
    
    public List<Group> getGroups() {
        return this.groups;
    }
    
    public NetcdfFile getNetcdfFile() {
        return this.ncfile;
    }
    
    public Group findGroup(String groupShortName) {
        if (groupShortName == null) {
            return null;
        }
        groupShortName = NetcdfFile.unescapeName(groupShortName);
        for (final Group group : this.groups) {
            if (groupShortName.equals(group.getShortName())) {
                return group;
            }
        }
        return null;
    }
    
    public List<Dimension> getDimensions() {
        return this.dimensions;
    }
    
    public List<EnumTypedef> getEnumTypedefs() {
        return this.enumTypedefs;
    }
    
    public Dimension findDimension(String name) {
        if (name == null) {
            return null;
        }
        name = NetcdfFile.unescapeName(name);
        final Dimension d = this.findDimensionLocal(name);
        if (d != null) {
            return d;
        }
        if (this.parent != null) {
            return this.parent.findDimension(name);
        }
        return null;
    }
    
    public Dimension findDimensionLocal(String name) {
        if (name == null) {
            return null;
        }
        name = NetcdfFile.unescapeName(name);
        for (final Dimension d : this.dimensions) {
            if (name.equals(d.getName())) {
                return d;
            }
        }
        return null;
    }
    
    public List<Attribute> getAttributes() {
        return this.attributes;
    }
    
    public Attribute findAttribute(String name) {
        if (name == null) {
            return null;
        }
        name = NetcdfFile.unescapeName(name);
        for (final Attribute a : this.attributes) {
            if (name.equals(a.getName())) {
                return a;
            }
        }
        return null;
    }
    
    public Attribute findAttributeIgnoreCase(String name) {
        if (name == null) {
            return null;
        }
        name = NetcdfFile.unescapeName(name);
        for (final Attribute a : this.attributes) {
            if (name.equalsIgnoreCase(a.getName())) {
                return a;
            }
        }
        return null;
    }
    
    public EnumTypedef findEnumeration(String name) {
        if (name == null) {
            return null;
        }
        name = NetcdfFile.unescapeName(name);
        for (final EnumTypedef d : this.enumTypedefs) {
            if (name.equals(d.getName())) {
                return d;
            }
        }
        if (this.parent != null) {
            return this.parent.findEnumeration(name);
        }
        return null;
    }
    
    public Group commonParent(Group other) {
        if (this.isParent(other)) {
            return this;
        }
        if (other.isParent(this)) {
            return other;
        }
        while (!other.isParent(this)) {
            other = other.getParentGroup();
        }
        return other;
    }
    
    public boolean isParent(Group other) {
        while (other != this && other.getParentGroup() != null) {
            other = other.getParentGroup();
        }
        return other == this;
    }
    
    public String getNameAndAttributes() {
        final StringBuilder sbuff = new StringBuilder();
        sbuff.append("Group ");
        sbuff.append(this.getShortName());
        sbuff.append("\n");
        for (final Attribute att : this.attributes) {
            sbuff.append("  ").append(this.getShortName()).append(":");
            sbuff.append(att.toString());
            sbuff.append(";");
            sbuff.append("\n");
        }
        return sbuff.toString();
    }
    
    protected void writeCDL(final PrintWriter out, final String indent, final boolean strict) {
        final boolean hasE = this.enumTypedefs.size() > 0;
        final boolean hasD = this.dimensions.size() > 0;
        final boolean hasV = this.variables.size() > 0;
        final boolean hasG = this.groups.size() > 0;
        final boolean hasA = this.attributes.size() > 0;
        if (hasE) {
            out.print(indent + " types:\n");
            for (final EnumTypedef e : this.enumTypedefs) {
                out.print(indent + e.writeCDL(strict));
                out.print(indent + "\n");
            }
            out.print(indent + "\n");
        }
        if (hasD) {
            out.print(indent + " dimensions:\n");
        }
        for (final Dimension myd : this.dimensions) {
            out.print(indent + myd.writeCDL(strict));
            out.print(indent + "\n");
        }
        if (hasV) {
            out.print(indent + " variables:\n");
        }
        for (final Variable v : this.variables) {
            out.print(v.writeCDL(indent + "   ", false, strict));
        }
        for (final Group g : this.groups) {
            final String gname = strict ? NetcdfFile.escapeName(g.getShortName()) : g.getShortName();
            out.print("\n " + indent + "Group " + gname + " {\n");
            g.writeCDL(out, indent + "  ", strict);
            out.print(indent + " }\n");
        }
        if (hasA && (hasE || hasD || hasV || hasG)) {
            out.print("\n");
        }
        if (hasA && strict) {
            out.print(indent + " // global attributes:\n");
        }
        for (final Attribute att : this.attributes) {
            final String name = strict ? NetcdfFile.escapeName(this.getShortName()) : this.getShortName();
            out.print(indent + " " + name + ":");
            out.print(att.toString(strict));
            out.print(";");
            if (!strict && att.getDataType() != DataType.STRING) {
                out.print(" // " + att.getDataType());
            }
            out.print("\n");
        }
    }
    
    public Group(final NetcdfFile ncfile, final Group parent, final String shortName) {
        this.variables = new ArrayList<Variable>();
        this.dimensions = new ArrayList<Dimension>();
        this.groups = new ArrayList<Group>();
        this.attributes = new ArrayList<Attribute>();
        this.enumTypedefs = new ArrayList<EnumTypedef>();
        this.immutable = false;
        this.hashCode = 0;
        this.ncfile = ncfile;
        this.parent = ((parent == null) ? ncfile.getRootGroup() : parent);
        this.shortName = shortName;
    }
    
    public void setParentGroup(final Group parent) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        this.parent = ((parent == null) ? this.ncfile.getRootGroup() : parent);
    }
    
    public void setName(final String shortName) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        this.shortName = shortName;
    }
    
    public void addAttribute(final Attribute att) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        for (int i = 0; i < this.attributes.size(); ++i) {
            final Attribute a = this.attributes.get(i);
            if (att.getName().equals(a.getName())) {
                this.attributes.set(i, att);
                return;
            }
        }
        this.attributes.add(att);
    }
    
    public void addDimension(final Dimension d) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        if (this.findDimensionLocal(d.getName()) != null) {
            throw new IllegalArgumentException("Variable name (" + d.getName() + ") must be unique within Group " + this.getName());
        }
        this.dimensions.add(d);
        d.setGroup(this);
    }
    
    public void addGroup(final Group g) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        if (this.findGroup(g.getShortName()) != null) {
            throw new IllegalArgumentException("Variable name (" + g.getShortName() + ") must be unique within Group " + this.getName());
        }
        this.groups.add(g);
        g.parent = this;
    }
    
    public void addEnumeration(final EnumTypedef e) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        if (e == null) {
            return;
        }
        this.enumTypedefs.add(e);
    }
    
    public void addVariable(final Variable v) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        if (v == null) {
            return;
        }
        if (this.findVariable(v.getShortName()) != null) {
            throw new IllegalArgumentException("Variable name (" + v.getShortName() + ") must be unique within Group " + this.getName());
        }
        this.variables.add(v);
        v.setParentGroup(this);
    }
    
    public boolean remove(final Attribute a) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        return a != null && this.attributes.remove(a);
    }
    
    public boolean remove(final Dimension d) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        return d != null && this.dimensions.remove(d);
    }
    
    public boolean remove(final Group g) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        return g != null && this.groups.remove(g);
    }
    
    public boolean remove(final Variable v) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        return v != null && this.variables.remove(v);
    }
    
    public boolean removeDimension(final String dimName) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        for (int i = 0; i < this.dimensions.size(); ++i) {
            final Dimension d = this.dimensions.get(i);
            if (dimName.equals(d.getName())) {
                this.dimensions.remove(d);
                return true;
            }
        }
        return false;
    }
    
    public boolean removeVariable(final String shortName) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        for (int i = 0; i < this.variables.size(); ++i) {
            final Variable v = this.variables.get(i);
            if (shortName.equals(v.getShortName())) {
                this.variables.remove(v);
                return true;
            }
        }
        return false;
    }
    
    public Group setImmutable() {
        this.immutable = true;
        this.variables = Collections.unmodifiableList((List<? extends Variable>)this.variables);
        this.dimensions = Collections.unmodifiableList((List<? extends Dimension>)this.dimensions);
        this.groups = Collections.unmodifiableList((List<? extends Group>)this.groups);
        this.attributes = Collections.unmodifiableList((List<? extends Attribute>)this.attributes);
        return this;
    }
    
    @Override
    public String toString() {
        return this.getName();
    }
    
    @Override
    public boolean equals(final Object oo) {
        if (this == oo) {
            return true;
        }
        if (!(oo instanceof Group)) {
            return false;
        }
        final Group og = (Group)oo;
        return this.getShortName().equals(og.getShortName()) && (this.getParentGroup() == null || this.getParentGroup().equals(og.getParentGroup()));
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int result = 17;
            result = 37 * result + this.getShortName().hashCode();
            if (this.getParentGroup() != null) {
                result = 37 * result + this.getParentGroup().hashCode();
            }
            this.hashCode = result;
        }
        return this.hashCode;
    }
}
