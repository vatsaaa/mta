// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.constants;

public class CF
{
    public static final String POSITIVE_UP = "up";
    public static final String POSITIVE_DOWN = "down";
    public static final String featureTypeAtt = "CF:featureType";
    public static final String featureTypeAtt2 = "CF-featureType";
    public static final String featureTypeAtt3 = "CFfeatureType";
    public static final String featureTypeAtt4 = "featureType";
    public static final String COORDINATES = "coordinates";
    public static final String GRID_MAPPING = "grid_mapping";
    public static final String GRID_MAPPING_NAME = "grid_mapping_name";
    public static final String STANDARD_NAME = "standard_name";
    public static final String UNITS = "units";
    public static final String RAGGED_ROWSIZE = "CF:ragged_row_count";
    public static final String RAGGED_PARENTINDEX = "CF:ragged_parent_index";
    public static final String STATION_ID = "station_id";
    public static final String STATION_DESC = "station_desc";
    public static final String STATION_ALTITUDE = "surface_altitude";
    public static final String STATION_WMOID = "station_WMO_id";
    public static final String TRAJ_ID = "trajectory_id";
    public static final String PROFILE_ID = "profile_id";
    
    public enum FeatureType
    {
        point, 
        timeSeries, 
        profile, 
        trajectory, 
        timeSeriesProfile, 
        trajectoryProfile;
        
        public static FeatureType convert(final ucar.nc2.constants.FeatureType ft) {
            switch (ft) {
                case POINT: {
                    return FeatureType.point;
                }
                case STATION: {
                    return FeatureType.timeSeries;
                }
                case PROFILE: {
                    return FeatureType.profile;
                }
                case TRAJECTORY: {
                    return FeatureType.trajectory;
                }
                case STATION_PROFILE: {
                    return FeatureType.timeSeriesProfile;
                }
                case SECTION: {
                    return FeatureType.trajectoryProfile;
                }
                default: {
                    return null;
                }
            }
        }
        
        public static FeatureType getFeatureType(final String s) {
            if (s.equalsIgnoreCase("point")) {
                return FeatureType.point;
            }
            if (s.equalsIgnoreCase("timeSeries")) {
                return FeatureType.timeSeries;
            }
            if (s.equalsIgnoreCase("stationTimeSeries")) {
                return FeatureType.timeSeries;
            }
            if (s.equalsIgnoreCase("station")) {
                return FeatureType.timeSeries;
            }
            if (s.equalsIgnoreCase("profile")) {
                return FeatureType.profile;
            }
            if (s.equalsIgnoreCase("trajectory")) {
                return FeatureType.trajectory;
            }
            if (s.equalsIgnoreCase("timeSeriesProfile")) {
                return FeatureType.timeSeriesProfile;
            }
            if (s.equalsIgnoreCase("stationProfile")) {
                return FeatureType.timeSeriesProfile;
            }
            if (s.equalsIgnoreCase("stationProfileTimeSeries")) {
                return FeatureType.timeSeriesProfile;
            }
            if (s.equalsIgnoreCase("trajectoryProfile")) {
                return FeatureType.trajectoryProfile;
            }
            if (s.equalsIgnoreCase("section")) {
                return FeatureType.trajectoryProfile;
            }
            return null;
        }
    }
    
    public enum CellMethods
    {
        point, 
        sum, 
        mean, 
        maximum, 
        minimum, 
        mid_range, 
        standard_deviation, 
        variance, 
        mode, 
        median;
        
        public static CellMethods convertGribCodeTable4_10(final int code) {
            switch (code) {
                case 0: {
                    return CellMethods.mean;
                }
                case 1: {
                    return CellMethods.sum;
                }
                case 2: {
                    return CellMethods.maximum;
                }
                case 3: {
                    return CellMethods.minimum;
                }
                case 6: {
                    return CellMethods.standard_deviation;
                }
                case 7: {
                    return CellMethods.variance;
                }
                default: {
                    return null;
                }
            }
        }
    }
}
