// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.constants;

public class _Coordinate
{
    public static final String AliasForDimension = "_CoordinateAliasForDimension";
    public static final String Axes = "_CoordinateAxes";
    public static final String AxisType = "_CoordinateAxisType";
    public static final String AxisTypes = "_CoordinateAxisTypes";
    public static final String Convention = "_Coordinates";
    public static final String ModelBaseDate = "_CoordinateModelBaseDate";
    public static final String ModelRunDate = "_CoordinateModelRunDate";
    public static final String SystemFor = "_CoordinateSystemFor";
    public static final String Systems = "_CoordinateSystems";
    public static final String Transforms = "_CoordinateTransforms";
    public static final String TransformType = "_CoordinateTransformType";
    public static final String ZisLayer = "_CoordinateZisLayer";
    public static final String ZisPositive = "_CoordinateZisPositive";
    
    private _Coordinate() {
    }
}
