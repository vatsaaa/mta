// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.constants;

public enum FeatureType
{
    ANY, 
    GRID, 
    RADIAL, 
    SWATH, 
    IMAGE, 
    ANY_POINT, 
    POINT, 
    PROFILE, 
    SECTION, 
    STATION, 
    STATION_PROFILE, 
    TRAJECTORY, 
    STATION_RADIAL, 
    NONE;
    
    public static FeatureType getType(final String name) {
        if (name == null) {
            return null;
        }
        try {
            return valueOf(name.toUpperCase());
        }
        catch (IllegalArgumentException e) {
            return null;
        }
    }
    
    public boolean isPointFeatureType() {
        return this == FeatureType.POINT || this == FeatureType.STATION || this == FeatureType.TRAJECTORY || this == FeatureType.PROFILE || this == FeatureType.STATION_PROFILE || this == FeatureType.SECTION;
    }
}
