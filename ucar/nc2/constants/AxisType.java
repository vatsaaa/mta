// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.constants;

public enum AxisType
{
    RunTime(0), 
    Ensemble(2), 
    Time(1), 
    GeoX(5), 
    GeoY(4), 
    GeoZ(3), 
    Lat(4), 
    Lon(5), 
    Height(3), 
    Pressure(3), 
    RadialAzimuth(7), 
    RadialDistance(8), 
    RadialElevation(6);
    
    private int order;
    
    private AxisType(final int order) {
        this.order = order;
    }
    
    public static AxisType getType(final String name) {
        if (name == null) {
            return null;
        }
        try {
            return valueOf(name);
        }
        catch (IllegalArgumentException e) {
            return null;
        }
    }
    
    public int axisOrder() {
        return this.order;
    }
}
