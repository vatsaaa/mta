// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.stream;

import ucar.nc2.ft.FeatureCollection;
import ucar.nc2.ft.FeatureDatasetPoint;
import java.io.InputStream;
import org.apache.commons.httpclient.HttpMethod;
import org.jdom.output.XMLOutputter;
import org.jdom.input.SAXBuilder;
import java.io.IOException;
import ucar.nc2.units.DateRange;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.VariableSimpleIF;
import java.util.List;
import org.jdom.Element;
import org.jdom.Document;
import ucar.nc2.ft.point.remote.PointDatasetRemote;
import ucar.nc2.ft.point.writer.FeatureDatasetPointXML;
import ucar.nc2.dt.grid.GridDataset;
import java.util.Set;
import ucar.nc2.NetcdfFile;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.ft.FeatureDataset;
import ucar.nc2.constants.FeatureType;

public class CdmRemoteFeatureDataset
{
    private static boolean debug;
    private static boolean showXML;
    
    public static FeatureDataset factory(final FeatureType wantFeatureType, String endpoint) throws IOException {
        if (endpoint.startsWith("cdmremote:")) {
            endpoint = endpoint.substring("cdmremote:".length());
        }
        final Document doc = getCapabilities(endpoint);
        final Element root = doc.getRootElement();
        final Element elem = root.getChild("featureDataset");
        final String fType = elem.getAttribute("type").getValue();
        final String uri = elem.getAttribute("url").getValue();
        if (CdmRemoteFeatureDataset.debug) {
            System.out.printf("CdmRemoteFeatureDataset endpoint %s%n ftype= %s uri=%s%n", endpoint, fType, uri);
        }
        final FeatureType ft = FeatureType.getType(fType);
        if (ft == null || ft == FeatureType.NONE || ft == FeatureType.GRID) {
            final CdmRemote ncremote = new CdmRemote(uri);
            final NetcdfDataset ncd = new NetcdfDataset(ncremote, null);
            return new GridDataset(ncd);
        }
        final List<VariableSimpleIF> dataVars = FeatureDatasetPointXML.getDataVariables(doc);
        final LatLonRect bb = FeatureDatasetPointXML.getSpatialExtent(doc);
        final DateRange dr = FeatureDatasetPointXML.getTimeSpan(doc);
        return new PointDatasetRemote(ft, uri, dataVars, bb, dr);
    }
    
    private static Document getCapabilities(final String endpoint) throws IOException {
        HttpMethod method = null;
        Document doc;
        try {
            method = CdmRemote.sendQuery(endpoint, "req=capabilities");
            final InputStream in = method.getResponseBodyAsStream();
            final SAXBuilder builder = new SAXBuilder(false);
            doc = builder.build(in);
        }
        catch (Throwable t) {
            throw new IOException(t);
        }
        finally {
            if (method != null) {
                method.releaseConnection();
            }
        }
        if (CdmRemoteFeatureDataset.showXML) {
            System.out.printf("*** endpoint = %s %n", endpoint);
            final XMLOutputter xmlOut = new XMLOutputter();
            System.out.printf("*** CdmRemoteFeatureDataset/showParsedXML = %n %s %n", xmlOut.outputString(doc));
        }
        return doc;
    }
    
    public static void main(final String[] args) throws IOException {
        final String endpoint = "http://localhost:8080/thredds/cdmremote/idd/metar/ncdecodedLocalHome";
        final FeatureDatasetPoint fd = (FeatureDatasetPoint)factory(FeatureType.ANY, endpoint);
        final FeatureCollection fc = fd.getPointFeatureCollectionList().get(0);
        System.out.printf("Result= %s %n %s %n", fd, fc);
    }
    
    static {
        CdmRemoteFeatureDataset.debug = true;
        CdmRemoteFeatureDataset.showXML = true;
    }
}
