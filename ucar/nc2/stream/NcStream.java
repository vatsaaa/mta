// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.stream;

import ucar.nc2.Sequence;
import java.util.ArrayList;
import ucar.ma2.DataType;
import java.util.List;
import java.util.HashMap;
import ucar.nc2.NetcdfFile;
import java.io.InputStream;
import java.nio.channels.WritableByteChannel;
import java.io.OutputStream;
import com.google.protobuf.InvalidProtocolBufferException;
import ucar.ma2.Range;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Section;
import java.util.Map;
import java.nio.ByteBuffer;
import ucar.ma2.Array;
import com.google.protobuf.ByteString;
import java.io.IOException;
import java.util.Iterator;
import ucar.nc2.Structure;
import ucar.nc2.Variable;
import ucar.nc2.EnumTypedef;
import ucar.nc2.Attribute;
import ucar.nc2.Dimension;
import ucar.nc2.Group;

public class NcStream
{
    static final byte[] MAGIC_START;
    static final byte[] MAGIC_HEADER;
    static final byte[] MAGIC_DATA;
    public static final byte[] MAGIC_ERR;
    public static final byte[] MAGIC_END;
    
    static NcStreamProto.Group.Builder encodeGroup(final Group g, final int sizeToCache) throws IOException {
        final NcStreamProto.Group.Builder groupBuilder = NcStreamProto.Group.newBuilder();
        groupBuilder.setName(g.getShortName());
        for (final Dimension dim : g.getDimensions()) {
            groupBuilder.addDims(encodeDim(dim));
        }
        for (final Attribute att : g.getAttributes()) {
            groupBuilder.addAtts(encodeAtt(att));
        }
        for (final EnumTypedef enumType : g.getEnumTypedefs()) {
            groupBuilder.addEnumTypes(encodeEnumTypedef(enumType));
        }
        for (final Variable var : g.getVariables()) {
            if (var instanceof Structure) {
                groupBuilder.addStructs(encodeStructure((Structure)var));
            }
            else {
                groupBuilder.addVars(encodeVar(var, sizeToCache));
            }
        }
        for (final Group ng : g.getGroups()) {
            groupBuilder.addGroups(encodeGroup(ng, sizeToCache));
        }
        return groupBuilder;
    }
    
    static NcStreamProto.Attribute.Builder encodeAtt(final Attribute att) {
        final NcStreamProto.Attribute.Builder attBuilder = NcStreamProto.Attribute.newBuilder();
        attBuilder.setName(att.getName());
        attBuilder.setType(encodeAttributeType(att.getDataType()));
        attBuilder.setLen(att.getLength());
        if (att.getLength() > 0) {
            if (att.isString()) {
                for (int i = 0; i < att.getLength(); ++i) {
                    attBuilder.addSdata(att.getStringValue(i));
                }
            }
            else {
                final Array data = att.getValues();
                final ByteBuffer bb = data.getDataAsByteBuffer();
                attBuilder.setData(ByteString.copyFrom(bb.array()));
            }
        }
        return attBuilder;
    }
    
    static NcStreamProto.Dimension.Builder encodeDim(final Dimension dim) {
        final NcStreamProto.Dimension.Builder dimBuilder = NcStreamProto.Dimension.newBuilder();
        dimBuilder.setName((dim.getName() == null) ? "" : dim.getName());
        dimBuilder.setLength(dim.getLength());
        if (!dim.isShared()) {
            dimBuilder.setIsPrivate(true);
        }
        if (dim.isVariableLength()) {
            dimBuilder.setIsVlen(true);
        }
        if (dim.isUnlimited()) {
            dimBuilder.setIsUnlimited(true);
        }
        return dimBuilder;
    }
    
    static NcStreamProto.EnumTypedef.Builder encodeEnumTypedef(final EnumTypedef enumType) throws IOException {
        final NcStreamProto.EnumTypedef.Builder builder = NcStreamProto.EnumTypedef.newBuilder();
        builder.setName(enumType.getName());
        final Map<Integer, String> map = enumType.getMap();
        final NcStreamProto.EnumTypedef.EnumType.Builder b2 = NcStreamProto.EnumTypedef.EnumType.newBuilder();
        for (final int code : map.keySet()) {
            b2.clear();
            b2.setCode(code);
            b2.setValue(map.get(code));
            builder.addMap(b2);
        }
        return builder;
    }
    
    static NcStreamProto.Variable.Builder encodeVar(final Variable var, final int sizeToCache) throws IOException {
        final NcStreamProto.Variable.Builder builder = NcStreamProto.Variable.newBuilder();
        builder.setName(var.getShortName());
        builder.setDataType(encodeDataType(var.getDataType()));
        if (var.isUnsigned()) {
            builder.setUnsigned(true);
        }
        if (var.getDataType().isEnum()) {
            final EnumTypedef enumType = var.getEnumTypedef();
            if (enumType != null) {
                builder.setEnumType(enumType.getName());
            }
        }
        for (final Dimension dim : var.getDimensions()) {
            builder.addShape(encodeDim(dim));
        }
        for (final Attribute att : var.getAttributes()) {
            builder.addAtts(encodeAtt(att));
        }
        if (var.isCaching() && var.getDataType().isNumeric() && (var.isCoordinateVariable() || var.getSize() * var.getElementSize() < sizeToCache)) {
            final Array data = var.read();
            final ByteBuffer bb = data.getDataAsByteBuffer();
            builder.setData(ByteString.copyFrom(bb.array()));
        }
        return builder;
    }
    
    static NcStreamProto.Structure.Builder encodeStructure(final Structure s) throws IOException {
        final NcStreamProto.Structure.Builder builder = NcStreamProto.Structure.newBuilder();
        builder.setName(s.getShortName());
        builder.setDataType(encodeDataType(s.getDataType()));
        for (final Dimension dim : s.getDimensions()) {
            builder.addShape(encodeDim(dim));
        }
        for (final Attribute att : s.getAttributes()) {
            builder.addAtts(encodeAtt(att));
        }
        for (final Variable v : s.getVariables()) {
            if (v instanceof Structure) {
                builder.addStructs(encodeStructure((Structure)v));
            }
            else {
                builder.addVars(encodeVar(v, -1));
            }
        }
        return builder;
    }
    
    public static NcStreamProto.Error encodeErrorMessage(final String message) throws IOException {
        final NcStreamProto.Error.Builder builder = NcStreamProto.Error.newBuilder();
        builder.setMessage(message);
        return builder.build();
    }
    
    static NcStreamProto.Data encodeDataProto(final Variable var, final Section section) throws IOException, InvalidRangeException {
        final NcStreamProto.Data.Builder builder = NcStreamProto.Data.newBuilder();
        builder.setVarName(var.getName());
        builder.setDataType(encodeDataType(var.getDataType()));
        builder.setSection(encodeSection(section));
        return builder.build();
    }
    
    public static NcStreamProto.Section encodeSection(final Section section) {
        final NcStreamProto.Section.Builder sbuilder = NcStreamProto.Section.newBuilder();
        for (final Range r : section.getRanges()) {
            final NcStreamProto.Range.Builder rbuilder = NcStreamProto.Range.newBuilder();
            rbuilder.setSize(r.length());
            sbuilder.addRange(rbuilder);
        }
        return sbuilder.build();
    }
    
    static void show(final NcStreamProto.Header proto) throws InvalidProtocolBufferException {
        final NcStreamProto.Group root = proto.getRoot();
        for (final NcStreamProto.Dimension dim : root.getDimsList()) {
            System.out.println("dim= " + dim);
        }
        for (final NcStreamProto.Attribute att : root.getAttsList()) {
            System.out.println("att= " + att);
        }
        for (final NcStreamProto.Variable var : root.getVarsList()) {
            System.out.println("var= " + var);
        }
    }
    
    static int writeByte(final OutputStream out, final byte b) throws IOException {
        out.write(b);
        return 1;
    }
    
    static int writeBytes(final OutputStream out, final byte[] b, final int offset, final int length) throws IOException {
        out.write(b, offset, length);
        return length;
    }
    
    public static int writeBytes(final OutputStream out, final byte[] b) throws IOException {
        return writeBytes(out, b, 0, b.length);
    }
    
    public static int writeVInt(final OutputStream out, int value) throws IOException {
        final int count = 0;
        while ((value & 0xFFFFFF80) != 0x0) {
            writeByte(out, (byte)((value & 0x7F) | 0x80));
            value >>>= 7;
        }
        writeByte(out, (byte)value);
        return count + 1;
    }
    
    public static int writeVInt(final WritableByteChannel wbc, int value) throws IOException {
        final ByteBuffer bb = ByteBuffer.allocate(8);
        while ((value & 0xFFFFFF80) != 0x0) {
            bb.put((byte)((value & 0x7F) | 0x80));
            value >>>= 7;
        }
        bb.put((byte)value);
        bb.flip();
        wbc.write(bb);
        return bb.limit();
    }
    
    static int writeVLong(final OutputStream out, long i) throws IOException {
        int count;
        for (count = 0; (i & 0xFFFFFFFFFFFFFF80L) != 0x0L; i >>>= 7, ++count) {
            writeByte(out, (byte)((i & 0x7FL) | 0x80L));
        }
        writeByte(out, (byte)i);
        return count + 1;
    }
    
    public static int readVInt(final InputStream is) throws IOException {
        int ib = is.read();
        if (ib == -1) {
            return -1;
        }
        byte b = (byte)ib;
        int i = b & 0x7F;
        for (int shift = 7; (b & 0x80) != 0x0; b = (byte)ib, i |= (b & 0x7F) << shift, shift += 7) {
            ib = is.read();
            if (ib == -1) {
                return -1;
            }
        }
        return i;
    }
    
    public static int readFully(final InputStream is, final byte[] b) throws IOException {
        int done = 0;
        int bytesRead;
        for (int want = b.length; want > 0; want -= bytesRead) {
            bytesRead = is.read(b, done, want);
            if (bytesRead == -1) {
                break;
            }
            done += bytesRead;
        }
        return done;
    }
    
    public static boolean readAndTest(final InputStream is, final byte[] test) throws IOException {
        final byte[] b = new byte[test.length];
        readFully(is, b);
        if (b.length != test.length) {
            return false;
        }
        for (int i = 0; i < b.length; ++i) {
            if (b[i] != test[i]) {
                return false;
            }
        }
        return true;
    }
    
    public static String decodeErrorMessage(final NcStreamProto.Error err) {
        return err.getMessage();
    }
    
    static Dimension decodeDim(final NcStreamProto.Dimension dim) {
        final String name = (dim.getName() == "") ? null : dim.getName();
        return new Dimension(name, (int)dim.getLength(), !dim.getIsPrivate(), dim.getIsUnlimited(), dim.getIsVlen());
    }
    
    static void readGroup(final NcStreamProto.Group proto, final NetcdfFile ncfile, final Group g) throws InvalidProtocolBufferException {
        for (final NcStreamProto.Dimension dim : proto.getDimsList()) {
            g.addDimension(decodeDim(dim));
        }
        for (final NcStreamProto.Attribute att : proto.getAttsList()) {
            g.addAttribute(decodeAtt(att));
        }
        for (final NcStreamProto.EnumTypedef enumType : proto.getEnumTypesList()) {
            g.addEnumeration(decodeEnumTypedef(enumType));
        }
        for (final NcStreamProto.Variable var : proto.getVarsList()) {
            g.addVariable(decodeVar(ncfile, g, null, var));
        }
        for (final NcStreamProto.Structure s : proto.getStructsList()) {
            g.addVariable(decodeStructure(ncfile, g, null, s));
        }
        for (final NcStreamProto.Group gp : proto.getGroupsList()) {
            final Group ng = new Group(ncfile, g, gp.getName());
            g.addGroup(ng);
            readGroup(gp, ncfile, ng);
        }
    }
    
    static EnumTypedef decodeEnumTypedef(final NcStreamProto.EnumTypedef enumType) {
        final List<NcStreamProto.EnumTypedef.EnumType> list = enumType.getMapList();
        final Map<Integer, String> map = new HashMap<Integer, String>(2 * list.size());
        for (final NcStreamProto.EnumTypedef.EnumType et : list) {
            map.put(et.getCode(), et.getValue());
        }
        return new EnumTypedef(enumType.getName(), map);
    }
    
    static Attribute decodeAtt(final NcStreamProto.Attribute attp) {
        final int len = attp.getLen();
        if (len == 0) {
            return new Attribute(attp.getName(), decodeAttributeType(attp.getType()));
        }
        final DataType dt = decodeAttributeType(attp.getType());
        if (dt != DataType.STRING) {
            final ByteString bs = attp.getData();
            final ByteBuffer bb = ByteBuffer.wrap(bs.toByteArray());
            return new Attribute(attp.getName(), Array.factory(decodeAttributeType(attp.getType()), null, bb));
        }
        final int lenp = attp.getSdataCount();
        if (lenp != len) {
            System.out.println("HEY lenp != len");
        }
        if (lenp == 1) {
            return new Attribute(attp.getName(), attp.getSdata(0));
        }
        final Array data = Array.factory(dt, new int[] { lenp });
        for (int i = 0; i < lenp; ++i) {
            data.setObject(i, attp.getSdata(i));
        }
        return new Attribute(attp.getName(), data);
    }
    
    static Variable decodeVar(final NetcdfFile ncfile, final Group g, final Structure parent, final NcStreamProto.Variable var) {
        final Variable ncvar = new Variable(ncfile, g, parent, var.getName());
        final DataType varType = decodeDataType(var.getDataType());
        ncvar.setDataType(decodeDataType(var.getDataType()));
        if (varType.isEnum()) {
            final String enumName = var.getEnumType();
            final EnumTypedef enumType = g.findEnumeration(enumName);
            if (enumType != null) {
                ncvar.setEnumTypedef(enumType);
            }
        }
        final List<Dimension> dims = new ArrayList<Dimension>(6);
        for (final NcStreamProto.Dimension dim : var.getShapeList()) {
            if (dim.getIsPrivate()) {
                dims.add(new Dimension(dim.getName(), (int)dim.getLength(), false, dim.getIsUnlimited(), dim.getIsVlen()));
            }
            else {
                final Dimension d = g.findDimension(dim.getName());
                if (d == null) {
                    throw new IllegalStateException("Can find shared dimension " + dim.getName());
                }
                dims.add(d);
            }
        }
        ncvar.setDimensions(dims);
        for (final NcStreamProto.Attribute att : var.getAttsList()) {
            ncvar.addAttribute(decodeAtt(att));
        }
        if (var.getUnsigned()) {
            ncvar.addAttribute(new Attribute("_Unsigned", "true"));
        }
        if (var.hasData()) {
            final ByteBuffer bb = ByteBuffer.wrap(var.getData().toByteArray());
            final Array data = Array.factory(varType, ncvar.getShape(), bb);
            ncvar.setCachedData(data, false);
        }
        return ncvar;
    }
    
    static Structure decodeStructure(final NetcdfFile ncfile, final Group g, final Structure parent, final NcStreamProto.Structure s) {
        final Structure ncvar = (s.getDataType() == NcStreamProto.DataType.SEQUENCE) ? new Sequence(ncfile, g, parent, s.getName()) : new Structure(ncfile, g, parent, s.getName());
        ncvar.setDataType(decodeDataType(s.getDataType()));
        final List<Dimension> dims = new ArrayList<Dimension>(6);
        for (final NcStreamProto.Dimension dim : s.getShapeList()) {
            if (dim.getIsPrivate()) {
                dims.add(new Dimension(dim.getName(), (int)dim.getLength(), false, dim.getIsUnlimited(), dim.getIsVlen()));
            }
            else {
                final Dimension d = g.findDimension(dim.getName());
                if (d == null) {
                    throw new IllegalStateException("Can find shared dimension " + dim.getName());
                }
                dims.add(d);
            }
        }
        ncvar.setDimensions(dims);
        for (final NcStreamProto.Attribute att : s.getAttsList()) {
            ncvar.addAttribute(decodeAtt(att));
        }
        for (final NcStreamProto.Variable vp : s.getVarsList()) {
            ncvar.addMemberVariable(decodeVar(ncfile, g, ncvar, vp));
        }
        for (final NcStreamProto.Structure sp : s.getStructsList()) {
            ncvar.addMemberVariable(decodeStructure(ncfile, g, ncvar, sp));
        }
        return ncvar;
    }
    
    public static Section decodeSection(final NcStreamProto.Section proto) {
        final Section section = new Section();
        for (final NcStreamProto.Range pr : proto.getRangeList()) {
            try {
                section.appendRange((int)pr.getStart(), (int)(pr.getStart() + pr.getSize() - 1L));
            }
            catch (InvalidRangeException e) {
                throw new RuntimeException(e);
            }
        }
        return section;
    }
    
    static NcStreamProto.Attribute.Type encodeAttributeType(final DataType dtype) {
        switch (dtype) {
            case CHAR:
            case STRING: {
                return NcStreamProto.Attribute.Type.STRING;
            }
            case BYTE: {
                return NcStreamProto.Attribute.Type.BYTE;
            }
            case SHORT: {
                return NcStreamProto.Attribute.Type.SHORT;
            }
            case INT: {
                return NcStreamProto.Attribute.Type.INT;
            }
            case LONG: {
                return NcStreamProto.Attribute.Type.LONG;
            }
            case FLOAT: {
                return NcStreamProto.Attribute.Type.FLOAT;
            }
            case DOUBLE: {
                return NcStreamProto.Attribute.Type.DOUBLE;
            }
            default: {
                throw new IllegalStateException("illegal att type " + dtype);
            }
        }
    }
    
    public static NcStreamProto.DataType encodeDataType(final DataType dtype) {
        switch (dtype) {
            case CHAR: {
                return NcStreamProto.DataType.CHAR;
            }
            case BYTE: {
                return NcStreamProto.DataType.BYTE;
            }
            case SHORT: {
                return NcStreamProto.DataType.SHORT;
            }
            case INT: {
                return NcStreamProto.DataType.INT;
            }
            case LONG: {
                return NcStreamProto.DataType.LONG;
            }
            case FLOAT: {
                return NcStreamProto.DataType.FLOAT;
            }
            case DOUBLE: {
                return NcStreamProto.DataType.DOUBLE;
            }
            case STRING: {
                return NcStreamProto.DataType.STRING;
            }
            case STRUCTURE: {
                return NcStreamProto.DataType.STRUCTURE;
            }
            case SEQUENCE: {
                return NcStreamProto.DataType.SEQUENCE;
            }
            case ENUM1: {
                return NcStreamProto.DataType.ENUM1;
            }
            case ENUM2: {
                return NcStreamProto.DataType.ENUM2;
            }
            case ENUM4: {
                return NcStreamProto.DataType.ENUM4;
            }
            case OPAQUE: {
                return NcStreamProto.DataType.OPAQUE;
            }
            default: {
                throw new IllegalStateException("illegal data type " + dtype);
            }
        }
    }
    
    static DataType decodeAttributeType(final NcStreamProto.Attribute.Type dtype) {
        switch (dtype) {
            case STRING: {
                return DataType.STRING;
            }
            case BYTE: {
                return DataType.BYTE;
            }
            case SHORT: {
                return DataType.SHORT;
            }
            case INT: {
                return DataType.INT;
            }
            case LONG: {
                return DataType.LONG;
            }
            case FLOAT: {
                return DataType.FLOAT;
            }
            case DOUBLE: {
                return DataType.DOUBLE;
            }
            default: {
                throw new IllegalStateException("illegal att type " + dtype);
            }
        }
    }
    
    public static DataType decodeDataType(final NcStreamProto.DataType dtype) {
        switch (dtype) {
            case CHAR: {
                return DataType.CHAR;
            }
            case BYTE: {
                return DataType.BYTE;
            }
            case SHORT: {
                return DataType.SHORT;
            }
            case INT: {
                return DataType.INT;
            }
            case LONG: {
                return DataType.LONG;
            }
            case FLOAT: {
                return DataType.FLOAT;
            }
            case DOUBLE: {
                return DataType.DOUBLE;
            }
            case STRING: {
                return DataType.STRING;
            }
            case STRUCTURE: {
                return DataType.STRUCTURE;
            }
            case SEQUENCE: {
                return DataType.SEQUENCE;
            }
            case ENUM1: {
                return DataType.ENUM1;
            }
            case ENUM2: {
                return DataType.ENUM2;
            }
            case ENUM4: {
                return DataType.ENUM4;
            }
            case OPAQUE: {
                return DataType.OPAQUE;
            }
            default: {
                throw new IllegalStateException("illegal data type " + dtype);
            }
        }
    }
    
    static {
        MAGIC_START = new byte[] { 67, 68, 70, 83 };
        MAGIC_HEADER = new byte[] { -83, -20, -50, -38 };
        MAGIC_DATA = new byte[] { -85, -20, -50, -70 };
        MAGIC_ERR = new byte[] { -85, -83, -70, -38 };
        MAGIC_END = new byte[] { -19, -19, -34, -34 };
    }
}
