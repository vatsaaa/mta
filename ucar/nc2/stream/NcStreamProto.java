// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.stream;

import com.google.protobuf.AbstractMessage;
import com.google.protobuf.UnknownFieldSet;
import java.util.Collection;
import java.util.ArrayList;
import com.google.protobuf.UninitializedMessageException;
import com.google.protobuf.Message;
import com.google.protobuf.CodedInputStream;
import java.io.InputStream;
import com.google.protobuf.InvalidProtocolBufferException;
import java.io.IOException;
import java.util.Iterator;
import com.google.protobuf.CodedOutputStream;
import java.util.Collections;
import java.util.List;
import com.google.protobuf.ByteString;
import com.google.protobuf.ProtocolMessageEnum;
import com.google.protobuf.ExtensionRegistry;
import com.google.protobuf.GeneratedMessage;
import com.google.protobuf.Descriptors;

public final class NcStreamProto
{
    private static Descriptors.Descriptor internal_static_ncstream_Attribute_descriptor;
    private static GeneratedMessage.FieldAccessorTable internal_static_ncstream_Attribute_fieldAccessorTable;
    private static Descriptors.Descriptor internal_static_ncstream_Dimension_descriptor;
    private static GeneratedMessage.FieldAccessorTable internal_static_ncstream_Dimension_fieldAccessorTable;
    private static Descriptors.Descriptor internal_static_ncstream_Variable_descriptor;
    private static GeneratedMessage.FieldAccessorTable internal_static_ncstream_Variable_fieldAccessorTable;
    private static Descriptors.Descriptor internal_static_ncstream_Structure_descriptor;
    private static GeneratedMessage.FieldAccessorTable internal_static_ncstream_Structure_fieldAccessorTable;
    private static Descriptors.Descriptor internal_static_ncstream_EnumTypedef_descriptor;
    private static GeneratedMessage.FieldAccessorTable internal_static_ncstream_EnumTypedef_fieldAccessorTable;
    private static Descriptors.Descriptor internal_static_ncstream_EnumTypedef_EnumType_descriptor;
    private static GeneratedMessage.FieldAccessorTable internal_static_ncstream_EnumTypedef_EnumType_fieldAccessorTable;
    private static Descriptors.Descriptor internal_static_ncstream_Group_descriptor;
    private static GeneratedMessage.FieldAccessorTable internal_static_ncstream_Group_fieldAccessorTable;
    private static Descriptors.Descriptor internal_static_ncstream_Header_descriptor;
    private static GeneratedMessage.FieldAccessorTable internal_static_ncstream_Header_fieldAccessorTable;
    private static Descriptors.Descriptor internal_static_ncstream_Data_descriptor;
    private static GeneratedMessage.FieldAccessorTable internal_static_ncstream_Data_fieldAccessorTable;
    private static Descriptors.Descriptor internal_static_ncstream_Range_descriptor;
    private static GeneratedMessage.FieldAccessorTable internal_static_ncstream_Range_fieldAccessorTable;
    private static Descriptors.Descriptor internal_static_ncstream_Section_descriptor;
    private static GeneratedMessage.FieldAccessorTable internal_static_ncstream_Section_fieldAccessorTable;
    private static Descriptors.Descriptor internal_static_ncstream_Error_descriptor;
    private static GeneratedMessage.FieldAccessorTable internal_static_ncstream_Error_fieldAccessorTable;
    private static Descriptors.FileDescriptor descriptor;
    
    private NcStreamProto() {
    }
    
    public static void registerAllExtensions(final ExtensionRegistry registry) {
    }
    
    public static Descriptors.FileDescriptor getDescriptor() {
        return NcStreamProto.descriptor;
    }
    
    static {
        final String descriptorData = "\n\u001eucar/nc2/stream/ncStream.proto\u0012\bncstream\"¾\u0001\n\tAttribute\u0012\f\n\u0004name\u0018\u0001 \u0002(\t\u0012&\n\u0004type\u0018\u0002 \u0002(\u000e2\u0018.ncstream.Attribute.Type\u0012\u000b\n\u0003len\u0018\u0003 \u0002(\r\u0012\f\n\u0004data\u0018\u0004 \u0001(\f\u0012\r\n\u0005sdata\u0018\u0005 \u0003(\t\"Q\n\u0004Type\u0012\n\n\u0006STRING\u0010\u0000\u0012\b\n\u0004BYTE\u0010\u0001\u0012\t\n\u0005SHORT\u0010\u0002\u0012\u0007\n\u0003INT\u0010\u0003\u0012\b\n\u0004LONG\u0010\u0004\u0012\t\n\u0005FLOAT\u0010\u0005\u0012\n\n\u0006DOUBLE\u0010\u0006\"a\n\tDimension\u0012\f\n\u0004name\u0018\u0001 \u0001(\t\u0012\u000e\n\u0006length\u0018\u0002 \u0001(\u0004\u0012\u0013\n\u000bisUnlimited\u0018\u0003 \u0001(\b\u0012\u000e\n\u0006isVlen\u0018\u0004 \u0001(\b\u0012\u0011\n\tisPrivate\u0018\u0005 \u0001(\b\"·\u0001\n\bVariable\u0012\f\n\u0004name\u0018\u0001 \u0002(\t\u0012$\n\bdataType\u0018\u0002 \u0002(\u000e2\u0012.ncstream.DataType\u0012\"\n\u0005shape\u0018\u0003 \u0003(\u000b2\u0013.ncstream.Dimension\u0012!\n\u0004atts\u0018\u0004 \u0003(\u000b2\u0013.ncstream.Attribute\u0012\u0010\n\bunsigned\u0018\u0005 \u0001(\b\u0012\f\n\u0004data\u0018\u0006 \u0001(\f\u0012\u0010\n\benumType\u0018\u0007 \u0001(\t\"\u00ce\u0001\n\tStructure\u0012\f\n\u0004name\u0018\u0001 \u0002(\t\u0012$\n\bdataType\u0018\u0002 \u0002(\u000e2\u0012.ncstream.DataType\u0012\"\n\u0005shape\u0018\u0003 \u0003(\u000b2\u0013.ncstream.Dimension\u0012!\n\u0004atts\u0018\u0004 \u0003(\u000b2\u0013.ncstream.Attribute\u0012 \n\u0004vars\u0018\u0005 \u0003(\u000b2\u0012.ncstream.Variable\u0012$\n\u0007structs\u0018\u0006 \u0003(\u000b2\u0013.ncstream.Structure\"q\n\u000bEnumTypedef\u0012\f\n\u0004name\u0018\u0001 \u0002(\t\u0012+\n\u0003map\u0018\u0002 \u0003(\u000b2\u001e.ncstream.EnumTypedef.EnumType\u001a'\n\bEnumType\u0012\f\n\u0004code\u0018\u0001 \u0002(\r\u0012\r\n\u0005value\u0018\u0002 \u0002(\t\"\u00ee\u0001\n\u0005Group\u0012\f\n\u0004name\u0018\u0001 \u0002(\t\u0012!\n\u0004dims\u0018\u0002 \u0003(\u000b2\u0013.ncstream.Dimension\u0012 \n\u0004vars\u0018\u0003 \u0003(\u000b2\u0012.ncstream.Variable\u0012$\n\u0007structs\u0018\u0004 \u0003(\u000b2\u0013.ncstream.Structure\u0012!\n\u0004atts\u0018\u0005 \u0003(\u000b2\u0013.ncstream.Attribute\u0012\u001f\n\u0006groups\u0018\u0006 \u0003(\u000b2\u000f.ncstream.Group\u0012(\n\tenumTypes\u0018\u0007 \u0003(\u000b2\u0015.ncstream.EnumTypedef\"T\n\u0006Header\u0012\u0010\n\blocation\u0018\u0001 \u0001(\t\u0012\r\n\u0005title\u0018\u0002 \u0001(\t\u0012\n\n\u0002id\u0018\u0003 \u0001(\t\u0012\u001d\n\u0004root\u0018\u0004 \u0002(\u000b2\u000f.ncstream.Group\"w\n\u0004Data\u0012\u000f\n\u0007varName\u0018\u0001 \u0002(\t\u0012$\n\bdataType\u0018\u0002 \u0002(\u000e2\u0012.ncstream.DataType\u0012\"\n\u0007section\u0018\u0003 \u0002(\u000b2\u0011.ncstream.Section\u0012\u0014\n\u0006bigend\u0018\u0004 \u0001(\b:\u0004true\"4\n\u0005Range\u0012\r\n\u0005start\u0018\u0001 \u0001(\u0004\u0012\f\n\u0004size\u0018\u0002 \u0002(\u0004\u0012\u000e\n\u0006stride\u0018\u0003 \u0001(\u0004\")\n\u0007Section\u0012\u001e\n\u0005range\u0018\u0001 \u0003(\u000b2\u000f.ncstream.Range\"\u0018\n\u0005Error\u0012\u000f\n\u0007message\u0018\u0001 \u0002(\t*©\u0001\n\bDataType\u0012\b\n\u0004CHAR\u0010\u0000\u0012\b\n\u0004BYTE\u0010\u0001\u0012\t\n\u0005SHORT\u0010\u0002\u0012\u0007\n\u0003INT\u0010\u0003\u0012\b\n\u0004LONG\u0010\u0004\u0012\t\n\u0005FLOAT\u0010\u0005\u0012\n\n\u0006DOUBLE\u0010\u0006\u0012\n\n\u0006STRING\u0010\u0007\u0012\r\n\tSTRUCTURE\u0010\b\u0012\f\n\bSEQUENCE\u0010\t\u0012\t\n\u0005ENUM1\u0010\n\u0012\t\n\u0005ENUM2\u0010\u000b\u0012\t\n\u0005ENUM4\u0010\f\u0012\n\n\u0006OPAQUE\u0010\rB \n\u000fucar.nc2.streamB\rNcStreamProto";
        final Descriptors.FileDescriptor.InternalDescriptorAssigner assigner = (Descriptors.FileDescriptor.InternalDescriptorAssigner)new Descriptors.FileDescriptor.InternalDescriptorAssigner() {
            public ExtensionRegistry assignDescriptors(final Descriptors.FileDescriptor root) {
                NcStreamProto.descriptor = root;
                NcStreamProto.internal_static_ncstream_Attribute_descriptor = NcStreamProto.getDescriptor().getMessageTypes().get(0);
                NcStreamProto.internal_static_ncstream_Attribute_fieldAccessorTable = new GeneratedMessage.FieldAccessorTable(NcStreamProto.internal_static_ncstream_Attribute_descriptor, new String[] { "Name", "Type", "Len", "Data", "Sdata" }, (Class)Attribute.class, (Class)Attribute.Builder.class);
                NcStreamProto.internal_static_ncstream_Dimension_descriptor = NcStreamProto.getDescriptor().getMessageTypes().get(1);
                NcStreamProto.internal_static_ncstream_Dimension_fieldAccessorTable = new GeneratedMessage.FieldAccessorTable(NcStreamProto.internal_static_ncstream_Dimension_descriptor, new String[] { "Name", "Length", "IsUnlimited", "IsVlen", "IsPrivate" }, (Class)Dimension.class, (Class)Dimension.Builder.class);
                NcStreamProto.internal_static_ncstream_Variable_descriptor = NcStreamProto.getDescriptor().getMessageTypes().get(2);
                NcStreamProto.internal_static_ncstream_Variable_fieldAccessorTable = new GeneratedMessage.FieldAccessorTable(NcStreamProto.internal_static_ncstream_Variable_descriptor, new String[] { "Name", "DataType", "Shape", "Atts", "Unsigned", "Data", "EnumType" }, (Class)Variable.class, (Class)Variable.Builder.class);
                NcStreamProto.internal_static_ncstream_Structure_descriptor = NcStreamProto.getDescriptor().getMessageTypes().get(3);
                NcStreamProto.internal_static_ncstream_Structure_fieldAccessorTable = new GeneratedMessage.FieldAccessorTable(NcStreamProto.internal_static_ncstream_Structure_descriptor, new String[] { "Name", "DataType", "Shape", "Atts", "Vars", "Structs" }, (Class)Structure.class, (Class)Structure.Builder.class);
                NcStreamProto.internal_static_ncstream_EnumTypedef_descriptor = NcStreamProto.getDescriptor().getMessageTypes().get(4);
                NcStreamProto.internal_static_ncstream_EnumTypedef_fieldAccessorTable = new GeneratedMessage.FieldAccessorTable(NcStreamProto.internal_static_ncstream_EnumTypedef_descriptor, new String[] { "Name", "Map" }, (Class)EnumTypedef.class, (Class)EnumTypedef.Builder.class);
                NcStreamProto.internal_static_ncstream_EnumTypedef_EnumType_descriptor = NcStreamProto.internal_static_ncstream_EnumTypedef_descriptor.getNestedTypes().get(0);
                NcStreamProto.internal_static_ncstream_EnumTypedef_EnumType_fieldAccessorTable = new GeneratedMessage.FieldAccessorTable(NcStreamProto.internal_static_ncstream_EnumTypedef_EnumType_descriptor, new String[] { "Code", "Value" }, (Class)EnumTypedef.EnumType.class, (Class)EnumTypedef.EnumType.Builder.class);
                NcStreamProto.internal_static_ncstream_Group_descriptor = NcStreamProto.getDescriptor().getMessageTypes().get(5);
                NcStreamProto.internal_static_ncstream_Group_fieldAccessorTable = new GeneratedMessage.FieldAccessorTable(NcStreamProto.internal_static_ncstream_Group_descriptor, new String[] { "Name", "Dims", "Vars", "Structs", "Atts", "Groups", "EnumTypes" }, (Class)Group.class, (Class)Group.Builder.class);
                NcStreamProto.internal_static_ncstream_Header_descriptor = NcStreamProto.getDescriptor().getMessageTypes().get(6);
                NcStreamProto.internal_static_ncstream_Header_fieldAccessorTable = new GeneratedMessage.FieldAccessorTable(NcStreamProto.internal_static_ncstream_Header_descriptor, new String[] { "Location", "Title", "Id", "Root" }, (Class)Header.class, (Class)Header.Builder.class);
                NcStreamProto.internal_static_ncstream_Data_descriptor = NcStreamProto.getDescriptor().getMessageTypes().get(7);
                NcStreamProto.internal_static_ncstream_Data_fieldAccessorTable = new GeneratedMessage.FieldAccessorTable(NcStreamProto.internal_static_ncstream_Data_descriptor, new String[] { "VarName", "DataType", "Section", "Bigend" }, (Class)Data.class, (Class)Data.Builder.class);
                NcStreamProto.internal_static_ncstream_Range_descriptor = NcStreamProto.getDescriptor().getMessageTypes().get(8);
                NcStreamProto.internal_static_ncstream_Range_fieldAccessorTable = new GeneratedMessage.FieldAccessorTable(NcStreamProto.internal_static_ncstream_Range_descriptor, new String[] { "Start", "Size", "Stride" }, (Class)Range.class, (Class)Range.Builder.class);
                NcStreamProto.internal_static_ncstream_Section_descriptor = NcStreamProto.getDescriptor().getMessageTypes().get(9);
                NcStreamProto.internal_static_ncstream_Section_fieldAccessorTable = new GeneratedMessage.FieldAccessorTable(NcStreamProto.internal_static_ncstream_Section_descriptor, new String[] { "Range" }, (Class)Section.class, (Class)Section.Builder.class);
                NcStreamProto.internal_static_ncstream_Error_descriptor = NcStreamProto.getDescriptor().getMessageTypes().get(10);
                NcStreamProto.internal_static_ncstream_Error_fieldAccessorTable = new GeneratedMessage.FieldAccessorTable(NcStreamProto.internal_static_ncstream_Error_descriptor, new String[] { "Message" }, (Class)Error.class, (Class)Error.Builder.class);
                return null;
            }
        };
        Descriptors.FileDescriptor.internalBuildGeneratedFileFrom(descriptorData, new Descriptors.FileDescriptor[0], assigner);
    }
    
    public enum DataType implements ProtocolMessageEnum
    {
        CHAR(0, 0), 
        BYTE(1, 1), 
        SHORT(2, 2), 
        INT(3, 3), 
        LONG(4, 4), 
        FLOAT(5, 5), 
        DOUBLE(6, 6), 
        STRING(7, 7), 
        STRUCTURE(8, 8), 
        SEQUENCE(9, 9), 
        ENUM1(10, 10), 
        ENUM2(11, 11), 
        ENUM4(12, 12), 
        OPAQUE(13, 13);
        
        private static final DataType[] VALUES;
        private final int index;
        private final int value;
        
        public final int getNumber() {
            return this.value;
        }
        
        public static DataType valueOf(final int value) {
            switch (value) {
                case 0: {
                    return DataType.CHAR;
                }
                case 1: {
                    return DataType.BYTE;
                }
                case 2: {
                    return DataType.SHORT;
                }
                case 3: {
                    return DataType.INT;
                }
                case 4: {
                    return DataType.LONG;
                }
                case 5: {
                    return DataType.FLOAT;
                }
                case 6: {
                    return DataType.DOUBLE;
                }
                case 7: {
                    return DataType.STRING;
                }
                case 8: {
                    return DataType.STRUCTURE;
                }
                case 9: {
                    return DataType.SEQUENCE;
                }
                case 10: {
                    return DataType.ENUM1;
                }
                case 11: {
                    return DataType.ENUM2;
                }
                case 12: {
                    return DataType.ENUM4;
                }
                case 13: {
                    return DataType.OPAQUE;
                }
                default: {
                    return null;
                }
            }
        }
        
        public final Descriptors.EnumValueDescriptor getValueDescriptor() {
            return getDescriptor().getValues().get(this.index);
        }
        
        public final Descriptors.EnumDescriptor getDescriptorForType() {
            return getDescriptor();
        }
        
        public static final Descriptors.EnumDescriptor getDescriptor() {
            return NcStreamProto.getDescriptor().getEnumTypes().get(0);
        }
        
        public static DataType valueOf(final Descriptors.EnumValueDescriptor desc) {
            if (desc.getType() != getDescriptor()) {
                throw new IllegalArgumentException("EnumValueDescriptor is not for this type.");
            }
            return DataType.VALUES[desc.getIndex()];
        }
        
        private DataType(final int index, final int value) {
            this.index = index;
            this.value = value;
        }
        
        static {
            VALUES = new DataType[] { DataType.CHAR, DataType.BYTE, DataType.SHORT, DataType.INT, DataType.LONG, DataType.FLOAT, DataType.DOUBLE, DataType.STRING, DataType.STRUCTURE, DataType.SEQUENCE, DataType.ENUM1, DataType.ENUM2, DataType.ENUM4, DataType.OPAQUE };
            NcStreamProto.getDescriptor();
        }
    }
    
    public static final class Attribute extends GeneratedMessage
    {
        private static final Attribute defaultInstance;
        public static final int NAME_FIELD_NUMBER = 1;
        private boolean hasName;
        private String name_;
        public static final int TYPE_FIELD_NUMBER = 2;
        private boolean hasType;
        private Type type_;
        public static final int LEN_FIELD_NUMBER = 3;
        private boolean hasLen;
        private int len_;
        public static final int DATA_FIELD_NUMBER = 4;
        private boolean hasData;
        private ByteString data_;
        public static final int SDATA_FIELD_NUMBER = 5;
        private List<String> sdata_;
        private int memoizedSerializedSize;
        
        private Attribute() {
            this.name_ = "";
            this.type_ = Type.STRING;
            this.len_ = 0;
            this.data_ = ByteString.EMPTY;
            this.sdata_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }
        
        public static Attribute getDefaultInstance() {
            return Attribute.defaultInstance;
        }
        
        public Attribute getDefaultInstanceForType() {
            return Attribute.defaultInstance;
        }
        
        public static final Descriptors.Descriptor getDescriptor() {
            return NcStreamProto.internal_static_ncstream_Attribute_descriptor;
        }
        
        protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return NcStreamProto.internal_static_ncstream_Attribute_fieldAccessorTable;
        }
        
        public boolean hasName() {
            return this.hasName;
        }
        
        public String getName() {
            return this.name_;
        }
        
        public boolean hasType() {
            return this.hasType;
        }
        
        public Type getType() {
            return this.type_;
        }
        
        public boolean hasLen() {
            return this.hasLen;
        }
        
        public int getLen() {
            return this.len_;
        }
        
        public boolean hasData() {
            return this.hasData;
        }
        
        public ByteString getData() {
            return this.data_;
        }
        
        public List<String> getSdataList() {
            return this.sdata_;
        }
        
        public int getSdataCount() {
            return this.sdata_.size();
        }
        
        public String getSdata(final int index) {
            return this.sdata_.get(index);
        }
        
        public final boolean isInitialized() {
            return this.hasName && this.hasType && this.hasLen;
        }
        
        public void writeTo(final CodedOutputStream output) throws IOException {
            if (this.hasName()) {
                output.writeString(1, this.getName());
            }
            if (this.hasType()) {
                output.writeEnum(2, this.getType().getNumber());
            }
            if (this.hasLen()) {
                output.writeUInt32(3, this.getLen());
            }
            if (this.hasData()) {
                output.writeBytes(4, this.getData());
            }
            for (final String element : this.getSdataList()) {
                output.writeString(5, element);
            }
            this.getUnknownFields().writeTo(output);
        }
        
        public int getSerializedSize() {
            int size = this.memoizedSerializedSize;
            if (size != -1) {
                return size;
            }
            size = 0;
            if (this.hasName()) {
                size += CodedOutputStream.computeStringSize(1, this.getName());
            }
            if (this.hasType()) {
                size += CodedOutputStream.computeEnumSize(2, this.getType().getNumber());
            }
            if (this.hasLen()) {
                size += CodedOutputStream.computeUInt32Size(3, this.getLen());
            }
            if (this.hasData()) {
                size += CodedOutputStream.computeBytesSize(4, this.getData());
            }
            int dataSize = 0;
            for (final String element : this.getSdataList()) {
                dataSize += CodedOutputStream.computeStringSizeNoTag(element);
            }
            size += dataSize;
            size += 1 * this.getSdataList().size();
            size += this.getUnknownFields().getSerializedSize();
            return this.memoizedSerializedSize = size;
        }
        
        public static Attribute parseFrom(final ByteString data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static Attribute parseFrom(final ByteString data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static Attribute parseFrom(final byte[] data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static Attribute parseFrom(final byte[] data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static Attribute parseFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input)).buildParsed();
        }
        
        public static Attribute parseFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static Attribute parseDelimitedFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input)).buildParsed();
        }
        
        public static Attribute parseDelimitedFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static Attribute parseFrom(final CodedInputStream input) throws IOException {
            return newBuilder().mergeFrom(input).buildParsed();
        }
        
        public static Attribute parseFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return newBuilder().mergeFrom(input, extensionRegistry).buildParsed();
        }
        
        public static Builder newBuilder() {
            return new Builder();
        }
        
        public Builder newBuilderForType() {
            return new Builder();
        }
        
        public static Builder newBuilder(final Attribute prototype) {
            return new Builder().mergeFrom(prototype);
        }
        
        public Builder toBuilder() {
            return newBuilder(this);
        }
        
        static {
            defaultInstance = new Attribute();
            NcStreamProto.getDescriptor();
        }
        
        public enum Type implements ProtocolMessageEnum
        {
            STRING(0, 0), 
            BYTE(1, 1), 
            SHORT(2, 2), 
            INT(3, 3), 
            LONG(4, 4), 
            FLOAT(5, 5), 
            DOUBLE(6, 6);
            
            private static final Type[] VALUES;
            private final int index;
            private final int value;
            
            public final int getNumber() {
                return this.value;
            }
            
            public static Type valueOf(final int value) {
                switch (value) {
                    case 0: {
                        return Type.STRING;
                    }
                    case 1: {
                        return Type.BYTE;
                    }
                    case 2: {
                        return Type.SHORT;
                    }
                    case 3: {
                        return Type.INT;
                    }
                    case 4: {
                        return Type.LONG;
                    }
                    case 5: {
                        return Type.FLOAT;
                    }
                    case 6: {
                        return Type.DOUBLE;
                    }
                    default: {
                        return null;
                    }
                }
            }
            
            public final Descriptors.EnumValueDescriptor getValueDescriptor() {
                return getDescriptor().getValues().get(this.index);
            }
            
            public final Descriptors.EnumDescriptor getDescriptorForType() {
                return getDescriptor();
            }
            
            public static final Descriptors.EnumDescriptor getDescriptor() {
                return Attribute.getDescriptor().getEnumTypes().get(0);
            }
            
            public static Type valueOf(final Descriptors.EnumValueDescriptor desc) {
                if (desc.getType() != getDescriptor()) {
                    throw new IllegalArgumentException("EnumValueDescriptor is not for this type.");
                }
                return Type.VALUES[desc.getIndex()];
            }
            
            private Type(final int index, final int value) {
                this.index = index;
                this.value = value;
            }
            
            static {
                VALUES = new Type[] { Type.STRING, Type.BYTE, Type.SHORT, Type.INT, Type.LONG, Type.FLOAT, Type.DOUBLE };
                NcStreamProto.getDescriptor();
            }
        }
        
        public static final class Builder extends GeneratedMessage.Builder<Builder>
        {
            Attribute result;
            
            private Builder() {
                this.result = new Attribute();
            }
            
            protected Attribute internalGetResult() {
                return this.result;
            }
            
            public Builder clear() {
                this.result = new Attribute();
                return this;
            }
            
            public Builder clone() {
                return new Builder().mergeFrom(this.result);
            }
            
            public Descriptors.Descriptor getDescriptorForType() {
                return Attribute.getDescriptor();
            }
            
            public Attribute getDefaultInstanceForType() {
                return Attribute.getDefaultInstance();
            }
            
            public Attribute build() {
                if (this.result != null && !this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result);
                }
                return this.buildPartial();
            }
            
            private Attribute buildParsed() throws InvalidProtocolBufferException {
                if (!this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result).asInvalidProtocolBufferException();
                }
                return this.buildPartial();
            }
            
            public Attribute buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.sdata_ != Collections.EMPTY_LIST) {
                    this.result.sdata_ = (List<String>)Collections.unmodifiableList((List<?>)this.result.sdata_);
                }
                final Attribute returnMe = this.result;
                this.result = null;
                return returnMe;
            }
            
            public Builder mergeFrom(final Message other) {
                if (other instanceof Attribute) {
                    return this.mergeFrom((Attribute)other);
                }
                super.mergeFrom(other);
                return this;
            }
            
            public Builder mergeFrom(final Attribute other) {
                if (other == Attribute.getDefaultInstance()) {
                    return this;
                }
                if (other.hasName()) {
                    this.setName(other.getName());
                }
                if (other.hasType()) {
                    this.setType(other.getType());
                }
                if (other.hasLen()) {
                    this.setLen(other.getLen());
                }
                if (other.hasData()) {
                    this.setData(other.getData());
                }
                if (!other.sdata_.isEmpty()) {
                    if (this.result.sdata_.isEmpty()) {
                        this.result.sdata_ = (List<String>)new ArrayList();
                    }
                    this.result.sdata_.addAll(other.sdata_);
                }
                this.mergeUnknownFields(other.getUnknownFields());
                return this;
            }
            
            public Builder mergeFrom(final CodedInputStream input) throws IOException {
                return this.mergeFrom(input, ExtensionRegistry.getEmptyRegistry());
            }
            
            public Builder mergeFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
                final UnknownFieldSet.Builder unknownFields = UnknownFieldSet.newBuilder(this.getUnknownFields());
                while (true) {
                    final int tag = input.readTag();
                    switch (tag) {
                        case 0: {
                            this.setUnknownFields(unknownFields.build());
                            return this;
                        }
                        default: {
                            if (!this.parseUnknownField(input, unknownFields, extensionRegistry, tag)) {
                                this.setUnknownFields(unknownFields.build());
                                return this;
                            }
                            continue;
                        }
                        case 10: {
                            this.setName(input.readString());
                            continue;
                        }
                        case 16: {
                            final int rawValue = input.readEnum();
                            final Type value = Type.valueOf(rawValue);
                            if (value == null) {
                                unknownFields.mergeVarintField(2, rawValue);
                                continue;
                            }
                            this.setType(value);
                            continue;
                        }
                        case 24: {
                            this.setLen(input.readUInt32());
                            continue;
                        }
                        case 34: {
                            this.setData(input.readBytes());
                            continue;
                        }
                        case 42: {
                            this.addSdata(input.readString());
                            continue;
                        }
                    }
                }
            }
            
            public boolean hasName() {
                return this.result.hasName();
            }
            
            public String getName() {
                return this.result.getName();
            }
            
            public Builder setName(final String value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasName = true;
                this.result.name_ = value;
                return this;
            }
            
            public Builder clearName() {
                this.result.hasName = false;
                this.result.name_ = "";
                return this;
            }
            
            public boolean hasType() {
                return this.result.hasType();
            }
            
            public Type getType() {
                return this.result.getType();
            }
            
            public Builder setType(final Type value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasType = true;
                this.result.type_ = value;
                return this;
            }
            
            public Builder clearType() {
                this.result.hasType = false;
                this.result.type_ = Type.STRING;
                return this;
            }
            
            public boolean hasLen() {
                return this.result.hasLen();
            }
            
            public int getLen() {
                return this.result.getLen();
            }
            
            public Builder setLen(final int value) {
                this.result.hasLen = true;
                this.result.len_ = value;
                return this;
            }
            
            public Builder clearLen() {
                this.result.hasLen = false;
                this.result.len_ = 0;
                return this;
            }
            
            public boolean hasData() {
                return this.result.hasData();
            }
            
            public ByteString getData() {
                return this.result.getData();
            }
            
            public Builder setData(final ByteString value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasData = true;
                this.result.data_ = value;
                return this;
            }
            
            public Builder clearData() {
                this.result.hasData = false;
                this.result.data_ = ByteString.EMPTY;
                return this;
            }
            
            public List<String> getSdataList() {
                return Collections.unmodifiableList((List<? extends String>)this.result.sdata_);
            }
            
            public int getSdataCount() {
                return this.result.getSdataCount();
            }
            
            public String getSdata(final int index) {
                return this.result.getSdata(index);
            }
            
            public Builder setSdata(final int index, final String value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.sdata_.set(index, value);
                return this;
            }
            
            public Builder addSdata(final String value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                if (this.result.sdata_.isEmpty()) {
                    this.result.sdata_ = (List<String>)new ArrayList();
                }
                this.result.sdata_.add(value);
                return this;
            }
            
            public Builder addAllSdata(final Iterable<? extends String> values) {
                if (this.result.sdata_.isEmpty()) {
                    this.result.sdata_ = (List<String>)new ArrayList();
                }
                super.addAll((Iterable)values, (Collection)this.result.sdata_);
                return this;
            }
            
            public Builder clearSdata() {
                this.result.sdata_ = (List<String>)Collections.emptyList();
                return this;
            }
        }
    }
    
    public static final class Dimension extends GeneratedMessage
    {
        private static final Dimension defaultInstance;
        public static final int NAME_FIELD_NUMBER = 1;
        private boolean hasName;
        private String name_;
        public static final int LENGTH_FIELD_NUMBER = 2;
        private boolean hasLength;
        private long length_;
        public static final int ISUNLIMITED_FIELD_NUMBER = 3;
        private boolean hasIsUnlimited;
        private boolean isUnlimited_;
        public static final int ISVLEN_FIELD_NUMBER = 4;
        private boolean hasIsVlen;
        private boolean isVlen_;
        public static final int ISPRIVATE_FIELD_NUMBER = 5;
        private boolean hasIsPrivate;
        private boolean isPrivate_;
        private int memoizedSerializedSize;
        
        private Dimension() {
            this.name_ = "";
            this.length_ = 0L;
            this.isUnlimited_ = false;
            this.isVlen_ = false;
            this.isPrivate_ = false;
            this.memoizedSerializedSize = -1;
        }
        
        public static Dimension getDefaultInstance() {
            return Dimension.defaultInstance;
        }
        
        public Dimension getDefaultInstanceForType() {
            return Dimension.defaultInstance;
        }
        
        public static final Descriptors.Descriptor getDescriptor() {
            return NcStreamProto.internal_static_ncstream_Dimension_descriptor;
        }
        
        protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return NcStreamProto.internal_static_ncstream_Dimension_fieldAccessorTable;
        }
        
        public boolean hasName() {
            return this.hasName;
        }
        
        public String getName() {
            return this.name_;
        }
        
        public boolean hasLength() {
            return this.hasLength;
        }
        
        public long getLength() {
            return this.length_;
        }
        
        public boolean hasIsUnlimited() {
            return this.hasIsUnlimited;
        }
        
        public boolean getIsUnlimited() {
            return this.isUnlimited_;
        }
        
        public boolean hasIsVlen() {
            return this.hasIsVlen;
        }
        
        public boolean getIsVlen() {
            return this.isVlen_;
        }
        
        public boolean hasIsPrivate() {
            return this.hasIsPrivate;
        }
        
        public boolean getIsPrivate() {
            return this.isPrivate_;
        }
        
        public final boolean isInitialized() {
            return true;
        }
        
        public void writeTo(final CodedOutputStream output) throws IOException {
            if (this.hasName()) {
                output.writeString(1, this.getName());
            }
            if (this.hasLength()) {
                output.writeUInt64(2, this.getLength());
            }
            if (this.hasIsUnlimited()) {
                output.writeBool(3, this.getIsUnlimited());
            }
            if (this.hasIsVlen()) {
                output.writeBool(4, this.getIsVlen());
            }
            if (this.hasIsPrivate()) {
                output.writeBool(5, this.getIsPrivate());
            }
            this.getUnknownFields().writeTo(output);
        }
        
        public int getSerializedSize() {
            int size = this.memoizedSerializedSize;
            if (size != -1) {
                return size;
            }
            size = 0;
            if (this.hasName()) {
                size += CodedOutputStream.computeStringSize(1, this.getName());
            }
            if (this.hasLength()) {
                size += CodedOutputStream.computeUInt64Size(2, this.getLength());
            }
            if (this.hasIsUnlimited()) {
                size += CodedOutputStream.computeBoolSize(3, this.getIsUnlimited());
            }
            if (this.hasIsVlen()) {
                size += CodedOutputStream.computeBoolSize(4, this.getIsVlen());
            }
            if (this.hasIsPrivate()) {
                size += CodedOutputStream.computeBoolSize(5, this.getIsPrivate());
            }
            size += this.getUnknownFields().getSerializedSize();
            return this.memoizedSerializedSize = size;
        }
        
        public static Dimension parseFrom(final ByteString data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static Dimension parseFrom(final ByteString data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static Dimension parseFrom(final byte[] data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static Dimension parseFrom(final byte[] data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static Dimension parseFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input)).buildParsed();
        }
        
        public static Dimension parseFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static Dimension parseDelimitedFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input)).buildParsed();
        }
        
        public static Dimension parseDelimitedFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static Dimension parseFrom(final CodedInputStream input) throws IOException {
            return newBuilder().mergeFrom(input).buildParsed();
        }
        
        public static Dimension parseFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return newBuilder().mergeFrom(input, extensionRegistry).buildParsed();
        }
        
        public static Builder newBuilder() {
            return new Builder();
        }
        
        public Builder newBuilderForType() {
            return new Builder();
        }
        
        public static Builder newBuilder(final Dimension prototype) {
            return new Builder().mergeFrom(prototype);
        }
        
        public Builder toBuilder() {
            return newBuilder(this);
        }
        
        static {
            defaultInstance = new Dimension();
            NcStreamProto.getDescriptor();
        }
        
        public static final class Builder extends GeneratedMessage.Builder<Builder>
        {
            Dimension result;
            
            private Builder() {
                this.result = new Dimension();
            }
            
            protected Dimension internalGetResult() {
                return this.result;
            }
            
            public Builder clear() {
                this.result = new Dimension();
                return this;
            }
            
            public Builder clone() {
                return new Builder().mergeFrom(this.result);
            }
            
            public Descriptors.Descriptor getDescriptorForType() {
                return Dimension.getDescriptor();
            }
            
            public Dimension getDefaultInstanceForType() {
                return Dimension.getDefaultInstance();
            }
            
            public Dimension build() {
                if (this.result != null && !this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result);
                }
                return this.buildPartial();
            }
            
            private Dimension buildParsed() throws InvalidProtocolBufferException {
                if (!this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result).asInvalidProtocolBufferException();
                }
                return this.buildPartial();
            }
            
            public Dimension buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                final Dimension returnMe = this.result;
                this.result = null;
                return returnMe;
            }
            
            public Builder mergeFrom(final Message other) {
                if (other instanceof Dimension) {
                    return this.mergeFrom((Dimension)other);
                }
                super.mergeFrom(other);
                return this;
            }
            
            public Builder mergeFrom(final Dimension other) {
                if (other == Dimension.getDefaultInstance()) {
                    return this;
                }
                if (other.hasName()) {
                    this.setName(other.getName());
                }
                if (other.hasLength()) {
                    this.setLength(other.getLength());
                }
                if (other.hasIsUnlimited()) {
                    this.setIsUnlimited(other.getIsUnlimited());
                }
                if (other.hasIsVlen()) {
                    this.setIsVlen(other.getIsVlen());
                }
                if (other.hasIsPrivate()) {
                    this.setIsPrivate(other.getIsPrivate());
                }
                this.mergeUnknownFields(other.getUnknownFields());
                return this;
            }
            
            public Builder mergeFrom(final CodedInputStream input) throws IOException {
                return this.mergeFrom(input, ExtensionRegistry.getEmptyRegistry());
            }
            
            public Builder mergeFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
                final UnknownFieldSet.Builder unknownFields = UnknownFieldSet.newBuilder(this.getUnknownFields());
                while (true) {
                    final int tag = input.readTag();
                    switch (tag) {
                        case 0: {
                            this.setUnknownFields(unknownFields.build());
                            return this;
                        }
                        default: {
                            if (!this.parseUnknownField(input, unknownFields, extensionRegistry, tag)) {
                                this.setUnknownFields(unknownFields.build());
                                return this;
                            }
                            continue;
                        }
                        case 10: {
                            this.setName(input.readString());
                            continue;
                        }
                        case 16: {
                            this.setLength(input.readUInt64());
                            continue;
                        }
                        case 24: {
                            this.setIsUnlimited(input.readBool());
                            continue;
                        }
                        case 32: {
                            this.setIsVlen(input.readBool());
                            continue;
                        }
                        case 40: {
                            this.setIsPrivate(input.readBool());
                            continue;
                        }
                    }
                }
            }
            
            public boolean hasName() {
                return this.result.hasName();
            }
            
            public String getName() {
                return this.result.getName();
            }
            
            public Builder setName(final String value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasName = true;
                this.result.name_ = value;
                return this;
            }
            
            public Builder clearName() {
                this.result.hasName = false;
                this.result.name_ = "";
                return this;
            }
            
            public boolean hasLength() {
                return this.result.hasLength();
            }
            
            public long getLength() {
                return this.result.getLength();
            }
            
            public Builder setLength(final long value) {
                this.result.hasLength = true;
                this.result.length_ = value;
                return this;
            }
            
            public Builder clearLength() {
                this.result.hasLength = false;
                this.result.length_ = 0L;
                return this;
            }
            
            public boolean hasIsUnlimited() {
                return this.result.hasIsUnlimited();
            }
            
            public boolean getIsUnlimited() {
                return this.result.getIsUnlimited();
            }
            
            public Builder setIsUnlimited(final boolean value) {
                this.result.hasIsUnlimited = true;
                this.result.isUnlimited_ = value;
                return this;
            }
            
            public Builder clearIsUnlimited() {
                this.result.hasIsUnlimited = false;
                this.result.isUnlimited_ = false;
                return this;
            }
            
            public boolean hasIsVlen() {
                return this.result.hasIsVlen();
            }
            
            public boolean getIsVlen() {
                return this.result.getIsVlen();
            }
            
            public Builder setIsVlen(final boolean value) {
                this.result.hasIsVlen = true;
                this.result.isVlen_ = value;
                return this;
            }
            
            public Builder clearIsVlen() {
                this.result.hasIsVlen = false;
                this.result.isVlen_ = false;
                return this;
            }
            
            public boolean hasIsPrivate() {
                return this.result.hasIsPrivate();
            }
            
            public boolean getIsPrivate() {
                return this.result.getIsPrivate();
            }
            
            public Builder setIsPrivate(final boolean value) {
                this.result.hasIsPrivate = true;
                this.result.isPrivate_ = value;
                return this;
            }
            
            public Builder clearIsPrivate() {
                this.result.hasIsPrivate = false;
                this.result.isPrivate_ = false;
                return this;
            }
        }
    }
    
    public static final class Variable extends GeneratedMessage
    {
        private static final Variable defaultInstance;
        public static final int NAME_FIELD_NUMBER = 1;
        private boolean hasName;
        private String name_;
        public static final int DATATYPE_FIELD_NUMBER = 2;
        private boolean hasDataType;
        private DataType dataType_;
        public static final int SHAPE_FIELD_NUMBER = 3;
        private List<Dimension> shape_;
        public static final int ATTS_FIELD_NUMBER = 4;
        private List<Attribute> atts_;
        public static final int UNSIGNED_FIELD_NUMBER = 5;
        private boolean hasUnsigned;
        private boolean unsigned_;
        public static final int DATA_FIELD_NUMBER = 6;
        private boolean hasData;
        private ByteString data_;
        public static final int ENUMTYPE_FIELD_NUMBER = 7;
        private boolean hasEnumType;
        private String enumType_;
        private int memoizedSerializedSize;
        
        private Variable() {
            this.name_ = "";
            this.dataType_ = DataType.CHAR;
            this.shape_ = Collections.emptyList();
            this.atts_ = Collections.emptyList();
            this.unsigned_ = false;
            this.data_ = ByteString.EMPTY;
            this.enumType_ = "";
            this.memoizedSerializedSize = -1;
        }
        
        public static Variable getDefaultInstance() {
            return Variable.defaultInstance;
        }
        
        public Variable getDefaultInstanceForType() {
            return Variable.defaultInstance;
        }
        
        public static final Descriptors.Descriptor getDescriptor() {
            return NcStreamProto.internal_static_ncstream_Variable_descriptor;
        }
        
        protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return NcStreamProto.internal_static_ncstream_Variable_fieldAccessorTable;
        }
        
        public boolean hasName() {
            return this.hasName;
        }
        
        public String getName() {
            return this.name_;
        }
        
        public boolean hasDataType() {
            return this.hasDataType;
        }
        
        public DataType getDataType() {
            return this.dataType_;
        }
        
        public List<Dimension> getShapeList() {
            return this.shape_;
        }
        
        public int getShapeCount() {
            return this.shape_.size();
        }
        
        public Dimension getShape(final int index) {
            return this.shape_.get(index);
        }
        
        public List<Attribute> getAttsList() {
            return this.atts_;
        }
        
        public int getAttsCount() {
            return this.atts_.size();
        }
        
        public Attribute getAtts(final int index) {
            return this.atts_.get(index);
        }
        
        public boolean hasUnsigned() {
            return this.hasUnsigned;
        }
        
        public boolean getUnsigned() {
            return this.unsigned_;
        }
        
        public boolean hasData() {
            return this.hasData;
        }
        
        public ByteString getData() {
            return this.data_;
        }
        
        public boolean hasEnumType() {
            return this.hasEnumType;
        }
        
        public String getEnumType() {
            return this.enumType_;
        }
        
        public final boolean isInitialized() {
            if (!this.hasName) {
                return false;
            }
            if (!this.hasDataType) {
                return false;
            }
            for (final Attribute element : this.getAttsList()) {
                if (!element.isInitialized()) {
                    return false;
                }
            }
            return true;
        }
        
        public void writeTo(final CodedOutputStream output) throws IOException {
            if (this.hasName()) {
                output.writeString(1, this.getName());
            }
            if (this.hasDataType()) {
                output.writeEnum(2, this.getDataType().getNumber());
            }
            for (final Dimension element : this.getShapeList()) {
                output.writeMessage(3, (Message)element);
            }
            for (final Attribute element2 : this.getAttsList()) {
                output.writeMessage(4, (Message)element2);
            }
            if (this.hasUnsigned()) {
                output.writeBool(5, this.getUnsigned());
            }
            if (this.hasData()) {
                output.writeBytes(6, this.getData());
            }
            if (this.hasEnumType()) {
                output.writeString(7, this.getEnumType());
            }
            this.getUnknownFields().writeTo(output);
        }
        
        public int getSerializedSize() {
            int size = this.memoizedSerializedSize;
            if (size != -1) {
                return size;
            }
            size = 0;
            if (this.hasName()) {
                size += CodedOutputStream.computeStringSize(1, this.getName());
            }
            if (this.hasDataType()) {
                size += CodedOutputStream.computeEnumSize(2, this.getDataType().getNumber());
            }
            for (final Dimension element : this.getShapeList()) {
                size += CodedOutputStream.computeMessageSize(3, (Message)element);
            }
            for (final Attribute element2 : this.getAttsList()) {
                size += CodedOutputStream.computeMessageSize(4, (Message)element2);
            }
            if (this.hasUnsigned()) {
                size += CodedOutputStream.computeBoolSize(5, this.getUnsigned());
            }
            if (this.hasData()) {
                size += CodedOutputStream.computeBytesSize(6, this.getData());
            }
            if (this.hasEnumType()) {
                size += CodedOutputStream.computeStringSize(7, this.getEnumType());
            }
            size += this.getUnknownFields().getSerializedSize();
            return this.memoizedSerializedSize = size;
        }
        
        public static Variable parseFrom(final ByteString data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static Variable parseFrom(final ByteString data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static Variable parseFrom(final byte[] data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static Variable parseFrom(final byte[] data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static Variable parseFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input)).buildParsed();
        }
        
        public static Variable parseFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static Variable parseDelimitedFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input)).buildParsed();
        }
        
        public static Variable parseDelimitedFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static Variable parseFrom(final CodedInputStream input) throws IOException {
            return newBuilder().mergeFrom(input).buildParsed();
        }
        
        public static Variable parseFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return newBuilder().mergeFrom(input, extensionRegistry).buildParsed();
        }
        
        public static Builder newBuilder() {
            return new Builder();
        }
        
        public Builder newBuilderForType() {
            return new Builder();
        }
        
        public static Builder newBuilder(final Variable prototype) {
            return new Builder().mergeFrom(prototype);
        }
        
        public Builder toBuilder() {
            return newBuilder(this);
        }
        
        static {
            defaultInstance = new Variable();
            NcStreamProto.getDescriptor();
        }
        
        public static final class Builder extends GeneratedMessage.Builder<Builder>
        {
            Variable result;
            
            private Builder() {
                this.result = new Variable();
            }
            
            protected Variable internalGetResult() {
                return this.result;
            }
            
            public Builder clear() {
                this.result = new Variable();
                return this;
            }
            
            public Builder clone() {
                return new Builder().mergeFrom(this.result);
            }
            
            public Descriptors.Descriptor getDescriptorForType() {
                return Variable.getDescriptor();
            }
            
            public Variable getDefaultInstanceForType() {
                return Variable.getDefaultInstance();
            }
            
            public Variable build() {
                if (this.result != null && !this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result);
                }
                return this.buildPartial();
            }
            
            private Variable buildParsed() throws InvalidProtocolBufferException {
                if (!this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result).asInvalidProtocolBufferException();
                }
                return this.buildPartial();
            }
            
            public Variable buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.shape_ != Collections.EMPTY_LIST) {
                    this.result.shape_ = (List<Dimension>)Collections.unmodifiableList((List<?>)this.result.shape_);
                }
                if (this.result.atts_ != Collections.EMPTY_LIST) {
                    this.result.atts_ = (List<Attribute>)Collections.unmodifiableList((List<?>)this.result.atts_);
                }
                final Variable returnMe = this.result;
                this.result = null;
                return returnMe;
            }
            
            public Builder mergeFrom(final Message other) {
                if (other instanceof Variable) {
                    return this.mergeFrom((Variable)other);
                }
                super.mergeFrom(other);
                return this;
            }
            
            public Builder mergeFrom(final Variable other) {
                if (other == Variable.getDefaultInstance()) {
                    return this;
                }
                if (other.hasName()) {
                    this.setName(other.getName());
                }
                if (other.hasDataType()) {
                    this.setDataType(other.getDataType());
                }
                if (!other.shape_.isEmpty()) {
                    if (this.result.shape_.isEmpty()) {
                        this.result.shape_ = (List<Dimension>)new ArrayList();
                    }
                    this.result.shape_.addAll(other.shape_);
                }
                if (!other.atts_.isEmpty()) {
                    if (this.result.atts_.isEmpty()) {
                        this.result.atts_ = (List<Attribute>)new ArrayList();
                    }
                    this.result.atts_.addAll(other.atts_);
                }
                if (other.hasUnsigned()) {
                    this.setUnsigned(other.getUnsigned());
                }
                if (other.hasData()) {
                    this.setData(other.getData());
                }
                if (other.hasEnumType()) {
                    this.setEnumType(other.getEnumType());
                }
                this.mergeUnknownFields(other.getUnknownFields());
                return this;
            }
            
            public Builder mergeFrom(final CodedInputStream input) throws IOException {
                return this.mergeFrom(input, ExtensionRegistry.getEmptyRegistry());
            }
            
            public Builder mergeFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
                final UnknownFieldSet.Builder unknownFields = UnknownFieldSet.newBuilder(this.getUnknownFields());
                while (true) {
                    final int tag = input.readTag();
                    switch (tag) {
                        case 0: {
                            this.setUnknownFields(unknownFields.build());
                            return this;
                        }
                        default: {
                            if (!this.parseUnknownField(input, unknownFields, extensionRegistry, tag)) {
                                this.setUnknownFields(unknownFields.build());
                                return this;
                            }
                            continue;
                        }
                        case 10: {
                            this.setName(input.readString());
                            continue;
                        }
                        case 16: {
                            final int rawValue = input.readEnum();
                            final DataType value = DataType.valueOf(rawValue);
                            if (value == null) {
                                unknownFields.mergeVarintField(2, rawValue);
                                continue;
                            }
                            this.setDataType(value);
                            continue;
                        }
                        case 26: {
                            final Dimension.Builder subBuilder = Dimension.newBuilder();
                            input.readMessage((Message.Builder)subBuilder, extensionRegistry);
                            this.addShape(subBuilder.buildPartial());
                            continue;
                        }
                        case 34: {
                            final Attribute.Builder subBuilder2 = Attribute.newBuilder();
                            input.readMessage((Message.Builder)subBuilder2, extensionRegistry);
                            this.addAtts(subBuilder2.buildPartial());
                            continue;
                        }
                        case 40: {
                            this.setUnsigned(input.readBool());
                            continue;
                        }
                        case 50: {
                            this.setData(input.readBytes());
                            continue;
                        }
                        case 58: {
                            this.setEnumType(input.readString());
                            continue;
                        }
                    }
                }
            }
            
            public boolean hasName() {
                return this.result.hasName();
            }
            
            public String getName() {
                return this.result.getName();
            }
            
            public Builder setName(final String value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasName = true;
                this.result.name_ = value;
                return this;
            }
            
            public Builder clearName() {
                this.result.hasName = false;
                this.result.name_ = "";
                return this;
            }
            
            public boolean hasDataType() {
                return this.result.hasDataType();
            }
            
            public DataType getDataType() {
                return this.result.getDataType();
            }
            
            public Builder setDataType(final DataType value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasDataType = true;
                this.result.dataType_ = value;
                return this;
            }
            
            public Builder clearDataType() {
                this.result.hasDataType = false;
                this.result.dataType_ = DataType.CHAR;
                return this;
            }
            
            public List<Dimension> getShapeList() {
                return Collections.unmodifiableList((List<? extends Dimension>)this.result.shape_);
            }
            
            public int getShapeCount() {
                return this.result.getShapeCount();
            }
            
            public Dimension getShape(final int index) {
                return this.result.getShape(index);
            }
            
            public Builder setShape(final int index, final Dimension value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.shape_.set(index, value);
                return this;
            }
            
            public Builder setShape(final int index, final Dimension.Builder builderForValue) {
                this.result.shape_.set(index, builderForValue.build());
                return this;
            }
            
            public Builder addShape(final Dimension value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                if (this.result.shape_.isEmpty()) {
                    this.result.shape_ = (List<Dimension>)new ArrayList();
                }
                this.result.shape_.add(value);
                return this;
            }
            
            public Builder addShape(final Dimension.Builder builderForValue) {
                if (this.result.shape_.isEmpty()) {
                    this.result.shape_ = (List<Dimension>)new ArrayList();
                }
                this.result.shape_.add(builderForValue.build());
                return this;
            }
            
            public Builder addAllShape(final Iterable<? extends Dimension> values) {
                if (this.result.shape_.isEmpty()) {
                    this.result.shape_ = (List<Dimension>)new ArrayList();
                }
                super.addAll((Iterable)values, (Collection)this.result.shape_);
                return this;
            }
            
            public Builder clearShape() {
                this.result.shape_ = (List<Dimension>)Collections.emptyList();
                return this;
            }
            
            public List<Attribute> getAttsList() {
                return Collections.unmodifiableList((List<? extends Attribute>)this.result.atts_);
            }
            
            public int getAttsCount() {
                return this.result.getAttsCount();
            }
            
            public Attribute getAtts(final int index) {
                return this.result.getAtts(index);
            }
            
            public Builder setAtts(final int index, final Attribute value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.atts_.set(index, value);
                return this;
            }
            
            public Builder setAtts(final int index, final Attribute.Builder builderForValue) {
                this.result.atts_.set(index, builderForValue.build());
                return this;
            }
            
            public Builder addAtts(final Attribute value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                if (this.result.atts_.isEmpty()) {
                    this.result.atts_ = (List<Attribute>)new ArrayList();
                }
                this.result.atts_.add(value);
                return this;
            }
            
            public Builder addAtts(final Attribute.Builder builderForValue) {
                if (this.result.atts_.isEmpty()) {
                    this.result.atts_ = (List<Attribute>)new ArrayList();
                }
                this.result.atts_.add(builderForValue.build());
                return this;
            }
            
            public Builder addAllAtts(final Iterable<? extends Attribute> values) {
                if (this.result.atts_.isEmpty()) {
                    this.result.atts_ = (List<Attribute>)new ArrayList();
                }
                super.addAll((Iterable)values, (Collection)this.result.atts_);
                return this;
            }
            
            public Builder clearAtts() {
                this.result.atts_ = (List<Attribute>)Collections.emptyList();
                return this;
            }
            
            public boolean hasUnsigned() {
                return this.result.hasUnsigned();
            }
            
            public boolean getUnsigned() {
                return this.result.getUnsigned();
            }
            
            public Builder setUnsigned(final boolean value) {
                this.result.hasUnsigned = true;
                this.result.unsigned_ = value;
                return this;
            }
            
            public Builder clearUnsigned() {
                this.result.hasUnsigned = false;
                this.result.unsigned_ = false;
                return this;
            }
            
            public boolean hasData() {
                return this.result.hasData();
            }
            
            public ByteString getData() {
                return this.result.getData();
            }
            
            public Builder setData(final ByteString value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasData = true;
                this.result.data_ = value;
                return this;
            }
            
            public Builder clearData() {
                this.result.hasData = false;
                this.result.data_ = ByteString.EMPTY;
                return this;
            }
            
            public boolean hasEnumType() {
                return this.result.hasEnumType();
            }
            
            public String getEnumType() {
                return this.result.getEnumType();
            }
            
            public Builder setEnumType(final String value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasEnumType = true;
                this.result.enumType_ = value;
                return this;
            }
            
            public Builder clearEnumType() {
                this.result.hasEnumType = false;
                this.result.enumType_ = "";
                return this;
            }
        }
    }
    
    public static final class Structure extends GeneratedMessage
    {
        private static final Structure defaultInstance;
        public static final int NAME_FIELD_NUMBER = 1;
        private boolean hasName;
        private String name_;
        public static final int DATATYPE_FIELD_NUMBER = 2;
        private boolean hasDataType;
        private DataType dataType_;
        public static final int SHAPE_FIELD_NUMBER = 3;
        private List<Dimension> shape_;
        public static final int ATTS_FIELD_NUMBER = 4;
        private List<Attribute> atts_;
        public static final int VARS_FIELD_NUMBER = 5;
        private List<Variable> vars_;
        public static final int STRUCTS_FIELD_NUMBER = 6;
        private List<Structure> structs_;
        private int memoizedSerializedSize;
        
        private Structure() {
            this.name_ = "";
            this.dataType_ = DataType.CHAR;
            this.shape_ = Collections.emptyList();
            this.atts_ = Collections.emptyList();
            this.vars_ = Collections.emptyList();
            this.structs_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }
        
        public static Structure getDefaultInstance() {
            return Structure.defaultInstance;
        }
        
        public Structure getDefaultInstanceForType() {
            return Structure.defaultInstance;
        }
        
        public static final Descriptors.Descriptor getDescriptor() {
            return NcStreamProto.internal_static_ncstream_Structure_descriptor;
        }
        
        protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return NcStreamProto.internal_static_ncstream_Structure_fieldAccessorTable;
        }
        
        public boolean hasName() {
            return this.hasName;
        }
        
        public String getName() {
            return this.name_;
        }
        
        public boolean hasDataType() {
            return this.hasDataType;
        }
        
        public DataType getDataType() {
            return this.dataType_;
        }
        
        public List<Dimension> getShapeList() {
            return this.shape_;
        }
        
        public int getShapeCount() {
            return this.shape_.size();
        }
        
        public Dimension getShape(final int index) {
            return this.shape_.get(index);
        }
        
        public List<Attribute> getAttsList() {
            return this.atts_;
        }
        
        public int getAttsCount() {
            return this.atts_.size();
        }
        
        public Attribute getAtts(final int index) {
            return this.atts_.get(index);
        }
        
        public List<Variable> getVarsList() {
            return this.vars_;
        }
        
        public int getVarsCount() {
            return this.vars_.size();
        }
        
        public Variable getVars(final int index) {
            return this.vars_.get(index);
        }
        
        public List<Structure> getStructsList() {
            return this.structs_;
        }
        
        public int getStructsCount() {
            return this.structs_.size();
        }
        
        public Structure getStructs(final int index) {
            return this.structs_.get(index);
        }
        
        public final boolean isInitialized() {
            if (!this.hasName) {
                return false;
            }
            if (!this.hasDataType) {
                return false;
            }
            for (final Attribute element : this.getAttsList()) {
                if (!element.isInitialized()) {
                    return false;
                }
            }
            for (final Variable element2 : this.getVarsList()) {
                if (!element2.isInitialized()) {
                    return false;
                }
            }
            for (final Structure element3 : this.getStructsList()) {
                if (!element3.isInitialized()) {
                    return false;
                }
            }
            return true;
        }
        
        public void writeTo(final CodedOutputStream output) throws IOException {
            if (this.hasName()) {
                output.writeString(1, this.getName());
            }
            if (this.hasDataType()) {
                output.writeEnum(2, this.getDataType().getNumber());
            }
            for (final Dimension element : this.getShapeList()) {
                output.writeMessage(3, (Message)element);
            }
            for (final Attribute element2 : this.getAttsList()) {
                output.writeMessage(4, (Message)element2);
            }
            for (final Variable element3 : this.getVarsList()) {
                output.writeMessage(5, (Message)element3);
            }
            for (final Structure element4 : this.getStructsList()) {
                output.writeMessage(6, (Message)element4);
            }
            this.getUnknownFields().writeTo(output);
        }
        
        public int getSerializedSize() {
            int size = this.memoizedSerializedSize;
            if (size != -1) {
                return size;
            }
            size = 0;
            if (this.hasName()) {
                size += CodedOutputStream.computeStringSize(1, this.getName());
            }
            if (this.hasDataType()) {
                size += CodedOutputStream.computeEnumSize(2, this.getDataType().getNumber());
            }
            for (final Dimension element : this.getShapeList()) {
                size += CodedOutputStream.computeMessageSize(3, (Message)element);
            }
            for (final Attribute element2 : this.getAttsList()) {
                size += CodedOutputStream.computeMessageSize(4, (Message)element2);
            }
            for (final Variable element3 : this.getVarsList()) {
                size += CodedOutputStream.computeMessageSize(5, (Message)element3);
            }
            for (final Structure element4 : this.getStructsList()) {
                size += CodedOutputStream.computeMessageSize(6, (Message)element4);
            }
            size += this.getUnknownFields().getSerializedSize();
            return this.memoizedSerializedSize = size;
        }
        
        public static Structure parseFrom(final ByteString data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static Structure parseFrom(final ByteString data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static Structure parseFrom(final byte[] data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static Structure parseFrom(final byte[] data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static Structure parseFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input)).buildParsed();
        }
        
        public static Structure parseFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static Structure parseDelimitedFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input)).buildParsed();
        }
        
        public static Structure parseDelimitedFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static Structure parseFrom(final CodedInputStream input) throws IOException {
            return newBuilder().mergeFrom(input).buildParsed();
        }
        
        public static Structure parseFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return newBuilder().mergeFrom(input, extensionRegistry).buildParsed();
        }
        
        public static Builder newBuilder() {
            return new Builder();
        }
        
        public Builder newBuilderForType() {
            return new Builder();
        }
        
        public static Builder newBuilder(final Structure prototype) {
            return new Builder().mergeFrom(prototype);
        }
        
        public Builder toBuilder() {
            return newBuilder(this);
        }
        
        static {
            defaultInstance = new Structure();
            NcStreamProto.getDescriptor();
        }
        
        public static final class Builder extends GeneratedMessage.Builder<Builder>
        {
            Structure result;
            
            private Builder() {
                this.result = new Structure();
            }
            
            protected Structure internalGetResult() {
                return this.result;
            }
            
            public Builder clear() {
                this.result = new Structure();
                return this;
            }
            
            public Builder clone() {
                return new Builder().mergeFrom(this.result);
            }
            
            public Descriptors.Descriptor getDescriptorForType() {
                return Structure.getDescriptor();
            }
            
            public Structure getDefaultInstanceForType() {
                return Structure.getDefaultInstance();
            }
            
            public Structure build() {
                if (this.result != null && !this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result);
                }
                return this.buildPartial();
            }
            
            private Structure buildParsed() throws InvalidProtocolBufferException {
                if (!this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result).asInvalidProtocolBufferException();
                }
                return this.buildPartial();
            }
            
            public Structure buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.shape_ != Collections.EMPTY_LIST) {
                    this.result.shape_ = (List<Dimension>)Collections.unmodifiableList((List<?>)this.result.shape_);
                }
                if (this.result.atts_ != Collections.EMPTY_LIST) {
                    this.result.atts_ = (List<Attribute>)Collections.unmodifiableList((List<?>)this.result.atts_);
                }
                if (this.result.vars_ != Collections.EMPTY_LIST) {
                    this.result.vars_ = (List<Variable>)Collections.unmodifiableList((List<?>)this.result.vars_);
                }
                if (this.result.structs_ != Collections.EMPTY_LIST) {
                    this.result.structs_ = (List<Structure>)Collections.unmodifiableList((List<?>)this.result.structs_);
                }
                final Structure returnMe = this.result;
                this.result = null;
                return returnMe;
            }
            
            public Builder mergeFrom(final Message other) {
                if (other instanceof Structure) {
                    return this.mergeFrom((Structure)other);
                }
                super.mergeFrom(other);
                return this;
            }
            
            public Builder mergeFrom(final Structure other) {
                if (other == Structure.getDefaultInstance()) {
                    return this;
                }
                if (other.hasName()) {
                    this.setName(other.getName());
                }
                if (other.hasDataType()) {
                    this.setDataType(other.getDataType());
                }
                if (!other.shape_.isEmpty()) {
                    if (this.result.shape_.isEmpty()) {
                        this.result.shape_ = (List<Dimension>)new ArrayList();
                    }
                    this.result.shape_.addAll(other.shape_);
                }
                if (!other.atts_.isEmpty()) {
                    if (this.result.atts_.isEmpty()) {
                        this.result.atts_ = (List<Attribute>)new ArrayList();
                    }
                    this.result.atts_.addAll(other.atts_);
                }
                if (!other.vars_.isEmpty()) {
                    if (this.result.vars_.isEmpty()) {
                        this.result.vars_ = (List<Variable>)new ArrayList();
                    }
                    this.result.vars_.addAll(other.vars_);
                }
                if (!other.structs_.isEmpty()) {
                    if (this.result.structs_.isEmpty()) {
                        this.result.structs_ = (List<Structure>)new ArrayList();
                    }
                    this.result.structs_.addAll(other.structs_);
                }
                this.mergeUnknownFields(other.getUnknownFields());
                return this;
            }
            
            public Builder mergeFrom(final CodedInputStream input) throws IOException {
                return this.mergeFrom(input, ExtensionRegistry.getEmptyRegistry());
            }
            
            public Builder mergeFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
                final UnknownFieldSet.Builder unknownFields = UnknownFieldSet.newBuilder(this.getUnknownFields());
                while (true) {
                    final int tag = input.readTag();
                    switch (tag) {
                        case 0: {
                            this.setUnknownFields(unknownFields.build());
                            return this;
                        }
                        default: {
                            if (!this.parseUnknownField(input, unknownFields, extensionRegistry, tag)) {
                                this.setUnknownFields(unknownFields.build());
                                return this;
                            }
                            continue;
                        }
                        case 10: {
                            this.setName(input.readString());
                            continue;
                        }
                        case 16: {
                            final int rawValue = input.readEnum();
                            final DataType value = DataType.valueOf(rawValue);
                            if (value == null) {
                                unknownFields.mergeVarintField(2, rawValue);
                                continue;
                            }
                            this.setDataType(value);
                            continue;
                        }
                        case 26: {
                            final Dimension.Builder subBuilder = Dimension.newBuilder();
                            input.readMessage((Message.Builder)subBuilder, extensionRegistry);
                            this.addShape(subBuilder.buildPartial());
                            continue;
                        }
                        case 34: {
                            final Attribute.Builder subBuilder2 = Attribute.newBuilder();
                            input.readMessage((Message.Builder)subBuilder2, extensionRegistry);
                            this.addAtts(subBuilder2.buildPartial());
                            continue;
                        }
                        case 42: {
                            final Variable.Builder subBuilder3 = Variable.newBuilder();
                            input.readMessage((Message.Builder)subBuilder3, extensionRegistry);
                            this.addVars(subBuilder3.buildPartial());
                            continue;
                        }
                        case 50: {
                            final Builder subBuilder4 = Structure.newBuilder();
                            input.readMessage((Message.Builder)subBuilder4, extensionRegistry);
                            this.addStructs(subBuilder4.buildPartial());
                            continue;
                        }
                    }
                }
            }
            
            public boolean hasName() {
                return this.result.hasName();
            }
            
            public String getName() {
                return this.result.getName();
            }
            
            public Builder setName(final String value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasName = true;
                this.result.name_ = value;
                return this;
            }
            
            public Builder clearName() {
                this.result.hasName = false;
                this.result.name_ = "";
                return this;
            }
            
            public boolean hasDataType() {
                return this.result.hasDataType();
            }
            
            public DataType getDataType() {
                return this.result.getDataType();
            }
            
            public Builder setDataType(final DataType value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasDataType = true;
                this.result.dataType_ = value;
                return this;
            }
            
            public Builder clearDataType() {
                this.result.hasDataType = false;
                this.result.dataType_ = DataType.CHAR;
                return this;
            }
            
            public List<Dimension> getShapeList() {
                return Collections.unmodifiableList((List<? extends Dimension>)this.result.shape_);
            }
            
            public int getShapeCount() {
                return this.result.getShapeCount();
            }
            
            public Dimension getShape(final int index) {
                return this.result.getShape(index);
            }
            
            public Builder setShape(final int index, final Dimension value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.shape_.set(index, value);
                return this;
            }
            
            public Builder setShape(final int index, final Dimension.Builder builderForValue) {
                this.result.shape_.set(index, builderForValue.build());
                return this;
            }
            
            public Builder addShape(final Dimension value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                if (this.result.shape_.isEmpty()) {
                    this.result.shape_ = (List<Dimension>)new ArrayList();
                }
                this.result.shape_.add(value);
                return this;
            }
            
            public Builder addShape(final Dimension.Builder builderForValue) {
                if (this.result.shape_.isEmpty()) {
                    this.result.shape_ = (List<Dimension>)new ArrayList();
                }
                this.result.shape_.add(builderForValue.build());
                return this;
            }
            
            public Builder addAllShape(final Iterable<? extends Dimension> values) {
                if (this.result.shape_.isEmpty()) {
                    this.result.shape_ = (List<Dimension>)new ArrayList();
                }
                super.addAll((Iterable)values, (Collection)this.result.shape_);
                return this;
            }
            
            public Builder clearShape() {
                this.result.shape_ = (List<Dimension>)Collections.emptyList();
                return this;
            }
            
            public List<Attribute> getAttsList() {
                return Collections.unmodifiableList((List<? extends Attribute>)this.result.atts_);
            }
            
            public int getAttsCount() {
                return this.result.getAttsCount();
            }
            
            public Attribute getAtts(final int index) {
                return this.result.getAtts(index);
            }
            
            public Builder setAtts(final int index, final Attribute value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.atts_.set(index, value);
                return this;
            }
            
            public Builder setAtts(final int index, final Attribute.Builder builderForValue) {
                this.result.atts_.set(index, builderForValue.build());
                return this;
            }
            
            public Builder addAtts(final Attribute value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                if (this.result.atts_.isEmpty()) {
                    this.result.atts_ = (List<Attribute>)new ArrayList();
                }
                this.result.atts_.add(value);
                return this;
            }
            
            public Builder addAtts(final Attribute.Builder builderForValue) {
                if (this.result.atts_.isEmpty()) {
                    this.result.atts_ = (List<Attribute>)new ArrayList();
                }
                this.result.atts_.add(builderForValue.build());
                return this;
            }
            
            public Builder addAllAtts(final Iterable<? extends Attribute> values) {
                if (this.result.atts_.isEmpty()) {
                    this.result.atts_ = (List<Attribute>)new ArrayList();
                }
                super.addAll((Iterable)values, (Collection)this.result.atts_);
                return this;
            }
            
            public Builder clearAtts() {
                this.result.atts_ = (List<Attribute>)Collections.emptyList();
                return this;
            }
            
            public List<Variable> getVarsList() {
                return Collections.unmodifiableList((List<? extends Variable>)this.result.vars_);
            }
            
            public int getVarsCount() {
                return this.result.getVarsCount();
            }
            
            public Variable getVars(final int index) {
                return this.result.getVars(index);
            }
            
            public Builder setVars(final int index, final Variable value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.vars_.set(index, value);
                return this;
            }
            
            public Builder setVars(final int index, final Variable.Builder builderForValue) {
                this.result.vars_.set(index, builderForValue.build());
                return this;
            }
            
            public Builder addVars(final Variable value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                if (this.result.vars_.isEmpty()) {
                    this.result.vars_ = (List<Variable>)new ArrayList();
                }
                this.result.vars_.add(value);
                return this;
            }
            
            public Builder addVars(final Variable.Builder builderForValue) {
                if (this.result.vars_.isEmpty()) {
                    this.result.vars_ = (List<Variable>)new ArrayList();
                }
                this.result.vars_.add(builderForValue.build());
                return this;
            }
            
            public Builder addAllVars(final Iterable<? extends Variable> values) {
                if (this.result.vars_.isEmpty()) {
                    this.result.vars_ = (List<Variable>)new ArrayList();
                }
                super.addAll((Iterable)values, (Collection)this.result.vars_);
                return this;
            }
            
            public Builder clearVars() {
                this.result.vars_ = (List<Variable>)Collections.emptyList();
                return this;
            }
            
            public List<Structure> getStructsList() {
                return Collections.unmodifiableList((List<? extends Structure>)this.result.structs_);
            }
            
            public int getStructsCount() {
                return this.result.getStructsCount();
            }
            
            public Structure getStructs(final int index) {
                return this.result.getStructs(index);
            }
            
            public Builder setStructs(final int index, final Structure value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.structs_.set(index, value);
                return this;
            }
            
            public Builder setStructs(final int index, final Builder builderForValue) {
                this.result.structs_.set(index, builderForValue.build());
                return this;
            }
            
            public Builder addStructs(final Structure value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                if (this.result.structs_.isEmpty()) {
                    this.result.structs_ = (List<Structure>)new ArrayList();
                }
                this.result.structs_.add(value);
                return this;
            }
            
            public Builder addStructs(final Builder builderForValue) {
                if (this.result.structs_.isEmpty()) {
                    this.result.structs_ = (List<Structure>)new ArrayList();
                }
                this.result.structs_.add(builderForValue.build());
                return this;
            }
            
            public Builder addAllStructs(final Iterable<? extends Structure> values) {
                if (this.result.structs_.isEmpty()) {
                    this.result.structs_ = (List<Structure>)new ArrayList();
                }
                super.addAll((Iterable)values, (Collection)this.result.structs_);
                return this;
            }
            
            public Builder clearStructs() {
                this.result.structs_ = (List<Structure>)Collections.emptyList();
                return this;
            }
        }
    }
    
    public static final class EnumTypedef extends GeneratedMessage
    {
        private static final EnumTypedef defaultInstance;
        public static final int NAME_FIELD_NUMBER = 1;
        private boolean hasName;
        private String name_;
        public static final int MAP_FIELD_NUMBER = 2;
        private List<EnumType> map_;
        private int memoizedSerializedSize;
        
        private EnumTypedef() {
            this.name_ = "";
            this.map_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }
        
        public static EnumTypedef getDefaultInstance() {
            return EnumTypedef.defaultInstance;
        }
        
        public EnumTypedef getDefaultInstanceForType() {
            return EnumTypedef.defaultInstance;
        }
        
        public static final Descriptors.Descriptor getDescriptor() {
            return NcStreamProto.internal_static_ncstream_EnumTypedef_descriptor;
        }
        
        protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return NcStreamProto.internal_static_ncstream_EnumTypedef_fieldAccessorTable;
        }
        
        public boolean hasName() {
            return this.hasName;
        }
        
        public String getName() {
            return this.name_;
        }
        
        public List<EnumType> getMapList() {
            return this.map_;
        }
        
        public int getMapCount() {
            return this.map_.size();
        }
        
        public EnumType getMap(final int index) {
            return this.map_.get(index);
        }
        
        public final boolean isInitialized() {
            if (!this.hasName) {
                return false;
            }
            for (final EnumType element : this.getMapList()) {
                if (!element.isInitialized()) {
                    return false;
                }
            }
            return true;
        }
        
        public void writeTo(final CodedOutputStream output) throws IOException {
            if (this.hasName()) {
                output.writeString(1, this.getName());
            }
            for (final EnumType element : this.getMapList()) {
                output.writeMessage(2, (Message)element);
            }
            this.getUnknownFields().writeTo(output);
        }
        
        public int getSerializedSize() {
            int size = this.memoizedSerializedSize;
            if (size != -1) {
                return size;
            }
            size = 0;
            if (this.hasName()) {
                size += CodedOutputStream.computeStringSize(1, this.getName());
            }
            for (final EnumType element : this.getMapList()) {
                size += CodedOutputStream.computeMessageSize(2, (Message)element);
            }
            size += this.getUnknownFields().getSerializedSize();
            return this.memoizedSerializedSize = size;
        }
        
        public static EnumTypedef parseFrom(final ByteString data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static EnumTypedef parseFrom(final ByteString data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static EnumTypedef parseFrom(final byte[] data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static EnumTypedef parseFrom(final byte[] data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static EnumTypedef parseFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input)).buildParsed();
        }
        
        public static EnumTypedef parseFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static EnumTypedef parseDelimitedFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input)).buildParsed();
        }
        
        public static EnumTypedef parseDelimitedFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static EnumTypedef parseFrom(final CodedInputStream input) throws IOException {
            return newBuilder().mergeFrom(input).buildParsed();
        }
        
        public static EnumTypedef parseFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return newBuilder().mergeFrom(input, extensionRegistry).buildParsed();
        }
        
        public static Builder newBuilder() {
            return new Builder();
        }
        
        public Builder newBuilderForType() {
            return new Builder();
        }
        
        public static Builder newBuilder(final EnumTypedef prototype) {
            return new Builder().mergeFrom(prototype);
        }
        
        public Builder toBuilder() {
            return newBuilder(this);
        }
        
        static {
            defaultInstance = new EnumTypedef();
            NcStreamProto.getDescriptor();
        }
        
        public static final class EnumType extends GeneratedMessage
        {
            private static final EnumType defaultInstance;
            public static final int CODE_FIELD_NUMBER = 1;
            private boolean hasCode;
            private int code_;
            public static final int VALUE_FIELD_NUMBER = 2;
            private boolean hasValue;
            private String value_;
            private int memoizedSerializedSize;
            
            private EnumType() {
                this.code_ = 0;
                this.value_ = "";
                this.memoizedSerializedSize = -1;
            }
            
            public static EnumType getDefaultInstance() {
                return EnumType.defaultInstance;
            }
            
            public EnumType getDefaultInstanceForType() {
                return EnumType.defaultInstance;
            }
            
            public static final Descriptors.Descriptor getDescriptor() {
                return NcStreamProto.internal_static_ncstream_EnumTypedef_EnumType_descriptor;
            }
            
            protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
                return NcStreamProto.internal_static_ncstream_EnumTypedef_EnumType_fieldAccessorTable;
            }
            
            public boolean hasCode() {
                return this.hasCode;
            }
            
            public int getCode() {
                return this.code_;
            }
            
            public boolean hasValue() {
                return this.hasValue;
            }
            
            public String getValue() {
                return this.value_;
            }
            
            public final boolean isInitialized() {
                return this.hasCode && this.hasValue;
            }
            
            public void writeTo(final CodedOutputStream output) throws IOException {
                if (this.hasCode()) {
                    output.writeUInt32(1, this.getCode());
                }
                if (this.hasValue()) {
                    output.writeString(2, this.getValue());
                }
                this.getUnknownFields().writeTo(output);
            }
            
            public int getSerializedSize() {
                int size = this.memoizedSerializedSize;
                if (size != -1) {
                    return size;
                }
                size = 0;
                if (this.hasCode()) {
                    size += CodedOutputStream.computeUInt32Size(1, this.getCode());
                }
                if (this.hasValue()) {
                    size += CodedOutputStream.computeStringSize(2, this.getValue());
                }
                size += this.getUnknownFields().getSerializedSize();
                return this.memoizedSerializedSize = size;
            }
            
            public static EnumType parseFrom(final ByteString data) throws InvalidProtocolBufferException {
                return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
            }
            
            public static EnumType parseFrom(final ByteString data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
                return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
            }
            
            public static EnumType parseFrom(final byte[] data) throws InvalidProtocolBufferException {
                return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
            }
            
            public static EnumType parseFrom(final byte[] data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
                return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
            }
            
            public static EnumType parseFrom(final InputStream input) throws IOException {
                return ((Builder)newBuilder().mergeFrom(input)).buildParsed();
            }
            
            public static EnumType parseFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
                return ((Builder)newBuilder().mergeFrom(input, extensionRegistry)).buildParsed();
            }
            
            public static EnumType parseDelimitedFrom(final InputStream input) throws IOException {
                return ((Builder)newBuilder().mergeDelimitedFrom(input)).buildParsed();
            }
            
            public static EnumType parseDelimitedFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
                return ((Builder)newBuilder().mergeDelimitedFrom(input, extensionRegistry)).buildParsed();
            }
            
            public static EnumType parseFrom(final CodedInputStream input) throws IOException {
                return newBuilder().mergeFrom(input).buildParsed();
            }
            
            public static EnumType parseFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
                return newBuilder().mergeFrom(input, extensionRegistry).buildParsed();
            }
            
            public static Builder newBuilder() {
                return new Builder();
            }
            
            public Builder newBuilderForType() {
                return new Builder();
            }
            
            public static Builder newBuilder(final EnumType prototype) {
                return new Builder().mergeFrom(prototype);
            }
            
            public Builder toBuilder() {
                return newBuilder(this);
            }
            
            static {
                defaultInstance = new EnumType();
                NcStreamProto.getDescriptor();
            }
            
            public static final class Builder extends GeneratedMessage.Builder<Builder>
            {
                EnumType result;
                
                private Builder() {
                    this.result = new EnumType();
                }
                
                protected EnumType internalGetResult() {
                    return this.result;
                }
                
                public Builder clear() {
                    this.result = new EnumType();
                    return this;
                }
                
                public Builder clone() {
                    return new Builder().mergeFrom(this.result);
                }
                
                public Descriptors.Descriptor getDescriptorForType() {
                    return EnumType.getDescriptor();
                }
                
                public EnumType getDefaultInstanceForType() {
                    return EnumType.getDefaultInstance();
                }
                
                public EnumType build() {
                    if (this.result != null && !this.isInitialized()) {
                        throw new UninitializedMessageException((Message)this.result);
                    }
                    return this.buildPartial();
                }
                
                private EnumType buildParsed() throws InvalidProtocolBufferException {
                    if (!this.isInitialized()) {
                        throw new UninitializedMessageException((Message)this.result).asInvalidProtocolBufferException();
                    }
                    return this.buildPartial();
                }
                
                public EnumType buildPartial() {
                    if (this.result == null) {
                        throw new IllegalStateException("build() has already been called on this Builder.");
                    }
                    final EnumType returnMe = this.result;
                    this.result = null;
                    return returnMe;
                }
                
                public Builder mergeFrom(final Message other) {
                    if (other instanceof EnumType) {
                        return this.mergeFrom((EnumType)other);
                    }
                    super.mergeFrom(other);
                    return this;
                }
                
                public Builder mergeFrom(final EnumType other) {
                    if (other == EnumType.getDefaultInstance()) {
                        return this;
                    }
                    if (other.hasCode()) {
                        this.setCode(other.getCode());
                    }
                    if (other.hasValue()) {
                        this.setValue(other.getValue());
                    }
                    this.mergeUnknownFields(other.getUnknownFields());
                    return this;
                }
                
                public Builder mergeFrom(final CodedInputStream input) throws IOException {
                    return this.mergeFrom(input, ExtensionRegistry.getEmptyRegistry());
                }
                
                public Builder mergeFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
                    final UnknownFieldSet.Builder unknownFields = UnknownFieldSet.newBuilder(this.getUnknownFields());
                    while (true) {
                        final int tag = input.readTag();
                        switch (tag) {
                            case 0: {
                                this.setUnknownFields(unknownFields.build());
                                return this;
                            }
                            default: {
                                if (!this.parseUnknownField(input, unknownFields, extensionRegistry, tag)) {
                                    this.setUnknownFields(unknownFields.build());
                                    return this;
                                }
                                continue;
                            }
                            case 8: {
                                this.setCode(input.readUInt32());
                                continue;
                            }
                            case 18: {
                                this.setValue(input.readString());
                                continue;
                            }
                        }
                    }
                }
                
                public boolean hasCode() {
                    return this.result.hasCode();
                }
                
                public int getCode() {
                    return this.result.getCode();
                }
                
                public Builder setCode(final int value) {
                    this.result.hasCode = true;
                    this.result.code_ = value;
                    return this;
                }
                
                public Builder clearCode() {
                    this.result.hasCode = false;
                    this.result.code_ = 0;
                    return this;
                }
                
                public boolean hasValue() {
                    return this.result.hasValue();
                }
                
                public String getValue() {
                    return this.result.getValue();
                }
                
                public Builder setValue(final String value) {
                    if (value == null) {
                        throw new NullPointerException();
                    }
                    this.result.hasValue = true;
                    this.result.value_ = value;
                    return this;
                }
                
                public Builder clearValue() {
                    this.result.hasValue = false;
                    this.result.value_ = "";
                    return this;
                }
            }
        }
        
        public static final class Builder extends GeneratedMessage.Builder<Builder>
        {
            EnumTypedef result;
            
            private Builder() {
                this.result = new EnumTypedef();
            }
            
            protected EnumTypedef internalGetResult() {
                return this.result;
            }
            
            public Builder clear() {
                this.result = new EnumTypedef();
                return this;
            }
            
            public Builder clone() {
                return new Builder().mergeFrom(this.result);
            }
            
            public Descriptors.Descriptor getDescriptorForType() {
                return EnumTypedef.getDescriptor();
            }
            
            public EnumTypedef getDefaultInstanceForType() {
                return EnumTypedef.getDefaultInstance();
            }
            
            public EnumTypedef build() {
                if (this.result != null && !this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result);
                }
                return this.buildPartial();
            }
            
            private EnumTypedef buildParsed() throws InvalidProtocolBufferException {
                if (!this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result).asInvalidProtocolBufferException();
                }
                return this.buildPartial();
            }
            
            public EnumTypedef buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.map_ != Collections.EMPTY_LIST) {
                    this.result.map_ = (List<EnumType>)Collections.unmodifiableList((List<?>)this.result.map_);
                }
                final EnumTypedef returnMe = this.result;
                this.result = null;
                return returnMe;
            }
            
            public Builder mergeFrom(final Message other) {
                if (other instanceof EnumTypedef) {
                    return this.mergeFrom((EnumTypedef)other);
                }
                super.mergeFrom(other);
                return this;
            }
            
            public Builder mergeFrom(final EnumTypedef other) {
                if (other == EnumTypedef.getDefaultInstance()) {
                    return this;
                }
                if (other.hasName()) {
                    this.setName(other.getName());
                }
                if (!other.map_.isEmpty()) {
                    if (this.result.map_.isEmpty()) {
                        this.result.map_ = (List<EnumType>)new ArrayList();
                    }
                    this.result.map_.addAll(other.map_);
                }
                this.mergeUnknownFields(other.getUnknownFields());
                return this;
            }
            
            public Builder mergeFrom(final CodedInputStream input) throws IOException {
                return this.mergeFrom(input, ExtensionRegistry.getEmptyRegistry());
            }
            
            public Builder mergeFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
                final UnknownFieldSet.Builder unknownFields = UnknownFieldSet.newBuilder(this.getUnknownFields());
                while (true) {
                    final int tag = input.readTag();
                    switch (tag) {
                        case 0: {
                            this.setUnknownFields(unknownFields.build());
                            return this;
                        }
                        default: {
                            if (!this.parseUnknownField(input, unknownFields, extensionRegistry, tag)) {
                                this.setUnknownFields(unknownFields.build());
                                return this;
                            }
                            continue;
                        }
                        case 10: {
                            this.setName(input.readString());
                            continue;
                        }
                        case 18: {
                            final EnumType.Builder subBuilder = EnumType.newBuilder();
                            input.readMessage((Message.Builder)subBuilder, extensionRegistry);
                            this.addMap(subBuilder.buildPartial());
                            continue;
                        }
                    }
                }
            }
            
            public boolean hasName() {
                return this.result.hasName();
            }
            
            public String getName() {
                return this.result.getName();
            }
            
            public Builder setName(final String value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasName = true;
                this.result.name_ = value;
                return this;
            }
            
            public Builder clearName() {
                this.result.hasName = false;
                this.result.name_ = "";
                return this;
            }
            
            public List<EnumType> getMapList() {
                return Collections.unmodifiableList((List<? extends EnumType>)this.result.map_);
            }
            
            public int getMapCount() {
                return this.result.getMapCount();
            }
            
            public EnumType getMap(final int index) {
                return this.result.getMap(index);
            }
            
            public Builder setMap(final int index, final EnumType value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.map_.set(index, value);
                return this;
            }
            
            public Builder setMap(final int index, final EnumType.Builder builderForValue) {
                this.result.map_.set(index, builderForValue.build());
                return this;
            }
            
            public Builder addMap(final EnumType value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                if (this.result.map_.isEmpty()) {
                    this.result.map_ = (List<EnumType>)new ArrayList();
                }
                this.result.map_.add(value);
                return this;
            }
            
            public Builder addMap(final EnumType.Builder builderForValue) {
                if (this.result.map_.isEmpty()) {
                    this.result.map_ = (List<EnumType>)new ArrayList();
                }
                this.result.map_.add(builderForValue.build());
                return this;
            }
            
            public Builder addAllMap(final Iterable<? extends EnumType> values) {
                if (this.result.map_.isEmpty()) {
                    this.result.map_ = (List<EnumType>)new ArrayList();
                }
                super.addAll((Iterable)values, (Collection)this.result.map_);
                return this;
            }
            
            public Builder clearMap() {
                this.result.map_ = (List<EnumType>)Collections.emptyList();
                return this;
            }
        }
    }
    
    public static final class Group extends GeneratedMessage
    {
        private static final Group defaultInstance;
        public static final int NAME_FIELD_NUMBER = 1;
        private boolean hasName;
        private String name_;
        public static final int DIMS_FIELD_NUMBER = 2;
        private List<Dimension> dims_;
        public static final int VARS_FIELD_NUMBER = 3;
        private List<Variable> vars_;
        public static final int STRUCTS_FIELD_NUMBER = 4;
        private List<Structure> structs_;
        public static final int ATTS_FIELD_NUMBER = 5;
        private List<Attribute> atts_;
        public static final int GROUPS_FIELD_NUMBER = 6;
        private List<Group> groups_;
        public static final int ENUMTYPES_FIELD_NUMBER = 7;
        private List<EnumTypedef> enumTypes_;
        private int memoizedSerializedSize;
        
        private Group() {
            this.name_ = "";
            this.dims_ = Collections.emptyList();
            this.vars_ = Collections.emptyList();
            this.structs_ = Collections.emptyList();
            this.atts_ = Collections.emptyList();
            this.groups_ = Collections.emptyList();
            this.enumTypes_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }
        
        public static Group getDefaultInstance() {
            return Group.defaultInstance;
        }
        
        public Group getDefaultInstanceForType() {
            return Group.defaultInstance;
        }
        
        public static final Descriptors.Descriptor getDescriptor() {
            return NcStreamProto.internal_static_ncstream_Group_descriptor;
        }
        
        protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return NcStreamProto.internal_static_ncstream_Group_fieldAccessorTable;
        }
        
        public boolean hasName() {
            return this.hasName;
        }
        
        public String getName() {
            return this.name_;
        }
        
        public List<Dimension> getDimsList() {
            return this.dims_;
        }
        
        public int getDimsCount() {
            return this.dims_.size();
        }
        
        public Dimension getDims(final int index) {
            return this.dims_.get(index);
        }
        
        public List<Variable> getVarsList() {
            return this.vars_;
        }
        
        public int getVarsCount() {
            return this.vars_.size();
        }
        
        public Variable getVars(final int index) {
            return this.vars_.get(index);
        }
        
        public List<Structure> getStructsList() {
            return this.structs_;
        }
        
        public int getStructsCount() {
            return this.structs_.size();
        }
        
        public Structure getStructs(final int index) {
            return this.structs_.get(index);
        }
        
        public List<Attribute> getAttsList() {
            return this.atts_;
        }
        
        public int getAttsCount() {
            return this.atts_.size();
        }
        
        public Attribute getAtts(final int index) {
            return this.atts_.get(index);
        }
        
        public List<Group> getGroupsList() {
            return this.groups_;
        }
        
        public int getGroupsCount() {
            return this.groups_.size();
        }
        
        public Group getGroups(final int index) {
            return this.groups_.get(index);
        }
        
        public List<EnumTypedef> getEnumTypesList() {
            return this.enumTypes_;
        }
        
        public int getEnumTypesCount() {
            return this.enumTypes_.size();
        }
        
        public EnumTypedef getEnumTypes(final int index) {
            return this.enumTypes_.get(index);
        }
        
        public final boolean isInitialized() {
            if (!this.hasName) {
                return false;
            }
            for (final Variable element : this.getVarsList()) {
                if (!element.isInitialized()) {
                    return false;
                }
            }
            for (final Structure element2 : this.getStructsList()) {
                if (!element2.isInitialized()) {
                    return false;
                }
            }
            for (final Attribute element3 : this.getAttsList()) {
                if (!element3.isInitialized()) {
                    return false;
                }
            }
            for (final Group element4 : this.getGroupsList()) {
                if (!element4.isInitialized()) {
                    return false;
                }
            }
            for (final EnumTypedef element5 : this.getEnumTypesList()) {
                if (!element5.isInitialized()) {
                    return false;
                }
            }
            return true;
        }
        
        public void writeTo(final CodedOutputStream output) throws IOException {
            if (this.hasName()) {
                output.writeString(1, this.getName());
            }
            for (final Dimension element : this.getDimsList()) {
                output.writeMessage(2, (Message)element);
            }
            for (final Variable element2 : this.getVarsList()) {
                output.writeMessage(3, (Message)element2);
            }
            for (final Structure element3 : this.getStructsList()) {
                output.writeMessage(4, (Message)element3);
            }
            for (final Attribute element4 : this.getAttsList()) {
                output.writeMessage(5, (Message)element4);
            }
            for (final Group element5 : this.getGroupsList()) {
                output.writeMessage(6, (Message)element5);
            }
            for (final EnumTypedef element6 : this.getEnumTypesList()) {
                output.writeMessage(7, (Message)element6);
            }
            this.getUnknownFields().writeTo(output);
        }
        
        public int getSerializedSize() {
            int size = this.memoizedSerializedSize;
            if (size != -1) {
                return size;
            }
            size = 0;
            if (this.hasName()) {
                size += CodedOutputStream.computeStringSize(1, this.getName());
            }
            for (final Dimension element : this.getDimsList()) {
                size += CodedOutputStream.computeMessageSize(2, (Message)element);
            }
            for (final Variable element2 : this.getVarsList()) {
                size += CodedOutputStream.computeMessageSize(3, (Message)element2);
            }
            for (final Structure element3 : this.getStructsList()) {
                size += CodedOutputStream.computeMessageSize(4, (Message)element3);
            }
            for (final Attribute element4 : this.getAttsList()) {
                size += CodedOutputStream.computeMessageSize(5, (Message)element4);
            }
            for (final Group element5 : this.getGroupsList()) {
                size += CodedOutputStream.computeMessageSize(6, (Message)element5);
            }
            for (final EnumTypedef element6 : this.getEnumTypesList()) {
                size += CodedOutputStream.computeMessageSize(7, (Message)element6);
            }
            size += this.getUnknownFields().getSerializedSize();
            return this.memoizedSerializedSize = size;
        }
        
        public static Group parseFrom(final ByteString data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static Group parseFrom(final ByteString data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static Group parseFrom(final byte[] data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static Group parseFrom(final byte[] data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static Group parseFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input)).buildParsed();
        }
        
        public static Group parseFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static Group parseDelimitedFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input)).buildParsed();
        }
        
        public static Group parseDelimitedFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static Group parseFrom(final CodedInputStream input) throws IOException {
            return newBuilder().mergeFrom(input).buildParsed();
        }
        
        public static Group parseFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return newBuilder().mergeFrom(input, extensionRegistry).buildParsed();
        }
        
        public static Builder newBuilder() {
            return new Builder();
        }
        
        public Builder newBuilderForType() {
            return new Builder();
        }
        
        public static Builder newBuilder(final Group prototype) {
            return new Builder().mergeFrom(prototype);
        }
        
        public Builder toBuilder() {
            return newBuilder(this);
        }
        
        static {
            defaultInstance = new Group();
            NcStreamProto.getDescriptor();
        }
        
        public static final class Builder extends GeneratedMessage.Builder<Builder>
        {
            Group result;
            
            private Builder() {
                this.result = new Group();
            }
            
            protected Group internalGetResult() {
                return this.result;
            }
            
            public Builder clear() {
                this.result = new Group();
                return this;
            }
            
            public Builder clone() {
                return new Builder().mergeFrom(this.result);
            }
            
            public Descriptors.Descriptor getDescriptorForType() {
                return Group.getDescriptor();
            }
            
            public Group getDefaultInstanceForType() {
                return Group.getDefaultInstance();
            }
            
            public Group build() {
                if (this.result != null && !this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result);
                }
                return this.buildPartial();
            }
            
            private Group buildParsed() throws InvalidProtocolBufferException {
                if (!this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result).asInvalidProtocolBufferException();
                }
                return this.buildPartial();
            }
            
            public Group buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.dims_ != Collections.EMPTY_LIST) {
                    this.result.dims_ = (List<Dimension>)Collections.unmodifiableList((List<?>)this.result.dims_);
                }
                if (this.result.vars_ != Collections.EMPTY_LIST) {
                    this.result.vars_ = (List<Variable>)Collections.unmodifiableList((List<?>)this.result.vars_);
                }
                if (this.result.structs_ != Collections.EMPTY_LIST) {
                    this.result.structs_ = (List<Structure>)Collections.unmodifiableList((List<?>)this.result.structs_);
                }
                if (this.result.atts_ != Collections.EMPTY_LIST) {
                    this.result.atts_ = (List<Attribute>)Collections.unmodifiableList((List<?>)this.result.atts_);
                }
                if (this.result.groups_ != Collections.EMPTY_LIST) {
                    this.result.groups_ = (List<Group>)Collections.unmodifiableList((List<?>)this.result.groups_);
                }
                if (this.result.enumTypes_ != Collections.EMPTY_LIST) {
                    this.result.enumTypes_ = (List<EnumTypedef>)Collections.unmodifiableList((List<?>)this.result.enumTypes_);
                }
                final Group returnMe = this.result;
                this.result = null;
                return returnMe;
            }
            
            public Builder mergeFrom(final Message other) {
                if (other instanceof Group) {
                    return this.mergeFrom((Group)other);
                }
                super.mergeFrom(other);
                return this;
            }
            
            public Builder mergeFrom(final Group other) {
                if (other == Group.getDefaultInstance()) {
                    return this;
                }
                if (other.hasName()) {
                    this.setName(other.getName());
                }
                if (!other.dims_.isEmpty()) {
                    if (this.result.dims_.isEmpty()) {
                        this.result.dims_ = (List<Dimension>)new ArrayList();
                    }
                    this.result.dims_.addAll(other.dims_);
                }
                if (!other.vars_.isEmpty()) {
                    if (this.result.vars_.isEmpty()) {
                        this.result.vars_ = (List<Variable>)new ArrayList();
                    }
                    this.result.vars_.addAll(other.vars_);
                }
                if (!other.structs_.isEmpty()) {
                    if (this.result.structs_.isEmpty()) {
                        this.result.structs_ = (List<Structure>)new ArrayList();
                    }
                    this.result.structs_.addAll(other.structs_);
                }
                if (!other.atts_.isEmpty()) {
                    if (this.result.atts_.isEmpty()) {
                        this.result.atts_ = (List<Attribute>)new ArrayList();
                    }
                    this.result.atts_.addAll(other.atts_);
                }
                if (!other.groups_.isEmpty()) {
                    if (this.result.groups_.isEmpty()) {
                        this.result.groups_ = (List<Group>)new ArrayList();
                    }
                    this.result.groups_.addAll(other.groups_);
                }
                if (!other.enumTypes_.isEmpty()) {
                    if (this.result.enumTypes_.isEmpty()) {
                        this.result.enumTypes_ = (List<EnumTypedef>)new ArrayList();
                    }
                    this.result.enumTypes_.addAll(other.enumTypes_);
                }
                this.mergeUnknownFields(other.getUnknownFields());
                return this;
            }
            
            public Builder mergeFrom(final CodedInputStream input) throws IOException {
                return this.mergeFrom(input, ExtensionRegistry.getEmptyRegistry());
            }
            
            public Builder mergeFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
                final UnknownFieldSet.Builder unknownFields = UnknownFieldSet.newBuilder(this.getUnknownFields());
                while (true) {
                    final int tag = input.readTag();
                    switch (tag) {
                        case 0: {
                            this.setUnknownFields(unknownFields.build());
                            return this;
                        }
                        default: {
                            if (!this.parseUnknownField(input, unknownFields, extensionRegistry, tag)) {
                                this.setUnknownFields(unknownFields.build());
                                return this;
                            }
                            continue;
                        }
                        case 10: {
                            this.setName(input.readString());
                            continue;
                        }
                        case 18: {
                            final Dimension.Builder subBuilder = Dimension.newBuilder();
                            input.readMessage((Message.Builder)subBuilder, extensionRegistry);
                            this.addDims(subBuilder.buildPartial());
                            continue;
                        }
                        case 26: {
                            final Variable.Builder subBuilder2 = Variable.newBuilder();
                            input.readMessage((Message.Builder)subBuilder2, extensionRegistry);
                            this.addVars(subBuilder2.buildPartial());
                            continue;
                        }
                        case 34: {
                            final Structure.Builder subBuilder3 = Structure.newBuilder();
                            input.readMessage((Message.Builder)subBuilder3, extensionRegistry);
                            this.addStructs(subBuilder3.buildPartial());
                            continue;
                        }
                        case 42: {
                            final Attribute.Builder subBuilder4 = Attribute.newBuilder();
                            input.readMessage((Message.Builder)subBuilder4, extensionRegistry);
                            this.addAtts(subBuilder4.buildPartial());
                            continue;
                        }
                        case 50: {
                            final Builder subBuilder5 = Group.newBuilder();
                            input.readMessage((Message.Builder)subBuilder5, extensionRegistry);
                            this.addGroups(subBuilder5.buildPartial());
                            continue;
                        }
                        case 58: {
                            final EnumTypedef.Builder subBuilder6 = EnumTypedef.newBuilder();
                            input.readMessage((Message.Builder)subBuilder6, extensionRegistry);
                            this.addEnumTypes(subBuilder6.buildPartial());
                            continue;
                        }
                    }
                }
            }
            
            public boolean hasName() {
                return this.result.hasName();
            }
            
            public String getName() {
                return this.result.getName();
            }
            
            public Builder setName(final String value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasName = true;
                this.result.name_ = value;
                return this;
            }
            
            public Builder clearName() {
                this.result.hasName = false;
                this.result.name_ = "";
                return this;
            }
            
            public List<Dimension> getDimsList() {
                return Collections.unmodifiableList((List<? extends Dimension>)this.result.dims_);
            }
            
            public int getDimsCount() {
                return this.result.getDimsCount();
            }
            
            public Dimension getDims(final int index) {
                return this.result.getDims(index);
            }
            
            public Builder setDims(final int index, final Dimension value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.dims_.set(index, value);
                return this;
            }
            
            public Builder setDims(final int index, final Dimension.Builder builderForValue) {
                this.result.dims_.set(index, builderForValue.build());
                return this;
            }
            
            public Builder addDims(final Dimension value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                if (this.result.dims_.isEmpty()) {
                    this.result.dims_ = (List<Dimension>)new ArrayList();
                }
                this.result.dims_.add(value);
                return this;
            }
            
            public Builder addDims(final Dimension.Builder builderForValue) {
                if (this.result.dims_.isEmpty()) {
                    this.result.dims_ = (List<Dimension>)new ArrayList();
                }
                this.result.dims_.add(builderForValue.build());
                return this;
            }
            
            public Builder addAllDims(final Iterable<? extends Dimension> values) {
                if (this.result.dims_.isEmpty()) {
                    this.result.dims_ = (List<Dimension>)new ArrayList();
                }
                super.addAll((Iterable)values, (Collection)this.result.dims_);
                return this;
            }
            
            public Builder clearDims() {
                this.result.dims_ = (List<Dimension>)Collections.emptyList();
                return this;
            }
            
            public List<Variable> getVarsList() {
                return Collections.unmodifiableList((List<? extends Variable>)this.result.vars_);
            }
            
            public int getVarsCount() {
                return this.result.getVarsCount();
            }
            
            public Variable getVars(final int index) {
                return this.result.getVars(index);
            }
            
            public Builder setVars(final int index, final Variable value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.vars_.set(index, value);
                return this;
            }
            
            public Builder setVars(final int index, final Variable.Builder builderForValue) {
                this.result.vars_.set(index, builderForValue.build());
                return this;
            }
            
            public Builder addVars(final Variable value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                if (this.result.vars_.isEmpty()) {
                    this.result.vars_ = (List<Variable>)new ArrayList();
                }
                this.result.vars_.add(value);
                return this;
            }
            
            public Builder addVars(final Variable.Builder builderForValue) {
                if (this.result.vars_.isEmpty()) {
                    this.result.vars_ = (List<Variable>)new ArrayList();
                }
                this.result.vars_.add(builderForValue.build());
                return this;
            }
            
            public Builder addAllVars(final Iterable<? extends Variable> values) {
                if (this.result.vars_.isEmpty()) {
                    this.result.vars_ = (List<Variable>)new ArrayList();
                }
                super.addAll((Iterable)values, (Collection)this.result.vars_);
                return this;
            }
            
            public Builder clearVars() {
                this.result.vars_ = (List<Variable>)Collections.emptyList();
                return this;
            }
            
            public List<Structure> getStructsList() {
                return Collections.unmodifiableList((List<? extends Structure>)this.result.structs_);
            }
            
            public int getStructsCount() {
                return this.result.getStructsCount();
            }
            
            public Structure getStructs(final int index) {
                return this.result.getStructs(index);
            }
            
            public Builder setStructs(final int index, final Structure value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.structs_.set(index, value);
                return this;
            }
            
            public Builder setStructs(final int index, final Structure.Builder builderForValue) {
                this.result.structs_.set(index, builderForValue.build());
                return this;
            }
            
            public Builder addStructs(final Structure value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                if (this.result.structs_.isEmpty()) {
                    this.result.structs_ = (List<Structure>)new ArrayList();
                }
                this.result.structs_.add(value);
                return this;
            }
            
            public Builder addStructs(final Structure.Builder builderForValue) {
                if (this.result.structs_.isEmpty()) {
                    this.result.structs_ = (List<Structure>)new ArrayList();
                }
                this.result.structs_.add(builderForValue.build());
                return this;
            }
            
            public Builder addAllStructs(final Iterable<? extends Structure> values) {
                if (this.result.structs_.isEmpty()) {
                    this.result.structs_ = (List<Structure>)new ArrayList();
                }
                super.addAll((Iterable)values, (Collection)this.result.structs_);
                return this;
            }
            
            public Builder clearStructs() {
                this.result.structs_ = (List<Structure>)Collections.emptyList();
                return this;
            }
            
            public List<Attribute> getAttsList() {
                return Collections.unmodifiableList((List<? extends Attribute>)this.result.atts_);
            }
            
            public int getAttsCount() {
                return this.result.getAttsCount();
            }
            
            public Attribute getAtts(final int index) {
                return this.result.getAtts(index);
            }
            
            public Builder setAtts(final int index, final Attribute value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.atts_.set(index, value);
                return this;
            }
            
            public Builder setAtts(final int index, final Attribute.Builder builderForValue) {
                this.result.atts_.set(index, builderForValue.build());
                return this;
            }
            
            public Builder addAtts(final Attribute value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                if (this.result.atts_.isEmpty()) {
                    this.result.atts_ = (List<Attribute>)new ArrayList();
                }
                this.result.atts_.add(value);
                return this;
            }
            
            public Builder addAtts(final Attribute.Builder builderForValue) {
                if (this.result.atts_.isEmpty()) {
                    this.result.atts_ = (List<Attribute>)new ArrayList();
                }
                this.result.atts_.add(builderForValue.build());
                return this;
            }
            
            public Builder addAllAtts(final Iterable<? extends Attribute> values) {
                if (this.result.atts_.isEmpty()) {
                    this.result.atts_ = (List<Attribute>)new ArrayList();
                }
                super.addAll((Iterable)values, (Collection)this.result.atts_);
                return this;
            }
            
            public Builder clearAtts() {
                this.result.atts_ = (List<Attribute>)Collections.emptyList();
                return this;
            }
            
            public List<Group> getGroupsList() {
                return Collections.unmodifiableList((List<? extends Group>)this.result.groups_);
            }
            
            public int getGroupsCount() {
                return this.result.getGroupsCount();
            }
            
            public Group getGroups(final int index) {
                return this.result.getGroups(index);
            }
            
            public Builder setGroups(final int index, final Group value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.groups_.set(index, value);
                return this;
            }
            
            public Builder setGroups(final int index, final Builder builderForValue) {
                this.result.groups_.set(index, builderForValue.build());
                return this;
            }
            
            public Builder addGroups(final Group value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                if (this.result.groups_.isEmpty()) {
                    this.result.groups_ = (List<Group>)new ArrayList();
                }
                this.result.groups_.add(value);
                return this;
            }
            
            public Builder addGroups(final Builder builderForValue) {
                if (this.result.groups_.isEmpty()) {
                    this.result.groups_ = (List<Group>)new ArrayList();
                }
                this.result.groups_.add(builderForValue.build());
                return this;
            }
            
            public Builder addAllGroups(final Iterable<? extends Group> values) {
                if (this.result.groups_.isEmpty()) {
                    this.result.groups_ = (List<Group>)new ArrayList();
                }
                super.addAll((Iterable)values, (Collection)this.result.groups_);
                return this;
            }
            
            public Builder clearGroups() {
                this.result.groups_ = (List<Group>)Collections.emptyList();
                return this;
            }
            
            public List<EnumTypedef> getEnumTypesList() {
                return Collections.unmodifiableList((List<? extends EnumTypedef>)this.result.enumTypes_);
            }
            
            public int getEnumTypesCount() {
                return this.result.getEnumTypesCount();
            }
            
            public EnumTypedef getEnumTypes(final int index) {
                return this.result.getEnumTypes(index);
            }
            
            public Builder setEnumTypes(final int index, final EnumTypedef value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.enumTypes_.set(index, value);
                return this;
            }
            
            public Builder setEnumTypes(final int index, final EnumTypedef.Builder builderForValue) {
                this.result.enumTypes_.set(index, builderForValue.build());
                return this;
            }
            
            public Builder addEnumTypes(final EnumTypedef value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                if (this.result.enumTypes_.isEmpty()) {
                    this.result.enumTypes_ = (List<EnumTypedef>)new ArrayList();
                }
                this.result.enumTypes_.add(value);
                return this;
            }
            
            public Builder addEnumTypes(final EnumTypedef.Builder builderForValue) {
                if (this.result.enumTypes_.isEmpty()) {
                    this.result.enumTypes_ = (List<EnumTypedef>)new ArrayList();
                }
                this.result.enumTypes_.add(builderForValue.build());
                return this;
            }
            
            public Builder addAllEnumTypes(final Iterable<? extends EnumTypedef> values) {
                if (this.result.enumTypes_.isEmpty()) {
                    this.result.enumTypes_ = (List<EnumTypedef>)new ArrayList();
                }
                super.addAll((Iterable)values, (Collection)this.result.enumTypes_);
                return this;
            }
            
            public Builder clearEnumTypes() {
                this.result.enumTypes_ = (List<EnumTypedef>)Collections.emptyList();
                return this;
            }
        }
    }
    
    public static final class Header extends GeneratedMessage
    {
        private static final Header defaultInstance;
        public static final int LOCATION_FIELD_NUMBER = 1;
        private boolean hasLocation;
        private String location_;
        public static final int TITLE_FIELD_NUMBER = 2;
        private boolean hasTitle;
        private String title_;
        public static final int ID_FIELD_NUMBER = 3;
        private boolean hasId;
        private String id_;
        public static final int ROOT_FIELD_NUMBER = 4;
        private boolean hasRoot;
        private Group root_;
        private int memoizedSerializedSize;
        
        private Header() {
            this.location_ = "";
            this.title_ = "";
            this.id_ = "";
            this.root_ = Group.getDefaultInstance();
            this.memoizedSerializedSize = -1;
        }
        
        public static Header getDefaultInstance() {
            return Header.defaultInstance;
        }
        
        public Header getDefaultInstanceForType() {
            return Header.defaultInstance;
        }
        
        public static final Descriptors.Descriptor getDescriptor() {
            return NcStreamProto.internal_static_ncstream_Header_descriptor;
        }
        
        protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return NcStreamProto.internal_static_ncstream_Header_fieldAccessorTable;
        }
        
        public boolean hasLocation() {
            return this.hasLocation;
        }
        
        public String getLocation() {
            return this.location_;
        }
        
        public boolean hasTitle() {
            return this.hasTitle;
        }
        
        public String getTitle() {
            return this.title_;
        }
        
        public boolean hasId() {
            return this.hasId;
        }
        
        public String getId() {
            return this.id_;
        }
        
        public boolean hasRoot() {
            return this.hasRoot;
        }
        
        public Group getRoot() {
            return this.root_;
        }
        
        public final boolean isInitialized() {
            return this.hasRoot && this.getRoot().isInitialized();
        }
        
        public void writeTo(final CodedOutputStream output) throws IOException {
            if (this.hasLocation()) {
                output.writeString(1, this.getLocation());
            }
            if (this.hasTitle()) {
                output.writeString(2, this.getTitle());
            }
            if (this.hasId()) {
                output.writeString(3, this.getId());
            }
            if (this.hasRoot()) {
                output.writeMessage(4, (Message)this.getRoot());
            }
            this.getUnknownFields().writeTo(output);
        }
        
        public int getSerializedSize() {
            int size = this.memoizedSerializedSize;
            if (size != -1) {
                return size;
            }
            size = 0;
            if (this.hasLocation()) {
                size += CodedOutputStream.computeStringSize(1, this.getLocation());
            }
            if (this.hasTitle()) {
                size += CodedOutputStream.computeStringSize(2, this.getTitle());
            }
            if (this.hasId()) {
                size += CodedOutputStream.computeStringSize(3, this.getId());
            }
            if (this.hasRoot()) {
                size += CodedOutputStream.computeMessageSize(4, (Message)this.getRoot());
            }
            size += this.getUnknownFields().getSerializedSize();
            return this.memoizedSerializedSize = size;
        }
        
        public static Header parseFrom(final ByteString data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static Header parseFrom(final ByteString data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static Header parseFrom(final byte[] data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static Header parseFrom(final byte[] data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static Header parseFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input)).buildParsed();
        }
        
        public static Header parseFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static Header parseDelimitedFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input)).buildParsed();
        }
        
        public static Header parseDelimitedFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static Header parseFrom(final CodedInputStream input) throws IOException {
            return newBuilder().mergeFrom(input).buildParsed();
        }
        
        public static Header parseFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return newBuilder().mergeFrom(input, extensionRegistry).buildParsed();
        }
        
        public static Builder newBuilder() {
            return new Builder();
        }
        
        public Builder newBuilderForType() {
            return new Builder();
        }
        
        public static Builder newBuilder(final Header prototype) {
            return new Builder().mergeFrom(prototype);
        }
        
        public Builder toBuilder() {
            return newBuilder(this);
        }
        
        static {
            defaultInstance = new Header();
            NcStreamProto.getDescriptor();
        }
        
        public static final class Builder extends GeneratedMessage.Builder<Builder>
        {
            Header result;
            
            private Builder() {
                this.result = new Header();
            }
            
            protected Header internalGetResult() {
                return this.result;
            }
            
            public Builder clear() {
                this.result = new Header();
                return this;
            }
            
            public Builder clone() {
                return new Builder().mergeFrom(this.result);
            }
            
            public Descriptors.Descriptor getDescriptorForType() {
                return Header.getDescriptor();
            }
            
            public Header getDefaultInstanceForType() {
                return Header.getDefaultInstance();
            }
            
            public Header build() {
                if (this.result != null && !this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result);
                }
                return this.buildPartial();
            }
            
            private Header buildParsed() throws InvalidProtocolBufferException {
                if (!this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result).asInvalidProtocolBufferException();
                }
                return this.buildPartial();
            }
            
            public Header buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                final Header returnMe = this.result;
                this.result = null;
                return returnMe;
            }
            
            public Builder mergeFrom(final Message other) {
                if (other instanceof Header) {
                    return this.mergeFrom((Header)other);
                }
                super.mergeFrom(other);
                return this;
            }
            
            public Builder mergeFrom(final Header other) {
                if (other == Header.getDefaultInstance()) {
                    return this;
                }
                if (other.hasLocation()) {
                    this.setLocation(other.getLocation());
                }
                if (other.hasTitle()) {
                    this.setTitle(other.getTitle());
                }
                if (other.hasId()) {
                    this.setId(other.getId());
                }
                if (other.hasRoot()) {
                    this.mergeRoot(other.getRoot());
                }
                this.mergeUnknownFields(other.getUnknownFields());
                return this;
            }
            
            public Builder mergeFrom(final CodedInputStream input) throws IOException {
                return this.mergeFrom(input, ExtensionRegistry.getEmptyRegistry());
            }
            
            public Builder mergeFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
                final UnknownFieldSet.Builder unknownFields = UnknownFieldSet.newBuilder(this.getUnknownFields());
                while (true) {
                    final int tag = input.readTag();
                    switch (tag) {
                        case 0: {
                            this.setUnknownFields(unknownFields.build());
                            return this;
                        }
                        default: {
                            if (!this.parseUnknownField(input, unknownFields, extensionRegistry, tag)) {
                                this.setUnknownFields(unknownFields.build());
                                return this;
                            }
                            continue;
                        }
                        case 10: {
                            this.setLocation(input.readString());
                            continue;
                        }
                        case 18: {
                            this.setTitle(input.readString());
                            continue;
                        }
                        case 26: {
                            this.setId(input.readString());
                            continue;
                        }
                        case 34: {
                            final Group.Builder subBuilder = Group.newBuilder();
                            if (this.hasRoot()) {
                                subBuilder.mergeFrom(this.getRoot());
                            }
                            input.readMessage((Message.Builder)subBuilder, extensionRegistry);
                            this.setRoot(subBuilder.buildPartial());
                            continue;
                        }
                    }
                }
            }
            
            public boolean hasLocation() {
                return this.result.hasLocation();
            }
            
            public String getLocation() {
                return this.result.getLocation();
            }
            
            public Builder setLocation(final String value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasLocation = true;
                this.result.location_ = value;
                return this;
            }
            
            public Builder clearLocation() {
                this.result.hasLocation = false;
                this.result.location_ = "";
                return this;
            }
            
            public boolean hasTitle() {
                return this.result.hasTitle();
            }
            
            public String getTitle() {
                return this.result.getTitle();
            }
            
            public Builder setTitle(final String value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasTitle = true;
                this.result.title_ = value;
                return this;
            }
            
            public Builder clearTitle() {
                this.result.hasTitle = false;
                this.result.title_ = "";
                return this;
            }
            
            public boolean hasId() {
                return this.result.hasId();
            }
            
            public String getId() {
                return this.result.getId();
            }
            
            public Builder setId(final String value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasId = true;
                this.result.id_ = value;
                return this;
            }
            
            public Builder clearId() {
                this.result.hasId = false;
                this.result.id_ = "";
                return this;
            }
            
            public boolean hasRoot() {
                return this.result.hasRoot();
            }
            
            public Group getRoot() {
                return this.result.getRoot();
            }
            
            public Builder setRoot(final Group value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasRoot = true;
                this.result.root_ = value;
                return this;
            }
            
            public Builder setRoot(final Group.Builder builderForValue) {
                this.result.hasRoot = true;
                this.result.root_ = builderForValue.build();
                return this;
            }
            
            public Builder mergeRoot(final Group value) {
                if (this.result.hasRoot() && this.result.root_ != Group.getDefaultInstance()) {
                    this.result.root_ = Group.newBuilder(this.result.root_).mergeFrom(value).buildPartial();
                }
                else {
                    this.result.root_ = value;
                }
                this.result.hasRoot = true;
                return this;
            }
            
            public Builder clearRoot() {
                this.result.hasRoot = false;
                this.result.root_ = Group.getDefaultInstance();
                return this;
            }
        }
    }
    
    public static final class Data extends GeneratedMessage
    {
        private static final Data defaultInstance;
        public static final int VARNAME_FIELD_NUMBER = 1;
        private boolean hasVarName;
        private String varName_;
        public static final int DATATYPE_FIELD_NUMBER = 2;
        private boolean hasDataType;
        private DataType dataType_;
        public static final int SECTION_FIELD_NUMBER = 3;
        private boolean hasSection;
        private Section section_;
        public static final int BIGEND_FIELD_NUMBER = 4;
        private boolean hasBigend;
        private boolean bigend_;
        private int memoizedSerializedSize;
        
        private Data() {
            this.varName_ = "";
            this.dataType_ = DataType.CHAR;
            this.section_ = Section.getDefaultInstance();
            this.bigend_ = true;
            this.memoizedSerializedSize = -1;
        }
        
        public static Data getDefaultInstance() {
            return Data.defaultInstance;
        }
        
        public Data getDefaultInstanceForType() {
            return Data.defaultInstance;
        }
        
        public static final Descriptors.Descriptor getDescriptor() {
            return NcStreamProto.internal_static_ncstream_Data_descriptor;
        }
        
        protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return NcStreamProto.internal_static_ncstream_Data_fieldAccessorTable;
        }
        
        public boolean hasVarName() {
            return this.hasVarName;
        }
        
        public String getVarName() {
            return this.varName_;
        }
        
        public boolean hasDataType() {
            return this.hasDataType;
        }
        
        public DataType getDataType() {
            return this.dataType_;
        }
        
        public boolean hasSection() {
            return this.hasSection;
        }
        
        public Section getSection() {
            return this.section_;
        }
        
        public boolean hasBigend() {
            return this.hasBigend;
        }
        
        public boolean getBigend() {
            return this.bigend_;
        }
        
        public final boolean isInitialized() {
            return this.hasVarName && this.hasDataType && this.hasSection && this.getSection().isInitialized();
        }
        
        public void writeTo(final CodedOutputStream output) throws IOException {
            if (this.hasVarName()) {
                output.writeString(1, this.getVarName());
            }
            if (this.hasDataType()) {
                output.writeEnum(2, this.getDataType().getNumber());
            }
            if (this.hasSection()) {
                output.writeMessage(3, (Message)this.getSection());
            }
            if (this.hasBigend()) {
                output.writeBool(4, this.getBigend());
            }
            this.getUnknownFields().writeTo(output);
        }
        
        public int getSerializedSize() {
            int size = this.memoizedSerializedSize;
            if (size != -1) {
                return size;
            }
            size = 0;
            if (this.hasVarName()) {
                size += CodedOutputStream.computeStringSize(1, this.getVarName());
            }
            if (this.hasDataType()) {
                size += CodedOutputStream.computeEnumSize(2, this.getDataType().getNumber());
            }
            if (this.hasSection()) {
                size += CodedOutputStream.computeMessageSize(3, (Message)this.getSection());
            }
            if (this.hasBigend()) {
                size += CodedOutputStream.computeBoolSize(4, this.getBigend());
            }
            size += this.getUnknownFields().getSerializedSize();
            return this.memoizedSerializedSize = size;
        }
        
        public static Data parseFrom(final ByteString data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static Data parseFrom(final ByteString data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static Data parseFrom(final byte[] data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static Data parseFrom(final byte[] data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static Data parseFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input)).buildParsed();
        }
        
        public static Data parseFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static Data parseDelimitedFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input)).buildParsed();
        }
        
        public static Data parseDelimitedFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static Data parseFrom(final CodedInputStream input) throws IOException {
            return newBuilder().mergeFrom(input).buildParsed();
        }
        
        public static Data parseFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return newBuilder().mergeFrom(input, extensionRegistry).buildParsed();
        }
        
        public static Builder newBuilder() {
            return new Builder();
        }
        
        public Builder newBuilderForType() {
            return new Builder();
        }
        
        public static Builder newBuilder(final Data prototype) {
            return new Builder().mergeFrom(prototype);
        }
        
        public Builder toBuilder() {
            return newBuilder(this);
        }
        
        static {
            defaultInstance = new Data();
            NcStreamProto.getDescriptor();
        }
        
        public static final class Builder extends GeneratedMessage.Builder<Builder>
        {
            Data result;
            
            private Builder() {
                this.result = new Data();
            }
            
            protected Data internalGetResult() {
                return this.result;
            }
            
            public Builder clear() {
                this.result = new Data();
                return this;
            }
            
            public Builder clone() {
                return new Builder().mergeFrom(this.result);
            }
            
            public Descriptors.Descriptor getDescriptorForType() {
                return Data.getDescriptor();
            }
            
            public Data getDefaultInstanceForType() {
                return Data.getDefaultInstance();
            }
            
            public Data build() {
                if (this.result != null && !this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result);
                }
                return this.buildPartial();
            }
            
            private Data buildParsed() throws InvalidProtocolBufferException {
                if (!this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result).asInvalidProtocolBufferException();
                }
                return this.buildPartial();
            }
            
            public Data buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                final Data returnMe = this.result;
                this.result = null;
                return returnMe;
            }
            
            public Builder mergeFrom(final Message other) {
                if (other instanceof Data) {
                    return this.mergeFrom((Data)other);
                }
                super.mergeFrom(other);
                return this;
            }
            
            public Builder mergeFrom(final Data other) {
                if (other == Data.getDefaultInstance()) {
                    return this;
                }
                if (other.hasVarName()) {
                    this.setVarName(other.getVarName());
                }
                if (other.hasDataType()) {
                    this.setDataType(other.getDataType());
                }
                if (other.hasSection()) {
                    this.mergeSection(other.getSection());
                }
                if (other.hasBigend()) {
                    this.setBigend(other.getBigend());
                }
                this.mergeUnknownFields(other.getUnknownFields());
                return this;
            }
            
            public Builder mergeFrom(final CodedInputStream input) throws IOException {
                return this.mergeFrom(input, ExtensionRegistry.getEmptyRegistry());
            }
            
            public Builder mergeFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
                final UnknownFieldSet.Builder unknownFields = UnknownFieldSet.newBuilder(this.getUnknownFields());
                while (true) {
                    final int tag = input.readTag();
                    switch (tag) {
                        case 0: {
                            this.setUnknownFields(unknownFields.build());
                            return this;
                        }
                        default: {
                            if (!this.parseUnknownField(input, unknownFields, extensionRegistry, tag)) {
                                this.setUnknownFields(unknownFields.build());
                                return this;
                            }
                            continue;
                        }
                        case 10: {
                            this.setVarName(input.readString());
                            continue;
                        }
                        case 16: {
                            final int rawValue = input.readEnum();
                            final DataType value = DataType.valueOf(rawValue);
                            if (value == null) {
                                unknownFields.mergeVarintField(2, rawValue);
                                continue;
                            }
                            this.setDataType(value);
                            continue;
                        }
                        case 26: {
                            final Section.Builder subBuilder = Section.newBuilder();
                            if (this.hasSection()) {
                                subBuilder.mergeFrom(this.getSection());
                            }
                            input.readMessage((Message.Builder)subBuilder, extensionRegistry);
                            this.setSection(subBuilder.buildPartial());
                            continue;
                        }
                        case 32: {
                            this.setBigend(input.readBool());
                            continue;
                        }
                    }
                }
            }
            
            public boolean hasVarName() {
                return this.result.hasVarName();
            }
            
            public String getVarName() {
                return this.result.getVarName();
            }
            
            public Builder setVarName(final String value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasVarName = true;
                this.result.varName_ = value;
                return this;
            }
            
            public Builder clearVarName() {
                this.result.hasVarName = false;
                this.result.varName_ = "";
                return this;
            }
            
            public boolean hasDataType() {
                return this.result.hasDataType();
            }
            
            public DataType getDataType() {
                return this.result.getDataType();
            }
            
            public Builder setDataType(final DataType value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasDataType = true;
                this.result.dataType_ = value;
                return this;
            }
            
            public Builder clearDataType() {
                this.result.hasDataType = false;
                this.result.dataType_ = DataType.CHAR;
                return this;
            }
            
            public boolean hasSection() {
                return this.result.hasSection();
            }
            
            public Section getSection() {
                return this.result.getSection();
            }
            
            public Builder setSection(final Section value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasSection = true;
                this.result.section_ = value;
                return this;
            }
            
            public Builder setSection(final Section.Builder builderForValue) {
                this.result.hasSection = true;
                this.result.section_ = builderForValue.build();
                return this;
            }
            
            public Builder mergeSection(final Section value) {
                if (this.result.hasSection() && this.result.section_ != Section.getDefaultInstance()) {
                    this.result.section_ = Section.newBuilder(this.result.section_).mergeFrom(value).buildPartial();
                }
                else {
                    this.result.section_ = value;
                }
                this.result.hasSection = true;
                return this;
            }
            
            public Builder clearSection() {
                this.result.hasSection = false;
                this.result.section_ = Section.getDefaultInstance();
                return this;
            }
            
            public boolean hasBigend() {
                return this.result.hasBigend();
            }
            
            public boolean getBigend() {
                return this.result.getBigend();
            }
            
            public Builder setBigend(final boolean value) {
                this.result.hasBigend = true;
                this.result.bigend_ = value;
                return this;
            }
            
            public Builder clearBigend() {
                this.result.hasBigend = false;
                this.result.bigend_ = true;
                return this;
            }
        }
    }
    
    public static final class Range extends GeneratedMessage
    {
        private static final Range defaultInstance;
        public static final int START_FIELD_NUMBER = 1;
        private boolean hasStart;
        private long start_;
        public static final int SIZE_FIELD_NUMBER = 2;
        private boolean hasSize;
        private long size_;
        public static final int STRIDE_FIELD_NUMBER = 3;
        private boolean hasStride;
        private long stride_;
        private int memoizedSerializedSize;
        
        private Range() {
            this.start_ = 0L;
            this.size_ = 0L;
            this.stride_ = 0L;
            this.memoizedSerializedSize = -1;
        }
        
        public static Range getDefaultInstance() {
            return Range.defaultInstance;
        }
        
        public Range getDefaultInstanceForType() {
            return Range.defaultInstance;
        }
        
        public static final Descriptors.Descriptor getDescriptor() {
            return NcStreamProto.internal_static_ncstream_Range_descriptor;
        }
        
        protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return NcStreamProto.internal_static_ncstream_Range_fieldAccessorTable;
        }
        
        public boolean hasStart() {
            return this.hasStart;
        }
        
        public long getStart() {
            return this.start_;
        }
        
        public boolean hasSize() {
            return this.hasSize;
        }
        
        public long getSize() {
            return this.size_;
        }
        
        public boolean hasStride() {
            return this.hasStride;
        }
        
        public long getStride() {
            return this.stride_;
        }
        
        public final boolean isInitialized() {
            return this.hasSize;
        }
        
        public void writeTo(final CodedOutputStream output) throws IOException {
            if (this.hasStart()) {
                output.writeUInt64(1, this.getStart());
            }
            if (this.hasSize()) {
                output.writeUInt64(2, this.getSize());
            }
            if (this.hasStride()) {
                output.writeUInt64(3, this.getStride());
            }
            this.getUnknownFields().writeTo(output);
        }
        
        public int getSerializedSize() {
            int size = this.memoizedSerializedSize;
            if (size != -1) {
                return size;
            }
            size = 0;
            if (this.hasStart()) {
                size += CodedOutputStream.computeUInt64Size(1, this.getStart());
            }
            if (this.hasSize()) {
                size += CodedOutputStream.computeUInt64Size(2, this.getSize());
            }
            if (this.hasStride()) {
                size += CodedOutputStream.computeUInt64Size(3, this.getStride());
            }
            size += this.getUnknownFields().getSerializedSize();
            return this.memoizedSerializedSize = size;
        }
        
        public static Range parseFrom(final ByteString data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static Range parseFrom(final ByteString data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static Range parseFrom(final byte[] data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static Range parseFrom(final byte[] data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static Range parseFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input)).buildParsed();
        }
        
        public static Range parseFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static Range parseDelimitedFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input)).buildParsed();
        }
        
        public static Range parseDelimitedFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static Range parseFrom(final CodedInputStream input) throws IOException {
            return newBuilder().mergeFrom(input).buildParsed();
        }
        
        public static Range parseFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return newBuilder().mergeFrom(input, extensionRegistry).buildParsed();
        }
        
        public static Builder newBuilder() {
            return new Builder();
        }
        
        public Builder newBuilderForType() {
            return new Builder();
        }
        
        public static Builder newBuilder(final Range prototype) {
            return new Builder().mergeFrom(prototype);
        }
        
        public Builder toBuilder() {
            return newBuilder(this);
        }
        
        static {
            defaultInstance = new Range();
            NcStreamProto.getDescriptor();
        }
        
        public static final class Builder extends GeneratedMessage.Builder<Builder>
        {
            Range result;
            
            private Builder() {
                this.result = new Range();
            }
            
            protected Range internalGetResult() {
                return this.result;
            }
            
            public Builder clear() {
                this.result = new Range();
                return this;
            }
            
            public Builder clone() {
                return new Builder().mergeFrom(this.result);
            }
            
            public Descriptors.Descriptor getDescriptorForType() {
                return Range.getDescriptor();
            }
            
            public Range getDefaultInstanceForType() {
                return Range.getDefaultInstance();
            }
            
            public Range build() {
                if (this.result != null && !this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result);
                }
                return this.buildPartial();
            }
            
            private Range buildParsed() throws InvalidProtocolBufferException {
                if (!this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result).asInvalidProtocolBufferException();
                }
                return this.buildPartial();
            }
            
            public Range buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                final Range returnMe = this.result;
                this.result = null;
                return returnMe;
            }
            
            public Builder mergeFrom(final Message other) {
                if (other instanceof Range) {
                    return this.mergeFrom((Range)other);
                }
                super.mergeFrom(other);
                return this;
            }
            
            public Builder mergeFrom(final Range other) {
                if (other == Range.getDefaultInstance()) {
                    return this;
                }
                if (other.hasStart()) {
                    this.setStart(other.getStart());
                }
                if (other.hasSize()) {
                    this.setSize(other.getSize());
                }
                if (other.hasStride()) {
                    this.setStride(other.getStride());
                }
                this.mergeUnknownFields(other.getUnknownFields());
                return this;
            }
            
            public Builder mergeFrom(final CodedInputStream input) throws IOException {
                return this.mergeFrom(input, ExtensionRegistry.getEmptyRegistry());
            }
            
            public Builder mergeFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
                final UnknownFieldSet.Builder unknownFields = UnknownFieldSet.newBuilder(this.getUnknownFields());
                while (true) {
                    final int tag = input.readTag();
                    switch (tag) {
                        case 0: {
                            this.setUnknownFields(unknownFields.build());
                            return this;
                        }
                        default: {
                            if (!this.parseUnknownField(input, unknownFields, extensionRegistry, tag)) {
                                this.setUnknownFields(unknownFields.build());
                                return this;
                            }
                            continue;
                        }
                        case 8: {
                            this.setStart(input.readUInt64());
                            continue;
                        }
                        case 16: {
                            this.setSize(input.readUInt64());
                            continue;
                        }
                        case 24: {
                            this.setStride(input.readUInt64());
                            continue;
                        }
                    }
                }
            }
            
            public boolean hasStart() {
                return this.result.hasStart();
            }
            
            public long getStart() {
                return this.result.getStart();
            }
            
            public Builder setStart(final long value) {
                this.result.hasStart = true;
                this.result.start_ = value;
                return this;
            }
            
            public Builder clearStart() {
                this.result.hasStart = false;
                this.result.start_ = 0L;
                return this;
            }
            
            public boolean hasSize() {
                return this.result.hasSize();
            }
            
            public long getSize() {
                return this.result.getSize();
            }
            
            public Builder setSize(final long value) {
                this.result.hasSize = true;
                this.result.size_ = value;
                return this;
            }
            
            public Builder clearSize() {
                this.result.hasSize = false;
                this.result.size_ = 0L;
                return this;
            }
            
            public boolean hasStride() {
                return this.result.hasStride();
            }
            
            public long getStride() {
                return this.result.getStride();
            }
            
            public Builder setStride(final long value) {
                this.result.hasStride = true;
                this.result.stride_ = value;
                return this;
            }
            
            public Builder clearStride() {
                this.result.hasStride = false;
                this.result.stride_ = 0L;
                return this;
            }
        }
    }
    
    public static final class Section extends GeneratedMessage
    {
        private static final Section defaultInstance;
        public static final int RANGE_FIELD_NUMBER = 1;
        private List<Range> range_;
        private int memoizedSerializedSize;
        
        private Section() {
            this.range_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }
        
        public static Section getDefaultInstance() {
            return Section.defaultInstance;
        }
        
        public Section getDefaultInstanceForType() {
            return Section.defaultInstance;
        }
        
        public static final Descriptors.Descriptor getDescriptor() {
            return NcStreamProto.internal_static_ncstream_Section_descriptor;
        }
        
        protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return NcStreamProto.internal_static_ncstream_Section_fieldAccessorTable;
        }
        
        public List<Range> getRangeList() {
            return this.range_;
        }
        
        public int getRangeCount() {
            return this.range_.size();
        }
        
        public Range getRange(final int index) {
            return this.range_.get(index);
        }
        
        public final boolean isInitialized() {
            for (final Range element : this.getRangeList()) {
                if (!element.isInitialized()) {
                    return false;
                }
            }
            return true;
        }
        
        public void writeTo(final CodedOutputStream output) throws IOException {
            for (final Range element : this.getRangeList()) {
                output.writeMessage(1, (Message)element);
            }
            this.getUnknownFields().writeTo(output);
        }
        
        public int getSerializedSize() {
            int size = this.memoizedSerializedSize;
            if (size != -1) {
                return size;
            }
            size = 0;
            for (final Range element : this.getRangeList()) {
                size += CodedOutputStream.computeMessageSize(1, (Message)element);
            }
            size += this.getUnknownFields().getSerializedSize();
            return this.memoizedSerializedSize = size;
        }
        
        public static Section parseFrom(final ByteString data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static Section parseFrom(final ByteString data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static Section parseFrom(final byte[] data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static Section parseFrom(final byte[] data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static Section parseFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input)).buildParsed();
        }
        
        public static Section parseFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static Section parseDelimitedFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input)).buildParsed();
        }
        
        public static Section parseDelimitedFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static Section parseFrom(final CodedInputStream input) throws IOException {
            return newBuilder().mergeFrom(input).buildParsed();
        }
        
        public static Section parseFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return newBuilder().mergeFrom(input, extensionRegistry).buildParsed();
        }
        
        public static Builder newBuilder() {
            return new Builder();
        }
        
        public Builder newBuilderForType() {
            return new Builder();
        }
        
        public static Builder newBuilder(final Section prototype) {
            return new Builder().mergeFrom(prototype);
        }
        
        public Builder toBuilder() {
            return newBuilder(this);
        }
        
        static {
            defaultInstance = new Section();
            NcStreamProto.getDescriptor();
        }
        
        public static final class Builder extends GeneratedMessage.Builder<Builder>
        {
            Section result;
            
            private Builder() {
                this.result = new Section();
            }
            
            protected Section internalGetResult() {
                return this.result;
            }
            
            public Builder clear() {
                this.result = new Section();
                return this;
            }
            
            public Builder clone() {
                return new Builder().mergeFrom(this.result);
            }
            
            public Descriptors.Descriptor getDescriptorForType() {
                return Section.getDescriptor();
            }
            
            public Section getDefaultInstanceForType() {
                return Section.getDefaultInstance();
            }
            
            public Section build() {
                if (this.result != null && !this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result);
                }
                return this.buildPartial();
            }
            
            private Section buildParsed() throws InvalidProtocolBufferException {
                if (!this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result).asInvalidProtocolBufferException();
                }
                return this.buildPartial();
            }
            
            public Section buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.range_ != Collections.EMPTY_LIST) {
                    this.result.range_ = (List<Range>)Collections.unmodifiableList((List<?>)this.result.range_);
                }
                final Section returnMe = this.result;
                this.result = null;
                return returnMe;
            }
            
            public Builder mergeFrom(final Message other) {
                if (other instanceof Section) {
                    return this.mergeFrom((Section)other);
                }
                super.mergeFrom(other);
                return this;
            }
            
            public Builder mergeFrom(final Section other) {
                if (other == Section.getDefaultInstance()) {
                    return this;
                }
                if (!other.range_.isEmpty()) {
                    if (this.result.range_.isEmpty()) {
                        this.result.range_ = (List<Range>)new ArrayList();
                    }
                    this.result.range_.addAll(other.range_);
                }
                this.mergeUnknownFields(other.getUnknownFields());
                return this;
            }
            
            public Builder mergeFrom(final CodedInputStream input) throws IOException {
                return this.mergeFrom(input, ExtensionRegistry.getEmptyRegistry());
            }
            
            public Builder mergeFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
                final UnknownFieldSet.Builder unknownFields = UnknownFieldSet.newBuilder(this.getUnknownFields());
                while (true) {
                    final int tag = input.readTag();
                    switch (tag) {
                        case 0: {
                            this.setUnknownFields(unknownFields.build());
                            return this;
                        }
                        default: {
                            if (!this.parseUnknownField(input, unknownFields, extensionRegistry, tag)) {
                                this.setUnknownFields(unknownFields.build());
                                return this;
                            }
                            continue;
                        }
                        case 10: {
                            final Range.Builder subBuilder = Range.newBuilder();
                            input.readMessage((Message.Builder)subBuilder, extensionRegistry);
                            this.addRange(subBuilder.buildPartial());
                            continue;
                        }
                    }
                }
            }
            
            public List<Range> getRangeList() {
                return Collections.unmodifiableList((List<? extends Range>)this.result.range_);
            }
            
            public int getRangeCount() {
                return this.result.getRangeCount();
            }
            
            public Range getRange(final int index) {
                return this.result.getRange(index);
            }
            
            public Builder setRange(final int index, final Range value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.range_.set(index, value);
                return this;
            }
            
            public Builder setRange(final int index, final Range.Builder builderForValue) {
                this.result.range_.set(index, builderForValue.build());
                return this;
            }
            
            public Builder addRange(final Range value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                if (this.result.range_.isEmpty()) {
                    this.result.range_ = (List<Range>)new ArrayList();
                }
                this.result.range_.add(value);
                return this;
            }
            
            public Builder addRange(final Range.Builder builderForValue) {
                if (this.result.range_.isEmpty()) {
                    this.result.range_ = (List<Range>)new ArrayList();
                }
                this.result.range_.add(builderForValue.build());
                return this;
            }
            
            public Builder addAllRange(final Iterable<? extends Range> values) {
                if (this.result.range_.isEmpty()) {
                    this.result.range_ = (List<Range>)new ArrayList();
                }
                super.addAll((Iterable)values, (Collection)this.result.range_);
                return this;
            }
            
            public Builder clearRange() {
                this.result.range_ = (List<Range>)Collections.emptyList();
                return this;
            }
        }
    }
    
    public static final class Error extends GeneratedMessage
    {
        private static final Error defaultInstance;
        public static final int MESSAGE_FIELD_NUMBER = 1;
        private boolean hasMessage;
        private String message_;
        private int memoizedSerializedSize;
        
        private Error() {
            this.message_ = "";
            this.memoizedSerializedSize = -1;
        }
        
        public static Error getDefaultInstance() {
            return Error.defaultInstance;
        }
        
        public Error getDefaultInstanceForType() {
            return Error.defaultInstance;
        }
        
        public static final Descriptors.Descriptor getDescriptor() {
            return NcStreamProto.internal_static_ncstream_Error_descriptor;
        }
        
        protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return NcStreamProto.internal_static_ncstream_Error_fieldAccessorTable;
        }
        
        public boolean hasMessage() {
            return this.hasMessage;
        }
        
        public String getMessage() {
            return this.message_;
        }
        
        public final boolean isInitialized() {
            return this.hasMessage;
        }
        
        public void writeTo(final CodedOutputStream output) throws IOException {
            if (this.hasMessage()) {
                output.writeString(1, this.getMessage());
            }
            this.getUnknownFields().writeTo(output);
        }
        
        public int getSerializedSize() {
            int size = this.memoizedSerializedSize;
            if (size != -1) {
                return size;
            }
            size = 0;
            if (this.hasMessage()) {
                size += CodedOutputStream.computeStringSize(1, this.getMessage());
            }
            size += this.getUnknownFields().getSerializedSize();
            return this.memoizedSerializedSize = size;
        }
        
        public static Error parseFrom(final ByteString data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static Error parseFrom(final ByteString data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static Error parseFrom(final byte[] data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static Error parseFrom(final byte[] data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static Error parseFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input)).buildParsed();
        }
        
        public static Error parseFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static Error parseDelimitedFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input)).buildParsed();
        }
        
        public static Error parseDelimitedFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static Error parseFrom(final CodedInputStream input) throws IOException {
            return newBuilder().mergeFrom(input).buildParsed();
        }
        
        public static Error parseFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return newBuilder().mergeFrom(input, extensionRegistry).buildParsed();
        }
        
        public static Builder newBuilder() {
            return new Builder();
        }
        
        public Builder newBuilderForType() {
            return new Builder();
        }
        
        public static Builder newBuilder(final Error prototype) {
            return new Builder().mergeFrom(prototype);
        }
        
        public Builder toBuilder() {
            return newBuilder(this);
        }
        
        static {
            defaultInstance = new Error();
            NcStreamProto.getDescriptor();
        }
        
        public static final class Builder extends GeneratedMessage.Builder<Builder>
        {
            Error result;
            
            private Builder() {
                this.result = new Error();
            }
            
            protected Error internalGetResult() {
                return this.result;
            }
            
            public Builder clear() {
                this.result = new Error();
                return this;
            }
            
            public Builder clone() {
                return new Builder().mergeFrom(this.result);
            }
            
            public Descriptors.Descriptor getDescriptorForType() {
                return Error.getDescriptor();
            }
            
            public Error getDefaultInstanceForType() {
                return Error.getDefaultInstance();
            }
            
            public Error build() {
                if (this.result != null && !this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result);
                }
                return this.buildPartial();
            }
            
            private Error buildParsed() throws InvalidProtocolBufferException {
                if (!this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result).asInvalidProtocolBufferException();
                }
                return this.buildPartial();
            }
            
            public Error buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                final Error returnMe = this.result;
                this.result = null;
                return returnMe;
            }
            
            public Builder mergeFrom(final Message other) {
                if (other instanceof Error) {
                    return this.mergeFrom((Error)other);
                }
                super.mergeFrom(other);
                return this;
            }
            
            public Builder mergeFrom(final Error other) {
                if (other == Error.getDefaultInstance()) {
                    return this;
                }
                if (other.hasMessage()) {
                    this.setMessage(other.getMessage());
                }
                this.mergeUnknownFields(other.getUnknownFields());
                return this;
            }
            
            public Builder mergeFrom(final CodedInputStream input) throws IOException {
                return this.mergeFrom(input, ExtensionRegistry.getEmptyRegistry());
            }
            
            public Builder mergeFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
                final UnknownFieldSet.Builder unknownFields = UnknownFieldSet.newBuilder(this.getUnknownFields());
                while (true) {
                    final int tag = input.readTag();
                    switch (tag) {
                        case 0: {
                            this.setUnknownFields(unknownFields.build());
                            return this;
                        }
                        default: {
                            if (!this.parseUnknownField(input, unknownFields, extensionRegistry, tag)) {
                                this.setUnknownFields(unknownFields.build());
                                return this;
                            }
                            continue;
                        }
                        case 10: {
                            this.setMessage(input.readString());
                            continue;
                        }
                    }
                }
            }
            
            public boolean hasMessage() {
                return this.result.hasMessage();
            }
            
            public String getMessage() {
                return this.result.getMessage();
            }
            
            public Builder setMessage(final String value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasMessage = true;
                this.result.message_ = value;
                return this;
            }
            
            public Builder clearMessage() {
                this.result.hasMessage = false;
                this.result.message_ = "";
                return this;
            }
        }
    }
}
