// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.stream;

import java.util.ArrayList;
import java.util.List;
import ucar.ma2.InvalidRangeException;
import java.nio.ByteBuffer;
import ucar.ma2.Array;
import ucar.ma2.Section;
import ucar.nc2.Variable;
import ucar.nc2.util.CancelTask;
import ucar.nc2.NetcdfFile;
import java.io.IOException;
import ucar.unidata.io.RandomAccessFile;
import ucar.nc2.iosp.AbstractIOServiceProvider;

public class NcStreamIosp extends AbstractIOServiceProvider
{
    private static final boolean debug = false;
    
    public boolean isValidFile(final RandomAccessFile raf) throws IOException {
        raf.seek(0L);
        if (!this.readAndTest(raf, NcStream.MAGIC_START)) {
            return false;
        }
        final byte[] b = new byte[4];
        raf.read(b);
        return this.test(b, NcStream.MAGIC_HEADER) || this.test(b, NcStream.MAGIC_DATA);
    }
    
    public String getFileTypeId() {
        return "ncstream";
    }
    
    public String getFileTypeDescription() {
        return "netCDF streaming protocol";
    }
    
    @Override
    public void open(final RandomAccessFile raf, final NetcdfFile ncfile, final CancelTask cancelTask) throws IOException {
        (this.raf = raf).seek(0L);
        assert this.readAndTest(raf, NcStream.MAGIC_START);
        assert this.readAndTest(raf, NcStream.MAGIC_HEADER);
        final int msize = this.readVInt(raf);
        final byte[] m = new byte[msize];
        raf.read(m);
        final NcStreamProto.Header proto = NcStreamProto.Header.parseFrom(m);
        final NcStreamProto.Group root = proto.getRoot();
        NcStream.readGroup(root, ncfile, ncfile.getRootGroup());
        ncfile.finish();
        while (!raf.isAtEndOfFile()) {
            assert this.readAndTest(raf, NcStream.MAGIC_DATA);
            final int psize = this.readVInt(raf);
            final byte[] dp = new byte[psize];
            raf.read(dp);
            final NcStreamProto.Data dproto = NcStreamProto.Data.parseFrom(dp);
            final int dsize = this.readVInt(raf);
            final DataSection dataSection = new DataSection();
            dataSection.size = dsize;
            dataSection.filePos = raf.getFilePointer();
            dataSection.section = NcStream.decodeSection(dproto.getSection());
            final Variable v = ncfile.getRootGroup().findVariable(dproto.getVarName());
            v.setSPobject(dataSection);
            raf.skipBytes(dsize);
        }
    }
    
    public Array readData(final Variable v, final Section section) throws IOException, InvalidRangeException {
        final DataSection dataSection = (DataSection)v.getSPobject();
        this.raf.seek(dataSection.filePos);
        final byte[] data = new byte[dataSection.size];
        this.raf.read(data);
        final Array dataArray = Array.factory(v.getDataType(), v.getShape(), ByteBuffer.wrap(data));
        return dataArray.section(section.getRanges());
    }
    
    private int readVInt(final RandomAccessFile raf) throws IOException {
        byte b = (byte)raf.read();
        int i = b & 0x7F;
        for (int shift = 7; (b & 0x80) != 0x0; b = (byte)raf.read(), i |= (b & 0x7F) << shift, shift += 7) {}
        return i;
    }
    
    private boolean readAndTest(final RandomAccessFile raf, final byte[] test) throws IOException {
        final byte[] b = new byte[test.length];
        raf.read(b);
        return this.test(b, test);
    }
    
    private boolean test(final byte[] bread, final byte[] test) throws IOException {
        if (bread.length != test.length) {
            return false;
        }
        for (int i = 0; i < bread.length; ++i) {
            if (bread[i] != test[i]) {
                return false;
            }
        }
        return true;
    }
    
    public List<NcsMess> open(final RandomAccessFile raf, final NetcdfFile ncfile) throws IOException {
        (this.raf = raf).seek(0L);
        assert this.readAndTest(raf, NcStream.MAGIC_START);
        assert this.readAndTest(raf, NcStream.MAGIC_HEADER);
        final int msize = this.readVInt(raf);
        final byte[] m = new byte[msize];
        raf.read(m);
        final NcStreamProto.Header proto = NcStreamProto.Header.parseFrom(m);
        final NcStreamProto.Group root = proto.getRoot();
        NcStream.readGroup(root, ncfile, ncfile.getRootGroup());
        ncfile.finish();
        final List<NcsMess> ncm = new ArrayList<NcsMess>();
        ncm.add(new NcsMess(msize, proto));
        while (!raf.isAtEndOfFile()) {
            assert this.readAndTest(raf, NcStream.MAGIC_DATA);
            final int psize = this.readVInt(raf);
            final byte[] dp = new byte[psize];
            raf.read(dp);
            final NcStreamProto.Data dproto = NcStreamProto.Data.parseFrom(dp);
            ncm.add(new NcsMess(psize, dproto));
            final int dsize = this.readVInt(raf);
            final DataSection dataSection = new DataSection();
            dataSection.size = dsize;
            dataSection.filePos = raf.getFilePointer();
            dataSection.section = NcStream.decodeSection(dproto.getSection());
            ncm.add(new NcsMess(dsize, dataSection));
            final Variable v = ncfile.getRootGroup().findVariable(dproto.getVarName());
            v.setSPobject(dataSection);
            raf.skipBytes(dsize);
        }
        return ncm;
    }
    
    private class DataSection
    {
        int size;
        long filePos;
        Section section;
        
        @Override
        public String toString() {
            return "DataSection{size=" + this.size + ", filePos=" + this.filePos + ", section=" + this.section + '}';
        }
    }
    
    public class NcsMess
    {
        public int len;
        public Object what;
        
        public NcsMess(final int len, final Object what) {
            this.len = len;
            this.what = what;
        }
    }
}
