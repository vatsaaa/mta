// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.stream;

import ucar.ma2.Index;
import ucar.nc2.FileWriter;
import java.util.Iterator;
import java.nio.ByteBuffer;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.DataType;
import ucar.ma2.Section;
import ucar.nc2.Variable;
import java.nio.channels.WritableByteChannel;
import java.io.IOException;
import ucar.nc2.NetcdfFile;

public class NcStreamWriter
{
    private static long maxChunk;
    private static int sizeToCache;
    private NetcdfFile ncfile;
    private NcStreamProto.Header header;
    private boolean show;
    
    public NcStreamWriter(final NetcdfFile ncfile, final String location) throws IOException {
        this.show = false;
        this.ncfile = ncfile;
        final NcStreamProto.Group.Builder rootBuilder = NcStream.encodeGroup(ncfile.getRootGroup(), NcStreamWriter.sizeToCache);
        final NcStreamProto.Header.Builder headerBuilder = NcStreamProto.Header.newBuilder();
        headerBuilder.setLocation((location == null) ? ncfile.getLocation() : location);
        if (ncfile.getTitle() != null) {
            headerBuilder.setTitle(ncfile.getTitle());
        }
        if (ncfile.getId() != null) {
            headerBuilder.setId(ncfile.getId());
        }
        headerBuilder.setRoot(rootBuilder);
        this.header = headerBuilder.build();
    }
    
    public long sendHeader(final WritableByteChannel wbc) throws IOException {
        long size = 0L;
        size += this.writeBytes(wbc, NcStream.MAGIC_HEADER);
        final byte[] b = this.header.toByteArray();
        size += NcStream.writeVInt(wbc, b.length);
        if (this.show) {
            System.out.println("Write Header len=" + b.length);
        }
        size += this.writeBytes(wbc, b);
        if (this.show) {
            System.out.println(" header size=" + size);
        }
        return size;
    }
    
    public long sendData(final Variable v, final Section section, final WritableByteChannel wbc) throws IOException, InvalidRangeException {
        if (this.show) {
            System.out.printf(" %s section=%s%n", v.getName(), section);
        }
        long size = 0L;
        size += this.writeBytes(wbc, NcStream.MAGIC_DATA);
        final NcStreamProto.Data dataProto = NcStream.encodeDataProto(v, section);
        final byte[] datab = dataProto.toByteArray();
        size += NcStream.writeVInt(wbc, datab.length);
        size += this.writeBytes(wbc, datab);
        long len = section.computeSize();
        if (v.getDataType() != DataType.STRING && v.getDataType() != DataType.OPAQUE) {
            len *= v.getElementSize();
        }
        size += NcStream.writeVInt(wbc, (int)len);
        if (this.show) {
            System.out.printf("  %s proto=%d data=%d%n", v.getName(), datab.length, len);
        }
        size += v.readToByteChannel(section, wbc);
        return size;
    }
    
    private int writeBytes(final WritableByteChannel wbc, final byte[] b) throws IOException {
        return wbc.write(ByteBuffer.wrap(b));
    }
    
    public long streamAll(final WritableByteChannel wbc) throws IOException, InvalidRangeException {
        long size = this.writeBytes(wbc, NcStream.MAGIC_START);
        size += this.sendHeader(wbc);
        for (final Variable v : this.ncfile.getVariables()) {
            final long vsize = v.getSize() * v.getElementSize();
            if (vsize <= NcStreamWriter.maxChunk) {
                size += this.sendData(v, v.getShapeAsSection(), wbc);
            }
            else {
                size += this.copyChunks(wbc, v, NcStreamWriter.maxChunk);
            }
        }
        if (this.show) {
            System.out.printf("total size= %d%n", size);
        }
        return size;
    }
    
    private long copyChunks(final WritableByteChannel wbc, final Variable oldVar, final long maxChunkSize) throws IOException {
        final long maxChunkElems = maxChunkSize / oldVar.getElementSize();
        final FileWriter.ChunkingIndex index = new FileWriter.ChunkingIndex(oldVar.getShape());
        long size = 0L;
        while (index.currentElement() < index.getSize()) {
            try {
                final int[] chunkOrigin = index.getCurrentCounter();
                final int[] chunkShape = index.computeChunkShape(maxChunkElems);
                size += this.sendData(oldVar, new Section(chunkOrigin, chunkShape), wbc);
                index.setCurrentCounter(index.currentElement() + (int)Index.computeSize(chunkShape));
                continue;
            }
            catch (InvalidRangeException e) {
                e.printStackTrace();
                throw new IOException(e.getMessage());
            }
            break;
        }
        return size;
    }
    
    static {
        NcStreamWriter.maxChunk = 1000000L;
        NcStreamWriter.sizeToCache = 100;
    }
}
