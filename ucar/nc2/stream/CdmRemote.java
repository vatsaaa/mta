// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.stream;

import org.slf4j.LoggerFactory;
import java.util.Iterator;
import java.nio.channels.WritableByteChannel;
import java.io.OutputStream;
import ucar.nc2.util.IO;
import java.io.FileOutputStream;
import java.io.File;
import ucar.ma2.InvalidRangeException;
import org.apache.commons.httpclient.Header;
import java.net.URLEncoder;
import ucar.ma2.Array;
import ucar.ma2.Section;
import ucar.nc2.Variable;
import java.io.InputStream;
import org.apache.commons.httpclient.HttpMethod;
import java.io.IOException;
import java.io.FileNotFoundException;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.HttpClient;
import org.slf4j.Logger;
import ucar.nc2.NetcdfFile;

public class CdmRemote extends NetcdfFile
{
    public static final String SCHEME = "cdmremote:";
    private static Logger logger;
    private static HttpClient httpClient;
    private static boolean showRequest;
    private final String remoteURI;
    
    public static String canonicalURL(final String urlName) {
        if (urlName.startsWith("http:")) {
            return "cdmremote:" + urlName.substring(5);
        }
        return urlName;
    }
    
    public static void setHttpClient(final HttpClient client) {
        CdmRemote.httpClient = client;
    }
    
    private static synchronized void initHttpClient() {
        if (CdmRemote.httpClient != null) {
            return;
        }
        final MultiThreadedHttpConnectionManager connectionManager = new MultiThreadedHttpConnectionManager();
        CdmRemote.httpClient = new HttpClient((HttpConnectionManager)connectionManager);
    }
    
    public CdmRemote(final String _remoteURI) throws IOException {
        String temp = _remoteURI;
        try {
            if (temp.startsWith("cdmremote:")) {
                temp = temp.substring("cdmremote:".length());
            }
            if (!temp.startsWith("http:")) {
                temp = "http:" + temp;
            }
        }
        catch (Exception ex) {}
        this.remoteURI = temp;
        initHttpClient();
        HttpMethod method = null;
        try {
            final String url = this.remoteURI + "?req=header";
            method = (HttpMethod)new GetMethod(url);
            method.setFollowRedirects(true);
            if (CdmRemote.showRequest) {
                System.out.printf("CdmRemote request %s %n", url);
            }
            final int statusCode = CdmRemote.httpClient.executeMethod(method);
            if (statusCode == 404) {
                throw new FileNotFoundException(method.getURI() + " " + method.getStatusLine());
            }
            if (statusCode >= 300) {
                throw new IOException(method.getURI() + " " + method.getStatusLine());
            }
            final InputStream is = method.getResponseBodyAsStream();
            final NcStreamReader reader = new NcStreamReader();
            reader.readStream(is, this);
            this.location = "cdmremote:" + this.remoteURI;
        }
        finally {
            if (method != null) {
                method.releaseConnection();
            }
        }
    }
    
    @Override
    protected Array readData(final Variable v, final Section section) throws IOException, InvalidRangeException {
        if (this.unlocked) {
            throw new IllegalStateException("File is unlocked - cannot use");
        }
        final StringBuilder sbuff = new StringBuilder(this.remoteURI);
        sbuff.append("?var=");
        sbuff.append(URLEncoder.encode(v.getShortName(), "UTF-8"));
        sbuff.append("(");
        sbuff.append(section.toString());
        sbuff.append(")");
        if (CdmRemote.showRequest) {
            System.out.println(" CdmRemote data request for variable: " + v.getName() + " section= " + section + " url=" + (Object)sbuff);
        }
        HttpMethod method = null;
        try {
            method = (HttpMethod)new GetMethod(sbuff.toString());
            method.setFollowRedirects(true);
            final int statusCode = CdmRemote.httpClient.executeMethod(method);
            if (statusCode == 404) {
                throw new FileNotFoundException(method.getPath() + " " + method.getStatusLine());
            }
            if (statusCode >= 300) {
                throw new IOException(method.getPath() + " " + method.getStatusLine());
            }
            final int wantSize = (int)(v.getElementSize() * section.computeSize());
            final Header h = method.getResponseHeader("Content-Length");
            if (h != null) {
                final String s = h.getValue();
                final int readLen = Integer.parseInt(s);
                if (readLen != wantSize) {
                    throw new IOException("content-length= " + readLen + " not equal expected Size= " + wantSize);
                }
            }
            final InputStream is = method.getResponseBodyAsStream();
            final NcStreamReader reader = new NcStreamReader();
            final NcStreamReader.DataResult result = reader.readData(is, this);
            assert v.getName().equals(result.varName);
            result.data.setUnsigned(v.isUnsigned());
            return result.data;
        }
        finally {
            if (method != null) {
                method.releaseConnection();
            }
        }
    }
    
    public static HttpMethod sendQuery(final String remoteURI, final String query) throws IOException {
        initHttpClient();
        final StringBuilder sbuff = new StringBuilder(remoteURI);
        sbuff.append("?");
        sbuff.append(query);
        if (CdmRemote.showRequest) {
            System.out.println(" CdmRemote sendQuery=" + (Object)sbuff);
        }
        final HttpMethod method = (HttpMethod)new GetMethod(sbuff.toString());
        method.setFollowRedirects(true);
        final int statusCode = CdmRemote.httpClient.executeMethod(method);
        if (statusCode == 404) {
            throw new FileNotFoundException(method.getPath() + " " + method.getStatusLine());
        }
        if (statusCode >= 300) {
            throw new IOException(method.getPath() + " " + method.getStatusLine());
        }
        return method;
    }
    
    @Override
    public String getFileTypeId() {
        return "ncstreamRemote";
    }
    
    @Override
    public String getFileTypeDescription() {
        return "ncstreamRemote";
    }
    
    public void writeToFile(final String filename) throws IOException {
        final File file = new File(filename);
        final FileOutputStream fos = new FileOutputStream(file);
        final WritableByteChannel wbc = fos.getChannel();
        long size = 4L;
        fos.write(NcStream.MAGIC_START);
        HttpMethod method = null;
        try {
            final String url = this.remoteURI + "?req=header";
            method = (HttpMethod)new GetMethod(url);
            method.setFollowRedirects(true);
            if (CdmRemote.showRequest) {
                System.out.printf("CdmRemote request %s %n", url);
            }
            final int statusCode = CdmRemote.httpClient.executeMethod(method);
            if (statusCode == 404) {
                throw new FileNotFoundException(method.getURI() + " " + method.getStatusLine());
            }
            if (statusCode >= 300) {
                throw new IOException(method.getURI() + " " + method.getStatusLine());
            }
            final InputStream is = method.getResponseBodyAsStream();
            size += IO.copyB(is, fos, IO.default_socket_buffersize);
        }
        finally {
            if (method != null) {
                method.releaseConnection();
            }
        }
        for (final Variable v : this.getVariables()) {
            final StringBuilder sbuff = new StringBuilder(this.remoteURI);
            sbuff.append("?var=");
            sbuff.append(URLEncoder.encode(v.getShortName(), "UTF-8"));
            if (CdmRemote.showRequest) {
                System.out.println(" CdmRemote data request for variable: " + v.getName() + " url=" + (Object)sbuff);
            }
            try {
                method = (HttpMethod)new GetMethod(sbuff.toString());
                method.setFollowRedirects(true);
                final int statusCode2 = CdmRemote.httpClient.executeMethod(method);
                if (statusCode2 == 404) {
                    throw new FileNotFoundException(method.getPath() + " " + method.getStatusLine());
                }
                if (statusCode2 >= 300) {
                    throw new IOException(method.getPath() + " " + method.getStatusLine());
                }
                final int wantSize = (int)v.getSize();
                final Header h = method.getResponseHeader("Content-Length");
                if (h != null) {
                    final String s = h.getValue();
                    final int readLen = Integer.parseInt(s);
                    if (readLen != wantSize) {
                        throw new IOException("content-length= " + readLen + " not equal expected Size= " + wantSize);
                    }
                }
                final InputStream is2 = method.getResponseBodyAsStream();
                size += IO.copyB(is2, fos, IO.default_socket_buffersize);
            }
            finally {
                if (method != null) {
                    method.releaseConnection();
                }
            }
        }
        fos.flush();
        fos.close();
    }
    
    static {
        CdmRemote.logger = LoggerFactory.getLogger(CdmRemote.class);
        CdmRemote.showRequest = true;
    }
}
