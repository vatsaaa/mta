// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.stream;

import com.google.protobuf.InvalidProtocolBufferException;
import ucar.ma2.StructureMembers;
import ucar.ma2.IndexIterator;
import ucar.ma2.Section;
import ucar.ma2.ArrayStructureBB;
import ucar.nc2.Structure;
import java.nio.ByteBuffer;
import ucar.ma2.Array;
import ucar.ma2.DataType;
import java.io.IOException;
import ucar.nc2.NetcdfFile;
import java.io.InputStream;

public class NcStreamReader
{
    private static final boolean debug = false;
    
    public NetcdfFile readStream(final InputStream is, NetcdfFile ncfile) throws IOException {
        final byte[] b = new byte[4];
        NcStream.readFully(is, b);
        if (this.test(b, NcStream.MAGIC_START)) {
            assert this.readAndTest(is, NcStream.MAGIC_HEADER);
        }
        else {
            assert this.test(b, NcStream.MAGIC_HEADER);
        }
        final int msize = NcStream.readVInt(is);
        final byte[] m = new byte[msize];
        NcStream.readFully(is, m);
        final NcStreamProto.Header proto = NcStreamProto.Header.parseFrom(m);
        ncfile = this.proto2nc(proto, ncfile);
        while (is.available() > 0) {
            this.readData(is, ncfile);
        }
        return ncfile;
    }
    
    public DataResult readData(final InputStream is, final NetcdfFile ncfile) throws IOException {
        assert this.readAndTest(is, NcStream.MAGIC_DATA);
        final int psize = NcStream.readVInt(is);
        final byte[] dp = new byte[psize];
        NcStream.readFully(is, dp);
        final NcStreamProto.Data dproto = NcStreamProto.Data.parseFrom(dp);
        final int dsize = NcStream.readVInt(is);
        final DataType dataType = NcStream.decodeDataType(dproto.getDataType());
        final Section section = NcStream.decodeSection(dproto.getSection());
        if (dataType == DataType.STRING) {
            final Array data = Array.factory(dataType, section.getShape());
            final IndexIterator ii = data.getIndexIterator();
            while (ii.hasNext()) {
                final int slen = NcStream.readVInt(is);
                final byte[] sb = new byte[slen];
                NcStream.readFully(is, sb);
                ii.setObjectNext(new String(sb, "UTF-8"));
            }
            return new DataResult(dproto.getVarName(), section, data);
        }
        if (dataType == DataType.OPAQUE) {
            final Array data = Array.factory(dataType, section.getShape());
            final IndexIterator ii = data.getIndexIterator();
            while (ii.hasNext()) {
                final int slen = NcStream.readVInt(is);
                final byte[] sb = new byte[slen];
                NcStream.readFully(is, sb);
                ii.setObjectNext(ByteBuffer.wrap(sb));
            }
            return new DataResult(dproto.getVarName(), section, data);
        }
        final byte[] datab = new byte[dsize];
        NcStream.readFully(is, datab);
        final ByteBuffer dataBB = ByteBuffer.wrap(datab);
        if (dataType == DataType.STRUCTURE) {
            final Structure s = (Structure)ncfile.findVariable(dproto.getVarName());
            final StructureMembers members = s.makeStructureMembers();
            ArrayStructureBB.setOffsets(members);
            final ArrayStructureBB data2 = new ArrayStructureBB(members, section.getShape(), ByteBuffer.wrap(datab), 0);
            return new DataResult(dproto.getVarName(), section, data2);
        }
        final Array data3 = Array.factory(dataType, section.getShape(), dataBB);
        return new DataResult(dproto.getVarName(), section, data3);
    }
    
    private boolean readAndTest(final InputStream is, final byte[] test) throws IOException {
        final byte[] b = new byte[test.length];
        NcStream.readFully(is, b);
        if (b.length != test.length) {
            return false;
        }
        for (int i = 0; i < b.length; ++i) {
            if (b[i] != test[i]) {
                return false;
            }
        }
        return true;
    }
    
    private boolean test(final byte[] b, final byte[] test) throws IOException {
        if (b.length != test.length) {
            return false;
        }
        for (int i = 0; i < b.length; ++i) {
            if (b[i] != test[i]) {
                return false;
            }
        }
        return true;
    }
    
    public NetcdfFile proto2nc(final NcStreamProto.Header proto, NetcdfFile ncfile) throws InvalidProtocolBufferException {
        if (ncfile == null) {
            ncfile = new NetcdfFileStream();
        }
        ncfile.setLocation(proto.getLocation());
        if (proto.hasId()) {
            ncfile.setId(proto.getId());
        }
        if (proto.hasTitle()) {
            ncfile.setTitle(proto.getTitle());
        }
        final NcStreamProto.Group root = proto.getRoot();
        NcStream.readGroup(root, ncfile, ncfile.getRootGroup());
        ncfile.finish();
        return ncfile;
    }
    
    class DataResult
    {
        String varName;
        Section section;
        Array data;
        
        DataResult(final String varName, final Section section, final Array data) {
            this.varName = varName;
            this.section = section;
            this.data = data;
        }
    }
    
    private class NetcdfFileStream extends NetcdfFile
    {
    }
}
