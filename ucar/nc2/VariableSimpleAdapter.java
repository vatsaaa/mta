// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2;

import ucar.ma2.DataType;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import ucar.ma2.StructureMembers;

public class VariableSimpleAdapter implements VariableSimpleIF
{
    private StructureMembers.Member m;
    
    public static List<VariableSimpleIF> convert(final StructureMembers sm) {
        final List<StructureMembers.Member> mlist = sm.getMembers();
        final List<VariableSimpleIF> result = new ArrayList<VariableSimpleIF>(mlist.size());
        for (final StructureMembers.Member m : mlist) {
            result.add(new VariableSimpleAdapter(m));
        }
        return result;
    }
    
    public VariableSimpleAdapter(final StructureMembers.Member m) {
        this.m = m;
    }
    
    public String getName() {
        return this.m.getName();
    }
    
    public String getShortName() {
        return this.m.getName();
    }
    
    public DataType getDataType() {
        return this.m.getDataType();
    }
    
    public String getDescription() {
        return this.m.getDescription();
    }
    
    public String getUnitsString() {
        return this.m.getUnitsString();
    }
    
    public int getRank() {
        return this.m.getShape().length;
    }
    
    public int[] getShape() {
        return this.m.getShape();
    }
    
    public List<Dimension> getDimensions() {
        final List<Dimension> result = new ArrayList<Dimension>(this.getRank());
        for (final int aShape : this.getShape()) {
            result.add(new Dimension(null, aShape, false));
        }
        return result;
    }
    
    public List<Attribute> getAttributes() {
        return new ArrayList<Attribute>(1);
    }
    
    public Attribute findAttributeIgnoreCase(final String attName) {
        return null;
    }
    
    @Override
    public String toString() {
        return this.m.getName();
    }
    
    public int compareTo(final VariableSimpleIF o) {
        return this.getName().compareTo(o.getName());
    }
}
