// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2;

import org.slf4j.LoggerFactory;
import ucar.nc2.util.CancelTask;
import java.util.Arrays;
import ucar.ma2.ArrayObject;
import ucar.ma2.ArrayChar;
import ucar.ma2.Index;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.IndexIterator;
import ucar.ma2.Array;
import java.util.Map;
import java.util.Iterator;
import ucar.ma2.DataType;
import java.util.ArrayList;
import ucar.nc2.iosp.netcdf3.N3iosp;
import java.io.IOException;
import ucar.nc2.util.DebugFlags;
import java.util.List;
import java.util.HashMap;
import org.slf4j.Logger;

public class FileWriter
{
    private static Logger log;
    private static boolean debug;
    private static boolean debugWrite;
    private static boolean debugExtend;
    private static long maxSize;
    private NetcdfFileWriteable ncfile;
    private HashMap<String, Dimension> dimHash;
    private List<Variable> varList;
    private Structure recordVar;
    
    public static void setDebugFlags(final DebugFlags debugFlags) {
        FileWriter.debug = debugFlags.isSet("ncfileWriter/debug");
        FileWriter.debugExtend = debugFlags.isSet("ncfileWriter/debugExtend");
    }
    
    public static NetcdfFile writeToFile(final NetcdfFile fileIn, final String fileOutName) throws IOException {
        return writeToFile(fileIn, fileOutName, false, false, null);
    }
    
    public static NetcdfFile writeToFile(final NetcdfFile fileIn, final String fileOutName, final boolean fill) throws IOException {
        return writeToFile(fileIn, fileOutName, fill, false, null);
    }
    
    public static NetcdfFile writeToFile(final NetcdfFile fileIn, final String fileOutName, final boolean fill, final boolean isLargeFile) throws IOException {
        return writeToFile(fileIn, fileOutName, fill, isLargeFile, null);
    }
    
    public static NetcdfFile writeToFile(final NetcdfFile fileIn, final String fileOutName, final boolean fill, final boolean isLargeFile, final List<FileWriterProgressListener> progressListeners) throws IOException {
        final NetcdfFileWriteable ncfile = NetcdfFileWriteable.createNew(fileOutName, fill);
        if (FileWriter.debug) {
            System.out.println("FileWriter write " + fileIn.getLocation() + " to " + fileOutName);
            System.out.println("File In = " + fileIn);
        }
        ncfile.setLargeFile(isLargeFile);
        final List<Attribute> glist = fileIn.getGlobalAttributes();
        for (final Attribute att : glist) {
            final String useName = N3iosp.makeValidNetcdfObjectName(att.getName());
            Attribute useAtt;
            if (att.isArray()) {
                useAtt = ncfile.addGlobalAttribute(useName, att.getValues());
            }
            else if (att.isString()) {
                useAtt = ncfile.addGlobalAttribute(useName, att.getStringValue());
            }
            else {
                useAtt = ncfile.addGlobalAttribute(useName, att.getNumericValue());
            }
            if (FileWriter.debug) {
                System.out.println("add gatt= " + useAtt);
            }
        }
        final Map<String, Dimension> dimHash = new HashMap<String, Dimension>();
        for (final Dimension oldD : fileIn.getDimensions()) {
            final String useName2 = N3iosp.makeValidNetcdfObjectName(oldD.getName());
            final Dimension newD = ncfile.addDimension(useName2, oldD.isUnlimited() ? 0 : oldD.getLength(), oldD.isShared(), oldD.isUnlimited(), oldD.isVariableLength());
            dimHash.put(newD.getName(), newD);
            if (FileWriter.debug) {
                System.out.println("add dim= " + newD);
            }
        }
        final List<Variable> varlist = fileIn.getVariables();
        for (final Variable oldVar : varlist) {
            final List<Dimension> dims = new ArrayList<Dimension>();
            final List<Dimension> dimvList = oldVar.getDimensions();
            for (final Dimension oldD2 : dimvList) {
                final String useName3 = N3iosp.makeValidNetcdfObjectName(oldD2.getName());
                final Dimension dim = dimHash.get(useName3);
                if (dim == null) {
                    throw new IllegalStateException("Unknown dimension= " + oldD2.getName());
                }
                dims.add(dim);
            }
            DataType newType = oldVar.getDataType();
            if (oldVar.getDataType() == DataType.STRING) {
                final Array data = oldVar.read();
                final IndexIterator ii = data.getIndexIterator();
                int max_len = 0;
                while (ii.hasNext()) {
                    final String s = (String)ii.getObjectNext();
                    max_len = Math.max(max_len, s.length());
                }
                final String useName4 = N3iosp.makeValidNetcdfObjectName(oldVar.getName() + "_strlen");
                final Dimension newD2 = ncfile.addDimension(useName4, max_len);
                dims.add(newD2);
                newType = DataType.CHAR;
            }
            final String varName = N3iosp.makeValidNetcdfObjectName(oldVar.getShortName());
            final Variable v = ncfile.addVariable(varName, newType, dims);
            if (FileWriter.debug) {
                System.out.println("add var= " + v);
            }
            final List<Attribute> attList = oldVar.getAttributes();
            for (final Attribute att2 : attList) {
                final String useName5 = N3iosp.makeValidNetcdfObjectName(att2.getName());
                if (att2.isArray()) {
                    ncfile.addVariableAttribute(varName, useName5, att2.getValues());
                }
                else if (att2.isString()) {
                    ncfile.addVariableAttribute(varName, useName5, att2.getStringValue());
                }
                else {
                    ncfile.addVariableAttribute(varName, useName5, att2.getNumericValue());
                }
            }
        }
        ncfile.create();
        if (FileWriter.debug) {
            System.out.println("File Out= " + ncfile.toString());
        }
        if (fileIn.hasUnlimitedDimension()) {
            fileIn.sendIospMessage("AddRecordStructure");
            ncfile.sendIospMessage("AddRecordStructure");
        }
        final boolean useRecordDimension = hasRecordStructure(fileIn) && hasRecordStructure(ncfile);
        final Structure recordVar = useRecordDimension ? ((Structure)fileIn.findVariable("record")) : null;
        final double total = copyVarData(ncfile, varlist, recordVar, progressListeners);
        ncfile.flush();
        if (FileWriter.debug) {
            System.out.println("FileWriter done total bytes = " + total);
        }
        fileIn.sendIospMessage("RemoveRecordStructure");
        return ncfile;
    }
    
    private static boolean hasRecordStructure(final NetcdfFile file) {
        final Variable v = file.findVariable("record");
        return v != null && v.getDataType() == DataType.STRUCTURE;
    }
    
    public static double copyVarData(final NetcdfFileWriteable ncfile, final List<Variable> varlist, final Structure recordVar, final List<FileWriterProgressListener> progressListeners) throws IOException {
        final boolean useRecordDimension = recordVar != null;
        double total = 0.0;
        for (final Variable oldVar : varlist) {
            if (useRecordDimension && oldVar.isUnlimited()) {
                continue;
            }
            if (oldVar == recordVar) {
                continue;
            }
            if (FileWriter.debug) {
                System.out.println("write var= " + oldVar.getName() + " size = " + oldVar.getSize() + " type=" + oldVar.getDataType());
            }
            final long size = oldVar.getSize() * oldVar.getElementSize();
            total += size;
            if (size <= FileWriter.maxSize) {
                copyAll(ncfile, oldVar);
            }
            else {
                copySome(ncfile, oldVar, FileWriter.maxSize, progressListeners);
            }
        }
        if (useRecordDimension) {
            final int[] origin = { 0 };
            final int[] size2 = { 1 };
            final int nrecs = (int)recordVar.getSize();
            final int sdataSize = recordVar.getElementSize();
            double totalRecordBytes = 0.0;
            for (int count = 0; count < nrecs; ++count) {
                origin[0] = count;
                try {
                    final Array recordData = recordVar.read(origin, size2);
                    ncfile.write("record", origin, recordData);
                    if (FileWriter.debug && count == 0) {
                        System.out.println("write record size = " + sdataSize);
                    }
                }
                catch (InvalidRangeException e) {
                    e.printStackTrace();
                    break;
                }
                totalRecordBytes += sdataSize;
            }
            total += totalRecordBytes;
            totalRecordBytes /= 1000000.0;
            if (FileWriter.debug) {
                System.out.println("write record var; total = " + totalRecordBytes + " Mbytes # recs=" + nrecs);
            }
        }
        return total;
    }
    
    private static void copyAll(final NetcdfFileWriteable ncfile, final Variable oldVar) throws IOException {
        final String newName = N3iosp.makeValidNetcdfObjectName(oldVar.getName());
        Array data = oldVar.read();
        try {
            if (oldVar.getDataType() == DataType.STRING) {
                data = convertToChar(ncfile.findVariable(newName), data);
            }
            if (data.getSize() > 0L) {
                ncfile.write(newName, data);
            }
        }
        catch (InvalidRangeException e) {
            e.printStackTrace();
            throw new IOException(e.getMessage() + " for Variable " + oldVar.getName());
        }
    }
    
    private static void copySome(final NetcdfFileWriteable ncfile, final Variable oldVar, final long maxChunkSize, final List<FileWriterProgressListener> progressListeners) throws IOException {
        final String newName = N3iosp.makeValidNetcdfObjectName(oldVar.getName());
        final long maxChunkElems = maxChunkSize / oldVar.getElementSize();
        long byteWriteTotal = 0L;
        final FileWriterProgressEvent writeProgressEvent = new FileWriterProgressEvent();
        writeProgressEvent.setStatus("Variable: " + oldVar.getName());
        if (progressListeners != null) {
            for (final FileWriterProgressListener listener : progressListeners) {
                listener.writeStatus(writeProgressEvent);
            }
        }
        final ChunkingIndex index = new ChunkingIndex(oldVar.getShape());
        while (index.currentElement() < index.getSize()) {
            try {
                final int[] chunkOrigin = index.getCurrentCounter();
                final int[] chunkShape = index.computeChunkShape(maxChunkElems);
                writeProgressEvent.setWriteStatus("Reading chunk from variable: " + oldVar.getName());
                if (progressListeners != null) {
                    for (final FileWriterProgressListener listener2 : progressListeners) {
                        listener2.writeProgress(writeProgressEvent);
                    }
                }
                Array data = oldVar.read(chunkOrigin, chunkShape);
                if (oldVar.getDataType() == DataType.STRING) {
                    data = convertToChar(ncfile.findVariable(newName), data);
                }
                if (data.getSize() > 0L) {
                    writeProgressEvent.setWriteStatus("Writing chunk of variable: " + oldVar.getName());
                    writeProgressEvent.setBytesToWrite(data.getSize());
                    if (progressListeners != null) {
                        for (final FileWriterProgressListener listener3 : progressListeners) {
                            listener3.writeProgress(writeProgressEvent);
                        }
                    }
                    ncfile.write(newName, chunkOrigin, data);
                    if (FileWriter.debugWrite) {
                        System.out.println("write " + data.getSize() + " bytes");
                    }
                    byteWriteTotal += data.getSize();
                    writeProgressEvent.setBytesWritten(byteWriteTotal);
                    writeProgressEvent.setProgressPercent(100.0 * byteWriteTotal / oldVar.getSize());
                    if (progressListeners != null) {
                        for (final FileWriterProgressListener listener3 : progressListeners) {
                            listener3.writeProgress(writeProgressEvent);
                        }
                    }
                }
                index.setCurrentCounter(index.currentElement() + (int)Index.computeSize(chunkShape));
                continue;
            }
            catch (InvalidRangeException e) {
                e.printStackTrace();
                throw new IOException(e.getMessage());
            }
            break;
        }
    }
    
    private static Array convertToChar(final Variable newVar, final Array oldData) {
        final ArrayChar newData = (ArrayChar)Array.factory(DataType.CHAR, newVar.getShape());
        final Index ima = newData.getIndex();
        final IndexIterator ii = oldData.getIndexIterator();
        while (ii.hasNext()) {
            final String s = (String)ii.getObjectNext();
            final int[] c = ii.getCurrentCounter();
            for (int i = 0; i < c.length; ++i) {
                ima.setDim(i, c[i]);
            }
            newData.setString(ima, s);
        }
        return newData;
    }
    
    public FileWriter(final String fileOutName, final boolean fill) throws IOException {
        this.dimHash = new HashMap<String, Dimension>();
        this.varList = new ArrayList<Variable>();
        this.recordVar = null;
        this.ncfile = NetcdfFileWriteable.createNew(fileOutName, fill);
    }
    
    public NetcdfFileWriteable getNetcdf() {
        return this.ncfile;
    }
    
    public void writeGlobalAttribute(final Attribute att) {
        final String useName = N3iosp.makeValidNetcdfObjectName(att.getName());
        if (att.isArray()) {
            this.ncfile.addGlobalAttribute(useName, att.getValues());
        }
        else if (att.isString()) {
            this.ncfile.addGlobalAttribute(useName, att.getStringValue());
        }
        else {
            this.ncfile.addGlobalAttribute(useName, att.getNumericValue());
        }
    }
    
    public void writeAttribute(String varName, final Attribute att) {
        final String attName = N3iosp.makeValidNetcdfObjectName(att.getName());
        varName = N3iosp.makeValidNetcdfObjectName(varName);
        if (att.isArray()) {
            this.ncfile.addVariableAttribute(varName, attName, att.getValues());
        }
        else if (att.isString()) {
            this.ncfile.addVariableAttribute(varName, attName, att.getStringValue());
        }
        else {
            this.ncfile.addVariableAttribute(varName, attName, att.getNumericValue());
        }
    }
    
    public Dimension writeDimension(final Dimension dim) {
        final String useName = N3iosp.makeValidNetcdfObjectName(dim.getName());
        final Dimension newDim = this.ncfile.addDimension(useName, dim.isUnlimited() ? 0 : dim.getLength(), dim.isShared(), dim.isUnlimited(), dim.isVariableLength());
        this.dimHash.put(useName, newDim);
        if (FileWriter.debug) {
            System.out.println("write dim= " + newDim);
        }
        return newDim;
    }
    
    public void writeVariable(final Variable oldVar) {
        final Dimension[] dims = new Dimension[oldVar.getRank()];
        final List<Dimension> dimvList = oldVar.getDimensions();
        for (int j = 0; j < dimvList.size(); ++j) {
            final Dimension oldD = dimvList.get(j);
            Dimension newD = this.dimHash.get(N3iosp.makeValidNetcdfObjectName(oldD.getName()));
            if (null == newD) {
                newD = this.writeDimension(oldD);
                this.dimHash.put(newD.getName(), newD);
            }
            dims[j] = newD;
        }
        final String useName = N3iosp.makeValidNetcdfObjectName(oldVar.getShortName());
        Label_0242: {
            if (oldVar.getDataType() == DataType.STRING) {
                try {
                    int max_strlen = 0;
                    final ArrayObject data = (ArrayObject)oldVar.read();
                    final IndexIterator ii = data.getIndexIterator();
                    while (ii.hasNext()) {
                        final String s = (String)ii.next();
                        max_strlen = Math.max(max_strlen, s.length());
                    }
                    this.ncfile.addStringVariable(useName, Arrays.asList(dims), max_strlen);
                    break Label_0242;
                }
                catch (IOException ioe) {
                    FileWriter.log.error("Error reading String variable " + oldVar, ioe);
                    return;
                }
            }
            this.ncfile.addVariable(useName, oldVar.getDataType(), dims);
        }
        this.varList.add(oldVar);
        if (FileWriter.debug) {
            System.out.println("write var= " + oldVar);
        }
        final List<Attribute> attList = oldVar.getAttributes();
        for (final Attribute att : attList) {
            this.writeAttribute(useName, att);
        }
    }
    
    public void writeVariables(final List<Variable> varList) {
        for (final Variable v : varList) {
            this.writeVariable(v);
        }
    }
    
    public void setRecordVariable(final Structure recordVar) {
        this.recordVar = recordVar;
    }
    
    public void finish() throws IOException {
        this.ncfile.create();
        final double total = copyVarData(this.ncfile, this.varList, this.recordVar, 0L);
        this.ncfile.close();
        if (FileWriter.debug) {
            System.out.println("FileWriter finish total bytes = " + total);
        }
    }
    
    private static void usage() {
        System.out.println("usage: ucar.nc2.FileWriter -in <fileIn> -out <fileOut> [-delay <millisecs>]");
    }
    
    @Deprecated
    public static double copyVarData(final NetcdfFileWriteable ncfile, final List<Variable> varlist, final Structure recordVar, final long delay) throws IOException {
        return copyVarData(ncfile, varlist, recordVar, null);
    }
    
    @Deprecated
    public static NetcdfFile writeToFile(final NetcdfFile fileIn, final String fileOutName, final boolean fill, final int delay) throws IOException {
        return writeToFile(fileIn, fileOutName, fill, false, null);
    }
    
    @Deprecated
    public static NetcdfFile writeToFile(final NetcdfFile fileIn, final String fileOutName, final boolean fill, final int delay, final boolean isLargeFile) throws IOException {
        return writeToFile(fileIn, fileOutName, fill, isLargeFile, null);
    }
    
    public static void main(final String[] arg) throws IOException {
        if (arg.length < 4) {
            usage();
            System.exit(0);
        }
        String datasetIn = null;
        String datasetOut = null;
        final int delay = 0;
        for (int i = 0; i < arg.length; ++i) {
            final String s = arg[i];
            if (s.equalsIgnoreCase("-in")) {
                datasetIn = arg[i + 1];
            }
            if (s.equalsIgnoreCase("-out")) {
                datasetOut = arg[i + 1];
            }
        }
        if (datasetIn == null || datasetOut == null) {
            usage();
            System.exit(0);
        }
        FileWriter.debugExtend = true;
        final NetcdfFile ncfileIn = NetcdfFile.open(datasetIn, null);
        final NetcdfFile ncfileOut = writeToFile(ncfileIn, datasetOut, false, false, null);
        ncfileIn.close();
        ncfileOut.close();
        System.out.println("NetcdfFile written = " + ncfileOut);
    }
    
    static {
        FileWriter.log = LoggerFactory.getLogger(FileWriter.class);
        FileWriter.debug = false;
        FileWriter.debugWrite = false;
        FileWriter.maxSize = 1000000L;
    }
    
    public static class ChunkingIndex extends Index
    {
        public ChunkingIndex(final int[] shape) {
            super(shape);
        }
        
        public int[] computeChunkShape(final long maxChunkElems) {
            final int[] chunkShape = new int[this.rank];
            for (int iDim = 0; iDim < this.rank; ++iDim) {
                chunkShape[iDim] = (int)(maxChunkElems / this.stride[iDim]);
                chunkShape[iDim] = ((chunkShape[iDim] == 0) ? 1 : chunkShape[iDim]);
                chunkShape[iDim] = Math.min(chunkShape[iDim], this.shape[iDim] - this.current[iDim]);
            }
            return chunkShape;
        }
    }
    
    public static class FileWriterProgressEvent
    {
        private double progressPercent;
        private long bytesWritten;
        private long bytesToWrite;
        private String status;
        private String writeStatus;
        
        public void setProgressPercent(final double progressPercent) {
            this.progressPercent = progressPercent;
        }
        
        public double getProgressPercent() {
            return this.progressPercent;
        }
        
        public void setBytesWritten(final long bytesWritten) {
            this.bytesWritten = bytesWritten;
        }
        
        public long getBytesWritten() {
            return this.bytesWritten;
        }
        
        public void setBytesToWrite(final long bytesToWrite) {
            this.bytesToWrite = bytesToWrite;
        }
        
        public long getBytesToWrite() {
            return this.bytesToWrite;
        }
        
        public void setStatus(final String status) {
            this.status = status;
        }
        
        public String getStatus() {
            return this.status;
        }
        
        public void setWriteStatus(final String writeStatus) {
            this.writeStatus = writeStatus;
        }
        
        public String getWriteStatus() {
            return this.writeStatus;
        }
    }
    
    public interface FileWriterProgressListener
    {
        void writeProgress(final FileWriterProgressEvent p0);
        
        void writeStatus(final FileWriterProgressEvent p0);
    }
}
