// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2;

import org.slf4j.LoggerFactory;
import java.io.IOException;
import ucar.ma2.Array;
import ucar.nc2.util.CancelTask;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Section;
import org.slf4j.Logger;

class SectionReader implements ProxyReader
{
    private static Logger log;
    private Section orgSection;
    private Variable orgClient;
    
    SectionReader(final Variable orgClient, final Section section) throws InvalidRangeException {
        this.orgClient = orgClient;
        this.orgSection = (section.isImmutable() ? section : new Section(section.getRanges()));
    }
    
    public Array reallyRead(final Variable client, final CancelTask cancelTask) throws IOException {
        try {
            return this.orgClient._read(this.orgSection);
        }
        catch (InvalidRangeException e) {
            throw new RuntimeException(e);
        }
    }
    
    public Array reallyRead(final Variable client, final Section section, final CancelTask cancelTask) throws IOException, InvalidRangeException {
        final Section want = this.orgSection.compose(section);
        return this.orgClient._read(want);
    }
    
    static {
        SectionReader.log = LoggerFactory.getLogger(SectionReader.class);
    }
}
