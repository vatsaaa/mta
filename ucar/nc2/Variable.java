// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2;

import org.slf4j.LoggerFactory;
import java.util.Collections;
import java.util.StringTokenizer;
import java.util.Collection;
import ucar.nc2.iosp.IospHelper;
import java.nio.channels.WritableByteChannel;
import ucar.ma2.ArrayStructure;
import ucar.nc2.util.CancelTask;
import ucar.ma2.ArrayChar;
import ucar.ma2.Index;
import java.io.IOException;
import ucar.ma2.Array;
import ucar.ma2.InvalidRangeException;
import java.util.ArrayList;
import ucar.ma2.Range;
import java.util.Formatter;
import java.util.Iterator;
import java.util.List;
import ucar.ma2.DataType;
import ucar.ma2.Section;
import org.slf4j.Logger;

public class Variable implements VariableIF, ProxyReader
{
    public static final int defaultSizeToCache = 4000;
    public static final int defaultCoordsSizeToCache = 40000;
    protected static boolean debugCaching;
    private static Logger log;
    protected NetcdfFile ncfile;
    protected Group group;
    protected String shortName;
    protected int[] shape;
    protected Section shapeAsSection;
    protected DataType dataType;
    protected int elementSize;
    protected List<Dimension> dimensions;
    protected List<Attribute> attributes;
    protected boolean isVariableLength;
    protected boolean isMetadata;
    private boolean immutable;
    protected Cache cache;
    protected int sizeToCache;
    protected Structure parent;
    protected ProxyReader proxyReader;
    private EnumTypedef enumTypedef;
    private static boolean showSize;
    protected int hashCode;
    protected Object spiObject;
    
    public String getName() {
        return NetcdfFile.makeFullName(this.group, this);
    }
    
    public String getNameEscaped() {
        return NetcdfFile.makeFullNameEscaped(this.group, this);
    }
    
    public String getShortName() {
        return this.shortName;
    }
    
    public DataType getDataType() {
        return this.dataType;
    }
    
    public int[] getShape() {
        final int[] result = new int[this.shape.length];
        System.arraycopy(this.shape, 0, result, 0, this.shape.length);
        return result;
    }
    
    public int getShape(final int index) {
        return this.shape[index];
    }
    
    public long getSize() {
        long size = 1L;
        for (int i = 0; i < this.shape.length; ++i) {
            if (this.shape[i] >= 0) {
                size *= this.shape[i];
            }
        }
        return size;
    }
    
    public int getElementSize() {
        return this.elementSize;
    }
    
    public int getRank() {
        return this.shape.length;
    }
    
    public Group getParentGroup() {
        return this.group;
    }
    
    public boolean isMetadata() {
        return this.isMetadata;
    }
    
    public boolean isScalar() {
        return this.getRank() == 0;
    }
    
    public boolean isVariableLength() {
        return this.isVariableLength;
    }
    
    public boolean isUnsigned() {
        final Attribute att = this.findAttributeIgnoreCase("_Unsigned");
        return att != null && att.getStringValue().equalsIgnoreCase("true");
    }
    
    public boolean isUnlimited() {
        for (final Dimension d : this.dimensions) {
            if (d.isUnlimited()) {
                return true;
            }
        }
        return false;
    }
    
    public List<Dimension> getDimensions() {
        return this.dimensions;
    }
    
    public Dimension getDimension(final int i) {
        if (i < 0 || i >= this.getRank()) {
            return null;
        }
        return this.dimensions.get(i);
    }
    
    public String getDimensionsString() {
        final Formatter buf = new Formatter();
        for (int i = 0; i < this.dimensions.size(); ++i) {
            final Dimension myd = this.dimensions.get(i);
            final String dimName = myd.getName();
            if (i != 0) {
                buf.format(" ", new Object[0]);
            }
            if (myd.isVariableLength()) {
                buf.format("*", new Object[0]);
            }
            else if (myd.isShared()) {
                buf.format("%s", dimName);
            }
            else {
                buf.format("%d", myd.getLength());
            }
        }
        return buf.toString();
    }
    
    public int findDimensionIndex(final String name) {
        for (int i = 0; i < this.dimensions.size(); ++i) {
            final Dimension d = this.dimensions.get(i);
            if (name.equals(d.getName())) {
                return i;
            }
        }
        return -1;
    }
    
    public List<Attribute> getAttributes() {
        return this.attributes;
    }
    
    public Attribute findAttribute(final String name) {
        for (final Attribute a : this.attributes) {
            if (name.equals(a.getName())) {
                return a;
            }
        }
        return null;
    }
    
    public Attribute findAttributeIgnoreCase(final String name) {
        for (final Attribute a : this.attributes) {
            if (name.equalsIgnoreCase(a.getName())) {
                return a;
            }
        }
        return null;
    }
    
    public String getDescription() {
        String desc = null;
        Attribute att = this.findAttributeIgnoreCase("long_name");
        if (att != null && att.isString()) {
            desc = att.getStringValue();
        }
        if (desc == null) {
            att = this.findAttributeIgnoreCase("description");
            if (att != null && att.isString()) {
                desc = att.getStringValue();
            }
        }
        if (desc == null) {
            att = this.findAttributeIgnoreCase("title");
            if (att != null && att.isString()) {
                desc = att.getStringValue();
            }
        }
        if (desc == null) {
            att = this.findAttributeIgnoreCase("standard_name");
            if (att != null && att.isString()) {
                desc = att.getStringValue();
            }
        }
        return desc;
    }
    
    public String getUnitsString() {
        String units = null;
        final Attribute att = this.findAttributeIgnoreCase("units");
        if (att != null && att.isString()) {
            units = att.getStringValue().trim();
        }
        return units;
    }
    
    public List<Range> getRanges() {
        return this.getShapeAsSection().getRanges();
    }
    
    public Section getShapeAsSection() {
        if (this.shapeAsSection == null) {
            try {
                final List<Range> list = new ArrayList<Range>();
                for (final Dimension d : this.dimensions) {
                    final int len = d.getLength();
                    if (len > 0) {
                        list.add(new Range(d.getName(), 0, len - 1));
                    }
                    else if (len == 0) {
                        list.add(Range.EMPTY);
                    }
                    else {
                        list.add(Range.VLEN);
                    }
                }
                this.shapeAsSection = new Section(list).makeImmutable();
            }
            catch (InvalidRangeException e) {
                Variable.log.error("Bad shape in variable " + this.getName(), e);
                throw new IllegalStateException(e.getMessage());
            }
        }
        return this.shapeAsSection;
    }
    
    public ProxyReader getProxyReader() {
        return this.proxyReader;
    }
    
    public void setProxyReader(final ProxyReader proxyReader) {
        this.proxyReader = proxyReader;
    }
    
    public Variable section(final List<Range> ranges) throws InvalidRangeException {
        return this.section(new Section(ranges, this.shape).makeImmutable());
    }
    
    public Variable section(Section subsection) throws InvalidRangeException {
        subsection = Section.fill(subsection, this.shape);
        final Variable sectionV = this.copy();
        sectionV.setProxyReader(new SectionReader(this, subsection));
        sectionV.shape = subsection.getShape();
        sectionV.createNewCache();
        sectionV.setCaching(false);
        sectionV.dimensions = new ArrayList<Dimension>();
        for (int i = 0; i < this.getRank(); ++i) {
            final Dimension oldD = this.getDimension(i);
            final Dimension newD = (oldD.getLength() == sectionV.shape[i]) ? oldD : new Dimension(oldD.getName(), sectionV.shape[i], false);
            newD.setUnlimited(oldD.isUnlimited());
            sectionV.dimensions.add(newD);
        }
        sectionV.resetShape();
        return sectionV;
    }
    
    public Variable slice(final int dim, final int value) throws InvalidRangeException {
        if (dim < 0 || dim >= this.shape.length) {
            throw new InvalidRangeException("Slice dim invalid= " + dim);
        }
        boolean recordSliceOk = false;
        if (dim == 0 && value == 0) {
            final Dimension d = this.getDimension(0);
            recordSliceOk = d.isUnlimited();
        }
        if (!recordSliceOk && (value < 0 || value >= this.shape[dim])) {
            throw new InvalidRangeException("Slice value invalid= " + value + " for dimension " + dim);
        }
        final Variable sliceV = this.copy();
        final Section slice = new Section(this.getShapeAsSection());
        slice.replaceRange(dim, new Range(value, value)).makeImmutable();
        sliceV.setProxyReader(new SliceReader(this, dim, slice));
        sliceV.createNewCache();
        sliceV.setCaching(false);
        sliceV.dimensions.remove(dim);
        sliceV.resetShape();
        return sliceV;
    }
    
    protected Variable copy() {
        return new Variable(this);
    }
    
    public String lookupEnumString(final int val) {
        if (!this.dataType.isEnum()) {
            throw new UnsupportedOperationException("Can only call Variable.lookupEnumVal() on enum types");
        }
        return this.enumTypedef.lookupEnumString(val);
    }
    
    public void setEnumTypedef(final EnumTypedef enumTypedef) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        if (!this.dataType.isEnum()) {
            throw new UnsupportedOperationException("Can only call Variable.setEnumTypedef() on enum types");
        }
        this.enumTypedef = enumTypedef;
    }
    
    public EnumTypedef getEnumTypedef() {
        return this.enumTypedef;
    }
    
    public Array read(final int[] origin, final int[] shape) throws IOException, InvalidRangeException {
        if (origin == null && shape == null) {
            return this.read();
        }
        if (origin == null) {
            return this.read(new Section(shape));
        }
        if (shape == null) {
            return this.read(new Section(origin, shape));
        }
        return this.read(new Section(origin, shape));
    }
    
    public Array read(final String sectionSpec) throws IOException, InvalidRangeException {
        return this.read(new Section(sectionSpec));
    }
    
    public Array read(final List<Range> ranges) throws IOException, InvalidRangeException {
        if (null == ranges) {
            return this._read();
        }
        return this.read(new Section(ranges));
    }
    
    public Array read(final Section section) throws IOException, InvalidRangeException {
        return (section == null) ? this._read() : this._read(Section.fill(section, this.shape));
    }
    
    public Array read() throws IOException {
        return this._read();
    }
    
    public byte readScalarByte() throws IOException {
        final Array data = this.getScalarData();
        return data.getByte(Index.scalarIndexImmutable);
    }
    
    public short readScalarShort() throws IOException {
        final Array data = this.getScalarData();
        return data.getShort(Index.scalarIndexImmutable);
    }
    
    public int readScalarInt() throws IOException {
        final Array data = this.getScalarData();
        return data.getInt(Index.scalarIndexImmutable);
    }
    
    public long readScalarLong() throws IOException {
        final Array data = this.getScalarData();
        return data.getLong(Index.scalarIndexImmutable);
    }
    
    public float readScalarFloat() throws IOException {
        final Array data = this.getScalarData();
        return data.getFloat(Index.scalarIndexImmutable);
    }
    
    public double readScalarDouble() throws IOException {
        final Array data = this.getScalarData();
        return data.getDouble(Index.scalarIndexImmutable);
    }
    
    public String readScalarString() throws IOException {
        final Array data = this.getScalarData();
        if (this.dataType == DataType.STRING) {
            return (String)data.getObject(Index.scalarIndexImmutable);
        }
        if (this.dataType == DataType.CHAR) {
            final ArrayChar dataC = (ArrayChar)data;
            return dataC.getString();
        }
        throw new IllegalArgumentException("readScalarString not STRING or CHAR " + this.getName());
    }
    
    protected Array getScalarData() throws IOException {
        Array scalarData = (this.cache != null && this.cache.data != null) ? this.cache.data : this.read();
        scalarData = scalarData.reduce();
        if (scalarData.getRank() == 0 || (scalarData.getRank() == 1 && this.dataType == DataType.CHAR)) {
            return scalarData;
        }
        throw new UnsupportedOperationException("not a scalar variable =" + this);
    }
    
    protected Array _read() throws IOException {
        if (this.cache != null && this.cache.data != null) {
            if (Variable.debugCaching) {
                System.out.println("got data from cache " + this.getName());
            }
            return this.cache.data.copy();
        }
        final Array data = this.proxyReader.reallyRead(this, null);
        if (this.isCaching()) {
            this.setCachedData(data);
            if (Variable.debugCaching) {
                System.out.println("cache " + this.getName());
            }
            return this.cache.data.copy();
        }
        return data;
    }
    
    public Array reallyRead(final Variable client, final CancelTask cancelTask) throws IOException {
        if (this.isMemberOfStructure()) {
            final List<String> memList = new ArrayList<String>();
            memList.add(this.getShortName());
            final Structure s = this.parent.select(memList);
            final ArrayStructure as = (ArrayStructure)s.read();
            return as.extractMemberArray(as.findMember(this.shortName));
        }
        try {
            return this.ncfile.readData(this, this.getShapeAsSection());
        }
        catch (InvalidRangeException e) {
            e.printStackTrace();
            throw new IOException(e.getMessage());
        }
    }
    
    protected Array _read(final Section section) throws IOException, InvalidRangeException {
        if (null == section || section.computeSize() == this.getSize()) {
            return this._read();
        }
        if (this.isCaching()) {
            if (this.cache.data == null) {
                this.setCachedData(this._read());
                if (Variable.debugCaching) {
                    System.out.println("cache " + this.getName());
                }
            }
            if (Variable.debugCaching) {
                System.out.println("got data from cache " + this.getName());
            }
            return this.cache.data.sectionNoReduce(section.getRanges()).copy();
        }
        return this.proxyReader.reallyRead(this, section, null);
    }
    
    public Array reallyRead(final Variable client, final Section section, final CancelTask cancelTask) throws IOException, InvalidRangeException {
        if (this.isMemberOfStructure()) {
            throw new UnsupportedOperationException("Cannot directly read section of Member Variable=" + this.getName());
        }
        return this.ncfile.readData(this, section);
    }
    
    public long readToByteChannel(final Section section, final WritableByteChannel wbc) throws IOException, InvalidRangeException {
        if (this.ncfile == null || this.hasCachedData()) {
            return IospHelper.copyToByteChannel(this.read(section), wbc);
        }
        return this.ncfile.readToByteChannel(this, section, wbc);
    }
    
    public String getNameAndDimensions() {
        final Formatter buf = new Formatter();
        this.getNameAndDimensions(buf, true, false);
        return buf.toString();
    }
    
    public String getNameAndDimensions(final boolean strict) {
        final Formatter buf = new Formatter();
        this.getNameAndDimensions(buf, false, strict);
        return buf.toString();
    }
    
    public void getNameAndDimensions(final StringBuilder buf) {
        this.getNameAndDimensions(buf, true, false);
    }
    
    @Deprecated
    public void getNameAndDimensions(final StringBuffer buf) {
        final Formatter proxy = new Formatter();
        this.getNameAndDimensions(proxy, true, false);
        buf.append(proxy.toString());
    }
    
    public void getNameAndDimensions(final StringBuilder buf, final boolean useFullName, final boolean strict) {
        final Formatter proxy = new Formatter();
        this.getNameAndDimensions(proxy, useFullName, strict);
        buf.append(proxy.toString());
    }
    
    public void getNameAndDimensions(final Formatter buf, boolean useFullName, final boolean strict) {
        useFullName = (useFullName && !strict);
        String name = useFullName ? this.getName() : this.getShortName();
        if (strict) {
            name = NetcdfFile.escapeName(name);
        }
        buf.format("%s", name);
        if (this.getRank() > 0) {
            buf.format("(", new Object[0]);
        }
        for (int i = 0; i < this.dimensions.size(); ++i) {
            final Dimension myd = this.dimensions.get(i);
            String dimName = myd.getName();
            if (dimName != null && strict) {
                dimName = NetcdfFile.escapeName(dimName);
            }
            if (i != 0) {
                buf.format(", ", new Object[0]);
            }
            if (myd.isVariableLength()) {
                buf.format("*", new Object[0]);
            }
            else if (myd.isShared()) {
                if (!strict) {
                    buf.format("%s=%d", dimName, myd.getLength());
                }
                else {
                    buf.format("%s", dimName);
                }
            }
            else {
                if (dimName != null) {
                    buf.format("%s=", dimName);
                }
                buf.format("%d", myd.getLength());
            }
        }
        if (this.getRank() > 0) {
            buf.format(")", new Object[0]);
        }
    }
    
    @Override
    public String toString() {
        return this.writeCDL("   ", false, false);
    }
    
    public String writeCDL(final String indent, final boolean useFullName, final boolean strict) {
        final Formatter buf = new Formatter();
        this.writeCDL(buf, indent, useFullName, strict);
        return buf.toString();
    }
    
    protected void writeCDL(final Formatter buf, final String indent, final boolean useFullName, final boolean strict) {
        buf.format(indent, new Object[0]);
        if (this.dataType.isEnum()) {
            if (this.enumTypedef == null) {
                buf.format("enum UNKNOWN", new Object[0]);
            }
            else {
                buf.format("enum %s", this.enumTypedef.getName());
            }
        }
        else {
            buf.format(this.dataType.toString(), new Object[0]);
        }
        buf.format(" ", new Object[0]);
        this.getNameAndDimensions(buf, useFullName, strict);
        buf.format(";", new Object[0]);
        if (!strict) {
            buf.format(this.extraInfo(), new Object[0]);
        }
        buf.format("\n", new Object[0]);
        for (final Attribute att : this.getAttributes()) {
            buf.format("%s  ", indent);
            if (strict) {
                buf.format(NetcdfFile.escapeName(this.getShortName()), new Object[0]);
            }
            buf.format(":%s;", att.toString(strict));
            if (!strict && att.getDataType() != DataType.STRING) {
                buf.format(" // %s", att.getDataType());
            }
            buf.format("\n", new Object[0]);
        }
    }
    
    public String toStringDebug() {
        return this.ncfile.toStringDebug(this);
    }
    
    protected String extraInfo() {
        return Variable.showSize ? (" // " + this.getElementSize() + " " + this.getSize()) : "";
    }
    
    @Override
    public boolean equals(final Object oo) {
        if (this == oo) {
            return true;
        }
        if (!(oo instanceof Variable)) {
            return false;
        }
        final Variable o = (Variable)oo;
        if (!this.getShortName().equals(o.getShortName())) {
            return false;
        }
        if (this.isScalar() != o.isScalar()) {
            return false;
        }
        if (this.getDataType() != o.getDataType()) {
            return false;
        }
        if (!this.getParentGroup().equals(o.getParentGroup())) {
            return false;
        }
        if (this.getParentStructure() != null && !this.getParentStructure().equals(o.getParentStructure())) {
            return false;
        }
        if (this.isVariableLength() != o.isVariableLength()) {
            return false;
        }
        if (this.dimensions.size() != o.getDimensions().size()) {
            return false;
        }
        for (int i = 0; i < this.dimensions.size(); ++i) {
            if (!this.getDimension(i).equals(o.getDimension(i))) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int result = 17;
            result = 37 * result + this.getShortName().hashCode();
            if (this.isScalar()) {
                ++result;
            }
            result = 37 * result + this.getDataType().hashCode();
            result = 37 * result + this.getParentGroup().hashCode();
            if (this.parent != null) {
                result = 37 * result + this.parent.hashCode();
            }
            if (this.isVariableLength) {
                ++result;
            }
            result = 37 * result + this.dimensions.hashCode();
            this.hashCode = result;
        }
        return this.hashCode;
    }
    
    public int compareTo(final VariableSimpleIF o) {
        return this.getName().compareTo(o.getName());
    }
    
    protected Variable() {
        this.dimensions = new ArrayList<Dimension>(5);
        this.attributes = new ArrayList<Attribute>();
        this.isVariableLength = false;
        this.isMetadata = false;
        this.immutable = false;
        this.cache = new Cache();
        this.sizeToCache = -1;
        this.parent = null;
        this.proxyReader = this;
        this.hashCode = 0;
    }
    
    public Variable(final NetcdfFile ncfile, final Group group, final Structure parent, final String shortName) {
        this.dimensions = new ArrayList<Dimension>(5);
        this.attributes = new ArrayList<Attribute>();
        this.isVariableLength = false;
        this.isMetadata = false;
        this.immutable = false;
        this.cache = new Cache();
        this.sizeToCache = -1;
        this.parent = null;
        this.proxyReader = this;
        this.hashCode = 0;
        this.ncfile = ncfile;
        this.group = ((group == null) ? ncfile.getRootGroup() : group);
        this.parent = parent;
        this.shortName = shortName;
    }
    
    public Variable(final NetcdfFile ncfile, final Group group, final Structure parent, final String shortName, final DataType dtype, final String dims) {
        this.dimensions = new ArrayList<Dimension>(5);
        this.attributes = new ArrayList<Attribute>();
        this.isVariableLength = false;
        this.isMetadata = false;
        this.immutable = false;
        this.cache = new Cache();
        this.sizeToCache = -1;
        this.parent = null;
        this.proxyReader = this;
        this.hashCode = 0;
        this.ncfile = ncfile;
        this.group = ((group == null) ? ncfile.getRootGroup() : group);
        this.parent = parent;
        this.shortName = shortName;
        this.setDataType(dtype);
        this.setDimensions(dims);
    }
    
    public Variable(final Variable from) {
        this.dimensions = new ArrayList<Dimension>(5);
        this.attributes = new ArrayList<Attribute>();
        this.isVariableLength = false;
        this.isMetadata = false;
        this.immutable = false;
        this.cache = new Cache();
        this.sizeToCache = -1;
        this.parent = null;
        this.proxyReader = this;
        this.hashCode = 0;
        this.attributes = new ArrayList<Attribute>(from.attributes);
        this.cache = from.cache;
        this.dataType = from.getDataType();
        this.dimensions = new ArrayList<Dimension>(from.dimensions);
        this.elementSize = from.getElementSize();
        this.enumTypedef = from.enumTypedef;
        this.group = from.group;
        this.isMetadata = from.isMetadata;
        this.isVariableLength = from.isVariableLength;
        this.ncfile = from.ncfile;
        this.parent = from.parent;
        this.shape = from.getShape();
        this.shortName = from.shortName;
        this.sizeToCache = from.sizeToCache;
        this.spiObject = from.spiObject;
    }
    
    public void setDataType(final DataType dataType) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        this.dataType = dataType;
        this.elementSize = this.getDataType().getSize();
    }
    
    public void setName(final String shortName) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        this.shortName = shortName;
    }
    
    public void setParentGroup(final Group group) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        this.group = group;
    }
    
    public void setElementSize(final int elementSize) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        this.elementSize = elementSize;
    }
    
    public Attribute addAttribute(final Attribute att) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        for (int i = 0; i < this.attributes.size(); ++i) {
            final Attribute a = this.attributes.get(i);
            if (att.getName().equals(a.getName())) {
                this.attributes.set(i, att);
                return att;
            }
        }
        this.attributes.add(att);
        return att;
    }
    
    public boolean remove(final Attribute a) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        return a != null && this.attributes.remove(a);
    }
    
    public boolean removeAttribute(final String attName) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        final Attribute att = this.findAttribute(attName);
        return att != null && this.attributes.remove(att);
    }
    
    public boolean removeAttributeIgnoreCase(final String attName) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        final Attribute att = this.findAttributeIgnoreCase(attName);
        return att != null && this.attributes.remove(att);
    }
    
    public void setDimensions(final List<Dimension> dims) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        this.dimensions = ((dims == null) ? new ArrayList<Dimension>() : new ArrayList<Dimension>(dims));
        this.resetShape();
    }
    
    public void resetShape() {
        this.shape = new int[this.dimensions.size()];
        for (int i = 0; i < this.dimensions.size(); ++i) {
            final Dimension dim = this.dimensions.get(i);
            this.shape[i] = dim.getLength();
            if (dim.isVariableLength()) {
                this.isVariableLength = true;
            }
        }
        this.shapeAsSection = null;
    }
    
    public void setDimensions(final String dimString) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        final List<Dimension> newDimensions = new ArrayList<Dimension>();
        if (dimString == null || dimString.length() == 0) {
            this.dimensions = newDimensions;
            this.resetShape();
            return;
        }
        final StringTokenizer stoke = new StringTokenizer(dimString);
        while (stoke.hasMoreTokens()) {
            final String dimName = stoke.nextToken();
            Dimension d = dimName.equals("*") ? Dimension.VLEN : this.group.findDimension(dimName);
            if (d == null) {
                try {
                    final int len = Integer.parseInt(dimName);
                    d = new Dimension("", len, false, false, false);
                }
                catch (Exception e) {
                    throw new IllegalArgumentException("Variable " + this.getName() + " setDimensions = " + dimString + " FAILED, dim doesnt exist=" + dimName + " file = " + this.ncfile.getLocation());
                }
            }
            newDimensions.add(d);
        }
        this.dimensions = newDimensions;
        this.resetShape();
    }
    
    public void resetDimensions() {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        final ArrayList<Dimension> newDimensions = new ArrayList<Dimension>();
        for (final Dimension dim : this.dimensions) {
            if (dim.isShared()) {
                final Dimension newD = this.group.findDimension(dim.getName());
                if (newD == null) {
                    throw new IllegalArgumentException("Variable " + this.getName() + " resetDimensions  FAILED, dim doesnt exist in parent group=" + dim);
                }
                newDimensions.add(newD);
            }
            else {
                newDimensions.add(dim);
            }
        }
        this.dimensions = newDimensions;
        this.resetShape();
    }
    
    public void setDimensionsAnonymous(final int[] shape) throws InvalidRangeException {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        this.dimensions = new ArrayList<Dimension>();
        for (int i = 0; i < shape.length; ++i) {
            if (shape[i] < 1 && shape[i] != -1) {
                throw new InvalidRangeException("shape[" + i + "]=" + shape[i] + " must be > 0");
            }
            Dimension anon;
            if (shape[i] == -1) {
                anon = Dimension.VLEN;
                this.isVariableLength = true;
            }
            else {
                anon = new Dimension(null, shape[i], false, false, false);
            }
            this.dimensions.add(anon);
        }
        this.resetShape();
    }
    
    public void setIsScalar() {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        this.dimensions = new ArrayList<Dimension>();
        this.resetShape();
    }
    
    public void setDimension(final int idx, final Dimension dim) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        this.dimensions.set(idx, dim);
        this.resetShape();
    }
    
    public Variable setImmutable() {
        this.immutable = true;
        this.dimensions = Collections.unmodifiableList((List<? extends Dimension>)this.dimensions);
        this.attributes = Collections.unmodifiableList((List<? extends Attribute>)this.attributes);
        return this;
    }
    
    public boolean isImmutable() {
        return this.immutable;
    }
    
    public Object getSPobject() {
        return this.spiObject;
    }
    
    public void setSPobject(final Object spiObject) {
        this.spiObject = spiObject;
    }
    
    public int getSizeToCache() {
        if (this.sizeToCache >= 0) {
            return this.sizeToCache;
        }
        return this.isCoordinateVariable() ? 40000 : 4000;
    }
    
    public void setSizeToCache(final int sizeToCache) {
        this.sizeToCache = sizeToCache;
    }
    
    public void setCaching(final boolean caching) {
        this.cache.isCaching = caching;
        this.cache.cachingSet = true;
    }
    
    public boolean isCaching() {
        if (!this.cache.cachingSet) {
            this.cache.isCaching = (!this.isVariableLength && this.getSize() * this.getElementSize() < this.getSizeToCache());
            this.cache.cachingSet = true;
        }
        return this.cache.isCaching;
    }
    
    public void invalidateCache() {
        this.cache.data = null;
    }
    
    public void setCachedData(final Array cacheData) {
        this.setCachedData(cacheData, false);
    }
    
    public void setCachedData(final Array cacheData, final boolean isMetadata) {
        if (cacheData != null && cacheData.getElementType() != this.getDataType().getPrimitiveClassType()) {
            throw new IllegalArgumentException("setCachedData type=" + cacheData.getElementType() + " incompatible with variable type=" + this.getDataType());
        }
        this.cache.data = cacheData;
        this.isMetadata = isMetadata;
        this.cache.cachingSet = true;
        this.cache.isCaching = true;
    }
    
    public void createNewCache() {
        this.cache = new Cache();
    }
    
    public boolean hasCachedData() {
        return this.cache != null && null != this.cache.data;
    }
    
    public boolean isMemberOfStructure() {
        return this.parent != null;
    }
    
    public Structure getParentStructure() {
        return this.parent;
    }
    
    public void setParentStructure(final Structure parent) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        this.parent = parent;
    }
    
    public List<Dimension> getDimensionsAll() {
        final List<Dimension> dimsAll = new ArrayList<Dimension>();
        this.addDimensionsAll(dimsAll, this);
        return dimsAll;
    }
    
    private void addDimensionsAll(final List<Dimension> result, final Variable v) {
        if (v.isMemberOfStructure()) {
            this.addDimensionsAll(result, v.getParentStructure());
        }
        for (int i = 0; i < v.getRank(); ++i) {
            result.add(v.getDimension(i));
        }
    }
    
    public int[] getShapeAll() {
        if (this.parent == null) {
            return this.getShape();
        }
        final List<Dimension> dimAll = this.getDimensionsAll();
        final int[] shapeAll = new int[dimAll.size()];
        for (int i = 0; i < dimAll.size(); ++i) {
            shapeAll[i] = dimAll.get(i).getLength();
        }
        return shapeAll;
    }
    
    public boolean isCoordinateVariable() {
        if (this.dataType == DataType.STRUCTURE || this.isMemberOfStructure()) {
            return false;
        }
        final int n = this.getRank();
        if (n == 1 && this.dimensions.size() == 1) {
            final Dimension firstd = this.dimensions.get(0);
            if (this.shortName.equals(firstd.getName())) {
                return true;
            }
        }
        if (n == 2 && this.dimensions.size() == 2) {
            final Dimension firstd = this.dimensions.get(0);
            if (this.shortName.equals(firstd.getName()) && this.getDataType() == DataType.CHAR) {
                return true;
            }
        }
        return false;
    }
    
    @Deprecated
    public boolean isUnknownLength() {
        return this.isVariableLength;
    }
    
    static {
        Variable.debugCaching = false;
        Variable.log = LoggerFactory.getLogger(Variable.class);
        Variable.showSize = false;
    }
    
    protected static class Cache
    {
        public Array data;
        public boolean isCaching;
        public boolean cachingSet;
        
        public Cache() {
            this.isCaching = false;
            this.cachingSet = false;
        }
    }
}
