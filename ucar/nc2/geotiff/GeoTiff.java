// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.geotiff;

import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Collections;
import java.nio.FloatBuffer;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.io.IOException;
import java.util.ArrayList;
import java.nio.ByteOrder;
import java.util.List;
import java.nio.channels.FileChannel;
import java.io.RandomAccessFile;

public class GeoTiff
{
    private String filename;
    private RandomAccessFile file;
    private FileChannel channel;
    private List<IFDEntry> tags;
    private ByteOrder byteOrder;
    private boolean readonly;
    private boolean showBytes;
    private boolean debugRead;
    private boolean debugReadGeoKey;
    private boolean showHeaderBytes;
    private int headerSize;
    private int firstIFD;
    private int lastIFD;
    private int startOverflowData;
    private int nextOverflowData;
    private List<GeoKey> geokeys;
    
    public GeoTiff(final String filename) {
        this.tags = new ArrayList<IFDEntry>();
        this.byteOrder = ByteOrder.BIG_ENDIAN;
        this.showBytes = false;
        this.debugRead = false;
        this.debugReadGeoKey = false;
        this.showHeaderBytes = false;
        this.headerSize = 8;
        this.firstIFD = 0;
        this.lastIFD = 0;
        this.startOverflowData = 0;
        this.nextOverflowData = 0;
        this.geokeys = new ArrayList<GeoKey>();
        this.filename = filename;
    }
    
    public void close() throws IOException {
        if (this.channel != null) {
            if (!this.readonly) {
                this.channel.force(true);
                this.channel.truncate(this.nextOverflowData);
            }
            this.channel.close();
        }
        if (this.file != null) {
            this.file.close();
        }
    }
    
    void addTag(final IFDEntry ifd) {
        this.tags.add(ifd);
    }
    
    void deleteTag(final IFDEntry ifd) {
        this.tags.remove(ifd);
    }
    
    void setTransform(final double xStart, final double yStart, final double xInc, final double yInc) {
        this.addTag(new IFDEntry(Tag.ModelTiepointTag, FieldType.DOUBLE).setValue(new double[] { 0.0, 0.0, 0.0, xStart, yStart, 0.0 }));
        this.addTag(new IFDEntry(Tag.ModelPixelScaleTag, FieldType.DOUBLE).setValue(new double[] { xInc, yInc, 0.0 }));
    }
    
    void addGeoKey(final GeoKey geokey) {
        this.geokeys.add(geokey);
    }
    
    private void writeGeoKeys() {
        if (this.geokeys.size() == 0) {
            return;
        }
        int extra_chars = 0;
        int extra_ints = 0;
        int extra_doubles = 0;
        for (final GeoKey geokey : this.geokeys) {
            if (geokey.isDouble) {
                extra_doubles += geokey.count();
            }
            else if (geokey.isString) {
                extra_chars += geokey.valueString().length() + 1;
            }
            else {
                if (geokey.count() <= 1) {
                    continue;
                }
                extra_ints += geokey.count();
            }
        }
        final int n = (this.geokeys.size() + 1) * 4;
        final int[] values = new int[n + extra_ints];
        final double[] dvalues = new double[extra_doubles];
        final char[] cvalues = new char[extra_chars];
        int icounter = n;
        int dcounter = 0;
        int ccounter = 0;
        values[values[0] = 1] = 1;
        values[2] = 0;
        values[3] = this.geokeys.size();
        int count = 4;
        for (final GeoKey geokey2 : this.geokeys) {
            values[count++] = geokey2.tagCode();
            if (geokey2.isDouble) {
                values[count++] = Tag.GeoDoubleParamsTag.getCode();
                values[count++] = geokey2.count();
                values[count++] = dcounter;
                for (int k = 0; k < geokey2.count(); ++k) {
                    dvalues[dcounter++] = geokey2.valueD(k);
                }
            }
            else if (geokey2.isString) {
                final String s = geokey2.valueString();
                values[count++] = Tag.GeoAsciiParamsTag.getCode();
                values[count++] = s.length();
                values[count++] = ccounter;
                for (int i = 0; i < s.length(); ++i) {
                    cvalues[ccounter++] = s.charAt(i);
                }
                cvalues[ccounter++] = '\0';
            }
            else if (geokey2.count() > 1) {
                values[count++] = Tag.GeoKeyDirectoryTag.getCode();
                values[count++] = geokey2.count();
                values[count++] = icounter;
                for (int k = 0; k < geokey2.count(); ++k) {
                    values[icounter++] = geokey2.value(k);
                }
            }
            else {
                values[count++] = 0;
                values[count++] = 1;
                values[count++] = geokey2.value();
            }
        }
        this.addTag(new IFDEntry(Tag.GeoKeyDirectoryTag, FieldType.SHORT).setValue(values));
        if (extra_doubles > 0) {
            this.addTag(new IFDEntry(Tag.GeoDoubleParamsTag, FieldType.DOUBLE).setValue(dvalues));
        }
        if (extra_chars > 0) {
            this.addTag(new IFDEntry(Tag.GeoAsciiParamsTag, FieldType.ASCII).setValue(new String(cvalues)));
        }
    }
    
    int writeData(final byte[] data, final int imageNumber) throws IOException {
        if (this.file == null) {
            this.init();
        }
        if (imageNumber == 1) {
            this.channel.position(this.headerSize);
        }
        else {
            this.channel.position(this.nextOverflowData);
        }
        final ByteBuffer buffer = ByteBuffer.wrap(data);
        this.channel.write(buffer);
        if (imageNumber == 1) {
            this.firstIFD = this.headerSize + data.length;
        }
        else {
            this.firstIFD = data.length + this.nextOverflowData;
        }
        return this.nextOverflowData;
    }
    
    int writeData(final float[] data, final int imageNumber) throws IOException {
        if (this.file == null) {
            this.init();
        }
        if (imageNumber == 1) {
            this.channel.position(this.headerSize);
        }
        else {
            this.channel.position(this.nextOverflowData);
        }
        final ByteBuffer direct = ByteBuffer.allocateDirect(4 * data.length);
        final FloatBuffer buffer = direct.asFloatBuffer();
        buffer.put(data);
        this.channel.write(direct);
        if (imageNumber == 1) {
            this.firstIFD = this.headerSize + 4 * data.length;
        }
        else {
            this.firstIFD = 4 * data.length + this.nextOverflowData;
        }
        return this.nextOverflowData;
    }
    
    void writeMetadata(final int imageNumber) throws IOException {
        if (this.file == null) {
            this.init();
        }
        this.writeGeoKeys();
        Collections.sort(this.tags);
        int start = 0;
        if (imageNumber == 1) {
            start = this.writeHeader(this.channel);
        }
        else {
            this.channel.position(this.lastIFD);
            final ByteBuffer buffer = ByteBuffer.allocate(4);
            if (this.debugRead) {
                System.out.println("position before writing nextIFD= " + this.channel.position() + " IFD is " + this.firstIFD);
            }
            buffer.putInt(this.firstIFD);
            buffer.flip();
            this.channel.write(buffer);
        }
        this.writeIFD(this.channel, this.firstIFD);
    }
    
    private int writeHeader(final FileChannel channel) throws IOException {
        channel.position(0L);
        final ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.put((byte)77);
        buffer.put((byte)77);
        buffer.putShort((short)42);
        buffer.putInt(this.firstIFD);
        buffer.flip();
        channel.write(buffer);
        return this.firstIFD;
    }
    
    public void initTags() throws IOException {
        this.tags = new ArrayList<IFDEntry>();
        this.geokeys = new ArrayList<GeoKey>();
    }
    
    private void init() throws IOException {
        this.file = new RandomAccessFile(this.filename, "rw");
        this.channel = this.file.getChannel();
        if (this.debugRead) {
            System.out.println("Opened file to write: '" + this.filename + "', size=" + this.channel.size());
        }
        this.readonly = false;
    }
    
    private void writeIFD(final FileChannel channel, int start) throws IOException {
        channel.position(start);
        ByteBuffer buffer = ByteBuffer.allocate(2);
        final int n = this.tags.size();
        buffer.putShort((short)n);
        buffer.flip();
        channel.write(buffer);
        start += 2;
        this.startOverflowData = start + 12 * this.tags.size() + 4;
        this.nextOverflowData = this.startOverflowData;
        for (final IFDEntry elem : this.tags) {
            this.writeIFDEntry(channel, elem, start);
            start += 12;
        }
        channel.position(this.startOverflowData - 4);
        this.lastIFD = this.startOverflowData - 4;
        if (this.debugRead) {
            System.out.println("pos before writing nextIFD= " + channel.position());
        }
        buffer = ByteBuffer.allocate(4);
        buffer.putInt(0);
        buffer.flip();
        channel.write(buffer);
    }
    
    private void writeIFDEntry(final FileChannel channel, final IFDEntry ifd, final int start) throws IOException {
        channel.position(start);
        final ByteBuffer buffer = ByteBuffer.allocate(12);
        buffer.putShort((short)ifd.tag.getCode());
        buffer.putShort((short)ifd.type.code);
        buffer.putInt(ifd.count);
        final int size = ifd.count * ifd.type.size;
        if (size <= 4) {
            for (int done = this.writeValues(buffer, ifd), k = 0; k < 4 - done; ++k) {
                buffer.put((byte)0);
            }
            buffer.flip();
            channel.write(buffer);
        }
        else {
            buffer.putInt(this.nextOverflowData);
            buffer.flip();
            channel.write(buffer);
            channel.position(this.nextOverflowData);
            final ByteBuffer vbuffer = ByteBuffer.allocate(size);
            this.writeValues(vbuffer, ifd);
            vbuffer.flip();
            channel.write(vbuffer);
            this.nextOverflowData += size;
        }
    }
    
    private int writeValues(final ByteBuffer buffer, final IFDEntry ifd) {
        int done = 0;
        if (ifd.type == FieldType.ASCII) {
            return this.writeSValue(buffer, ifd);
        }
        if (ifd.type == FieldType.RATIONAL) {
            for (int i = 0; i < ifd.count * 2; ++i) {
                done += this.writeIntValue(buffer, ifd, ifd.value[i]);
            }
        }
        else if (ifd.type == FieldType.FLOAT) {
            for (int i = 0; i < ifd.count; ++i) {
                buffer.putFloat((float)ifd.valueD[i]);
            }
            done += ifd.count * 4;
        }
        else if (ifd.type == FieldType.DOUBLE) {
            for (int i = 0; i < ifd.count; ++i) {
                buffer.putDouble(ifd.valueD[i]);
            }
            done += ifd.count * 8;
        }
        else {
            for (int i = 0; i < ifd.count; ++i) {
                done += this.writeIntValue(buffer, ifd, ifd.value[i]);
            }
        }
        return done;
    }
    
    private int writeIntValue(final ByteBuffer buffer, final IFDEntry ifd, final int v) {
        switch (ifd.type.code) {
            case 1: {
                buffer.put((byte)v);
                return 1;
            }
            case 3: {
                buffer.putShort((short)v);
                return 2;
            }
            case 4: {
                buffer.putInt(v);
                return 4;
            }
            case 5: {
                buffer.putInt(v);
                return 4;
            }
            default: {
                return 0;
            }
        }
    }
    
    private int writeSValue(final ByteBuffer buffer, final IFDEntry ifd) {
        buffer.put(ifd.valueS.getBytes());
        int size = ifd.valueS.length();
        if (size % 2 == 1) {
            ++size;
        }
        return size;
    }
    
    public void read() throws IOException {
        this.file = new RandomAccessFile(this.filename, "r");
        this.channel = this.file.getChannel();
        if (this.debugRead) {
            System.out.println("Opened file to read:'" + this.filename + "', size=" + this.channel.size());
        }
        this.readonly = true;
        int nextOffset = this.readHeader(this.channel);
        while (nextOffset > 0) {
            nextOffset = this.readIFD(this.channel, nextOffset);
            this.parseGeoInfo();
        }
    }
    
    IFDEntry findTag(final Tag tag) {
        if (tag == null) {
            return null;
        }
        for (final IFDEntry ifd : this.tags) {
            if (ifd.tag == tag) {
                return ifd;
            }
        }
        return null;
    }
    
    void testReadData() throws IOException {
        final IFDEntry tileOffsetTag = this.findTag(Tag.TileOffsets);
        if (tileOffsetTag != null) {
            final int tileOffset = tileOffsetTag.value[0];
            final IFDEntry tileSizeTag = this.findTag(Tag.TileByteCounts);
            final int tileSize = tileSizeTag.value[0];
            System.out.println("tileOffset =" + tileOffset + " tileSize=" + tileSize);
            this.testReadData(tileOffset, tileSize);
        }
        else {
            final IFDEntry stripOffsetTag = this.findTag(Tag.StripOffsets);
            if (stripOffsetTag != null) {
                final int stripOffset = stripOffsetTag.value[0];
                final IFDEntry stripSizeTag = this.findTag(Tag.StripByteCounts);
                final int stripSize = stripSizeTag.value[0];
                System.out.println("stripOffset =" + stripOffset + " stripSize=" + stripSize);
                this.testReadData(stripOffset, stripSize);
            }
        }
    }
    
    private void testReadData(final int offset, final int size) throws IOException {
        this.channel.position(offset);
        final ByteBuffer buffer = ByteBuffer.allocate(size);
        buffer.order(this.byteOrder);
        this.channel.read(buffer);
        buffer.flip();
        for (int i = 0; i < size / 4; ++i) {
            System.out.println(i + ": " + buffer.getFloat());
        }
    }
    
    private int readHeader(final FileChannel channel) throws IOException {
        channel.position(0L);
        final ByteBuffer buffer = ByteBuffer.allocate(8);
        channel.read(buffer);
        buffer.flip();
        if (this.showHeaderBytes) {
            this.printBytes(System.out, "header", buffer, 4);
            buffer.rewind();
        }
        final byte b = buffer.get();
        if (b == 73) {
            this.byteOrder = ByteOrder.LITTLE_ENDIAN;
        }
        buffer.order(this.byteOrder);
        buffer.position(4);
        final int firstIFD = buffer.getInt();
        if (this.debugRead) {
            System.out.println(" firstIFD == " + firstIFD);
        }
        return firstIFD;
    }
    
    private int readIFD(final FileChannel channel, int start) throws IOException {
        channel.position(start);
        ByteBuffer buffer = ByteBuffer.allocate(2);
        buffer.order(this.byteOrder);
        int n = channel.read(buffer);
        buffer.flip();
        if (this.showBytes) {
            this.printBytes(System.out, "IFD", buffer, 2);
            buffer.rewind();
        }
        final short nentries = buffer.getShort();
        if (this.debugRead) {
            System.out.println(" nentries = " + nentries);
        }
        start += 2;
        for (int i = 0; i < nentries; ++i) {
            final IFDEntry ifd = this.readIFDEntry(channel, start);
            if (this.debugRead) {
                System.out.println(i + " == " + ifd);
            }
            this.tags.add(ifd);
            start += 12;
        }
        if (this.debugRead) {
            System.out.println(" looking for nextIFD at pos == " + channel.position() + " start = " + start);
        }
        channel.position(start);
        buffer = ByteBuffer.allocate(4);
        buffer.order(this.byteOrder);
        n = channel.read(buffer);
        buffer.flip();
        final int nextIFD = buffer.getInt();
        if (this.debugRead) {
            System.out.println(" nextIFD == " + nextIFD);
        }
        return nextIFD;
    }
    
    private IFDEntry readIFDEntry(final FileChannel channel, final int start) throws IOException {
        if (this.debugRead) {
            System.out.println("readIFDEntry starting position to " + start);
        }
        channel.position(start);
        final ByteBuffer buffer = ByteBuffer.allocate(12);
        buffer.order(this.byteOrder);
        channel.read(buffer);
        buffer.flip();
        if (this.showBytes) {
            this.printBytes(System.out, "IFDEntry bytes", buffer, 12);
        }
        buffer.position(0);
        final int code = this.readUShortValue(buffer);
        Tag tag = Tag.get(code);
        if (tag == null) {
            tag = new Tag(code);
        }
        final FieldType type = FieldType.get(this.readUShortValue(buffer));
        final int count = buffer.getInt();
        final IFDEntry ifd = new IFDEntry(tag, type, count);
        if (ifd.count * ifd.type.size <= 4) {
            this.readValues(buffer, ifd);
        }
        else {
            final int offset = buffer.getInt();
            if (this.debugRead) {
                System.out.println("position to " + offset);
            }
            channel.position(offset);
            final ByteBuffer vbuffer = ByteBuffer.allocate(ifd.count * ifd.type.size);
            vbuffer.order(this.byteOrder);
            channel.read(vbuffer);
            vbuffer.flip();
            this.readValues(vbuffer, ifd);
        }
        return ifd;
    }
    
    private void readValues(final ByteBuffer buffer, final IFDEntry ifd) {
        if (ifd.type == FieldType.ASCII) {
            ifd.valueS = this.readSValue(buffer, ifd);
        }
        else if (ifd.type == FieldType.RATIONAL) {
            ifd.value = new int[ifd.count * 2];
            for (int i = 0; i < ifd.count * 2; ++i) {
                ifd.value[i] = this.readIntValue(buffer, ifd);
            }
        }
        else if (ifd.type == FieldType.FLOAT) {
            ifd.valueD = new double[ifd.count];
            for (int i = 0; i < ifd.count; ++i) {
                ifd.valueD[i] = buffer.getFloat();
            }
        }
        else if (ifd.type == FieldType.DOUBLE) {
            ifd.valueD = new double[ifd.count];
            for (int i = 0; i < ifd.count; ++i) {
                ifd.valueD[i] = buffer.getDouble();
            }
        }
        else {
            ifd.value = new int[ifd.count];
            for (int i = 0; i < ifd.count; ++i) {
                ifd.value[i] = this.readIntValue(buffer, ifd);
            }
        }
    }
    
    private int readIntValue(final ByteBuffer buffer, final IFDEntry ifd) {
        switch (ifd.type.code) {
            case 1: {
                return buffer.get();
            }
            case 2: {
                return buffer.get();
            }
            case 3: {
                return this.readUShortValue(buffer);
            }
            case 4: {
                return buffer.getInt();
            }
            case 5: {
                return buffer.getInt();
            }
            default: {
                return 0;
            }
        }
    }
    
    private int readUShortValue(final ByteBuffer buffer) {
        return buffer.getShort() & 0xFFFF;
    }
    
    private String readSValue(final ByteBuffer buffer, final IFDEntry ifd) {
        final byte[] dst = new byte[ifd.count];
        buffer.get(dst);
        return new String(dst);
    }
    
    private void printBytes(final PrintStream ps, final String head, final ByteBuffer buffer, final int n) {
        ps.print(head + " == ");
        for (int i = 0; i < n; ++i) {
            final byte b = buffer.get();
            final int ub = (b < 0) ? (b + 256) : b;
            ps.print(ub + "(");
            ps.write(b);
            ps.print(") ");
        }
        ps.println();
    }
    
    private void parseGeoInfo() {
        final IFDEntry keyDir = this.findTag(Tag.GeoKeyDirectoryTag);
        final IFDEntry dparms = this.findTag(Tag.GeoDoubleParamsTag);
        final IFDEntry aparams = this.findTag(Tag.GeoAsciiParamsTag);
        if (null == keyDir) {
            return;
        }
        final int nkeys = keyDir.value[3];
        if (this.debugReadGeoKey) {
            System.out.println("parseGeoInfo nkeys = " + nkeys + " keyDir= " + keyDir);
        }
        int pos = 4;
        for (int i = 0; i < nkeys; ++i) {
            final int id = keyDir.value[pos++];
            final int location = keyDir.value[pos++];
            final int vcount = keyDir.value[pos++];
            final int offset = keyDir.value[pos++];
            final GeoKey.Tag tag = GeoKey.Tag.getOrMake(id);
            GeoKey key = null;
            if (location == 0) {
                key = new GeoKey(id, offset);
            }
            else {
                final IFDEntry data = this.findTag(Tag.get(location));
                if (data == null) {
                    System.out.println("********ERROR parseGeoInfo: cant find Tag code = " + location);
                }
                else if (data.tag == Tag.GeoDoubleParamsTag) {
                    final double[] dvalue = new double[vcount];
                    for (int k = 0; k < vcount; ++k) {
                        dvalue[k] = data.valueD[offset + k];
                    }
                    key = new GeoKey(tag, dvalue);
                }
                else if (data.tag == Tag.GeoKeyDirectoryTag) {
                    final int[] value = new int[vcount];
                    for (int k = 0; k < vcount; ++k) {
                        value[k] = data.value[offset + k];
                    }
                    key = new GeoKey(tag, value);
                }
                else if (data.tag == Tag.GeoAsciiParamsTag) {
                    final String value2 = data.valueS.substring(offset, offset + vcount);
                    key = new GeoKey(tag, value2);
                }
            }
            if (key != null) {
                keyDir.addGeoKey(key);
                if (this.debugReadGeoKey) {
                    System.out.println(" yyy  add geokey=" + key);
                }
            }
        }
    }
    
    public void showInfo(final PrintStream out) {
        out.println("Geotiff file= " + this.filename);
        for (int i = 0; i < this.tags.size(); ++i) {
            final IFDEntry ifd = this.tags.get(i);
            out.println(i + " IFDEntry == " + ifd);
        }
    }
    
    public String showInfo() {
        final ByteArrayOutputStream bout = new ByteArrayOutputStream(10000);
        this.showInfo(new PrintStream(bout));
        return bout.toString();
    }
    
    public static void main(final String[] argv) {
        try {
            final GeoTiff geotiff = new GeoTiff("/home/yuanho/tmp/ilatlon_float.tif");
            geotiff.read();
            geotiff.showInfo(System.out);
            geotiff.testReadData();
            geotiff.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
