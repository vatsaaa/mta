// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.geotiff;

import java.util.HashMap;

class Tag implements Comparable
{
    private static HashMap map;
    public static final Tag NewSubfileType;
    public static final Tag ImageWidth;
    public static final Tag ImageLength;
    public static final Tag BitsPerSample;
    public static final Tag Compression;
    public static final Tag PhotometricInterpretation;
    public static final Tag FillOrder;
    public static final Tag DocumentName;
    public static final Tag ImageDescription;
    public static final Tag StripOffsets;
    public static final Tag Orientation;
    public static final Tag SamplesPerPixel;
    public static final Tag RowsPerStrip;
    public static final Tag StripByteCounts;
    public static final Tag XResolution;
    public static final Tag YResolution;
    public static final Tag PlanarConfiguration;
    public static final Tag ResolutionUnit;
    public static final Tag PageNumber;
    public static final Tag Software;
    public static final Tag ColorMap;
    public static final Tag TileWidth;
    public static final Tag TileLength;
    public static final Tag TileOffsets;
    public static final Tag TileByteCounts;
    public static final Tag SampleFormat;
    public static final Tag SMinSampleValue;
    public static final Tag SMaxSampleValue;
    public static final Tag ModelPixelScaleTag;
    public static final Tag IntergraphMatrixTag;
    public static final Tag ModelTiepointTag;
    public static final Tag ModelTransformationTag;
    public static final Tag GeoKeyDirectoryTag;
    public static final Tag GeoDoubleParamsTag;
    public static final Tag GeoAsciiParamsTag;
    public static final Tag GDALNoData;
    private String name;
    private int code;
    
    static Tag get(final int code) {
        return Tag.map.get(new Integer(code));
    }
    
    private Tag(final String name, final int code) {
        this.name = name;
        this.code = code;
        Tag.map.put(new Integer(code), this);
    }
    
    public Tag(final int code) {
        this.code = code;
    }
    
    public String getName() {
        return this.name;
    }
    
    public int getCode() {
        return this.code;
    }
    
    @Override
    public String toString() {
        return (this.name == null) ? (this.code + " ") : (this.code + " (" + this.name + ")");
    }
    
    public int compareTo(final Object o) {
        return this.code - ((Tag)o).getCode();
    }
    
    static {
        Tag.map = new HashMap();
        NewSubfileType = new Tag("NewSubfileType", 254);
        ImageWidth = new Tag("ImageWidth", 256);
        ImageLength = new Tag("ImageLength", 257);
        BitsPerSample = new Tag("BitsPerSample", 258);
        Compression = new Tag("Compression", 259);
        PhotometricInterpretation = new Tag("PhotometricInterpretation", 262);
        FillOrder = new Tag("FillOrder", 266);
        DocumentName = new Tag("DocumentName", 269);
        ImageDescription = new Tag("ImageDescription", 270);
        StripOffsets = new Tag("StripOffsets", 273);
        Orientation = new Tag("Orientation", 274);
        SamplesPerPixel = new Tag("SamplesPerPixel", 277);
        RowsPerStrip = new Tag("RowsPerStrip", 278);
        StripByteCounts = new Tag("StripByteCounts", 279);
        XResolution = new Tag("XResolution", 282);
        YResolution = new Tag("YResolution", 283);
        PlanarConfiguration = new Tag("PlanarConfiguration", 284);
        ResolutionUnit = new Tag("ResolutionUnit", 296);
        PageNumber = new Tag("PageNumber", 297);
        Software = new Tag("Software", 305);
        ColorMap = new Tag("ColorMap", 320);
        TileWidth = new Tag("TileWidth", 322);
        TileLength = new Tag("TileLength", 323);
        TileOffsets = new Tag("TileOffsets", 324);
        TileByteCounts = new Tag("TileByteCounts", 325);
        SampleFormat = new Tag("SampleFormat", 339);
        SMinSampleValue = new Tag("SMinSampleValue", 340);
        SMaxSampleValue = new Tag("SMaxSampleValue", 341);
        ModelPixelScaleTag = new Tag("ModelPixelScaleTag", 33550);
        IntergraphMatrixTag = new Tag("IntergraphMatrixTag", 33920);
        ModelTiepointTag = new Tag("ModelTiepointTag", 33922);
        ModelTransformationTag = new Tag("ModelTransformationTag", 34264);
        GeoKeyDirectoryTag = new Tag("GeoKeyDirectoryTag", 34735);
        GeoDoubleParamsTag = new Tag("GeoDoubleParamsTag", 34736);
        GeoAsciiParamsTag = new Tag("GeoAsciiParamsTag", 34737);
        GDALNoData = new Tag("GDALNoDataTag", 42113);
    }
}
