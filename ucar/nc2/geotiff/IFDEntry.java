// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.geotiff;

import java.util.ArrayList;

class IFDEntry implements Comparable
{
    protected Tag tag;
    protected FieldType type;
    protected int count;
    protected int[] value;
    protected double[] valueD;
    protected String valueS;
    protected ArrayList geokeys;
    
    public IFDEntry(final Tag tag, final FieldType type) {
        this.geokeys = null;
        this.tag = tag;
        this.type = type;
        this.count = 1;
    }
    
    public IFDEntry(final Tag tag, final FieldType type, final int count) {
        this.geokeys = null;
        this.tag = tag;
        this.type = type;
        this.count = count;
    }
    
    public IFDEntry setValue(final int v) {
        (this.value = new int[1])[0] = v;
        return this;
    }
    
    public IFDEntry setValue(final int n, final int d) {
        (this.value = new int[2])[0] = n;
        this.value[1] = d;
        return this;
    }
    
    public IFDEntry setValue(final int n, final int d, final int f) {
        (this.value = new int[3])[0] = n;
        this.value[1] = d;
        this.value[2] = f;
        return this;
    }
    
    public IFDEntry setValue(final int[] v) {
        this.count = v.length;
        this.value = v.clone();
        return this;
    }
    
    public IFDEntry setValue(final double v) {
        this.count = 1;
        (this.valueD = new double[1])[0] = v;
        return this;
    }
    
    public IFDEntry setValue(final double[] v) {
        this.count = v.length;
        this.valueD = v.clone();
        return this;
    }
    
    public IFDEntry setValue(final String v) {
        this.count = v.length();
        this.valueS = v;
        return this;
    }
    
    public void addGeoKey(final GeoKey geokey) {
        if (this.geokeys == null) {
            this.geokeys = new ArrayList();
        }
        this.geokeys.add(geokey);
    }
    
    public int compareTo(final Object o) {
        return this.tag.compareTo(((IFDEntry)o).tag);
    }
    
    @Override
    public String toString() {
        final StringBuilder sbuf = new StringBuilder();
        sbuf.append(" tag = " + this.tag);
        sbuf.append(" type = " + this.type);
        sbuf.append(" count = " + this.count);
        sbuf.append(" values = ");
        if (this.type == FieldType.ASCII) {
            sbuf.append(this.valueS);
        }
        else if (this.type == FieldType.RATIONAL) {
            for (int i = 0; i < 2; i += 2) {
                if (i > 1) {
                    sbuf.append(", ");
                }
                sbuf.append(this.value[i] + "/" + this.value[i + 1]);
            }
        }
        else if (this.type == FieldType.DOUBLE || this.type == FieldType.FLOAT) {
            for (int i = 0; i < this.count; ++i) {
                sbuf.append(this.valueD[i] + " ");
            }
        }
        else {
            for (int n = Math.min(this.count, 30), j = 0; j < n; ++j) {
                sbuf.append(this.value[j] + " ");
            }
        }
        if (this.geokeys != null) {
            sbuf.append("\n");
            for (int i = 0; i < this.geokeys.size(); ++i) {
                final GeoKey elem = this.geokeys.get(i);
                sbuf.append("        " + elem + "\n");
            }
        }
        return sbuf.toString();
    }
}
