// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.geotiff;

import java.util.HashMap;

class GeoKey
{
    boolean isDouble;
    boolean isString;
    private int count;
    private int[] value;
    private double[] dvalue;
    private String valueS;
    private Tag tag;
    private TagValue tagValue;
    private int id;
    
    GeoKey(final Tag tag, final TagValue tagValue) {
        this.isDouble = false;
        this.isString = false;
        this.tag = tag;
        this.tagValue = tagValue;
        this.count = 1;
    }
    
    GeoKey(final Tag tag, final int value) {
        this.isDouble = false;
        this.isString = false;
        this.tag = tag;
        (this.value = new int[1])[0] = value;
        this.count = 1;
    }
    
    GeoKey(final Tag tag, final int[] value) {
        this.isDouble = false;
        this.isString = false;
        this.tag = tag;
        this.value = value;
        this.count = value.length;
    }
    
    GeoKey(final Tag tag, final double[] value) {
        this.isDouble = false;
        this.isString = false;
        this.tag = tag;
        this.dvalue = value;
        this.count = value.length;
        this.isDouble = true;
    }
    
    GeoKey(final Tag tag, final double value) {
        this.isDouble = false;
        this.isString = false;
        this.tag = tag;
        (this.dvalue = new double[1])[0] = value;
        this.count = 1;
        this.isDouble = true;
    }
    
    GeoKey(final Tag tag, final String value) {
        this.isDouble = false;
        this.isString = false;
        this.tag = tag;
        this.valueS = value;
        this.count = 1;
        this.isString = true;
    }
    
    int count() {
        return this.count;
    }
    
    int tagCode() {
        if (this.tag != null) {
            return this.tag.code();
        }
        return this.id;
    }
    
    int value() {
        if (this.tagValue != null) {
            return this.tagValue.value();
        }
        return this.value[0];
    }
    
    int value(final int idx) {
        if (idx == 0) {
            return this.value();
        }
        return this.value[idx];
    }
    
    double valueD(final int idx) {
        return this.dvalue[idx];
    }
    
    String valueString() {
        return this.valueS;
    }
    
    GeoKey(final int id, final int v) {
        this.isDouble = false;
        this.isString = false;
        this.id = id;
        this.count = 1;
        this.tag = Tag.get(id);
        this.tagValue = TagValue.get(this.tag, v);
        if (this.tagValue == null) {
            (this.value = new int[1])[0] = v;
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sbuf = new StringBuilder();
        if (this.tag != null) {
            sbuf.append(" geoKey = " + this.tag);
        }
        else {
            sbuf.append(" geoKey = " + this.id);
        }
        if (this.tagValue != null) {
            sbuf.append(" value = " + this.tagValue);
        }
        else {
            sbuf.append(" values = ");
            if (this.valueS != null) {
                sbuf.append(this.valueS);
            }
            else if (this.isDouble) {
                for (int i = 0; i < this.count; ++i) {
                    sbuf.append(this.dvalue[i] + " ");
                }
            }
            else {
                for (int i = 0; i < this.count; ++i) {
                    sbuf.append(this.value[i] + " ");
                }
            }
        }
        return sbuf.toString();
    }
    
    static class Tag implements Comparable
    {
        private static HashMap map;
        public static final Tag GTModelTypeGeoKey;
        public static final Tag GTRasterTypeGeoKey;
        public static final Tag GTCitationGeoKey;
        public static final Tag GeographicTypeGeoKey;
        public static final Tag GeogCitationGeoKey;
        public static final Tag GeogGeodeticDatumGeoKey;
        public static final Tag GeogPrimeMeridianGeoKey;
        public static final Tag GeogLinearUnitsGeoKey;
        public static final Tag GeogAngularUnitsGeoKey;
        public static final Tag GeogAngularUnitsSizeGeoKey;
        public static final Tag GeogSemiMajorAxisGeoKey;
        public static final Tag GeogSemiMinorAxisGeoKey;
        public static final Tag GeogInvFlatteningGeoKey;
        public static final Tag GeogAzimuthUnitsGeoKey;
        public static final Tag ProjectedCSTypeGeoKey;
        public static final Tag PCSCitationGeoKey;
        public static final Tag ProjectionGeoKey;
        public static final Tag ProjCoordTransGeoKey;
        public static final Tag ProjLinearUnitsGeoKey;
        public static final Tag ProjLinearUnitsSizeGeoKey;
        public static final Tag ProjStdParallel1GeoKey;
        public static final Tag ProjStdParallel2GeoKey;
        public static final Tag ProjNatOriginLongGeoKey;
        public static final Tag ProjNatOriginLatGeoKey;
        public static final Tag ProjFalseEastingGeoKey;
        public static final Tag ProjFalseNorthingGeoKey;
        public static final Tag ProjFalseOriginLongGeoKey;
        public static final Tag ProjFalseOriginLatGeoKey;
        public static final Tag ProjFalseOriginEastingGeoKey;
        public static final Tag ProjFalseOriginNorthingGeoKey;
        public static final Tag ProjCenterLongGeoKey;
        public static final Tag ProjCenterLatGeoKey;
        public static final Tag ProjCenterEastingGeoKey;
        public static final Tag ProjCenterNorthingGeoKey;
        public static final Tag ProjScaleAtNatOriginGeoKey;
        public static final Tag ProjScaleAtCenterGeoKey;
        public static final Tag ProjAzimuthAngleGeoKey;
        public static final Tag ProjStraightVertPoleLongGeoKey;
        public static final Tag VerticalCSTypeGeoKey;
        public static final Tag VerticalCitationGeoKey;
        public static final Tag VerticalDatumGeoKey;
        public static final Tag VerticalUnitsGeoKey;
        public static final Tag GeoKey_ProjCoordTrans;
        public static final Tag GeoKey_ProjStdParallel1;
        public static final Tag GeoKey_ProjStdParallel2;
        public static final Tag GeoKey_ProjNatOriginLong;
        public static final Tag GeoKey_ProjNatOriginLat;
        public static final Tag GeoKey_ProjCenterLong;
        public static final Tag GeoKey_ProjFalseEasting;
        public static final Tag GeoKey_ProjFalseNorthing;
        public static final Tag GeoKey_ProjFalseOriginLong;
        public static final Tag GeoKey_ProjFalseOriginLat;
        String name;
        int code;
        
        static Tag get(final int code) {
            return Tag.map.get(new Integer(code));
        }
        
        static Tag getOrMake(final int code) {
            final Tag tag = get(code);
            return (tag != null) ? tag : new Tag(code);
        }
        
        private Tag(final String name, final int code) {
            this.name = name;
            this.code = code;
            Tag.map.put(new Integer(code), this);
        }
        
        public Tag(final int code) {
            this.code = code;
        }
        
        public int code() {
            return this.code;
        }
        
        @Override
        public String toString() {
            return (this.name == null) ? (this.code + " ") : (this.code + " (" + this.name + ")");
        }
        
        public int compareTo(final Object o) {
            if (!(o instanceof Tag)) {
                return 0;
            }
            final Tag to = (Tag)o;
            return this.code - to.code;
        }
        
        static {
            Tag.map = new HashMap();
            GTModelTypeGeoKey = new Tag("GTModelTypeGeoKey", 1024);
            GTRasterTypeGeoKey = new Tag("GTRasterTypeGeoKey", 1025);
            GTCitationGeoKey = new Tag("GTCitationGeoKey", 1026);
            GeographicTypeGeoKey = new Tag("GeographicTypeGeoKey", 2048);
            GeogCitationGeoKey = new Tag("GeogCitationGeoKey", 2049);
            GeogGeodeticDatumGeoKey = new Tag("GeogGeodeticDatumGeoKey", 2050);
            GeogPrimeMeridianGeoKey = new Tag("GeogPrimeMeridianGeoKey", 2051);
            GeogLinearUnitsGeoKey = new Tag("GeogLinearUnitsGeoKey", 2052);
            GeogAngularUnitsGeoKey = new Tag("GeogAngularUnitsGeoKey", 2054);
            GeogAngularUnitsSizeGeoKey = new Tag("GeogAngularUnitsSizeGeoKey", 2055);
            GeogSemiMajorAxisGeoKey = new Tag("GeogSemiMajorAxisGeoKey", 2056);
            GeogSemiMinorAxisGeoKey = new Tag("GeogSemiMinorAxisGeoKey", 2057);
            GeogInvFlatteningGeoKey = new Tag("GeogInvFlatteningGeoKey", 2058);
            GeogAzimuthUnitsGeoKey = new Tag("GeogAzimuthUnitsGeoKey", 2060);
            ProjectedCSTypeGeoKey = new Tag("ProjectedCSTypeGeoKey,", 3072);
            PCSCitationGeoKey = new Tag("PCSCitationGeoKey,", 3073);
            ProjectionGeoKey = new Tag("ProjectionGeoKey,", 3074);
            ProjCoordTransGeoKey = new Tag("ProjCoordTransGeoKey", 3075);
            ProjLinearUnitsGeoKey = new Tag("ProjLinearUnitsGeoKey", 3076);
            ProjLinearUnitsSizeGeoKey = new Tag("ProjLinearUnitsSizeGeoKey", 3077);
            ProjStdParallel1GeoKey = new Tag("ProjStdParallel1GeoKey", 3078);
            ProjStdParallel2GeoKey = new Tag("ProjStdParallel2GeoKey", 3079);
            ProjNatOriginLongGeoKey = new Tag("ProjNatOriginLongGeoKey", 3080);
            ProjNatOriginLatGeoKey = new Tag("ProjNatOriginLatGeoKey", 3081);
            ProjFalseEastingGeoKey = new Tag("ProjFalseEastingGeoKey", 3082);
            ProjFalseNorthingGeoKey = new Tag("ProjFalseNorthingGeoKey", 3083);
            ProjFalseOriginLongGeoKey = new Tag("ProjFalseOriginLongGeoKey", 3084);
            ProjFalseOriginLatGeoKey = new Tag("ProjFalseOriginLatGeoKey", 3085);
            ProjFalseOriginEastingGeoKey = new Tag("ProjFalseOriginEastingGeoKey", 3086);
            ProjFalseOriginNorthingGeoKey = new Tag("ProjFalseOriginNorthingGeoKey", 3087);
            ProjCenterLongGeoKey = new Tag("ProjCenterLongGeoKey", 3088);
            ProjCenterLatGeoKey = new Tag("ProjCenterLatGeoKey", 3089);
            ProjCenterEastingGeoKey = new Tag("ProjCenterEastingGeoKey", 3090);
            ProjCenterNorthingGeoKey = new Tag("ProjCenterNorthingGeoKey", 3091);
            ProjScaleAtNatOriginGeoKey = new Tag("ProjScaleAtNatOriginGeoKey", 3092);
            ProjScaleAtCenterGeoKey = new Tag("ProjScaleAtCenterGeoKey", 3093);
            ProjAzimuthAngleGeoKey = new Tag("ProjAzimuthAngleGeoKey", 3094);
            ProjStraightVertPoleLongGeoKey = new Tag("ProjStraightVertPoleLongGeoKey", 3095);
            VerticalCSTypeGeoKey = new Tag("VerticalCSTypeGeoKey", 4096);
            VerticalCitationGeoKey = new Tag("VerticalCitationGeoKey", 4097);
            VerticalDatumGeoKey = new Tag("VerticalDatumGeoKey", 4098);
            VerticalUnitsGeoKey = new Tag("VerticalUnitsGeoKey", 4099);
            GeoKey_ProjCoordTrans = new Tag("GeoKey_ProjCoordTrans", 3075);
            GeoKey_ProjStdParallel1 = new Tag("GeoKey_ProjStdParallel1", 3078);
            GeoKey_ProjStdParallel2 = new Tag("GeoKey_ProjStdParallel2", 3079);
            GeoKey_ProjNatOriginLong = new Tag("GeoKey_ProjNatOriginLong", 3080);
            GeoKey_ProjNatOriginLat = new Tag("GeoKey_ProjNatOriginLat", 3081);
            GeoKey_ProjCenterLong = new Tag("GeoKey_ProjCenterLong", 3088);
            GeoKey_ProjFalseEasting = new Tag("GeoKey_ProjFalseEasting", 3082);
            GeoKey_ProjFalseNorthing = new Tag("GeoKey_ProjFalseNorthing", 3083);
            GeoKey_ProjFalseOriginLong = new Tag("GeoKey_ProjFalseOriginLong", 3084);
            GeoKey_ProjFalseOriginLat = new Tag("GeoKey_ProjFalseOriginLat", 3085);
        }
    }
    
    static class TagValue implements Comparable
    {
        private static HashMap map;
        public static final TagValue ModelType_Projected;
        public static final TagValue ModelType_Geographic;
        public static final TagValue ModelType_Geocentric;
        public static final TagValue RasterType_Area;
        public static final TagValue RasterType_Point;
        public static final TagValue GeographicType_Clarke1866;
        public static final TagValue GeographicType_GRS_80;
        public static final TagValue GeographicType_Sphere;
        public static final TagValue GeographicType_NAD83;
        public static final TagValue GeographicType_WGS_84;
        public static final TagValue GeographicType_GCS_NAD27;
        public static final TagValue GeogGeodeticDatum_WGS_84;
        public static final TagValue GeogPrimeMeridian_GREENWICH;
        public static final TagValue ProjectedCSType_UserDefined;
        public static final TagValue ProjCoordTrans_LambertConfConic_2SP;
        public static final TagValue ProjCoordTrans_LambertConfConic_1SP;
        public static final TagValue ProjCoordTrans_Stereographic;
        public static final TagValue ProjCoordTrans_TransverseMercator;
        public static final TagValue ProjCoordTrans_AlbersConicalEqualArea;
        public static final TagValue ProjCoordTrans_AlbersEqualAreaEllipse;
        public static final TagValue ProjCoordTrans_Mercator;
        public static final TagValue ProjLinearUnits_METER;
        public static final TagValue GeogAngularUnits_DEGREE;
        public static final TagValue GeogGeodeticDatum6267;
        private Tag tag;
        private String name;
        private int value;
        
        static TagValue get(final Tag tag, final int code) {
            if (tag == null) {
                return null;
            }
            return TagValue.map.get(tag.name + code);
        }
        
        private TagValue(final Tag tag, final String name, final int value) {
            this.tag = tag;
            this.name = name;
            this.value = value;
            TagValue.map.put(tag.name + value, this);
        }
        
        public Tag tag() {
            return this.tag;
        }
        
        public int value() {
            return this.value;
        }
        
        @Override
        public String toString() {
            return this.value + " (" + this.name + ")";
        }
        
        public int compareTo(final Object o) {
            if (!(o instanceof TagValue)) {
                return 0;
            }
            final int ret = this.tag.compareTo(o);
            if (ret != 0) {
                return ret;
            }
            return this.value - ((TagValue)o).value;
        }
        
        static {
            TagValue.map = new HashMap();
            ModelType_Projected = new TagValue(Tag.GTModelTypeGeoKey, "Projected", 1);
            ModelType_Geographic = new TagValue(Tag.GTModelTypeGeoKey, "Geographic", 2);
            ModelType_Geocentric = new TagValue(Tag.GTModelTypeGeoKey, "Geocentric", 3);
            RasterType_Area = new TagValue(Tag.GTRasterTypeGeoKey, "Area", 1);
            RasterType_Point = new TagValue(Tag.GTRasterTypeGeoKey, "Point", 2);
            GeographicType_Clarke1866 = new TagValue(Tag.GeographicTypeGeoKey, "Clarke1866", 4008);
            GeographicType_GRS_80 = new TagValue(Tag.GeographicTypeGeoKey, "GRS_80", 4019);
            GeographicType_Sphere = new TagValue(Tag.GeographicTypeGeoKey, "Sphere", 4035);
            GeographicType_NAD83 = new TagValue(Tag.GeographicTypeGeoKey, "GCS_NAD83", 4269);
            GeographicType_WGS_84 = new TagValue(Tag.GeographicTypeGeoKey, "WGS_84", 4326);
            GeographicType_GCS_NAD27 = new TagValue(Tag.GeographicTypeGeoKey, "GCS_NAD27", 4267);
            GeogGeodeticDatum_WGS_84 = new TagValue(Tag.GeogGeodeticDatumGeoKey, "WGS_84", 4326);
            GeogPrimeMeridian_GREENWICH = new TagValue(Tag.GeogPrimeMeridianGeoKey, "Greenwich", 8901);
            ProjectedCSType_UserDefined = new TagValue(Tag.ProjectedCSTypeGeoKey, "UserDefined", 32767);
            ProjCoordTrans_LambertConfConic_2SP = new TagValue(Tag.ProjCoordTransGeoKey, "LambertConfConic_2SP", 8);
            ProjCoordTrans_LambertConfConic_1SP = new TagValue(Tag.ProjCoordTransGeoKey, "LambertConfConic_1SP", 9);
            ProjCoordTrans_Stereographic = new TagValue(Tag.ProjCoordTransGeoKey, "Stereographic", 14);
            ProjCoordTrans_TransverseMercator = new TagValue(Tag.ProjCoordTransGeoKey, "TransverseMercator", 1);
            ProjCoordTrans_AlbersConicalEqualArea = new TagValue(Tag.ProjCoordTransGeoKey, "AlbersConicalEqualArea", 11);
            ProjCoordTrans_AlbersEqualAreaEllipse = new TagValue(Tag.ProjCoordTransGeoKey, "AlbersEqualAreaEllipse", 11);
            ProjCoordTrans_Mercator = new TagValue(Tag.ProjCoordTransGeoKey, "Mercator", 7);
            ProjLinearUnits_METER = new TagValue(Tag.ProjLinearUnitsGeoKey, "Meter", 9001);
            GeogAngularUnits_DEGREE = new TagValue(Tag.GeogAngularUnitsGeoKey, "Degree", 9102);
            GeogGeodeticDatum6267 = new TagValue(Tag.GeogGeodeticDatumGeoKey, "North_American_1927", 6267);
        }
    }
}
