// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.geotiff;

class FieldType
{
    private static FieldType[] types;
    public static final FieldType BYTE;
    public static final FieldType ASCII;
    public static final FieldType SHORT;
    public static final FieldType LONG;
    public static final FieldType RATIONAL;
    public static final FieldType SBYTE;
    public static final FieldType UNDEFINED;
    public static final FieldType SSHORT;
    public static final FieldType SLONG;
    public static final FieldType SRATIONAL;
    public static final FieldType FLOAT;
    public static final FieldType DOUBLE;
    String name;
    int code;
    int size;
    
    static FieldType get(final int code) {
        return FieldType.types[code];
    }
    
    private FieldType(final String name, final int code, final int size) {
        this.name = name;
        this.code = code;
        this.size = size;
        FieldType.types[code] = this;
    }
    
    @Override
    public String toString() {
        return this.name;
    }
    
    static {
        FieldType.types = new FieldType[20];
        BYTE = new FieldType("BYTE", 1, 1);
        ASCII = new FieldType("ASCII", 2, 1);
        SHORT = new FieldType("SHORT", 3, 2);
        LONG = new FieldType("LONG", 4, 4);
        RATIONAL = new FieldType("RATIONAL", 5, 8);
        SBYTE = new FieldType("SBYTE", 6, 1);
        UNDEFINED = new FieldType("UNDEFINED", 7, 1);
        SSHORT = new FieldType("SSHORT", 8, 2);
        SLONG = new FieldType("SLONG", 9, 4);
        SRATIONAL = new FieldType("SRATIONAL", 10, 8);
        FLOAT = new FieldType("FLOAT", 11, 4);
        DOUBLE = new FieldType("DOUBLE", 12, 8);
    }
}
