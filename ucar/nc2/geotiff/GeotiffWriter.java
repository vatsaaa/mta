// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.geotiff;

import ucar.ma2.MAMath;
import ucar.ma2.ArrayFloat;
import ucar.ma2.ArrayByte;
import ucar.unidata.geoloc.projection.TransverseMercator;
import ucar.unidata.geoloc.projection.AlbersEqualArea;
import ucar.unidata.geoloc.projection.proj4.AlbersEqualAreaEllipse;
import ucar.unidata.geoloc.projection.Mercator;
import ucar.unidata.geoloc.projection.Stereographic;
import ucar.unidata.geoloc.projection.LambertConformal;
import ucar.ma2.DataType;
import ucar.ma2.Index;
import ucar.ma2.IndexIterator;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.nc2.dataset.CoordinateAxis2D;
import ucar.unidata.geoloc.ProjectionPoint;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.nc2.Attribute;
import ucar.unidata.geoloc.ProjectionImpl;
import ucar.unidata.geoloc.LatLonRect;
import java.io.IOException;
import ucar.nc2.dt.GridCoordSystem;
import ucar.nc2.dataset.CoordinateAxis1D;
import ucar.ma2.Array;
import ucar.nc2.dt.GridDatatype;
import ucar.nc2.dt.GridDataset;

public class GeotiffWriter
{
    private String fileOut;
    private GeoTiff geotiff;
    private short pageNumber;
    
    public GeotiffWriter(final String fileOut) {
        this.pageNumber = 1;
        this.fileOut = fileOut;
        this.geotiff = new GeoTiff(fileOut);
    }
    
    public void writeGrid(final GridDataset dataset, final GridDatatype grid, Array data, final boolean greyScale) throws IOException {
        final GridCoordSystem gcs = grid.getCoordinateSystem();
        if (!gcs.isRegularSpatial()) {
            throw new IllegalArgumentException("Must have 1D x and y axes for " + grid.getName());
        }
        final CoordinateAxis1D xaxis = (CoordinateAxis1D)gcs.getXHorizAxis();
        final CoordinateAxis1D yaxis = (CoordinateAxis1D)gcs.getYHorizAxis();
        final double scaler = gcs.isLatLon() ? 1.0 : 1000.0;
        double xStart = xaxis.getCoordValue(0) * scaler;
        double yStart = yaxis.getCoordValue(0) * scaler;
        final double xInc = xaxis.getIncrement() * scaler;
        final double yInc = Math.abs(yaxis.getIncrement()) * scaler;
        if (yaxis.getCoordValue(0) < yaxis.getCoordValue(1)) {
            data = data.flip(0);
            yStart = yaxis.getCoordValue((int)yaxis.getSize() - 1) * scaler;
        }
        if (gcs.isLatLon()) {
            final Array lon = xaxis.read();
            data = this.geoShiftDataAtLon(data, lon);
            xStart = this.geoShiftGetXstart(lon, xInc);
        }
        if (!xaxis.isRegular() || !yaxis.isRegular()) {
            throw new IllegalArgumentException("Must be evenly spaced grid = " + grid.getName());
        }
        if (this.pageNumber > 1) {
            this.geotiff.initTags();
        }
        this.writeGrid(grid, data, greyScale, xStart, yStart, xInc, yInc, this.pageNumber);
        ++this.pageNumber;
    }
    
    public void writeGrid(final String fileName, final String gridName, final int time, final int level, final boolean greyScale, final LatLonRect pt) throws IOException {
        final GridDataset dataset = ucar.nc2.dt.grid.GridDataset.open(fileName);
        final GridDatatype grid = dataset.findGridDatatype(gridName);
        final GridCoordSystem gcs = grid.getCoordinateSystem();
        final ProjectionImpl proj = grid.getProjection();
        if (grid == null) {
            throw new IllegalArgumentException("No grid named " + gridName + " in fileName");
        }
        if (!gcs.isRegularSpatial()) {
            final Attribute att = dataset.findGlobalAttributeIgnoreCase("datasetId");
            if (att != null && att.getStringValue().contains("DMSP")) {
                this.writeSwathGrid(fileName, gridName, time, level, greyScale, pt);
                return;
            }
            throw new IllegalArgumentException("Must have 1D x and y axes for " + grid.getName());
        }
        else {
            final CoordinateAxis1D xaxis = (CoordinateAxis1D)gcs.getXHorizAxis();
            final CoordinateAxis1D yaxis = (CoordinateAxis1D)gcs.getYHorizAxis();
            if (!xaxis.isRegular() || !yaxis.isRegular()) {
                throw new IllegalArgumentException("Must be evenly spaced grid = " + grid.getName());
            }
            Array data = grid.readDataSlice(time, level, -1, -1);
            Array lon = xaxis.read();
            Array lat = yaxis.read();
            double scaler;
            if (gcs.isLatLon()) {
                scaler = 1.0;
            }
            else {
                scaler = 1000.0;
            }
            if (yaxis.getCoordValue(0) < yaxis.getCoordValue(1)) {
                data = data.flip(0);
                lat = lat.flip(0);
            }
            if (gcs.isLatLon()) {
                data = this.geoShiftDataAtLon(data, lon);
                lon = this.geoShiftLon(lon);
            }
            final LatLonPointImpl llp0 = pt.getLowerLeftPoint();
            final LatLonPointImpl llpn = pt.getUpperRightPoint();
            final double minLon = llp0.getLongitude();
            final double minLat = llp0.getLatitude();
            final double maxLon = llpn.getLongitude();
            final double maxLat = llpn.getLatitude();
            int x1;
            int y1;
            double yStart;
            double xStart;
            int x2;
            int y2;
            if (!gcs.isLatLon()) {
                final ProjectionPoint pjp0 = proj.latLonToProj(maxLat, minLon);
                final double[] lonArray = (double[])lon.copyTo1DJavaArray();
                final double[] latArray = (double[])lat.copyTo1DJavaArray();
                x1 = this.getXIndex(lon, pjp0.getX(), 0);
                y1 = this.getYIndex(lat, pjp0.getY(), 0);
                yStart = pjp0.getY() * 1000.0;
                xStart = pjp0.getX() * 1000.0;
                final ProjectionPoint pjpn = proj.latLonToProj(minLat, maxLon);
                x2 = this.getXIndex(lon, pjpn.getX(), 1);
                y2 = this.getYIndex(lat, pjpn.getY(), 1);
            }
            else {
                xStart = minLon;
                yStart = maxLat;
                x1 = this.getLonIndex(lon, minLon, 0);
                y1 = this.getLatIndex(lat, maxLat, 0);
                x2 = this.getLonIndex(lon, maxLon, 1);
                y2 = this.getLatIndex(lat, minLat, 1);
            }
            final double xInc = xaxis.getIncrement() * scaler;
            final double yInc = Math.abs(yaxis.getIncrement()) * scaler;
            final Array data2 = this.getYXDataInBox(data, x1, x2, y1, y2);
            if (this.pageNumber > 1) {
                this.geotiff.initTags();
            }
            this.writeGrid(grid, data2, greyScale, xStart, yStart, xInc, yInc, this.pageNumber);
            ++this.pageNumber;
        }
    }
    
    public void writeSwathGrid(final String fileName, final String gridName, final int time, final int level, final boolean greyScale, final LatLonRect llr) throws IOException {
        final GridDataset dataset = ucar.nc2.dt.grid.GridDataset.open(fileName);
        final GridDatatype grid = dataset.findGridDatatype(gridName);
        final GridCoordSystem gcs = grid.getCoordinateSystem();
        final ProjectionImpl proj = grid.getProjection();
        final CoordinateAxis2D xaxis = (CoordinateAxis2D)gcs.getXHorizAxis();
        final CoordinateAxis2D yaxis = (CoordinateAxis2D)gcs.getYHorizAxis();
        Array data = grid.readDataSlice(time, level, -1, -1);
        Array lon = xaxis.read();
        final Array lat = yaxis.read();
        final double[] swathInfo = this.getSwathLatLonInformation(lat, lon);
        double scaler;
        if (gcs.isLatLon()) {
            scaler = 1.0;
        }
        else {
            scaler = 1000.0;
        }
        data = data.flip(0);
        if (gcs.isLatLon()) {
            data = this.geoShiftDataAtLon(data, lon);
            lon = this.geoShiftLon(lon);
        }
        final double xInc = swathInfo[0] * scaler;
        final double yInc = swathInfo[1] * scaler;
        double minLon;
        double minLat;
        double maxLon;
        double maxLat;
        double xStart;
        double yStart;
        int x1;
        int y1;
        int x2;
        int y2;
        if (llr == null) {
            minLon = swathInfo[4];
            minLat = swathInfo[2];
            maxLon = swathInfo[5];
            maxLat = swathInfo[3];
            xStart = minLon;
            yStart = maxLat;
            x1 = 0;
            y1 = 0;
            x2 = (int)((maxLon - minLon) / xInc + 0.5);
            y2 = (int)((maxLat - minLat) / yInc + 0.5);
        }
        else {
            final LatLonPointImpl llp0 = llr.getLowerLeftPoint();
            final LatLonPointImpl llpn = llr.getUpperRightPoint();
            minLon = ((llp0.getLongitude() < swathInfo[4]) ? swathInfo[4] : llp0.getLongitude());
            minLat = ((llp0.getLatitude() < swathInfo[2]) ? swathInfo[2] : llp0.getLatitude());
            maxLon = ((llpn.getLongitude() > swathInfo[5]) ? swathInfo[5] : llpn.getLongitude());
            maxLat = ((llpn.getLatitude() > swathInfo[3]) ? swathInfo[3] : llpn.getLatitude());
            final LatLonPointImpl pUpLeft = new LatLonPointImpl(swathInfo[3], swathInfo[4]);
            final LatLonPointImpl pDownRight = new LatLonPointImpl(swathInfo[2], swathInfo[5]);
            final LatLonRect swathLLR = new LatLonRect(pUpLeft, pDownRight);
            final LatLonRect bIntersect = swathLLR.intersect(llr);
            if (bIntersect == null) {
                throw new IllegalArgumentException("The assigned extent of latitude and longitude is unvalid. No intersection with the swath extent");
            }
            xStart = minLon;
            yStart = maxLat;
            x1 = (int)((minLon - swathInfo[4]) / xInc + 0.5);
            y1 = (int)Math.abs((maxLat - swathInfo[3]) / yInc + 0.5);
            x2 = (int)((maxLon - swathInfo[4]) / xInc + 0.5);
            y2 = (int)Math.abs((minLat - swathInfo[3]) / yInc + 0.5);
        }
        if (!gcs.isLatLon()) {
            final ProjectionPoint pjp0 = proj.latLonToProj(maxLat, minLon);
            x1 = this.getXIndex(lon, pjp0.getX(), 0);
            y1 = this.getYIndex(lat, pjp0.getY(), 0);
            yStart = pjp0.getY() * 1000.0;
            xStart = pjp0.getX() * 1000.0;
            final ProjectionPoint pjpn = proj.latLonToProj(minLat, maxLon);
            x2 = this.getXIndex(lon, pjpn.getX(), 1);
            y2 = this.getYIndex(lat, pjpn.getY(), 1);
        }
        final Array targetImage = this.getTargetImagerFromSwath(lat, lon, data, swathInfo);
        final Array interpolatedImage = this.interpolation(targetImage);
        final Array clippedImage = this.getClippedImageFrominterpolation(interpolatedImage, x1, x2, y1, y2);
        if (this.pageNumber > 1) {
            this.geotiff.initTags();
        }
        this.writeGrid(grid, clippedImage, greyScale, xStart, yStart, xInc, yInc, this.pageNumber);
        ++this.pageNumber;
    }
    
    int getXIndex(final Array aAxis, final double value, final int side) {
        final IndexIterator aIter = aAxis.getIndexIterator();
        int count = 0;
        int isInd = 0;
        double aValue = aIter.getFloatNext();
        if (aValue == value || aValue > value) {
            return 0;
        }
        while (aIter.hasNext() && aValue < value) {
            ++count;
            aValue = aIter.getFloatNext();
            if (aValue == value) {
                isInd = 1;
            }
        }
        if (isInd == 1) {
            count += side;
        }
        count -= side;
        return count;
    }
    
    int getYIndex(final Array aAxis, final double value, final int side) {
        final IndexIterator aIter = aAxis.getIndexIterator();
        int count = 0;
        int isInd = 0;
        double aValue = aIter.getFloatNext();
        if (aValue == value || aValue < value) {
            return 0;
        }
        while (aIter.hasNext() && aValue > value) {
            ++count;
            aValue = aIter.getFloatNext();
            if (aValue == value) {
                isInd = 1;
            }
        }
        if (isInd == 1) {
            count += side;
        }
        count -= side;
        return count;
    }
    
    int getLatIndex(final Array lat, final double value, final int side) {
        final int[] shape = lat.getShape();
        final IndexIterator latIter = lat.getIndexIterator();
        final Index ind = lat.getIndex();
        int count = 0;
        int isInd = 0;
        double xlat = latIter.getFloatNext();
        if (xlat == value) {
            return 0;
        }
        while (latIter.hasNext() && xlat > value) {
            ++count;
            xlat = latIter.getFloatNext();
            if (xlat == value) {
                isInd = 1;
            }
        }
        if (isInd == 1) {
            count += side;
        }
        count -= side;
        return count;
    }
    
    int getLonIndex(final Array lon, final double value, final int side) {
        final int[] shape = lon.getShape();
        final IndexIterator lonIter = lon.getIndexIterator();
        final Index ind = lon.getIndex();
        int count = 0;
        int isInd = 0;
        float xlon = lonIter.getFloatNext();
        if (xlon > 180.0f) {
            xlon -= 360.0f;
        }
        if (xlon == value) {
            return 0;
        }
        while (lonIter.hasNext() && xlon < value) {
            ++count;
            xlon = lonIter.getFloatNext();
            if (xlon > 180.0f) {
                xlon -= 360.0f;
            }
            if (xlon == value) {
                isInd = 1;
            }
        }
        if (isInd == 1) {
            count += side;
        }
        count -= side;
        return count;
    }
    
    public Array getYXDataInBox(final Array data, final int x1, final int x2, final int y1, final int y2) throws IOException {
        final int rank = data.getRank();
        final int[] start = new int[rank];
        final int[] shape = new int[rank];
        for (int i = 0; i < rank; ++i) {
            start[i] = 0;
            shape[i] = 1;
        }
        if (y1 >= 0 && y2 >= 0) {
            shape[0] = y2 - (start[0] = y1);
        }
        if (x1 >= 0 && x2 >= 0) {
            shape[1] = x2 - (start[1] = x1);
        }
        Array dataVolume;
        try {
            dataVolume = data.section(start, shape);
        }
        catch (Exception e) {
            throw new IOException(e.getMessage());
        }
        return dataVolume;
    }
    
    public Array getClippedImageFrominterpolation(final Array arr, final int x1, final int x2, final int y1, final int y2) {
        final int[] srcShape = arr.getShape();
        final int rank = arr.getRank();
        final int[] start = new int[rank];
        final int[] shape = new int[rank];
        for (int i = 0; i < rank; ++i) {
            start[i] = 0;
            shape[i] = 1;
        }
        if (y1 >= 0 && y2 >= 0) {
            shape[0] = y2 - (start[0] = y1);
        }
        if (x1 >= 0 && x2 >= 0) {
            shape[1] = x2 - (start[1] = x1);
        }
        final Array dataVolume = Array.factory(DataType.FLOAT, shape);
        int count = 0;
        for (int j = y1; j < y2; ++j) {
            for (int k = x1; k < x2; ++k) {
                int index = j * srcShape[1] + k;
                if (index >= srcShape[0] * srcShape[1]) {
                    index = srcShape[0] * srcShape[1] - 1;
                }
                final float curValue = arr.getFloat(index);
                if (count >= shape[0] * shape[1]) {
                    count = shape[0] * shape[1] - 1;
                }
                dataVolume.setFloat(count, curValue);
                ++count;
            }
        }
        return dataVolume;
    }
    
    public Array getTargetImagerFromSwath(final Array lat, final Array lon, final Array data, final double[] swathInfo) {
        final int srcDataHeight = data.getShape()[0];
        final int srcDataWidth = data.getShape()[1];
        final int BBoxHeight = (int)((swathInfo[3] - swathInfo[2]) / swathInfo[1] + 0.5);
        final int BBoxWidth = (int)((swathInfo[5] - swathInfo[4]) / swathInfo[0] + 0.5);
        final int[] BBoxShape = { BBoxHeight, BBoxWidth };
        final Array bBoxArray = Array.factory(DataType.FLOAT, BBoxShape);
        final double startLon = swathInfo[4];
        final double startLat = swathInfo[3];
        final IndexIterator dataIter = data.getIndexIterator();
        final IndexIterator latIter = lat.getIndexIterator();
        final IndexIterator lonIter = lon.getIndexIterator();
        for (int i = 0; i < srcDataHeight; ++i) {
            for (int j = 0; j < srcDataWidth; ++j) {
                while (latIter.hasNext() && lonIter.hasNext() && dataIter.hasNext()) {
                    final float curLat = latIter.getFloatNext();
                    final float curLon = lonIter.getFloatNext();
                    final float curPix = dataIter.getFloatNext();
                    float alreadyValue = 0.0f;
                    final int curPixelInBBoxIndex = this.getIndexOfBBFromLatlonOfOri(startLat, startLon, swathInfo[1], swathInfo[0], curLat, curLon, BBoxHeight, BBoxWidth);
                    try {
                        alreadyValue = bBoxArray.getFloat(curPixelInBBoxIndex);
                    }
                    catch (Exception e) {
                        alreadyValue = 0.0f;
                    }
                    if (alreadyValue > 0.0f) {
                        bBoxArray.setFloat(curPixelInBBoxIndex, (curPix + alreadyValue) / 2.0f);
                    }
                    else {
                        bBoxArray.setFloat(curPixelInBBoxIndex, curPix);
                    }
                }
            }
        }
        return bBoxArray;
    }
    
    int getIndexOfBBFromLatlonOfOri(final double sLat, final double sLon, final double latInc, final double lonInc, final double curLat, final double curLon, final int bbHeight, final int bbWidth) {
        final double lonDelta = Math.abs((curLon - sLon) / lonInc);
        final double latDelta = Math.abs((curLat - sLat) / latInc);
        int row = (int)(lonDelta + 0.5);
        if (row >= bbWidth - 1) {
            row = bbWidth - 2;
        }
        if (row == 0) {
            row = 1;
        }
        final int col = (int)(latDelta + 0.5);
        int index = col * bbWidth + row;
        if (index >= bbHeight * bbWidth) {
            index = (col - 1) * bbWidth + row;
        }
        return index;
    }
    
    public Array interpolation(final Array arr) {
        final int[] orishape = arr.getShape();
        final int width = orishape[1];
        final int height = orishape[0];
        final int pixelNum = width * height;
        final Array interpolatedArray = Array.factory(DataType.FLOAT, orishape);
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                final int curIndex = i * width + j;
                final float curValue = arr.getFloat(curIndex);
                if (curValue == 0.0f) {
                    float tempPixelSum = 0.0f;
                    int numNeighborHasValue = 0;
                    float left = 0.0f;
                    float right = 0.0f;
                    float up = 0.0f;
                    float down = 0.0f;
                    float upleft = 0.0f;
                    float upright = 0.0f;
                    float downleft = 0.0f;
                    float downright = 0.0f;
                    if (curIndex - 1 >= 0 && curIndex - 1 < pixelNum) {
                        left = arr.getFloat(curIndex - 1);
                        if (left > 0.0f) {
                            tempPixelSum += left;
                            ++numNeighborHasValue;
                        }
                    }
                    if (curIndex + 1 >= 0 && curIndex + 1 < pixelNum) {
                        right = arr.getFloat(curIndex + 1);
                        if (right > 0.0f) {
                            tempPixelSum += right;
                            ++numNeighborHasValue;
                        }
                    }
                    if (curIndex - width >= 0 && curIndex - width < pixelNum) {
                        up = arr.getFloat(curIndex - width);
                        if (up > 0.0f) {
                            tempPixelSum += up;
                            ++numNeighborHasValue;
                        }
                    }
                    if (curIndex + width >= 0 && curIndex + width < pixelNum) {
                        down = arr.getFloat(curIndex + width);
                        if (down > 0.0f) {
                            tempPixelSum += down;
                            ++numNeighborHasValue;
                        }
                    }
                    if (curIndex - width - 1 >= 0 && curIndex - width - 1 < pixelNum) {
                        upleft = arr.getFloat(curIndex - width - 1);
                        if (upleft > 0.0f) {
                            tempPixelSum += upleft;
                            ++numNeighborHasValue;
                        }
                    }
                    if (curIndex - width + 1 >= 0 && curIndex - width + 1 < pixelNum) {
                        upright = arr.getFloat(curIndex - width + 1);
                        if (upright > 0.0f) {
                            tempPixelSum += upright;
                            ++numNeighborHasValue;
                        }
                    }
                    if (curIndex + width - 1 >= 0 && curIndex + width - 1 < pixelNum) {
                        downleft = arr.getFloat(curIndex + width - 1);
                        if (downleft > 0.0f) {
                            tempPixelSum += downleft;
                            ++numNeighborHasValue;
                        }
                    }
                    if (curIndex + width + 1 >= 0 && curIndex + width + 1 < pixelNum) {
                        downright = arr.getFloat(curIndex + width + 1);
                        if (downright > 0.0f) {
                            tempPixelSum += downright;
                            ++numNeighborHasValue;
                        }
                    }
                    if (tempPixelSum > 0.0f) {
                        interpolatedArray.setFloat(curIndex, tempPixelSum / numNeighborHasValue);
                    }
                }
                else {
                    interpolatedArray.setFloat(curIndex, curValue);
                }
            }
        }
        return interpolatedArray;
    }
    
    public double[] getSwathLatLonInformation(final Array lat, final Array lon) {
        final double[] increment = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
        final IndexIterator latIter = lat.getIndexIterator();
        final IndexIterator lonIter = lon.getIndexIterator();
        final int numScan = lat.getShape()[0];
        final int numSample = lat.getShape()[1];
        double maxLat = -91.0;
        double minLat = 91.0;
        double maxLon = -181.0;
        double minLon = 181.0;
        float firstLineStartLat = 0.0f;
        float firstLineStartLon = 0.0f;
        float firstLineEndLat = 0.0f;
        float firstLineEndLon = 0.0f;
        float lastLineStartLat = 0.0f;
        float lastLineStartLon = 0.0f;
        float lastLineEndLat = 0.0f;
        float lastLineEndLon = 0.0f;
        for (int i = 0; i < numScan; ++i) {
            for (int j = 0; j < numSample; ++j) {
                if (latIter.hasNext() && lonIter.hasNext()) {
                    final float curLat = latIter.getFloatNext();
                    final float curLon = lonIter.getFloatNext();
                    if (i == 0 && j == 0) {
                        firstLineStartLat = curLat;
                        firstLineStartLon = curLon;
                    }
                    else if (i == 0 && j == numSample - 1) {
                        firstLineEndLat = curLat;
                        firstLineEndLon = curLon;
                    }
                    else if (i == numScan - 1 && j == 0) {
                        lastLineStartLat = curLat;
                        lastLineStartLon = curLon;
                    }
                    else if (i == numScan - 1 && j == numSample - 1) {
                        lastLineEndLat = curLat;
                        lastLineEndLon = curLon;
                    }
                }
            }
        }
        final double[] edgeLat = { firstLineStartLat, firstLineEndLat, lastLineStartLat, lastLineEndLat };
        final double[] edgeLon = { firstLineStartLon, firstLineEndLon, lastLineStartLon, lastLineEndLon };
        for (int k = 0; k < edgeLat.length; ++k) {
            maxLat = ((maxLat > edgeLat[k]) ? maxLat : edgeLat[k]);
            minLat = ((minLat < edgeLat[k]) ? minLat : edgeLat[k]);
            maxLon = ((maxLon > edgeLon[k]) ? maxLon : edgeLon[k]);
            minLon = ((minLon < edgeLon[k]) ? minLon : edgeLon[k]);
        }
        final double xInc1 = Math.abs((firstLineEndLon - firstLineStartLon) / numSample);
        final double yInc1 = Math.abs((lastLineStartLat - firstLineStartLat) / numScan);
        increment[0] = xInc1;
        increment[1] = yInc1;
        increment[2] = minLat;
        increment[3] = maxLat;
        increment[4] = minLon;
        increment[6] = ((increment[5] = maxLon) - minLon) / xInc1;
        increment[7] = (maxLat - minLat) / yInc1;
        return increment;
    }
    
    public void writeGrid(final GridDatatype grid, final Array data, final boolean greyScale, final double xStart, final double yStart, final double xInc, final double yInc, final int imageNumber) throws IOException {
        int nextStart = 0;
        final GridCoordSystem gcs = grid.getCoordinateSystem();
        if (!gcs.isLatLon() && !(gcs.getProjection() instanceof LambertConformal) && !(gcs.getProjection() instanceof Stereographic) && !(gcs.getProjection() instanceof Mercator) && !(gcs.getProjection() instanceof AlbersEqualAreaEllipse) && !(gcs.getProjection() instanceof AlbersEqualArea)) {
            throw new IllegalArgumentException("Must be lat/lon or LambertConformal or Mercator and grid = " + gcs.getProjection().getClass().getName());
        }
        if (greyScale) {
            final ArrayByte result = this.replaceMissingValuesAndScale(grid, data);
            nextStart = this.geotiff.writeData((byte[])result.getStorage(), imageNumber);
        }
        else {
            final ArrayFloat result2 = this.replaceMissingValues(grid, data);
            nextStart = this.geotiff.writeData((float[])result2.getStorage(), imageNumber);
        }
        final int elemSize = greyScale ? 1 : 4;
        final int height = data.getShape()[0];
        final int width = data.getShape()[1];
        final int size = elemSize * height * width;
        this.geotiff.addTag(new IFDEntry(Tag.ImageWidth, FieldType.SHORT).setValue(width));
        this.geotiff.addTag(new IFDEntry(Tag.ImageLength, FieldType.SHORT).setValue(height));
        final int ff = 2;
        final int page = imageNumber - 1;
        this.geotiff.addTag(new IFDEntry(Tag.NewSubfileType, FieldType.SHORT).setValue(ff));
        this.geotiff.addTag(new IFDEntry(Tag.PageNumber, FieldType.SHORT).setValue(page, 2));
        this.geotiff.addTag(new IFDEntry(Tag.RowsPerStrip, FieldType.SHORT).setValue(1));
        final int[] soffset = new int[height];
        final int[] sbytecount = new int[height];
        if (imageNumber == 1) {
            soffset[0] = 8;
        }
        else {
            soffset[0] = nextStart;
        }
        sbytecount[0] = width * elemSize;
        for (int i = 1; i < height; ++i) {
            soffset[i] = soffset[i - 1] + width * elemSize;
            sbytecount[i] = width * elemSize;
        }
        this.geotiff.addTag(new IFDEntry(Tag.StripByteCounts, FieldType.LONG, width).setValue(sbytecount));
        this.geotiff.addTag(new IFDEntry(Tag.StripOffsets, FieldType.LONG, width).setValue(soffset));
        this.geotiff.addTag(new IFDEntry(Tag.Orientation, FieldType.SHORT).setValue(1));
        this.geotiff.addTag(new IFDEntry(Tag.Compression, FieldType.SHORT).setValue(1));
        this.geotiff.addTag(new IFDEntry(Tag.Software, FieldType.ASCII).setValue("nc2geotiff"));
        this.geotiff.addTag(new IFDEntry(Tag.PhotometricInterpretation, FieldType.SHORT).setValue(1));
        this.geotiff.addTag(new IFDEntry(Tag.PlanarConfiguration, FieldType.SHORT).setValue(1));
        if (greyScale) {
            this.geotiff.addTag(new IFDEntry(Tag.BitsPerSample, FieldType.SHORT).setValue(8));
            this.geotiff.addTag(new IFDEntry(Tag.SamplesPerPixel, FieldType.SHORT).setValue(1));
            this.geotiff.addTag(new IFDEntry(Tag.XResolution, FieldType.RATIONAL).setValue(1, 1));
            this.geotiff.addTag(new IFDEntry(Tag.YResolution, FieldType.RATIONAL).setValue(1, 1));
            this.geotiff.addTag(new IFDEntry(Tag.ResolutionUnit, FieldType.SHORT).setValue(1));
        }
        else {
            this.geotiff.addTag(new IFDEntry(Tag.BitsPerSample, FieldType.SHORT).setValue(32));
            this.geotiff.addTag(new IFDEntry(Tag.SampleFormat, FieldType.SHORT).setValue(3));
            this.geotiff.addTag(new IFDEntry(Tag.SamplesPerPixel, FieldType.SHORT).setValue(1));
            final MAMath.MinMax dataMinMax = grid.getMinMaxSkipMissingData(data);
            final float min = (float)dataMinMax.min;
            final float max = (float)dataMinMax.max;
            this.geotiff.addTag(new IFDEntry(Tag.SMinSampleValue, FieldType.FLOAT).setValue(min));
            this.geotiff.addTag(new IFDEntry(Tag.SMaxSampleValue, FieldType.FLOAT).setValue(max));
            this.geotiff.addTag(new IFDEntry(Tag.GDALNoData, FieldType.FLOAT).setValue(min - 1.0f));
        }
        this.geotiff.setTransform(xStart, yStart, xInc, yInc);
        if (gcs.isLatLon()) {
            this.addLatLonTags();
        }
        else if (gcs.getProjection() instanceof LambertConformal) {
            this.addLambertConformalTags((LambertConformal)gcs.getProjection(), xStart, yStart);
        }
        else if (gcs.getProjection() instanceof Stereographic) {
            this.addPolarStereographicTags((Stereographic)gcs.getProjection(), xStart, yStart);
        }
        else if (gcs.getProjection() instanceof Mercator) {
            this.addMercatorTags((Mercator)gcs.getProjection());
        }
        else if (gcs.getProjection() instanceof TransverseMercator) {
            this.addTransverseMercatorTags((TransverseMercator)gcs.getProjection());
        }
        else if (gcs.getProjection() instanceof AlbersEqualArea) {
            this.addAlbersEqualAreaTags((AlbersEqualArea)gcs.getProjection());
        }
        else if (gcs.getProjection() instanceof AlbersEqualAreaEllipse) {
            this.addAlbersEqualAreaEllipseTags((AlbersEqualAreaEllipse)gcs.getProjection());
        }
        this.geotiff.writeMetadata(imageNumber);
    }
    
    public void close() throws IOException {
        this.geotiff.close();
    }
    
    private ArrayFloat replaceMissingValues(final GridDatatype grid, final Array data) {
        final MAMath.MinMax dataMinMax = grid.getMinMaxSkipMissingData(data);
        final float minValue = (float)(dataMinMax.min - 1.0);
        final ArrayFloat floatArray = (ArrayFloat)Array.factory(Float.TYPE, data.getShape());
        final IndexIterator dataIter = data.getIndexIterator();
        final IndexIterator floatIter = floatArray.getIndexIterator();
        while (dataIter.hasNext()) {
            float v = dataIter.getFloatNext();
            if (grid.isMissingData(v)) {
                v = minValue;
            }
            floatIter.setFloatNext(v);
        }
        return floatArray;
    }
    
    private ArrayByte replaceMissingValuesAndScale(final GridDatatype grid, final Array data) {
        final MAMath.MinMax dataMinMax = grid.getMinMaxSkipMissingData(data);
        final double scale = 254.0 / (dataMinMax.max - dataMinMax.min);
        final ArrayByte byteArray = (ArrayByte)Array.factory(Byte.TYPE, data.getShape());
        final IndexIterator dataIter = data.getIndexIterator();
        final IndexIterator resultIter = byteArray.getIndexIterator();
        while (dataIter.hasNext()) {
            final double v = dataIter.getDoubleNext();
            byte bv;
            if (grid.isMissingData(v)) {
                bv = 0;
            }
            else {
                final int iv = (int)((v - dataMinMax.min) * scale + 1.0);
                bv = (byte)(iv & 0xFF);
            }
            resultIter.setByteNext(bv);
        }
        return byteArray;
    }
    
    private void addLatLonTags1() {
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GTModelTypeGeoKey, GeoKey.TagValue.ModelType_Geographic));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GeogGeodeticDatumGeoKey, GeoKey.TagValue.GeogGeodeticDatum6267));
    }
    
    private void addLatLonTags() {
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GTModelTypeGeoKey, GeoKey.TagValue.ModelType_Geographic));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GTRasterTypeGeoKey, GeoKey.TagValue.RasterType_Area));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GeographicTypeGeoKey, GeoKey.TagValue.GeographicType_WGS_84));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GeogPrimeMeridianGeoKey, GeoKey.TagValue.GeogPrimeMeridian_GREENWICH));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GeogAngularUnitsGeoKey, GeoKey.TagValue.GeogAngularUnits_DEGREE));
    }
    
    private void addPolarStereographicTags(final Stereographic proj, final double FalseEasting, final double FalseNorthing) {
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GTModelTypeGeoKey, GeoKey.TagValue.ModelType_Projected));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GTRasterTypeGeoKey, GeoKey.TagValue.RasterType_Area));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GeographicTypeGeoKey, GeoKey.TagValue.GeographicType_WGS_84));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjectedCSTypeGeoKey, GeoKey.TagValue.ProjectedCSType_UserDefined));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.PCSCitationGeoKey, "Snyder"));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjectionGeoKey, GeoKey.TagValue.ProjectedCSType_UserDefined));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjLinearUnitsGeoKey, GeoKey.TagValue.ProjLinearUnits_METER));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjCoordTransGeoKey, GeoKey.TagValue.ProjCoordTrans_Stereographic));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjCenterLongGeoKey, 0.0));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjNatOriginLatGeoKey, 90.0));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjScaleAtNatOriginGeoKey, 1.0));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjFalseEastingGeoKey, 0.0));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjFalseNorthingGeoKey, 0.0));
    }
    
    private void addLambertConformalTags(final LambertConformal proj, final double FalseEasting, final double FalseNorthing) {
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GTModelTypeGeoKey, GeoKey.TagValue.ModelType_Projected));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GTRasterTypeGeoKey, GeoKey.TagValue.RasterType_Area));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GeographicTypeGeoKey, GeoKey.TagValue.GeographicType_WGS_84));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjectedCSTypeGeoKey, GeoKey.TagValue.ProjectedCSType_UserDefined));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.PCSCitationGeoKey, "Snyder"));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjectionGeoKey, GeoKey.TagValue.ProjectedCSType_UserDefined));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjLinearUnitsGeoKey, GeoKey.TagValue.ProjLinearUnits_METER));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjCoordTransGeoKey, GeoKey.TagValue.ProjCoordTrans_LambertConfConic_2SP));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjStdParallel1GeoKey, proj.getParallelOne()));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjStdParallel2GeoKey, proj.getParallelTwo()));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjCenterLongGeoKey, proj.getOriginLon()));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjNatOriginLatGeoKey, proj.getOriginLat()));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjNatOriginLongGeoKey, proj.getOriginLon()));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjScaleAtNatOriginGeoKey, 1.0));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjFalseEastingGeoKey, 0.0));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjFalseNorthingGeoKey, 0.0));
    }
    
    private void addMercatorTags(final Mercator proj) {
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GTModelTypeGeoKey, GeoKey.TagValue.ModelType_Projected));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GTRasterTypeGeoKey, GeoKey.TagValue.RasterType_Area));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GeographicTypeGeoKey, GeoKey.TagValue.GeographicType_WGS_84));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjectedCSTypeGeoKey, GeoKey.TagValue.ProjectedCSType_UserDefined));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.PCSCitationGeoKey, "Mercator"));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjectionGeoKey, GeoKey.TagValue.ProjectedCSType_UserDefined));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjLinearUnitsGeoKey, GeoKey.TagValue.ProjLinearUnits_METER));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjCoordTransGeoKey, GeoKey.TagValue.ProjCoordTrans_Mercator));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjNatOriginLongGeoKey, proj.getOriginLon()));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjNatOriginLatGeoKey, proj.getParallel()));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjFalseEastingGeoKey, 0.0));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjFalseNorthingGeoKey, 0.0));
    }
    
    private void addTransverseMercatorTags(final TransverseMercator proj) {
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GTModelTypeGeoKey, GeoKey.TagValue.ModelType_Projected));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GTRasterTypeGeoKey, GeoKey.TagValue.RasterType_Area));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GeographicTypeGeoKey, GeoKey.TagValue.GeographicType_WGS_84));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GeogLinearUnitsGeoKey, GeoKey.TagValue.ProjLinearUnits_METER));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GeogAngularUnitsGeoKey, GeoKey.TagValue.GeogAngularUnits_DEGREE));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjectedCSTypeGeoKey, GeoKey.TagValue.ProjectedCSType_UserDefined));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.PCSCitationGeoKey, "Transvers Mercator"));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjectionGeoKey, GeoKey.TagValue.ProjectedCSType_UserDefined));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjLinearUnitsGeoKey, GeoKey.TagValue.ProjLinearUnits_METER));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjCoordTransGeoKey, GeoKey.TagValue.ProjCoordTrans_TransverseMercator));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjNatOriginLatGeoKey, proj.getOriginLat()));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjNatOriginLongGeoKey, proj.getTangentLon()));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjScaleAtNatOriginGeoKey, proj.getScale()));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjScaleAtNatOriginGeoKey, 1.0));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjFalseEastingGeoKey, 0.0));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjFalseNorthingGeoKey, 0.0));
    }
    
    private void addAlbersEqualAreaEllipseTags(final AlbersEqualAreaEllipse proj) {
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GTModelTypeGeoKey, GeoKey.TagValue.ModelType_Projected));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GTRasterTypeGeoKey, GeoKey.TagValue.RasterType_Area));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GeographicTypeGeoKey, GeoKey.TagValue.GeographicType_WGS_84));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GeogLinearUnitsGeoKey, GeoKey.TagValue.ProjLinearUnits_METER));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GeogSemiMajorAxisGeoKey, proj.getEarth().getMajor()));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GeogSemiMinorAxisGeoKey, proj.getEarth().getMinor()));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GeogAngularUnitsGeoKey, GeoKey.TagValue.GeogAngularUnits_DEGREE));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjectedCSTypeGeoKey, GeoKey.TagValue.ProjectedCSType_UserDefined));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.PCSCitationGeoKey, "Albers Conial Equal Area"));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjectionGeoKey, GeoKey.TagValue.ProjectedCSType_UserDefined));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjLinearUnitsGeoKey, GeoKey.TagValue.ProjLinearUnits_METER));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjCoordTransGeoKey, GeoKey.TagValue.ProjCoordTrans_AlbersEqualAreaEllipse));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjNatOriginLatGeoKey, proj.getOriginLat()));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjNatOriginLongGeoKey, proj.getOriginLon()));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjStdParallel1GeoKey, proj.getParallelOne()));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjStdParallel2GeoKey, proj.getParallelTwo()));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjFalseEastingGeoKey, 0.0));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjFalseNorthingGeoKey, 0.0));
    }
    
    private void addAlbersEqualAreaTags(final AlbersEqualArea proj) {
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GTModelTypeGeoKey, GeoKey.TagValue.ModelType_Projected));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GTRasterTypeGeoKey, GeoKey.TagValue.RasterType_Area));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GeographicTypeGeoKey, GeoKey.TagValue.GeographicType_WGS_84));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GeogLinearUnitsGeoKey, GeoKey.TagValue.ProjLinearUnits_METER));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.GeogAngularUnitsGeoKey, GeoKey.TagValue.GeogAngularUnits_DEGREE));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjectedCSTypeGeoKey, GeoKey.TagValue.ProjectedCSType_UserDefined));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.PCSCitationGeoKey, "Albers Conial Equal Area"));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjectionGeoKey, GeoKey.TagValue.ProjectedCSType_UserDefined));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjLinearUnitsGeoKey, GeoKey.TagValue.ProjLinearUnits_METER));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjCoordTransGeoKey, GeoKey.TagValue.ProjCoordTrans_AlbersConicalEqualArea));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjNatOriginLatGeoKey, proj.getOriginLat()));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjNatOriginLongGeoKey, proj.getOriginLon()));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjStdParallel1GeoKey, proj.getParallelOne()));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjStdParallel2GeoKey, proj.getParallelTwo()));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjFalseEastingGeoKey, 0.0));
        this.geotiff.addGeoKey(new GeoKey(GeoKey.Tag.ProjFalseNorthingGeoKey, 0.0));
    }
    
    private void dump(final Array data, final int col) {
        final int[] shape = data.getShape();
        final Index ima = data.getIndex();
        for (int j = 0; j < shape[0]; ++j) {
            final float dd = data.getFloat(ima.set(j, col));
            System.out.println(j + " value= " + dd);
        }
    }
    
    private double geoShiftGetXstart(final Array lon, final double inc) {
        final int count = 0;
        final Index ilon = lon.getIndex();
        final int[] lonShape = lon.getShape();
        final IndexIterator lonIter = lon.getIndexIterator();
        double xlon = 0.0;
        final LatLonPoint p0 = new LatLonPointImpl(0.0, lon.getFloat(ilon.set(0)));
        final LatLonPoint pN = new LatLonPointImpl(0.0, lon.getFloat(ilon.set(lonShape[0] - 1)));
        xlon = p0.getLongitude();
        while (lonIter.hasNext()) {
            final float l = lonIter.getFloatNext();
            final LatLonPoint pn = new LatLonPointImpl(0.0, l);
            if (pn.getLongitude() < xlon) {
                xlon = pn.getLongitude();
            }
        }
        if (p0.getLongitude() == pN.getLongitude()) {
            xlon -= inc;
        }
        return xlon;
    }
    
    private Array geoShiftDataAtLon(final Array data, final Array lon) {
        int count = 0;
        final int[] shape = data.getShape();
        final Index ima = data.getIndex();
        final Index ilon = lon.getIndex();
        final int[] lonShape = lon.getShape();
        final ArrayFloat adata = new ArrayFloat(new int[] { shape[0], shape[1] });
        final Index imaa = adata.getIndex();
        final IndexIterator lonIter = lon.getIndexIterator();
        final LatLonPoint p0 = new LatLonPointImpl(0.0, lon.getFloat(ilon.set(lonShape[0] - 1)));
        final LatLonPoint pN = new LatLonPointImpl(0.0, lon.getFloat(ilon.set(0)));
        while (lonIter.hasNext()) {
            final float l = lonIter.getFloatNext();
            if (l > 180.0) {
                ++count;
            }
        }
        int spoint = 0;
        if (p0.getLongitude() == pN.getLongitude()) {
            spoint = shape[1] - count - 1;
        }
        else {
            spoint = shape[1] - count;
        }
        if (count > 0 && shape[1] > count) {
            for (int j = 1; j < shape[1]; ++j) {
                int jj = 0;
                if (j >= count) {
                    jj = j - count;
                }
                else {
                    jj = j + spoint;
                }
                for (int i = 0; i < shape[0]; ++i) {
                    final float dd = data.getFloat(ima.set(i, jj));
                    adata.setFloat(imaa.set(i, j), dd);
                }
            }
            if (p0.getLongitude() == pN.getLongitude()) {
                for (int k = 0; k < shape[0]; ++k) {
                    final float dd2 = adata.getFloat(imaa.set(k, shape[1] - 1));
                    adata.setFloat(imaa.set(k, 0), dd2);
                }
            }
            return adata;
        }
        return data;
    }
    
    private Array geoShiftLon(final Array lon) {
        int count = 0;
        final Index lonIndex = lon.getIndex();
        final int[] lonShape = lon.getShape();
        final ArrayFloat slon = new ArrayFloat(new int[] { lonShape[0] });
        final Index slonIndex = slon.getIndex();
        final IndexIterator lonIter = lon.getIndexIterator();
        final LatLonPointImpl llp = new LatLonPointImpl();
        final LatLonPoint p0 = new LatLonPointImpl(0.0, lon.getFloat(lonIndex.set(lonShape[0] - 1)));
        final LatLonPoint pN = new LatLonPointImpl(0.0, lon.getFloat(lonIndex.set(0)));
        while (lonIter.hasNext()) {
            final float l = lonIter.getFloatNext();
            if (l > 180.0) {
                ++count;
            }
        }
        int spoint = 0;
        if (p0.getLongitude() == pN.getLongitude()) {
            spoint = lonShape[0] - count - 1;
        }
        else {
            spoint = lonShape[0] - count;
        }
        if (count > 0 && lonShape[0] > count) {
            for (int j = 1; j < lonShape[0]; ++j) {
                int jj = 0;
                if (j >= count) {
                    jj = j - count;
                }
                else {
                    jj = j + spoint;
                }
                final float dd = lon.getFloat(lonIndex.set(jj));
                slon.setFloat(slonIndex.set(j), (float)LatLonPointImpl.lonNormal(dd));
            }
            if (p0.getLongitude() == pN.getLongitude()) {
                final float dd2 = slon.getFloat(slonIndex.set(lonShape[0] - 1));
                slon.setFloat(slonIndex.set(0), -(float)LatLonPointImpl.lonNormal(dd2));
            }
            return slon;
        }
        return lon;
    }
    
    public static void main(final String[] args) throws IOException {
        final String fileOut = "/home/yuanho/Download/F15_s.tmp_new.tif";
        final LatLonPointImpl p1 = new LatLonPointImpl(-5.0, -52.0);
        final LatLonPointImpl p2 = new LatLonPointImpl(25.0, -20.0);
        final LatLonRect llr = new LatLonRect(p1, p2);
        final GeotiffWriter writer = new GeotiffWriter(fileOut);
        writer.writeGrid("/home/yuanho/GIS/DataAndCode/F15_s.tmp", "infraredImagery", 0, 0, true, llr);
        writer.close();
        final GeoTiff geotiff = new GeoTiff(fileOut);
        geotiff.read();
        System.out.println("geotiff read in = " + geotiff.showInfo());
        geotiff.close();
    }
}
