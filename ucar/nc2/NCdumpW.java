// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.List;
import ucar.nc2.util.URLnaming;
import ucar.unidata.util.StringUtil;
import java.util.Formatter;
import java.io.OutputStream;
import ucar.ma2.StructureMembers;
import ucar.ma2.IndexIterator;
import ucar.ma2.StructureDataIterator;
import ucar.ma2.Index;
import java.nio.ByteBuffer;
import ucar.ma2.ArraySequence;
import ucar.ma2.StructureData;
import ucar.ma2.ArrayStructure;
import ucar.ma2.ArrayObject;
import ucar.ma2.ArrayChar;
import java.io.CharArrayWriter;
import ucar.nc2.util.Indent;
import ucar.ma2.InvalidRangeException;
import java.io.StringWriter;
import ucar.ma2.Array;
import java.util.Iterator;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.util.StringTokenizer;
import java.io.IOException;
import ucar.nc2.util.CancelTask;
import java.io.Writer;

public class NCdumpW
{
    private static String usage;
    private static int totalWidth;
    private static char[] org;
    private static String[] replace;
    
    public static boolean printHeader(final String fileName, final Writer out) throws IOException {
        return print(fileName, out, false, false, false, false, null, null);
    }
    
    public static boolean printNcML(final String fileName, final Writer out) throws IOException {
        return print(fileName, out, false, true, true, false, null, null);
    }
    
    public static boolean print(final String command, final Writer out) throws IOException {
        return print(command, out, null);
    }
    
    public static boolean print(String command, final Writer out, final CancelTask ct) throws IOException {
        final StringTokenizer stoke = new StringTokenizer(command);
        if (!stoke.hasMoreTokens()) {
            out.write(NCdumpW.usage);
            return false;
        }
        final String filename = stoke.nextToken();
        NetcdfFile nc = null;
        try {
            nc = NetcdfFile.open(filename, ct);
            final int pos = command.indexOf(filename);
            command = command.substring(pos + filename.length());
            return print(nc, command, out, ct);
        }
        catch (FileNotFoundException e) {
            out.write("file not found= ");
            out.write(filename);
            return false;
        }
        finally {
            if (nc != null) {
                nc.close();
            }
            out.flush();
        }
    }
    
    public static boolean print(final NetcdfFile nc, final String command, final Writer out, final CancelTask ct) throws IOException {
        WantValues showValues = WantValues.none;
        boolean ncml = false;
        boolean strict = false;
        String varNames = null;
        if (command != null) {
            final StringTokenizer stoke = new StringTokenizer(command);
            while (stoke.hasMoreTokens()) {
                final String toke = stoke.nextToken();
                if (toke.equalsIgnoreCase("-help")) {
                    out.write(NCdumpW.usage);
                    out.write(10);
                    return true;
                }
                if (toke.equalsIgnoreCase("-vall")) {
                    showValues = WantValues.all;
                }
                if (toke.equalsIgnoreCase("-c") && showValues == WantValues.none) {
                    showValues = WantValues.coordsOnly;
                }
                if (toke.equalsIgnoreCase("-ncml")) {
                    ncml = true;
                }
                if (toke.equalsIgnoreCase("-cdl")) {
                    strict = true;
                }
                if (!toke.equalsIgnoreCase("-v") || !stoke.hasMoreTokens()) {
                    continue;
                }
                varNames = stoke.nextToken();
            }
        }
        return print(nc, out, showValues, ncml, strict, varNames, ct);
    }
    
    public static boolean print(final String filename, final Writer out, final boolean showAll, final boolean showCoords, final boolean ncml, final boolean strict, final String varNames, final CancelTask ct) throws IOException {
        NetcdfFile nc = null;
        try {
            nc = NetcdfFile.open(filename, ct);
            return print(nc, out, showAll, showCoords, ncml, strict, varNames, ct);
        }
        catch (FileNotFoundException e) {
            out.write("file not found= ");
            out.write(filename);
            out.flush();
            return false;
        }
        finally {
            if (nc != null) {
                nc.close();
            }
        }
    }
    
    public static boolean print(final NetcdfFile nc, final Writer out, final boolean showAll, final boolean showCoords, final boolean ncml, final boolean strict, final String varNames, final CancelTask ct) throws IOException {
        WantValues showValues = WantValues.none;
        if (showAll) {
            showValues = WantValues.all;
        }
        else if (showCoords) {
            showValues = WantValues.coordsOnly;
        }
        return print(nc, out, showValues, ncml, strict, varNames, ct);
    }
    
    public static boolean print(final NetcdfFile nc, final Writer out, final WantValues showValues, final boolean ncml, final boolean strict, final String varNames, final CancelTask ct) throws IOException {
        final boolean headerOnly = showValues == WantValues.none && varNames == null;
        try {
            if (ncml) {
                writeNcML(nc, out, showValues, null);
            }
            else if (headerOnly) {
                nc.writeCDL(new PrintWriter(out), strict);
            }
            else {
                final PrintWriter ps = new PrintWriter(out);
                nc.toStringStart(ps, strict);
                ps.print(" data:\n");
                if (showValues == WantValues.all) {
                    for (final Variable v : nc.getVariables()) {
                        printArray(v.read(), v.getName(), ps, ct);
                        if (ct != null && ct.isCancel()) {
                            return false;
                        }
                    }
                }
                else if (showValues == WantValues.coordsOnly) {
                    for (final Variable v : nc.getVariables()) {
                        if (v.isCoordinateVariable()) {
                            printArray(v.read(), v.getName(), ps, ct);
                        }
                        if (ct != null && ct.isCancel()) {
                            return false;
                        }
                    }
                }
                if (showValues != WantValues.all && varNames != null) {
                    final StringTokenizer stoke = new StringTokenizer(varNames, ";");
                    while (stoke.hasMoreTokens()) {
                        final String varSubset = stoke.nextToken();
                        if (varSubset.indexOf(40) >= 0) {
                            final Array data = nc.readSection(varSubset);
                            printArray(data, varSubset, ps, ct);
                        }
                        else {
                            final Variable v2 = nc.findVariable(varSubset);
                            if (v2 == null) {
                                ps.print(" cant find variable: " + varSubset + "\n   " + NCdumpW.usage);
                                continue;
                            }
                            if (showValues != WantValues.coordsOnly || v2.isCoordinateVariable()) {
                                printArray(v2.read(), v2.getName(), ps, ct);
                            }
                        }
                        if (ct != null && ct.isCancel()) {
                            return false;
                        }
                    }
                }
                nc.toStringEnd(ps);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            out.write(e.getMessage());
            out.flush();
            return false;
        }
        out.flush();
        return true;
    }
    
    public static String printVariableData(final VariableIF v, final CancelTask ct) throws IOException {
        final Array data = v.read();
        final StringWriter writer = new StringWriter(10000);
        printArray(data, v.getName(), new PrintWriter(writer), ct);
        return writer.toString();
    }
    
    public static String printVariableDataSection(final Variable v, final String sectionSpec, final CancelTask ct) throws IOException, InvalidRangeException {
        final Array data = v.read(sectionSpec);
        final StringWriter writer = new StringWriter(20000);
        printArray(data, v.getName(), new PrintWriter(writer), ct);
        return writer.toString();
    }
    
    public static void printArray(final Array array, final String name, final PrintWriter out, final CancelTask ct) throws IOException {
        printArray(array, name, null, out, new Indent(2), ct);
        out.flush();
    }
    
    public static String printArray(final Array array, final String name, final CancelTask ct) throws IOException {
        final CharArrayWriter carray = new CharArrayWriter(100000);
        final PrintWriter pw = new PrintWriter(carray);
        printArray(array, name, null, pw, new Indent(2), ct);
        return carray.toString();
    }
    
    private static void printArray(final Array array, final String name, final String units, final PrintWriter out, final Indent ilev, final CancelTask ct) throws IOException {
        if (ct != null && ct.isCancel()) {
            return;
        }
        if (name != null) {
            out.print(ilev + name + " =");
        }
        ilev.incr();
        if (array == null) {
            throw new IllegalArgumentException("null array for " + name);
        }
        if (array instanceof ArrayChar && array.getRank() > 0) {
            printStringArray(out, (ArrayChar)array, ilev, ct);
        }
        else if (array.getElementType() == String.class) {
            printStringArray(out, (ArrayObject)array, ilev, ct);
        }
        else if (array instanceof ArrayStructure) {
            if (array.getSize() == 1L) {
                printStructureData(out, (StructureData)array.getObject(array.getIndex()), ilev, ct);
            }
            else {
                printStructureDataArray(out, (ArrayStructure)array, ilev, ct);
            }
        }
        else if (array instanceof ArraySequence) {
            printSequence(out, (ArraySequence)array, ilev, ct);
        }
        else if (array.getElementType() == ByteBuffer.class) {
            array.resetLocalIterator();
            while (array.hasNext()) {
                printByteBuffer(out, (ByteBuffer)array.next(), ilev);
                out.println(",");
                if (ct != null && ct.isCancel()) {
                    return;
                }
            }
        }
        else {
            printArray(array, out, ilev, ct);
        }
        if (units != null) {
            out.print(" " + units);
        }
        out.print("\n");
        ilev.decr();
        out.flush();
    }
    
    private static void printArray(final Array ma, final PrintWriter out, final Indent indent, final CancelTask ct) {
        if (ct != null && ct.isCancel()) {
            return;
        }
        final int rank = ma.getRank();
        final Index ima = ma.getIndex();
        if (rank == 0) {
            out.print(ma.getObject(ima).toString());
            return;
        }
        final int[] dims = ma.getShape();
        final int last = dims[0];
        out.print("\n" + indent + "{");
        if (rank == 1 && ma.getElementType() != StructureData.class) {
            for (int ii = 0; ii < last; ++ii) {
                out.print(ma.getObject(ima.set(ii)).toString());
                if (ii != last - 1) {
                    out.print(", ");
                }
                if (ct != null && ct.isCancel()) {
                    return;
                }
            }
            out.print("}");
            return;
        }
        indent.incr();
        for (int ii = 0; ii < last; ++ii) {
            final Array slice = ma.slice(0, ii);
            printArray(slice, out, indent, ct);
            if (ii != last - 1) {
                out.print(",");
            }
            if (ct != null && ct.isCancel()) {
                return;
            }
        }
        indent.decr();
        out.print("\n" + indent + "}");
    }
    
    static void printStringArray(final PrintWriter out, final ArrayChar ma, final Indent indent, final CancelTask ct) {
        if (ct != null && ct.isCancel()) {
            return;
        }
        final int rank = ma.getRank();
        if (rank == 1) {
            out.print("  \"" + ma.getString() + "\"");
            return;
        }
        if (rank == 2) {
            boolean first = true;
            final ArrayChar.StringIterator iter = ma.getStringIterator();
            while (iter.hasNext()) {
                if (!first) {
                    out.print(", ");
                }
                out.print("\"" + iter.next() + "\"");
                first = false;
                if (ct != null && ct.isCancel()) {
                    return;
                }
            }
            return;
        }
        final int[] dims = ma.getShape();
        final int last = dims[0];
        out.print("\n" + indent + "{");
        indent.incr();
        for (int ii = 0; ii < last; ++ii) {
            final ArrayChar slice = (ArrayChar)ma.slice(0, ii);
            printStringArray(out, slice, indent, ct);
            if (ii != last - 1) {
                out.print(",");
            }
            if (ct != null && ct.isCancel()) {
                return;
            }
        }
        indent.decr();
        out.print("\n" + indent + "}");
    }
    
    private static void printByteBuffer(final PrintWriter out, final ByteBuffer bb, final Indent indent) {
        out.print(indent + "0x");
        for (int last = bb.limit() - 1, i = 0; i <= last; ++i) {
            out.printf("%02x", bb.get(i));
        }
    }
    
    static void printStringArray(final PrintWriter out, final ArrayObject ma, final Indent indent, final CancelTask ct) {
        if (ct != null && ct.isCancel()) {
            return;
        }
        final int rank = ma.getRank();
        final Index ima = ma.getIndex();
        if (rank == 0) {
            out.print("  \"" + ma.getObject(ima) + "\"");
            return;
        }
        if (rank == 1) {
            boolean first = true;
            for (int i = 0; i < ma.getSize(); ++i) {
                if (!first) {
                    out.print(", ");
                }
                out.print("  \"" + ma.getObject(ima.set(i)) + "\"");
                first = false;
            }
            return;
        }
        final int[] dims = ma.getShape();
        final int last = dims[0];
        out.print("\n" + indent + "{");
        indent.incr();
        for (int ii = 0; ii < last; ++ii) {
            final ArrayObject slice = (ArrayObject)ma.slice(0, ii);
            printStringArray(out, slice, indent, ct);
            if (ii != last - 1) {
                out.print(",");
            }
        }
        indent.decr();
        out.print("\n" + indent + "}");
    }
    
    private static void printStructureDataArray(final PrintWriter out, final ArrayStructure array, final Indent indent, final CancelTask ct) throws IOException {
        final StructureDataIterator sdataIter = array.getStructureDataIterator();
        int count = 0;
        while (sdataIter.hasNext()) {
            final StructureData sdata = sdataIter.next();
            out.println("\n" + indent + "{");
            printStructureData(out, sdata, indent, ct);
            out.print(indent + "} " + sdata.getName() + "(" + count + ")");
            if (ct != null && ct.isCancel()) {
                return;
            }
            ++count;
        }
    }
    
    private static void printVariableArray(final PrintWriter out, final ArrayObject array, final Indent indent, final CancelTask ct) throws IOException {
        out.println("\n" + indent + "{");
        indent.incr();
        final IndexIterator iter = array.getIndexIterator();
        while (iter.hasNext()) {
            final Array data = (Array)iter.next();
            printArray(data, out, indent, ct);
        }
        indent.decr();
        out.print(indent + "}");
    }
    
    private static void printSequence(final PrintWriter out, final ArraySequence seq, final Indent indent, final CancelTask ct) throws IOException {
        final StructureDataIterator iter = seq.getStructureDataIterator();
        while (iter.hasNext()) {
            final StructureData sdata = iter.next();
            out.println("\n" + indent + "{");
            printStructureData(out, sdata, indent, ct);
            out.print(indent + "} " + sdata.getName());
            if (ct != null && ct.isCancel()) {
                return;
            }
        }
    }
    
    public static void printStructureData(final PrintWriter out, final StructureData sdata) throws IOException {
        printStructureData(out, sdata, new Indent(2), null);
        out.flush();
    }
    
    private static void printStructureData(final PrintWriter out, final StructureData sdata, final Indent indent, final CancelTask ct) throws IOException {
        indent.incr();
        for (final StructureMembers.Member m : sdata.getMembers()) {
            final Array sdataArray = sdata.getArray(m);
            printArray(sdataArray, m.getName(), m.getUnitsString(), out, indent, ct);
            if (ct != null && ct.isCancel()) {
                return;
            }
        }
        indent.decr();
    }
    
    public static void printArray(final Array ma, final PrintWriter out) {
        ma.resetLocalIterator();
        while (ma.hasNext()) {
            out.print(ma.next());
            out.print(' ');
        }
    }
    
    public static void printArray(final Array ma) {
        final PrintWriter out = new PrintWriter(System.out);
        printArray(ma, out);
        out.flush();
    }
    
    public static void writeNcML(final NetcdfFile ncfile, final Writer os, final boolean showCoords, final String uri) throws IOException {
        writeNcML(ncfile, os, showCoords ? WantValues.coordsOnly : WantValues.none, uri);
    }
    
    public static void writeNcML(final NetcdfFile ncfile, final Writer os, final WantValues showValues, final String uri) throws IOException {
        writeNcML(ncfile, new Formatter(os), showValues, uri);
    }
    
    public static void writeNcML(final NetcdfFile ncfile, final Formatter out, final WantValues showValues, final String uri) throws IOException {
        out.format("<?xml version='1.0' encoding='UTF-8'?>%n", new Object[0]);
        out.format("<netcdf xmlns='http://www.unidata.ucar.edu/namespaces/netcdf/ncml-2.2'%n", new Object[0]);
        if (uri != null) {
            out.format("    location='%s' >%n%n", StringUtil.quoteXmlAttribute(uri));
        }
        else {
            out.format("    location='%s' >%n%n", StringUtil.quoteXmlAttribute(URLnaming.canonicalizeWrite(ncfile.getLocation())));
        }
        if (ncfile.getId() != null) {
            out.format("    id='%s'%n", StringUtil.quoteXmlAttribute(ncfile.getId()));
        }
        if (ncfile.getTitle() != null) {
            out.format("    title='%s'%n", StringUtil.quoteXmlAttribute(ncfile.getTitle()));
        }
        writeNcMLGroup(ncfile, ncfile.getRootGroup(), out, new Indent(2), showValues);
        out.format("</netcdf>%n", new Object[0]);
        out.flush();
    }
    
    public static void writeNcMLVariable(final Variable v, final Formatter out) throws IOException {
        if (v instanceof Structure) {
            writeNcMLStructure((Structure)v, out, new Indent(2), WantValues.none);
        }
        else {
            writeNcMLVariable(v, out, new Indent(2), WantValues.none);
        }
    }
    
    private static void writeNcMLGroup(final NetcdfFile ncfile, final Group g, final Formatter out, final Indent indent, final WantValues showValues) throws IOException {
        if (g != ncfile.getRootGroup()) {
            out.format("%s<group name='%s' >%n", indent, StringUtil.quoteXmlAttribute(g.getShortName()));
        }
        indent.incr();
        final List<Dimension> dimList = g.getDimensions();
        for (final Dimension dim : dimList) {
            out.format("%s<dimension name='%s' length='%s'", indent, StringUtil.quoteXmlAttribute(dim.getName()), dim.getLength());
            if (dim.isUnlimited()) {
                out.format(" isUnlimited='true'", new Object[0]);
            }
            out.format(" />%n", new Object[0]);
        }
        if (dimList.size() > 0) {
            out.format("%n", new Object[0]);
        }
        final List<Attribute> attList = g.getAttributes();
        for (final Attribute att : attList) {
            writeNcMLAtt(att, out, indent);
        }
        if (attList.size() > 0) {
            out.format("%n", new Object[0]);
        }
        for (final Variable v : g.getVariables()) {
            if (v instanceof Structure) {
                writeNcMLStructure((Structure)v, out, indent, showValues);
            }
            else {
                writeNcMLVariable(v, out, indent, showValues);
            }
        }
        final List groupList = g.getGroups();
        for (int i = 0; i < groupList.size(); ++i) {
            if (i > 0) {
                out.format("%n", new Object[0]);
            }
            final Group nested = groupList.get(i);
            writeNcMLGroup(ncfile, nested, out, indent, showValues);
        }
        indent.decr();
        if (g != ncfile.getRootGroup()) {
            out.format("%s</group>%n", indent);
        }
    }
    
    private static void writeNcMLStructure(final Structure s, final Formatter out, final Indent indent, final WantValues showValues) throws IOException {
        out.format("%s<structure name='%s", indent, StringUtil.quoteXmlAttribute(s.getShortName()));
        if (s.getRank() > 0) {
            writeNcMLDimension(s, out);
        }
        out.format(">%n", new Object[0]);
        indent.incr();
        final List<Attribute> attList = s.getAttributes();
        for (final Attribute att : attList) {
            writeNcMLAtt(att, out, indent);
        }
        if (attList.size() > 0) {
            out.format("%n", new Object[0]);
        }
        final List<Variable> varList = s.getVariables();
        for (final Variable v : varList) {
            writeNcMLVariable(v, out, indent, showValues);
        }
        indent.decr();
        out.format("%s</structure>%n", indent);
    }
    
    private static void writeNcMLVariable(final Variable v, final Formatter out, final Indent indent, final WantValues showValues) throws IOException {
        out.format("%s<variable name='%s' type='%s'", indent, StringUtil.quoteXmlAttribute(v.getShortName()), v.getDataType());
        if (v.getRank() > 0) {
            writeNcMLDimension(v, out);
        }
        indent.incr();
        boolean closed = false;
        final List<Attribute> atts = v.getAttributes();
        if (atts.size() > 0) {
            out.format(" >\n", new Object[0]);
            closed = true;
            for (final Attribute att : atts) {
                writeNcMLAtt(att, out, indent);
            }
        }
        if (showValues == WantValues.all || (showValues == WantValues.coordsOnly && v.isCoordinateVariable())) {
            if (!closed) {
                out.format(" >\n", new Object[0]);
                closed = true;
            }
            writeNcMLValues(v, out, indent);
        }
        indent.decr();
        if (!closed) {
            out.format(" />\n", new Object[0]);
        }
        else {
            out.format("%s</variable>%n", indent);
        }
    }
    
    private static void writeNcMLDimension(final Variable v, final Formatter out) {
        out.format(" shape='", new Object[0]);
        final List<Dimension> dims = v.getDimensions();
        for (int j = 0; j < dims.size(); ++j) {
            final Dimension dim = dims.get(j);
            if (j != 0) {
                out.format(" ", new Object[0]);
            }
            if (dim.isShared()) {
                out.format("%s", StringUtil.quoteXmlAttribute(dim.getName()));
            }
            else {
                out.format("%d", dim.getLength());
            }
        }
        out.format("'", new Object[0]);
    }
    
    private static void writeNcMLAtt(final Attribute att, final Formatter out, final Indent indent) {
        out.format("%s<attribute name='%s' value='", indent, StringUtil.quoteXmlAttribute(att.getName()));
        if (att.isString()) {
            for (int i = 0; i < att.getLength(); ++i) {
                if (i > 0) {
                    out.format("\\, ", new Object[0]);
                }
                out.format("%s", StringUtil.quoteXmlAttribute(att.getStringValue(i)));
            }
        }
        else {
            for (int i = 0; i < att.getLength(); ++i) {
                if (i > 0) {
                    out.format(" ", new Object[0]);
                }
                out.format("%s ", att.getNumericValue(i));
            }
            out.format("' type='%s", att.getDataType());
        }
        out.format("' />\n", new Object[0]);
    }
    
    private static void writeNcMLValues(final Variable v, final Formatter out, final Indent indent) throws IOException {
        final Array data = v.read();
        int width = formatValues(indent + "<values>", out, 0, indent);
        final IndexIterator ii = data.getIndexIterator();
        while (ii.hasNext()) {
            width = formatValues(ii.next() + " ", out, width, indent);
        }
        formatValues("</values>\n", out, width, indent);
    }
    
    private static int formatValues(final String s, final Formatter out, int width, final Indent indent) {
        final int len = s.length();
        if (len + width > NCdumpW.totalWidth) {
            out.format("%n%s", indent);
            width = indent.toString().length();
        }
        out.format("%s", s);
        width += len;
        return width;
    }
    
    public static String encodeString(final String s) {
        return StringUtil.replace(s, NCdumpW.org, NCdumpW.replace);
    }
    
    public static void main(final String[] args) {
        if (args.length == 0) {
            System.out.println(NCdumpW.usage);
            return;
        }
        final StringBuilder sbuff = new StringBuilder();
        for (final String arg : args) {
            sbuff.append(arg);
            sbuff.append(" ");
        }
        try {
            final Writer writer = new BufferedWriter(new OutputStreamWriter(System.out, Charset.forName("UTF-8")));
            print(sbuff.toString(), writer, null);
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
    
    static {
        NCdumpW.usage = "usage: NCdumpW <filename> [-cdl | -ncml] [-c | -vall] [-v varName1;varName2;..] [-v varName(0:1,:,12)]\n";
        NCdumpW.totalWidth = 80;
        NCdumpW.org = new char[] { '\b', '\f', '\n', '\r', '\t', '\\', '\'', '\"' };
        NCdumpW.replace = new String[] { "\\b", "\\f", "\\n", "\\r", "\\t", "\\\\", "\\'", "\\\"" };
    }
    
    public enum WantValues
    {
        none, 
        coordsOnly, 
        all;
    }
}
