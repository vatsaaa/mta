// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft;

import java.io.IOException;

public interface PointFeatureCollectionIterator
{
    boolean hasNext() throws IOException;
    
    PointFeatureCollection next() throws IOException;
    
    void finish();
    
    void setBufferSize(final int p0);
    
    public interface Filter
    {
        boolean filter(final PointFeatureCollection p0);
    }
}
