// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft;

class DataCost
{
    private int dataCount;
    private int timeMsecs;
    
    public DataCost(final int dataCount, final int timeMsecs) {
        this.dataCount = dataCount;
        this.timeMsecs = timeMsecs;
    }
    
    int estimateDataCount() {
        return this.dataCount;
    }
    
    int estimateTimeCostInMilliSeconds() {
        return this.timeMsecs;
    }
}
