// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft;

import java.io.IOException;

public interface NestedPointFeatureCollectionIterator
{
    boolean hasNext() throws IOException;
    
    NestedPointFeatureCollection next() throws IOException;
    
    void setBufferSize(final int p0);
    
    public interface Filter
    {
        boolean filter(final NestedPointFeatureCollection p0);
    }
}
