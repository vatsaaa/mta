// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft;

import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.units.DateRange;
import java.io.IOException;

public interface PointFeatureCollection extends FeatureCollection
{
    boolean hasNext() throws IOException;
    
    PointFeature next() throws IOException;
    
    void resetIteration() throws IOException;
    
    void finish();
    
    int size();
    
    DateRange getDateRange();
    
    LatLonRect getBoundingBox();
    
    void setDateRange(final DateRange p0);
    
    void setBoundingBox(final LatLonRect p0);
    
    void setSize(final int p0);
    
    void calcBounds() throws IOException;
    
    PointFeatureIterator getPointFeatureIterator(final int p0) throws IOException;
    
    PointFeatureCollection subset(final LatLonRect p0, final DateRange p1) throws IOException;
}
