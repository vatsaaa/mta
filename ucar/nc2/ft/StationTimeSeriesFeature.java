// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft;

import java.io.IOException;
import ucar.nc2.units.DateRange;
import ucar.unidata.geoloc.Station;

public interface StationTimeSeriesFeature extends Station, PointFeatureCollection
{
    int size();
    
    StationTimeSeriesFeature subset(final DateRange p0) throws IOException;
}
