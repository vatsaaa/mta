// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft;

import ucar.unidata.geoloc.LatLonRect;
import java.io.IOException;
import ucar.unidata.geoloc.Station;
import java.util.List;

public interface StationCollection
{
    List<Station> getStations() throws IOException;
    
    List<Station> getStations(final LatLonRect p0) throws IOException;
    
    List<Station> getStations(final List<String> p0);
    
    Station getStation(final String p0);
    
    LatLonRect getBoundingBox();
}
