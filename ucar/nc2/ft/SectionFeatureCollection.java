// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft;

import java.io.IOException;

public interface SectionFeatureCollection extends NestedPointFeatureCollection
{
    boolean hasNext() throws IOException;
    
    SectionFeature next() throws IOException;
    
    void resetIteration() throws IOException;
}
