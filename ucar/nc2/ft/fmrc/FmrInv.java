// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.fmrc;

import java.util.HashSet;
import java.util.Set;
import java.util.Collections;
import java.util.Collection;
import java.util.Iterator;
import java.util.Formatter;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.List;

public class FmrInv implements Comparable<FmrInv>
{
    private final List<TimeCoord> timeCoords;
    private final List<EnsCoord> ensCoords;
    private final List<VertCoord> vertCoords;
    private final Map<String, GridVariable> uvHash;
    private List<GridVariable> gridList;
    private final List<GridDatasetInv> invList;
    private final Date runtime;
    
    public List<TimeCoord> getTimeCoords() {
        return this.timeCoords;
    }
    
    public List<EnsCoord> getEnsCoords() {
        return this.ensCoords;
    }
    
    public List<VertCoord> getVertCoords() {
        return this.vertCoords;
    }
    
    public List<GridVariable> getGrids() {
        return this.gridList;
    }
    
    public List<GridDatasetInv> getInventoryList() {
        return this.invList;
    }
    
    public Date getRunDate() {
        return this.runtime;
    }
    
    public String getName() {
        return "";
    }
    
    FmrInv(final Date runtime) {
        this.timeCoords = new ArrayList<TimeCoord>();
        this.ensCoords = new ArrayList<EnsCoord>();
        this.vertCoords = new ArrayList<VertCoord>();
        this.uvHash = new HashMap<String, GridVariable>();
        this.invList = new ArrayList<GridDatasetInv>();
        this.runtime = runtime;
    }
    
    void addDataset(final GridDatasetInv inv, final Formatter debug) {
        this.invList.add(inv);
        if (debug != null) {
            debug.format(" Fmr add GridDatasetInv %s = ", inv.getLocation());
            for (final TimeCoord tc : inv.getTimeCoords()) {
                debug.format("  %s %n", tc);
            }
        }
        for (final TimeCoord tc : inv.getTimeCoords()) {
            for (final GridDatasetInv.Grid grid : tc.getGridInventory()) {
                GridVariable uv = this.uvHash.get(grid.getName());
                if (uv == null) {
                    uv = new GridVariable(grid.getName());
                    this.uvHash.put(grid.getName(), uv);
                }
                uv.addGridDatasetInv(grid);
            }
        }
    }
    
    void finish() {
        Collections.sort(this.gridList = new ArrayList<GridVariable>(this.uvHash.values()));
        for (final GridVariable grid : this.gridList) {
            grid.finish();
        }
        int seqno = 0;
        for (final TimeCoord tc : this.timeCoords) {
            tc.setId(seqno++);
        }
    }
    
    public int compareTo(final FmrInv fmr) {
        return this.runtime.compareTo(fmr.getRunDate());
    }
    
    public Set<GridDatasetInv> getFiles() {
        final HashSet<GridDatasetInv> fileSet = new HashSet<GridDatasetInv>();
        for (final GridVariable grid : this.getGrids()) {
            for (final GridDatasetInv.Grid inv : grid.getInventory()) {
                fileSet.add(inv.getFile());
            }
        }
        return fileSet;
    }
    
    public class GridVariable implements Comparable
    {
        private final String name;
        private final List<GridDatasetInv.Grid> gridList;
        VertCoord vertCoordUnion;
        EnsCoord ensCoordUnion;
        TimeCoord timeCoordUnion;
        TimeCoord timeExpected;
        
        GridVariable(final String name) {
            this.gridList = new ArrayList<GridDatasetInv.Grid>();
            this.vertCoordUnion = null;
            this.ensCoordUnion = null;
            this.timeCoordUnion = null;
            this.timeExpected = null;
            this.name = name;
        }
        
        public String getName() {
            return this.name;
        }
        
        public Date getRunDate() {
            return FmrInv.this.getRunDate();
        }
        
        void addGridDatasetInv(final GridDatasetInv.Grid grid) {
            this.gridList.add(grid);
        }
        
        public List<GridDatasetInv.Grid> getInventory() {
            return this.gridList;
        }
        
        public TimeCoord getTimeExpected() {
            return this.timeExpected;
        }
        
        public TimeCoord getTimeCoord() {
            return this.timeCoordUnion;
        }
        
        public int compareTo(final Object o) {
            final GridVariable uv = (GridVariable)o;
            return this.name.compareTo(uv.name);
        }
        
        public int getNVerts() {
            return (this.vertCoordUnion == null) ? 1 : this.vertCoordUnion.getSize();
        }
        
        public int countTotal() {
            int total = 0;
            for (final GridDatasetInv.Grid grid : this.gridList) {
                total += grid.countTotal();
            }
            return total;
        }
        
        void finish() {
            if (this.gridList.size() == 1) {
                final GridDatasetInv.Grid grid = this.gridList.get(0);
                this.ensCoordUnion = EnsCoord.findEnsCoord(FmrInv.this.getEnsCoords(), grid.ec);
                this.vertCoordUnion = VertCoord.findVertCoord(FmrInv.this.getVertCoords(), grid.vc);
                this.timeCoordUnion = TimeCoord.findTimeCoord(FmrInv.this.getTimeCoords(), grid.tc);
                return;
            }
            final List<EnsCoord> ensList = new ArrayList<EnsCoord>();
            EnsCoord ec_union = null;
            for (final GridDatasetInv.Grid grid2 : this.gridList) {
                final EnsCoord ec = grid2.ec;
                if (ec == null) {
                    continue;
                }
                if (ec_union == null) {
                    ec_union = new EnsCoord(ec);
                }
                else {
                    if (ec_union.equalsData(ec)) {
                        continue;
                    }
                    ensList.add(ec);
                }
            }
            if (ec_union != null) {
                if (ensList.size() > 0) {
                    EnsCoord.normalize(ec_union, ensList);
                }
                this.ensCoordUnion = EnsCoord.findEnsCoord(FmrInv.this.getEnsCoords(), ec_union);
            }
            final List<VertCoord> vertList = new ArrayList<VertCoord>();
            VertCoord vc_union = null;
            for (final GridDatasetInv.Grid grid3 : this.gridList) {
                final VertCoord vc = grid3.vc;
                if (vc == null) {
                    continue;
                }
                if (vc_union == null) {
                    vc_union = new VertCoord(vc);
                }
                else {
                    if (vc_union.equalsData(vc)) {
                        continue;
                    }
                    System.out.printf("GridVariable %s has different vert coords in file %s %n", grid3.getName(), grid3.getFile());
                    vertList.add(vc);
                }
            }
            if (vc_union != null) {
                if (vertList.size() > 0) {
                    VertCoord.normalize(vc_union, vertList);
                }
                this.vertCoordUnion = VertCoord.findVertCoord(FmrInv.this.getVertCoords(), vc_union);
            }
            final List<TimeCoord> timeList = new ArrayList<TimeCoord>();
            for (final GridDatasetInv.Grid grid4 : this.gridList) {
                final TimeCoord tc = grid4.tc;
                timeList.add(tc);
            }
            final TimeCoord tc_union = TimeCoord.makeUnion(timeList, this.getRunDate());
            this.timeCoordUnion = TimeCoord.findTimeCoord(FmrInv.this.getTimeCoords(), tc_union);
        }
    }
}
