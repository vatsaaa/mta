// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.fmrc;

import java.util.Comparator;
import org.slf4j.LoggerFactory;
import java.util.TimeZone;
import java.util.Iterator;
import java.util.Set;
import java.util.Map;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import net.jcip.annotations.Immutable;

@Immutable
public class FmrcInv
{
    private static Logger log;
    private final String name;
    private final List<RunSeq> runSeqs;
    private final List<EnsCoord> ensCoords;
    private final List<VertCoord> vertCoords;
    private final List<FmrInv> fmrList;
    private final List<UberGrid> uberGridList;
    private final List<Date> runTimeList;
    private final Date baseDate;
    private final List<Date> forecastTimeList;
    private Calendar cal;
    private final boolean regularize;
    private final TimeCoord tcOffAll;
    private final TimeCoord tcIntAll;
    
    public static Date addHour(final Date d, final double hour) {
        long msecs = d.getTime();
        msecs += (long)(hour * 3600.0 * 1000.0);
        return new Date(msecs);
    }
    
    FmrcInv(final String name, final List<FmrInv> fmrList, final boolean regularize) {
        this.runSeqs = new ArrayList<RunSeq>();
        this.ensCoords = new ArrayList<EnsCoord>();
        this.vertCoords = new ArrayList<VertCoord>();
        this.name = name;
        this.regularize = regularize;
        this.fmrList = new ArrayList<FmrInv>(fmrList);
        this.runTimeList = new ArrayList<Date>();
        Date firstDate = null;
        final Map<String, UberGrid> uvHash = new HashMap<String, UberGrid>();
        final Set<Double> offsetHash = new HashSet<Double>();
        final Set<TimeCoord.Tinv> intervalHash = new HashSet<TimeCoord.Tinv>();
        final Set<Date> forecastTimeHash = new HashSet<Date>();
        for (final FmrInv fmrInv : fmrList) {
            this.runTimeList.add(fmrInv.getRunDate());
            if (firstDate == null) {
                firstDate = fmrInv.getRunDate();
            }
            final int hour = this.getHour(fmrInv.getRunDate());
            for (final FmrInv.GridVariable fmrGrid : fmrInv.getGrids()) {
                UberGrid uv = uvHash.get(fmrGrid.getName());
                if (uv == null) {
                    uv = new UberGrid(fmrGrid.getName());
                    uvHash.put(fmrGrid.getName(), uv);
                }
                uv.addGridVariable(fmrGrid, hour);
            }
            for (final TimeCoord tc : fmrInv.getTimeCoords()) {
                if (tc.isInterval()) {
                    final double[] bounds1 = tc.getBound1();
                    final double[] bounds2 = tc.getBound2();
                    for (int i = 0; i < bounds1.length; ++i) {
                        final Date date1 = addHour(fmrInv.getRunDate(), bounds1[i]);
                        final Date date2 = addHour(fmrInv.getRunDate(), bounds2[i]);
                        forecastTimeHash.add(date2);
                        final double b1 = getOffsetInHours(firstDate, date1);
                        final double b2 = getOffsetInHours(firstDate, date2);
                        intervalHash.add(new TimeCoord.Tinv(b1, b2));
                    }
                }
                else {
                    for (final double offset : tc.getOffsetTimes()) {
                        final Date fcDate = addHour(fmrInv.getRunDate(), offset);
                        forecastTimeHash.add(fcDate);
                        final double d = getOffsetInHours(firstDate, fcDate);
                        offsetHash.add(d);
                    }
                }
            }
        }
        this.baseDate = firstDate;
        Collections.sort(this.uberGridList = new ArrayList<UberGrid>(uvHash.values()));
        for (final UberGrid uv2 : this.uberGridList) {
            uv2.finish();
        }
        int seqno = 0;
        for (final RunSeq seq : this.runSeqs) {
            seq.id = seqno++;
        }
        Collections.sort(this.forecastTimeList = Arrays.asList((Date[])forecastTimeHash.toArray((T[])new Date[forecastTimeHash.size()])));
        final List<Double> offsetsAll = Arrays.asList((Double[])offsetHash.toArray((T[])new Double[offsetHash.size()]));
        Collections.sort(offsetsAll);
        int counto = 0;
        final double[] offs = new double[offsetsAll.size()];
        for (final double off : offsetsAll) {
            offs[counto++] = off;
        }
        (this.tcOffAll = new TimeCoord(this.baseDate)).setOffsetTimes(offs);
        final List<TimeCoord.Tinv> intervalAll = Arrays.asList((TimeCoord.Tinv[])intervalHash.toArray((T[])new TimeCoord.Tinv[intervalHash.size()]));
        Collections.sort(intervalAll);
        (this.tcIntAll = new TimeCoord(this.baseDate)).setBounds(intervalAll);
    }
    
    public List<FmrInv> getFmrList() {
        return this.fmrList;
    }
    
    private int getHour(final Date d) {
        if (this.cal == null) {
            (this.cal = Calendar.getInstance()).setTimeZone(TimeZone.getTimeZone("GMT"));
        }
        this.cal.setTime(d);
        return this.cal.get(11);
    }
    
    public String getName() {
        return this.name;
    }
    
    public List<RunSeq> getRunSeqs() {
        return this.runSeqs;
    }
    
    public List<EnsCoord> getEnsCoords() {
        return this.ensCoords;
    }
    
    public List<VertCoord> getVertCoords() {
        return this.vertCoords;
    }
    
    public List<UberGrid> getUberGrids() {
        return this.uberGridList;
    }
    
    public UberGrid findUberGrid(final String name) {
        for (final UberGrid grid : this.uberGridList) {
            if (grid.getName().equals(name)) {
                return grid;
            }
        }
        return null;
    }
    
    public List<Date> getForecastTimes() {
        return this.forecastTimeList;
    }
    
    public List<FmrInv> getFmrInv() {
        return this.fmrList;
    }
    
    public Date getBaseDate() {
        return this.baseDate;
    }
    
    public static double getOffsetInHours(final Date base, final Date forecast) {
        final double diff = (double)(forecast.getTime() - base.getTime());
        return diff / 1000.0 / 60.0 / 60.0;
    }
    
    public static Date makeOffsetDate(final Date base, final double offset) {
        final long time = base.getTime() + (long)(offset * 60.0 * 60.0 * 1000.0);
        return new Date(time);
    }
    
    private RunSeq findRunSeq(final List<FmrInv.GridVariable> runs) {
        for (final RunSeq seq : this.runSeqs) {
            if (seq.equalsData(runs)) {
                return seq;
            }
        }
        final RunSeq result = new RunSeq(runs);
        this.runSeqs.add(result);
        return result;
    }
    
    static {
        FmrcInv.log = LoggerFactory.getLogger(FmrcInv.class);
    }
    
    public class UberGrid implements Comparable<UberGrid>
    {
        private final String gridName;
        private final List<FmrInv.GridVariable> runs;
        private VertCoord vertCoordUnion;
        private EnsCoord ensCoordUnion;
        private RunSeq runSeq;
        
        UberGrid(final String name) {
            this.runs = new ArrayList<FmrInv.GridVariable>();
            this.vertCoordUnion = null;
            this.ensCoordUnion = null;
            this.runSeq = null;
            this.gridName = name;
        }
        
        void addGridVariable(final FmrInv.GridVariable grid, final int hour) {
            this.runs.add(grid);
        }
        
        public String getName() {
            return this.gridName;
        }
        
        @Override
        public String toString() {
            return this.gridName;
        }
        
        public TimeCoord getUnionTimeCoord() {
            return this.runSeq.getUnionTimeCoord();
        }
        
        public boolean isInterval() {
            return this.getUnionTimeCoord().isInterval();
        }
        
        public String getTimeCoordName() {
            return this.runSeq.getName();
        }
        
        public String getVertCoordName() {
            return (this.vertCoordUnion == null) ? "" : this.vertCoordUnion.getName();
        }
        
        public List<FmrInv.GridVariable> getRuns() {
            return this.runs;
        }
        
        public int compareTo(final UberGrid o) {
            return this.gridName.compareTo(o.gridName);
        }
        
        public int countTotal() {
            int total = 0;
            for (final FmrInv.GridVariable grid : this.runs) {
                total += grid.countTotal();
            }
            return total;
        }
        
        public int countExpected() {
            final int nvert = (this.vertCoordUnion == null) ? 1 : this.vertCoordUnion.getSize();
            int ntimes = 0;
            for (final FmrInv.GridVariable grid : this.runs) {
                final TimeCoord exp = grid.getTimeExpected();
                ntimes += exp.getNCoords();
            }
            return ntimes * nvert;
        }
        
        void finish() {
            if (this.runs.size() == 1) {
                final FmrInv.GridVariable grid = this.runs.get(0);
                this.ensCoordUnion = EnsCoord.findEnsCoord(FmrcInv.this.getEnsCoords(), grid.ensCoordUnion);
                this.vertCoordUnion = VertCoord.findVertCoord(FmrcInv.this.getVertCoords(), grid.vertCoordUnion);
                grid.timeExpected = grid.timeCoordUnion;
                (this.runSeq = FmrcInv.this.findRunSeq(this.runs)).addVariable(this);
                return;
            }
            final List<EnsCoord> ensList = new ArrayList<EnsCoord>();
            EnsCoord ec_union = null;
            for (final FmrInv.GridVariable grid2 : this.runs) {
                final EnsCoord ec = grid2.ensCoordUnion;
                if (ec == null) {
                    continue;
                }
                if (ec_union == null) {
                    ec_union = new EnsCoord(ec);
                }
                else {
                    if (ec_union.equalsData(ec)) {
                        continue;
                    }
                    ensList.add(ec);
                }
            }
            if (ec_union != null) {
                if (ensList.size() > 0) {
                    EnsCoord.normalize(ec_union, ensList);
                }
                this.ensCoordUnion = EnsCoord.findEnsCoord(FmrcInv.this.getEnsCoords(), ec_union);
            }
            final List<VertCoord> vertList = new ArrayList<VertCoord>();
            VertCoord vc_union = null;
            for (final FmrInv.GridVariable grid3 : this.runs) {
                final VertCoord vc = grid3.vertCoordUnion;
                if (vc == null) {
                    continue;
                }
                if (vc_union == null) {
                    vc_union = new VertCoord(vc);
                }
                else {
                    if (vc_union.equalsData(vc)) {
                        continue;
                    }
                    FmrcInv.log.warn(FmrcInv.this.name + " Grid " + this.gridName + " has different vert coords in run " + grid3.getRunDate());
                    vertList.add(vc);
                }
            }
            if (vc_union != null) {
                if (vertList.size() > 0) {
                    VertCoord.normalize(vc_union, vertList);
                }
                this.vertCoordUnion = VertCoord.findVertCoord(FmrcInv.this.getVertCoords(), vc_union);
            }
            if (FmrcInv.this.regularize) {
                final Map<Integer, HourGroup> hourMap = new HashMap<Integer, HourGroup>();
                for (final FmrInv.GridVariable grid4 : this.runs) {
                    final Date runDate = grid4.getRunDate();
                    final int hour = FmrcInv.this.getHour(runDate);
                    HourGroup hg = hourMap.get(hour);
                    if (hg == null) {
                        hg = new HourGroup(hour);
                        hourMap.put(hour, hg);
                    }
                    hg.runs.add(grid4);
                }
                for (final HourGroup hg2 : hourMap.values()) {
                    final List<TimeCoord> timeListExp = new ArrayList<TimeCoord>();
                    for (final FmrInv.GridVariable run : hg2.runs) {
                        timeListExp.add(run.timeCoordUnion);
                    }
                    hg2.expected = TimeCoord.makeUnion(timeListExp, FmrcInv.this.baseDate);
                    if (hg2.expected.isInterval()) {
                        for (final FmrInv.GridVariable grid5 : hg2.runs) {
                            (grid5.timeExpected = new TimeCoord(grid5.getRunDate())).setBounds(hg2.expected.getBound1(), hg2.expected.getBound2());
                        }
                    }
                    else {
                        for (final FmrInv.GridVariable grid5 : hg2.runs) {
                            grid5.timeExpected = new TimeCoord(grid5.getRunDate(), hg2.expected.getOffsetTimes());
                        }
                    }
                }
                (this.runSeq = FmrcInv.this.findRunSeq(this.runs)).addVariable(this);
            }
            else {
                for (final FmrInv.GridVariable grid3 : this.runs) {
                    grid3.timeExpected = grid3.timeCoordUnion;
                }
                (this.runSeq = FmrcInv.this.findRunSeq(this.runs)).addVariable(this);
            }
        }
    }
    
    private class HourGroup
    {
        final int hour;
        final List<FmrInv.GridVariable> runs;
        private TimeCoord expected;
        
        HourGroup(final int hour) {
            this.runs = new ArrayList<FmrInv.GridVariable>();
            this.hour = hour;
        }
    }
    
    public class RunSeq
    {
        private final HashMap<Date, TimeCoord> coordMap;
        private final List<UberGrid> vars;
        private int id;
        private List<TimeCoord> timeList;
        private TimeCoord timeCoordUnion;
        private boolean isInterval;
        
        RunSeq(final List<FmrInv.GridVariable> runs) {
            this.vars = new ArrayList<UberGrid>();
            this.timeList = null;
            this.timeCoordUnion = null;
            this.coordMap = new HashMap<Date, TimeCoord>(2 * runs.size());
            for (final Date d : FmrcInv.this.runTimeList) {
                this.coordMap.put(d, TimeCoord.EMPTY);
            }
            boolean first = true;
            for (final FmrInv.GridVariable grid : runs) {
                this.coordMap.put(grid.getRunDate(), grid.getTimeExpected());
                if (first) {
                    this.isInterval = grid.getTimeCoord().isInterval();
                }
                else if (this.isInterval != grid.getTimeCoord().isInterval()) {
                    FmrcInv.log.error("mixed intervals for grid " + grid.getName());
                    throw new IllegalArgumentException("mixed intervals for grid " + grid.getName());
                }
                first = false;
            }
        }
        
        public boolean isInterval() {
            return this.isInterval;
        }
        
        public List<TimeCoord> getTimes() {
            if (this.timeList == null) {
                this.getUnionTimeCoord();
            }
            return this.timeList;
        }
        
        public String getName() {
            return (this.id == 0) ? "time" : ("time" + this.id);
        }
        
        public int getNTimeOffsets() {
            int n = 0;
            for (final TimeCoord tc : this.coordMap.values()) {
                n = Math.max(n, tc.getNCoords());
            }
            return n;
        }
        
        public TimeCoord getUnionTimeCoord() {
            if (this.timeCoordUnion == null) {
                this.timeList = new ArrayList<TimeCoord>();
                for (final TimeCoord tc : this.coordMap.values()) {
                    if (tc != null && tc != TimeCoord.EMPTY) {
                        this.timeList.add(tc);
                    }
                }
                Collections.sort(this.timeList, new Comparator<TimeCoord>() {
                    public int compare(final TimeCoord o1, final TimeCoord o2) {
                        if (o1 == null || o1.getRunDate() == null) {
                            return -1;
                        }
                        if (o2 == null || o2.getRunDate() == null) {
                            return 1;
                        }
                        return o1.getRunDate().compareTo(o2.getRunDate());
                    }
                });
                this.timeCoordUnion = TimeCoord.makeUnion(this.timeList, FmrcInv.this.baseDate);
            }
            return this.timeCoordUnion;
        }
        
        boolean equalsData(final List<FmrInv.GridVariable> oruns) {
            List<FmrInv.GridVariable> okAdd = null;
            for (final FmrInv.GridVariable grid : oruns) {
                final TimeCoord run = this.coordMap.get(grid.getRunDate());
                if (run == null) {
                    if (okAdd == null) {
                        okAdd = new ArrayList<FmrInv.GridVariable>();
                    }
                    okAdd.add(grid);
                }
                else {
                    final TimeCoord orune = grid.getTimeExpected();
                    if (!run.equalsData(orune)) {
                        return false;
                    }
                    continue;
                }
            }
            if (okAdd != null) {
                for (final FmrInv.GridVariable grid : okAdd) {
                    this.coordMap.put(grid.getRunDate(), grid.getTimeExpected());
                }
            }
            return true;
        }
        
        void addVariable(final UberGrid uv) {
            this.vars.add(uv);
        }
        
        public List<UberGrid> getUberGrids() {
            return this.vars;
        }
    }
}
