// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.fmrc;

import org.slf4j.LoggerFactory;
import java.util.StringTokenizer;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.util.Formatter;
import org.jdom.Content;
import java.util.Collections;
import org.jdom.Document;
import org.jdom.output.XMLOutputter;
import org.jdom.output.Format;
import ucar.nc2.dataset.CoordinateAxis1D;
import ucar.nc2.dataset.CoordinateAxis1DTime;
import ucar.nc2.dt.GridCoordSystem;
import java.util.Iterator;
import ucar.nc2.dt.GridDatatype;
import ucar.nc2.units.DateFormatter;
import ucar.nc2.units.DateUnit;
import ucar.nc2.Variable;
import java.util.ArrayList;
import java.io.IOException;
import ucar.nc2.NetcdfFile;
import ucar.nc2.ncml.NcMLReader;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dt.grid.GridDataset;
import org.jdom.Element;
import thredds.inventory.MFile;
import thredds.inventory.CollectionManager;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;

public class GridDatasetInv
{
    private static final Logger log;
    private static final int REQ_VERSION = 1;
    private static final int CURR_VERSION = 1;
    private static boolean debug;
    private String location;
    private int version;
    private final List<TimeCoord> times;
    private final List<VertCoord> vaxes;
    private final List<EnsCoord> eaxes;
    private Date runDate;
    private String runTime;
    private Date lastModified;
    
    public static GridDatasetInv open(final CollectionManager cm, final MFile mfile, final Element ncml) throws IOException {
        final byte[] xmlBytes = cm.getMetadata(mfile, "fmrInv.xml");
        if (xmlBytes != null) {
            if (GridDatasetInv.log.isDebugEnabled()) {
                GridDatasetInv.log.debug(" got xmlFile in cache =" + mfile.getPath() + " size = " + xmlBytes.length);
            }
            if (xmlBytes.length < 300) {
                GridDatasetInv.log.warn(" xmlFile in cache only has nbytes =" + xmlBytes.length + "; will reread");
            }
            else {
                final GridDatasetInv inv = readXML(xmlBytes);
                if (inv.version >= 1) {
                    final long fileModifiedSecs = mfile.getLastModified() / 1000L;
                    final long xmlModifiedSecs = inv.getLastModified() / 1000L;
                    if (xmlModifiedSecs >= fileModifiedSecs) {
                        return inv;
                    }
                    if (GridDatasetInv.log.isInfoEnabled()) {
                        GridDatasetInv.log.info(" cache out of date " + new Date(inv.getLastModified()) + " < " + new Date(mfile.getLastModified()) + " for " + mfile.getName());
                    }
                }
                else if (GridDatasetInv.log.isInfoEnabled()) {
                    GridDatasetInv.log.info(" version needs upgrade " + inv.version + " < " + 1 + " for " + mfile.getName());
                }
            }
        }
        GridDataset gds = null;
        try {
            if (ncml == null) {
                gds = GridDataset.open(mfile.getPath());
            }
            else {
                final NetcdfFile nc = NetcdfDataset.acquireFile(mfile.getPath(), null);
                final NetcdfDataset ncd = NcMLReader.mergeNcML(nc, ncml);
                ncd.enhance();
                gds = new GridDataset(ncd);
            }
            final GridDatasetInv inv2 = new GridDatasetInv(gds, cm.extractRunDate(mfile));
            final String xmlString = inv2.writeXML(new Date(mfile.getLastModified()));
            cm.putMetadata(mfile, "fmrInv.xml", xmlString.getBytes("UTF-8"));
            if (GridDatasetInv.log.isDebugEnabled()) {
                GridDatasetInv.log.debug(" added xmlFile " + mfile.getPath() + ".fmrInv.xml to cache");
            }
            if (GridDatasetInv.debug) {
                System.out.printf(" added xmlFile %s.fmrInv.xml to cache%n", mfile.getPath());
            }
            return inv2;
        }
        finally {
            if (gds != null) {
                gds.close();
            }
        }
    }
    
    private GridDatasetInv() {
        this.times = new ArrayList<TimeCoord>();
        this.vaxes = new ArrayList<VertCoord>();
        this.eaxes = new ArrayList<EnsCoord>();
    }
    
    public GridDatasetInv(final ucar.nc2.dt.GridDataset gds, final Date runDate) {
        this.times = new ArrayList<TimeCoord>();
        this.vaxes = new ArrayList<VertCoord>();
        this.eaxes = new ArrayList<EnsCoord>();
        this.location = gds.getLocationURI();
        final NetcdfFile ncfile = gds.getNetcdfFile();
        if (runDate == null) {
            this.runTime = ncfile.findAttValueIgnoreCase(null, "_CoordinateModelBaseDate", null);
            if (this.runTime == null) {
                this.runTime = ncfile.findAttValueIgnoreCase(null, "_CoordinateModelRunDate", null);
            }
            if (this.runTime == null) {
                GridDatasetInv.log.error("GridDatasetInv missing rundate in file=" + this.location);
                throw new IllegalArgumentException("File must have _CoordinateModelBaseDate or _CoordinateModelRunDate attribute ");
            }
            this.runDate = DateUnit.getStandardOrISO(this.runTime);
            if (this.runDate == null) {
                GridDatasetInv.log.error("GridDatasetInv rundate not ISO date string (%s) file=%s", this.runTime, this.location);
                throw new IllegalArgumentException("_CoordinateModelRunDate must be ISO date string " + this.runTime);
            }
        }
        else {
            this.runDate = runDate;
            final DateFormatter df = new DateFormatter();
            this.runTime = df.toDateTimeStringISO(runDate);
        }
        for (final GridDatatype gg : gds.getGrids()) {
            final GridCoordSystem gcs = gg.getCoordinateSystem();
            final Grid grid = new Grid(gg.getName());
            final CoordinateAxis1DTime axis = gcs.getTimeAxis1D();
            if (axis != null) {
                final TimeCoord tc = this.getTimeCoordinate(axis);
                tc.addGridInventory(grid);
                grid.tc = tc;
            }
            final CoordinateAxis1D vaxis = gcs.getVerticalAxis();
            if (vaxis != null) {
                grid.vc = this.getVertCoordinate(vaxis);
            }
        }
        int seqno = 0;
        for (final TimeCoord tc2 : this.times) {
            tc2.setId(seqno++);
        }
    }
    
    @Override
    public String toString() {
        return this.location;
    }
    
    public String getLocation() {
        return this.location;
    }
    
    public long getLastModified() {
        return this.lastModified.getTime();
    }
    
    public Date getRunDate() {
        return this.runDate;
    }
    
    public String getRunDateString() {
        return this.runTime;
    }
    
    public List<TimeCoord> getTimeCoords() {
        return this.times;
    }
    
    public List<VertCoord> getVertCoords() {
        return this.vaxes;
    }
    
    public Grid findGrid(final String name) {
        for (final TimeCoord tc : this.times) {
            final List<Grid> grids = tc.getGridInventory();
            for (final Grid g : grids) {
                if (g.name.equals(name)) {
                    return g;
                }
            }
        }
        return null;
    }
    
    private TimeCoord getTimeCoordinate(final CoordinateAxis1DTime axis) {
        for (final TimeCoord tc : this.times) {
            if (tc.getAxisName().equals(axis.getName())) {
                return tc;
            }
        }
        final TimeCoord want = new TimeCoord(this.runDate, axis);
        for (final TimeCoord tc2 : this.times) {
            if (tc2.equalsData(want)) {
                return tc2;
            }
        }
        this.times.add(want);
        return want;
    }
    
    Grid makeGrid(final String gridName) {
        return new Grid(gridName);
    }
    
    private VertCoord getVertCoordinate(final int wantId) {
        if (wantId < 0) {
            return null;
        }
        for (final VertCoord vc : this.vaxes) {
            if (vc.getId() == wantId) {
                return vc;
            }
        }
        return null;
    }
    
    private VertCoord getVertCoordinate(final CoordinateAxis1D axis) {
        for (final VertCoord vc : this.vaxes) {
            if (vc.getName().equals(axis.getName())) {
                return vc;
            }
        }
        final VertCoord want = new VertCoord(axis);
        for (final VertCoord vc2 : this.vaxes) {
            if (vc2.equalsData(want)) {
                return vc2;
            }
        }
        this.vaxes.add(want);
        return want;
    }
    
    private EnsCoord getEnsCoordinate(final int ens_id) {
        if (ens_id < 0) {
            return null;
        }
        for (final EnsCoord ec : this.eaxes) {
            if (ec.getId() == ens_id) {
                return ec;
            }
        }
        return null;
    }
    
    private EnsCoord getEnsCoordinate(final CoordinateAxis1D axis) {
        for (final EnsCoord ec : this.eaxes) {
            if (ec.getName().equals(axis.getName())) {
                return ec;
            }
        }
        final EnsCoord want = new EnsCoord(axis, null);
        for (final EnsCoord ec2 : this.eaxes) {
            if (ec2.equalsData(want)) {
                return ec2;
            }
        }
        this.eaxes.add(want);
        return want;
    }
    
    public String writeXML(final Date lastModified) {
        final XMLOutputter fmt = new XMLOutputter(Format.getPrettyFormat());
        return fmt.outputString(this.writeDocument(lastModified));
    }
    
    Document writeDocument(final Date lastModified) {
        final Element rootElem = new Element("gridInventory");
        final Document doc = new Document(rootElem);
        rootElem.setAttribute("location", this.location);
        rootElem.setAttribute("runTime", this.runTime);
        if (lastModified != null) {
            final DateFormatter df = new DateFormatter();
            rootElem.setAttribute("lastModified", df.toDateTimeString(lastModified));
        }
        rootElem.setAttribute("version", Integer.toString(1));
        Collections.sort(this.vaxes);
        int count = 0;
        for (final VertCoord vc : this.vaxes) {
            vc.setId(count++);
            final Element vcElem = new Element("vertCoord");
            rootElem.addContent(vcElem);
            vcElem.setAttribute("id", Integer.toString(vc.getId()));
            vcElem.setAttribute("name", vc.getName());
            if (vc.getUnits() != null) {
                vcElem.setAttribute("units", vc.getUnits());
            }
            final StringBuilder sbuff = new StringBuilder();
            final double[] values1 = vc.getValues1();
            final double[] values2 = vc.getValues2();
            for (int j = 0; j < values1.length; ++j) {
                if (j > 0) {
                    sbuff.append(" ");
                }
                sbuff.append(Double.toString(values1[j]));
                if (values2 != null) {
                    sbuff.append(",");
                    sbuff.append(Double.toString(values2[j]));
                }
            }
            vcElem.addContent(sbuff.toString());
        }
        count = 0;
        for (final TimeCoord tc : this.times) {
            tc.setId(count++);
            final Element timeElement = new Element("timeCoord");
            rootElem.addContent(timeElement);
            timeElement.setAttribute("id", Integer.toString(tc.getId()));
            timeElement.setAttribute("name", tc.getName());
            timeElement.setAttribute("isInterval", tc.isInterval() ? "true" : "false");
            final Formatter sbuff2 = new Formatter();
            if (tc.isInterval()) {
                final double[] bound1 = tc.getBound1();
                final double[] bound2 = tc.getBound2();
                for (int j = 0; j < bound1.length; ++j) {
                    sbuff2.format("%f %f,", bound1[j], bound2[j]);
                }
            }
            else {
                for (final double offset : tc.getOffsetTimes()) {
                    sbuff2.format("%f,", offset);
                }
            }
            timeElement.addContent(sbuff2.toString());
            final List<Grid> vars = tc.getGridInventory();
            Collections.sort(vars);
            for (final Grid grid : vars) {
                final Element varElem = new Element("grid");
                timeElement.addContent(varElem);
                varElem.setAttribute("name", grid.name);
                if (grid.ec != null) {
                    varElem.setAttribute("ens_id", Integer.toString(grid.ec.getId()));
                }
                if (grid.vc != null) {
                    varElem.setAttribute("vert_id", Integer.toString(grid.vc.getId()));
                }
            }
        }
        return doc;
    }
    
    private static GridDatasetInv readXML(final byte[] xmlString) throws IOException {
        final InputStream is = new BufferedInputStream(new ByteArrayInputStream(xmlString));
        Document doc;
        try {
            final SAXBuilder builder = new SAXBuilder();
            doc = builder.build(is);
        }
        catch (JDOMException e) {
            throw new IOException(e.getMessage() + " reading from XML ");
        }
        final Element rootElem = doc.getRootElement();
        final GridDatasetInv fmr = new GridDatasetInv();
        fmr.runTime = rootElem.getAttributeValue("runTime");
        fmr.location = rootElem.getAttributeValue("location");
        if (fmr.location == null) {
            fmr.location = rootElem.getAttributeValue("name");
        }
        final String lastModifiedS = rootElem.getAttributeValue("lastModified");
        final DateFormatter df = new DateFormatter();
        if (lastModifiedS != null) {
            fmr.lastModified = df.getISODate(lastModifiedS);
        }
        final String version = rootElem.getAttributeValue("version");
        fmr.version = ((version == null) ? 0 : Integer.parseInt(version));
        if (fmr.version < 1) {
            return fmr;
        }
        final DateFormatter formatter = new DateFormatter();
        fmr.runDate = formatter.getISODate(fmr.runTime);
        final List<Element> vList = (List<Element>)rootElem.getChildren("vertCoord");
        for (final Element vertElem : vList) {
            final VertCoord vc = new VertCoord();
            fmr.vaxes.add(vc);
            vc.setId(Integer.parseInt(vertElem.getAttributeValue("id")));
            vc.setName(vertElem.getAttributeValue("name"));
            vc.setUnits(vertElem.getAttributeValue("units"));
            final String values = vertElem.getTextNormalize();
            final StringTokenizer stoke = new StringTokenizer(values);
            final int n = stoke.countTokens();
            final double[] values2 = new double[n];
            double[] values3 = null;
            int count = 0;
            while (stoke.hasMoreTokens()) {
                final String toke = stoke.nextToken();
                final int pos = toke.indexOf(44);
                if (pos < 0) {
                    values2[count] = Double.parseDouble(toke);
                }
                else {
                    if (values3 == null) {
                        values3 = new double[n];
                    }
                    final String val1 = toke.substring(0, pos);
                    final String val2 = toke.substring(pos + 1);
                    values2[count] = Double.parseDouble(val1);
                    values3[count] = Double.parseDouble(val2);
                }
                ++count;
            }
            vc.setValues1(values2);
            vc.setValues2(values3);
        }
        final List<Element> tList = (List<Element>)rootElem.getChildren("timeCoord");
        for (final Element timeElem : tList) {
            final TimeCoord tc = new TimeCoord(fmr.runDate);
            fmr.times.add(tc);
            tc.setId(Integer.parseInt(timeElem.getAttributeValue("id")));
            final String s = timeElem.getAttributeValue("isInterval");
            final boolean isInterval = s != null && s.equals("true");
            if (isInterval) {
                final String boundsAll = timeElem.getTextNormalize();
                final String[] bounds = boundsAll.split(",");
                final int n2 = bounds.length;
                final double[] bound1 = new double[n2];
                final double[] bound2 = new double[n2];
                int count2 = 0;
                for (final String b : bounds) {
                    final String[] value = b.split(" ");
                    bound1[count2] = Double.parseDouble(value[0]);
                    bound2[count2] = Double.parseDouble(value[1]);
                    ++count2;
                }
                tc.setBounds(bound1, bound2);
            }
            else {
                final String values4 = timeElem.getTextNormalize();
                final String[] value2 = values4.split(",");
                final int n2 = value2.length;
                final double[] offsets = new double[n2];
                int count3 = 0;
                for (final String v : value2) {
                    offsets[count3++] = Double.parseDouble(v);
                }
                tc.setOffsetTimes(offsets);
            }
            final List<Element> varList = (List<Element>)timeElem.getChildren("grid");
            for (final Element vElem : varList) {
                final Grid grid = fmr.makeGrid(vElem.getAttributeValue("name"));
                if (vElem.getAttributeValue("ens_id") != null) {
                    grid.ec = fmr.getEnsCoordinate(Integer.parseInt(vElem.getAttributeValue("ens_id")));
                }
                if (vElem.getAttributeValue("vert_id") != null) {
                    grid.vc = fmr.getVertCoordinate(Integer.parseInt(vElem.getAttributeValue("vert_id")));
                }
                tc.addGridInventory(grid);
                grid.tc = tc;
            }
        }
        return fmr;
    }
    
    public static void main(final String[] args) {
        final String values = "1,2,3,4";
        final String[] arr$;
        final String[] value = arr$ = values.split("[,]");
        for (final String s : arr$) {
            System.out.printf("%s%n", s);
        }
    }
    
    static {
        log = LoggerFactory.getLogger(GridDatasetInv.class);
        GridDatasetInv.debug = false;
    }
    
    public class Grid implements Comparable
    {
        final String name;
        TimeCoord tc;
        EnsCoord ec;
        VertCoord vc;
        
        private Grid(final String name) {
            this.tc = null;
            this.ec = null;
            this.vc = null;
            this.name = name;
        }
        
        public String getName() {
            return this.name;
        }
        
        public String getLocation() {
            return GridDatasetInv.this.location;
        }
        
        public String getTimeCoordName() {
            return (this.tc == null) ? "" : this.tc.getName();
        }
        
        public String getVertCoordName() {
            return (this.vc == null) ? "" : this.vc.getName();
        }
        
        public int compareTo(final Object o) {
            final Grid other = (Grid)o;
            return this.name.compareTo(other.name);
        }
        
        public int countTotal() {
            final int ntimes = this.tc.getNCoords();
            return ntimes * this.getVertCoordLength();
        }
        
        @Override
        public String toString() {
            return this.name;
        }
        
        public int getVertCoordLength() {
            return (this.vc == null) ? 1 : this.vc.getValues1().length;
        }
        
        public TimeCoord getTimeCoord() {
            return this.tc;
        }
        
        public GridDatasetInv getFile() {
            return GridDatasetInv.this;
        }
    }
}
