// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.fmrc;

import java.util.Collection;
import java.util.Collections;
import java.util.Arrays;
import ucar.nc2.util.Misc;
import ucar.nc2.units.DateFormatter;
import org.slf4j.LoggerFactory;
import java.io.FileNotFoundException;
import thredds.inventory.FeatureCollectionConfig;
import java.util.Formatter;
import java.util.TreeSet;
import java.util.Iterator;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import org.slf4j.Logger;
import java.io.Serializable;

public class FmrcInvLite implements Serializable
{
    private static Logger log;
    public String collectionName;
    public Date base;
    public int nruns;
    public double[] runOffset;
    public double[] forecastOffset;
    public double[] offsets;
    public List<String> locationList;
    public Map<String, Integer> locationMap;
    public List<Gridset> gridSets;
    public List<Gridset.GridInventory> invList;
    
    public FmrcInvLite(final FmrcInv fmrcInv) {
        this.locationList = new ArrayList<String>();
        this.locationMap = new HashMap<String, Integer>();
        this.gridSets = new ArrayList<Gridset>();
        this.invList = new ArrayList<Gridset.GridInventory>();
        this.collectionName = fmrcInv.getName();
        this.base = fmrcInv.getBaseDate();
        final List<Date> forecasts = fmrcInv.getForecastTimes();
        this.forecastOffset = new double[forecasts.size()];
        for (int i = 0; i < forecasts.size(); ++i) {
            final Date f = forecasts.get(i);
            this.forecastOffset[i] = FmrcInv.getOffsetInHours(this.base, f);
        }
        final List<FmrInv> fmrList = fmrcInv.getFmrInv();
        this.nruns = fmrList.size();
        this.runOffset = new double[this.nruns];
        int countIndex = 0;
        for (int run = 0; run < this.nruns; ++run) {
            final FmrInv fmr = fmrList.get(run);
            this.runOffset[run] = FmrcInv.getOffsetInHours(this.base, fmr.getRunDate());
            for (final GridDatasetInv inv : fmr.getInventoryList()) {
                this.locationList.add(inv.getLocation());
                this.locationMap.put(inv.getLocation(), countIndex);
                ++countIndex;
            }
        }
        for (final FmrcInv.RunSeq runseq : fmrcInv.getRunSeqs()) {
            this.gridSets.add(new Gridset(runseq));
        }
    }
    
    public List<Date> getRunDates() {
        final List<Date> result = new ArrayList<Date>(this.runOffset.length);
        for (final double off : this.runOffset) {
            result.add(FmrcInv.makeOffsetDate(this.base, off));
        }
        return result;
    }
    
    public List<Date> getForecastDates() {
        final List<Date> result = new ArrayList<Date>(this.forecastOffset.length);
        for (final double f : this.forecastOffset) {
            result.add(FmrcInv.makeOffsetDate(this.base, f));
        }
        return result;
    }
    
    public double[] getForecastOffsets() {
        if (this.offsets == null) {
            final TreeSet<Double> tree = new TreeSet<Double>();
            for (final Gridset gridset : this.gridSets) {
                for (int run = 0; run < this.nruns; ++run) {
                    final double baseOffset = gridset.timeOffset[run * gridset.noffsets];
                    for (int time = 0; time < gridset.noffsets; ++time) {
                        final double offset = gridset.timeOffset[run * gridset.noffsets + time];
                        if (!Double.isNaN(offset)) {
                            tree.add(offset - baseOffset);
                        }
                    }
                }
            }
            this.offsets = new double[tree.size()];
            final Iterator<Double> iter = tree.iterator();
            for (int i = 0; i < tree.size(); ++i) {
                this.offsets[i] = iter.next();
            }
        }
        return this.offsets;
    }
    
    public Gridset.Grid findGrid(final String gridName) {
        for (final Gridset gridset : this.gridSets) {
            for (final Gridset.Grid grid : gridset.grids) {
                if (gridName.equals(grid.name)) {
                    return grid;
                }
            }
        }
        return null;
    }
    
    public Gridset findGridset(final String gridName) {
        for (final Gridset gridset : this.gridSets) {
            for (final Gridset.Grid grid : gridset.grids) {
                if (gridName.equals(grid.name)) {
                    return gridset;
                }
            }
        }
        return null;
    }
    
    public void showGridInfo(final String gridName, final Formatter out) {
        final Gridset.Grid grid = this.findGrid(gridName);
        if (grid == null) {
            out.format("Cant find grid = %s%n", gridName);
            return;
        }
        final Gridset gridset = grid.getGridset();
        out.format("%n=======================================%nFmrcLite.Grid%n", new Object[0]);
        out.format("2D%n   run%n time  ", new Object[0]);
        for (int i = 0; i < gridset.noffsets; ++i) {
            out.format("%6d ", i);
        }
        out.format("%n", new Object[0]);
        for (int run = 0; run < this.nruns; ++run) {
            out.format("%6d", run);
            for (int time = 0; time < gridset.noffsets; ++time) {
                out.format(" %6.0f", gridset.getTimeCoord(run, time));
            }
            out.format("%n", new Object[0]);
        }
        out.format("%n", new Object[0]);
        out.format("Best%n", new Object[0]);
        final BestDatasetInventory best = new BestDatasetInventory(null);
        List<TimeInv> bestInv = gridset.timeCoordMap.get("Best");
        if (bestInv == null) {
            bestInv = gridset.makeBest(null);
        }
        final ValueB coords = best.getTimeCoords(gridset);
        out.format("        ", new Object[0]);
        for (int j = 0; j < bestInv.size(); ++j) {
            out.format(" %6d", j);
        }
        out.format("%n", new Object[0]);
        out.format(" coord =", new Object[0]);
        for (final TimeInv inv : bestInv) {
            out.format(" %6.0f", inv.offset);
        }
        out.format("%n", new Object[0]);
        out.format(" run   =", new Object[0]);
        for (final TimeInv inv : bestInv) {
            out.format(" %6d", inv.runIdx);
        }
        out.format("%n", new Object[0]);
        out.format(" idx   =", new Object[0]);
        for (final TimeInv inv : bestInv) {
            out.format(" %6d", inv.timeIdx);
        }
        out.format("%n", new Object[0]);
    }
    
    public TimeInventory makeBestDatasetInventory() {
        return new BestDatasetInventory(null);
    }
    
    TimeInventory makeBestDatasetInventory(final FeatureCollectionConfig.BestDataset bd) {
        return new BestDatasetInventory(bd);
    }
    
    public TimeInventory makeRunTimeDatasetInventory(final Date run) throws FileNotFoundException {
        return new RunTimeDatasetInventory(run);
    }
    
    public TimeInventory getConstantForecastDataset(final Date time) throws FileNotFoundException {
        return new ConstantForecastDataset(time);
    }
    
    public TimeInventory getConstantOffsetDataset(final double hour) throws FileNotFoundException {
        return new ConstantOffsetDataset(hour);
    }
    
    static {
        FmrcInvLite.log = LoggerFactory.getLogger(FmrcInvLite.class);
    }
    
    public class Gridset implements Serializable
    {
        String gridsetName;
        List<Grid> grids;
        int noffsets;
        double[] timeOffset;
        double[] timeBounds;
        Map<String, List<TimeInv>> timeCoordMap;
        final /* synthetic */ FmrcInvLite this$0;
        
        Gridset(final FmrcInv.RunSeq runseq) {
            this.grids = new ArrayList<Grid>();
            this.timeCoordMap = new HashMap<String, List<TimeInv>>();
            this.gridsetName = runseq.getName();
            final List<TimeCoord> timeList = runseq.getTimes();
            final boolean hasMissingTimes = FmrcInvLite.this.nruns != timeList.size();
            this.noffsets = 0;
            for (final TimeCoord tc : timeList) {
                this.noffsets = Math.max(this.noffsets, tc.getNCoords());
            }
            this.timeOffset = new double[FmrcInvLite.this.nruns * this.noffsets];
            for (int i = 0; i < this.timeOffset.length; ++i) {
                this.timeOffset[i] = Double.NaN;
            }
            if (runseq.isInterval()) {
                this.timeBounds = new double[FmrcInvLite.this.nruns * this.noffsets * 2];
                for (int i = 0; i < this.timeBounds.length; ++i) {
                    this.timeBounds[i] = Double.NaN;
                }
            }
            final DateFormatter df = new DateFormatter();
            int runIdx = 0;
            for (int seqIdx = 0; seqIdx < timeList.size(); ++seqIdx) {
                TimeCoord tc2 = null;
                if (hasMissingTimes) {
                    tc2 = timeList.get(seqIdx);
                    final double tc_offset = FmrcInv.getOffsetInHours(FmrcInvLite.this.base, tc2.getRunDate());
                    while (true) {
                        final double run_offset = FmrcInvLite.this.runOffset[runIdx];
                        if (Misc.closeEnough(run_offset, tc_offset)) {
                            break;
                        }
                        ++runIdx;
                        if (!FmrcInvLite.log.isDebugEnabled()) {
                            continue;
                        }
                        final String missingDate = df.toDateTimeStringISO(FmrcInv.makeOffsetDate(FmrcInvLite.this.base, run_offset));
                        final String wantDate = df.toDateTimeStringISO(tc2.getRunDate());
                        FmrcInvLite.log.debug(FmrcInvLite.this.collectionName + ": runseq missing time " + missingDate + " looking for " + wantDate + " for var = " + runseq.getUberGrids().get(0).getName());
                    }
                }
                else {
                    tc2 = timeList.get(runIdx);
                }
                final double run_offset2 = FmrcInv.getOffsetInHours(FmrcInvLite.this.base, tc2.getRunDate());
                final double[] offsets = tc2.getOffsetTimes();
                final int ntimes = offsets.length;
                for (int time = 0; time < ntimes; ++time) {
                    this.timeOffset[runIdx * this.noffsets + time] = run_offset2 + offsets[time];
                }
                if (runseq.isInterval()) {
                    final double[] bound1 = tc2.getBound1();
                    final double[] bound2 = tc2.getBound2();
                    for (int time2 = 0; time2 < ntimes; ++time2) {
                        this.timeBounds[2 * (runIdx * this.noffsets + time2)] = run_offset2 + bound1[time2];
                        this.timeBounds[2 * (runIdx * this.noffsets + time2) + 1] = run_offset2 + bound2[time2];
                    }
                }
                ++runIdx;
            }
            for (final FmrcInv.UberGrid ugrid : runseq.getUberGrids()) {
                this.grids.add(new Grid(ugrid.getName(), this.getInventory(ugrid)));
            }
        }
        
        private GridInventory getInventory(final FmrcInv.UberGrid ugrid) {
            GridInventory result = null;
            final GridInventory need = new GridInventory(ugrid);
            for (final GridInventory got : FmrcInvLite.this.invList) {
                if (got.equalData(need)) {
                    result = got;
                    break;
                }
            }
            if (result == null) {
                FmrcInvLite.this.invList.add(need);
                result = need;
            }
            return result;
        }
        
        double getTimeCoord(final int run, final int time) {
            return this.timeOffset[run * this.noffsets + time];
        }
        
        private List<TimeInv> makeBest(final FeatureCollectionConfig.BestDataset bd) {
            final Map<TimeCoord.Tinv, TimeInv> map = new HashMap<TimeCoord.Tinv, TimeInv>();
            for (int run = 0; run < FmrcInvLite.this.nruns; ++run) {
                for (int time = 0; time < this.noffsets; ++time) {
                    final double baseOffset = this.timeOffset[run * this.noffsets + time];
                    if (!Double.isNaN(baseOffset)) {
                        if (bd == null || baseOffset >= bd.greaterThan) {
                            if (this.timeBounds == null) {
                                map.put(new TimeCoord.Tinv(baseOffset), new TimeInv(run, time, baseOffset));
                            }
                            else {
                                final double b1 = this.timeBounds[2 * (run * this.noffsets + time)];
                                final double b2 = this.timeBounds[2 * (run * this.noffsets + time) + 1];
                                map.put(new TimeCoord.Tinv(b1, b2), new TimeInv(run, time, b1, b2));
                            }
                        }
                    }
                }
            }
            final Collection<TimeInv> values = map.values();
            final int n = values.size();
            final List<TimeInv> best = Arrays.asList((TimeInv[])values.toArray((T[])new TimeInv[n]));
            Collections.sort(best);
            this.timeCoordMap.put("best", best);
            return best;
        }
        
        private List<TimeInv> makeRun(final int runIdx) {
            final List<TimeInv> result = new ArrayList<TimeInv>(this.noffsets);
            for (int time = 0; time < this.noffsets; ++time) {
                final double offset = this.timeOffset[runIdx * this.noffsets + time];
                if (!Double.isNaN(offset)) {
                    if (this.timeBounds == null) {
                        result.add(new TimeInv(runIdx, time, offset));
                    }
                    else {
                        final double b1 = this.timeBounds[2 * (runIdx * this.noffsets + time)];
                        final double b2 = this.timeBounds[2 * (runIdx * this.noffsets + time) + 1];
                        result.add(new TimeInv(runIdx, time, b1, b2));
                    }
                }
            }
            this.timeCoordMap.put("run" + runIdx, result);
            return result;
        }
        
        private List<TimeInv> makeConstantForecast(final double offset) {
            final List<TimeInv> result = new ArrayList<TimeInv>(this.noffsets);
            for (int run = 0; run < FmrcInvLite.this.nruns; ++run) {
                for (int time = 0; time < this.noffsets; ++time) {
                    final double baseOffset = this.timeOffset[run * this.noffsets + time];
                    if (!Double.isNaN(baseOffset)) {
                        if (Misc.closeEnough(baseOffset, offset)) {
                            result.add(new TimeInv(run, time, offset - this.timeOffset[run * this.noffsets]));
                        }
                    }
                }
            }
            this.timeCoordMap.put("forecast" + offset, result);
            return result;
        }
        
        private List<TimeInv> makeConstantOffset(final double offset) {
            final List<TimeInv> result = new ArrayList<TimeInv>(FmrcInvLite.this.nruns);
            for (int run = 0; run < FmrcInvLite.this.nruns; ++run) {
                for (int time = 0; time < this.noffsets; ++time) {
                    final double baseOffset = this.getTimeCoord(run, time);
                    if (!Double.isNaN(baseOffset)) {
                        final double runOffset = baseOffset - this.getTimeCoord(run, 0);
                        if (!Double.isNaN(baseOffset) && Misc.closeEnough(runOffset, offset)) {
                            result.add(new TimeInv(run, time, baseOffset));
                        }
                    }
                }
            }
            this.timeCoordMap.put("offset" + offset, result);
            return result;
        }
        
        public class Grid implements Serializable
        {
            String name;
            GridInventory inv;
            
            Grid(final String name, final GridInventory inv) {
                this.name = name;
                this.inv = inv;
            }
            
            Gridset getGridset() {
                return Gridset.this;
            }
            
            TimeInventory.Instance getInstance(final int runIdx, final int timeIdx) {
                final int locIdx = this.inv.getLocation(runIdx, timeIdx);
                if (locIdx == 0) {
                    return null;
                }
                final int invIndex = this.inv.getInvIndex(runIdx, timeIdx);
                return new TimeInstance(FmrcInvLite.this.locationList.get(locIdx - 1), invIndex);
            }
        }
        
        public class GridInventory implements Serializable
        {
            int[] location;
            int[] invIndex;
            
            GridInventory(final FmrcInv.UberGrid ugrid) {
                this.location = new int[Gridset.this.this$0.nruns * Gridset.this.noffsets];
                this.invIndex = new int[Gridset.this.this$0.nruns * Gridset.this.noffsets];
                int gridIdx = 0;
                final List<FmrInv.GridVariable> grids = ugrid.getRuns();
                for (int runIdx = 0; runIdx < Gridset.this.this$0.nruns; ++runIdx) {
                    final Date runDate = FmrcInv.makeOffsetDate(Gridset.this.this$0.base, Gridset.this.this$0.runOffset[runIdx]);
                    if (gridIdx >= grids.size()) {
                        final DateFormatter df = new DateFormatter();
                        FmrcInvLite.log.debug(Gridset.this.this$0.collectionName + ": cant find " + ugrid.getName() + " for " + df.toDateTimeStringISO(runDate));
                        break;
                    }
                    final FmrInv.GridVariable grid = grids.get(gridIdx);
                    if (grid.getRunDate().equals(runDate)) {
                        ++gridIdx;
                        for (final GridDatasetInv.Grid inv : grid.getInventory()) {
                            final double invOffset = FmrcInv.getOffsetInHours(Gridset.this.this$0.base, inv.tc.getRunDate());
                            for (int i = 0; i < inv.tc.getNCoords(); ++i) {
                                int timeIdx = -1;
                                if (Gridset.this.timeBounds == null) {
                                    timeIdx = this.findIndex(runIdx, invOffset + inv.tc.getOffsetTimes()[i]);
                                }
                                else {
                                    timeIdx = this.findBounds(runIdx, invOffset + inv.tc.getBound1()[i], invOffset + inv.tc.getBound2()[i]);
                                }
                                if (timeIdx >= 0) {
                                    this.location[runIdx * Gridset.this.noffsets + timeIdx] = this.findLocation(inv.getLocation()) + 1;
                                    this.invIndex[runIdx * Gridset.this.noffsets + timeIdx] = i;
                                }
                            }
                        }
                    }
                }
            }
            
            private boolean equalData(final Object oo) {
                final GridInventory o = (GridInventory)oo;
                if (o.location.length != this.location.length) {
                    return false;
                }
                if (o.invIndex.length != this.invIndex.length) {
                    return false;
                }
                for (int i = 0; i < this.location.length; ++i) {
                    if (this.location[i] != o.location[i]) {
                        return false;
                    }
                }
                for (int i = 0; i < this.invIndex.length; ++i) {
                    if (this.invIndex[i] != o.invIndex[i]) {
                        return false;
                    }
                }
                return true;
            }
            
            private int findIndex(final int runIdx, final double want) {
                for (int j = 0; j < Gridset.this.noffsets; ++j) {
                    if (Misc.closeEnough(Gridset.this.timeOffset[runIdx * Gridset.this.noffsets + j], want)) {
                        return j;
                    }
                }
                return -1;
            }
            
            private int findBounds(final int runIdx, final double b1, final double b2) {
                for (int j = 0; j < Gridset.this.noffsets; ++j) {
                    if (Misc.closeEnough(Gridset.this.timeBounds[2 * (runIdx * Gridset.this.noffsets + j)], b1) && Misc.closeEnough(Gridset.this.timeBounds[2 * (runIdx * Gridset.this.noffsets + j) + 1], b2)) {
                        return j;
                    }
                }
                return -1;
            }
            
            private int findLocation(final String location) {
                return FmrcInvLite.this.locationMap.get(location);
            }
            
            int getLocation(final int run, final int time) {
                return this.location[run * Gridset.this.noffsets + time];
            }
            
            int getInvIndex(final int run, final int time) {
                return this.invIndex[run * Gridset.this.noffsets + time];
            }
        }
    }
    
    static class TimeInstance implements TimeInventory.Instance
    {
        String location;
        int index;
        
        TimeInstance(final String location, final int index) {
            this.location = location;
            this.index = index;
        }
        
        public String getDatasetLocation() {
            return this.location;
        }
        
        public int getDatasetIndex() {
            return this.index;
        }
    }
    
    private class TimeInv implements Comparable<TimeInv>
    {
        int runIdx;
        int timeIdx;
        double offset;
        double startIntv;
        boolean isInterval;
        
        TimeInv(final int runIdx, final int timeIdx, final double b1, final double b2) {
            this.startIntv = Double.NaN;
            this.isInterval = false;
            this.runIdx = runIdx;
            this.timeIdx = timeIdx;
            this.startIntv = b1;
            this.offset = b2;
            this.isInterval = true;
        }
        
        TimeInv(final int runIdx, final int timeIdx, final double offset) {
            this.startIntv = Double.NaN;
            this.isInterval = false;
            this.runIdx = runIdx;
            this.timeIdx = timeIdx;
            this.offset = offset;
        }
        
        public int compareTo(final TimeInv o) {
            final int c1 = Double.compare(this.offset, o.offset);
            if (c1 == 0 && this.isInterval) {
                return Double.compare(this.startIntv, o.startIntv);
            }
            return c1;
        }
    }
    
    public static class ValueB
    {
        public double[] offset;
        public double[] bounds;
        
        public ValueB(final List<TimeInv> invs) {
            final boolean isInterval = invs.size() > 0 && invs.get(0).isInterval;
            this.offset = new double[invs.size()];
            if (isInterval) {
                this.bounds = new double[2 * invs.size()];
                for (int i = 0; i < invs.size(); ++i) {
                    final TimeInv b = invs.get(i);
                    this.offset[i] = b.offset;
                    this.bounds[2 * i] = b.startIntv;
                    this.bounds[2 * i + 1] = b.offset;
                }
            }
            else {
                for (int i = 0; i < invs.size(); ++i) {
                    final TimeInv b = invs.get(i);
                    this.offset[i] = b.offset;
                }
            }
        }
    }
    
    class BestDatasetInventory implements TimeInventory
    {
        FeatureCollectionConfig.BestDataset bd;
        
        BestDatasetInventory(final FeatureCollectionConfig.BestDataset bd) {
            this.bd = bd;
        }
        
        public String getName() {
            return (this.bd == null) ? "Best" : this.bd.name;
        }
        
        public int getTimeLength(final Gridset gridset) {
            List<TimeInv> best = gridset.timeCoordMap.get(this.getName());
            if (best == null) {
                best = gridset.makeBest(this.bd);
            }
            return best.size();
        }
        
        public ValueB getTimeCoords(final Gridset gridset) {
            List<TimeInv> best = gridset.timeCoordMap.get(this.getName());
            if (best == null) {
                best = gridset.makeBest(this.bd);
            }
            return new ValueB(best);
        }
        
        public double[] getRunTimeCoords(final Gridset gridset) {
            List<TimeInv> best = gridset.timeCoordMap.get(this.getName());
            if (best == null) {
                best = gridset.makeBest(this.bd);
            }
            final double[] result = new double[best.size()];
            for (int i = 0; i < best.size(); ++i) {
                final TimeInv b = best.get(i);
                result[i] = gridset.getTimeCoord(b.runIdx, 0);
            }
            return result;
        }
        
        public double[] getOffsetCoords(final Gridset gridset) {
            List<TimeInv> best = gridset.timeCoordMap.get(this.getName());
            if (best == null) {
                best = gridset.makeBest(this.bd);
            }
            final double[] result = new double[best.size()];
            for (int i = 0; i < best.size(); ++i) {
                final TimeInv b = best.get(i);
                result[i] = b.offset - gridset.getTimeCoord(b.runIdx, 0);
            }
            return result;
        }
        
        public Instance getInstance(final Gridset.Grid grid, final int timeIdx) {
            final Gridset gridset = grid.getGridset();
            List<TimeInv> best = gridset.timeCoordMap.get(this.getName());
            if (best == null) {
                best = gridset.makeBest(this.bd);
            }
            final TimeInv b = best.get(timeIdx);
            final int locIdx = grid.inv.getLocation(b.runIdx, b.timeIdx);
            if (locIdx == 0) {
                return null;
            }
            final int invIndex = grid.inv.getInvIndex(b.runIdx, b.timeIdx);
            return new TimeInstance(FmrcInvLite.this.locationList.get(locIdx - 1), invIndex);
        }
    }
    
    class RunTimeDatasetInventory implements TimeInventory
    {
        int runIdx;
        
        RunTimeDatasetInventory(final Date run) throws FileNotFoundException {
            this.runIdx = -1;
            final double offset = FmrcInv.getOffsetInHours(FmrcInvLite.this.base, run);
            for (int i = 0; i < FmrcInvLite.this.runOffset.length; ++i) {
                if (Misc.closeEnough(FmrcInvLite.this.runOffset[i], offset)) {
                    this.runIdx = i;
                    break;
                }
            }
            if (this.runIdx < 0) {
                throw new FileNotFoundException("No run date of " + run);
            }
        }
        
        public String getName() {
            final DateFormatter df = new DateFormatter();
            return "Run " + df.toDateTimeStringISO(FmrcInv.makeOffsetDate(FmrcInvLite.this.base, FmrcInvLite.this.runOffset[this.runIdx]));
        }
        
        public int getTimeLength(final Gridset gridset) {
            List<TimeInv> coords = gridset.timeCoordMap.get("run" + this.runIdx);
            if (coords == null) {
                coords = gridset.makeRun(this.runIdx);
            }
            return coords.size();
        }
        
        public ValueB getTimeCoords(final Gridset gridset) {
            List<TimeInv> coords = gridset.timeCoordMap.get("run" + this.runIdx);
            if (coords == null) {
                coords = gridset.makeRun(this.runIdx);
            }
            return new ValueB(coords);
        }
        
        public double[] getRunTimeCoords(final Gridset gridset) {
            return null;
        }
        
        public double[] getOffsetCoords(final Gridset gridset) {
            List<TimeInv> coords = gridset.timeCoordMap.get("run" + this.runIdx);
            if (coords == null) {
                coords = gridset.makeRun(this.runIdx);
            }
            final double startRun = gridset.getTimeCoord(this.runIdx, 0);
            final double[] result = new double[coords.size()];
            for (int i = 0; i < coords.size(); ++i) {
                final TimeInv b = coords.get(i);
                result[i] = b.offset - startRun;
            }
            return result;
        }
        
        public Instance getInstance(final Gridset.Grid grid, final int timeIdx) {
            final Gridset gridset = grid.getGridset();
            List<TimeInv> coords = gridset.timeCoordMap.get("run" + this.runIdx);
            if (coords == null) {
                coords = gridset.makeRun(this.runIdx);
            }
            final TimeInv b = coords.get(timeIdx);
            return grid.getInstance(b.runIdx, b.timeIdx);
        }
    }
    
    class ConstantForecastDataset implements TimeInventory
    {
        double offset;
        
        ConstantForecastDataset(final Date time) throws FileNotFoundException {
            this.offset = FmrcInv.getOffsetInHours(FmrcInvLite.this.base, time);
            for (final Date d : FmrcInvLite.this.getForecastDates()) {
                if (d.equals(time)) {
                    return;
                }
            }
            throw new FileNotFoundException("No forecast date of " + time);
        }
        
        public String getName() {
            final DateFormatter df = new DateFormatter();
            return "Constant Forecast " + df.toDateTimeStringISO(FmrcInv.makeOffsetDate(FmrcInvLite.this.base, this.offset));
        }
        
        public int getTimeLength(final Gridset gridset) {
            List<TimeInv> coords = gridset.timeCoordMap.get("forecast" + this.offset);
            if (coords == null) {
                coords = gridset.makeConstantForecast(this.offset);
            }
            return coords.size();
        }
        
        public ValueB getTimeCoords(final Gridset gridset) {
            return null;
        }
        
        public double[] getRunTimeCoords(final Gridset gridset) {
            List<TimeInv> coords = gridset.timeCoordMap.get("forecast" + this.offset);
            if (coords == null) {
                coords = gridset.makeConstantForecast(this.offset);
            }
            final double[] result = new double[coords.size()];
            for (int i = 0; i < coords.size(); ++i) {
                final TimeInv b = coords.get(i);
                result[i] = gridset.getTimeCoord(b.runIdx, 0);
            }
            return result;
        }
        
        public double[] getOffsetCoords(final Gridset gridset) {
            List<TimeInv> coords = gridset.timeCoordMap.get("forecast" + this.offset);
            if (coords == null) {
                coords = gridset.makeConstantForecast(this.offset);
            }
            final double[] result = new double[coords.size()];
            for (int i = 0; i < coords.size(); ++i) {
                final TimeInv b = coords.get(i);
                result[i] = b.offset;
            }
            return result;
        }
        
        public Instance getInstance(final Gridset.Grid grid, final int timeIdx) {
            final Gridset gridset = grid.getGridset();
            List<TimeInv> coords = gridset.timeCoordMap.get("forecast" + this.offset);
            if (coords == null) {
                coords = gridset.makeConstantForecast(this.offset);
            }
            final TimeInv b = coords.get(timeIdx);
            return grid.getInstance(b.runIdx, b.timeIdx);
        }
    }
    
    class ConstantOffsetDataset implements TimeInventory
    {
        double offset;
        
        ConstantOffsetDataset(final double offset) throws FileNotFoundException {
            this.offset = offset;
            boolean ok = false;
            final double[] offsets = FmrcInvLite.this.getForecastOffsets();
            for (int i = 0; i < offsets.length; ++i) {
                if (Misc.closeEnough(offsets[i], offset)) {
                    ok = true;
                }
            }
            if (!ok) {
                throw new FileNotFoundException("No constant offset dataset for = " + offset);
            }
        }
        
        public String getName() {
            return "Constant Offset " + this.offset + " hours";
        }
        
        public int getTimeLength(final Gridset gridset) {
            List<TimeInv> coords = gridset.timeCoordMap.get("offset" + this.offset);
            if (coords == null) {
                coords = gridset.makeConstantOffset(this.offset);
            }
            return coords.size();
        }
        
        public ValueB getTimeCoords(final Gridset gridset) {
            List<TimeInv> coords = gridset.timeCoordMap.get("offset" + this.offset);
            if (coords == null) {
                coords = gridset.makeConstantOffset(this.offset);
            }
            return new ValueB(coords);
        }
        
        public double[] getRunTimeCoords(final Gridset gridset) {
            List<TimeInv> coords = gridset.timeCoordMap.get("offset" + this.offset);
            if (coords == null) {
                coords = gridset.makeConstantOffset(this.offset);
            }
            final double[] result = new double[coords.size()];
            for (int i = 0; i < coords.size(); ++i) {
                final TimeInv b = coords.get(i);
                result[i] = gridset.getTimeCoord(b.runIdx, 0);
            }
            return result;
        }
        
        public double[] getOffsetCoords(final Gridset gridset) {
            return null;
        }
        
        public Instance getInstance(final Gridset.Grid grid, final int timeIdx) {
            final Gridset gridset = grid.getGridset();
            List<TimeInv> coords = gridset.timeCoordMap.get("offset" + this.offset);
            if (coords == null) {
                coords = gridset.makeConstantOffset(this.offset);
            }
            final TimeInv b = coords.get(timeIdx);
            return grid.getInstance(b.runIdx, b.timeIdx);
        }
    }
}
