// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.fmrc;

import net.jcip.annotations.Immutable;
import java.util.Set;
import java.util.Collections;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Formatter;
import ucar.nc2.util.Misc;
import ucar.nc2.dataset.CoordinateAxis1D;

public class VertCoord implements Comparable
{
    private String name;
    private String units;
    private int id;
    private double[] values1;
    private double[] values2;
    
    VertCoord() {
    }
    
    VertCoord(final CoordinateAxis1D axis) {
        this.name = axis.getName();
        this.units = axis.getUnitsString();
        final int n = (int)axis.getSize();
        if (axis.isInterval()) {
            this.values1 = axis.getBound1();
            this.values2 = axis.getBound2();
        }
        else {
            this.values1 = new double[n];
            for (int i = 0; i < axis.getSize(); ++i) {
                this.values1[i] = axis.getCoordValue(i);
            }
        }
    }
    
    VertCoord(final VertCoord vc) {
        this.name = vc.getName();
        this.units = vc.getUnits();
        this.id = vc.id;
        this.values1 = vc.getValues1().clone();
        this.values2 = (double[])((vc.getValues2() == null) ? null : ((double[])vc.getValues2().clone()));
    }
    
    public int getId() {
        return this.id;
    }
    
    public void setId(final int id) {
        this.id = id;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public String getUnits() {
        return this.units;
    }
    
    public void setUnits(final String units) {
        this.units = units;
    }
    
    public double[] getValues1() {
        return this.values1;
    }
    
    public void setValues1(final double[] values) {
        this.values1 = values;
    }
    
    public double[] getValues2() {
        return this.values2;
    }
    
    public void setValues2(final double[] values) {
        this.values2 = values;
    }
    
    public int getSize() {
        return this.values1.length;
    }
    
    public boolean equalsData(final VertCoord other) {
        if (this.values1.length != other.values1.length) {
            return false;
        }
        for (int i = 0; i < this.values1.length; ++i) {
            if (!Misc.closeEnough(this.values1[i], other.values1[i])) {
                return false;
            }
        }
        if (this.values2 == null && other.values2 == null) {
            return true;
        }
        if (this.values2 == null || other.values2 == null) {
            return false;
        }
        if (this.values2.length != other.values2.length) {
            return false;
        }
        for (int i = 0; i < this.values2.length; ++i) {
            if (!Misc.closeEnough(this.values2[i], other.values2[i])) {
                return false;
            }
        }
        return true;
    }
    
    public int compareTo(final Object o) {
        final VertCoord other = (VertCoord)o;
        return this.name.compareTo(other.name);
    }
    
    @Override
    public String toString() {
        final Formatter out = new Formatter();
        out.format("values=", new Object[0]);
        if (this.values2 == null) {
            for (final double val : this.values1) {
                out.format("%5f, ", val);
            }
        }
        else {
            for (int i = 0; i < this.values1.length; ++i) {
                out.format("(%6.3f,%6.3f) ", this.values1[i], this.values2[i]);
            }
        }
        out.format("; name=%s", this.name);
        return out.toString();
    }
    
    public static VertCoord findVertCoord(final List<VertCoord> vertCoords, final VertCoord want) {
        if (want == null) {
            return null;
        }
        for (final VertCoord vc : vertCoords) {
            if (want.equalsData(vc)) {
                return vc;
            }
        }
        final VertCoord result = new VertCoord(want);
        vertCoords.add(result);
        return result;
    }
    
    public static void normalize(final VertCoord result, final List<VertCoord> vcList) {
        final Set<LevelCoord> valueSet = new HashSet<LevelCoord>();
        addValues(valueSet, result.getValues1(), result.getValues2());
        for (final VertCoord vc : vcList) {
            addValues(valueSet, vc.getValues1(), vc.getValues2());
        }
        final List<LevelCoord> valueList = Arrays.asList((LevelCoord[])valueSet.toArray((T[])new LevelCoord[valueSet.size()]));
        Collections.sort(valueList);
        final double[] values1 = new double[valueList.size()];
        final double[] values2 = new double[valueList.size()];
        boolean has_values2 = false;
        for (int i = 0; i < valueList.size(); ++i) {
            final LevelCoord lc = valueList.get(i);
            values1[i] = lc.value1;
            values2[i] = lc.value2;
            if (lc.value2 != 0.0) {
                has_values2 = true;
            }
        }
        result.setValues1(values1);
        if (has_values2) {
            result.setValues2(values2);
        }
    }
    
    private static void addValues(final Set<LevelCoord> valueSet, final double[] values1, final double[] values2) {
        for (int i = 0; i < values1.length; ++i) {
            final double val2 = (values2 == null) ? 0.0 : values2[i];
            valueSet.add(new LevelCoord(values1[i], val2));
        }
    }
    
    @Immutable
    private static class LevelCoord implements Comparable
    {
        final double mid;
        final double value1;
        final double value2;
        
        LevelCoord(final double value1, final double value2) {
            this.value1 = value1;
            this.value2 = value2;
            this.mid = ((value2 == 0.0) ? value1 : ((value1 + value2) / 2.0));
        }
        
        public int compareTo(final Object o) {
            final LevelCoord other = (LevelCoord)o;
            if (this.mid < other.mid) {
                return -1;
            }
            if (this.mid > other.mid) {
                return 1;
            }
            return 0;
        }
        
        @Override
        public boolean equals(final Object oo) {
            if (this == oo) {
                return true;
            }
            if (!(oo instanceof LevelCoord)) {
                return false;
            }
            final LevelCoord other = (LevelCoord)oo;
            return Misc.closeEnough(this.value1, other.value1) && Misc.closeEnough(this.value2, other.value2);
        }
        
        @Override
        public int hashCode() {
            return (int)(this.value1 * 100000.0 + this.value2 * 100.0);
        }
    }
}
