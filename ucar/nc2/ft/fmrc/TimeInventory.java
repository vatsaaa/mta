// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.fmrc;

public interface TimeInventory
{
    String getName();
    
    int getTimeLength(final FmrcInvLite.Gridset p0);
    
    FmrcInvLite.ValueB getTimeCoords(final FmrcInvLite.Gridset p0);
    
    double[] getRunTimeCoords(final FmrcInvLite.Gridset p0);
    
    double[] getOffsetCoords(final FmrcInvLite.Gridset p0);
    
    Instance getInstance(final FmrcInvLite.Gridset.Grid p0, final int p1);
    
    public interface Instance
    {
        String getDatasetLocation();
        
        int getDatasetIndex();
    }
}
