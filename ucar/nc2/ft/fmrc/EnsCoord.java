// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.fmrc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import ucar.nc2.dataset.CoordinateAxis1D;

public class EnsCoord implements Comparable
{
    private String name;
    private int id;
    public int ensembles;
    public int pdn;
    public int[] ensTypes;
    
    EnsCoord() {
    }
    
    EnsCoord(final CoordinateAxis1D axis, final int[] einfo) {
        this.name = axis.getName();
        this.ensembles = einfo[0];
        this.pdn = einfo[1];
        System.arraycopy(einfo, 2, this.ensTypes = new int[this.ensembles], 0, this.ensembles);
    }
    
    EnsCoord(final EnsCoord ec) {
        this.name = ec.getName();
        this.id = ec.getId();
        this.ensembles = ec.getNEnsembles();
        this.pdn = ec.getPDN();
        this.ensTypes = ec.getEnsTypes().clone();
    }
    
    public int getId() {
        return this.id;
    }
    
    public void setId(final int id) {
        this.id = id;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public int getNEnsembles() {
        return this.ensembles;
    }
    
    public void setNEnsembles(final int ensembles) {
        this.ensembles = ensembles;
    }
    
    public int getPDN() {
        return this.pdn;
    }
    
    public void setPDN(final int pdn) {
        this.pdn = pdn;
    }
    
    public int[] getEnsTypes() {
        return this.ensTypes;
    }
    
    public void setEnsTypes(final int[] ensTypes) {
        this.ensTypes = ensTypes;
    }
    
    public int getSize() {
        return this.ensembles;
    }
    
    public boolean equalsData(final EnsCoord other) {
        if (this.ensembles != other.ensembles) {
            return false;
        }
        if (this.pdn != other.pdn) {
            return false;
        }
        for (int i = 0; i < this.ensTypes.length; ++i) {
            if (this.ensTypes[i] != other.ensTypes[i]) {
                return false;
            }
        }
        return true;
    }
    
    public int compareTo(final Object o) {
        final EnsCoord other = (EnsCoord)o;
        return this.name.compareTo(other.name);
    }
    
    public static EnsCoord findEnsCoord(final List<EnsCoord> ensCoords, final EnsCoord want) {
        if (want == null) {
            return null;
        }
        for (final EnsCoord ec : ensCoords) {
            if (want.equalsData(ec)) {
                return ec;
            }
        }
        final EnsCoord result = new EnsCoord(want);
        ensCoords.add(result);
        return result;
    }
    
    public static void normalize(EnsCoord result, final List<EnsCoord> ecList) {
        final List<EnsCoord> extra = new ArrayList<EnsCoord>();
        for (final EnsCoord ec : ecList) {
            if (!result.equalsData(ec)) {
                extra.add(ec);
            }
        }
        if (extra.size() == 0) {
            return;
        }
        for (final EnsCoord ec : extra) {
            if (ec.getNEnsembles() < result.getNEnsembles()) {
                continue;
            }
            result = ec;
        }
    }
}
