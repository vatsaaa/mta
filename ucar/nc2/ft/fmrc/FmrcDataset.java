// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.fmrc;

import ucar.ma2.MAMath;
import org.slf4j.LoggerFactory;
import ucar.nc2.dataset.VariableEnhanced;
import ucar.nc2.util.CancelTask;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Section;
import ucar.ma2.Range;
import ucar.ma2.ArrayDouble;
import ucar.nc2.dataset.CoordinateSystem;
import ucar.nc2.ProxyReader;
import ucar.ma2.Array;
import ucar.ma2.DataType;
import ucar.nc2.units.DateFormatter;
import ucar.nc2.EnumTypedef;
import ucar.nc2.NetcdfFile;
import ucar.nc2.dataset.VariableDS;
import ucar.nc2.dataset.StructureDS;
import ucar.nc2.Structure;
import ucar.nc2.Dimension;
import ucar.nc2.dataset.DatasetConstructor;
import ucar.nc2.dt.GridCoordSystem;
import ucar.nc2.dataset.CoordSysBuilderIF;
import java.util.Iterator;
import java.util.Set;
import ucar.nc2.ncml.NcMLReader;
import ucar.nc2.Group;
import ucar.nc2.dataset.TransformType;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.nc2.constants.AxisType;
import ucar.nc2.dataset.CoordinateAxis;
import ucar.nc2.dt.GridDatatype;
import java.util.Formatter;
import java.util.Collection;
import ucar.nc2.Variable;
import java.util.ArrayList;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.Attribute;
import java.util.HashMap;
import java.util.TimeZone;
import java.util.Calendar;
import java.util.Random;
import java.io.IOException;
import ucar.nc2.dt.GridDataset;
import ucar.nc2.dataset.NetcdfDataset;
import java.util.Date;
import java.util.List;
import thredds.inventory.FeatureCollectionConfig;
import org.slf4j.Logger;
import net.jcip.annotations.ThreadSafe;

@ThreadSafe
class FmrcDataset
{
    private static final Logger logger;
    private static final boolean debugEnhance = false;
    private static final boolean debugRead = false;
    private final FeatureCollectionConfig.Config config;
    private State state;
    private Object lock;
    
    FmrcDataset(final FeatureCollectionConfig.Config config) {
        this.lock = new Object();
        this.config = config;
    }
    
    List<Date> getRunDates() {
        final State localState;
        synchronized (this.lock) {
            localState = this.state;
        }
        return localState.lite.getRunDates();
    }
    
    List<Date> getForecastDates() {
        final State localState;
        synchronized (this.lock) {
            localState = this.state;
        }
        return localState.lite.getForecastDates();
    }
    
    double[] getForecastOffsets() {
        final State localState;
        synchronized (this.lock) {
            localState = this.state;
        }
        return localState.lite.getForecastOffsets();
    }
    
    GridDataset getNetcdfDataset2D(final NetcdfDataset result) throws IOException {
        final State localState;
        synchronized (this.lock) {
            localState = this.state;
        }
        return this.buildDataset2D(result, localState.proto, localState.lite);
    }
    
    GridDataset getBest() throws IOException {
        final State localState;
        synchronized (this.lock) {
            localState = this.state;
        }
        return this.buildDataset1D(localState.proto, localState.lite, localState.lite.makeBestDatasetInventory());
    }
    
    GridDataset getBest(final FeatureCollectionConfig.BestDataset bd) throws IOException {
        final State localState;
        synchronized (this.lock) {
            localState = this.state;
        }
        return this.buildDataset1D(localState.proto, localState.lite, localState.lite.makeBestDatasetInventory(bd));
    }
    
    GridDataset getRunTimeDataset(final Date run) throws IOException {
        final State localState;
        synchronized (this.lock) {
            localState = this.state;
        }
        return this.buildDataset1D(localState.proto, localState.lite, localState.lite.makeRunTimeDatasetInventory(run));
    }
    
    GridDataset getConstantForecastDataset(final Date run) throws IOException {
        final State localState;
        synchronized (this.lock) {
            localState = this.state;
        }
        return this.buildDataset1D(localState.proto, localState.lite, localState.lite.getConstantForecastDataset(run));
    }
    
    GridDataset getConstantOffsetDataset(final double offset) throws IOException {
        final State localState;
        synchronized (this.lock) {
            localState = this.state;
        }
        return this.buildDataset1D(localState.proto, localState.lite, localState.lite.getConstantOffsetDataset(offset));
    }
    
    void setInventory(final FmrcInv fmrcInv, final boolean forceProto) throws IOException {
        NetcdfDataset protoLocal = null;
        if (this.state == null || forceProto) {
            protoLocal = this.buildProto(fmrcInv, this.config.protoConfig);
        }
        final FmrcInvLite liteLocal = new FmrcInvLite(fmrcInv);
        synchronized (this.lock) {
            if (protoLocal == null) {
                protoLocal = this.state.proto;
            }
            this.state = new State(protoLocal, liteLocal);
        }
    }
    
    private NetcdfDataset buildProto(final FmrcInv fmrcInv, final FeatureCollectionConfig.ProtoConfig protoConfig) throws IOException {
        final NetcdfDataset result = new NetcdfDataset();
        final List<FmrInv> list = fmrcInv.getFmrInv();
        if (list.size() == 0) {
            FmrcDataset.logger.error("Fmrc collection is empty =" + fmrcInv.getName());
            throw new IllegalStateException("Fmrc collection is empty =" + fmrcInv.getName());
        }
        int protoIdx = 0;
        switch (protoConfig.choice) {
            case First: {
                protoIdx = 0;
                break;
            }
            case Random: {
                final Random r = new Random(System.currentTimeMillis());
                protoIdx = r.nextInt(list.size() - 1);
                break;
            }
            case Penultimate: {
                protoIdx = Math.max(list.size() - 2, 0);
                break;
            }
            case Latest: {
                protoIdx = Math.max(list.size() - 1, 0);
                break;
            }
            case Run: {
                int runOffset = 0;
                if (protoConfig.param != null) {
                    runOffset = Integer.parseInt(protoConfig.param);
                }
                final Calendar cal = Calendar.getInstance();
                cal.setTimeZone(TimeZone.getTimeZone("GMT"));
                for (int i = 0; i < list.size(); ++i) {
                    final FmrInv fmr = list.get(i);
                    cal.setTime(fmr.getRunDate());
                    final int hour = cal.get(11);
                    if (hour == runOffset) {
                        protoIdx = i;
                    }
                }
                break;
            }
        }
        final FmrInv proto = list.get(protoIdx);
        final HashMap<String, NetcdfDataset> openFilesProto = new HashMap<String, NetcdfDataset>();
        try {
            final Set<GridDatasetInv> files = proto.getFiles();
            if (FmrcDataset.logger.isDebugEnabled()) {
                FmrcDataset.logger.debug("FmrcDataset: proto= " + proto.getName() + " " + proto.getRunDate() + " collection= " + fmrcInv.getName());
            }
            for (final GridDatasetInv file : files) {
                final NetcdfDataset ncfile = this.open(file.getLocation(), openFilesProto);
                this.transferGroup(ncfile.getRootGroup(), result.getRootGroup(), result);
                if (FmrcDataset.logger.isDebugEnabled()) {
                    FmrcDataset.logger.debug("FmrcDataset: proto dataset= " + file.getLocation());
                }
            }
            final Group root = result.getRootGroup();
            root.addAttribute(new Attribute("Conventions", "CF-1.4, _Coordinates"));
            root.addAttribute(new Attribute("cdm_data_type", FeatureType.GRID.toString()));
            root.addAttribute(new Attribute("CF:feature_type", FeatureType.GRID.toString()));
            root.addAttribute(new Attribute("location", "Proto " + fmrcInv.getName()));
            root.remove(root.findAttribute("_CoordinateModelRunDate"));
            final List<Variable> copyList = new ArrayList<Variable>(root.getVariables());
            for (final Variable v : copyList) {
                final FmrcInv.UberGrid grid = fmrcInv.findUberGrid(v.getName());
                if (grid == null) {
                    final Variable orgV = (Variable)v.getSPobject();
                    if (orgV.getSize() > 10000000L) {
                        FmrcDataset.logger.info("FMRCDataset build Proto cache >10M var= " + orgV.getName());
                    }
                    v.setCachedData(orgV.read());
                }
                v.setSPobject(null);
            }
            result.finish();
            final CoordSysBuilderIF builder = result.enhance();
            final Formatter parseInfo = new Formatter();
            final GridDataset gds = new ucar.nc2.dt.grid.GridDataset(result, parseInfo);
            for (final GridDatatype grid2 : gds.getGrids()) {
                final Variable newV = result.findVariable(grid2.getName());
                if (newV == null) {
                    FmrcDataset.logger.warn("FmrcDataset cant find " + grid2.getName() + " in proto gds ");
                }
                else {
                    final StringBuilder sbuff = new StringBuilder();
                    final GridCoordSystem gcs = grid2.getCoordinateSystem();
                    for (final CoordinateAxis axis : gcs.getCoordinateAxes()) {
                        if (axis.getAxisType() != AxisType.Time && axis.getAxisType() != AxisType.RunTime) {
                            sbuff.append(axis.getName()).append(" ");
                        }
                    }
                    newV.addAttribute(new Attribute("coordinates", sbuff.toString()));
                    for (final CoordinateTransform ct : gcs.getCoordinateTransforms()) {
                        final Variable ctv = result.findVariable(ct.getName());
                        if (ctv != null && ct.getTransformType() == TransformType.Projection) {
                            newV.addAttribute(new Attribute("grid_mapping", ctv.getName()));
                        }
                    }
                    for (final CoordinateAxis axis : gcs.getCoordinateAxes()) {
                        final Variable coordV = result.findVariable(axis.getName());
                        if ((axis.getAxisType() == AxisType.Height || axis.getAxisType() == AxisType.Pressure || axis.getAxisType() == AxisType.GeoZ) && null != axis.getPositive()) {
                            coordV.addAttribute(new Attribute("positive", axis.getPositive()));
                        }
                        if (axis.getAxisType() == AxisType.Lat) {
                            coordV.addAttribute(new Attribute("units", "degrees_north"));
                            coordV.addAttribute(new Attribute("standard_name", "latitude"));
                        }
                        if (axis.getAxisType() == AxisType.Lon) {
                            coordV.addAttribute(new Attribute("units", "degrees_east"));
                            coordV.addAttribute(new Attribute("standard_name", "longitude"));
                        }
                        if (axis.getAxisType() == AxisType.GeoX) {
                            coordV.addAttribute(new Attribute("standard_name", "projection_x_coordinate"));
                        }
                        if (axis.getAxisType() == AxisType.GeoY) {
                            coordV.addAttribute(new Attribute("standard_name", "projection_y_coordinate"));
                        }
                        if (axis.getAxisType() == AxisType.Time) {
                            final Attribute att = axis.findAttribute("bounds");
                            if (att == null || !att.isString()) {
                                continue;
                            }
                            result.removeVariable(null, att.getStringValue());
                        }
                    }
                }
            }
            for (final Variable v2 : result.getVariables()) {
                Attribute att2 = null;
                if (null != (att2 = v2.findAttribute("_CoordinateAxes".toString()))) {
                    v2.remove(att2);
                }
                if (null != (att2 = v2.findAttribute("_CoordinateSystems".toString()))) {
                    v2.remove(att2);
                }
                if (null != (att2 = v2.findAttribute("_CoordinateSystemFor".toString()))) {
                    v2.remove(att2);
                }
                if (null != (att2 = v2.findAttribute("_CoordinateTransforms".toString()))) {
                    v2.remove(att2);
                }
            }
            if (protoConfig.outerNcml != null) {
                NcMLReader.mergeNcMLdirect(result, protoConfig.outerNcml);
            }
            return result;
        }
        finally {
            this.closeAll(openFilesProto);
        }
    }
    
    private void transferGroup(final Group srcGroup, final Group targetGroup, final NetcdfDataset target) throws IOException {
        DatasetConstructor.transferGroupAttributes(srcGroup, targetGroup);
        for (final Dimension d : srcGroup.getDimensions()) {
            if (null == targetGroup.findDimensionLocal(d.getName())) {
                final Dimension newd = new Dimension(d.getName(), d.getLength(), d.isShared(), d.isUnlimited(), d.isVariableLength());
                targetGroup.addDimension(newd);
            }
        }
        for (final Variable v : srcGroup.getVariables()) {
            Variable targetV = targetGroup.findVariable(v.getShortName());
            if (null == targetV) {
                if (v instanceof Structure) {
                    targetV = new StructureDS(target, targetGroup, null, v.getShortName(), v.getDimensionsString(), v.getUnitsString(), v.getDescription());
                }
                else {
                    targetV = new VariableDS(target, targetGroup, null, v.getShortName(), v.getDataType(), v.getDimensionsString(), v.getUnitsString(), v.getDescription());
                }
                DatasetConstructor.transferVariableAttributes(v, targetV);
                final VariableDS vds = (VariableDS)v;
                targetV.setSPobject(vds);
                if (vds.hasCachedDataRecurse()) {
                    targetV.setCachedData(vds.read());
                }
                targetGroup.addVariable(targetV);
            }
        }
        for (final Group srcNested : srcGroup.getGroups()) {
            Group nested = targetGroup.findGroup(srcNested.getShortName());
            if (null == nested) {
                nested = new Group(target, targetGroup, srcNested.getShortName());
                targetGroup.addGroup(nested);
                for (final EnumTypedef et : srcNested.getEnumTypedefs()) {
                    targetGroup.addEnumeration(et);
                }
            }
            this.transferGroup(srcNested, nested, target);
        }
    }
    
    private String getRunDimensionName() {
        return "run";
    }
    
    private String makeCoordinateList(final VariableDS aggVar, final String timeCoordName, final boolean is2D) {
        String coords = "";
        Attribute att = aggVar.findAttribute("coordinates");
        if (att == null) {
            att = aggVar.findAttribute("_CoordinateAxes");
        }
        if (att != null) {
            coords = att.getStringValue();
        }
        if (is2D) {
            return this.getRunDimensionName() + " " + timeCoordName + " " + coords;
        }
        return timeCoordName + "_" + this.getRunDimensionName() + " " + timeCoordName + " " + coords;
    }
    
    private void addAttributeInfo(final NetcdfDataset result, final String attName, final String info) {
        final Attribute att = result.findGlobalAttribute(attName);
        if (att == null) {
            result.addAttribute(null, new Attribute(attName, info));
        }
        else {
            final String oldValue = att.getStringValue();
            result.addAttribute(null, new Attribute(attName, oldValue + " ;\n" + info));
        }
    }
    
    private GridDataset buildDataset2D(NetcdfDataset result, final NetcdfDataset proto, final FmrcInvLite lite) throws IOException {
        if (result == null) {
            result = new NetcdfDataset();
        }
        result.setLocation(lite.collectionName);
        this.transferGroup(proto.getRootGroup(), result.getRootGroup(), result);
        result.finish();
        this.addAttributeInfo(result, "history", "FMRC 2D Dataset");
        final double[] runOffset = lite.runOffset;
        final String runtimeDimName = this.getRunDimensionName();
        final int nruns = runOffset.length;
        final Dimension runDim = new Dimension(runtimeDimName, nruns);
        result.removeDimension(null, runtimeDimName);
        result.addDimension(null, runDim);
        final DateFormatter dateFormatter = new DateFormatter();
        final ProxyReader2D proxyReader2D = new ProxyReader2D();
        final DataType coordType = DataType.DOUBLE;
        final VariableDS runtimeCoordVar = new VariableDS(result, null, null, runtimeDimName, coordType, runtimeDimName, null, null);
        runtimeCoordVar.addAttribute(new Attribute("long_name", "Run time for ForecastModelRunCollection"));
        runtimeCoordVar.addAttribute(new Attribute("standard_name", "forecast_reference_time"));
        runtimeCoordVar.addAttribute(new Attribute("units", "hours since " + dateFormatter.toDateTimeStringISO(lite.base)));
        runtimeCoordVar.addAttribute(new Attribute("_CoordinateAxisType", AxisType.RunTime.toString()));
        result.removeVariable(null, runtimeCoordVar.getShortName());
        result.addVariable(null, runtimeCoordVar);
        if (FmrcDataset.logger.isDebugEnabled()) {
            FmrcDataset.logger.debug("FmrcDataset: added runtimeCoordVar " + runtimeCoordVar.getName());
        }
        final Array runCoordVals = Array.factory(DataType.DOUBLE, new int[] { nruns }, runOffset);
        runtimeCoordVar.setCachedData(runCoordVals);
        final List<Variable> nonAggVars = result.getVariables();
        for (final FmrcInvLite.Gridset gridset : lite.gridSets) {
            final Group newGroup = result.getRootGroup();
            final Dimension timeDim = new Dimension(gridset.gridsetName, gridset.noffsets);
            result.removeDimension(null, gridset.gridsetName);
            result.addDimension(null, timeDim);
            final DataType dtype = DataType.DOUBLE;
            final String dims = this.getRunDimensionName() + " " + gridset.gridsetName;
            final VariableDS timeVar = new VariableDS(result, newGroup, null, gridset.gridsetName, dtype, dims, null, null);
            timeVar.addAttribute(new Attribute("long_name", "Forecast time for ForecastModelRunCollection"));
            timeVar.addAttribute(new Attribute("standard_name", "time"));
            timeVar.addAttribute(new Attribute("units", "hours since " + dateFormatter.toDateTimeStringISO(lite.base)));
            timeVar.addAttribute(new Attribute("missing_value", Double.NaN));
            timeVar.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Time.toString()));
            newGroup.removeVariable(gridset.gridsetName);
            newGroup.addVariable(timeVar);
            final Array timeCoordVals = Array.factory(DataType.DOUBLE, timeVar.getShape(), gridset.timeOffset);
            timeVar.setCachedData(timeCoordVals);
            if (gridset.timeBounds != null) {
                final String bname = timeVar.getShortName() + "_bounds";
                timeVar.addAttribute(new Attribute("bounds", bname));
                final Dimension bd = DatasetConstructor.getBoundsDimension(result);
                final VariableDS boundsVar = new VariableDS(result, newGroup, null, bname, dtype, dims + " " + bd.getName(), null, null);
                boundsVar.addAttribute(new Attribute("long_name", "bounds for " + timeVar.getShortName()));
                boundsVar.setCachedData(Array.factory(DataType.DOUBLE, new int[] { nruns, gridset.noffsets, 2 }, gridset.timeBounds));
                newGroup.addVariable(boundsVar);
            }
            for (final FmrcInvLite.Gridset.Grid ugrid : gridset.grids) {
                final VariableDS aggVar = (VariableDS)result.findVariable(ugrid.name);
                if (aggVar == null) {
                    FmrcDataset.logger.error("cant find ugrid variable " + ugrid.name + " in collection " + lite.collectionName);
                }
                else {
                    List<Dimension> dimList = aggVar.getDimensions();
                    dimList = dimList.subList(1, dimList.size());
                    dimList.add(0, timeDim);
                    dimList.add(0, runDim);
                    aggVar.setDimensions(dimList);
                    aggVar.setProxyReader(proxyReader2D);
                    aggVar.setSPobject(ugrid);
                    nonAggVars.remove(aggVar);
                    final String coords = this.makeCoordinateList(aggVar, gridset.gridsetName, true);
                    aggVar.removeAttribute("_CoordinateAxes");
                    aggVar.addAttribute(new Attribute("coordinates", coords));
                }
            }
        }
        result.finish();
        final Formatter parseInfo = new Formatter();
        final GridDataset gds = new ucar.nc2.dt.grid.GridDataset(result, parseInfo);
        return gds;
    }
    
    private CoordinateSystem findReplacementCs(final CoordinateSystem protoCs, final String timeDim, final NetcdfDataset result) {
        final CoordinateSystem replace = result.findCoordinateSystem(protoCs.getName());
        if (replace != null) {
            return replace;
        }
        final List<CoordinateAxis> axes = new ArrayList<CoordinateAxis>();
        for (final CoordinateAxis axis : protoCs.getCoordinateAxes()) {
            final Variable v = result.findCoordinateAxis(axis.getName());
            CoordinateAxis ra;
            if (v instanceof CoordinateAxis) {
                ra = (CoordinateAxis)v;
            }
            else {
                ra = result.addCoordinateAxis((VariableDS)v);
                if (axis.getAxisType() != null) {
                    ra.setAxisType(axis.getAxisType());
                    ra.addAttribute(new Attribute("_CoordinateAxisType", axis.getAxisType().toString()));
                }
            }
            axes.add(ra);
        }
        final CoordinateSystem cs = new CoordinateSystem(result, axes, protoCs.getCoordinateTransforms());
        result.addCoordinateSystem(cs);
        return cs;
    }
    
    private GridDataset buildDataset1D(final NetcdfDataset proto, final FmrcInvLite lite, final TimeInventory timeInv) throws IOException {
        final NetcdfDataset result = new NetcdfDataset();
        result.setLocation(lite.collectionName);
        this.transferGroup(proto.getRootGroup(), result.getRootGroup(), result);
        result.finish();
        this.addAttributeInfo(result, "history", "FMRC " + timeInv.getName() + " Dataset");
        final DateFormatter dateFormatter = new DateFormatter();
        final ProxyReader1D proxyReader1D = new ProxyReader1D();
        final List<Variable> nonAggVars = result.getVariables();
        for (final FmrcInvLite.Gridset gridset : lite.gridSets) {
            final Group group = result.getRootGroup();
            final String timeDimName = gridset.gridsetName;
            final int ntimes = timeInv.getTimeLength(gridset);
            if (ntimes == 0) {
                for (final FmrcInvLite.Gridset.Grid ugrid : gridset.grids) {
                    result.removeVariable(group, ugrid.name);
                }
            }
            else {
                final Dimension timeDim = new Dimension(timeDimName, ntimes);
                result.removeDimension(group, timeDimName);
                result.addDimension(group, timeDim);
                group.removeVariable(timeDimName);
                final FmrcInvLite.ValueB timeCoordValues = timeInv.getTimeCoords(gridset);
                if (timeCoordValues != null) {
                    this.makeTimeCoordinate(result, group, timeDimName, lite.base, timeCoordValues, dateFormatter);
                }
                group.removeVariable(timeDimName + "_run");
                final double[] runtimeCoordValues = timeInv.getRunTimeCoords(gridset);
                if (runtimeCoordValues != null) {
                    this.makeRunTimeCoordinate(result, group, timeDimName, lite.base, runtimeCoordValues, dateFormatter);
                }
                group.removeVariable(timeDimName + "_offset");
                final double[] offsetCoordValues = timeInv.getOffsetCoords(gridset);
                if (offsetCoordValues != null) {
                    this.makeOffsetCoordinate(result, group, timeDimName, lite.base, offsetCoordValues, dateFormatter);
                }
                for (final FmrcInvLite.Gridset.Grid ugrid2 : gridset.grids) {
                    final VariableDS aggVar = (VariableDS)result.findVariable(ugrid2.name);
                    if (aggVar == null) {
                        FmrcDataset.logger.error("cant find ugrid variable " + ugrid2.name + " in collection " + lite.collectionName);
                    }
                    else {
                        List<Dimension> dimList = aggVar.getDimensions();
                        dimList = dimList.subList(1, dimList.size());
                        dimList.add(0, timeDim);
                        aggVar.setDimensions(dimList);
                        aggVar.setProxyReader(proxyReader1D);
                        aggVar.setSPobject(new Vstate1D(ugrid2, timeInv));
                        nonAggVars.remove(aggVar);
                        final String coords = this.makeCoordinateList(aggVar, timeDimName, false);
                        aggVar.removeAttribute("_CoordinateAxes");
                        aggVar.addAttribute(new Attribute("coordinates", coords));
                    }
                }
            }
        }
        result.finish();
        for (final Variable v : nonAggVars) {
            final VariableDS protoV = (VariableDS)proto.findVariable(v.getName());
            if (protoV.hasCachedDataRecurse()) {
                v.setCachedData(protoV.read());
            }
            else {
                v.setProxyReader(protoV.getProxyReader());
            }
        }
        final CoordSysBuilderIF builder = result.enhance();
        return new ucar.nc2.dt.grid.GridDataset(result);
    }
    
    private VariableDS makeTimeCoordinate(final NetcdfDataset result, final Group group, final String dimName, final Date base, final FmrcInvLite.ValueB valueb, final DateFormatter dateFormatter) {
        final DataType dtype = DataType.DOUBLE;
        final VariableDS timeVar = new VariableDS(result, group, null, dimName, dtype, dimName, null, null);
        timeVar.addAttribute(new Attribute("long_name", "Forecast time for ForecastModelRunCollection"));
        timeVar.addAttribute(new Attribute("standard_name", "time"));
        timeVar.addAttribute(new Attribute("units", "hours since " + dateFormatter.toDateTimeStringISO(base)));
        timeVar.addAttribute(new Attribute("missing_value", Double.NaN));
        timeVar.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Time.toString()));
        final int ntimes = valueb.offset.length;
        timeVar.setCachedData(Array.factory(DataType.DOUBLE, new int[] { ntimes }, valueb.offset));
        group.addVariable(timeVar);
        if (valueb.bounds != null) {
            final String bname = timeVar.getShortName() + "_bounds";
            timeVar.addAttribute(new Attribute("bounds", bname));
            final Dimension bd = DatasetConstructor.getBoundsDimension(result);
            final VariableDS boundsVar = new VariableDS(result, group, null, bname, dtype, dimName + " " + bd.getName(), null, null);
            boundsVar.addAttribute(new Attribute("long_name", "bounds for " + timeVar.getShortName()));
            boundsVar.setCachedData(Array.factory(DataType.DOUBLE, new int[] { ntimes, 2 }, valueb.bounds));
            group.addVariable(boundsVar);
        }
        return timeVar;
    }
    
    private VariableDS makeRunTimeCoordinate(final NetcdfDataset result, final Group group, final String dimName, final Date base, final double[] values, final DateFormatter dateFormatter) {
        final DataType dtype = DataType.DOUBLE;
        final VariableDS timeVar = new VariableDS(result, group, null, dimName + "_run", dtype, dimName, null, null);
        timeVar.addAttribute(new Attribute("long_name", "run times for coordinate = " + dimName));
        timeVar.addAttribute(new Attribute("standard_name", "forecast_reference_time"));
        timeVar.addAttribute(new Attribute("units", "hours since " + dateFormatter.toDateTimeStringISO(base)));
        timeVar.addAttribute(new Attribute("missing_value", Double.NaN));
        timeVar.addAttribute(new Attribute("_CoordinateAxisType", AxisType.RunTime.toString()));
        final int ntimes = values.length;
        final ArrayDouble.D1 timeCoordVals = (ArrayDouble.D1)Array.factory(DataType.DOUBLE, new int[] { ntimes }, values);
        timeVar.setCachedData(timeCoordVals);
        group.addVariable(timeVar);
        return timeVar;
    }
    
    private VariableDS makeOffsetCoordinate(final NetcdfDataset result, final Group group, final String dimName, final Date base, final double[] values, final DateFormatter dateFormatter) {
        final DataType dtype = DataType.DOUBLE;
        final VariableDS timeVar = new VariableDS(result, group, null, dimName + "_offset", dtype, dimName, null, null);
        timeVar.addAttribute(new Attribute("long_name", "offset hour from start of run for coordinate = " + dimName));
        timeVar.addAttribute(new Attribute("standard_name", "forecast_period"));
        timeVar.addAttribute(new Attribute("units", "hours since " + dateFormatter.toDateTimeStringISO(base)));
        timeVar.addAttribute(new Attribute("missing_value", Double.NaN));
        final int ntimes = values.length;
        final ArrayDouble.D1 timeCoordVals = (ArrayDouble.D1)Array.factory(DataType.DOUBLE, new int[] { ntimes }, values);
        timeVar.setCachedData(timeCoordVals);
        group.addVariable(timeVar);
        return timeVar;
    }
    
    private Array read(final TimeInventory.Instance timeInstance, final String varName, final List<Range> innerSection, final HashMap<String, NetcdfDataset> openFilesRead) throws IOException, InvalidRangeException {
        final NetcdfFile ncfile = this.open(timeInstance.getDatasetLocation(), openFilesRead);
        if (ncfile == null) {
            return null;
        }
        final Variable v = ncfile.findVariable(varName);
        if (v == null) {
            return null;
        }
        final Range timeRange = new Range(timeInstance.getDatasetIndex(), timeInstance.getDatasetIndex());
        final Section s = new Section(innerSection);
        s.insertRange(0, timeRange);
        return v.read(s);
    }
    
    private NetcdfDataset open(final String location, final HashMap<String, NetcdfDataset> openFiles) {
        NetcdfDataset ncd = null;
        if (openFiles != null) {
            ncd = openFiles.get(location);
            if (ncd != null) {
                return ncd;
            }
        }
        try {
            if (this.config.innerNcml == null) {
                ncd = NetcdfDataset.acquireDataset(location, null);
            }
            else {
                final NetcdfFile nc = NetcdfDataset.acquireFile(location, null);
                ncd = NcMLReader.mergeNcML(nc, this.config.innerNcml);
                ncd.enhance();
            }
        }
        catch (IOException ioe) {
            FmrcDataset.logger.error("Cant open file ", ioe);
            return null;
        }
        if (openFiles != null && ncd != null) {
            openFiles.put(location, ncd);
        }
        return ncd;
    }
    
    private void closeAll(final HashMap<String, NetcdfDataset> openFiles) throws IOException {
        for (final NetcdfDataset ncfile : openFiles.values()) {
            ncfile.close();
        }
        openFiles.clear();
    }
    
    protected Variable findVariable(final NetcdfFile ncfile, final Variable client) {
        Variable v = ncfile.findVariable(client.getName());
        if (v == null) {
            final VariableEnhanced ve = (VariableEnhanced)client;
            v = ncfile.findVariable(ve.getOriginalName());
        }
        return v;
    }
    
    public void showDetails(final Formatter out) {
        out.format("==========================%nproto=%n%s%n", this.state.proto);
    }
    
    static {
        logger = LoggerFactory.getLogger(FmrcDataset.class);
    }
    
    private class State
    {
        NetcdfDataset proto;
        FmrcInvLite lite;
        
        private State(final NetcdfDataset proto, final FmrcInvLite lite) {
            this.proto = proto;
            this.lite = lite;
        }
    }
    
    private class ProxyReader2D implements ProxyReader
    {
        public Array reallyRead(final Variable mainv, final CancelTask cancelTask) throws IOException {
            try {
                return this.reallyRead(mainv, mainv.getShapeAsSection(), cancelTask);
            }
            catch (InvalidRangeException e) {
                throw new IOException(e);
            }
        }
        
        public Array reallyRead(final Variable mainv, final Section section, final CancelTask cancelTask) throws IOException, InvalidRangeException {
            final FmrcInvLite.Gridset.Grid gridLite = (FmrcInvLite.Gridset.Grid)mainv.getSPobject();
            final DataType dtype = (mainv instanceof VariableDS) ? ((VariableDS)mainv).getOriginalDataType() : mainv.getDataType();
            final Array allData = Array.factory(dtype, section.getShape());
            int destPos = 0;
            final List<Range> ranges = section.getRanges();
            final Range runRange = ranges.get(0);
            final Range timeRange = ranges.get(1);
            final List<Range> innerSection = ranges.subList(2, ranges.size());
            final HashMap<String, NetcdfDataset> openFilesRead = new HashMap<String, NetcdfDataset>();
            try {
                final Range.Iterator runIter = runRange.getIterator();
                while (runIter.hasNext()) {
                    final int runIdx = runIter.next();
                    final Range.Iterator timeIter = timeRange.getIterator();
                    while (timeIter.hasNext()) {
                        final int timeIdx = timeIter.next();
                        Array result = null;
                        final TimeInventory.Instance timeInv = gridLite.getInstance(runIdx, timeIdx);
                        if (timeInv != null) {
                            result = FmrcDataset.this.read(timeInv, gridLite.name, innerSection, openFilesRead);
                            result = MAMath.convert(result, dtype);
                        }
                        if (result == null) {
                            final int[] shape = new Section(innerSection).getShape();
                            result = ((VariableDS)mainv).getMissingDataArray(shape);
                        }
                        Array.arraycopy(result, 0, allData, destPos, (int)result.getSize());
                        destPos += (int)result.getSize();
                    }
                }
                return allData;
            }
            finally {
                FmrcDataset.this.closeAll(openFilesRead);
            }
        }
    }
    
    private class Vstate1D
    {
        FmrcInvLite.Gridset.Grid gridLite;
        TimeInventory timeInv;
        
        private Vstate1D(final FmrcInvLite.Gridset.Grid gridLite, final TimeInventory timeInv) {
            this.gridLite = gridLite;
            this.timeInv = timeInv;
        }
    }
    
    private class ProxyReader1D implements ProxyReader
    {
        public Array reallyRead(final Variable mainv, final CancelTask cancelTask) throws IOException {
            try {
                return this.reallyRead(mainv, mainv.getShapeAsSection(), cancelTask);
            }
            catch (InvalidRangeException e) {
                throw new IOException(e);
            }
        }
        
        public Array reallyRead(final Variable mainv, final Section section, final CancelTask cancelTask) throws IOException, InvalidRangeException {
            final Vstate1D vstate = (Vstate1D)mainv.getSPobject();
            final DataType dtype = (mainv instanceof VariableDS) ? ((VariableDS)mainv).getOriginalDataType() : mainv.getDataType();
            final Array allData = Array.factory(dtype, section.getShape());
            int destPos = 0;
            final List<Range> ranges = section.getRanges();
            final Range timeRange = ranges.get(0);
            final List<Range> innerSection = ranges.subList(1, ranges.size());
            final HashMap<String, NetcdfDataset> openFilesRead = new HashMap<String, NetcdfDataset>();
            try {
                final Range.Iterator timeIter = timeRange.getIterator();
                while (timeIter.hasNext()) {
                    final int timeIdx = timeIter.next();
                    Array result = null;
                    final TimeInventory.Instance timeInv = vstate.timeInv.getInstance(vstate.gridLite, timeIdx);
                    if (timeInv == null) {
                        FmrcDataset.logger.error("Missing Inventory timeInx=" + timeIdx + " for " + mainv.getName() + " in " + FmrcDataset.this.state.lite.collectionName);
                    }
                    else if (timeInv.getDatasetLocation() != null) {
                        result = FmrcDataset.this.read(timeInv, mainv.getName(), innerSection, openFilesRead);
                        result = MAMath.convert(result, dtype);
                    }
                    if (result == null) {
                        final int[] shape = new Section(innerSection).getShape();
                        result = ((VariableDS)mainv).getMissingDataArray(shape);
                    }
                    Array.arraycopy(result, 0, allData, destPos, (int)result.getSize());
                    destPos += (int)result.getSize();
                }
                return allData;
            }
            finally {
                FmrcDataset.this.closeAll(openFilesRead);
            }
        }
    }
    
    protected class DatasetProxyReader implements ProxyReader
    {
        String location;
        
        DatasetProxyReader(final String location) {
            this.location = location;
        }
        
        public Array reallyRead(final Variable client, final CancelTask cancelTask) throws IOException {
            NetcdfFile ncfile = null;
            try {
                ncfile = FmrcDataset.this.open(this.location, null);
                if (cancelTask != null && cancelTask.isCancel()) {
                    return null;
                }
                final Variable proxyV = FmrcDataset.this.findVariable(ncfile, client);
                return proxyV.read();
            }
            finally {
                ncfile.close();
            }
        }
        
        public Array reallyRead(final Variable client, final Section section, final CancelTask cancelTask) throws IOException, InvalidRangeException {
            NetcdfFile ncfile = null;
            try {
                ncfile = FmrcDataset.this.open(this.location, null);
                final Variable proxyV = FmrcDataset.this.findVariable(ncfile, client);
                if (cancelTask != null && cancelTask.isCancel()) {
                    return null;
                }
                return proxyV.read(section);
            }
            finally {
                ncfile.close();
            }
        }
    }
}
