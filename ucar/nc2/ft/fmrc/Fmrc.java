// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.fmrc;

import org.slf4j.LoggerFactory;
import java.util.Iterator;
import java.util.Map;
import java.util.Collections;
import thredds.inventory.MFile;
import java.util.ArrayList;
import java.util.HashMap;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dt.GridDataset;
import ucar.nc2.dataset.NetcdfDataset;
import java.util.List;
import org.jdom.Element;
import thredds.inventory.DatasetCollectionManager;
import java.io.IOException;
import thredds.inventory.NcmlCollectionReader;
import thredds.inventory.DatasetCollectionFromCatalog;
import java.util.Formatter;
import java.util.Date;
import thredds.inventory.FeatureCollectionConfig;
import thredds.inventory.CollectionManager;
import org.slf4j.Logger;
import net.jcip.annotations.ThreadSafe;

@ThreadSafe
public class Fmrc
{
    private static Logger logger;
    private final CollectionManager manager;
    private final FeatureCollectionConfig.Config config;
    private Object lock;
    private FmrcDataset fmrcDataset;
    private volatile boolean forceProto;
    private volatile Date lastInvChanged;
    private volatile Date lastProtoChanged;
    
    public static Fmrc open(final String collection, final Formatter errlog) throws IOException {
        if (collection.startsWith("catalog:")) {
            final DatasetCollectionFromCatalog manager = new DatasetCollectionFromCatalog(collection);
            return new Fmrc(manager);
        }
        if (!collection.endsWith(".ncml")) {
            return new Fmrc(collection, errlog);
        }
        final NcmlCollectionReader ncmlCollection = NcmlCollectionReader.open(collection, errlog);
        if (ncmlCollection == null) {
            return null;
        }
        final Fmrc fmrc = new Fmrc(ncmlCollection.getDatasetManager());
        fmrc.setNcml(ncmlCollection.getNcmlOuter(), ncmlCollection.getNcmlInner());
        return fmrc;
    }
    
    public static Fmrc open(final FeatureCollectionConfig.Config config, final Formatter errlog) throws IOException {
        if (config.spec.startsWith("catalog:")) {
            final DatasetCollectionFromCatalog manager = new DatasetCollectionFromCatalog(config.spec);
            return new Fmrc(manager);
        }
        return new Fmrc(config, errlog);
    }
    
    private Fmrc(final String collectionSpec, final Formatter errlog) {
        this.lock = new Object();
        this.forceProto = false;
        this.manager = new DatasetCollectionManager(collectionSpec, errlog);
        this.config = new FeatureCollectionConfig.Config();
    }
    
    private Fmrc(final FeatureCollectionConfig.Config config, final Formatter errlog) {
        this.lock = new Object();
        this.forceProto = false;
        final DatasetCollectionManager dcm = new DatasetCollectionManager(config, errlog);
        dcm.setRecheck(config.recheckAfter);
        this.manager = dcm;
        this.config = config;
    }
    
    public Fmrc(final CollectionManager manager) {
        this.lock = new Object();
        this.forceProto = false;
        this.manager = manager;
        this.config = new FeatureCollectionConfig.Config();
    }
    
    public void setNcml(final Element outerNcml, final Element innerNcml) {
        this.config.protoConfig.outerNcml = outerNcml;
        this.config.innerNcml = innerNcml;
    }
    
    public double getOlderThanFilterInSecs() {
        if (this.manager instanceof DatasetCollectionManager) {
            return ((DatasetCollectionManager)this.manager).getOlderThanFilterInSecs();
        }
        return -1.0;
    }
    
    public void close() {
        this.manager.close();
    }
    
    public CollectionManager getManager() {
        return this.manager;
    }
    
    public FmrcInv getFmrcInv(final Formatter debug) throws IOException {
        return this.makeFmrcInv(debug);
    }
    
    public void triggerProto() {
        this.forceProto = true;
    }
    
    public void triggerRescan() throws IOException {
        this.checkNeeded(true);
    }
    
    public List<Date> getRunDates() throws IOException {
        this.checkNeeded(false);
        return this.fmrcDataset.getRunDates();
    }
    
    public List<Date> getForecastDates() throws IOException {
        this.checkNeeded(false);
        return this.fmrcDataset.getForecastDates();
    }
    
    public double[] getForecastOffsets() throws IOException {
        this.checkNeeded(false);
        return this.fmrcDataset.getForecastOffsets();
    }
    
    public GridDataset getDataset2D(final NetcdfDataset result) throws IOException {
        this.checkNeeded(false);
        final GridDataset gds = this.fmrcDataset.getNetcdfDataset2D(result);
        return gds;
    }
    
    public GridDataset getDatasetBest() throws IOException {
        this.checkNeeded(false);
        final GridDataset gds = this.fmrcDataset.getBest();
        return gds;
    }
    
    public GridDataset getDatasetBest(final FeatureCollectionConfig.BestDataset bd) throws IOException {
        this.checkNeeded(false);
        final GridDataset gds = this.fmrcDataset.getBest(bd);
        return gds;
    }
    
    public GridDataset getRunTimeDataset(final Date run) throws IOException {
        this.checkNeeded(false);
        final GridDataset gds = this.fmrcDataset.getRunTimeDataset(run);
        return gds;
    }
    
    public GridDataset getConstantForecastDataset(final Date time) throws IOException {
        this.checkNeeded(false);
        final GridDataset gds = this.fmrcDataset.getConstantForecastDataset(time);
        return gds;
    }
    
    public GridDataset getConstantOffsetDataset(final double hour) throws IOException {
        this.checkNeeded(false);
        final GridDataset gds = this.fmrcDataset.getConstantOffsetDataset(hour);
        return gds;
    }
    
    public boolean checkInvState(final Date lastInvChange) throws IOException {
        this.checkNeeded(false);
        return !this.lastInvChanged.before(lastInvChange);
    }
    
    public boolean checkProtoState(final Date lastProtoChanged) throws IOException {
        this.checkNeeded(false);
        return !this.lastProtoChanged.before(lastProtoChanged);
    }
    
    private void checkNeeded(final boolean force) throws IOException {
        synchronized (this.lock) {
            final boolean forceProtoLocal = this.forceProto;
            if (this.fmrcDataset == null) {
                try {
                    this.fmrcDataset = new FmrcDataset(this.config);
                    this.manager.scan(null);
                    final FmrcInv fmrcInv = this.makeFmrcInv(null);
                    this.fmrcDataset.setInventory(fmrcInv, forceProtoLocal);
                    if (forceProtoLocal) {
                        this.forceProto = false;
                    }
                    this.lastInvChanged = new Date();
                    this.lastProtoChanged = new Date();
                    return;
                }
                catch (Throwable t) {
                    Fmrc.logger.error(this.config.spec + ": initial fmrcDataset creation failed", t);
                    throw new RuntimeException(t);
                }
            }
            if (!force && !this.manager.isRescanNeeded()) {
                return;
            }
            if (!this.manager.rescan()) {
                return;
            }
            try {
                final FmrcInv fmrcInv = this.makeFmrcInv(null);
                this.fmrcDataset.setInventory(fmrcInv, forceProtoLocal);
                if (Fmrc.logger.isInfoEnabled()) {
                    Fmrc.logger.info(this.config.spec + ": make new Dataset, new proto = " + forceProtoLocal);
                }
                if (forceProtoLocal) {
                    this.forceProto = false;
                }
                this.lastInvChanged = new Date();
                if (forceProtoLocal) {
                    this.lastProtoChanged = new Date();
                }
            }
            catch (Throwable t) {
                Fmrc.logger.error(this.config.spec + ": rescan failed");
                throw new RuntimeException(t);
            }
        }
    }
    
    private FmrcInv makeFmrcInv(final Formatter debug) throws IOException {
        try {
            final Map<Date, FmrInv> fmrMap = new HashMap<Date, FmrInv>();
            final List<FmrInv> fmrList = new ArrayList<FmrInv>();
            final List<MFile> fileList = this.manager.getFiles();
            for (final MFile f : fileList) {
                if (Fmrc.logger.isDebugEnabled()) {
                    Fmrc.logger.debug("Fmrc: " + this.config.spec + ": file=" + f.getPath());
                }
                GridDatasetInv inv = null;
                try {
                    inv = GridDatasetInv.open(this.manager, f, this.config.innerNcml);
                }
                catch (IOException ioe) {
                    Fmrc.logger.warn("Error opening " + f.getPath() + "(skipped)", ioe.getMessage());
                    continue;
                }
                final Date runDate = inv.getRunDate();
                if (debug != null) {
                    debug.format("  opened %s rundate = %s%n", f.getPath(), inv.getRunDateString());
                }
                FmrInv fmr = fmrMap.get(runDate);
                if (fmr == null) {
                    fmr = new FmrInv(runDate);
                    fmrMap.put(runDate, fmr);
                    fmrList.add(fmr);
                }
                fmr.addDataset(inv, debug);
            }
            if (debug != null) {
                debug.format("%n", new Object[0]);
            }
            Collections.sort(fmrList);
            for (final FmrInv fmr2 : fmrList) {
                fmr2.finish();
                if (Fmrc.logger.isDebugEnabled()) {
                    Fmrc.logger.debug("Fmrc: " + this.config.spec + ": fmr " + fmr2.getRunDate() + " nfiles= " + fmr2.getFiles().size());
                }
            }
            return new FmrcInv(this.manager.getCollectionName(), fmrList, this.config.fmrcConfig.regularize);
        }
        catch (Throwable t) {
            Fmrc.logger.error("makeFmrcInv", t);
            throw new RuntimeException(t);
        }
    }
    
    public void showDetails(final Formatter out) throws IOException {
        this.checkNeeded(false);
        this.fmrcDataset.showDetails(out);
    }
    
    public static void main(final String[] args) throws IOException {
        final Formatter errlog = new Formatter();
        final String spec1 = "/data/testdata/ncml/nc/nam_c20s/NAM_CONUS_20km_surface_#yyyyMMdd_HHmm#.grib1";
        final String spec2 = "/data/testdata/grid/grib/grib1/data/agg/.*grb";
        final String spec3 = "/data/testdata/ncml/nc/ruc_conus40/RUC_CONUS_40km_#yyyyMMdd_HHmm#.grib1";
        final String spec4 = "/data/testdata/cdmUnitTest/rtmodels/.*_nmm\\.GrbF[0-9]{5}$";
        final String cat1 = "catalog:http://motherlode.ucar.edu:8080/thredds/catalog/fmrc/NCEP/RUC2/CONUS_40km/files/catalog.xml";
        final String cat2 = "catalog:http://motherlode.ucar.edu:8080/thredds/catalog/fmrc/NCEP/NDFD/CONUS_5km/files/catalog.xml";
        final String specH = "C:/data/datasets/nogaps/US058GMET-GR1mdl.*air_temp";
        final String specH2 = "C:/data/ft/grid/cg/.*nc$";
        final String specH3 = "C:/data/ft/grid/namExtract/#yyyyMMdd_HHmm#.*nc$";
        final Fmrc fmrc = new Fmrc(specH3, errlog);
        System.out.printf("errlog = %s%n", errlog);
    }
    
    static {
        Fmrc.logger = LoggerFactory.getLogger(Fmrc.class);
    }
}
