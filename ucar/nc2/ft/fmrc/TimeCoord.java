// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.fmrc;

import java.util.Set;
import java.util.Collections;
import java.util.Arrays;
import java.util.HashSet;
import ucar.nc2.util.Misc;
import java.util.Formatter;
import ucar.nc2.units.DateFormatter;
import java.util.Iterator;
import java.util.ArrayList;
import ucar.nc2.units.DateUnit;
import ucar.nc2.dataset.CoordinateAxis1DTime;
import java.util.List;
import java.util.Date;

public class TimeCoord implements Comparable
{
    public static final TimeCoord EMPTY;
    private Date runDate;
    private List<GridDatasetInv.Grid> gridInv;
    private int id;
    private String axisName;
    private boolean isInterval;
    private double[] offset;
    private double[] bound1;
    private double[] bound2;
    
    TimeCoord(final Date runDate) {
        this.isInterval = false;
        this.runDate = runDate;
    }
    
    TimeCoord(final Date runDate, final double[] offset) {
        this.isInterval = false;
        this.runDate = runDate;
        this.offset = offset;
    }
    
    TimeCoord(final TimeCoord from) {
        this.isInterval = false;
        this.runDate = from.runDate;
        this.axisName = from.axisName;
        this.offset = from.offset;
        this.isInterval = from.isInterval;
        this.bound1 = from.bound1;
        this.bound2 = from.bound2;
        this.id = from.id;
    }
    
    TimeCoord(final Date runDate, final CoordinateAxis1DTime axis) {
        this.isInterval = false;
        this.runDate = runDate;
        this.axisName = axis.getName();
        DateUnit unit = null;
        try {
            unit = new DateUnit(axis.getUnitsString());
        }
        catch (Exception e) {
            throw new IllegalArgumentException("Not a unit of time " + axis.getUnitsString());
        }
        final int n = (int)axis.getSize();
        if (axis.isInterval()) {
            this.isInterval = true;
            this.bound1 = axis.getBound1();
            this.bound2 = axis.getBound2();
        }
        else {
            this.offset = new double[n];
            for (int i = 0; i < axis.getSize(); ++i) {
                final Date d = unit.makeDate(axis.getCoordValue(i));
                this.offset[i] = FmrcInv.getOffsetInHours(runDate, d);
            }
        }
    }
    
    void addGridInventory(final GridDatasetInv.Grid grid) {
        if (this.gridInv == null) {
            this.gridInv = new ArrayList<GridDatasetInv.Grid>();
        }
        this.gridInv.add(grid);
    }
    
    public Date getRunDate() {
        return this.runDate;
    }
    
    public boolean isInterval() {
        return this.isInterval;
    }
    
    public List<GridDatasetInv.Grid> getGridInventory() {
        return (this.gridInv == null) ? new ArrayList<GridDatasetInv.Grid>() : this.gridInv;
    }
    
    public int getId() {
        return this.id;
    }
    
    public void setId(final int id) {
        this.id = id;
    }
    
    public String getName() {
        if (this == TimeCoord.EMPTY) {
            return "EMPTY";
        }
        return (this.id == 0) ? "time" : ("time" + this.id);
    }
    
    public String getAxisName() {
        return this.axisName;
    }
    
    public int getNCoords() {
        return this.isInterval ? this.bound1.length : this.offset.length;
    }
    
    public double[] getOffsetTimes() {
        return this.isInterval ? this.bound2 : this.offset;
    }
    
    public double[] getBound1() {
        return this.bound1;
    }
    
    public double[] getBound2() {
        return this.bound2;
    }
    
    public void setOffsetTimes(final double[] offset) {
        this.offset = offset;
    }
    
    public void setBounds(final double[] bound1, final double[] bound2) {
        this.bound1 = bound1;
        this.bound2 = bound2;
        this.isInterval = true;
    }
    
    public void setBounds(final List<Tinv> tinvs) {
        this.bound1 = new double[tinvs.size()];
        this.bound2 = new double[tinvs.size()];
        int count = 0;
        for (final Tinv tinv : tinvs) {
            this.bound1[count] = tinv.b1;
            this.bound2[count] = tinv.b2;
            ++count;
        }
        this.isInterval = true;
    }
    
    @Override
    public String toString() {
        final DateFormatter df = new DateFormatter();
        final Formatter out = new Formatter();
        out.format("%-10s %-26s offsets=", this.getName(), df.toDateTimeString(this.runDate));
        if (this.isInterval) {
            for (int i = 0; i < this.bound1.length; ++i) {
                out.format("(%3.1f,%3.1f) ", this.bound1[i], this.bound2[i]);
            }
        }
        else {
            for (final double val : this.offset) {
                out.format("%3.1f, ", val);
            }
        }
        return out.toString();
    }
    
    public boolean equalsData(final TimeCoord tother) {
        if (this.getRunDate() != null && !this.getRunDate().equals(tother.getRunDate())) {
            return false;
        }
        if (this.isInterval != tother.isInterval) {
            return false;
        }
        if (this.isInterval) {
            if (this.bound1.length != tother.bound1.length) {
                return false;
            }
            for (int i = 0; i < this.bound1.length; ++i) {
                if (!Misc.closeEnough(this.bound1[i], tother.bound1[i])) {
                    return false;
                }
                if (!Misc.closeEnough(this.bound2[i], tother.bound2[i])) {
                    return false;
                }
            }
            return true;
        }
        else {
            if (this.offset.length != tother.offset.length) {
                return false;
            }
            for (int i = 0; i < this.offset.length; ++i) {
                if (!Misc.closeEnough(this.offset[i], tother.offset[i])) {
                    return false;
                }
            }
            return true;
        }
    }
    
    public int findInterval(final double b1, final double b2) {
        for (int i = 0; i < this.getNCoords(); ++i) {
            if (Misc.closeEnough(this.bound1[i], b1) && Misc.closeEnough(this.bound2[i], b2)) {
                return i;
            }
        }
        return -1;
    }
    
    public int findIndex(final double offsetHour) {
        final double[] off = this.getOffsetTimes();
        for (int i = 0; i < off.length; ++i) {
            if (Misc.closeEnough(off[i], offsetHour)) {
                return i;
            }
        }
        return -1;
    }
    
    public int compareTo(final Object o) {
        final TimeCoord ot = (TimeCoord)o;
        return this.id - ot.id;
    }
    
    public static TimeCoord findTimeCoord(final List<TimeCoord> timeCoords, final TimeCoord want) {
        if (want == null) {
            return null;
        }
        for (final TimeCoord tc : timeCoords) {
            if (want.equalsData(tc)) {
                return tc;
            }
        }
        final TimeCoord result = new TimeCoord(want);
        timeCoords.add(result);
        return result;
    }
    
    public static TimeCoord makeUnion(final List<TimeCoord> timeCoords, final Date baseDate) {
        if (timeCoords.size() == 0) {
            return new TimeCoord(baseDate);
        }
        if (timeCoords.size() == 1) {
            return timeCoords.get(0);
        }
        if (timeCoords.get(0).isInterval) {
            return makeUnionIntv(timeCoords, baseDate);
        }
        return makeUnionReg(timeCoords, baseDate);
    }
    
    private static TimeCoord makeUnionReg(final List<TimeCoord> timeCoords, final Date baseDate) {
        final Set<Double> offsets = new HashSet<Double>();
        for (final TimeCoord tc : timeCoords) {
            if (tc.isInterval) {
                throw new IllegalArgumentException("Cant mix interval coordinates");
            }
            for (final double off : tc.getOffsetTimes()) {
                offsets.add(off);
            }
        }
        final List<Double> offsetList = Arrays.asList((Double[])offsets.toArray((T[])new Double[offsets.size()]));
        Collections.sort(offsetList);
        final double[] offset = new double[offsetList.size()];
        int count = 0;
        for (final double off2 : offsetList) {
            offset[count++] = off2;
        }
        final TimeCoord result = new TimeCoord(baseDate);
        result.setOffsetTimes(offset);
        return result;
    }
    
    private static TimeCoord makeUnionIntv(final List<TimeCoord> timeCoords, final Date baseDate) {
        final Set<Tinv> offsets = new HashSet<Tinv>();
        for (final TimeCoord tc : timeCoords) {
            if (!tc.isInterval) {
                throw new IllegalArgumentException("Cant mix non-interval coordinates");
            }
            for (int i = 0; i < tc.bound1.length; ++i) {
                offsets.add(new Tinv(tc.bound1[i], tc.bound2[i]));
            }
        }
        final List<Tinv> bounds = Arrays.asList((Tinv[])offsets.toArray((T[])new Tinv[offsets.size()]));
        Collections.sort(bounds);
        final int n = bounds.size();
        final double[] bounds2 = new double[n];
        final double[] bounds3 = new double[n];
        for (int j = 0; j < n; ++j) {
            final Tinv tinv = bounds.get(j);
            bounds2[j] = tinv.b1;
            bounds3[j] = tinv.b2;
        }
        final TimeCoord result = new TimeCoord(baseDate);
        result.setBounds(bounds2, bounds3);
        return result;
    }
    
    static {
        EMPTY = new TimeCoord(new Date(), new double[0]);
    }
    
    public static class Tinv implements Comparable<Tinv>
    {
        private double b1;
        private double b2;
        
        public Tinv(final double offset) {
            this.b2 = offset;
        }
        
        public Tinv(final double b1, final double b2) {
            this.b1 = b1;
            this.b2 = b2;
        }
        
        @Override
        public boolean equals(final Object o) {
            final Tinv tinv = (Tinv)o;
            return Double.compare(tinv.b1, this.b1) == 0 && Double.compare(tinv.b2, this.b2) == 0;
        }
        
        @Override
        public int hashCode() {
            long temp = (this.b1 != 0.0) ? Double.doubleToLongBits(this.b1) : 0L;
            int result = (int)(temp ^ temp >>> 32);
            temp = ((this.b2 != 0.0) ? Double.doubleToLongBits(this.b2) : 0L);
            result = 31 * result + (int)(temp ^ temp >>> 32);
            return result;
        }
        
        public int compareTo(final Tinv o) {
            final int c1 = Double.compare(this.b2, o.b2);
            if (c1 == 0) {
                return Double.compare(this.b1, o.b1);
            }
            return c1;
        }
    }
}
