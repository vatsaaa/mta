// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.scan;

import java.io.OutputStream;
import java.io.PrintStream;
import java.io.ByteArrayOutputStream;
import ucar.nc2.dataset.CoordinateSystem;
import ucar.nc2.ft.FeatureDataset;
import java.io.IOException;
import ucar.nc2.util.CancelTask;
import ucar.nc2.ft.FeatureDatasetFactoryManager;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.constants.FeatureType;
import java.util.Iterator;
import java.util.Collection;
import java.util.Collections;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Formatter;

public class FeatureScan
{
    private String top;
    private boolean subdirs;
    private boolean debug;
    
    public FeatureScan(final String top, final boolean subdirs) {
        this.debug = true;
        this.top = top;
        this.subdirs = subdirs;
    }
    
    public List<Bean> scan(final Formatter errlog) {
        final List<Bean> result = new ArrayList<Bean>();
        final File topFile = new File(this.top);
        if (!topFile.exists()) {
            errlog.format("File %s does not exist", this.top);
            return result;
        }
        if (topFile.isDirectory()) {
            this.scanDirectory(topFile, result, errlog);
        }
        else {
            final Bean fdb = new Bean(topFile);
            result.add(fdb);
        }
        return result;
    }
    
    private void scanDirectory(final File dir, final List<Bean> result, final Formatter errlog) {
        final List<File> files = new ArrayList<File>();
        for (final File f : dir.listFiles()) {
            if (!f.isDirectory()) {
                files.add(f);
            }
        }
        if (files.size() > 0) {
            Collections.sort(files);
            final ArrayList<File> files2 = new ArrayList<File>(files);
            File prev = null;
            for (final File f : files) {
                if (prev != null) {
                    final String name = f.getName();
                    final String stem = this.stem(name);
                    if (name.endsWith(".gbx") || name.endsWith(".gbx8")) {
                        files2.remove(f);
                    }
                    else if (name.endsWith(".ncml")) {
                        if (prev.getName().equals(stem + ".nc")) {
                            files2.remove(prev);
                        }
                    }
                    else if (name.endsWith(".bz2")) {
                        if (prev.getName().equals(stem)) {
                            files2.remove(f);
                        }
                    }
                    else if (name.endsWith(".gz")) {
                        if (prev.getName().equals(stem)) {
                            files2.remove(f);
                        }
                    }
                    else if (name.endsWith(".gzip")) {
                        if (prev.getName().equals(stem)) {
                            files2.remove(f);
                        }
                    }
                    else if (name.endsWith(".zip")) {
                        if (prev.getName().equals(stem)) {
                            files2.remove(f);
                        }
                    }
                    else if (name.endsWith(".Z") && prev.getName().equals(stem)) {
                        files2.remove(f);
                    }
                }
                prev = f;
            }
            for (final File f : files2) {
                result.add(new Bean(f));
            }
        }
        if (this.subdirs) {
            for (final File f : dir.listFiles()) {
                if (f.isDirectory()) {
                    this.scanDirectory(f, result, errlog);
                }
            }
        }
    }
    
    private String stem(final String name) {
        final int pos = name.lastIndexOf(".");
        return (pos > 0) ? name.substring(0, pos) : name;
    }
    
    public static void main(final String[] arg) {
        final FeatureScan scanner = new FeatureScan("C:/data/datasets/modis", true);
        System.out.printf("Beans found %n", new Object[0]);
        final List<Bean> beans = scanner.scan(new Formatter());
        for (final Bean b : beans) {
            System.out.printf(" %40s %20s %10s %10s%n", b.getName(), b.getFileType(), b.getFeatureType(), b.getFeatureImpl());
        }
    }
    
    public class Bean
    {
        public File f;
        String fileType;
        String coordMap;
        FeatureType featureType;
        String ftype;
        String info;
        String ftImpl;
        Throwable problem;
        
        public Bean() {
        }
        
        public Bean(final File f) {
            this.f = f;
            NetcdfDataset ds = null;
            try {
                if (FeatureScan.this.debug) {
                    System.out.printf(" featureScan=%s%n", f.getPath());
                }
                ds = NetcdfDataset.openDataset(f.getPath());
                this.fileType = ds.getFileTypeId();
                this.setCoordMap(ds.getCoordinateSystems());
                final Formatter errlog = new Formatter();
                try {
                    final FeatureDataset featureDataset = FeatureDatasetFactoryManager.wrap(null, ds, null, errlog);
                    if (featureDataset != null) {
                        this.featureType = featureDataset.getFeatureType();
                        if (this.featureType != null) {
                            this.ftype = this.featureType.toString();
                        }
                        this.ftImpl = featureDataset.getImplementationName();
                        final Formatter infof = new Formatter();
                        featureDataset.getDetailInfo(infof);
                        this.info = infof.toString();
                    }
                    else {
                        this.ftype = "FAIL: " + errlog.toString();
                    }
                }
                catch (Throwable t) {
                    this.ftype = "ERR: " + t.getMessage();
                    this.info = errlog.toString();
                    this.problem = t;
                }
            }
            catch (Throwable t2) {
                this.fileType = "ERR: " + t2.getMessage();
                this.problem = t2;
            }
            finally {
                if (ds != null) {
                    try {
                        ds.close();
                    }
                    catch (IOException ex) {}
                }
            }
        }
        
        public String getName() {
            return this.f.getPath();
        }
        
        public String getFileType() {
            return this.fileType;
        }
        
        public String getCoordMap() {
            return this.coordMap;
        }
        
        public void setCoordMap(final List<CoordinateSystem> csysList) {
            CoordinateSystem use = null;
            for (final CoordinateSystem csys : csysList) {
                if (use == null) {
                    use = csys;
                }
                else {
                    if (csys.getCoordinateAxes().size() <= use.getCoordinateAxes().size()) {
                        continue;
                    }
                    use = csys;
                }
            }
            this.coordMap = ((use == null) ? "" : ("f:D(" + use.getRankDomain() + ")->R(" + use.getRankRange() + ")"));
        }
        
        public String getFeatureType() {
            return this.ftype;
        }
        
        public String getFeatureImpl() {
            return this.ftImpl;
        }
        
        @Override
        public String toString() {
            final Formatter f = new Formatter();
            f.format("%s%n %s%n map = '%s'%n %s%n %s%n", this.getName(), this.getFileType(), this.getCoordMap(), this.getFeatureType(), this.getFeatureImpl());
            if (this.info != null) {
                f.format("\n%s", this.info);
            }
            if (this.problem != null) {
                final ByteArrayOutputStream bout = new ByteArrayOutputStream(10000);
                this.problem.printStackTrace(new PrintStream(bout));
                f.format("\n%s", bout.toString());
            }
            return f.toString();
        }
    }
}
