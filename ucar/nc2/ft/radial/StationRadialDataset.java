// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.radial;

import ucar.unidata.util.Product;
import ucar.nc2.units.DateRange;
import java.io.IOException;
import ucar.unidata.geoloc.Station;
import java.util.List;
import ucar.nc2.ft.FeatureDataset;
import ucar.nc2.ft.StationCollection;

public interface StationRadialDataset extends StationCollection, FeatureDataset
{
    StationRadialDataset subset(final List<Station> p0) throws IOException;
    
    RadialSweepFeature getFeature(final Station p0) throws IOException;
    
    RadialSweepFeature getFeature(final Station p0, final DateRange p1) throws IOException;
    
    boolean checkStationProduct(final Product p0);
    
    boolean checkStationProduct(final String p0, final Product p1);
    
    int getStationProductCount(final String p0);
}
