// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.radial;

import ucar.nc2.dt.radial.UF2Dataset;
import ucar.nc2.dt.radial.Nids2Dataset;
import ucar.nc2.dt.radial.LevelII2Dataset;
import ucar.nc2.dt.radial.Dorade2Dataset;
import ucar.nc2.dt.radial.Netcdf2Dataset;
import java.util.ArrayList;
import ucar.nc2.dt.TypedDataset;
import ucar.nc2.ft.FeatureDataset;
import ucar.nc2.util.CancelTask;
import java.io.IOException;
import java.util.Iterator;
import java.util.Formatter;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.dt.TypedDatasetFactoryIF;
import java.util.List;
import ucar.nc2.ft.FeatureDatasetFactory;

public class RadialDatasetStandardFactory implements FeatureDatasetFactory
{
    private static List<TypedDatasetFactoryIF> factories;
    
    static void registerFactory(final Class c) {
        if (!TypedDatasetFactoryIF.class.isAssignableFrom(c)) {
            throw new IllegalArgumentException("Class " + c.getName() + " must implement TypedDatasetFactoryIF");
        }
        Object instance;
        try {
            instance = c.newInstance();
        }
        catch (InstantiationException e) {
            throw new IllegalArgumentException("FeatureDatasetFactoryManager Class " + c.getName() + " cannot instantiate, probably need default Constructor");
        }
        catch (IllegalAccessException e2) {
            throw new IllegalArgumentException("FeatureDatasetFactoryManager Class " + c.getName() + " is not accessible");
        }
        RadialDatasetStandardFactory.factories.add((TypedDatasetFactoryIF)instance);
    }
    
    public Object isMine(final FeatureType wantFeatureType, final NetcdfDataset ds, final Formatter errlog) throws IOException {
        if (wantFeatureType != FeatureType.RADIAL && wantFeatureType != FeatureType.NONE && wantFeatureType != FeatureType.ANY) {
            return null;
        }
        for (final TypedDatasetFactoryIF fac : RadialDatasetStandardFactory.factories) {
            if (fac.isMine(ds)) {
                return fac;
            }
        }
        return null;
    }
    
    public FeatureDataset open(final FeatureType ftype, final NetcdfDataset ncd, final Object analysis, final CancelTask task, final Formatter errlog) throws IOException {
        final TypedDatasetFactoryIF fac = (TypedDatasetFactoryIF)analysis;
        final StringBuilder sbuff = new StringBuilder();
        final TypedDataset result = fac.open(ncd, task, sbuff);
        errlog.format("%s", sbuff);
        return (FeatureDataset)result;
    }
    
    public FeatureType[] getFeatureType() {
        return new FeatureType[] { FeatureType.RADIAL };
    }
    
    static {
        RadialDatasetStandardFactory.factories = new ArrayList<TypedDatasetFactoryIF>(10);
        registerFactory(Netcdf2Dataset.class);
        registerFactory(Dorade2Dataset.class);
        registerFactory(LevelII2Dataset.class);
        registerFactory(Nids2Dataset.class);
        registerFactory(UF2Dataset.class);
    }
}
