// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.radial;

import java.util.Date;
import ucar.unidata.geoloc.EarthLocation;
import java.io.IOException;
import ucar.nc2.Variable;

public interface RadialSweepFeature
{
    Type getType();
    
    Variable getSweepVar();
    
    int getRadialNumber();
    
    int getGateNumber();
    
    float getBeamWidth();
    
    float getNyquistFrequency();
    
    float getRangeToFirstGate();
    
    float getGateSize();
    
    float[] readData() throws IOException;
    
    float[] readData(final int p0) throws IOException;
    
    float getElevation(final int p0) throws IOException;
    
    float[] getElevation() throws IOException;
    
    float getMeanElevation();
    
    float getAzimuth(final int p0) throws IOException;
    
    float[] getAzimuth() throws IOException;
    
    float getMeanAzimuth();
    
    EarthLocation getOrigin(final int p0);
    
    float getTime(final int p0) throws IOException;
    
    Date getStartingTime();
    
    Date getEndingTime();
    
    int getSweepIndex();
    
    void clearSweepMemory();
    
    public enum Type
    {
        NONE;
    }
}
