// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft;

import ucar.unidata.geoloc.LatLonRect;
import java.io.IOException;

public interface ProfileFeatureCollection extends NestedPointFeatureCollection
{
    boolean hasNext() throws IOException;
    
    ProfileFeature next() throws IOException;
    
    void resetIteration() throws IOException;
    
    ProfileFeatureCollection subset(final LatLonRect p0) throws IOException;
}
