// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft;

import ucar.unidata.geoloc.Station;
import java.util.List;
import java.io.IOException;

public interface StationProfileFeatureCollection extends NestedPointFeatureCollection, StationCollection
{
    boolean hasNext() throws IOException;
    
    StationProfileFeature next() throws IOException;
    
    void resetIteration() throws IOException;
    
    StationProfileFeatureCollection subset(final List<Station> p0) throws IOException;
    
    StationProfileFeature getStationProfileFeature(final Station p0) throws IOException;
}
