// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft;

import ucar.nc2.constants.FeatureType;

public interface FeatureCollection
{
    String getName();
    
    FeatureType getCollectionFeatureType();
}
