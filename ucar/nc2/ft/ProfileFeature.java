// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft;

import java.util.Date;
import ucar.unidata.geoloc.LatLonPoint;

public interface ProfileFeature extends PointFeatureCollection
{
    LatLonPoint getLatLon();
    
    Date getTime();
    
    int size();
}
