// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft;

import ucar.nc2.units.DateRange;
import ucar.unidata.geoloc.LatLonRect;
import java.io.IOException;

public interface NestedPointFeatureCollection extends FeatureCollection
{
    int size();
    
    boolean isMultipleNested();
    
    PointFeatureCollectionIterator getPointFeatureCollectionIterator(final int p0) throws IOException;
    
    NestedPointFeatureCollectionIterator getNestedPointFeatureCollectionIterator(final int p0) throws IOException;
    
    NestedPointFeatureCollection subset(final LatLonRect p0) throws IOException;
    
    PointFeatureCollection flatten(final LatLonRect p0, final DateRange p1) throws IOException;
}
