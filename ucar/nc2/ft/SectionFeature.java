// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft;

import java.io.IOException;

public interface SectionFeature extends NestedPointFeatureCollection
{
    int size();
    
    boolean hasNext() throws IOException;
    
    ProfileFeature next() throws IOException;
    
    void resetIteration() throws IOException;
}
