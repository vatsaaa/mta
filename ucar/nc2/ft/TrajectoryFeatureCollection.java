// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft;

import java.io.IOException;

public interface TrajectoryFeatureCollection extends NestedPointFeatureCollection
{
    boolean hasNext() throws IOException;
    
    TrajectoryFeature next() throws IOException;
    
    void resetIteration() throws IOException;
}
