// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft;

import ucar.nc2.util.CancelTask;
import java.io.IOException;
import java.util.Formatter;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.constants.FeatureType;

public interface FeatureDatasetFactory
{
    Object isMine(final FeatureType p0, final NetcdfDataset p1, final Formatter p2) throws IOException;
    
    FeatureDataset open(final FeatureType p0, final NetcdfDataset p1, final Object p2, final CancelTask p3, final Formatter p4) throws IOException;
    
    FeatureType[] getFeatureType();
}
