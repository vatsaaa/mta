// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft;

import java.io.IOException;
import ucar.ma2.StructureData;
import ucar.nc2.units.DateUnit;
import java.util.Date;
import ucar.unidata.geoloc.EarthLocation;

public interface PointFeature
{
    EarthLocation getLocation();
    
    double getObservationTime();
    
    Date getObservationTimeAsDate();
    
    double getNominalTime();
    
    Date getNominalTimeAsDate();
    
    DateUnit getTimeUnit();
    
    StructureData getData() throws IOException;
}
