// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft;

import java.util.List;

public interface FeatureDatasetPoint extends FeatureDataset
{
    List<FeatureCollection> getPointFeatureCollectionList();
}
