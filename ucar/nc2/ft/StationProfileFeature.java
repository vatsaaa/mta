// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft;

import java.util.Date;
import java.util.List;
import ucar.nc2.units.DateRange;
import java.io.IOException;
import ucar.unidata.geoloc.Station;

public interface StationProfileFeature extends Station, NestedPointFeatureCollection
{
    boolean hasNext() throws IOException;
    
    ProfileFeature next() throws IOException;
    
    void resetIteration() throws IOException;
    
    int size();
    
    StationProfileFeature subset(final DateRange p0) throws IOException;
    
    List<Date> getTimes() throws IOException;
    
    ProfileFeature getProfileByDate(final Date p0) throws IOException;
}
