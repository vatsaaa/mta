// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft;

import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.units.DateRange;

public interface TrajectoryFeature extends PointFeatureCollection
{
    int size();
    
    DateRange getDateRange();
    
    LatLonRect getBoundingBox();
}
