// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft;

import ucar.nc2.ft.radial.RadialDatasetStandardFactory;
import ucar.nc2.ft.grid.GridDatasetStandardFactory;
import ucar.nc2.ft.point.standard.PointDatasetStandardFactory;
import java.util.ArrayList;
import ucar.nc2.Variable;
import ucar.nc2.dataset.CoordinateAxis;
import ucar.nc2.dataset.CoordinateSystem;
import ucar.nc2.dt.grid.GridDataset;
import java.util.Iterator;
import java.io.IOException;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.ft.point.collection.CompositeDatasetFactory;
import ucar.nc2.stream.CdmRemoteFeatureDataset;
import ucar.nc2.thredds.ThreddsDataFactory;
import java.util.Formatter;
import ucar.nc2.util.CancelTask;
import java.lang.reflect.Method;
import ucar.nc2.constants.FeatureType;
import java.util.List;

public class FeatureDatasetFactoryManager
{
    private static List<Factory> factoryList;
    private static boolean userMode;
    private static boolean debug;
    
    public static void registerFactory(final FeatureType datatype, final String className) throws ClassNotFoundException {
        final Class c = Class.forName(className);
        registerFactory(datatype, c);
    }
    
    public static void registerFactory(final FeatureType datatype, final Class c) {
        if (!FeatureDatasetFactory.class.isAssignableFrom(c)) {
            throw new IllegalArgumentException("Class " + c.getName() + " must implement FeatureDatasetFactory");
        }
        Object instance;
        try {
            instance = c.newInstance();
        }
        catch (InstantiationException e) {
            throw new IllegalArgumentException("FeatureDatasetFactoryManager Class " + c.getName() + " cannot instantiate, probably need default Constructor");
        }
        catch (IllegalAccessException e2) {
            throw new IllegalArgumentException("FeatureDatasetFactoryManager Class " + c.getName() + " is not accessible");
        }
        if (FeatureDatasetFactoryManager.userMode) {
            FeatureDatasetFactoryManager.factoryList.add(0, new Factory(datatype, c, (FeatureDatasetFactory)instance));
        }
        else {
            FeatureDatasetFactoryManager.factoryList.add(new Factory(datatype, c, (FeatureDatasetFactory)instance));
        }
    }
    
    public static void registerFactory(final String className) throws ClassNotFoundException {
        final Class c = Class.forName(className);
        registerFactory(c);
    }
    
    public static void registerFactory(final Class c) {
        if (!FeatureDatasetFactory.class.isAssignableFrom(c)) {
            throw new IllegalArgumentException("Class " + c.getName() + " must implement FeatureDatasetFactory");
        }
        Object instance;
        try {
            instance = c.newInstance();
        }
        catch (InstantiationException e) {
            throw new IllegalArgumentException("FeatureDatasetFactoryManager Class " + c.getName() + " cannot instantiate, probably need default Constructor");
        }
        catch (IllegalAccessException e2) {
            throw new IllegalArgumentException("FeatureDatasetFactoryManager Class " + c.getName() + " is not accessible");
        }
        try {
            final Method m = c.getMethod("getFeatureType", (Class<?>[])new Class[0]);
            final FeatureType[] arr$;
            final FeatureType[] result = arr$ = (FeatureType[])m.invoke(instance, new Object[0]);
            for (final FeatureType ft : arr$) {
                if (FeatureDatasetFactoryManager.userMode) {
                    FeatureDatasetFactoryManager.factoryList.add(0, new Factory(ft, c, (FeatureDatasetFactory)instance));
                }
                else {
                    FeatureDatasetFactoryManager.factoryList.add(new Factory(ft, c, (FeatureDatasetFactory)instance));
                }
            }
        }
        catch (Exception ex) {
            throw new IllegalArgumentException("FeatureDatasetFactoryManager Class " + c.getName() + " failed invoking getFeatureType()", ex);
        }
    }
    
    public static FeatureDataset open(final FeatureType wantFeatureType, final String location, final CancelTask task, final Formatter errlog) throws IOException {
        if (location.startsWith("thredds:")) {
            final ThreddsDataFactory.Result result = new ThreddsDataFactory().openFeatureDataset(wantFeatureType, location, task);
            errlog.format("%s", result.errLog);
            if (!featureTypeOk(wantFeatureType, result.featureType)) {
                errlog.format("wanted %s but dataset is of type %s%n", wantFeatureType, result.featureType);
                return null;
            }
            return result.featureDataset;
        }
        else {
            if (location.startsWith("cdmremote:")) {
                return CdmRemoteFeatureDataset.factory(wantFeatureType, location);
            }
            if (location.startsWith("collection:")) {
                return CompositeDatasetFactory.factory(location, wantFeatureType, location, errlog);
            }
            final NetcdfDataset ncd = NetcdfDataset.acquireDataset(location, task);
            return wrap(wantFeatureType, ncd, task, errlog);
        }
    }
    
    public static FeatureDataset wrap(final FeatureType wantFeatureType, final NetcdfDataset ncd, final CancelTask task, final Formatter errlog) throws IOException {
        if (FeatureDatasetFactoryManager.debug) {
            System.out.println("wrap " + ncd.getLocation() + " want = " + wantFeatureType);
        }
        if (wantFeatureType == null || wantFeatureType == FeatureType.NONE || wantFeatureType == FeatureType.ANY) {
            return wrapUnknown(ncd, task, errlog);
        }
        Object analysis = null;
        FeatureDatasetFactory useFactory = null;
        for (final Factory fac : FeatureDatasetFactoryManager.factoryList) {
            if (!featureTypeOk(wantFeatureType, fac.featureType)) {
                continue;
            }
            if (FeatureDatasetFactoryManager.debug) {
                System.out.println(" wrap try factory " + fac.factory.getClass().getName());
            }
            analysis = fac.factory.isMine(wantFeatureType, ncd, errlog);
            if (analysis != null) {
                useFactory = fac.factory;
                break;
            }
        }
        if (null == useFactory) {
            return null;
        }
        return useFactory.open(wantFeatureType, ncd, analysis, task, errlog);
    }
    
    private static FeatureDataset wrapUnknown(final NetcdfDataset ncd, final CancelTask task, final Formatter errlog) throws IOException {
        final FeatureType ft = findFeatureType(ncd);
        if (ft != null) {
            return wrap(ft, ncd, task, errlog);
        }
        if (isGrid(ncd.getCoordinateSystems())) {
            final GridDataset gds = new GridDataset(ncd);
            if (gds.getGrids().size() > 0) {
                if (FeatureDatasetFactoryManager.debug) {
                    System.out.println(" wrapUnknown found grids ");
                }
                return gds;
            }
        }
        Object analysis = null;
        FeatureDatasetFactory useFactory = null;
        for (final Factory fac : FeatureDatasetFactoryManager.factoryList) {
            if (!featureTypeOk(null, fac.featureType)) {
                continue;
            }
            if (FeatureDatasetFactoryManager.debug) {
                System.out.println(" wrapUnknown try factory " + fac.factory.getClass().getName());
            }
            analysis = fac.factory.isMine(null, ncd, errlog);
            if (null != analysis) {
                useFactory = fac.factory;
                break;
            }
        }
        if (null == useFactory) {
            final GridDataset gds2 = new GridDataset(ncd);
            if (gds2.getGrids().size() > 0) {
                return gds2;
            }
        }
        if (null == useFactory) {
            return null;
        }
        return useFactory.open(null, ncd, analysis, task, errlog);
    }
    
    private static boolean isGrid(final List<CoordinateSystem> csysList) {
        CoordinateSystem use = null;
        for (final CoordinateSystem csys : csysList) {
            if (use == null) {
                use = csys;
            }
            else {
                if (csys.getCoordinateAxes().size() <= use.getCoordinateAxes().size()) {
                    continue;
                }
                use = csys;
            }
        }
        if (use == null) {
            return false;
        }
        final CoordinateAxis lat = use.getLatAxis();
        final CoordinateAxis lon = use.getLonAxis();
        return (lat == null || lat.getSize() > 1L) && (lon == null || lon.getSize() > 1L) && use.getRankDomain() > 2 && use.getRankDomain() <= use.getRankRange();
    }
    
    public static boolean featureTypeOk(final FeatureType want, final FeatureType facType) {
        if (want == null) {
            return true;
        }
        if (want == facType) {
            return true;
        }
        if (want == FeatureType.ANY_POINT) {
            return facType.isPointFeatureType();
        }
        return facType == FeatureType.ANY_POINT && want.isPointFeatureType();
    }
    
    public static FeatureType findFeatureType(final NetcdfDataset ncd) {
        String cdm_datatype = ncd.findAttValueIgnoreCase(null, "cdm_data_type", null);
        if (cdm_datatype == null) {
            cdm_datatype = ncd.findAttValueIgnoreCase(null, "cdm_datatype", null);
        }
        if (cdm_datatype == null) {
            cdm_datatype = ncd.findAttValueIgnoreCase(null, "thredds_data_type", null);
        }
        if (cdm_datatype != null) {
            for (final FeatureType ft : FeatureType.values()) {
                if (cdm_datatype.equalsIgnoreCase(ft.name())) {
                    if (FeatureDatasetFactoryManager.debug) {
                        System.out.println(" wrapUnknown found cdm_datatype " + cdm_datatype);
                    }
                    return ft;
                }
            }
        }
        String cf_datatype = ncd.findAttValueIgnoreCase(null, "CF:featureType", null);
        if (cf_datatype == null) {
            cf_datatype = ncd.findAttValueIgnoreCase(null, "CFfeatureType", null);
        }
        if (cf_datatype != null) {
            if (FeatureDatasetFactoryManager.debug) {
                System.out.println(" wrapUnknown found cf_datatype " + cdm_datatype);
            }
            return FeatureType.getType(cdm_datatype);
        }
        return null;
    }
    
    public static void main(final String[] args) throws IOException {
        final String server = "http://motherlode:8080/";
        final String dataset = "/thredds/dodsC/fmrc/NCEP/GFS/Global_0p5deg/runs/NCEP-GFS-Global_0p5deg_RUN_2009-05-13T12:00:00Z";
        final Formatter errlog = new Formatter();
        final FeatureDataset fd = open(FeatureType.GRID, server + dataset, null, errlog);
        System.out.printf("%s%n", fd);
    }
    
    static {
        FeatureDatasetFactoryManager.factoryList = new ArrayList<Factory>();
        FeatureDatasetFactoryManager.userMode = false;
        FeatureDatasetFactoryManager.debug = false;
        registerFactory(FeatureType.ANY_POINT, PointDatasetStandardFactory.class);
        registerFactory(FeatureType.GRID, GridDatasetStandardFactory.class);
        registerFactory(FeatureType.RADIAL, RadialDatasetStandardFactory.class);
        FeatureDatasetFactoryManager.userMode = true;
    }
    
    private static class Factory
    {
        FeatureType featureType;
        Class c;
        FeatureDatasetFactory factory;
        
        Factory(final FeatureType featureType, final Class c, final FeatureDatasetFactory factory) {
            this.featureType = featureType;
            this.c = c;
            this.factory = factory;
        }
    }
}
