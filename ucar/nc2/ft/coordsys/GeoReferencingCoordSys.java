// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.coordsys;

import java.util.List;
import ucar.nc2.Dimension;
import ucar.ma2.Array;
import ucar.ma2.Section;
import ucar.ma2.InvalidRangeException;
import java.io.IOException;
import ucar.nc2.dataset.CoordinateAxis;
import ucar.nc2.Variable;
import ucar.nc2.dataset.CoordinateSystem;

public class GeoReferencingCoordSys
{
    private CoordinateSystem cs;
    
    public GeoReferencingCoordSys(final CoordinateSystem cs) {
        this.cs = cs;
    }
    
    public double readLatitudeCoord(final Variable fromVar, final int[] index) throws IOException, InvalidRangeException {
        final CoordinateAxis axis = this.cs.getLatAxis();
        if (null == axis) {
            throw new IllegalArgumentException("There is no latitude coordinate");
        }
        return this.readValue(axis, fromVar, index);
    }
    
    public double readLongitudeCoord(final Variable fromVar, final int[] index) throws IOException, InvalidRangeException {
        final CoordinateAxis axis = this.cs.getLonAxis();
        if (null == axis) {
            throw new IllegalArgumentException("There is no longiude coordinate");
        }
        return this.readValue(axis, fromVar, index);
    }
    
    public double readPressureCoord(final Variable fromVar, final int[] index) throws IOException, InvalidRangeException {
        final CoordinateAxis axis = this.cs.getPressureAxis();
        if (null == axis) {
            throw new IllegalArgumentException("There is no pressure coordinate");
        }
        return this.readValue(axis, fromVar, index);
    }
    
    public double readHeightCoord(final Variable fromVar, final int[] index) throws IOException, InvalidRangeException {
        final CoordinateAxis axis = this.cs.getHeightAxis();
        if (null == axis) {
            throw new IllegalArgumentException("There is no height coordinate");
        }
        return this.readValue(axis, fromVar, index);
    }
    
    public double readTimeCoord(final Variable fromVar, final int[] index) throws IOException, InvalidRangeException {
        final CoordinateAxis axis = this.cs.getTaxis();
        if (null == axis) {
            throw new IllegalArgumentException("There is no time coordinate");
        }
        return this.readValue(axis, fromVar, index);
    }
    
    public double readGeoXCoord(final Variable fromVar, final int[] index) throws IOException, InvalidRangeException {
        final CoordinateAxis axis = this.cs.getXaxis();
        if (null == axis) {
            throw new IllegalArgumentException("There is no GeoX coordinate");
        }
        return this.readValue(axis, fromVar, index);
    }
    
    public double readGeoYCoord(final Variable fromVar, final int[] index) throws IOException, InvalidRangeException {
        final CoordinateAxis axis = this.cs.getYaxis();
        if (null == axis) {
            throw new IllegalArgumentException("There is no GeoY coordinate");
        }
        return this.readValue(axis, fromVar, index);
    }
    
    public double readGeoZCoord(final Variable fromVar, final int[] index) throws IOException, InvalidRangeException {
        final CoordinateAxis axis = this.cs.getZaxis();
        if (null == axis) {
            throw new IllegalArgumentException("There is no GeoZ coordinate");
        }
        return this.readValue(axis, fromVar, index);
    }
    
    public double readValue(final Variable targetVar, final Variable fromVar, final int[] index) throws InvalidRangeException, IOException {
        final Section axisElement = this.mapIndex(targetVar, fromVar, index);
        final Array result = targetVar.read(axisElement);
        return result.nextDouble();
    }
    
    public Section mapIndex(final Variable targetVar, final Variable fromVar, final int[] fromIndex) throws InvalidRangeException {
        final List<Dimension> toDims = targetVar.getDimensions();
        final List<Dimension> fromDims = fromVar.getDimensions();
        final Section result = new Section();
        for (int i = 0; i < toDims.size(); ++i) {
            final Dimension dim = toDims.get(i);
            final int varIndex = fromDims.indexOf(toDims.get(i));
            if (varIndex < 0) {
                throw new IllegalArgumentException("Dimension " + dim + " does not exist");
            }
            result.appendRange(fromIndex[varIndex], fromIndex[varIndex]);
        }
        return result;
    }
}
