// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft;

import java.util.Formatter;
import ucar.nc2.NetcdfFile;
import ucar.nc2.VariableSimpleIF;
import ucar.nc2.Attribute;
import java.util.List;
import java.io.IOException;
import ucar.unidata.geoloc.LatLonRect;
import java.util.Date;
import ucar.nc2.units.DateRange;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.util.cache.FileCacheable;

public interface FeatureDataset extends FileCacheable
{
    FeatureType getFeatureType();
    
    String getTitle();
    
    String getDescription();
    
    String getLocation();
    
    DateRange getDateRange();
    
    Date getStartDate();
    
    Date getEndDate();
    
    LatLonRect getBoundingBox();
    
    void calcBounds() throws IOException;
    
    List<Attribute> getGlobalAttributes();
    
    Attribute findGlobalAttributeIgnoreCase(final String p0);
    
    List<VariableSimpleIF> getDataVariables();
    
    VariableSimpleIF getDataVariable(final String p0);
    
    NetcdfFile getNetcdfFile();
    
    void close() throws IOException;
    
    void getDetailInfo(final Formatter p0);
    
    String getImplementationName();
}
