// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.grid;

import ucar.nc2.ft.FeatureDataset;
import ucar.nc2.util.CancelTask;
import java.io.IOException;
import ucar.nc2.dt.grid.GridDataset;
import java.util.Formatter;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.ft.FeatureDatasetFactory;

public class GridDatasetStandardFactory implements FeatureDatasetFactory
{
    public Object isMine(final FeatureType wantFeatureType, final NetcdfDataset ncd, final Formatter errlog) throws IOException {
        if (wantFeatureType == FeatureType.GRID) {
            final GridDataset gds = new GridDataset(ncd);
            if (gds.getGrids().size() > 0) {
                return gds;
            }
        }
        return null;
    }
    
    public FeatureDataset open(final FeatureType ftype, final NetcdfDataset ncd, final Object analysis, final CancelTask task, final Formatter errlog) throws IOException {
        return (GridDataset)analysis;
    }
    
    public FeatureType[] getFeatureType() {
        return new FeatureType[] { FeatureType.GRID };
    }
}
