// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.grid;

import java.util.Date;
import ucar.nc2.dataset.CoordinateAxis1DTime;
import ucar.nc2.units.DateRange;
import ucar.unidata.geoloc.vertical.VerticalTransform;
import ucar.nc2.dataset.VerticalCT;
import ucar.nc2.dataset.CoordinateAxis1D;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Range;
import ucar.unidata.geoloc.ProjectionImpl;
import ucar.nc2.dataset.ProjectionCT;
import ucar.unidata.geoloc.ProjectionRect;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.nc2.dataset.CoordinateAxis;
import ucar.nc2.Dimension;
import java.util.List;

public interface GridCoordSys
{
    String getName();
    
    List<Dimension> getDomain();
    
    List<CoordinateAxis> getCoordinateAxes();
    
    boolean isProductSet();
    
    List<CoordinateTransform> getCoordinateTransforms();
    
    boolean isRegularSpatial();
    
    CoordinateAxis getXHorizAxis();
    
    CoordinateAxis getYHorizAxis();
    
    boolean isLatLon();
    
    LatLonRect getLatLonBoundingBox();
    
    ProjectionRect getBoundingBox();
    
    ProjectionCT getProjectionCT();
    
    ProjectionImpl getProjection();
    
    List<Range> getRangesFromLatLonRect(final LatLonRect p0) throws InvalidRangeException;
    
    int[] findXYindexFromCoord(final double p0, final double p1, final int[] p2);
    
    int[] findXYindexFromCoordBounded(final double p0, final double p1, final int[] p2);
    
    int[] findXYindexFromLatLon(final double p0, final double p1, final int[] p2);
    
    int[] findXYindexFromLatLonBounded(final double p0, final double p1, final int[] p2);
    
    LatLonPoint getLatLon(final int p0, final int p1);
    
    CoordinateAxis1D getVerticalAxis();
    
    boolean isZPositive();
    
    VerticalCT getVerticalCT();
    
    VerticalTransform getVerticalTransform();
    
    CoordinateAxis1D getEnsembleAxis();
    
    boolean hasTimeAxis();
    
    DateRange getDateRange();
    
    CoordinateAxis getTimeAxis();
    
    CoordinateAxis1DTime getRunTimeAxis();
    
    boolean hasTimeAxis1D();
    
    CoordinateAxis1DTime getTimeAxis1D();
    
    CoordinateAxis1DTime getTimeAxisForRun(final Date p0);
}
