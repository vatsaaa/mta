// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft;

import ucar.nc2.VariableSimpleIF;
import ucar.nc2.units.DateRange;
import ucar.unidata.geoloc.LatLonRect;
import ucar.unidata.geoloc.Station;
import java.util.List;
import java.io.IOException;

public interface StationTimeSeriesFeatureCollection extends StationCollection, NestedPointFeatureCollection
{
    boolean hasNext() throws IOException;
    
    StationTimeSeriesFeature next() throws IOException;
    
    void finish();
    
    void resetIteration() throws IOException;
    
    StationTimeSeriesFeatureCollection subset(final List<Station> p0) throws IOException;
    
    StationTimeSeriesFeatureCollection subset(final LatLonRect p0) throws IOException;
    
    StationTimeSeriesFeature getStationFeature(final Station p0) throws IOException;
    
    Station getStation(final PointFeature p0) throws IOException;
    
    PointFeatureCollection flatten(final List<String> p0, final DateRange p1, final List<VariableSimpleIF> p2) throws IOException;
}
