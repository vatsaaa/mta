// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point;

import ucar.nc2.ft.PointFeatureIterator;
import ucar.nc2.ft.PointFeatureCollection;
import ucar.nc2.units.DateRange;
import ucar.unidata.geoloc.LatLonRect;
import java.io.IOException;
import ucar.nc2.ft.NestedPointFeatureCollectionIterator;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.ft.NestedPointFeatureCollection;

public abstract class OneNestedPointCollectionImpl implements NestedPointFeatureCollection
{
    private String name;
    private FeatureType collectionFeatureType;
    protected int npts;
    
    protected OneNestedPointCollectionImpl(final String name, final FeatureType collectionFeatureType) {
        this.name = name;
        this.collectionFeatureType = collectionFeatureType;
        this.npts = -1;
    }
    
    public String getName() {
        return this.name;
    }
    
    public int size() {
        return this.npts;
    }
    
    protected void setSize(final int npts) {
        this.npts = npts;
    }
    
    public boolean isMultipleNested() {
        return false;
    }
    
    public FeatureType getCollectionFeatureType() {
        return this.collectionFeatureType;
    }
    
    public NestedPointFeatureCollectionIterator getNestedPointFeatureCollectionIterator(final int bufferSize) throws IOException {
        throw new UnsupportedOperationException(this.getClass().getName() + " (single nested) PointFeatureCollection does not implement getNestedPointFeatureCollectionIterator()");
    }
    
    public PointFeatureCollection flatten(final LatLonRect boundingBox, final DateRange dateRange) throws IOException {
        return new NestedPointFeatureCollectionFlatten(this, boundingBox, dateRange);
    }
    
    private class NestedPointFeatureCollectionFlatten extends PointCollectionImpl
    {
        protected OneNestedPointCollectionImpl from;
        protected LatLonRect boundingBox;
        protected DateRange dateRange;
        
        NestedPointFeatureCollectionFlatten(final OneNestedPointCollectionImpl from, final LatLonRect filter_bb, final DateRange filter_date) {
            super(from.getName());
            this.from = from;
            this.boundingBox = filter_bb;
            this.dateRange = filter_date;
        }
        
        public PointFeatureIterator getPointFeatureIterator(final int bufferSize) throws IOException {
            return new PointIteratorFlatten(this.from.getPointFeatureCollectionIterator(bufferSize), this.boundingBox, this.dateRange);
        }
    }
}
