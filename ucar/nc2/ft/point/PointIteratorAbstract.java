// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point;

import ucar.nc2.units.DateRange;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.nc2.ft.PointFeature;
import ucar.nc2.units.DateUnit;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.ft.PointFeatureCollection;
import ucar.nc2.ft.PointFeatureIterator;

public abstract class PointIteratorAbstract implements PointFeatureIterator
{
    protected boolean calcBounds;
    protected PointFeatureCollection collection;
    private LatLonRect bb;
    private double minTime;
    private double maxTime;
    private DateUnit timeUnit;
    private int count;
    
    protected PointIteratorAbstract() {
        this.calcBounds = false;
        this.bb = null;
        this.minTime = Double.MAX_VALUE;
        this.maxTime = -1.7976931348623157E308;
    }
    
    public void setCalculateBounds(final PointFeatureCollection collection) {
        this.calcBounds = true;
        this.collection = collection;
    }
    
    protected void calcBounds(final PointFeature pf) {
        ++this.count;
        if (!this.calcBounds) {
            return;
        }
        if (pf == null) {
            return;
        }
        if (this.bb == null) {
            this.bb = new LatLonRect(pf.getLocation().getLatLon(), 0.001, 0.001);
        }
        else {
            this.bb.extend(pf.getLocation().getLatLon());
        }
        if (this.timeUnit == null) {
            this.timeUnit = pf.getTimeUnit();
        }
        final double obsTime = pf.getObservationTime();
        this.minTime = Math.min(this.minTime, obsTime);
        this.maxTime = Math.max(this.maxTime, obsTime);
    }
    
    protected void finishCalcBounds() {
        if (!this.calcBounds) {
            return;
        }
        if (this.bb != null && this.bb.crossDateline() && this.bb.getWidth() > 350.0) {
            final double lat_min = this.bb.getLowerLeftPoint().getLatitude();
            final double deltaLat = this.bb.getUpperLeftPoint().getLatitude() - lat_min;
            this.bb = new LatLonRect(new LatLonPointImpl(lat_min, -180.0), deltaLat, 360.0);
        }
        if (this.collection != null) {
            if (this.collection.getBoundingBox() == null) {
                this.collection.setBoundingBox(this.bb);
            }
            if (this.collection.getDateRange() == null) {
                final DateRange dr = this.getDateRange();
                if (dr != null) {
                    this.collection.setDateRange(dr);
                }
            }
            if (this.collection.size() <= 0) {
                if (this.count < 0) {
                    this.count = 0;
                }
                this.collection.setSize(this.count);
            }
        }
    }
    
    public LatLonRect getBoundingBox() {
        return this.bb;
    }
    
    public DateRange getDateRange() {
        if (!this.calcBounds) {
            return null;
        }
        if (this.timeUnit == null) {
            return null;
        }
        return new DateRange(this.timeUnit.makeDate(this.minTime), this.timeUnit.makeDate(this.maxTime));
    }
    
    public int getCount() {
        return this.count;
    }
    
    public static class Filter implements PointFeatureIterator.Filter
    {
        private LatLonRect filter_bb;
        private DateRange filter_date;
        
        public Filter(final LatLonRect filter_bb, final DateRange filter_date) {
            this.filter_bb = filter_bb;
            this.filter_date = filter_date;
        }
        
        public boolean filter(final PointFeature pdata) {
            return (this.filter_date == null || this.filter_date.included(pdata.getObservationTimeAsDate())) && (this.filter_bb == null || this.filter_bb.contains(pdata.getLocation().getLatitude(), pdata.getLocation().getLongitude()));
        }
    }
}
