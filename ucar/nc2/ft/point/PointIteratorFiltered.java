// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point;

import java.io.IOException;
import ucar.nc2.ft.PointFeature;
import ucar.nc2.units.DateRange;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.ft.PointFeatureIterator;

public class PointIteratorFiltered extends PointIteratorAbstract
{
    private PointFeatureIterator orgIter;
    private LatLonRect filter_bb;
    private DateRange filter_date;
    private PointFeature pointFeature;
    private boolean finished;
    
    PointIteratorFiltered(final PointFeatureIterator orgIter, final LatLonRect filter_bb, final DateRange filter_date) {
        this.finished = false;
        this.orgIter = orgIter;
        this.filter_bb = filter_bb;
        this.filter_date = filter_date;
    }
    
    public void setBufferSize(final int bytes) {
        this.orgIter.setBufferSize(bytes);
    }
    
    public void finish() {
        if (this.finished) {
            return;
        }
        this.orgIter.finish();
        this.finishCalcBounds();
        this.finished = true;
    }
    
    public boolean hasNext() throws IOException {
        this.pointFeature = this.nextFilteredDataPoint();
        final boolean done = this.pointFeature == null;
        if (done) {
            this.finish();
        }
        return !done;
    }
    
    public PointFeature next() throws IOException {
        if (this.pointFeature == null) {
            return null;
        }
        this.calcBounds(this.pointFeature);
        return this.pointFeature;
    }
    
    private boolean filter(final PointFeature pdata) {
        return (this.filter_date == null || this.filter_date.included(pdata.getObservationTimeAsDate())) && (this.filter_bb == null || this.filter_bb.contains(pdata.getLocation().getLatitude(), pdata.getLocation().getLongitude()));
    }
    
    private PointFeature nextFilteredDataPoint() throws IOException {
        if (this.orgIter == null) {
            return null;
        }
        if (!this.orgIter.hasNext()) {
            return null;
        }
        PointFeature pdata;
        for (pdata = this.orgIter.next(); !this.filter(pdata); pdata = this.orgIter.next()) {
            if (!this.orgIter.hasNext()) {
                return null;
            }
        }
        return pdata;
    }
}
