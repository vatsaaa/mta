// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point;

import ucar.nc2.ft.NestedPointFeatureCollection;
import ucar.nc2.ft.NestedPointFeatureCollectionIterator;
import ucar.nc2.VariableSimpleIF;
import ucar.nc2.units.DateRange;
import java.util.List;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.ft.PointFeature;
import ucar.nc2.ft.StationTimeSeriesFeature;
import ucar.nc2.ft.PointFeatureCollection;
import java.io.IOException;
import ucar.unidata.geoloc.Station;
import java.util.Iterator;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.ft.PointFeatureCollectionIterator;
import ucar.nc2.ft.StationTimeSeriesFeatureCollection;

public abstract class StationTimeSeriesCollectionImpl extends OneNestedPointCollectionImpl implements StationTimeSeriesFeatureCollection
{
    protected StationHelper stationHelper;
    protected PointFeatureCollectionIterator localIterator;
    
    public StationTimeSeriesCollectionImpl(final String name) {
        super(name, FeatureType.STATION);
    }
    
    protected abstract void initStationHelper();
    
    public PointFeatureCollectionIterator getPointFeatureCollectionIterator(final int bufferSize) throws IOException {
        this.initStationHelper();
        return new PointFeatureCollectionIterator() {
            Iterator<Station> stationIter = StationTimeSeriesCollectionImpl.this.stationHelper.getStations().iterator();
            
            public boolean hasNext() throws IOException {
                return this.stationIter.hasNext();
            }
            
            public PointFeatureCollection next() throws IOException {
                return (PointFeatureCollection)this.stationIter.next();
            }
            
            public void setBufferSize(final int bytes) {
            }
            
            public void finish() {
            }
        };
    }
    
    public StationTimeSeriesFeature getStationFeature(final Station s) throws IOException {
        return (StationTimeSeriesFeature)s;
    }
    
    public Station getStation(final PointFeature feature) throws IOException {
        final StationPointFeature stationFeature = (StationPointFeature)feature;
        return stationFeature.getStation();
    }
    
    public StationTimeSeriesFeatureCollection subset(final LatLonRect boundingBox) throws IOException {
        return this.subset(this.getStations(boundingBox));
    }
    
    public StationTimeSeriesFeatureCollection subset(final List<Station> stations) throws IOException {
        if (stations == null) {
            return this;
        }
        return new StationTimeSeriesCollectionSubset(this, stations);
    }
    
    public PointFeatureCollection flatten(final List<String> stations, final DateRange dateRange, final List<VariableSimpleIF> varList) throws IOException {
        if (stations == null || stations.size() == 0) {
            return new StationTimeSeriesCollectionFlattened(this, dateRange);
        }
        this.initStationHelper();
        final List<Station> subsetStations = this.stationHelper.getStations(stations);
        return new StationTimeSeriesCollectionFlattened(new StationTimeSeriesCollectionSubset(this, subsetStations), dateRange);
    }
    
    @Override
    public PointFeatureCollection flatten(final LatLonRect boundingBox, final DateRange dateRange) throws IOException {
        if (boundingBox == null) {
            return new StationTimeSeriesCollectionFlattened(this, dateRange);
        }
        this.initStationHelper();
        final List<Station> subsetStations = this.stationHelper.getStations(boundingBox);
        return new StationTimeSeriesCollectionFlattened(new StationTimeSeriesCollectionSubset(this, subsetStations), dateRange);
    }
    
    public List<Station> getStations() {
        if (this.stationHelper == null) {
            this.initStationHelper();
        }
        return this.stationHelper.getStations();
    }
    
    public List<Station> getStations(final List<String> stnNames) {
        if (this.stationHelper == null) {
            this.initStationHelper();
        }
        return this.stationHelper.getStations(stnNames);
    }
    
    public List<Station> getStations(final LatLonRect boundingBox) throws IOException {
        if (this.stationHelper == null) {
            this.initStationHelper();
        }
        return this.stationHelper.getStations(boundingBox);
    }
    
    public Station getStation(final String name) {
        if (this.stationHelper == null) {
            this.initStationHelper();
        }
        return this.stationHelper.getStation(name);
    }
    
    public LatLonRect getBoundingBox() {
        if (this.stationHelper == null) {
            this.initStationHelper();
        }
        return this.stationHelper.getBoundingBox();
    }
    
    @Override
    public NestedPointFeatureCollectionIterator getNestedPointFeatureCollectionIterator(final int bufferSize) throws IOException {
        throw new UnsupportedOperationException("StationFeatureCollection does not implement getNestedPointFeatureCollection()");
    }
    
    public boolean hasNext() throws IOException {
        if (this.localIterator == null) {
            this.resetIteration();
        }
        return this.localIterator.hasNext();
    }
    
    public void finish() {
        if (this.localIterator != null) {
            this.localIterator.finish();
        }
    }
    
    public StationTimeSeriesFeature next() throws IOException {
        return (StationTimeSeriesFeature)this.localIterator.next();
    }
    
    public void resetIteration() throws IOException {
        this.localIterator = this.getPointFeatureCollectionIterator(-1);
    }
    
    private class StationTimeSeriesCollectionSubset extends StationTimeSeriesCollectionImpl
    {
        StationTimeSeriesCollectionImpl from;
        
        StationTimeSeriesCollectionSubset(final StationTimeSeriesCollectionImpl from, final List<Station> stations) {
            super(from.getName());
            this.from = from;
            (this.stationHelper = new StationHelper()).setStations(stations);
        }
        
        @Override
        protected void initStationHelper() {
        }
    }
}
