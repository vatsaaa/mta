// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point;

import org.slf4j.LoggerFactory;
import ucar.ma2.Array;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Range;
import ucar.ma2.Section;
import ucar.ma2.StructureDataW;
import java.util.Iterator;
import java.io.IOException;
import ucar.ma2.StructureData;
import ucar.nc2.ft.PointFeature;
import ucar.ma2.StructureMembers;
import ucar.nc2.Variable;
import java.util.List;
import org.slf4j.Logger;
import ucar.nc2.ft.PointFeatureIterator;

public abstract class PointIteratorMultidim implements PointFeatureIterator
{
    private static Logger log;
    private List<Variable> vars;
    private StructureMembers members;
    private int outerIndex;
    private Filter filter;
    private int count;
    private int npts;
    private PointFeature feature;
    
    protected abstract PointFeature makeFeature(final int p0, final StructureData p1) throws IOException;
    
    public PointIteratorMultidim(final String name, final List<Variable> vars, final int outerIndex, final Filter filter) {
        this.vars = vars;
        this.outerIndex = outerIndex;
        this.filter = filter;
        final Variable v = vars.get(0);
        this.npts = v.getDimension(1).getLength();
        this.members = new StructureMembers(name);
        for (final Variable var : vars) {
            final int[] shape = var.getShape();
            final int[] newShape = new int[shape.length - 2];
            System.arraycopy(shape, 2, newShape, 0, shape.length - 2);
            this.members.addMember(var.getShortName(), var.getDescription(), var.getUnitsString(), var.getDataType(), newShape);
        }
    }
    
    public boolean hasNext() throws IOException {
        while (this.count < this.npts) {
            final StructureData sdata = this.nextStructureData();
            this.feature = this.makeFeature(this.count, sdata);
            ++this.count;
            if (this.filter != null && !this.filter.filter(this.feature)) {
                continue;
            }
            return true;
        }
        this.feature = null;
        return false;
    }
    
    public PointFeature next() throws IOException {
        return this.feature;
    }
    
    private StructureData nextStructureData() throws IOException {
        final StructureDataW sdata = new StructureDataW(this.members);
        for (final Variable var : this.vars) {
            final Section s = new Section();
            try {
                s.appendRange(this.outerIndex, this.outerIndex);
                s.appendRange(this.count, this.count);
                for (int i = 2; i < var.getRank(); ++i) {
                    s.appendRange(null);
                }
                final Array data = var.read(s);
                sdata.setMemberData(var.getShortName(), data);
            }
            catch (InvalidRangeException e) {
                e.printStackTrace();
            }
        }
        return sdata;
    }
    
    public void setBufferSize(final int bytes) {
    }
    
    static {
        PointIteratorMultidim.log = LoggerFactory.getLogger(PointIteratorMultidim.class);
    }
}
