// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point;

import ucar.nc2.ft.PointFeatureIterator;
import java.io.IOException;
import ucar.nc2.units.DateRange;
import ucar.nc2.constants.FeatureType;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonRect;
import ucar.unidata.geoloc.StationImpl;
import ucar.nc2.units.DateUnit;
import ucar.unidata.geoloc.Station;
import ucar.nc2.ft.StationTimeSeriesFeature;

public abstract class StationFeatureImpl extends PointCollectionImpl implements StationTimeSeriesFeature
{
    protected Station s;
    protected DateUnit timeUnit;
    
    public StationFeatureImpl(final String name, final String desc, final String wmoId, final double lat, final double lon, final double alt, final DateUnit timeUnit, final int npts) {
        super(name);
        this.s = new StationImpl(name, desc, wmoId, lat, lon, alt);
        this.timeUnit = timeUnit;
        this.npts = npts;
    }
    
    public StationFeatureImpl(final Station s, final DateUnit timeUnit, final int npts) {
        super(s.getName());
        this.s = s;
        this.timeUnit = timeUnit;
        this.npts = npts;
        this.setBoundingBox(new LatLonRect(s.getLatLon(), 1.0E-4, 1.0E-4));
    }
    
    public String getWmoId() {
        return this.s.getWmoId();
    }
    
    @Override
    public int size() {
        return this.npts;
    }
    
    public void setNumberPoints(final int npts) {
        this.npts = npts;
    }
    
    @Override
    public String getName() {
        return this.s.getName();
    }
    
    public String getDescription() {
        return this.s.getDescription();
    }
    
    public double getLatitude() {
        return this.s.getLatitude();
    }
    
    public double getLongitude() {
        return this.s.getLongitude();
    }
    
    public double getAltitude() {
        return this.s.getAltitude();
    }
    
    public LatLonPoint getLatLon() {
        return this.s.getLatLon();
    }
    
    public boolean isMissing() {
        return Double.isNaN(this.getLatitude()) || Double.isNaN(this.getLongitude());
    }
    
    @Override
    public FeatureType getCollectionFeatureType() {
        return FeatureType.STATION;
    }
    
    @Override
    public String toString() {
        return "StationFeatureImpl{s=" + this.s + ", timeUnit=" + this.timeUnit + ", npts=" + this.npts + '}';
    }
    
    public StationTimeSeriesFeature subset(final DateRange dateRange) throws IOException {
        if (dateRange == null) {
            return this;
        }
        return new StationFeatureSubset(this, dateRange);
    }
    
    public int compareTo(final Station so) {
        return this.name.compareTo(so.getName());
    }
    
    private class StationFeatureSubset extends StationFeatureImpl
    {
        StationFeatureImpl from;
        
        StationFeatureSubset(final StationFeatureImpl from, final DateRange filter_date) {
            super(from.s, from.timeUnit, -1);
            this.from = from;
            if (filter_date == null) {
                this.dateRange = from.dateRange;
            }
            else {
                this.dateRange = ((from.dateRange == null) ? filter_date : from.dateRange.intersect(filter_date));
            }
        }
        
        public PointFeatureIterator getPointFeatureIterator(final int bufferSize) throws IOException {
            return new PointIteratorFiltered(this.from.getPointFeatureIterator(bufferSize), null, this.dateRange);
        }
    }
}
