// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point;

import ucar.unidata.geoloc.Station;
import ucar.nc2.ft.PointFeature;

public interface StationPointFeature extends PointFeature
{
    Station getStation();
}
