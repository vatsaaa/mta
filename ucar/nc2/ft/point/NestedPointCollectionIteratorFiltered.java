// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point;

import java.io.IOException;
import ucar.nc2.ft.NestedPointFeatureCollection;
import ucar.nc2.ft.NestedPointFeatureCollectionIterator;

public class NestedPointCollectionIteratorFiltered implements NestedPointFeatureCollectionIterator
{
    private NestedPointFeatureCollectionIterator npfciter;
    private Filter filter;
    private NestedPointFeatureCollection pointFeatureCollection;
    private boolean done;
    
    NestedPointCollectionIteratorFiltered(final NestedPointFeatureCollectionIterator npfciter, final Filter filter) {
        this.done = false;
        this.npfciter = npfciter;
        this.filter = filter;
    }
    
    public void setBufferSize(final int bytes) {
        this.npfciter.setBufferSize(bytes);
    }
    
    public boolean hasNext() throws IOException {
        if (this.done) {
            return false;
        }
        this.pointFeatureCollection = this.nextFilteredPointFeatureCollection();
        return this.pointFeatureCollection != null;
    }
    
    public NestedPointFeatureCollection next() throws IOException {
        return this.done ? null : this.pointFeatureCollection;
    }
    
    private boolean filter(final NestedPointFeatureCollection pdata) {
        return this.filter == null || this.filter.filter(pdata);
    }
    
    private NestedPointFeatureCollection nextFilteredPointFeatureCollection() throws IOException {
        if (this.npfciter == null) {
            return null;
        }
        if (!this.npfciter.hasNext()) {
            return null;
        }
        NestedPointFeatureCollection pdata = this.npfciter.next();
        if (!this.filter(pdata)) {
            if (!this.npfciter.hasNext()) {
                return null;
            }
            pdata = this.npfciter.next();
        }
        return pdata;
    }
}
