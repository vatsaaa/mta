// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point;

import java.io.IOException;
import ucar.ma2.StructureData;
import ucar.nc2.ft.PointFeature;
import ucar.ma2.StructureDataIterator;

public abstract class PointIteratorFromStructureData extends PointIteratorAbstract
{
    private Filter filter;
    private StructureDataIterator structIter;
    private PointFeature feature;
    private boolean finished;
    
    protected abstract PointFeature makeFeature(final int p0, final StructureData p1) throws IOException;
    
    public PointIteratorFromStructureData(final StructureDataIterator structIter, final Filter filter) throws IOException {
        this.feature = null;
        this.finished = false;
        this.structIter = structIter;
        this.filter = filter;
    }
    
    public boolean hasNext() throws IOException {
        while (true) {
            final StructureData sdata = this.nextStructureData();
            if (sdata == null) {
                this.feature = null;
                this.finish();
                return false;
            }
            this.feature = this.makeFeature(this.structIter.getCurrentRecno(), sdata);
            if (this.feature == null) {
                continue;
            }
            if (this.feature.getLocation().isMissing()) {
                continue;
            }
            if (this.filter == null || this.filter.filter(this.feature)) {
                return true;
            }
        }
    }
    
    public PointFeature next() throws IOException {
        if (this.feature == null) {
            return null;
        }
        this.calcBounds(this.feature);
        return this.feature;
    }
    
    public void setBufferSize(final int bytes) {
        this.structIter.setBufferSize(bytes);
    }
    
    public void finish() {
        if (this.finished) {
            return;
        }
        this.finishCalcBounds();
        this.finished = true;
    }
    
    private StructureData nextStructureData() throws IOException {
        return this.structIter.hasNext() ? this.structIter.next() : null;
    }
}
