// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point;

import ucar.nc2.constants.FeatureType;
import ucar.nc2.ft.PointFeature;
import java.io.IOException;
import ucar.nc2.ft.PointFeatureIterator;
import ucar.nc2.units.DateRange;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.ft.PointFeatureCollection;

public abstract class PointCollectionImpl implements PointFeatureCollection
{
    protected String name;
    protected LatLonRect boundingBox;
    protected DateRange dateRange;
    protected int npts;
    protected PointFeatureIterator localIterator;
    
    protected PointCollectionImpl(final String name) {
        this.name = name;
        this.npts = -1;
    }
    
    protected PointCollectionImpl(final String name, final LatLonRect boundingBox, final DateRange dateRange, final int npts) {
        this.name = name;
        this.boundingBox = boundingBox;
        this.dateRange = dateRange;
        this.npts = npts;
    }
    
    public String getName() {
        return this.name;
    }
    
    public boolean hasNext() throws IOException {
        if (this.localIterator == null) {
            this.resetIteration();
        }
        return this.localIterator.hasNext();
    }
    
    public void finish() {
        if (this.localIterator != null) {
            this.localIterator.finish();
        }
    }
    
    public PointFeature next() throws IOException {
        return this.localIterator.next();
    }
    
    public void resetIteration() throws IOException {
        this.localIterator = this.getPointFeatureIterator(-1);
    }
    
    public int size() {
        return this.npts;
    }
    
    public DateRange getDateRange() {
        return this.dateRange;
    }
    
    public LatLonRect getBoundingBox() {
        return this.boundingBox;
    }
    
    public void setDateRange(final DateRange range) {
        this.dateRange = range;
    }
    
    public void setBoundingBox(final LatLonRect bb) {
        this.boundingBox = bb;
    }
    
    public void calcBounds() throws IOException {
        if (this.dateRange != null && this.boundingBox != null && this.size() > 0) {
            return;
        }
        final PointFeatureIterator iter = this.getPointFeatureIterator(-1);
        iter.setCalculateBounds(this);
        try {
            while (iter.hasNext()) {
                iter.next();
            }
        }
        finally {
            iter.finish();
        }
    }
    
    public void setSize(final int npts) {
        this.npts = npts;
    }
    
    public FeatureType getCollectionFeatureType() {
        return FeatureType.POINT;
    }
    
    public PointFeatureCollection subset(final LatLonRect boundingBox, final DateRange dateRange) throws IOException {
        return new PointCollectionSubset(this, boundingBox, dateRange);
    }
    
    protected class PointCollectionSubset extends PointCollectionImpl
    {
        protected PointCollectionImpl from;
        
        public PointCollectionSubset(final PointCollectionImpl from, final LatLonRect filter_bb, final DateRange filter_date) {
            super(from.name);
            this.from = from;
            if (filter_bb == null) {
                this.boundingBox = from.boundingBox;
            }
            else {
                this.boundingBox = ((from.boundingBox == null) ? filter_bb : from.boundingBox.intersect(filter_bb));
            }
            if (filter_date == null) {
                this.dateRange = from.dateRange;
            }
            else {
                this.dateRange = ((from.dateRange == null) ? filter_date : from.dateRange.intersect(filter_date));
            }
        }
        
        public PointFeatureIterator getPointFeatureIterator(final int bufferSize) throws IOException {
            return new PointIteratorFiltered(this.from.getPointFeatureIterator(bufferSize), this.boundingBox, this.dateRange);
        }
    }
}
