// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard;

import ucar.ma2.StructureData;

public abstract class CoordVarExtractor
{
    protected String axisName;
    protected int nestingLevel;
    
    protected CoordVarExtractor(final String axisName, final int nestingLevel) {
        this.axisName = axisName;
        this.nestingLevel = nestingLevel;
    }
    
    public abstract double getCoordValue(final StructureData p0);
    
    public abstract String getCoordValueString(final StructureData p0);
    
    public abstract String getUnitsString();
    
    public abstract boolean isString();
    
    public double getCoordValue(final StructureData[] tableData) {
        return this.getCoordValue(tableData[this.nestingLevel]);
    }
    
    public String getCoordValueString(final StructureData[] tableData) {
        return this.getCoordValueString(tableData[this.nestingLevel]);
    }
    
    public String getCoordValueAsString(final StructureData sdata) {
        if (this.isString()) {
            return this.getCoordValueString(sdata);
        }
        final double dval = this.getCoordValue(sdata);
        return Double.toString(dval);
    }
    
    protected abstract boolean isMissing(final StructureData p0);
    
    public boolean isMissing(final StructureData[] tableData) {
        return this.isMissing(tableData[this.nestingLevel]);
    }
    
    @Override
    public String toString() {
        return this.axisName + " nestingLevel= " + this.nestingLevel;
    }
}
