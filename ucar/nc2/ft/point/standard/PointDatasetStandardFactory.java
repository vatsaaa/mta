// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard;

import java.util.List;
import java.util.Collection;
import ucar.nc2.VariableSimpleIF;
import ucar.nc2.ft.FeatureCollection;
import java.util.ArrayList;
import ucar.nc2.units.DateUnit;
import ucar.nc2.ft.point.PointDatasetImpl;
import org.slf4j.LoggerFactory;
import ucar.nc2.ft.FeatureDataset;
import ucar.nc2.util.CancelTask;
import java.io.IOException;
import java.util.Iterator;
import ucar.nc2.constants.AxisType;
import ucar.nc2.dataset.CoordinateAxis;
import java.util.Formatter;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.util.DebugFlags;
import org.slf4j.Logger;
import ucar.nc2.ft.FeatureDatasetFactory;

public class PointDatasetStandardFactory implements FeatureDatasetFactory
{
    private static Logger log;
    static boolean showTables;
    
    public static void setDebugFlags(final DebugFlags debugFlags) {
        PointDatasetStandardFactory.showTables = debugFlags.isSet("PointDatasetStandardFactory/showTables");
    }
    
    public Object isMine(FeatureType wantFeatureType, final NetcdfDataset ds, final Formatter errlog) throws IOException {
        if (wantFeatureType == null) {
            wantFeatureType = FeatureType.ANY_POINT;
        }
        if (wantFeatureType != FeatureType.ANY_POINT && !wantFeatureType.isPointFeatureType()) {
            return null;
        }
        final TableConfigurer tc = TableAnalyzer.getTableConfigurer(wantFeatureType, ds);
        if (tc == null) {
            boolean hasTime = false;
            boolean hasLat = false;
            boolean hasLon = false;
            for (final CoordinateAxis axis : ds.getCoordinateAxes()) {
                if (axis.getAxisType() == AxisType.Time) {
                    hasTime = true;
                }
                if (axis.getAxisType() == AxisType.Lat) {
                    hasLat = true;
                }
                if (axis.getAxisType() == AxisType.Lon) {
                    hasLon = true;
                }
            }
            if (!hasTime || !hasLon || !hasLat) {
                errlog.format("PointDataset must have lat,lon,time", new Object[0]);
                return null;
            }
        }
        else if (PointDatasetStandardFactory.showTables) {
            System.out.printf("TableConfigurer = %s%n", tc.getClass().getName());
        }
        final TableAnalyzer analyser = TableAnalyzer.factory(tc, wantFeatureType, ds);
        if (analyser == null) {
            return null;
        }
        errlog.format("%s%n", analyser.getErrlog());
        if (!analyser.featureTypeOk(wantFeatureType, errlog)) {
            return null;
        }
        return analyser;
    }
    
    public FeatureDataset open(final FeatureType wantFeatureType, final NetcdfDataset ncd, Object analyser, final CancelTask task, final Formatter errlog) throws IOException {
        if (analyser == null) {
            analyser = TableAnalyzer.factory(null, wantFeatureType, ncd);
        }
        return new PointDatasetStandard(wantFeatureType, (TableAnalyzer)analyser, ncd, errlog);
    }
    
    public FeatureType[] getFeatureType() {
        return new FeatureType[] { FeatureType.ANY_POINT };
    }
    
    static void doit(final PointDatasetStandardFactory fac, final String filename) throws IOException {
        System.out.println(filename);
        final Formatter errlog = new Formatter(System.out);
        final NetcdfDataset ncd = NetcdfDataset.openDataset(filename);
        final TableAnalyzer analysis = (TableAnalyzer)fac.isMine(FeatureType.ANY_POINT, ncd, errlog);
        fac.open(FeatureType.ANY_POINT, ncd, analysis, null, errlog);
        analysis.getDetailInfo(errlog);
        System.out.printf("\n-----------------", new Object[0]);
        ncd.close();
    }
    
    public static void main(final String[] args) throws IOException {
        final PointDatasetStandardFactory fac = new PointDatasetStandardFactory();
        doit(fac, "Q:/cdmUnitTest/formats/gempak/surface/20090521_sao.gem");
    }
    
    static {
        PointDatasetStandardFactory.log = LoggerFactory.getLogger(PointDatasetStandardFactory.class);
        PointDatasetStandardFactory.showTables = false;
    }
    
    static class PointDatasetStandard extends PointDatasetImpl
    {
        private TableAnalyzer analyser;
        private DateUnit timeUnit;
        
        PointDatasetStandard(final FeatureType wantFeatureType, final TableAnalyzer analyser, final NetcdfDataset ds, final Formatter errlog) throws IOException {
            super(ds, null);
            this.parseInfo.format(" PointFeatureDatasetImpl=%s\n", this.getClass().getName());
            this.analyser = analyser;
            final List<FeatureCollection> featureCollections = new ArrayList<FeatureCollection>();
            for (final NestedTable flatTable : analyser.getFlatTables()) {
                if (this.timeUnit == null) {
                    try {
                        this.timeUnit = flatTable.getTimeUnit();
                    }
                    catch (Exception e) {
                        if (null != errlog) {
                            errlog.format("%s\n", e.getMessage());
                        }
                        try {
                            this.timeUnit = new DateUnit("seconds since 1970-01-01");
                        }
                        catch (Exception e2) {
                            PointDatasetStandardFactory.log.error("Illegal time units", e2);
                        }
                    }
                }
                this.dataVariables = new ArrayList<VariableSimpleIF>(flatTable.getDataVariables());
                this.featureType = flatTable.getFeatureType();
                if (flatTable.getFeatureType() == FeatureType.POINT) {
                    featureCollections.add(new StandardPointCollectionImpl(flatTable, this.timeUnit));
                }
                else if (flatTable.getFeatureType() == FeatureType.PROFILE) {
                    featureCollections.add(new StandardProfileCollectionImpl(flatTable, this.timeUnit));
                }
                else if (flatTable.getFeatureType() == FeatureType.STATION) {
                    featureCollections.add(new StandardStationCollectionImpl(flatTable, this.timeUnit));
                }
                else if (flatTable.getFeatureType() == FeatureType.STATION_PROFILE) {
                    featureCollections.add(new StandardStationProfileCollectionImpl(flatTable, this.timeUnit));
                }
                else if (flatTable.getFeatureType() == FeatureType.SECTION) {
                    featureCollections.add(new StandardSectionCollectionImpl(flatTable, this.timeUnit));
                }
                else {
                    if (flatTable.getFeatureType() != FeatureType.TRAJECTORY) {
                        continue;
                    }
                    featureCollections.add(new StandardTrajectoryCollectionImpl(flatTable, this.timeUnit));
                }
            }
            if (featureCollections.size() == 0) {
                throw new IllegalStateException("No feature collections found");
            }
            this.setPointFeatureCollection(featureCollections);
        }
        
        @Override
        public void getDetailInfo(final Formatter sf) {
            super.getDetailInfo(sf);
            this.analyser.getDetailInfo(sf);
        }
        
        @Override
        public FeatureType getFeatureType() {
            return this.featureType;
        }
        
        @Override
        public String getImplementationName() {
            if (this.analyser != null) {
                return this.analyser.getImplementationName();
            }
            return super.getImplementationName();
        }
        
        TableAnalyzer getTableAnalyzer() {
            return this.analyser;
        }
    }
}
