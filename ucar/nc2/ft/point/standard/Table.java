// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard;

import ucar.ma2.ArraySequence;
import ucar.ma2.Range;
import ucar.ma2.ArrayStructureMA;
import ucar.ma2.InvalidRangeException;
import java.util.Collection;
import ucar.ma2.ArrayStructureW;
import ucar.ma2.ArrayChar;
import ucar.nc2.ft.point.StructureDataIteratorIndexed;
import java.util.HashMap;
import java.util.Map;
import ucar.ma2.StructureData;
import ucar.nc2.Structure;
import ucar.nc2.ft.point.StructureDataIteratorLinked;
import ucar.ma2.Array;
import ucar.ma2.Section;
import ucar.nc2.VariableSimpleAdapter;
import ucar.ma2.StructureMembers;
import ucar.ma2.ArrayStructure;
import ucar.ma2.DataType;
import ucar.nc2.Variable;
import ucar.nc2.dataset.StructurePseudo2Dim;
import ucar.nc2.dataset.StructurePseudoDS;
import ucar.nc2.Dimension;
import ucar.nc2.dataset.StructureDS;
import org.slf4j.LoggerFactory;
import java.util.Iterator;
import java.util.Formatter;
import ucar.nc2.dataset.VariableDS;
import java.io.IOException;
import ucar.ma2.StructureDataIterator;
import java.util.ArrayList;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.VariableSimpleIF;
import java.util.List;
import ucar.nc2.constants.FeatureType;
import org.slf4j.Logger;

public abstract class Table
{
    private static Logger log;
    String name;
    FeatureType featureType;
    Table parent;
    Table child;
    List<Join> extraJoins;
    String lat;
    String lon;
    String elev;
    String time;
    String timeNominal;
    String stnId;
    String stnDesc;
    String stnNpts;
    String stnWmoId;
    String stnAlt;
    String limit;
    String feature_id;
    String missingVar;
    List<VariableSimpleIF> cols;
    List<String> nondataVars;
    
    public static Table factory(final NetcdfDataset ds, final TableConfig config) {
        switch (config.type) {
            case ArrayStructure: {
                return new TableArrayStructure(ds, config);
            }
            case Construct: {
                return new TableConstruct(ds, config);
            }
            case Contiguous: {
                return new TableContiguous(ds, config);
            }
            case LinkedList: {
                return new TableLinkedList(ds, config);
            }
            case MultidimInner: {
                return new TableMultidimInner(ds, config);
            }
            case MultidimInner3D: {
                return new TableMultidimInner3D(ds, config);
            }
            case MultidimStructure: {
                return new TableMultidimStructure(ds, config);
            }
            case MultidimInnerPsuedo: {
                return new TableMultidimInnerPsuedo(ds, config);
            }
            case MultidimInnerPsuedo3D: {
                return new TableMultidimInnerPsuedo3D(ds, config);
            }
            case NestedStructure: {
                return new TableNestedStructure(ds, config);
            }
            case ParentId: {
                return new TableParentId(ds, config);
            }
            case ParentIndex: {
                return new TableParentIndex(ds, config);
            }
            case Singleton: {
                return new TableSingleton(ds, config);
            }
            case Structure: {
                return new TableStructure(ds, config);
            }
            case Top: {
                return new TableTop(ds, config);
            }
            default: {
                throw new IllegalStateException("Unimplemented Table type = " + config.type);
            }
        }
    }
    
    protected Table(final NetcdfDataset ds, final TableConfig config) {
        this.cols = new ArrayList<VariableSimpleIF>();
        this.nondataVars = new ArrayList<String>();
        this.name = config.name;
        this.featureType = config.featureType;
        this.lat = config.lat;
        this.lon = config.lon;
        this.elev = config.elev;
        this.time = config.time;
        this.timeNominal = config.timeNominal;
        this.stnId = config.stnId;
        this.stnDesc = config.stnDesc;
        this.stnNpts = config.stnNpts;
        this.stnWmoId = config.stnWmoId;
        this.stnAlt = config.stnAlt;
        this.limit = config.limit;
        this.feature_id = config.feature_id;
        this.missingVar = config.missingVar;
        if (config.parent != null) {
            this.parent = factory(ds, config.parent);
            this.parent.child = this;
        }
        this.extraJoins = config.extraJoin;
        this.checkNonDataVariable(config.stnNpts);
        this.checkNonDataVariable(config.limit);
    }
    
    protected void checkNonDataVariable(final String name) {
        if (name != null) {
            this.nondataVars.add(name);
        }
    }
    
    public abstract StructureDataIterator getStructureDataIterator(final Cursor p0, final int p1) throws IOException;
    
    String findCoordinateVariableName(final CoordName coordName) {
        switch (coordName) {
            case Elev: {
                return this.elev;
            }
            case Lat: {
                return this.lat;
            }
            case Lon: {
                return this.lon;
            }
            case Time: {
                return this.time;
            }
            case TimeNominal: {
                return this.timeNominal;
            }
            case StnId: {
                return this.stnId;
            }
            case StnDesc: {
                return this.stnDesc;
            }
            case WmoId: {
                return this.stnWmoId;
            }
            case StnAlt: {
                return this.stnAlt;
            }
            case FeatureId: {
                return this.feature_id;
            }
            case MissingVar: {
                return this.missingVar;
            }
            default: {
                return null;
            }
        }
    }
    
    public String getName() {
        return this.name;
    }
    
    public FeatureType getFeatureType() {
        return this.featureType;
    }
    
    public VariableDS findVariable(final String axisName) {
        return null;
    }
    
    public String showDimension() {
        return "";
    }
    
    @Override
    public String toString() {
        final Formatter formatter = new Formatter();
        formatter.format(" Table %s on dimension %s type=%s\n", this.getName(), this.showDimension(), this.getClass().toString());
        formatter.format("  Coordinates=", new Object[0]);
        formatter.format("\n  Data Variables= %d\n", this.cols.size());
        formatter.format("  Parent= %s\n", (this.parent == null) ? "none" : this.parent.getName());
        return formatter.toString();
    }
    
    public String showAll() {
        final StringBuilder sbuff = new StringBuilder();
        sbuff.append("Table on dimension ").append(this.showDimension()).append("\n");
        for (final VariableSimpleIF v : this.cols) {
            sbuff.append("  ").append(v.getName()).append("\n");
        }
        return sbuff.toString();
    }
    
    public int show(final Formatter f, int indent) {
        if (this.parent != null) {
            indent = this.parent.show(f, indent);
        }
        final String s = this.indent(indent);
        final String ftDesc = (this.featureType == null) ? "" : ("featureType=" + this.featureType.toString());
        f.format("%n%sTable %s: type=%s %s%n", s, this.getName(), this.getClass().toString(), ftDesc);
        if (this.extraJoins != null) {
            f.format("  %sExtraJoins:\n", s);
            for (final Join j : this.extraJoins) {
                f.format("   %s  %s \n", s, j);
            }
        }
        this.showTableExtraInfo(this.indent(indent + 2), f);
        this.showCoords(s, f);
        f.format("  %sVariables:\n", s);
        for (final VariableSimpleIF v : this.cols) {
            f.format("   %s  %s %s\n", s, v.getName(), this.getKind(v.getShortName()));
        }
        return indent + 2;
    }
    
    String indent(final int n) {
        final StringBuilder sbuff = new StringBuilder();
        for (int i = 0; i < n; ++i) {
            sbuff.append(' ');
        }
        return sbuff.toString();
    }
    
    protected abstract void showTableExtraInfo(final String p0, final Formatter p1);
    
    String getKind(final String v) {
        if (v.equals(this.lat)) {
            return "[Lat]";
        }
        if (v.equals(this.lon)) {
            return "[Lon]";
        }
        if (v.equals(this.elev)) {
            return "[Elev]";
        }
        if (v.equals(this.time)) {
            return "[Time]";
        }
        if (v.equals(this.timeNominal)) {
            return "[timeNominal]";
        }
        if (v.equals(this.stnId)) {
            return "[stnId]";
        }
        if (v.equals(this.stnDesc)) {
            return "[stnDesc]";
        }
        if (v.equals(this.stnNpts)) {
            return "[stnNpts]";
        }
        if (v.equals(this.stnWmoId)) {
            return "[stnWmoId]";
        }
        if (v.equals(this.stnAlt)) {
            return "[stnAlt]";
        }
        if (v.equals(this.limit)) {
            return "[limit]";
        }
        return "";
    }
    
    private void showCoords(final String indent, final Formatter out) {
        boolean gotSome = this.showCoord(out, this.lat, indent);
        gotSome |= this.showCoord(out, this.lon, indent);
        gotSome |= this.showCoord(out, this.elev, indent);
        gotSome |= this.showCoord(out, this.time, indent);
        gotSome |= this.showCoord(out, this.timeNominal, indent);
        gotSome |= this.showCoord(out, this.stnId, indent);
        gotSome |= this.showCoord(out, this.stnDesc, indent);
        gotSome |= this.showCoord(out, this.stnNpts, indent);
        gotSome |= this.showCoord(out, this.stnWmoId, indent);
        gotSome |= this.showCoord(out, this.stnAlt, indent);
        gotSome |= this.showCoord(out, this.limit, indent);
        if (gotSome) {
            out.format("\n", new Object[0]);
        }
    }
    
    private boolean showCoord(final Formatter out, final String name, final String indent) {
        if (name != null) {
            out.format(" %s Coord %s %s\n", indent, name, this.getKind(name));
            return true;
        }
        return false;
    }
    
    static {
        Table.log = LoggerFactory.getLogger(Table.class);
    }
    
    public enum CoordName
    {
        Lat, 
        Lon, 
        Elev, 
        Time, 
        TimeNominal, 
        StnId, 
        StnDesc, 
        WmoId, 
        StnAlt, 
        FeatureId, 
        MissingVar;
    }
    
    public enum Type
    {
        ArrayStructure, 
        Construct, 
        Contiguous, 
        LinkedList, 
        MultidimInner, 
        MultidimInner3D, 
        MultidimInnerPsuedo, 
        MultidimInnerPsuedo3D, 
        MultidimStructure, 
        NestedStructure, 
        ParentId, 
        ParentIndex, 
        Singleton, 
        Structure, 
        Top;
    }
    
    public static class TableStructure extends Table
    {
        StructureDS struct;
        Dimension dim;
        Dimension outer;
        TableConfig.StructureType stype;
        
        TableStructure(final NetcdfDataset ds, final TableConfig config) {
            super(ds, config);
            this.stype = config.structureType;
            switch (config.structureType) {
                case Structure: {
                    this.struct = (StructureDS)ds.findVariable(config.structName);
                    if (this.struct == null) {
                        throw new IllegalStateException("Cant find Structure " + config.structName);
                    }
                    this.dim = this.struct.getDimension(0);
                    if (config.vars != null) {
                        this.struct = (StructureDS)this.struct.select(config.vars);
                        break;
                    }
                    break;
                }
                case PsuedoStructure: {
                    this.dim = ds.findDimension(config.dimName);
                    assert this.dim != null;
                    this.struct = new StructurePseudoDS(ds, this.dim.getGroup(), config.structName, config.vars, this.dim);
                    break;
                }
                case PsuedoStructure2D: {
                    this.dim = ds.findDimension(config.dimName);
                    this.outer = ds.findDimension(config.outerName);
                    assert this.dim != null;
                    assert config.outerName != null;
                    this.struct = new StructurePseudo2Dim(ds, this.dim.getGroup(), config.structName, config.vars, this.dim, this.outer);
                    break;
                }
            }
            config.vars = new ArrayList<String>();
            for (final Variable v : this.struct.getVariables()) {
                if (v.getDataType() == DataType.STRUCTURE) {
                    if (config.structureType != TableConfig.StructureType.PsuedoStructure) {
                        continue;
                    }
                    this.struct.removeMemberVariable(v);
                }
                else {
                    this.cols.add(v);
                    config.vars.add(v.getShortName());
                }
            }
        }
        
        @Override
        protected void showTableExtraInfo(final String indent, final Formatter f) {
            f.format("%sstruct=%s, dim=%s type=%s%n", indent, this.struct.getNameAndDimensions(), this.dim.getName(), this.struct.getClass().getName());
        }
        
        @Override
        public VariableDS findVariable(final String axisName) {
            return (VariableDS)this.struct.findVariable(axisName);
        }
        
        @Override
        public String showDimension() {
            return this.dim.getName();
        }
        
        @Override
        public StructureDataIterator getStructureDataIterator(final Cursor cursor, final int bufferSize) throws IOException {
            return this.struct.getStructureIterator(bufferSize);
        }
        
        @Override
        public String getName() {
            return this.stype.toString() + "(" + this.struct.getName() + ")";
        }
    }
    
    public static class TableArrayStructure extends Table
    {
        ArrayStructure as;
        Dimension dim;
        
        TableArrayStructure(final NetcdfDataset ds, final TableConfig config) {
            super(ds, config);
            this.as = config.as;
            this.dim = new Dimension(config.structName, (int)config.as.getSize(), false);
            assert this.as != null;
            for (final StructureMembers.Member m : config.as.getStructureMembers().getMembers()) {
                this.cols.add(new VariableSimpleAdapter(m));
            }
        }
        
        @Override
        protected void showTableExtraInfo(final String indent, final Formatter f) {
            f.format("%sArrayStruct=%s, dim=%s%n", indent, new Section(this.as.getShape()), this.dim.getName());
        }
        
        @Override
        public String showDimension() {
            return this.dim.getName();
        }
        
        @Override
        public StructureDataIterator getStructureDataIterator(final Cursor cursor, final int bufferSize) throws IOException {
            return this.as.getStructureDataIterator();
        }
        
        @Override
        public String getName() {
            return "ArrayStructure(" + this.name + ")";
        }
    }
    
    public static class TableConstruct extends Table
    {
        ArrayStructure as;
        
        TableConstruct(final NetcdfDataset ds, final TableConfig config) {
            super(ds, config);
        }
        
        @Override
        protected void showTableExtraInfo(final String indent, final Formatter f) {
        }
        
        @Override
        public StructureDataIterator getStructureDataIterator(final Cursor cursor, final int bufferSize) throws IOException {
            return this.as.getStructureDataIterator();
        }
        
        @Override
        public String getName() {
            return "Constructed";
        }
    }
    
    public static class TableContiguous extends TableStructure
    {
        private String startVarName;
        private String numRecordsVarName;
        private int[] startIndex;
        private int[] numRecords;
        
        TableContiguous(final NetcdfDataset ds, final TableConfig config) {
            super(ds, config);
            this.startVarName = config.start;
            this.numRecordsVarName = config.numRecords;
            if (this.startVarName == null) {
                try {
                    final Variable v = ds.findVariable(config.numRecords);
                    final Array numRecords = v.read();
                    final int n = (int)v.getSize();
                    this.numRecords = new int[n];
                    this.startIndex = new int[n];
                    int i = 0;
                    int count = 0;
                    while (numRecords.hasNext()) {
                        this.startIndex[i] = count;
                        this.numRecords[i] = numRecords.nextInt();
                        count += this.numRecords[i];
                        ++i;
                    }
                }
                catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            this.checkNonDataVariable(config.start);
            this.checkNonDataVariable(config.numRecords);
        }
        
        @Override
        protected void showTableExtraInfo(final String indent, final Formatter f) {
            f.format("%sstart=%s, numRecords=%s%n", indent, this.startVarName, this.numRecordsVarName);
        }
        
        @Override
        public StructureDataIterator getStructureDataIterator(final Cursor cursor, final int bufferSize) throws IOException {
            final StructureData parentStruct = cursor.getParentStructure();
            int firstRecno;
            int numrecs;
            if (this.startIndex != null) {
                final int parentIndex = cursor.getParentRecnum();
                firstRecno = this.startIndex[parentIndex];
                numrecs = this.numRecords[parentIndex];
            }
            else {
                firstRecno = parentStruct.getScalarInt(this.startVarName);
                numrecs = parentStruct.getScalarInt(this.numRecordsVarName);
            }
            return new StructureDataIteratorLinked(this.struct, firstRecno, numrecs, null);
        }
        
        @Override
        public String getName() {
            return "Contig(" + this.numRecordsVarName + ")";
        }
    }
    
    public static class TableParentIndex extends TableStructure
    {
        private Map<Integer, List<Integer>> indexMap;
        private String parentIndexName;
        
        TableParentIndex(final NetcdfDataset ds, final TableConfig config) {
            super(ds, config);
            this.parentIndexName = config.parentIndex;
            try {
                final Variable rpIndex = ds.findVariable(config.parentIndex);
                final Array index = rpIndex.read();
                int childIndex = 0;
                this.indexMap = new HashMap<Integer, List<Integer>>((int)(2L * index.getSize()));
                while (index.hasNext()) {
                    final int parent = index.nextInt();
                    List<Integer> list = this.indexMap.get(parent);
                    if (list == null) {
                        list = new ArrayList<Integer>();
                        this.indexMap.put(parent, list);
                    }
                    list.add(childIndex);
                    ++childIndex;
                }
            }
            catch (IOException e) {
                throw new RuntimeException(e);
            }
            this.checkNonDataVariable(config.parentIndex);
        }
        
        @Override
        protected void showTableExtraInfo(final String indent, final Formatter f) {
            f.format("%sparentIndexName=%s, indexMap.size=%d%n", indent, this.parentIndexName, this.indexMap.size());
        }
        
        @Override
        public StructureDataIterator getStructureDataIterator(final Cursor cursor, final int bufferSize) throws IOException {
            final int parentIndex = cursor.getParentRecnum();
            List<Integer> index = this.indexMap.get(parentIndex);
            if (index == null) {
                index = new ArrayList<Integer>();
            }
            return new StructureDataIteratorIndexed(this.struct, index);
        }
        
        @Override
        public String getName() {
            return "Indexed(" + this.parentIndexName + ")";
        }
    }
    
    public static class TableParentId extends TableStructure
    {
        private ParentInfo[] indexMap;
        private String parentIdName;
        
        TableParentId(final NetcdfDataset ds, final TableConfig config) {
            super(ds, config);
            this.parentIdName = config.parentIndex;
            Map<Object, ParentInfo> parentHash;
            try {
                Variable rpIndex = ds.findVariable(this.parentIdName);
                if (rpIndex == null) {
                    rpIndex = this.struct.findVariable(this.parentIdName);
                }
                Array index = rpIndex.read();
                if (index instanceof ArrayChar) {
                    index = ((ArrayChar)index).make1DStringArray();
                }
                parentHash = new HashMap<Object, ParentInfo>((int)(2L * index.getSize()));
                int childIndex = 0;
                while (index.hasNext()) {
                    final Object parent = index.next();
                    ParentInfo info = parentHash.get(parent);
                    if (info == null) {
                        info = new ParentInfo();
                        parentHash.put(parent, info);
                    }
                    info.add(childIndex);
                    ++childIndex;
                }
            }
            catch (IOException e) {
                throw new RuntimeException(e);
            }
            final Collection<ParentInfo> parents = parentHash.values();
            final int n = parents.size();
            this.indexMap = new ParentInfo[n];
            final StructureData[] parentData = new StructureData[n];
            int count = 0;
            for (final ParentInfo info2 : parents) {
                this.indexMap[count] = info2;
                parentData[count++] = info2.sdata;
            }
            final ArrayStructure as = new ArrayStructureW(this.struct.makeStructureMembers(), new int[] { n }, parentData);
            Table t = this;
            while (t.parent != null) {
                t = t.parent;
                if (t instanceof TableConstruct) {
                    ((TableConstruct)t).as = as;
                    break;
                }
            }
            this.checkNonDataVariable(this.parentIdName);
        }
        
        @Override
        protected void showTableExtraInfo(final String indent, final Formatter f) {
            f.format("%sparentIdName=%s, indexMap.size=%d%n", indent, this.parentIdName, this.indexMap.length);
        }
        
        @Override
        public StructureDataIterator getStructureDataIterator(final Cursor cursor, final int bufferSize) throws IOException {
            final int parentIndex = cursor.getParentRecnum();
            final ParentInfo info = this.indexMap[parentIndex];
            final List<Integer> index = (info == null) ? new ArrayList<Integer>() : info.recnumList;
            return new StructureDataIteratorIndexed(this.struct, index);
        }
        
        @Override
        public String getName() {
            return "ParentId(" + this.parentIdName + ")";
        }
        
        private class ParentInfo
        {
            List<Integer> recnumList;
            StructureData sdata;
            
            private ParentInfo() {
                this.recnumList = new ArrayList<Integer>();
            }
            
            void add(final int recnum) throws IOException {
                this.recnumList.add(recnum);
                if (this.sdata != null) {
                    return;
                }
                try {
                    this.sdata = TableParentId.this.struct.readStructure(recnum);
                }
                catch (InvalidRangeException e) {
                    Table.log.error("TableParentId read recno=" + recnum, e);
                    throw new RuntimeException(e.getMessage());
                }
            }
        }
    }
    
    public static class TableLinkedList extends TableStructure
    {
        private String start;
        private String next;
        
        TableLinkedList(final NetcdfDataset ds, final TableConfig config) {
            super(ds, config);
            this.start = config.start;
            this.next = config.next;
            this.checkNonDataVariable(config.start);
            this.checkNonDataVariable(config.next);
        }
        
        @Override
        public StructureDataIterator getStructureDataIterator(final Cursor cursor, final int bufferSize) throws IOException {
            final StructureData parentStruct = cursor.getParentStructure();
            final int firstRecno = parentStruct.getScalarInt(this.start);
            return new StructureDataIteratorLinked(this.struct, firstRecno, -1, this.next);
        }
        
        @Override
        public String getName() {
            return "Linked(" + this.start + "->" + this.next + ")";
        }
    }
    
    public static class TableMultidimInner extends Table
    {
        StructureMembers sm;
        Dimension inner;
        Dimension outer;
        NetcdfDataset ds;
        
        TableMultidimInner(final NetcdfDataset ds, final TableConfig config) {
            super(ds, config);
            this.ds = ds;
            assert config.outerName != null;
            assert config.innerName != null;
            this.inner = ds.findDimension(config.innerName);
            this.outer = ds.findDimension(config.outerName);
            assert this.inner != null : config.innerName;
            assert this.outer != null : config.outerName;
            this.sm = new StructureMembers(config.name);
            if (config.vars != null) {
                for (final String name : config.vars) {
                    final Variable v = ds.findVariable(name);
                    if (v == null) {
                        continue;
                    }
                    this.cols.add(v);
                    final int rank = v.getRank();
                    final int[] shape = new int[rank - 2];
                    System.arraycopy(v.getShape(), 2, shape, 0, rank - 2);
                    this.sm.addMember(v.getShortName(), v.getDescription(), v.getUnitsString(), v.getDataType(), shape);
                }
            }
            else {
                for (final Variable v2 : ds.getVariables()) {
                    if (v2.getRank() < 2) {
                        continue;
                    }
                    if (!v2.getDimension(0).equals(this.outer) || !v2.getDimension(1).equals(this.inner)) {
                        continue;
                    }
                    this.cols.add(v2);
                    final int rank2 = v2.getRank();
                    final int[] shape2 = new int[rank2 - 2];
                    System.arraycopy(v2.getShape(), 2, shape2, 0, rank2 - 2);
                    this.sm.addMember(v2.getShortName(), v2.getDescription(), v2.getUnitsString(), v2.getDataType(), shape2);
                }
            }
        }
        
        @Override
        protected void showTableExtraInfo(final String indent, final Formatter f) {
            f.format("%sStructureMembers=%s, dim=%s,%s%n", indent, this.sm.getName(), this.outer.getName(), this.inner.getName());
        }
        
        @Override
        public String showDimension() {
            return this.inner.getName();
        }
        
        @Override
        public VariableDS findVariable(final String axisName) {
            return (VariableDS)this.ds.findVariable(axisName);
        }
        
        @Override
        public StructureDataIterator getStructureDataIterator(final Cursor cursor, final int bufferSize) throws IOException {
            final StructureData parentStruct = cursor.getParentStructure();
            final ArrayStructureMA asma = new ArrayStructureMA(this.sm, new int[] { this.inner.getLength() });
            for (final VariableSimpleIF v : this.cols) {
                final Array data = parentStruct.getArray(v.getShortName());
                final StructureMembers.Member childm = this.sm.findMember(v.getShortName());
                childm.setDataArray(data);
            }
            return asma.getStructureDataIterator();
        }
        
        @Override
        public String getName() {
            return "Multidim(" + this.outer.getName() + "," + this.inner.getName() + ")";
        }
    }
    
    public static class TableMultidimInner3D extends Table
    {
        StructureMembers sm;
        Dimension dim;
        Dimension inner;
        Dimension middle;
        NetcdfDataset ds;
        
        TableMultidimInner3D(final NetcdfDataset ds, final TableConfig config) {
            super(ds, config);
            this.ds = ds;
            assert config.dimName != null;
            assert config.outerName != null;
            assert config.innerName != null;
            this.dim = ds.findDimension(config.dimName);
            this.inner = ds.findDimension(config.innerName);
            this.middle = ds.findDimension(config.outerName);
            this.sm = new StructureMembers(config.name);
            if (config.vars != null) {
                for (final String name : config.vars) {
                    final Variable v = ds.findVariable(name);
                    if (v == null) {
                        continue;
                    }
                    this.cols.add(v);
                    final int rank = v.getRank();
                    final int[] shape = new int[rank - 3];
                    System.arraycopy(v.getShape(), 3, shape, 0, rank - 3);
                    this.sm.addMember(v.getShortName(), v.getDescription(), v.getUnitsString(), v.getDataType(), shape);
                }
            }
            else {
                for (final Variable v2 : ds.getVariables()) {
                    if (v2.getRank() < 3) {
                        continue;
                    }
                    if (!v2.getDimension(0).equals(this.dim) || !v2.getDimension(1).equals(this.middle) || !v2.getDimension(2).equals(this.inner)) {
                        continue;
                    }
                    this.cols.add(v2);
                    final int rank2 = v2.getRank();
                    final int[] shape2 = new int[rank2 - 3];
                    System.arraycopy(v2.getShape(), 3, shape2, 0, rank2 - 3);
                    this.sm.addMember(v2.getShortName(), v2.getDescription(), v2.getUnitsString(), v2.getDataType(), shape2);
                }
            }
        }
        
        @Override
        protected void showTableExtraInfo(final String indent, final Formatter f) {
            f.format("%sStructureMembers=%s, dim=%s%n", indent, this.sm.getName(), this.dim.getName());
        }
        
        @Override
        public String showDimension() {
            return this.dim.getName();
        }
        
        @Override
        public VariableDS findVariable(final String axisName) {
            return (VariableDS)this.ds.findVariable(axisName);
        }
        
        @Override
        public StructureDataIterator getStructureDataIterator(final Cursor cursor, final int bufferSize) throws IOException {
            final StructureData parentStruct = cursor.tableData[2];
            final int middleIndex = cursor.recnum[1];
            final ArrayStructureMA asma = new ArrayStructureMA(this.sm, new int[] { this.inner.getLength() });
            for (final VariableSimpleIF v : this.cols) {
                final Array data = parentStruct.getArray(v.getShortName());
                final Array myData = data.slice(0, middleIndex);
                final StructureMembers.Member childm = this.sm.findMember(v.getShortName());
                childm.setDataArray(myData.copy());
            }
            return asma.getStructureDataIterator();
        }
        
        @Override
        public String getName() {
            return "Multidim(" + this.dim.getName() + "," + this.middle.getName() + "," + this.inner.getName() + ")";
        }
    }
    
    public static class TableMultidimInnerPsuedo extends TableStructure
    {
        Dimension inner;
        Dimension outer;
        StructureMembers sm;
        
        TableMultidimInnerPsuedo(final NetcdfDataset ds, final TableConfig config) {
            super(ds, config);
            assert config.outerName != null;
            assert config.innerName != null;
            this.inner = ds.findDimension(config.innerName);
            this.outer = ds.findDimension(config.outerName);
            this.sm = new StructureMembers(config.name);
            for (final Variable v : this.struct.getVariables()) {
                final int rank = v.getRank();
                final int[] shape = new int[rank - 1];
                System.arraycopy(v.getShape(), 1, shape, 0, rank - 1);
                this.sm.addMember(v.getShortName(), v.getDescription(), v.getUnitsString(), v.getDataType(), shape);
            }
        }
        
        @Override
        public StructureDataIterator getStructureDataIterator(final Cursor cursor, final int bufferSize) throws IOException {
            final int recnum = cursor.getParentRecnum();
            try {
                final StructureData parentStruct = this.struct.readStructure(recnum);
                final ArrayStructureMA asma = new ArrayStructureMA(this.sm, new int[] { this.inner.getLength() });
                for (final VariableSimpleIF v : this.cols) {
                    final Array data = parentStruct.getArray(v.getShortName());
                    final StructureMembers.Member childm = this.sm.findMember(v.getShortName());
                    childm.setDataArray(data);
                }
                return asma.getStructureDataIterator();
            }
            catch (InvalidRangeException e) {
                throw new IllegalStateException(e);
            }
        }
        
        @Override
        public String getName() {
            return "MultidimPseudo(" + this.outer.getName() + "," + this.inner.getName() + ")";
        }
    }
    
    public static class TableMultidimInnerPsuedo3D extends TableStructure
    {
        Dimension middle;
        Dimension inner;
        StructureMembers sm;
        
        TableMultidimInnerPsuedo3D(final NetcdfDataset ds, final TableConfig config) {
            super(ds, config);
            assert config.dimName != null;
            assert config.outerName != null;
            assert config.innerName != null;
            this.dim = ds.findDimension(config.dimName);
            this.middle = ds.findDimension(config.outerName);
            this.inner = ds.findDimension(config.innerName);
            this.sm = new StructureMembers(config.name);
            for (final Variable v : this.struct.getVariables()) {
                final int rank = v.getRank();
                final int[] shape = new int[rank - 1];
                System.arraycopy(v.getShape(), 1, shape, 0, rank - 1);
                this.sm.addMember(v.getShortName(), v.getDescription(), v.getUnitsString(), v.getDataType(), shape);
            }
        }
        
        @Override
        public StructureDataIterator getStructureDataIterator(final Cursor cursor, final int bufferSize) throws IOException {
            final int outerIndex = cursor.recnum[2];
            final int middleIndex = cursor.recnum[1];
            try {
                final Section s = new Section().appendRange(outerIndex, outerIndex).appendRange(middleIndex, middleIndex);
                final ArrayStructure result = (ArrayStructure)this.struct.read(s);
                assert result.getSize() == 1L;
                final StructureData sdata = result.getStructureData(0);
                final ArrayStructureMA asma = new ArrayStructureMA(this.sm, new int[] { this.inner.getLength() });
                for (final VariableSimpleIF v : this.cols) {
                    final Array data = sdata.getArray(v.getShortName());
                    final StructureMembers.Member childm = this.sm.findMember(v.getShortName());
                    childm.setDataArray(data);
                }
                return asma.getStructureDataIterator();
            }
            catch (InvalidRangeException e) {
                throw new IllegalStateException(e);
            }
        }
        
        @Override
        public String getName() {
            return "MultidimPsuedo(" + this.dim.getName() + "," + this.middle.getName() + "," + this.inner.getName() + ")";
        }
    }
    
    public static class TableMultidimStructure extends TableStructure
    {
        TableMultidimStructure(final NetcdfDataset ds, final TableConfig config) {
            super(ds, config);
        }
        
        @Override
        public StructureDataIterator getStructureDataIterator(final Cursor cursor, final int bufferSize) throws IOException {
            final int recnum = cursor.getParentRecnum();
            try {
                final Section section = new Section().appendRange(recnum, recnum);
                int count = 1;
                while (count++ < this.struct.getRank()) {
                    section.appendRange(null);
                }
                final ArrayStructure data = (ArrayStructure)this.struct.read(section);
                return data.getStructureDataIterator();
            }
            catch (InvalidRangeException e) {
                throw new IllegalStateException(e);
            }
        }
        
        @Override
        public String getName() {
            return "MultidimStructure(" + this.struct.getName() + ")";
        }
    }
    
    public static class TableNestedStructure extends Table
    {
        String nestedTableName;
        Structure struct;
        
        TableNestedStructure(final NetcdfDataset ds, final TableConfig config) {
            super(ds, config);
            this.nestedTableName = config.nestedTableName;
            this.struct = (Structure)ds.findVariable(config.structName);
            assert this.struct != null;
            for (final Variable v : this.struct.getVariables()) {
                this.cols.add(v);
            }
        }
        
        @Override
        protected void showTableExtraInfo(final String indent, final Formatter f) {
            f.format("%sstruct=%s, nestedTableName=%s%n", indent, this.struct.getNameAndDimensions(), this.nestedTableName);
        }
        
        @Override
        public VariableDS findVariable(final String axisName) {
            return (VariableDS)this.struct.findVariable(axisName);
        }
        
        @Override
        public StructureDataIterator getStructureDataIterator(final Cursor cursor, final int bufferSize) throws IOException {
            final StructureData parentStruct = cursor.getParentStructure();
            final StructureMembers members = parentStruct.getStructureMembers();
            final StructureMembers.Member m = members.findMember(this.nestedTableName);
            members.hideMember(m);
            if (m.getDataType() == DataType.SEQUENCE) {
                final ArraySequence seq = parentStruct.getArraySequence(m);
                return seq.getStructureDataIterator();
            }
            if (m.getDataType() == DataType.STRUCTURE) {
                final ArrayStructure as = parentStruct.getArrayStructure(m);
                return as.getStructureDataIterator();
            }
            throw new IllegalStateException("Cant find nested table member = " + this.nestedTableName);
        }
        
        @Override
        public String getName() {
            return "NestedStructure(" + this.nestedTableName + ")";
        }
    }
    
    public static class TableSingleton extends Table
    {
        StructureData sdata;
        
        TableSingleton(final NetcdfDataset ds, final TableConfig config) {
            super(ds, config);
            this.sdata = config.sdata;
            if (this.sdata == null) {
                return;
            }
            for (final StructureMembers.Member m : this.sdata.getStructureMembers().getMembers()) {
                this.cols.add(new VariableSimpleAdapter(m));
            }
        }
        
        @Override
        protected void showTableExtraInfo(final String indent, final Formatter f) {
            f.format("%sStructureData=%s%n", indent, this.sdata);
        }
        
        @Override
        public StructureDataIterator getStructureDataIterator(final Cursor cursor, final int bufferSize) throws IOException {
            return new SingletonStructureDataIterator(this.sdata);
        }
        
        @Override
        public String getName() {
            return "Singleton";
        }
    }
    
    public static class TableTop extends Table
    {
        NetcdfDataset ds;
        
        TableTop(final NetcdfDataset ds, final TableConfig config) {
            super(ds, config);
            this.ds = ds;
        }
        
        @Override
        protected void showTableExtraInfo(final String indent, final Formatter f) {
        }
        
        @Override
        public StructureDataIterator getStructureDataIterator(final Cursor cursor, final int bufferSize) throws IOException {
            return new SingletonStructureDataIterator(null);
        }
        
        @Override
        public String getName() {
            return "TopScalars";
        }
    }
    
    private static class SingletonStructureDataIterator implements StructureDataIterator
    {
        private int count;
        private StructureData sdata;
        
        SingletonStructureDataIterator(final StructureData sdata) {
            this.count = 0;
            this.sdata = sdata;
        }
        
        public boolean hasNext() throws IOException {
            return this.count == 0;
        }
        
        public StructureData next() throws IOException {
            ++this.count;
            return this.sdata;
        }
        
        public void setBufferSize(final int bytes) {
        }
        
        public StructureDataIterator reset() {
            this.count = 0;
            return this;
        }
        
        public int getCurrentRecno() {
            return this.count - 1;
        }
    }
}
