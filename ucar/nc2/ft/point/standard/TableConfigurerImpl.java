// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard;

import ucar.nc2.Attribute;
import ucar.nc2.dataset.CoordinateAxis;
import ucar.nc2.constants.AxisType;
import ucar.ma2.DataType;
import java.util.Iterator;
import ucar.nc2.Variable;
import java.util.Formatter;
import ucar.nc2.Dimension;
import ucar.nc2.dataset.NetcdfDataset;

public abstract class TableConfigurerImpl implements TableConfigurer
{
    private String convName;
    private String convUsed;
    
    public String getConvName() {
        return this.convName;
    }
    
    public void setConvName(final String convName) {
        this.convName = convName;
    }
    
    public String getConvUsed() {
        return this.convUsed;
    }
    
    public void setConvUsed(final String convUsed) {
        this.convUsed = convUsed;
    }
    
    protected String findNameVariableWithStandardNameAndDimension(final NetcdfDataset ds, final String standard_name, final Dimension outer, final Formatter errlog) {
        final Variable v = this.findVariableWithStandardNameAndDimension(ds, standard_name, outer, errlog);
        return (v == null) ? null : v.getShortName();
    }
    
    protected Variable findVariableWithStandardNameAndDimension(final NetcdfDataset ds, final String standard_name, final Dimension outer, final Formatter errlog) {
        for (final Variable v : ds.getVariables()) {
            final String stdName = ds.findAttValueIgnoreCase(v, "standard_name", null);
            if (stdName != null && stdName.equals(standard_name)) {
                if (v.getRank() > 0 && v.getDimension(0).equals(outer)) {
                    return v;
                }
                if (this.isEffectivelyScaler(v) && outer == null) {
                    return v;
                }
                continue;
            }
        }
        return null;
    }
    
    protected boolean isEffectivelyScaler(final Variable v) {
        return v.getRank() == 0 || (v.getRank() == 1 && v.getDataType() == DataType.CHAR);
    }
    
    protected Variable findVariableWithStandardNameAndNotDimension(final NetcdfDataset ds, final String standard_name, final Dimension outer, final Formatter errlog) {
        for (final Variable v : ds.getVariables()) {
            final String stdName = ds.findAttValueIgnoreCase(v, "standard_name", null);
            if (stdName != null && stdName.equals(standard_name) && v.getRank() > 0 && !v.getDimension(0).equals(outer)) {
                return v;
            }
        }
        return null;
    }
    
    protected String matchAxisTypeAndDimension(final NetcdfDataset ds, final AxisType type, final Dimension outer) {
        final Variable var = CoordSysEvaluator.findCoordByType(ds, type, new CoordSysEvaluator.Predicate() {
            public boolean match(final CoordinateAxis axis) {
                return (outer == null && axis.getRank() == 0) || (outer != null && axis.getRank() == 1 && outer.equals(axis.getDimension(0)));
            }
        });
        if (var == null) {
            return null;
        }
        return var.getShortName();
    }
    
    protected String matchAxisTypeAndDimension(final NetcdfDataset ds, final AxisType type, final Dimension outer, final Dimension inner) {
        final Variable var = CoordSysEvaluator.findCoordByType(ds, type, new CoordSysEvaluator.Predicate() {
            public boolean match(final CoordinateAxis axis) {
                return axis.getRank() == 2 && outer.equals(axis.getDimension(0)) && inner.equals(axis.getDimension(1));
            }
        });
        if (var == null) {
            return null;
        }
        return var.getShortName();
    }
    
    protected String matchAxisTypeAndDimension(final NetcdfDataset ds, final AxisType type, final Dimension outer, final Dimension middle, final Dimension inner) {
        final Variable var = CoordSysEvaluator.findCoordByType(ds, type, new CoordSysEvaluator.Predicate() {
            public boolean match(final CoordinateAxis axis) {
                return axis.getRank() == 3 && outer.equals(axis.getDimension(0)) && middle.equals(axis.getDimension(1)) && inner.equals(axis.getDimension(2));
            }
        });
        if (var == null) {
            return null;
        }
        return var.getShortName();
    }
    
    protected CoordinateAxis findZAxisNotStationAlt(final NetcdfDataset ds) {
        CoordinateAxis z = CoordSysEvaluator.findCoordByType(ds, AxisType.Height, new CoordSysEvaluator.Predicate() {
            public boolean match(final CoordinateAxis axis) {
                final Attribute stdName = axis.findAttribute("standard_name");
                return stdName == null || !"surface_altitude".equals(stdName.getStringValue());
            }
        });
        if (z != null) {
            return z;
        }
        z = CoordSysEvaluator.findCoordByType(ds, AxisType.Pressure, new CoordSysEvaluator.Predicate() {
            public boolean match(final CoordinateAxis axis) {
                final Attribute stdName = axis.findAttribute("standard_name");
                return stdName == null || !"surface_altitude".equals(stdName.getStringValue());
            }
        });
        return z;
    }
}
