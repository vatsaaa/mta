// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard;

import java.io.IOException;
import ucar.nc2.ft.PointFeatureCollection;
import ucar.nc2.ft.PointFeatureIterator;
import ucar.nc2.units.DateUnit;
import ucar.nc2.ft.point.PointCollectionImpl;

public class StandardPointCollectionImpl extends PointCollectionImpl
{
    private DateUnit timeUnit;
    private NestedTable ft;
    
    StandardPointCollectionImpl(final NestedTable ft, final DateUnit timeUnit) {
        super(ft.getName());
        this.ft = ft;
        this.timeUnit = timeUnit;
    }
    
    public PointFeatureIterator getPointFeatureIterator(final int bufferSize) throws IOException {
        final Cursor tableData = new Cursor(this.ft.getNumberOfLevels());
        final PointFeatureIterator iter = new StandardPointFeatureIterator(this.ft, this.timeUnit, this.ft.getObsDataIterator(tableData, bufferSize), tableData);
        if (this.boundingBox == null || this.dateRange == null || this.npts < 0) {
            iter.setCalculateBounds(this);
        }
        return iter;
    }
}
