// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard;

import ucar.nc2.ft.ProfileFeature;
import ucar.nc2.ft.point.PointCollectionIteratorFiltered;
import ucar.nc2.ft.PointFeatureIterator;
import ucar.nc2.ft.point.TrajectoryFeatureImpl;
import ucar.nc2.ft.PointFeatureCollection;
import ucar.ma2.StructureData;
import ucar.ma2.StructureDataIterator;
import ucar.nc2.ft.NestedPointFeatureCollection;
import ucar.nc2.ft.TrajectoryFeature;
import ucar.unidata.geoloc.LatLonRect;
import java.io.IOException;
import ucar.nc2.ft.PointFeatureCollectionIterator;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.units.DateUnit;
import ucar.nc2.ft.TrajectoryFeatureCollection;
import ucar.nc2.ft.point.OneNestedPointCollectionImpl;

public class StandardTrajectoryCollectionImpl extends OneNestedPointCollectionImpl implements TrajectoryFeatureCollection
{
    private DateUnit timeUnit;
    private NestedTable ft;
    private TrajCollectionIterator localIterator;
    
    protected StandardTrajectoryCollectionImpl(final String name) {
        super(name, FeatureType.TRAJECTORY);
        this.localIterator = null;
    }
    
    StandardTrajectoryCollectionImpl(final NestedTable ft, final DateUnit timeUnit) {
        super(ft.getName(), FeatureType.TRAJECTORY);
        this.localIterator = null;
        this.ft = ft;
        this.timeUnit = timeUnit;
    }
    
    public PointFeatureCollectionIterator getPointFeatureCollectionIterator(final int bufferSize) throws IOException {
        return new TrajCollectionIterator(this.ft.getRootFeatureDataIterator(bufferSize));
    }
    
    public boolean hasNext() throws IOException {
        if (this.localIterator == null) {
            this.resetIteration();
        }
        return this.localIterator.hasNext();
    }
    
    public TrajectoryFeatureCollection subset(final LatLonRect boundingBox) throws IOException {
        return new StandardTrajectoryCollectionSubset(this, boundingBox);
    }
    
    public TrajectoryFeature next() throws IOException {
        return this.localIterator.next();
    }
    
    public void resetIteration() throws IOException {
        this.localIterator = (TrajCollectionIterator)this.getPointFeatureCollectionIterator(-1);
    }
    
    private class TrajCollectionIterator implements PointFeatureCollectionIterator
    {
        StructureDataIterator structIter;
        int count;
        StructureData nextTraj;
        
        TrajCollectionIterator(final StructureDataIterator structIter) throws IOException {
            this.count = 0;
            this.structIter = structIter;
        }
        
        public boolean hasNext() throws IOException {
            while (this.structIter.hasNext()) {
                this.nextTraj = this.structIter.next();
                if (!StandardTrajectoryCollectionImpl.this.ft.isFeatureMissing(this.nextTraj)) {
                    return true;
                }
            }
            return false;
        }
        
        public TrajectoryFeature next() throws IOException {
            final Cursor cursor = new Cursor(StandardTrajectoryCollectionImpl.this.ft.getNumberOfLevels());
            cursor.recnum[1] = this.structIter.getCurrentRecno();
            cursor.tableData[1] = this.nextTraj;
            cursor.currentIndex = 1;
            StandardTrajectoryCollectionImpl.this.ft.addParentJoin(cursor);
            return new StandardTrajectoryFeature(cursor);
        }
        
        public void setBufferSize(final int bytes) {
        }
        
        public void finish() {
        }
    }
    
    private class StandardTrajectoryFeature extends TrajectoryFeatureImpl
    {
        Cursor cursor;
        
        StandardTrajectoryFeature(final Cursor cursor) {
            super(StandardTrajectoryCollectionImpl.this.ft.getFeatureName(cursor), -1);
            this.cursor = cursor;
        }
        
        public PointFeatureIterator getPointFeatureIterator(final int bufferSize) throws IOException {
            final Cursor cursorIter = this.cursor.copy();
            final StructureDataIterator siter = StandardTrajectoryCollectionImpl.this.ft.getLeafFeatureDataIterator(cursorIter, bufferSize);
            final StandardPointFeatureIterator iter = new StandardPointFeatureIterator(StandardTrajectoryCollectionImpl.this.ft, StandardTrajectoryCollectionImpl.this.timeUnit, siter, cursorIter);
            if (this.boundingBox == null || this.dateRange == null || this.npts < 0) {
                iter.setCalculateBounds(this);
            }
            return iter;
        }
    }
    
    private class StandardTrajectoryCollectionSubset extends StandardTrajectoryCollectionImpl
    {
        TrajectoryFeatureCollection from;
        LatLonRect boundingBox;
        
        StandardTrajectoryCollectionSubset(final TrajectoryFeatureCollection from, final LatLonRect boundingBox) {
            super(from.getName() + "-subset");
            this.from = from;
            this.boundingBox = boundingBox;
        }
        
        @Override
        public PointFeatureCollectionIterator getPointFeatureCollectionIterator(final int bufferSize) throws IOException {
            return new PointCollectionIteratorFiltered(this.from.getPointFeatureCollectionIterator(bufferSize), new Filter());
        }
        
        private class Filter implements PointFeatureCollectionIterator.Filter
        {
            public boolean filter(final PointFeatureCollection pointFeatureCollection) {
                final ProfileFeature profileFeature = (ProfileFeature)pointFeatureCollection;
                return StandardTrajectoryCollectionSubset.this.boundingBox.contains(profileFeature.getLatLon());
            }
        }
    }
}
