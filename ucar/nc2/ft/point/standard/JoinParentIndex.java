// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard;

import ucar.nc2.dataset.VariableDS;
import ucar.ma2.StructureData;
import java.io.IOException;
import ucar.ma2.ArrayStructure;
import ucar.nc2.dataset.StructureDS;

public class JoinParentIndex implements Join
{
    StructureDS parentStructure;
    ArrayStructure parentData;
    String parentIndex;
    
    public JoinParentIndex(final StructureDS parentStructure, final String parentIndex) {
        this.parentStructure = parentStructure;
        this.parentIndex = parentIndex;
        try {
            this.parentData = (ArrayStructure)parentStructure.read();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public StructureData getJoinData(final Cursor cursor) {
        final StructureData sdata = cursor.tableData[0];
        final int index = sdata.getScalarInt(this.parentIndex);
        return this.parentData.getStructureData(index);
    }
    
    public VariableDS findVariable(final String axisName) {
        return (VariableDS)this.parentStructure.findVariable(axisName);
    }
    
    @Override
    public String toString() {
        return "JoinParentIndex{parentStructure=" + this.parentStructure + ", parentIndex='" + this.parentIndex + '}';
    }
}
