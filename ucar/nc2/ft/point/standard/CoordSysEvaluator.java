// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard;

import ucar.nc2.Dimension;
import java.util.Iterator;
import ucar.nc2.constants.AxisType;
import ucar.nc2.dataset.CoordinateAxis;
import java.util.List;
import ucar.nc2.dataset.CoordinateSystem;
import ucar.nc2.dataset.NetcdfDataset;

public class CoordSysEvaluator
{
    public static void findCoords(final TableConfig nt, final NetcdfDataset ds) {
        final CoordinateSystem use = findBestCoordinateSystem(ds);
        if (use == null) {
            findCoords(nt, ds.getCoordinateAxes());
        }
        else {
            findCoords(nt, use.getCoordinateAxes());
        }
    }
    
    public static void findCoords(final TableConfig nt, final List<CoordinateAxis> axes) {
        for (final CoordinateAxis axis : axes) {
            if (axis.getAxisType() == AxisType.Lat) {
                nt.lat = axis.getShortName();
            }
            else if (axis.getAxisType() == AxisType.Lon) {
                nt.lon = axis.getShortName();
            }
            else if (axis.getAxisType() == AxisType.Time) {
                nt.time = axis.getShortName();
            }
            else {
                if (axis.getAxisType() != AxisType.Height) {
                    continue;
                }
                nt.elev = axis.getShortName();
            }
        }
    }
    
    public static void findCoordWithDimension(final TableConfig nt, final NetcdfDataset ds, final Dimension outer) {
        final CoordinateSystem use = findBestCoordinateSystem(ds);
        if (use == null) {
            return;
        }
        for (final CoordinateAxis axis : use.getCoordinateAxes()) {
            if (!outer.equals(axis.getDimension(0))) {
                continue;
            }
            if (axis.getAxisType() == AxisType.Lat) {
                nt.lat = axis.getShortName();
            }
            else if (axis.getAxisType() == AxisType.Lon) {
                nt.lon = axis.getShortName();
            }
            else if (axis.getAxisType() == AxisType.Time) {
                nt.time = axis.getShortName();
            }
            else {
                if (axis.getAxisType() != AxisType.Height) {
                    continue;
                }
                nt.elev = axis.getShortName();
            }
        }
    }
    
    public static String findCoordNameByType(final NetcdfDataset ds, final AxisType atype) {
        final CoordinateAxis coordAxis = findCoordByType(ds, atype);
        return (coordAxis == null) ? null : coordAxis.getName();
    }
    
    public static CoordinateAxis findCoordByType(final NetcdfDataset ds, final AxisType atype) {
        final CoordinateSystem use = findBestCoordinateSystem(ds);
        if (use != null) {
            for (final CoordinateAxis axis : use.getCoordinateAxes()) {
                if (axis.getAxisType() == atype) {
                    return axis;
                }
            }
        }
        for (final CoordinateAxis axis : ds.getCoordinateAxes()) {
            if (axis.getAxisType() == atype) {
                return axis;
            }
        }
        return null;
    }
    
    public static CoordinateAxis findCoordByType(final NetcdfDataset ds, final AxisType atype, final Predicate p) {
        final CoordinateSystem use = findBestCoordinateSystem(ds);
        if (use == null) {
            return null;
        }
        for (final CoordinateAxis axis : use.getCoordinateAxes()) {
            if (axis.getAxisType() == atype && p.match(axis)) {
                return axis;
            }
        }
        for (final CoordinateAxis axis : ds.getCoordinateAxes()) {
            if (axis.getAxisType() == atype && p.match(axis)) {
                return axis;
            }
        }
        return null;
    }
    
    public static Dimension findDimensionByType(final NetcdfDataset ds, final AxisType atype) {
        final CoordinateAxis axis = findCoordByType(ds, atype);
        if (axis == null) {
            return null;
        }
        if (axis.isScalar()) {
            return null;
        }
        return axis.getDimension(0);
    }
    
    private static CoordinateSystem findBestCoordinateSystem(final NetcdfDataset ds) {
        CoordinateSystem use = null;
        for (final CoordinateSystem cs : ds.getCoordinateSystems()) {
            if (use == null) {
                use = cs;
            }
            else {
                if (cs.getCoordinateAxes().size() <= use.getCoordinateAxes().size()) {
                    continue;
                }
                use = cs;
            }
        }
        return use;
    }
    
    public interface Predicate
    {
        boolean match(final CoordinateAxis p0);
    }
}
