// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard;

import ucar.nc2.Dimension;
import ucar.ma2.DataType;
import ucar.nc2.Structure;
import ucar.nc2.Variable;
import java.io.InputStream;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import ucar.nc2.util.IO;
import java.io.FileNotFoundException;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.dataset.VariableDS;
import ucar.nc2.dataset.NetcdfDataset;
import java.util.Iterator;
import java.util.List;
import java.util.Collection;
import java.util.ArrayList;
import org.jdom.Content;
import org.jdom.Element;
import org.jdom.Document;
import org.jdom.output.XMLOutputter;
import org.jdom.output.Format;
import java.io.IOException;
import java.util.Formatter;
import ucar.nc2.ft.FeatureDatasetPoint;

public class PointConfigXML
{
    private TableConfig tc;
    private String tableConfigurerClass;
    private static boolean debugXML;
    private static boolean debugURL;
    private static boolean showParsedXML;
    
    public static void writeConfigXML(final FeatureDatasetPoint pfd, final Formatter f) {
        if (!(pfd instanceof PointDatasetStandardFactory.PointDatasetStandard)) {
            f.format("%s not instance of PointDatasetStandard%n", pfd.getLocation());
            return;
        }
        final PointDatasetStandardFactory.PointDatasetStandard spfd = (PointDatasetStandardFactory.PointDatasetStandard)pfd;
        final TableAnalyzer analyser = spfd.getTableAnalyzer();
        final TableConfig config = analyser.getTableConfig();
        final TableConfigurer tc = analyser.getTableConfigurer();
        if (tc == null) {
            f.format("%s has no TableConfig%n", pfd.getLocation());
            return;
        }
        final PointConfigXML writer = new PointConfigXML();
        try {
            writer.writeConfigXML(config, tc.getClass().getName(), f);
        }
        catch (IOException e) {
            f.format("%s error writing=%s%n", pfd.getLocation(), e.getMessage());
        }
    }
    
    public void writeConfigXML(final TableConfig tc, final String tableConfigurerClass, final Formatter sf) throws IOException {
        this.tc = tc;
        this.tableConfigurerClass = tableConfigurerClass;
        final XMLOutputter fmt = new XMLOutputter(Format.getPrettyFormat());
        sf.format("%s", fmt.outputString(this.makeDocument()));
    }
    
    public Document makeDocument() {
        final Element rootElem = new Element("pointConfig");
        final Document doc = new Document(rootElem);
        if (this.tableConfigurerClass != null) {
            rootElem.addContent(new Element("tableConfigurer").setAttribute("class", this.tableConfigurerClass));
        }
        if (this.tc.featureType != null) {
            rootElem.setAttribute("featureType", this.tc.featureType.toString());
        }
        rootElem.addContent(this.writeTable(this.tc));
        return doc;
    }
    
    private Element writeTable(final TableConfig config) {
        final Element tableElem = new Element("table");
        if (config.type != null) {
            tableElem.setAttribute("type", config.type.toString());
        }
        switch (config.type) {
            case ArrayStructure: {
                tableElem.setAttribute("dimension", config.dimName);
            }
            case Contiguous: {
                if (config.start != null) {
                    tableElem.setAttribute("start", config.start);
                }
                tableElem.setAttribute("numRecords", config.numRecords);
                break;
            }
            case LinkedList: {
                tableElem.setAttribute("start", config.start);
                tableElem.setAttribute("next", config.next);
                break;
            }
            case MultidimInner: {
                tableElem.setAttribute("structName", config.structName);
            }
            case MultidimInnerPsuedo: {
                tableElem.setAttribute("dim0", config.outerName);
                tableElem.setAttribute("dim1", config.innerName);
                tableElem.setAttribute("subtype", config.structureType.toString());
            }
            case MultidimInner3D: {}
            case MultidimStructure: {
                tableElem.setAttribute("structName", config.structName);
                break;
            }
            case NestedStructure: {
                tableElem.setAttribute("structName", config.structName);
                break;
            }
            case ParentId:
            case ParentIndex: {
                tableElem.setAttribute("parentIndex", config.parentIndex);
            }
            case Structure: {
                tableElem.setAttribute("subtype", config.structureType.toString());
                switch (config.structureType) {
                    case Structure: {
                        tableElem.setAttribute("structName", config.structName);
                        break;
                    }
                    case PsuedoStructure: {
                        tableElem.setAttribute("dim", config.dimName);
                        break;
                    }
                    case PsuedoStructure2D: {
                        tableElem.setAttribute("dim0", config.dimName);
                        tableElem.setAttribute("dim1", config.outerName);
                        break;
                    }
                }
                break;
            }
            case Top: {
                tableElem.setAttribute("structName", config.structName);
                break;
            }
        }
        final List<String> varNames = (config.vars == null) ? new ArrayList<String>() : new ArrayList<String>(config.vars);
        for (final Table.CoordName coord : Table.CoordName.values()) {
            this.addCoord(tableElem, config, coord, varNames);
        }
        if (config.vars != null) {
            for (final String col : varNames) {
                tableElem.addContent(new Element("variable").addContent(col));
            }
        }
        if (config.extraJoin != null) {
            for (final Join j : config.extraJoin) {
                if (j instanceof JoinArray) {
                    tableElem.addContent(this.writeJoinArray((JoinArray)j));
                }
                else if (j instanceof JoinMuiltdimStructure) {
                    tableElem.addContent(this.writeJoinMuiltdimStructure((JoinMuiltdimStructure)j));
                }
                else {
                    if (!(j instanceof JoinParentIndex)) {
                        continue;
                    }
                    tableElem.addContent(this.writeJoinParentIndex((JoinParentIndex)j));
                }
            }
        }
        if (config.children != null) {
            for (final TableConfig child : config.children) {
                tableElem.addContent(this.writeTable(child));
            }
        }
        return tableElem;
    }
    
    private void addCoord(final Element tableElem, final TableConfig table, final Table.CoordName type, final List<String> varNames) {
        final String name = table.findCoordinateVariableName(type);
        if (name != null) {
            final Element elem = new Element("coordinate").setAttribute("type", type.name());
            elem.addContent(name);
            tableElem.addContent(elem);
            varNames.remove(name);
        }
    }
    
    private Element writeJoinArray(final JoinArray join) {
        final Element joinElem = new Element("join");
        joinElem.setAttribute("class", join.getClass().toString());
        if (join.type != null) {
            joinElem.setAttribute("type", join.type.toString());
        }
        if (join.v != null) {
            joinElem.addContent(new Element("variable").addContent(join.v.getName()));
        }
        joinElem.addContent(new Element("param").addContent(Integer.toString(join.param)));
        return joinElem;
    }
    
    private JoinArray readJoinArray(final NetcdfDataset ds, final Element joinElement) {
        final JoinArray.Type type = JoinArray.Type.valueOf(joinElement.getAttributeValue("type"));
        final Element paramElem = joinElement.getChild("param");
        final String paramS = paramElem.getText();
        final Integer param = Integer.parseInt(paramS);
        final Element varElem = joinElement.getChild("variable");
        final String varName = varElem.getText();
        final VariableDS v = (VariableDS)ds.findVariable(varName);
        return new JoinArray(v, type, param);
    }
    
    private Element writeJoinMuiltdimStructure(final JoinMuiltdimStructure join) {
        final Element joinElem = new Element("join");
        joinElem.setAttribute("class", join.getClass().toString());
        if (join.parentStructure != null) {
            joinElem.addContent(new Element("parentStructure").addContent(join.parentStructure.getName()));
        }
        joinElem.addContent(new Element("dimLength").setAttribute("value", Integer.toString(join.dimLength)));
        return joinElem;
    }
    
    private Element writeJoinParentIndex(final JoinParentIndex join) {
        final Element joinElem = new Element("join");
        joinElem.setAttribute("class", join.getClass().toString());
        if (join.parentStructure != null) {
            joinElem.addContent(new Element("parentStructure").addContent(join.parentStructure.getName()));
        }
        if (join.parentIndex != null) {
            joinElem.addContent(new Element("parentIndex").addContent(join.parentIndex));
        }
        return joinElem;
    }
    
    public TableConfig readConfigXMLfromResource(final String resourceLocation, final FeatureType wantFeatureType, final NetcdfDataset ds, final Formatter errlog) throws IOException {
        final ClassLoader cl = this.getClass().getClassLoader();
        final InputStream is = cl.getResourceAsStream(resourceLocation);
        if (is == null) {
            throw new FileNotFoundException(resourceLocation);
        }
        if (PointConfigXML.debugXML) {
            System.out.println(" PointConfig URL = <" + resourceLocation + ">");
            final InputStream is2 = cl.getResourceAsStream(resourceLocation);
            System.out.println(" contents=\n" + IO.readContents(is2));
        }
        Document doc;
        try {
            final SAXBuilder builder = new SAXBuilder(false);
            if (PointConfigXML.debugURL) {
                System.out.println(" PointConfig URL = <" + resourceLocation + ">");
            }
            doc = builder.build(is);
        }
        catch (JDOMException e) {
            throw new IOException(e.getMessage());
        }
        if (PointConfigXML.debugXML) {
            System.out.println(" SAXBuilder done");
        }
        if (PointConfigXML.showParsedXML) {
            final XMLOutputter xmlOut = new XMLOutputter();
            System.out.println("*** PointConfig/showParsedXML = \n" + xmlOut.outputString(doc) + "\n*******");
        }
        final Element configElem = doc.getRootElement();
        final String featureType = configElem.getAttributeValue("featureType");
        final Element tableElem = configElem.getChild("table");
        final TableConfig tc = this.parseTableConfig(ds, tableElem, null);
        tc.featureType = FeatureType.valueOf(featureType);
        return tc;
    }
    
    public TableConfig readConfigXML(final String fileLocation, final FeatureType wantFeatureType, final NetcdfDataset ds, final Formatter errlog) throws IOException {
        Document doc;
        try {
            final SAXBuilder builder = new SAXBuilder(false);
            if (PointConfigXML.debugURL) {
                System.out.println(" PointConfig URL = <" + fileLocation + ">");
            }
            doc = builder.build(fileLocation);
        }
        catch (JDOMException e) {
            throw new IOException(e.getMessage());
        }
        if (PointConfigXML.debugXML) {
            System.out.println(" SAXBuilder done");
        }
        if (PointConfigXML.showParsedXML) {
            final XMLOutputter xmlOut = new XMLOutputter();
            System.out.println("*** PointConfig/showParsedXML = \n" + xmlOut.outputString(doc) + "\n*******");
        }
        final Element configElem = doc.getRootElement();
        final String featureType = configElem.getAttributeValue("featureType");
        final Element tableElem = configElem.getChild("table");
        final TableConfig tc = this.parseTableConfig(ds, tableElem, null);
        tc.featureType = FeatureType.valueOf(featureType);
        return tc;
    }
    
    private TableConfig parseTableConfig(final NetcdfDataset ds, final Element tableElem, final TableConfig parent) {
        final String typeS = tableElem.getAttributeValue("type");
        final Table.Type ttype = Table.Type.valueOf(typeS);
        final String name = tableElem.getAttributeValue("name");
        final TableConfig tc = new TableConfig(ttype, name);
        switch (ttype) {
            case ArrayStructure: {
                tc.dimName = tableElem.getAttributeValue("dimension");
            }
            case Contiguous: {
                tc.numRecords = tableElem.getAttributeValue("numRecords");
                tc.start = tableElem.getAttributeValue("start");
                break;
            }
            case LinkedList: {
                tc.start = tableElem.getAttributeValue("start");
                tc.next = tableElem.getAttributeValue("next");
                break;
            }
            case MultidimInner: {
                tc.structName = tableElem.getAttributeValue("structName");
            }
            case MultidimInnerPsuedo: {
                tc.outerName = tableElem.getAttributeValue("dim0");
                tc.dimName = tc.outerName;
                tc.innerName = tableElem.getAttributeValue("dim1");
                tc.structureType = TableConfig.StructureType.valueOf(tableElem.getAttributeValue("subtype"));
                this.makeMultidimInner(ds, parent, tc);
            }
            case MultidimInner3D: {}
            case MultidimStructure: {
                tc.structName = tableElem.getAttributeValue("structName");
                break;
            }
            case NestedStructure: {
                tc.structName = tableElem.getAttributeValue("structName");
                break;
            }
            case ParentId:
            case ParentIndex: {
                tc.parentIndex = tableElem.getAttributeValue("parentIndex");
            }
            case Structure: {
                tc.structureType = TableConfig.StructureType.valueOf(tableElem.getAttributeValue("subtype"));
                switch (tc.structureType) {
                    case Structure: {
                        tc.structName = tableElem.getAttributeValue("structName");
                        break;
                    }
                    case PsuedoStructure: {
                        tc.dimName = tableElem.getAttributeValue("dim");
                        break;
                    }
                    case PsuedoStructure2D: {
                        tc.dimName = tableElem.getAttributeValue("dim0");
                        tc.outerName = tableElem.getAttributeValue("dim1");
                        break;
                    }
                }
                break;
            }
            case Top: {
                tc.structName = tableElem.getAttributeValue("structName");
                break;
            }
        }
        final List<Element> coordList = (List<Element>)tableElem.getChildren("coordinate");
        for (final Element coordElem : coordList) {
            final String coordNameType = coordElem.getAttributeValue("type");
            final Table.CoordName coordName = Table.CoordName.valueOf(coordNameType);
            tc.setCoordinateVariableName(coordName, coordElem.getText());
        }
        final List<Element> joinList = (List<Element>)tableElem.getChildren("join");
        for (final Element joinElem : joinList) {
            tc.addJoin(this.readJoinArray(ds, joinElem));
        }
        final List<Element> nestedTableList = (List<Element>)tableElem.getChildren("table");
        for (final Element nestedTable : nestedTableList) {
            tc.addChild(this.parseTableConfig(ds, nestedTable, tc));
        }
        return tc;
    }
    
    private void makeMultidimInner(final NetcdfDataset ds, final TableConfig parentTable, final TableConfig childTable) {
        final Dimension parentDim = ds.findDimension(parentTable.dimName);
        final Dimension childDim = ds.findDimension(childTable.innerName);
        List<String> obsVars = null;
        final List<Variable> vars = ds.getVariables();
        final List<String> parentVars = new ArrayList<String>(vars.size());
        obsVars = new ArrayList<String>(vars.size());
        for (final Variable orgV : vars) {
            if (orgV instanceof Structure) {
                continue;
            }
            final Dimension dim0 = orgV.getDimension(0);
            if (dim0 == null || !dim0.equals(parentDim)) {
                continue;
            }
            if (orgV.getRank() == 1 || (orgV.getRank() == 2 && orgV.getDataType() == DataType.CHAR)) {
                parentVars.add(orgV.getShortName());
            }
            else {
                final Dimension dim2 = orgV.getDimension(1);
                if (dim2 == null || !dim2.equals(childDim)) {
                    continue;
                }
                obsVars.add(orgV.getShortName());
            }
        }
        parentTable.vars = parentVars;
        childTable.vars = obsVars;
    }
    
    static {
        PointConfigXML.debugXML = false;
        PointConfigXML.debugURL = false;
        PointConfigXML.showParsedXML = false;
    }
}
