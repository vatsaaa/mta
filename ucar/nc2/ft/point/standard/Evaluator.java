// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard;

import ucar.ma2.DataType;
import ucar.nc2.Dimension;
import ucar.nc2.Attribute;
import ucar.nc2.Structure;
import java.util.Iterator;
import ucar.nc2.constants.FeatureType;
import java.util.Formatter;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.Variable;

public class Evaluator
{
    private String constant;
    private Variable v;
    
    public static FeatureType getFeatureType(final NetcdfDataset ds, final String key, final Formatter errlog) {
        FeatureType ft = null;
        final String fts = getLiteral(ds, key, errlog);
        if (fts != null) {
            ft = FeatureType.valueOf(fts.toUpperCase());
            if (ft == null && errlog != null) {
                errlog.format(" Cant find Feature type %s from %s\n", fts, key);
            }
        }
        return ft;
    }
    
    public static String getLiteral(final NetcdfDataset ds, final String key, final Formatter errlog) {
        if (key.startsWith(":")) {
            final String val = ds.findAttValueIgnoreCase(null, key.substring(1), null);
            if (val == null && errlog != null) {
                errlog.format(" Cant find global attribute %s\n", key);
            }
            return val;
        }
        return key;
    }
    
    public static String getVariableName(final NetcdfDataset ds, final String key, final Formatter errlog) {
        Variable v = null;
        final String vs = getLiteral(ds, key, errlog);
        if (vs != null) {
            v = ds.findVariable(vs);
            if (v == null && errlog != null) {
                errlog.format(" Cant find Variable %s from %s\n", vs, key);
            }
        }
        return (v == null) ? null : v.getShortName();
    }
    
    public static Variable getVariableWithAttribute(final NetcdfDataset ds, final String attName) {
        for (final Variable v : ds.getVariables()) {
            final String stdName = ds.findAttValueIgnoreCase(v, attName, null);
            if (stdName != null) {
                return v;
            }
        }
        return null;
    }
    
    public static Variable getVariableWithAttributeValue(final NetcdfDataset ds, final String attName, final String attValue) {
        for (final Variable v : ds.getVariables()) {
            final String haveValue = ds.findAttValueIgnoreCase(v, attName, null);
            if (haveValue != null && haveValue.equals(attValue)) {
                return v;
            }
        }
        return null;
    }
    
    public static String getVariableAttributeValue(final NetcdfDataset ds, final String attName) {
        for (final Variable v : ds.getVariables()) {
            final String haveValue = ds.findAttValueIgnoreCase(v, attName, null);
            if (haveValue != null) {
                return haveValue;
            }
        }
        return null;
    }
    
    public static String getNameOfVariableWithAttribute(final NetcdfDataset ds, final String attName, final String attValue) {
        final Variable v = getVariableWithAttributeValue(ds, attName, attValue);
        return (v == null) ? null : v.getShortName();
    }
    
    public static String getNameOfVariableWithAttribute(final Structure struct, final String attName, final String attValue) {
        for (final Variable v : struct.getVariables()) {
            final Attribute att = v.findAttributeIgnoreCase(attName);
            if (att != null && att.getStringValue().equals(attValue)) {
                return v.getShortName();
            }
        }
        return null;
    }
    
    public static String getDimensionName(final NetcdfDataset ds, final String key, final Formatter errlog) {
        final Dimension d = getDimension(ds, key, errlog);
        return (d == null) ? null : d.getName();
    }
    
    public static Dimension getDimension(final NetcdfDataset ds, final String key, final Formatter errlog) {
        Dimension d = null;
        final String s = getLiteral(ds, key, errlog);
        if (s != null) {
            d = ds.findDimension(s);
            if (d == null && errlog != null) {
                errlog.format(" Cant find Variable %s from %s\n", s, key);
            }
        }
        return d;
    }
    
    public static Structure getStructureWithDimensions(final NetcdfDataset ds, final Dimension dim0, final Dimension dim1) {
        for (final Variable v : ds.getVariables()) {
            if (v instanceof Structure && v.getRank() == 2 && v.getDimension(0).equals(dim0) && v.getDimension(1).equals(dim1)) {
                return (Structure)v;
            }
        }
        return null;
    }
    
    public static Structure getNestedStructure(final Structure s) {
        for (final Variable v : s.getVariables()) {
            if (v instanceof Structure) {
                return (Structure)v;
            }
        }
        return null;
    }
    
    public static boolean hasRecordStructure(final NetcdfDataset ds) {
        final Variable v = ds.findVariable("record");
        return v != null && v.getDataType() == DataType.STRUCTURE;
    }
    
    Evaluator(final Variable v) {
        this.v = v;
    }
    
    Evaluator(final String constant) {
        this.constant = constant;
    }
    
    public String getValue() {
        if (this.constant != null) {
            return this.constant;
        }
        return null;
    }
}
