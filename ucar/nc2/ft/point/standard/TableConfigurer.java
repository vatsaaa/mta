// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard;

import java.util.Formatter;
import java.io.IOException;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.constants.FeatureType;

public interface TableConfigurer
{
    boolean isMine(final FeatureType p0, final NetcdfDataset p1) throws IOException;
    
    TableConfig getConfig(final FeatureType p0, final NetcdfDataset p1, final Formatter p2) throws IOException;
    
    String getConvName();
    
    void setConvName(final String p0);
    
    String getConvUsed();
    
    void setConvUsed(final String p0);
}
