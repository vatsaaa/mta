// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard.plug;

import java.util.List;
import ucar.nc2.Dimension;
import ucar.ma2.DataType;
import ucar.nc2.Structure;
import java.util.ArrayList;
import java.io.IOException;
import java.util.Iterator;
import ucar.nc2.ft.point.standard.PointConfigXML;
import ucar.nc2.ft.point.standard.TableConfig;
import java.util.Formatter;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.ft.point.standard.TableConfigurerImpl;

public class FslWindProfiler extends TableConfigurerImpl
{
    public boolean isMine(final FeatureType wantFeatureType, final NetcdfDataset ds) {
        String title = ds.findAttValueIgnoreCase(null, "title", null);
        if (title == null) {
            title = ds.findAttValueIgnoreCase(null, "DD_reference", null);
            if (title != null) {
                title = ((ds.findVariable("staLat") != null) ? title : null);
            }
        }
        return title != null && (title.startsWith("WPDN data") || title.startsWith("RASS data") || title.contains("88-21-R2"));
    }
    
    public TableConfig getConfig(final FeatureType wantFeatureType, final NetcdfDataset ds, final Formatter errlog) throws IOException {
        String title = ds.findAttValueIgnoreCase(null, "title", null);
        if (title == null) {
            title = ds.findAttValueIgnoreCase(null, "DD_reference", null);
        }
        final boolean isRass = title.startsWith("RASS data");
        final String xml = isRass ? "resources/nj22/pointConfig/FslRassProfiler.xml" : "resources/nj22/pointConfig/FslWindProfiler.xml";
        final PointConfigXML reader = new PointConfigXML();
        final TableConfig tc = reader.readConfigXMLfromResource(xml, wantFeatureType, ds, errlog);
        for (final TableConfig inner : tc.children.get(0).children) {
            this.makeMultidimInner(ds, tc, inner, inner.outerName, inner.innerName);
        }
        return tc;
    }
    
    private void makeMultidimInner(final NetcdfDataset ds, final TableConfig parentTable, final TableConfig childTable, final String outerDin, final String innerDim) {
        final Dimension parentDim = ds.findDimension(outerDin);
        final Dimension childDim = ds.findDimension(innerDim);
        List<String> obsVars = null;
        final List<Variable> vars = ds.getVariables();
        final List<String> parentVars = new ArrayList<String>(vars.size());
        obsVars = new ArrayList<String>(vars.size());
        for (final Variable orgV : vars) {
            if (orgV instanceof Structure) {
                continue;
            }
            final Dimension dim0 = orgV.getDimension(0);
            if (dim0 == null || !dim0.equals(parentDim)) {
                continue;
            }
            if (orgV.getRank() == 1 || (orgV.getRank() == 2 && orgV.getDataType() == DataType.CHAR)) {
                parentVars.add(orgV.getShortName());
            }
            else {
                final Dimension dim2 = orgV.getDimension(1);
                if (dim2 == null || !dim2.equals(childDim)) {
                    continue;
                }
                obsVars.add(orgV.getShortName());
            }
        }
        parentTable.vars = parentVars;
        childTable.vars = obsVars;
    }
}
