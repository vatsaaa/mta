// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard.plug;

import ucar.nc2.ft.point.standard.Evaluator;
import ucar.nc2.constants.AxisType;
import ucar.nc2.Structure;
import ucar.nc2.ft.point.standard.Table;
import ucar.nc2.constants.CF;
import ucar.nc2.ft.point.standard.TableConfig;
import java.util.Formatter;
import java.util.StringTokenizer;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.ft.point.standard.TableConfigurerImpl;

public class BufrCdm extends TableConfigurerImpl
{
    private final String BufrConvention = "BUFR/CDM";
    
    public boolean isMine(final FeatureType wantFeatureType, final NetcdfDataset ds) {
        final String conv = ds.findAttValueIgnoreCase(null, "Conventions", null);
        if (conv == null) {
            return false;
        }
        if (conv.equals("BUFR/CDM")) {
            return true;
        }
        final StringTokenizer stoke = new StringTokenizer(conv, ",");
        while (stoke.hasMoreTokens()) {
            final String toke = stoke.nextToken().trim();
            if (toke.equals("BUFR/CDM")) {
                return true;
            }
        }
        return false;
    }
    
    public TableConfig getConfig(final FeatureType wantFeatureType, final NetcdfDataset ds, final Formatter errlog) {
        final String ftypeS = ds.findAttValueIgnoreCase(null, "CF:featureType", null);
        final CF.FeatureType ftype = (ftypeS == null) ? CF.FeatureType.point : CF.FeatureType.getFeatureType(ftypeS);
        switch (ftype) {
            case point: {
                return this.getPointConfig(ds, errlog);
            }
            case timeSeries: {
                return this.getStationConfig(ds, errlog);
            }
            case profile: {
                return this.getProfileConfig(ds, errlog);
            }
            case trajectory: {
                return this.getTrajectoryConfig(ds, errlog);
            }
            case timeSeriesProfile: {
                return this.getStationProfileConfig(ds, errlog);
            }
            default: {
                throw new IllegalStateException("invalid ftype= " + ftype);
            }
        }
    }
    
    protected TableConfig getPointConfig(final NetcdfDataset ds, final Formatter errlog) {
        final TableConfig obsTable = new TableConfig(Table.Type.Structure, "obs");
        final Structure obsStruct = (Structure)ds.findVariable("obs");
        obsTable.structName = obsStruct.getName();
        obsTable.nestedTableName = obsStruct.getShortName();
        obsTable.lat = Evaluator.getNameOfVariableWithAttribute(obsStruct, "_CoordinateAxisType", AxisType.Lat.toString());
        obsTable.lon = Evaluator.getNameOfVariableWithAttribute(obsStruct, "_CoordinateAxisType", AxisType.Lon.toString());
        obsTable.elev = Evaluator.getNameOfVariableWithAttribute(obsStruct, "_CoordinateAxisType", AxisType.Height.toString());
        obsTable.time = Evaluator.getNameOfVariableWithAttribute(obsStruct, "_CoordinateAxisType", AxisType.Time.toString());
        return obsTable;
    }
    
    protected TableConfig getStationConfig(final NetcdfDataset ds, final Formatter errlog) {
        final TableConfig stnTable = new TableConfig(Table.Type.Construct, "station");
        stnTable.featureType = FeatureType.STATION;
        stnTable.structName = "obs";
        final TableConfig timeTable = new TableConfig(Table.Type.ParentId, "obs");
        final Structure stnStruct = (Structure)ds.findVariable("obs");
        timeTable.lat = Evaluator.getNameOfVariableWithAttribute(stnStruct, "_CoordinateAxisType", AxisType.Lat.toString());
        timeTable.lon = Evaluator.getNameOfVariableWithAttribute(stnStruct, "_CoordinateAxisType", AxisType.Lon.toString());
        timeTable.stnAlt = Evaluator.getNameOfVariableWithAttribute(stnStruct, "_CoordinateAxisType", AxisType.Height.toString());
        timeTable.stnId = Evaluator.getNameOfVariableWithAttribute(stnStruct, "standard_name", "station_id");
        timeTable.stnWmoId = Evaluator.getNameOfVariableWithAttribute(stnStruct, "standard_name", "station_WMO_id");
        if (timeTable.stnId == null) {
            timeTable.stnId = timeTable.stnWmoId;
        }
        timeTable.parentIndex = timeTable.stnId;
        timeTable.time = Evaluator.getNameOfVariableWithAttribute(stnStruct, "_CoordinateAxisType", AxisType.Time.toString());
        timeTable.structName = "obs";
        stnTable.addChild(timeTable);
        return stnTable;
    }
    
    protected TableConfig getTrajectoryConfig(final NetcdfDataset ds, final Formatter errlog) {
        final TableConfig topTable = new TableConfig(Table.Type.Top, "singleTrajectory");
        final TableConfig obsTable = new TableConfig(Table.Type.Structure, "obs");
        final Structure obsStruct = (Structure)ds.findVariable("obs");
        obsTable.structName = obsStruct.getName();
        obsTable.nestedTableName = obsStruct.getShortName();
        obsTable.lat = Evaluator.getNameOfVariableWithAttribute(obsStruct, "_CoordinateAxisType", AxisType.Lat.toString());
        obsTable.lon = Evaluator.getNameOfVariableWithAttribute(obsStruct, "_CoordinateAxisType", AxisType.Lon.toString());
        obsTable.elev = Evaluator.getNameOfVariableWithAttribute(obsStruct, "_CoordinateAxisType", AxisType.Height.toString());
        obsTable.time = Evaluator.getNameOfVariableWithAttribute(obsStruct, "_CoordinateAxisType", AxisType.Time.toString());
        topTable.addChild(obsTable);
        return topTable;
    }
    
    protected TableConfig getProfileConfig(final NetcdfDataset ds, final Formatter errlog) {
        final TableConfig profileTable = new TableConfig(Table.Type.Structure, "profile");
        profileTable.featureType = FeatureType.PROFILE;
        profileTable.structName = "obs";
        final Structure profileStruct = (Structure)ds.findVariable("obs");
        profileTable.lat = Evaluator.getNameOfVariableWithAttribute(profileStruct, "_CoordinateAxisType", AxisType.Lat.toString());
        profileTable.lon = Evaluator.getNameOfVariableWithAttribute(profileStruct, "_CoordinateAxisType", AxisType.Lon.toString());
        profileTable.time = Evaluator.getNameOfVariableWithAttribute(profileStruct, "_CoordinateAxisType", AxisType.Time.toString());
        final TableConfig obsTable = new TableConfig(Table.Type.NestedStructure, "struct5");
        final Structure obsStruct = (Structure)profileStruct.findVariable("struct5");
        obsTable.structName = obsStruct.getName();
        obsTable.nestedTableName = obsStruct.getShortName();
        obsTable.elev = Evaluator.getNameOfVariableWithAttribute(obsStruct, "_CoordinateAxisType", AxisType.Pressure.toString());
        profileTable.addChild(obsTable);
        return profileTable;
    }
    
    protected TableConfig getStationProfileConfig(final NetcdfDataset ds, final Formatter errlog) {
        final TableConfig stnTable = new TableConfig(Table.Type.Construct, "station");
        stnTable.featureType = FeatureType.STATION_PROFILE;
        stnTable.structName = "obs";
        final TableConfig timeTable = new TableConfig(Table.Type.ParentId, "obs");
        final Structure stnStruct = (Structure)ds.findVariable("obs");
        timeTable.lat = Evaluator.getNameOfVariableWithAttribute(stnStruct, "_CoordinateAxisType", AxisType.Lat.toString());
        timeTable.lon = Evaluator.getNameOfVariableWithAttribute(stnStruct, "_CoordinateAxisType", AxisType.Lon.toString());
        timeTable.stnAlt = Evaluator.getNameOfVariableWithAttribute(stnStruct, "_CoordinateAxisType", AxisType.Height.toString());
        timeTable.stnId = Evaluator.getNameOfVariableWithAttribute(stnStruct, "standard_name", "station_id");
        timeTable.stnWmoId = Evaluator.getNameOfVariableWithAttribute(stnStruct, "standard_name", "station_WMO_id");
        if (timeTable.stnId == null) {
            timeTable.stnId = timeTable.stnWmoId;
        }
        timeTable.parentIndex = timeTable.stnId;
        timeTable.time = Evaluator.getNameOfVariableWithAttribute(stnStruct, "_CoordinateAxisType", AxisType.Time.toString());
        timeTable.structName = "obs";
        stnTable.addChild(timeTable);
        final TableConfig obsTable = new TableConfig(Table.Type.NestedStructure, "levels");
        Structure obsStruct = (Structure)stnStruct.findVariable("seq1");
        if (obsStruct == null) {
            obsStruct = (Structure)stnStruct.findVariable("struct1");
        }
        obsTable.structName = obsStruct.getName();
        obsTable.nestedTableName = obsStruct.getShortName();
        obsTable.elev = Evaluator.getNameOfVariableWithAttribute(obsStruct, "_CoordinateAxisType", AxisType.Height.toString());
        timeTable.addChild(obsTable);
        return stnTable;
    }
}
