// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard.plug;

import ucar.nc2.ft.point.standard.Evaluator;
import ucar.nc2.ft.point.standard.Table;
import ucar.nc2.ft.point.standard.TableConfig;
import java.util.Formatter;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.constants.FeatureType;

public class MadisAcars extends Madis
{
    private static final String TRAJ_ID = "en_tailNumber";
    
    @Override
    public boolean isMine(final FeatureType wantFeatureType, final NetcdfDataset ds) {
        if (wantFeatureType != FeatureType.ANY_POINT && wantFeatureType != FeatureType.TRAJECTORY) {
            return false;
        }
        final String title = ds.findAttValueIgnoreCase(null, "title", null);
        if (title == null || !title.equals("MADIS ACARS data")) {
            return false;
        }
        if (!ds.hasUnlimitedDimension()) {
            return false;
        }
        if (ds.findDimension("recNum") == null) {
            return false;
        }
        final VNames vn = this.getVariableNames(ds, null);
        return ds.findVariable(vn.lat) != null && ds.findVariable(vn.lon) != null && ds.findVariable(vn.obsTime) != null && ds.findVariable("en_tailNumber") != null;
    }
    
    @Override
    public TableConfig getConfig(final FeatureType wantFeatureType, final NetcdfDataset ds, final Formatter errlog) {
        final VNames vn = this.getVariableNames(ds, errlog);
        final TableConfig trajTable = new TableConfig(Table.Type.Construct, "trajectory");
        trajTable.featureType = FeatureType.TRAJECTORY;
        trajTable.feature_id = "en_tailNumber";
        final TableConfig obs = new TableConfig(Table.Type.ParentId, "record");
        obs.parentIndex = "en_tailNumber";
        obs.dimName = Evaluator.getDimensionName(ds, "recNum", errlog);
        obs.time = vn.obsTime;
        obs.timeNominal = vn.nominalTime;
        obs.lat = vn.lat;
        obs.lon = vn.lon;
        obs.elev = vn.elev;
        trajTable.addChild(obs);
        return trajTable;
    }
}
