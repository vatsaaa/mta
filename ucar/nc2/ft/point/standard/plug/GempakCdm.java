// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard.plug;

import ucar.nc2.dataset.StructureDS;
import java.util.Iterator;
import java.util.List;
import ucar.nc2.ft.point.standard.JoinMuiltdimStructure;
import ucar.nc2.Group;
import ucar.nc2.dataset.StructurePseudoDS;
import ucar.ma2.DataType;
import java.util.ArrayList;
import ucar.nc2.Structure;
import ucar.nc2.dataset.VariableDS;
import ucar.nc2.Dimension;
import ucar.nc2.ft.point.standard.Join;
import ucar.nc2.ft.point.standard.JoinArray;
import ucar.nc2.ft.point.standard.Table;
import ucar.nc2.ft.point.standard.Evaluator;
import ucar.nc2.ft.point.standard.CoordSysEvaluator;
import ucar.nc2.constants.AxisType;
import java.io.IOException;
import ucar.nc2.ft.point.standard.TableConfig;
import java.util.Formatter;
import ucar.nc2.constants.CF;
import java.util.StringTokenizer;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.ft.point.standard.TableConfigurerImpl;

public class GempakCdm extends TableConfigurerImpl
{
    private final String Convention = "GEMPAK/CDM";
    
    public boolean isMine(final FeatureType wantFeatureType, final NetcdfDataset ds) {
        boolean ok = false;
        final String conv = ds.findAttValueIgnoreCase(null, "Conventions", null);
        if (conv == null) {
            return false;
        }
        if (conv.equals("GEMPAK/CDM")) {
            ok = true;
        }
        final StringTokenizer stoke = new StringTokenizer(conv, ",");
        while (stoke.hasMoreTokens()) {
            final String toke = stoke.nextToken().trim();
            if (toke.equals("GEMPAK/CDM")) {
                ok = true;
            }
        }
        if (!ok) {
            return false;
        }
        final String ftypeS = ds.findAttValueIgnoreCase(null, "CF:featureType", null);
        final CF.FeatureType ftype = (ftypeS == null) ? CF.FeatureType.point : CF.FeatureType.getFeatureType(ftypeS);
        return ftype == CF.FeatureType.timeSeries || ftype == CF.FeatureType.timeSeriesProfile;
    }
    
    public TableConfig getConfig(final FeatureType wantFeatureType, final NetcdfDataset ds, final Formatter errlog) throws IOException {
        final String ftypeS = ds.findAttValueIgnoreCase(null, "CF:featureType", null);
        final CF.FeatureType ftype = (ftypeS == null) ? CF.FeatureType.point : CF.FeatureType.getFeatureType(ftypeS);
        switch (ftype) {
            case point: {
                return null;
            }
            case timeSeries: {
                if (wantFeatureType == FeatureType.POINT) {
                    return this.getStationAsPointConfig(ds, errlog);
                }
                return this.getStationConfig(ds, errlog);
            }
            case timeSeriesProfile: {
                return this.getStationProfileConfig(ds, errlog);
            }
            default: {
                throw new IllegalStateException("unimplemented feature ftype= " + ftype);
            }
        }
    }
    
    protected TableConfig getStationConfig(final NetcdfDataset ds, final Formatter errlog) throws IOException {
        final boolean needFinish = false;
        final Variable lat = CoordSysEvaluator.findCoordByType(ds, AxisType.Lat);
        if (lat == null) {
            errlog.format("GempakCdm: Must have a Latitude coordinate", new Object[0]);
            return null;
        }
        final Variable lon = CoordSysEvaluator.findCoordByType(ds, AxisType.Lon);
        if (lon == null) {
            errlog.format("GempakCdm: Must have a Longitude coordinate", new Object[0]);
            return null;
        }
        if (lat.getRank() != lon.getRank()) {
            errlog.format("GempakCdm: Lat and Lon coordinate must have same rank", new Object[0]);
            return null;
        }
        final boolean stnIsScalar = lat.getRank() == 0;
        final boolean stnIsSingle = lat.getRank() == 1 && lat.getSize() == 1L;
        Dimension stationDim = null;
        if (!stnIsScalar) {
            if (lat.getDimension(0) != lon.getDimension(0)) {
                errlog.format("GempakCdm: Lat and Lon coordinate must have same size", new Object[0]);
                return null;
            }
            stationDim = lat.getDimension(0);
        }
        final boolean hasStruct = Evaluator.hasRecordStructure(ds);
        final Table.Type stationTableType = stnIsScalar ? Table.Type.Top : Table.Type.Structure;
        final TableConfig stnTable = new TableConfig(stationTableType, "station");
        stnTable.featureType = FeatureType.STATION;
        stnTable.structureType = (hasStruct ? TableConfig.StructureType.Structure : TableConfig.StructureType.PsuedoStructure);
        stnTable.dimName = stationDim.getName();
        stnTable.lat = lat.getName();
        stnTable.lon = lon.getName();
        final Variable alt = CoordSysEvaluator.findCoordByType(ds, AxisType.Height);
        if (alt != null) {
            stnTable.stnAlt = alt.getName();
        }
        stnTable.stnId = Evaluator.getNameOfVariableWithAttribute(ds, "standard_name", "station_id");
        if (stnTable.stnId == null) {
            errlog.format("Must have a Station id variable with standard name station_id", new Object[0]);
            return null;
        }
        final Variable stnId = ds.findVariable(stnTable.stnId);
        if (!stnIsScalar && !stnId.getDimension(0).equals(stationDim)) {
            errlog.format("GempakCdm: Station id (%s) outer dimension must match latitude/longitude dimension (%s)", stnTable.stnId, stationDim);
            return null;
        }
        final VariableDS time = CoordSysEvaluator.findCoordByType(ds, AxisType.Time);
        if (time == null) {
            errlog.format("GempakCdm: Must have a Time coordinate", new Object[0]);
            return null;
        }
        final Dimension obsDim = time.getDimension(time.getRank() - 1);
        Table.Type obsTableType = null;
        Structure multidimStruct = null;
        if (obsTableType == null) {
            multidimStruct = Evaluator.getStructureWithDimensions(ds, stationDim, obsDim);
            if (multidimStruct != null) {
                obsTableType = Table.Type.MultidimStructure;
            }
        }
        if (obsTableType == null && time.getRank() == 2) {
            obsTableType = Table.Type.MultidimInner;
        }
        if (obsTableType == null) {
            errlog.format("GempakCdm: Cannot figure out Station/obs table structure", new Object[0]);
            return null;
        }
        final TableConfig obs = new TableConfig(obsTableType, obsDim.getName());
        obs.dimName = obsDim.getName();
        obs.time = time.getName();
        obs.missingVar = "_isMissing";
        stnTable.addChild(obs);
        if (obsTableType == Table.Type.MultidimStructure) {
            obs.structName = multidimStruct.getName();
            obs.structureType = TableConfig.StructureType.Structure;
            if (multidimStruct.findVariable(time.getShortName()) == null) {
                obs.addJoin(new JoinArray(time, JoinArray.Type.raw, 0));
            }
        }
        if (obsTableType == Table.Type.MultidimInner) {
            obs.dimName = obsDim.getName();
        }
        if (needFinish) {
            ds.finish();
        }
        return stnTable;
    }
    
    protected TableConfig getStationAsPointConfig(final NetcdfDataset ds, final Formatter errlog) throws IOException {
        final boolean needFinish = false;
        final Variable lat = CoordSysEvaluator.findCoordByType(ds, AxisType.Lat);
        if (lat == null) {
            errlog.format("GempakCdm: Must have a Latitude coordinate", new Object[0]);
            return null;
        }
        final Variable lon = CoordSysEvaluator.findCoordByType(ds, AxisType.Lon);
        if (lon == null) {
            errlog.format("GempakCdm: Must have a Longitude coordinate", new Object[0]);
            return null;
        }
        if (lat.getRank() != lon.getRank()) {
            errlog.format("GempakCdm: Lat and Lon coordinate must have same rank", new Object[0]);
            return null;
        }
        final boolean stnIsScalar = lat.getRank() == 0;
        final boolean stnIsSingle = lat.getRank() == 1 && lat.getSize() == 1L;
        Dimension stationDim = null;
        if (!stnIsScalar) {
            if (lat.getDimension(0) != lon.getDimension(0)) {
                errlog.format("Lat and Lon coordinate must have same size", new Object[0]);
                return null;
            }
            stationDim = lat.getDimension(0);
        }
        final Variable alt = CoordSysEvaluator.findCoordByType(ds, AxisType.Height);
        final VariableDS time = CoordSysEvaluator.findCoordByType(ds, AxisType.Time);
        if (time == null) {
            errlog.format("GempakCdm: Must have a Time coordinate", new Object[0]);
            return null;
        }
        final Dimension obsDim = time.getDimension(time.getRank() - 1);
        final Table.Type obsTableType = Table.Type.Structure;
        final Structure multidimStruct = Evaluator.getStructureWithDimensions(ds, stationDim, obsDim);
        if (multidimStruct == null) {
            errlog.format("GempakCdm: Cannot figure out StationAsPoint table structure", new Object[0]);
            return null;
        }
        final TableConfig obs = new TableConfig(obsTableType, obsDim.getName());
        obs.dimName = obsDim.getName();
        obs.structName = multidimStruct.getName();
        obs.structureType = TableConfig.StructureType.Structure;
        obs.featureType = FeatureType.POINT;
        obs.lat = lat.getName();
        obs.lon = lon.getName();
        obs.time = time.getName();
        if (alt != null) {
            obs.elev = alt.getName();
        }
        final List<String> vars = new ArrayList<String>(30);
        for (final Variable v : ds.getVariables()) {
            if (v.getDimension(0) == stationDim && (v.getRank() == 1 || (v.getRank() == 2 && v.getDataType() == DataType.CHAR))) {
                vars.add(v.getShortName());
            }
        }
        final StructureDS s = new StructurePseudoDS(ds, null, "stnStruct", vars, stationDim);
        obs.addJoin(new JoinMuiltdimStructure(s, obsDim.getLength()));
        obs.addJoin(new JoinArray(time, JoinArray.Type.modulo, obsDim.getLength()));
        if (needFinish) {
            ds.finish();
        }
        return obs;
    }
    
    protected TableConfig getStationProfileConfig(final NetcdfDataset ds, final Formatter errlog) throws IOException {
        final TableConfig stnTable = this.makeStationTable(ds, errlog);
        if (stnTable == null) {
            return null;
        }
        final Dimension stationDim = ds.findDimension(stnTable.dimName);
        stnTable.featureType = FeatureType.STATION_PROFILE;
        final VariableDS time = CoordSysEvaluator.findCoordByType(ds, AxisType.Time);
        if (time == null) {
            errlog.format("GempakCdm: Must have a Time coordinate", new Object[0]);
            return null;
        }
        final Dimension obsDim = time.getDimension(time.getRank() - 1);
        final Structure multidimStruct = Evaluator.getStructureWithDimensions(ds, stationDim, obsDim);
        if (multidimStruct == null) {
            errlog.format("GempakCdm: Cannot figure out Station/obs table structure", new Object[0]);
            return null;
        }
        final TableConfig timeTable = new TableConfig(Table.Type.MultidimStructure, obsDim.getName());
        timeTable.missingVar = "_isMissing";
        timeTable.structName = multidimStruct.getName();
        timeTable.structureType = TableConfig.StructureType.Structure;
        timeTable.addJoin(new JoinArray(time, JoinArray.Type.level, 1));
        timeTable.time = time.getName();
        timeTable.feature_id = time.getName();
        stnTable.addChild(timeTable);
        final TableConfig obsTable = new TableConfig(Table.Type.NestedStructure, obsDim.getName());
        final Structure nestedStruct = Evaluator.getNestedStructure(multidimStruct);
        if (nestedStruct == null) {
            errlog.format("GempakCdm: Cannot find nested Structure for profile", new Object[0]);
            return null;
        }
        obsTable.structName = nestedStruct.getName();
        obsTable.nestedTableName = nestedStruct.getShortName();
        final Variable elev = this.findZAxisNotStationAlt(ds);
        if (elev == null) {
            errlog.format("GempakCdm: Cannot find profile elevation variable", new Object[0]);
            return null;
        }
        obsTable.elev = elev.getShortName();
        timeTable.addChild(obsTable);
        return stnTable;
    }
    
    protected TableConfig makeStationTable(final NetcdfDataset ds, final Formatter errlog) throws IOException {
        final Variable lat = CoordSysEvaluator.findCoordByType(ds, AxisType.Lat);
        if (lat == null) {
            errlog.format("GempakCdm: Must have a Latitude coordinate", new Object[0]);
            return null;
        }
        final Variable lon = CoordSysEvaluator.findCoordByType(ds, AxisType.Lon);
        if (lon == null) {
            errlog.format("GempakCdm: Must have a Longitude coordinate", new Object[0]);
            return null;
        }
        if (lat.getRank() != lon.getRank()) {
            errlog.format("GempakCdm: Lat and Lon coordinate must have same rank", new Object[0]);
            return null;
        }
        Dimension stationDim = null;
        if (lat.getDimension(0) != lon.getDimension(0)) {
            errlog.format("GempakCdm: Lat and Lon coordinate must have same size", new Object[0]);
            return null;
        }
        stationDim = lat.getDimension(0);
        final Table.Type stationTableType = Table.Type.Structure;
        final TableConfig stnTable = new TableConfig(stationTableType, "station");
        stnTable.structureType = TableConfig.StructureType.PsuedoStructure;
        stnTable.dimName = stationDim.getName();
        stnTable.lat = lat.getName();
        stnTable.lon = lon.getName();
        stnTable.stnId = this.findNameVariableWithStandardNameAndDimension(ds, "station_id", stationDim, errlog);
        stnTable.stnDesc = this.findNameVariableWithStandardNameAndDimension(ds, "station_desc", stationDim, errlog);
        stnTable.stnWmoId = this.findNameVariableWithStandardNameAndDimension(ds, "station_WMO_id", stationDim, errlog);
        stnTable.stnAlt = this.findNameVariableWithStandardNameAndDimension(ds, "surface_altitude", stationDim, errlog);
        if (stnTable.stnId == null) {
            errlog.format("Must have a Station id variable with standard name station_id", new Object[0]);
            return null;
        }
        final Variable stnId = ds.findVariable(stnTable.stnId);
        if (!stnId.getDimension(0).equals(stationDim)) {
            errlog.format("GempakCdm: Station id (%s) outer dimension must match latitude/longitude dimension (%s)", stnTable.stnId, stationDim);
            return null;
        }
        return stnTable;
    }
}
