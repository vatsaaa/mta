// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard.plug;

import ucar.nc2.dataset.CoordinateAxis;
import ucar.ma2.DataType;
import java.util.ArrayList;
import ucar.nc2.ft.point.standard.Table;
import java.util.Iterator;
import ucar.nc2.Structure;
import ucar.nc2.ft.point.standard.Evaluator;
import ucar.nc2.dataset.VariableDS;
import ucar.nc2.ft.point.standard.Join;
import ucar.nc2.ft.point.standard.JoinArray;
import ucar.nc2.Dimension;
import java.util.List;
import ucar.nc2.ft.point.standard.CoordSysEvaluator;
import ucar.nc2.constants.AxisType;
import java.io.IOException;
import ucar.nc2.constants.CF;
import ucar.nc2.ft.point.standard.TableConfig;
import java.util.Formatter;
import java.util.StringTokenizer;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.ft.point.standard.TableConfigurerImpl;

public class CFpointObs extends TableConfigurerImpl
{
    public boolean isMine(final FeatureType wantFeatureType, final NetcdfDataset ds) {
        final String conv = ds.findAttValueIgnoreCase(null, "Conventions", null);
        if (conv == null) {
            return false;
        }
        final StringTokenizer stoke = new StringTokenizer(conv, ",");
        while (stoke.hasMoreTokens()) {
            final String toke = stoke.nextToken().trim();
            if (toke.startsWith("CF")) {
                return true;
            }
        }
        return false;
    }
    
    public TableConfig getConfig(final FeatureType wantFeatureType, final NetcdfDataset ds, final Formatter errlog) throws IOException {
        String ftypeS = ds.findAttValueIgnoreCase(null, "CF:featureType", null);
        if (ftypeS == null) {
            ftypeS = ds.findAttValueIgnoreCase(null, "CF-featureType", null);
        }
        if (ftypeS == null) {
            ftypeS = ds.findAttValueIgnoreCase(null, "CFfeatureType", null);
        }
        if (ftypeS == null) {
            ftypeS = ds.findAttValueIgnoreCase(null, "featureType", null);
        }
        CF.FeatureType ftype;
        if (ftypeS == null) {
            ftype = CF.FeatureType.point;
        }
        else {
            ftype = CF.FeatureType.getFeatureType(ftypeS);
            if (ftypeS == null) {
                ftype = CF.FeatureType.point;
            }
        }
        if (!this.checkCoordinates(ds, errlog)) {
            return null;
        }
        switch (ftype) {
            case point: {
                return this.getPointConfig(ds, errlog);
            }
            case timeSeries: {
                return this.getStationConfig(ds, errlog);
            }
            case trajectory: {
                return this.getTrajectoryConfig(ds, errlog);
            }
            case profile: {
                return this.getProfileConfig(ds, errlog);
            }
            case timeSeriesProfile: {
                return this.getTimeSeriesProfileConfig(ds, errlog);
            }
            case trajectoryProfile: {
                return this.getSectionConfig(ds, errlog);
            }
            default: {
                return null;
            }
        }
    }
    
    private boolean checkCoordinates(final NetcdfDataset ds, final Formatter errlog) {
        boolean ok = true;
        final Variable time = CoordSysEvaluator.findCoordByType(ds, AxisType.Time);
        if (time == null) {
            errlog.format("CFpointObs cant find a Time coordinate %n", new Object[0]);
            ok = false;
        }
        final Variable lat = CoordSysEvaluator.findCoordByType(ds, AxisType.Lat);
        if (lat == null) {
            errlog.format("CFpointObs cant find a Latitude coordinate %n", new Object[0]);
            ok = false;
        }
        final Variable lon = CoordSysEvaluator.findCoordByType(ds, AxisType.Lon);
        if (lon == null) {
            errlog.format("CFpointObs cant find a Longitude coordinate %n", new Object[0]);
            ok = false;
        }
        if (!ok) {
            return false;
        }
        final List<Dimension> dimLat = lat.getDimensions();
        final List<Dimension> dimLon = lon.getDimensions();
        if (!dimLat.equals(dimLon)) {
            errlog.format("Lat and Lon coordinate dimensions must match lat=%s lon=%s %n", lat.getNameAndDimensions(), lon.getNameAndDimensions());
            ok = false;
        }
        return ok;
    }
    
    private TableConfig getPointConfig(final NetcdfDataset ds, final Formatter errlog) throws IOException {
        final Variable time = CoordSysEvaluator.findCoordByType(ds, AxisType.Time);
        if (time.getRank() != 1) {
            errlog.format("CFpointObs type=point: coord time must have rank 1, coord var= %s %n", time.getNameAndDimensions());
            return null;
        }
        final Dimension obsDim = time.getDimension(0);
        final TableConfig obsTable = this.makeSingle(ds, obsDim, errlog);
        obsTable.featureType = FeatureType.POINT;
        return obsTable;
    }
    
    private TableConfig getStationConfig(final NetcdfDataset ds, final Formatter errlog) throws IOException {
        final EncodingInfo info = this.identifyEncodingStation(ds, CF.FeatureType.timeSeries, errlog);
        if (info == null) {
            return null;
        }
        final VariableDS time = CoordSysEvaluator.findCoordByType(ds, AxisType.Time);
        final Variable parentId = this.identifyParent(ds, CF.FeatureType.timeSeries);
        final Dimension obsDim = info.childDim;
        final TableConfig stnTable = this.makeStationTable(ds, FeatureType.STATION, info, errlog);
        if (stnTable == null) {
            return null;
        }
        TableConfig obsTable = null;
        switch (info.encoding) {
            case single: {
                obsTable = this.makeSingle(ds, obsDim, errlog);
                break;
            }
            case multidim: {
                obsTable = this.makeMultidimInner(ds, stnTable, obsDim, errlog);
                if (time.getRank() == 1) {
                    obsTable.addJoin(new JoinArray(time, JoinArray.Type.raw, 0));
                    obsTable.time = time.getShortName();
                    break;
                }
                break;
            }
            case raggedContiguous: {
                obsTable = this.makeRaggedContiguous(ds, info.parentDim, info.childDim, errlog);
                break;
            }
            case raggedIndex: {
                obsTable = this.makeRaggedIndex(ds, info.parentDim, info.childDim, errlog);
                break;
            }
            case flat: {
                obsTable = this.makeStructTable(ds, FeatureType.STATION, new EncodingInfo(Encoding.flat, obsDim), errlog);
                obsTable.parentIndex = ((parentId == null) ? null : parentId.getName());
                obsTable.stnId = this.findNameVariableWithStandardNameAndDimension(ds, "station_id", obsDim, errlog);
                obsTable.stnDesc = this.findNameVariableWithStandardNameAndDimension(ds, "station_desc", obsDim, errlog);
                obsTable.stnWmoId = this.findNameVariableWithStandardNameAndDimension(ds, "station_WMO_id", obsDim, errlog);
                obsTable.stnAlt = this.findNameVariableWithStandardNameAndDimension(ds, "surface_altitude", obsDim, errlog);
                break;
            }
        }
        if (obsTable == null) {
            return null;
        }
        stnTable.addChild(obsTable);
        return stnTable;
    }
    
    private TableConfig getTrajectoryConfig(final NetcdfDataset ds, final Formatter errlog) throws IOException {
        final EncodingInfo info = this.identifyEncodingTraj(ds, CF.FeatureType.trajectory, errlog);
        if (info == null) {
            return null;
        }
        final TableConfig parentTable = this.makeStructTable(ds, FeatureType.TRAJECTORY, info, errlog);
        if (parentTable == null) {
            return null;
        }
        parentTable.feature_id = this.identifyParentId(ds, CF.FeatureType.trajectory);
        if (parentTable.feature_id == null) {
            errlog.format("getTrajectoryConfig cant find a trajectoy id %n", new Object[0]);
        }
        TableConfig obsConfig = null;
        switch (info.encoding) {
            case single: {
                obsConfig = this.makeSingle(ds, info.childDim, errlog);
                break;
            }
            case multidim: {
                obsConfig = this.makeMultidimInner(ds, parentTable, info.childDim, errlog);
                break;
            }
            case raggedContiguous: {
                obsConfig = this.makeRaggedContiguous(ds, info.parentDim, info.childDim, errlog);
                break;
            }
            case raggedIndex: {
                obsConfig = this.makeRaggedIndex(ds, info.parentDim, info.childDim, errlog);
                break;
            }
            case flat: {
                throw new UnsupportedOperationException("CFpointObs: trajectory flat encoding");
            }
        }
        if (obsConfig == null) {
            return null;
        }
        parentTable.addChild(obsConfig);
        return parentTable;
    }
    
    private TableConfig getProfileConfig(final NetcdfDataset ds, final Formatter errlog) throws IOException {
        final EncodingInfo info = this.identifyEncodingProfile(ds, CF.FeatureType.profile, errlog);
        if (info == null) {
            return null;
        }
        final TableConfig parentTable = this.makeStructTable(ds, FeatureType.PROFILE, info, errlog);
        if (parentTable == null) {
            return null;
        }
        parentTable.feature_id = this.identifyParentId(ds, CF.FeatureType.profile);
        if (parentTable.feature_id == null) {
            errlog.format("getProfileConfig cant find a profile id %n", new Object[0]);
        }
        final VariableDS z = CoordSysEvaluator.findCoordByType(ds, AxisType.Height);
        if (z == null) {
            errlog.format("getProfileConfig cant find a Height coordinate %n", new Object[0]);
            return null;
        }
        TableConfig obsTable = null;
        switch (info.encoding) {
            case single: {
                obsTable = this.makeSingle(ds, info.childDim, errlog);
                break;
            }
            case multidim: {
                obsTable = this.makeMultidimInner(ds, parentTable, info.childDim, errlog);
                if (z.getRank() == 1) {
                    obsTable.addJoin(new JoinArray(z, JoinArray.Type.raw, 0));
                    break;
                }
                break;
            }
            case raggedContiguous: {
                obsTable = this.makeRaggedContiguous(ds, info.parentDim, info.childDim, errlog);
                break;
            }
            case raggedIndex: {
                obsTable = this.makeRaggedIndex(ds, info.parentDim, info.childDim, errlog);
                break;
            }
            case flat: {
                throw new UnsupportedOperationException("CFpointObs: profile flat encoding");
            }
        }
        if (obsTable == null) {
            return null;
        }
        parentTable.addChild(obsTable);
        return parentTable;
    }
    
    private TableConfig getTimeSeriesProfileConfig(final NetcdfDataset ds, final Formatter errlog) throws IOException {
        final EncodingInfo info = this.identifyEncodingTimeSeriesProfile(ds, CF.FeatureType.timeSeriesProfile, errlog);
        if (info == null) {
            return null;
        }
        final VariableDS time = CoordSysEvaluator.findCoordByType(ds, AxisType.Time);
        if (time.getRank() == 0) {
            errlog.format("timeSeriesProfile cannot have a scalar time coordinate%n", new Object[0]);
            return null;
        }
        final VariableDS z = this.findZAxisNotStationAlt(ds);
        if (z == null) {
            errlog.format("timeSeriesProfile must have a z coordinate%n", new Object[0]);
            return null;
        }
        if (z.getRank() == 0) {
            errlog.format("timeSeriesProfile cannot have a scalar z coordinate%n", new Object[0]);
            return null;
        }
        final TableConfig stationTable = this.makeStationTable(ds, FeatureType.STATION_PROFILE, info, errlog);
        if (stationTable == null) {
            return null;
        }
        switch (info.encoding) {
            case single: {
                assert time.getRank() >= 1 && time.getRank() <= 2 : "time must be rank 1 or 2";
                assert z.getRank() >= 1 && z.getRank() <= 2 : "z must be rank 1 or 2";
                if (time.getRank() == 2) {
                    if (z.getRank() == 2) {
                        assert time.getDimensions().equals(z.getDimensions()) : "rank-2 time and z dimensions must be the same";
                    }
                    else {
                        assert time.getDimension(1).equals(z.getDimension(0)) : "rank-2 time must have z inner dimension";
                    }
                }
                else if (z.getRank() == 2) {
                    assert z.getDimension(0).equals(time.getDimension(0)) : "rank-2 z must have time outer dimension";
                }
                else {
                    assert !time.getDimension(0).equals(z.getDimension(0)) : "time and z dimensions must be different";
                }
                final TableConfig profileTable = this.makeStructTable(ds, FeatureType.PROFILE, new EncodingInfo(Encoding.multidim, info.childDim), errlog);
                if (profileTable == null) {
                    return null;
                }
                if (time.getRank() == 1) {
                    profileTable.addJoin(new JoinArray(time, JoinArray.Type.raw, 0));
                }
                stationTable.addChild(profileTable);
                final TableConfig zTable = this.makeMultidimInner(ds, profileTable, info.grandChildDim, errlog);
                if (z.getRank() == 1) {
                    zTable.addJoin(new JoinArray(z, JoinArray.Type.raw, 0));
                }
                profileTable.addChild(zTable);
                break;
            }
            case multidim: {
                assert time.getRank() >= 2 && time.getRank() <= 3 : "time must be rank 2 or 3";
                assert z.getRank() == 3 : "z must be rank 1 or 3";
                if (time.getRank() == 3) {
                    if (z.getRank() == 3) {
                        assert time.getDimensions().equals(z.getDimensions()) : "rank-3 time and z dimensions must be the same";
                    }
                    else {
                        assert time.getDimension(2).equals(z.getDimension(0)) : "rank-3 time must have z inner dimension";
                    }
                }
                else if (z.getRank() == 3) {
                    assert z.getDimension(1).equals(time.getDimension(1)) : "rank-2 time must have time inner dimension";
                }
                else {
                    assert !time.getDimension(0).equals(z.getDimension(0)) : "time and z dimensions must be different";
                    assert !time.getDimension(1).equals(z.getDimension(0)) : "time and z dimensions must be different";
                }
                final TableConfig profileTable = this.makeMultidimInner(ds, stationTable, info.childDim, errlog);
                if (profileTable == null) {
                    return null;
                }
                stationTable.addChild(profileTable);
                final TableConfig zTable = this.makeMultidimInner3D(ds, stationTable, profileTable, info.grandChildDim, errlog);
                if (z.getRank() == 1) {
                    zTable.addJoin(new JoinArray(z, JoinArray.Type.raw, 0));
                }
                profileTable.addChild(zTable);
                break;
            }
            case raggedIndex: {
                final Variable stationIndex = Evaluator.getVariableWithAttributeValue(ds, "CF:ragged_parent_index", info.parentDim.getName());
                if (stationIndex == null) {
                    errlog.format("timeSeriesProfile stationIndex: must have a ragged_parentIndex variable with profile dimension%n", new Object[0]);
                    return null;
                }
                if (stationIndex.getRank() != 1) {
                    errlog.format("timeSeriesProfile stationIndex: %s variable must be rank 1%n", stationIndex.getName());
                    return null;
                }
                final Variable ragged_rowSize = Evaluator.getVariableWithAttributeValue(ds, "CF:ragged_row_count", info.grandChildDim.getName());
                if (ragged_rowSize == null) {
                    errlog.format("timeSeriesProfile numObs: must have a ragged_rowSize variable with profile dimension %s%n", info.childDim);
                    return null;
                }
                if (ragged_rowSize.getRank() != 1) {
                    errlog.format("timeSeriesProfile numObs: %s variable for observations must be rank 1%n", ragged_rowSize.getName());
                    return null;
                }
                if (info.childDim.equals(info.grandChildDim)) {
                    errlog.format("timeSeriesProfile profile dimension %s cannot be obs dimension %s%n", info.childDim, info.grandChildDim);
                    return null;
                }
                final TableConfig profileTable2 = this.makeRaggedIndex(ds, info.parentDim, info.childDim, errlog);
                stationTable.addChild(profileTable2);
                final TableConfig zTable2 = this.makeRaggedContiguous(ds, info.childDim, info.grandChildDim, errlog);
                profileTable2.addChild(zTable2);
                break;
            }
            case raggedContiguous: {
                throw new UnsupportedOperationException("CFpointObs: profile raggedContiguous encoding");
            }
        }
        return stationTable;
    }
    
    private TableConfig getSectionConfig(final NetcdfDataset ds, final Formatter errlog) throws IOException {
        final EncodingInfo info = this.identifyEncodingSection(ds, CF.FeatureType.trajectoryProfile, errlog);
        if (info == null) {
            return null;
        }
        final VariableDS time = CoordSysEvaluator.findCoordByType(ds, AxisType.Time);
        if (time.getRank() == 0) {
            errlog.format("section cannot have a scalar time coordinate%n", new Object[0]);
            return null;
        }
        final TableConfig parentTable = this.makeStructTable(ds, FeatureType.SECTION, info, errlog);
        if (parentTable == null) {
            return null;
        }
        parentTable.feature_id = this.identifyParentId(ds, CF.FeatureType.trajectoryProfile);
        if (parentTable.feature_id == null) {
            errlog.format("getSectionConfig cant find a section id %n", new Object[0]);
        }
        final VariableDS z = this.findZAxisNotStationAlt(ds);
        if (z == null) {
            errlog.format("section must have a z coordinate%n", new Object[0]);
            return null;
        }
        if (z.getRank() == 0) {
            errlog.format("section cannot have a scalar z coordinate%n", new Object[0]);
            return null;
        }
        switch (info.encoding) {
            case single: {
                assert time.getRank() >= 1 && time.getRank() <= 2 : "time must be rank 1 or 2";
                assert z.getRank() >= 1 && z.getRank() <= 2 : "z must be rank 1 or 2";
                if (time.getRank() == 2) {
                    if (z.getRank() == 2) {
                        assert time.getDimensions().equals(z.getDimensions()) : "rank-2 time and z dimensions must be the same";
                    }
                    else {
                        assert time.getDimension(1).equals(z.getDimension(0)) : "rank-2 time must have z inner dimension";
                    }
                }
                else if (z.getRank() == 2) {
                    assert z.getDimension(0).equals(time.getDimension(0)) : "rank-2 z must have time outer dimension";
                }
                else {
                    assert !time.getDimension(0).equals(z.getDimension(0)) : "time and z dimensions must be different";
                }
                final TableConfig profileTable = this.makeStructTable(ds, FeatureType.PROFILE, new EncodingInfo(Encoding.multidim, info.childDim), errlog);
                if (profileTable == null) {
                    return null;
                }
                if (time.getRank() == 1) {
                    profileTable.addJoin(new JoinArray(time, JoinArray.Type.raw, 0));
                }
                parentTable.addChild(profileTable);
                final TableConfig zTable = this.makeMultidimInner(ds, profileTable, info.grandChildDim, errlog);
                if (z.getRank() == 1) {
                    zTable.addJoin(new JoinArray(z, JoinArray.Type.raw, 0));
                }
                profileTable.addChild(zTable);
                break;
            }
            case multidim: {
                assert time.getRank() >= 2 && time.getRank() <= 3 : "time must be rank 2 or 3";
                assert z.getRank() == 3 : "z must be rank 1 or 3";
                if (time.getRank() == 3) {
                    if (z.getRank() == 3) {
                        assert time.getDimensions().equals(z.getDimensions()) : "rank-3 time and z dimensions must be the same";
                    }
                    else {
                        assert time.getDimension(2).equals(z.getDimension(0)) : "rank-3 time must have z inner dimension";
                    }
                }
                else if (z.getRank() == 3) {
                    assert z.getDimension(1).equals(time.getDimension(1)) : "rank-2 time must have time inner dimension";
                }
                else {
                    assert !time.getDimension(0).equals(z.getDimension(0)) : "time and z dimensions must be different";
                    assert !time.getDimension(1).equals(z.getDimension(0)) : "time and z dimensions must be different";
                }
                final TableConfig profileTable = this.makeMultidimInner(ds, parentTable, info.childDim, errlog);
                if (profileTable == null) {
                    return null;
                }
                profileTable.feature_id = this.identifyParentId(ds, CF.FeatureType.profile);
                parentTable.addChild(profileTable);
                final TableConfig zTable = this.makeMultidimInner3D(ds, parentTable, profileTable, info.grandChildDim, errlog);
                if (z.getRank() == 1) {
                    zTable.addJoin(new JoinArray(z, JoinArray.Type.raw, 0));
                }
                profileTable.addChild(zTable);
                break;
            }
            case raggedIndex: {
                final Variable sectionIndex = Evaluator.getVariableWithAttributeValue(ds, "CF:ragged_parent_index", info.parentDim.getName());
                if (sectionIndex == null) {
                    errlog.format("section sectionIndex: must have a ragged_parentIndex variable with profile dimension%n", new Object[0]);
                    return null;
                }
                if (sectionIndex.getRank() != 1) {
                    errlog.format("section sectionIndex: %s variable must be rank 1%n", sectionIndex.getName());
                    return null;
                }
                final Variable numObs = Evaluator.getVariableWithAttributeValue(ds, "CF:ragged_row_count", info.grandChildDim.getName());
                if (numObs == null) {
                    errlog.format("section numObs: must have a ragged_rowSize variable with profile dimension %s%n", info.childDim);
                    return null;
                }
                if (numObs.getRank() != 1) {
                    errlog.format("section numObs: %s variable for observations must be rank 1%n", numObs.getName());
                    return null;
                }
                if (info.childDim.equals(info.grandChildDim)) {
                    errlog.format("section profile dimension %s cannot be obs dimension %s%n", info.childDim, info.grandChildDim);
                    return null;
                }
                final TableConfig profileTable2 = this.makeRaggedIndex(ds, info.parentDim, info.childDim, errlog);
                parentTable.addChild(profileTable2);
                final TableConfig zTable2 = this.makeRaggedContiguous(ds, info.childDim, info.grandChildDim, errlog);
                profileTable2.addChild(zTable2);
                break;
            }
            case raggedContiguous: {
                throw new UnsupportedOperationException("CFpointObs: section raggedContiguous encoding%n");
            }
        }
        return parentTable;
    }
    
    private EncodingInfo identifyEncoding(final NetcdfDataset ds, final CF.FeatureType ftype, final Formatter errlog) {
        final Variable ragged_rowSize = Evaluator.getVariableWithAttribute(ds, "CF:ragged_row_count");
        if (ragged_rowSize != null) {
            if (ftype != CF.FeatureType.trajectoryProfile) {
                return new EncodingInfo(Encoding.raggedContiguous, ragged_rowSize);
            }
            final Variable parentId = this.identifyParent(ds, ftype);
            if (parentId == null) {
                errlog.format("Section ragged must have section_id variable%n", new Object[0]);
                return null;
            }
            return new EncodingInfo(Encoding.raggedContiguous, parentId);
        }
        else {
            final Variable ragged_parentIndex = Evaluator.getVariableWithAttribute(ds, "CF:ragged_parent_index");
            if (ragged_parentIndex != null) {
                final Variable ragged_parentId = this.identifyParent(ds, ftype);
                return new EncodingInfo(Encoding.raggedIndex, ragged_parentId);
            }
            final Variable lat = CoordSysEvaluator.findCoordByType(ds, AxisType.Lat);
            if (lat == null) {
                errlog.format("Must have a Latitude coordinate%n", new Object[0]);
                return null;
            }
            switch (ftype) {
                case point: {
                    return new EncodingInfo(Encoding.multidim, (Dimension)null);
                }
                case timeSeries:
                case profile:
                case timeSeriesProfile: {
                    if (lat.getRank() == 0) {
                        return new EncodingInfo(Encoding.single, (Dimension)null);
                    }
                    if (lat.getRank() == 1) {
                        return new EncodingInfo(Encoding.multidim, lat);
                    }
                    errlog.format("CFpointObs %s Must have Lat/Lon coordinates of rank 0 or 1%n", ftype);
                    return null;
                }
                case trajectory:
                case trajectoryProfile: {
                    if (lat.getRank() == 1) {
                        return new EncodingInfo(Encoding.single, (Dimension)null);
                    }
                    if (lat.getRank() == 2) {
                        return new EncodingInfo(Encoding.multidim, lat);
                    }
                    errlog.format("CFpointObs %s Must have Lat/Lon coordinates of rank 1 or 2%n", ftype);
                    return null;
                }
                default: {
                    return null;
                }
            }
        }
    }
    
    private EncodingInfo identifyEncodingStation(final NetcdfDataset ds, final CF.FeatureType ftype, final Formatter errlog) {
        final VariableDS time = CoordSysEvaluator.findCoordByType(ds, AxisType.Time);
        if (time == null) {
            errlog.format("Must have a Time coordinate%n", new Object[0]);
            return null;
        }
        Dimension obsDim = null;
        if (time.getRank() > 0) {
            obsDim = time.getDimension(time.getRank() - 1);
        }
        else if (time.getParentStructure() != null) {
            final Structure parent = time.getParentStructure();
            obsDim = parent.getDimension(parent.getRank() - 1);
        }
        if (obsDim == null) {
            errlog.format("Must have a non-scalar Time coordinate%n", new Object[0]);
            return null;
        }
        final Variable lat = CoordSysEvaluator.findCoordByType(ds, AxisType.Lat);
        if (lat == null) {
            errlog.format("Must have a Latitude coordinate%n", new Object[0]);
            return null;
        }
        if (lat.getRank() == 0) {
            return new EncodingInfo(Encoding.single, null, obsDim);
        }
        final Dimension stnDim = lat.getDimension(0);
        if (obsDim == stnDim) {
            return new EncodingInfo(Encoding.flat, null, obsDim);
        }
        final Variable ragged_rowSize = Evaluator.getVariableWithAttributeValue(ds, "CF:ragged_row_count", obsDim.getName());
        if (ragged_rowSize != null) {
            return new EncodingInfo(Encoding.raggedContiguous, stnDim, obsDim);
        }
        final Variable ragged_parentIndex = Evaluator.getVariableWithAttributeValue(ds, "CF:ragged_parent_index", stnDim.getName());
        if (ragged_parentIndex != null) {
            return new EncodingInfo(Encoding.raggedIndex, stnDim, obsDim);
        }
        if (lat.getRank() == 1) {
            return new EncodingInfo(Encoding.multidim, stnDim, obsDim);
        }
        errlog.format("CFpointObs %s Must have Lat/Lon coordinates of rank 0 or 1%n", ftype);
        return null;
    }
    
    private EncodingInfo identifyEncodingTraj(final NetcdfDataset ds, final CF.FeatureType ftype, final Formatter errlog) {
        final VariableDS time = CoordSysEvaluator.findCoordByType(ds, AxisType.Time);
        if (time == null) {
            errlog.format("Must have a Time coordinate%n", new Object[0]);
            return null;
        }
        Dimension obsDim = null;
        if (time.getRank() > 0) {
            obsDim = time.getDimension(time.getRank() - 1);
        }
        else if (time.getParentStructure() != null) {
            final Structure parent = time.getParentStructure();
            obsDim = parent.getDimension(parent.getRank() - 1);
        }
        if (obsDim == null) {
            errlog.format("Must have a non-scalar Time coordinate%n", new Object[0]);
            return null;
        }
        Dimension parentDim = null;
        if (time.getRank() > 1) {
            parentDim = time.getDimension(0);
            return new EncodingInfo(Encoding.multidim, parentDim, obsDim);
        }
        final String dimName = Evaluator.getVariableAttributeValue(ds, "CF:ragged_parent_index");
        if (dimName != null) {
            parentDim = ds.findDimension(dimName);
            if (parentDim != null) {
                return new EncodingInfo(Encoding.raggedIndex, parentDim, obsDim);
            }
            errlog.format("CFpointObs %s ragged_parent_index must name parent dimension%n", ftype);
            return null;
        }
        else {
            final Variable ragged_rowSize = Evaluator.getVariableWithAttributeValue(ds, "CF:ragged_row_count", obsDim.getName());
            if (ragged_rowSize != null) {
                parentDim = ragged_rowSize.getDimension(0);
                return new EncodingInfo(Encoding.raggedContiguous, parentDim, obsDim);
            }
            final VariableDS lat = CoordSysEvaluator.findCoordByType(ds, AxisType.Lat);
            if (lat == null) {
                errlog.format("Must have a Lat coordinate%n", new Object[0]);
                return null;
            }
            if (lat.getRank() > 0) {
                for (final Dimension d : lat.getDimensions()) {
                    if (!d.equals(obsDim)) {
                        return new EncodingInfo(Encoding.multidim, d, obsDim);
                    }
                }
            }
            return new EncodingInfo(Encoding.single, null, obsDim);
        }
    }
    
    private EncodingInfo identifyEncodingProfile(final NetcdfDataset ds, final CF.FeatureType ftype, final Formatter errlog) {
        final VariableDS z = CoordSysEvaluator.findCoordByType(ds, AxisType.Height);
        if (z == null) {
            errlog.format("Must have a Height coordinate%n", new Object[0]);
            return null;
        }
        Dimension obsDim = null;
        if (z.getRank() > 0) {
            obsDim = z.getDimension(z.getRank() - 1);
        }
        else if (z.getParentStructure() != null) {
            final Structure parent = z.getParentStructure();
            obsDim = parent.getDimension(parent.getRank() - 1);
        }
        if (obsDim == null) {
            errlog.format("Must have a non-scalar Height coordinate%n", new Object[0]);
            return null;
        }
        Dimension parentDim = null;
        if (z.getRank() > 1) {
            parentDim = z.getDimension(0);
            return new EncodingInfo(Encoding.multidim, parentDim, obsDim);
        }
        final String dimName = Evaluator.getVariableAttributeValue(ds, "CF:ragged_parent_index");
        if (dimName != null) {
            parentDim = ds.findDimension(dimName);
            if (parentDim != null) {
                return new EncodingInfo(Encoding.raggedIndex, parentDim, obsDim);
            }
            errlog.format("CFpointObs %s ragged_parent_index must name parent dimension%n", ftype);
            return null;
        }
        else {
            final Variable ragged_rowSize = Evaluator.getVariableWithAttributeValue(ds, "CF:ragged_row_count", obsDim.getName());
            if (ragged_rowSize != null) {
                parentDim = ragged_rowSize.getDimension(0);
                return new EncodingInfo(Encoding.raggedContiguous, parentDim, obsDim);
            }
            final VariableDS time = CoordSysEvaluator.findCoordByType(ds, AxisType.Time);
            if (time == null) {
                errlog.format("Must have a Time coordinate%n", new Object[0]);
                return null;
            }
            if (time.getRank() == 0 || time.getDimension(0) == obsDim) {
                return new EncodingInfo(Encoding.single, null, obsDim);
            }
            parentDim = time.getDimension(0);
            return new EncodingInfo(Encoding.multidim, parentDim, obsDim);
        }
    }
    
    private EncodingInfo identifyEncodingSection(final NetcdfDataset ds, final CF.FeatureType ftype, final Formatter errlog) {
        final VariableDS z = CoordSysEvaluator.findCoordByType(ds, AxisType.Height);
        if (z == null) {
            errlog.format("Must have a Height coordinate%n", new Object[0]);
            return null;
        }
        Dimension obsDim = null;
        if (z.getRank() > 0) {
            obsDim = z.getDimension(z.getRank() - 1);
        }
        else if (z.getParentStructure() != null) {
            final Structure parent = z.getParentStructure();
            obsDim = parent.getDimension(parent.getRank() - 1);
        }
        if (obsDim == null) {
            errlog.format("Must have a non-scalar Height coordinate%n", new Object[0]);
            return null;
        }
        Dimension trajDim = null;
        Dimension profileDim = null;
        if (z.getRank() > 2) {
            trajDim = z.getDimension(0);
            profileDim = z.getDimension(1);
            return new EncodingInfo(Encoding.multidim, trajDim, profileDim, obsDim);
        }
        final String dimName = Evaluator.getVariableAttributeValue(ds, "CF:ragged_parent_index");
        if (dimName != null) {
            trajDim = ds.findDimension(dimName);
            if (trajDim == null) {
                errlog.format("CFpointObs %s ragged_parent_index must name station dimension%n", ftype);
                return null;
            }
            final Variable ragged_rowSize = Evaluator.getVariableWithAttributeValue(ds, "CF:ragged_row_count", obsDim.getName());
            if (ragged_rowSize != null) {
                profileDim = ragged_rowSize.getDimension(0);
                return new EncodingInfo(Encoding.raggedIndex, trajDim, profileDim, obsDim);
            }
            errlog.format("CFpointObs %s ragged_row_count must name obs dimension%n", ftype);
            return null;
        }
        else {
            final VariableDS lat = CoordSysEvaluator.findCoordByType(ds, AxisType.Lat);
            if (lat == null) {
                errlog.format("Must have a Lat coordinate%n", new Object[0]);
                return null;
            }
            final VariableDS time = CoordSysEvaluator.findCoordByType(ds, AxisType.Time);
            if (time == null) {
                errlog.format("Must have a Time coordinate%n", new Object[0]);
                return null;
            }
            if (lat.getRank() == 0) {
                errlog.format("CFpointObs %s may not have scalar lat/lon%n", ftype);
                return null;
            }
            if (time.getRank() > 2) {
                trajDim = time.getDimension(0);
                profileDim = time.getDimension(1);
                return new EncodingInfo(Encoding.multidim, trajDim, profileDim, obsDim);
            }
            if (lat.getRank() == 1) {
                profileDim = lat.getDimension(0);
                return new EncodingInfo(Encoding.single, null, profileDim, obsDim);
            }
            if (lat.getRank() == 2) {
                trajDim = lat.getDimension(0);
                profileDim = lat.getDimension(0);
                return new EncodingInfo(Encoding.multidim, trajDim, profileDim, obsDim);
            }
            errlog.format("CFpointObs %s unrecognized form%n", ftype);
            return null;
        }
    }
    
    private EncodingInfo identifyEncodingTimeSeriesProfile(final NetcdfDataset ds, final CF.FeatureType ftype, final Formatter errlog) {
        final VariableDS z = this.findZAxisNotStationAlt(ds);
        if (z == null) {
            errlog.format("timeSeriesProfile must have a z coordinate, not the station altitude%n", new Object[0]);
            return null;
        }
        if (z.getRank() == 0) {
            errlog.format("timeSeriesProfile cannot have a scalar z coordinate%n", new Object[0]);
            return null;
        }
        final Dimension obsDim = z.getDimension(z.getRank() - 1);
        Dimension stnDim = null;
        Dimension profileDim = null;
        if (z.getRank() > 2) {
            stnDim = z.getDimension(0);
            profileDim = z.getDimension(1);
            return new EncodingInfo(Encoding.multidim, stnDim, profileDim, obsDim);
        }
        final String dimName = Evaluator.getVariableAttributeValue(ds, "CF:ragged_parent_index");
        if (dimName != null) {
            stnDim = ds.findDimension(dimName);
            if (stnDim == null) {
                errlog.format("CFpointObs %s ragged_parent_index must name station dimension%n", ftype);
                return null;
            }
            final Variable ragged_rowSize = Evaluator.getVariableWithAttributeValue(ds, "CF:ragged_row_count", obsDim.getName());
            if (ragged_rowSize != null) {
                profileDim = ragged_rowSize.getDimension(0);
                return new EncodingInfo(Encoding.raggedIndex, stnDim, profileDim, obsDim);
            }
            errlog.format("CFpointObs %s ragged_row_count must name obs dimension%n", ftype);
            return null;
        }
        else {
            final VariableDS lat = CoordSysEvaluator.findCoordByType(ds, AxisType.Lat);
            if (lat == null) {
                errlog.format("Must have a Lat coordinate%n", new Object[0]);
                return null;
            }
            final VariableDS time = CoordSysEvaluator.findCoordByType(ds, AxisType.Time);
            if (time == null) {
                errlog.format("Must have a Time coordinate%n", new Object[0]);
                return null;
            }
            if (lat.getRank() == 0) {
                profileDim = time.getDimension(0);
                return new EncodingInfo(Encoding.single, null, profileDim, obsDim);
            }
            if (time.getRank() == 1 || (time.getRank() == 2 && time.getDimension(1) == obsDim)) {
                profileDim = time.getDimension(0);
                return new EncodingInfo(Encoding.flat, null, profileDim, obsDim);
            }
            if (time.getRank() > 1) {
                stnDim = time.getDimension(0);
                profileDim = time.getDimension(1);
                return new EncodingInfo(Encoding.multidim, stnDim, profileDim, obsDim);
            }
            errlog.format("CFpointObs %s unrecognized form%n", ftype);
            return null;
        }
    }
    
    private String identifyParentId(final NetcdfDataset ds, final CF.FeatureType ftype) {
        final Variable v = this.identifyParent(ds, ftype);
        return (v == null) ? null : v.getShortName();
    }
    
    private Variable identifyParent(final NetcdfDataset ds, final CF.FeatureType ftype) {
        switch (ftype) {
            case timeSeries:
            case timeSeriesProfile: {
                return Evaluator.getVariableWithAttributeValue(ds, "standard_name", "station_id");
            }
            case trajectory: {
                return Evaluator.getVariableWithAttributeValue(ds, "standard_name", "trajectory_id");
            }
            case profile: {
                return Evaluator.getVariableWithAttributeValue(ds, "standard_name", "profile_id");
            }
            case trajectoryProfile: {
                return Evaluator.getVariableWithAttributeValue(ds, "standard_name", "trajectory_id");
            }
            default: {
                return null;
            }
        }
    }
    
    private TableConfig makeStationTable(final NetcdfDataset ds, final FeatureType ftype, final EncodingInfo info, final Formatter errlog) throws IOException {
        final Variable lat = CoordSysEvaluator.findCoordByType(ds, AxisType.Lat);
        final Variable lon = CoordSysEvaluator.findCoordByType(ds, AxisType.Lon);
        Table.Type stationTableType = Table.Type.Structure;
        if (info.encoding == Encoding.single) {
            stationTableType = Table.Type.Top;
        }
        if (info.encoding == Encoding.flat) {
            stationTableType = Table.Type.Construct;
        }
        final Dimension stationDim = (info.encoding == Encoding.flat) ? info.childDim : info.parentDim;
        final String name = (stationDim == null) ? " single" : stationDim.getName();
        final TableConfig stnTable = new TableConfig(stationTableType, name);
        stnTable.featureType = ftype;
        stnTable.stnId = this.findNameVariableWithStandardNameAndDimension(ds, "station_id", stationDim, errlog);
        stnTable.stnDesc = this.findNameVariableWithStandardNameAndDimension(ds, "station_desc", stationDim, errlog);
        stnTable.stnWmoId = this.findNameVariableWithStandardNameAndDimension(ds, "station_WMO_id", stationDim, errlog);
        stnTable.stnAlt = this.findNameVariableWithStandardNameAndDimension(ds, "surface_altitude", stationDim, errlog);
        stnTable.lat = lat.getName();
        stnTable.lon = lon.getName();
        if (stnTable.stnId == null) {
            errlog.format("Must have a Station id variable with standard name station_id%n", new Object[0]);
            return null;
        }
        if (info.encoding != Encoding.single) {
            final boolean hasStruct = Evaluator.hasRecordStructure(ds) && stationDim.isUnlimited();
            stnTable.structureType = (hasStruct ? TableConfig.StructureType.Structure : TableConfig.StructureType.PsuedoStructure);
            stnTable.dimName = stationDim.getName();
            stnTable.structName = (hasStruct ? "record" : stationDim.getName());
        }
        if (stnTable.stnAlt == null) {
            final Variable alt = CoordSysEvaluator.findCoordByType(ds, AxisType.Height);
            if (alt != null) {
                if (info.encoding == Encoding.single && alt.getRank() == 0) {
                    stnTable.stnAlt = alt.getName();
                }
                if (info.encoding != Encoding.single && lat.getRank() == alt.getRank() && alt.getDimension(0).equals(stationDim)) {
                    stnTable.stnAlt = alt.getName();
                }
            }
        }
        return stnTable;
    }
    
    private TableConfig makeStructTable(final NetcdfDataset ds, final FeatureType ftype, final EncodingInfo info, final Formatter errlog) throws IOException {
        Table.Type tableType = Table.Type.Structure;
        if (info.encoding == Encoding.single) {
            tableType = Table.Type.Top;
        }
        if (info.encoding == Encoding.flat) {
            tableType = Table.Type.ParentId;
        }
        final String name = (info.parentDim == null) ? " single" : info.parentDim.getName();
        final TableConfig tableConfig = new TableConfig(tableType, name);
        tableConfig.lat = this.matchAxisTypeAndDimension(ds, AxisType.Lat, info.parentDim);
        tableConfig.lon = this.matchAxisTypeAndDimension(ds, AxisType.Lon, info.parentDim);
        tableConfig.elev = this.matchAxisTypeAndDimension(ds, AxisType.Height, info.parentDim);
        tableConfig.time = this.matchAxisTypeAndDimension(ds, AxisType.Time, info.parentDim);
        tableConfig.featureType = ftype;
        if (info.encoding != Encoding.single) {
            final boolean stnIsStruct = Evaluator.hasRecordStructure(ds) && info.parentDim.isUnlimited();
            tableConfig.structureType = (stnIsStruct ? TableConfig.StructureType.Structure : TableConfig.StructureType.PsuedoStructure);
            tableConfig.dimName = info.parentDim.getName();
            tableConfig.structName = (stnIsStruct ? "record" : tableConfig.dimName);
        }
        return tableConfig;
    }
    
    private TableConfig makeStructTableTestTraj(final NetcdfDataset ds, final FeatureType ftype, final EncodingInfo info, final Formatter errlog) throws IOException {
        Table.Type tableType = Table.Type.Structure;
        if (info.encoding == Encoding.single) {
            tableType = Table.Type.Top;
        }
        if (info.encoding == Encoding.flat) {
            tableType = Table.Type.ParentId;
        }
        final String name = (info.parentDim == null) ? " single" : info.parentDim.getName();
        final TableConfig tableConfig = new TableConfig(tableType, name);
        tableConfig.lat = CoordSysEvaluator.findCoordNameByType(ds, AxisType.Lat);
        tableConfig.lon = CoordSysEvaluator.findCoordNameByType(ds, AxisType.Lon);
        tableConfig.elev = CoordSysEvaluator.findCoordNameByType(ds, AxisType.Height);
        tableConfig.time = CoordSysEvaluator.findCoordNameByType(ds, AxisType.Time);
        tableConfig.featureType = ftype;
        if (info.encoding != Encoding.single) {
            final boolean stnIsStruct = Evaluator.hasRecordStructure(ds) && info.parentDim.isUnlimited();
            tableConfig.structureType = (stnIsStruct ? TableConfig.StructureType.Structure : TableConfig.StructureType.PsuedoStructure);
            tableConfig.dimName = info.parentDim.getName();
            tableConfig.structName = (stnIsStruct ? "record" : tableConfig.dimName);
        }
        return tableConfig;
    }
    
    private TableConfig makeRaggedContiguous(final NetcdfDataset ds, final Dimension parentDim, final Dimension childDim, final Formatter errlog) throws IOException {
        final TableConfig obsTable = new TableConfig(Table.Type.Contiguous, childDim.getName());
        obsTable.dimName = childDim.getName();
        obsTable.lat = this.matchAxisTypeAndDimension(ds, AxisType.Lat, childDim);
        obsTable.lon = this.matchAxisTypeAndDimension(ds, AxisType.Lon, childDim);
        obsTable.elev = this.matchAxisTypeAndDimension(ds, AxisType.Height, childDim);
        obsTable.time = this.matchAxisTypeAndDimension(ds, AxisType.Time, childDim);
        final boolean obsIsStruct = Evaluator.hasRecordStructure(ds) && childDim.isUnlimited();
        obsTable.structName = (obsIsStruct ? "record" : childDim.getName());
        obsTable.structureType = (obsIsStruct ? TableConfig.StructureType.Structure : TableConfig.StructureType.PsuedoStructure);
        final Variable ragged_rowSize = Evaluator.getVariableWithAttributeValue(ds, "CF:ragged_row_count", childDim.getName());
        if (null == ragged_rowSize || ragged_rowSize.getRank() == 0 || ragged_rowSize.getDimension(0).getName() != parentDim.getName()) {
            errlog.format("there must be a ragged_row_count variable with outer dimension that matches latitude/longitude dimension %s%n", parentDim.getName());
            return null;
        }
        obsTable.numRecords = ragged_rowSize.getName();
        return obsTable;
    }
    
    private TableConfig makeRaggedIndex(final NetcdfDataset ds, final Dimension parentDim, final Dimension childDim, final Formatter errlog) throws IOException {
        final TableConfig obsTable = new TableConfig(Table.Type.ParentIndex, childDim.getName());
        obsTable.dimName = childDim.getName();
        obsTable.lat = this.matchAxisTypeAndDimension(ds, AxisType.Lat, childDim);
        obsTable.lon = this.matchAxisTypeAndDimension(ds, AxisType.Lon, childDim);
        obsTable.elev = this.matchAxisTypeAndDimension(ds, AxisType.Height, childDim);
        obsTable.time = this.matchAxisTypeAndDimension(ds, AxisType.Time, childDim);
        final boolean obsIsStruct = Evaluator.hasRecordStructure(ds) && childDim.isUnlimited();
        obsTable.structName = (obsIsStruct ? "record" : childDim.getName());
        obsTable.structureType = (obsIsStruct ? TableConfig.StructureType.Structure : TableConfig.StructureType.PsuedoStructure);
        final Variable ragged_parentIndex = Evaluator.getVariableWithAttributeValue(ds, "CF:ragged_parent_index", parentDim.getName());
        if (null == ragged_parentIndex || ragged_parentIndex.getRank() == 0 || ragged_parentIndex.getDimension(0).getName() != childDim.getName()) {
            errlog.format("there must be a ragged_parent_index variable with outer dimension that matches obs dimension %s%n", childDim.getName());
            return null;
        }
        obsTable.parentIndex = ragged_parentIndex.getName();
        return obsTable;
    }
    
    private TableConfig makeMultidimInner(final NetcdfDataset ds, final TableConfig parentTable, final Dimension obsDim, final Formatter errlog) throws IOException {
        final Dimension parentDim = ds.findDimension(parentTable.dimName);
        final Table.Type obsTableType = (parentTable.structureType == TableConfig.StructureType.PsuedoStructure) ? Table.Type.MultidimInnerPsuedo : Table.Type.MultidimInner;
        final TableConfig obsTable = new TableConfig(obsTableType, obsDim.getName());
        obsTable.lat = this.matchAxisTypeAndDimension(ds, AxisType.Lat, parentDim, obsDim);
        obsTable.lon = this.matchAxisTypeAndDimension(ds, AxisType.Lon, parentDim, obsDim);
        obsTable.elev = this.matchAxisTypeAndDimension(ds, AxisType.Height, parentDim, obsDim);
        obsTable.time = this.matchAxisTypeAndDimension(ds, AxisType.Time, parentDim, obsDim);
        List<String> obsVars = null;
        final List<Variable> vars = ds.getVariables();
        final List<String> parentVars = new ArrayList<String>(vars.size());
        obsVars = new ArrayList<String>(vars.size());
        for (final Variable orgV : vars) {
            if (orgV instanceof Structure) {
                continue;
            }
            final Dimension dim0 = orgV.getDimension(0);
            if (dim0 == null || !dim0.equals(parentDim)) {
                continue;
            }
            if (orgV.getRank() == 1 || (orgV.getRank() == 2 && orgV.getDataType() == DataType.CHAR)) {
                parentVars.add(orgV.getShortName());
            }
            else {
                final Dimension dim2 = orgV.getDimension(1);
                if (dim2 == null || !dim2.equals(obsDim)) {
                    continue;
                }
                obsVars.add(orgV.getShortName());
            }
        }
        parentTable.vars = parentVars;
        obsTable.structureType = parentTable.structureType;
        obsTable.outerName = parentDim.getName();
        obsTable.innerName = obsDim.getName();
        obsTable.dimName = ((parentTable.structureType == TableConfig.StructureType.PsuedoStructure) ? obsTable.outerName : obsTable.innerName);
        obsTable.structName = obsDim.getName();
        obsTable.vars = obsVars;
        return obsTable;
    }
    
    private TableConfig makeMultidimInner3D(final NetcdfDataset ds, final TableConfig outerTable, final TableConfig middleTable, final Dimension innerDim, final Formatter errlog) throws IOException {
        final Dimension outerDim = ds.findDimension(outerTable.dimName);
        final Dimension middleDim = ds.findDimension(middleTable.innerName);
        final Table.Type obsTableType = (outerTable.structureType == TableConfig.StructureType.PsuedoStructure) ? Table.Type.MultidimInnerPsuedo3D : Table.Type.MultidimInner3D;
        final TableConfig obsTable = new TableConfig(obsTableType, innerDim.getName());
        obsTable.structureType = TableConfig.StructureType.PsuedoStructure2D;
        obsTable.dimName = outerTable.dimName;
        obsTable.outerName = middleTable.innerName;
        obsTable.innerName = innerDim.getName();
        obsTable.structName = innerDim.getName();
        obsTable.lat = this.matchAxisTypeAndDimension(ds, AxisType.Lat, outerDim, middleDim, innerDim);
        obsTable.lon = this.matchAxisTypeAndDimension(ds, AxisType.Lon, outerDim, middleDim, innerDim);
        obsTable.elev = this.matchAxisTypeAndDimension(ds, AxisType.Height, outerDim, middleDim, innerDim);
        obsTable.time = this.matchAxisTypeAndDimension(ds, AxisType.Time, outerDim, middleDim, innerDim);
        final List<Variable> vars = ds.getVariables();
        final List<String> outerVars = new ArrayList<String>(vars.size());
        final List<String> middleVars = new ArrayList<String>(vars.size());
        final List<String> innerVars = new ArrayList<String>(vars.size());
        for (final Variable orgV : vars) {
            if (orgV instanceof Structure) {
                continue;
            }
            if (orgV.getRank() == 1 || (orgV.getRank() == 2 && orgV.getDataType() == DataType.CHAR)) {
                if (!outerDim.equals(orgV.getDimension(0))) {
                    continue;
                }
                outerVars.add(orgV.getShortName());
            }
            else if (orgV.getRank() == 2) {
                if (!outerDim.equals(orgV.getDimension(0)) || !middleDim.equals(orgV.getDimension(1))) {
                    continue;
                }
                middleVars.add(orgV.getShortName());
            }
            else {
                if (orgV.getRank() != 3 || !outerDim.equals(orgV.getDimension(0)) || !middleDim.equals(orgV.getDimension(1)) || !innerDim.equals(orgV.getDimension(2))) {
                    continue;
                }
                innerVars.add(orgV.getShortName());
            }
        }
        outerTable.vars = outerVars;
        middleTable.vars = middleVars;
        obsTable.vars = innerVars;
        return obsTable;
    }
    
    private TableConfig makeSingle(final NetcdfDataset ds, final Dimension obsDim, final Formatter errlog) throws IOException {
        final Table.Type obsTableType = Table.Type.Structure;
        final TableConfig obsTable = new TableConfig(obsTableType, "single");
        obsTable.dimName = obsDim.getName();
        obsTable.lat = this.matchAxisTypeAndDimension(ds, AxisType.Lat, obsDim);
        obsTable.lon = this.matchAxisTypeAndDimension(ds, AxisType.Lon, obsDim);
        obsTable.elev = this.matchAxisTypeAndDimension(ds, AxisType.Height, obsDim);
        obsTable.time = this.matchAxisTypeAndDimension(ds, AxisType.Time, obsDim);
        final boolean obsIsStruct = Evaluator.hasRecordStructure(ds) && obsDim.isUnlimited();
        obsTable.structName = (obsIsStruct ? "record" : obsDim.getName());
        obsTable.structureType = (obsIsStruct ? TableConfig.StructureType.Structure : TableConfig.StructureType.PsuedoStructure);
        return obsTable;
    }
    
    private TableConfig makeMiddleTable(final NetcdfDataset ds, final TableConfig parentTable, final Dimension obsDim, final Formatter errlog) throws IOException {
        throw new UnsupportedOperationException("CFpointObs: middleTable encoding");
    }
    
    @Override
    protected String matchAxisTypeAndDimension(final NetcdfDataset ds, final AxisType type, final Dimension outer) {
        final Variable var = CoordSysEvaluator.findCoordByType(ds, type, new CoordSysEvaluator.Predicate() {
            public boolean match(final CoordinateAxis axis) {
                if (outer == null && axis.getRank() == 0) {
                    return true;
                }
                if (outer != null && axis.getRank() == 1 && outer.equals(axis.getDimension(0))) {
                    return true;
                }
                if (axis.getParentStructure() != null) {
                    final Structure parent = axis.getParentStructure();
                    if (outer != null && parent.getRank() == 1 && outer.equals(parent.getDimension(0))) {
                        return true;
                    }
                }
                return false;
            }
        });
        if (var == null) {
            return null;
        }
        return var.getShortName();
    }
    
    private enum Encoding
    {
        single, 
        multidim, 
        raggedContiguous, 
        raggedIndex, 
        flat;
    }
    
    private class EncodingInfo
    {
        Encoding encoding;
        Dimension parentDim;
        Dimension childDim;
        Dimension grandChildDim;
        
        EncodingInfo(final Encoding encoding, final Dimension parentDim) {
            this.encoding = encoding;
            this.parentDim = parentDim;
        }
        
        EncodingInfo(final Encoding encoding, final Dimension parentDim, final Dimension childDim) {
            this.encoding = encoding;
            this.parentDim = parentDim;
            this.childDim = childDim;
        }
        
        EncodingInfo(final Encoding encoding, final Dimension parentDim, final Dimension childDim, final Dimension grandChildDim) {
            this.encoding = encoding;
            this.parentDim = parentDim;
            this.childDim = childDim;
            this.grandChildDim = grandChildDim;
        }
        
        EncodingInfo(final Encoding encoding, final Variable v) {
            this.encoding = encoding;
            this.parentDim = ((v == null) ? null : v.getDimension(0));
        }
    }
}
