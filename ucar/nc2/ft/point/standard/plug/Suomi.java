// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard.plug;

import java.io.IOException;
import ucar.nc2.ft.point.standard.PointConfigXML;
import ucar.nc2.ft.point.standard.TableConfig;
import java.util.Formatter;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.ft.point.standard.TableConfigurerImpl;

public class Suomi extends TableConfigurerImpl
{
    public boolean isMine(final FeatureType wantFeatureType, final NetcdfDataset ds) {
        final String center = ds.findAttValueIgnoreCase(null, "Convention", null);
        return center != null && center.equals("Suomi-Station-CDM");
    }
    
    public TableConfig getConfig(final FeatureType wantFeatureType, final NetcdfDataset ds, final Formatter errlog) throws IOException {
        final PointConfigXML reader = new PointConfigXML();
        return reader.readConfigXMLfromResource("resources/nj22/pointConfig/Suomi.xml", wantFeatureType, ds, errlog);
    }
}
