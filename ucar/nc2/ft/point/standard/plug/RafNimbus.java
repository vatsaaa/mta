// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard.plug;

import java.io.IOException;
import ucar.nc2.Dimension;
import ucar.nc2.dataset.CoordinateAxis;
import ucar.nc2.ft.point.standard.Evaluator;
import ucar.nc2.ft.point.standard.CoordSysEvaluator;
import ucar.nc2.constants.AxisType;
import ucar.nc2.ft.point.standard.Table;
import ucar.nc2.ft.point.standard.TableConfig;
import java.util.Formatter;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.ft.point.standard.TableConfigurerImpl;

public class RafNimbus extends TableConfigurerImpl
{
    public boolean isMine(final FeatureType wantFeatureType, final NetcdfDataset ds) {
        final String center = ds.findAttValueIgnoreCase(null, "Convention", null);
        return center != null && center.equals("NCAR-RAF/nimbus");
    }
    
    public TableConfig getConfig(final FeatureType wantFeatureType, final NetcdfDataset ds, final Formatter errlog) throws IOException {
        final TableConfig topTable = new TableConfig(Table.Type.Top, "singleTrajectory");
        final CoordinateAxis coordAxis = CoordSysEvaluator.findCoordByType(ds, AxisType.Time);
        if (coordAxis == null) {
            errlog.format("Cant find a time coordinate", new Object[0]);
            return null;
        }
        final Dimension innerDim = coordAxis.getDimension(0);
        final boolean obsIsStruct = Evaluator.hasRecordStructure(ds) && innerDim.isUnlimited();
        final TableConfig obsTable = new TableConfig(Table.Type.Structure, innerDim.getName());
        obsTable.dimName = innerDim.getName();
        obsTable.time = coordAxis.getName();
        obsTable.structName = (obsIsStruct ? "record" : innerDim.getName());
        obsTable.structureType = (obsIsStruct ? TableConfig.StructureType.Structure : TableConfig.StructureType.PsuedoStructure);
        CoordSysEvaluator.findCoordWithDimension(obsTable, ds, innerDim);
        topTable.addChild(obsTable);
        return topTable;
    }
}
