// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard.plug;

import java.io.IOException;
import ucar.nc2.constants.AxisType;
import ucar.nc2.ft.point.standard.Evaluator;
import ucar.nc2.ft.point.standard.Table;
import ucar.nc2.ft.point.standard.TableConfig;
import java.util.Formatter;
import java.util.StringTokenizer;
import ucar.nc2.Variable;
import ucar.nc2.ft.FeatureDatasetFactoryManager;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.ft.point.standard.TableConfigurerImpl;

public class UnidataPointFeature extends TableConfigurerImpl
{
    private static final String STN_NAME = "name";
    private static final String STN_LAT = "Latitude";
    private static final String STN_LON = "Longitude";
    private static final String STN_ELEV = "Height_of_station";
    
    public boolean isMine(final FeatureType wantFeatureType, final NetcdfDataset ds) {
        final FeatureType featureType = FeatureDatasetFactoryManager.findFeatureType(ds);
        if (featureType != FeatureType.STATION_PROFILE) {
            return false;
        }
        final String conv = ds.findAttValueIgnoreCase(null, "Conventions", null);
        if (conv == null) {
            return false;
        }
        final StringTokenizer stoke = new StringTokenizer(conv, ",");
        while (stoke.hasMoreTokens()) {
            final String toke = stoke.nextToken().trim();
            if (toke.equalsIgnoreCase("Unidata Point Feature v1.0")) {
                return true;
            }
        }
        return false;
    }
    
    public TableConfig getConfig(final FeatureType wantFeatureType, final NetcdfDataset ds, final Formatter errlog) throws IOException {
        final TableConfig nt = new TableConfig(Table.Type.ArrayStructure, "station");
        nt.featureType = FeatureType.STATION_PROFILE;
        nt.structName = "station";
        nt.stnId = "name";
        nt.lat = "Latitude";
        nt.lon = "Longitude";
        nt.elev = "Height_of_station";
        final TableConfig obs = new TableConfig(Table.Type.Structure, "obsRecord");
        obs.structName = "record";
        obs.dimName = Evaluator.getDimensionName(ds, "record", errlog);
        obs.lat = UnidataPointDatasetHelper.getCoordinateName(ds, AxisType.Lat);
        obs.lon = UnidataPointDatasetHelper.getCoordinateName(ds, AxisType.Lon);
        obs.elev = UnidataPointDatasetHelper.getCoordinateName(ds, AxisType.Height);
        obs.time = UnidataPointDatasetHelper.getCoordinateName(ds, AxisType.Time);
        obs.stnId = Evaluator.getVariableName(ds, "name", errlog);
        nt.addChild(obs);
        final TableConfig levels = new TableConfig(Table.Type.Structure, "seq1");
        levels.structName = "seq1";
        levels.elev = UnidataPointDatasetHelper.getCoordinateName(ds, AxisType.Height);
        obs.addChild(levels);
        return nt;
    }
}
