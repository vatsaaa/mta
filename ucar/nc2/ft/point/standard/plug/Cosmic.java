// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard.plug;

import java.io.IOException;
import ucar.nc2.ft.point.standard.PointConfigXML;
import ucar.nc2.ft.point.standard.TableConfig;
import java.util.Formatter;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.ft.point.standard.TableConfigurerImpl;

public class Cosmic extends TableConfigurerImpl
{
    public boolean isMine(final FeatureType wantFeatureType, final NetcdfDataset ds) {
        final String center = ds.findAttValueIgnoreCase(null, "center", null);
        return center != null && center.equals("UCAR/CDAAC");
    }
    
    public TableConfig getConfig(final FeatureType wantFeatureType, final NetcdfDataset ds, final Formatter errlog) throws IOException {
        final PointConfigXML reader = new PointConfigXML();
        if (ds.getConventionUsed().equalsIgnoreCase("Cosmic1")) {
            return reader.readConfigXMLfromResource("resources/nj22/pointConfig/Cosmic1.xml", wantFeatureType, ds, errlog);
        }
        if (ds.getConventionUsed().equalsIgnoreCase("Cosmic2")) {
            return reader.readConfigXMLfromResource("resources/nj22/pointConfig/Cosmic2.xml", wantFeatureType, ds, errlog);
        }
        if (ds.getConventionUsed().equalsIgnoreCase("Cosmic3")) {
            return reader.readConfigXMLfromResource("resources/nj22/pointConfig/Cosmic3.xml", wantFeatureType, ds, errlog);
        }
        return null;
    }
}
