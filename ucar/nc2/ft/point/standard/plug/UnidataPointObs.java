// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard.plug;

import org.slf4j.LoggerFactory;
import ucar.nc2.dataset.StructureDS;
import ucar.nc2.Dimension;
import ucar.nc2.ft.point.standard.Join;
import ucar.nc2.ft.point.standard.JoinParentIndex;
import java.util.List;
import ucar.nc2.Group;
import ucar.nc2.dataset.StructurePseudoDS;
import ucar.nc2.constants.AxisType;
import ucar.nc2.ft.point.standard.Table;
import ucar.nc2.ft.point.standard.Evaluator;
import ucar.nc2.NetcdfFile;
import ucar.nc2.ft.point.standard.TableConfig;
import java.util.Formatter;
import java.util.StringTokenizer;
import ucar.nc2.Variable;
import ucar.nc2.ft.FeatureDatasetFactoryManager;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.constants.FeatureType;
import org.slf4j.Logger;
import ucar.nc2.ft.point.standard.TableConfigurerImpl;

public class UnidataPointObs extends TableConfigurerImpl
{
    private static Logger log;
    
    public boolean isMine(final FeatureType wantFeatureType, final NetcdfDataset ds) {
        if (wantFeatureType != FeatureType.ANY_POINT && wantFeatureType != FeatureType.STATION && wantFeatureType != FeatureType.POINT) {
            return false;
        }
        final FeatureType ft = FeatureDatasetFactoryManager.findFeatureType(ds);
        if (ft == null || (ft != FeatureType.STATION && ft != FeatureType.POINT)) {
            return false;
        }
        final String conv = ds.findAttValueIgnoreCase(null, "Conventions", null);
        if (conv == null) {
            return false;
        }
        final StringTokenizer stoke = new StringTokenizer(conv, ",");
        while (stoke.hasMoreTokens()) {
            final String toke = stoke.nextToken().trim();
            if (toke.equalsIgnoreCase("Unidata Observation Dataset v1.0")) {
                return true;
            }
        }
        return false;
    }
    
    public TableConfig getConfig(final FeatureType wantFeatureType, final NetcdfDataset ds, final Formatter errlog) {
        final Dimension obsDim = UnidataPointDatasetHelper.findObsDimension(ds);
        if (obsDim == null) {
            errlog.format("Must have an Observation dimension: named by global attribute 'observationDimension', or unlimited dimension", new Object[0]);
            return null;
        }
        final boolean hasStruct = Evaluator.hasRecordStructure(ds);
        FeatureType ft = Evaluator.getFeatureType(ds, ":cdm_datatype", null);
        if (ft == null) {
            ft = Evaluator.getFeatureType(ds, ":cdm_data_type", null);
        }
        if (ft == FeatureType.POINT) {
            final TableConfig obsTable = new TableConfig(Table.Type.Structure, hasStruct ? "record" : obsDim.getName());
            obsTable.structureType = (hasStruct ? TableConfig.StructureType.Structure : TableConfig.StructureType.PsuedoStructure);
            obsTable.featureType = FeatureType.POINT;
            obsTable.structName = "record";
            obsTable.time = UnidataPointDatasetHelper.getCoordinateName(ds, AxisType.Time, obsDim);
            obsTable.lat = UnidataPointDatasetHelper.getCoordinateName(ds, AxisType.Lat, obsDim);
            obsTable.lon = UnidataPointDatasetHelper.getCoordinateName(ds, AxisType.Lon, obsDim);
            obsTable.elev = UnidataPointDatasetHelper.getCoordinateName(ds, AxisType.Height, obsDim);
            obsTable.dimName = obsDim.getName();
            return obsTable;
        }
        if (ft == FeatureType.STATION && wantFeatureType == FeatureType.POINT) {
            final TableConfig obsTable = new TableConfig(Table.Type.Structure, hasStruct ? "record" : obsDim.getName());
            obsTable.structureType = (hasStruct ? TableConfig.StructureType.Structure : TableConfig.StructureType.PsuedoStructure);
            obsTable.featureType = FeatureType.POINT;
            obsTable.dimName = obsDim.getName();
            obsTable.structName = "record";
            obsTable.time = UnidataPointDatasetHelper.getCoordinateName(ds, AxisType.Time, obsDim);
            obsTable.lat = UnidataPointDatasetHelper.getCoordinateName(ds, AxisType.Lat, obsDim);
            obsTable.lon = UnidataPointDatasetHelper.getCoordinateName(ds, AxisType.Lon, obsDim);
            obsTable.stnAlt = UnidataPointDatasetHelper.getCoordinateName(ds, AxisType.Height, obsDim);
            if (obsTable.lat != null && obsTable.lon != null) {
                return obsTable;
            }
            final String parentIndexVar = UnidataPointDatasetHelper.findVariableName(ds, "parent_index");
            if (parentIndexVar == null) {
                errlog.format("Must have a parent_index variable", new Object[0]);
                return null;
            }
            final Dimension stationDim = UnidataPointDatasetHelper.findDimension(ds, "station");
            if (stationDim == null) {
                errlog.format("Must have a station dimension", new Object[0]);
                return null;
            }
            obsTable.lat = UnidataPointDatasetHelper.getCoordinateName(ds, AxisType.Lat, stationDim);
            obsTable.lon = UnidataPointDatasetHelper.getCoordinateName(ds, AxisType.Lon, stationDim);
            obsTable.elev = UnidataPointDatasetHelper.getCoordinateName(ds, AxisType.Height, stationDim);
            final StructureDS stns = new StructurePseudoDS(ds, null, "stationPsuedoStructure", null, stationDim);
            obsTable.addJoin(new JoinParentIndex(stns, parentIndexVar));
            return obsTable;
        }
        else {
            if (ft == FeatureType.TRAJECTORY) {
                final TableConfig obsTable = new TableConfig(Table.Type.Structure, hasStruct ? "record" : obsDim.getName());
                obsTable.structureType = (hasStruct ? TableConfig.StructureType.Structure : TableConfig.StructureType.PsuedoStructure);
                obsTable.featureType = FeatureType.TRAJECTORY;
                obsTable.time = UnidataPointDatasetHelper.getCoordinateName(ds, AxisType.Time, obsDim);
                obsTable.lat = UnidataPointDatasetHelper.getCoordinateName(ds, AxisType.Lat, obsDim);
                obsTable.lon = UnidataPointDatasetHelper.getCoordinateName(ds, AxisType.Lon, obsDim);
                obsTable.elev = UnidataPointDatasetHelper.getCoordinateName(ds, AxisType.Height, obsDim);
                obsTable.dimName = obsDim.getName();
                final Dimension trajDim = UnidataPointDatasetHelper.findDimension(ds, "trajectory");
                if (trajDim != null) {
                    UnidataPointObs.log.error("Ignoring trajectory structure " + ds.getLocation());
                }
                return obsTable;
            }
            final Dimension stationDim2 = UnidataPointDatasetHelper.findDimension(ds, "station");
            if (stationDim2 == null) {
                errlog.format("Must have a dimension named station, or named by global attribute 'stationDimension'", new Object[0]);
                return null;
            }
            final String lastVar = UnidataPointDatasetHelper.findVariableName(ds, "lastChild");
            final String prevVar = UnidataPointDatasetHelper.findVariableName(ds, "prevChild");
            final String firstVar = UnidataPointDatasetHelper.findVariableName(ds, "firstChild");
            final String nextVar = UnidataPointDatasetHelper.findVariableName(ds, "nextChild");
            final String numChildrenVar = UnidataPointDatasetHelper.findVariableName(ds, "numChildren");
            final boolean isForwardLinkedList = firstVar != null && nextVar != null;
            final boolean isBackwardLinkedList = lastVar != null && prevVar != null;
            final boolean isContiguousList = !isForwardLinkedList && !isBackwardLinkedList && firstVar != null && numChildrenVar != null;
            final boolean isMultiDim = !isForwardLinkedList && !isBackwardLinkedList && !isContiguousList;
            final TableConfig stationTable = new TableConfig(Table.Type.Structure, "station");
            stationTable.structureType = TableConfig.StructureType.PsuedoStructure;
            stationTable.featureType = FeatureType.STATION;
            stationTable.dimName = stationDim2.getName();
            stationTable.limit = Evaluator.getVariableName(ds, "number_stations", null);
            stationTable.stnId = Evaluator.getVariableName(ds, "station_id", null);
            stationTable.stnDesc = Evaluator.getVariableName(ds, "station_description", null);
            stationTable.stnWmoId = Evaluator.getVariableName(ds, "wmo_id", null);
            stationTable.lat = UnidataPointDatasetHelper.getCoordinateName(ds, AxisType.Lat);
            stationTable.lon = UnidataPointDatasetHelper.getCoordinateName(ds, AxisType.Lon);
            stationTable.stnAlt = UnidataPointDatasetHelper.getCoordinateName(ds, AxisType.Height);
            TableConfig obsTable2;
            if (isMultiDim) {
                obsTable2 = new TableConfig(Table.Type.MultidimInner, "obs");
                obsTable2.outerName = stationDim2.getName();
                obsTable2.innerName = obsDim.getName();
                obsTable2.dimName = obsDim.getName();
            }
            else {
                final Table.Type obsType = (isForwardLinkedList || isBackwardLinkedList) ? Table.Type.LinkedList : Table.Type.Contiguous;
                obsTable2 = new TableConfig(obsType, hasStruct ? "record" : obsDim.getName());
                obsTable2.structureType = (hasStruct ? TableConfig.StructureType.Structure : TableConfig.StructureType.PsuedoStructure);
                obsTable2.structName = "record";
                if (isForwardLinkedList) {
                    obsTable2.start = firstVar;
                    obsTable2.next = nextVar;
                }
                else if (isBackwardLinkedList) {
                    obsTable2.start = lastVar;
                    obsTable2.next = prevVar;
                }
                else if (isContiguousList) {
                    obsTable2.start = firstVar;
                }
                obsTable2.numRecords = numChildrenVar;
                obsTable2.parentIndex = Evaluator.getVariableName(ds, "parent_index", null);
            }
            obsTable2.dimName = obsDim.getName();
            obsTable2.time = UnidataPointDatasetHelper.getCoordinateName(ds, AxisType.Time);
            obsTable2.timeNominal = Evaluator.getVariableName(ds, "time_nominal", null);
            stationTable.addChild(obsTable2);
            return stationTable;
        }
    }
    
    static {
        UnidataPointObs.log = LoggerFactory.getLogger(UnidataPointObs.class);
    }
}
