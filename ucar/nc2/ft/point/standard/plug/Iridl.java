// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard.plug;

import ucar.nc2.Dimension;
import ucar.nc2.ft.point.standard.Table;
import ucar.nc2.ft.point.standard.CoordSysEvaluator;
import ucar.nc2.constants.AxisType;
import ucar.nc2.ft.point.standard.TableConfig;
import java.util.Formatter;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.ft.point.standard.TableConfigurerImpl;

public class Iridl extends TableConfigurerImpl
{
    public boolean isMine(final FeatureType wantFeatureType, final NetcdfDataset ds) {
        return ds.findAttValueIgnoreCase(null, "Conventions", "").equalsIgnoreCase("IRIDL");
    }
    
    public TableConfig getConfig(final FeatureType wantFeatureType, final NetcdfDataset ds, final Formatter errlog) {
        final Dimension stationDim = CoordSysEvaluator.findDimensionByType(ds, AxisType.Lat);
        if (stationDim == null) {
            errlog.format("Must have a latitude coordinate", new Object[0]);
            return null;
        }
        final Variable stationVar = ds.findVariable(stationDim.getName());
        if (stationVar == null) {
            errlog.format("Must have a station coordinate variable", new Object[0]);
            return null;
        }
        final Dimension obsDim = CoordSysEvaluator.findDimensionByType(ds, AxisType.Time);
        if (obsDim == null) {
            errlog.format("Must have a Time coordinate", new Object[0]);
            return null;
        }
        final TableConfig stationTable = new TableConfig(Table.Type.Structure, "station");
        stationTable.structName = "station";
        stationTable.structureType = TableConfig.StructureType.PsuedoStructure;
        stationTable.featureType = FeatureType.STATION;
        stationTable.dimName = stationDim.getName();
        stationTable.stnId = stationVar.getName();
        stationTable.lat = CoordSysEvaluator.findCoordNameByType(ds, AxisType.Lat);
        stationTable.lon = CoordSysEvaluator.findCoordNameByType(ds, AxisType.Lon);
        stationTable.stnAlt = CoordSysEvaluator.findCoordNameByType(ds, AxisType.Height);
        final TableConfig obsTable = new TableConfig(Table.Type.MultidimInner, "obs");
        obsTable.time = CoordSysEvaluator.findCoordNameByType(ds, AxisType.Time);
        obsTable.outerName = stationDim.getName();
        obsTable.dimName = obsDim.getName();
        stationTable.addChild(obsTable);
        return stationTable;
    }
}
