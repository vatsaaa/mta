// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard.plug;

import ucar.nc2.Dimension;
import java.util.Iterator;
import java.util.List;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Structure;
import ucar.nc2.Variable;
import ucar.nc2.constants.AxisType;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.Attribute;
import ucar.ma2.DataType;
import java.util.Date;
import ucar.nc2.units.DateUnit;
import ucar.nc2.dataset.NetcdfDataset;

public class UnidataPointDatasetHelper
{
    public static Date getStartDate(final NetcdfDataset ds, final DateUnit timeUnit) {
        return getDate(ds, timeUnit, "time_coverage_start");
    }
    
    public static Date getEndDate(final NetcdfDataset ds, final DateUnit timeUnit) {
        return getDate(ds, timeUnit, "time_coverage_end");
    }
    
    private static Date getDate(final NetcdfDataset ds, final DateUnit timeUnit, final String attName) {
        final Attribute att = ds.findGlobalAttributeIgnoreCase(attName);
        if (null == att) {
            throw new IllegalArgumentException("Must have a global attribute named " + attName);
        }
        Date result;
        if (att.getDataType() == DataType.STRING) {
            result = DateUnit.getStandardOrISO(att.getStringValue());
            if (result == null) {
                final double val = Double.parseDouble(att.getStringValue());
                result = timeUnit.makeDate(val);
            }
        }
        else {
            if (timeUnit == null) {
                throw new IllegalArgumentException(attName + " must be a ISO or udunit date string");
            }
            final double val = att.getNumericValue().doubleValue();
            result = timeUnit.makeDate(val);
        }
        return result;
    }
    
    public static LatLonRect getBoundingBox(final NetcdfDataset ds) {
        final double lat_max = getAttAsDouble(ds, "geospatial_lat_max");
        final double lat_min = getAttAsDouble(ds, "geospatial_lat_min");
        final double lon_max = getAttAsDouble(ds, "geospatial_lon_max");
        final double lon_min = getAttAsDouble(ds, "geospatial_lon_min");
        return new LatLonRect(new LatLonPointImpl(lat_min, lon_min), lat_max - lat_min, lon_max - lon_min);
    }
    
    private static double getAttAsDouble(final NetcdfDataset ds, final String attname) {
        final Attribute att = ds.findGlobalAttributeIgnoreCase(attname);
        if (null == att) {
            throw new IllegalArgumentException("Must have a " + attname + " global attribute");
        }
        if (att.getDataType() == DataType.STRING) {
            return Double.parseDouble(att.getStringValue());
        }
        return att.getNumericValue().doubleValue();
    }
    
    public static String getCoordinateName(final NetcdfDataset ds, final AxisType a) {
        final List<Variable> varList = ds.getVariables();
        for (final Variable v : varList) {
            if (v instanceof Structure) {
                final List<Variable> vars = ((Structure)v).getVariables();
                for (final Variable vs : vars) {
                    final String axisType = ds.findAttValueIgnoreCase(vs, "_CoordinateAxisType", null);
                    if (axisType != null && axisType.equals(a.toString())) {
                        return vs.getShortName();
                    }
                }
            }
            else {
                final String axisType2 = ds.findAttValueIgnoreCase(v, "_CoordinateAxisType", null);
                if (axisType2 != null && axisType2.equals(a.toString())) {
                    return v.getShortName();
                }
                continue;
            }
        }
        if (a == AxisType.Lat) {
            return findVariableName(ds, "latitude");
        }
        if (a == AxisType.Lon) {
            return findVariableName(ds, "longitude");
        }
        if (a == AxisType.Time) {
            return findVariableName(ds, "time");
        }
        if (a == AxisType.Height) {
            Variable v2 = findVariable(ds, "altitude");
            if (null == v2) {
                v2 = findVariable(ds, "depth");
            }
            if (v2 != null) {
                return v2.getShortName();
            }
        }
        return null;
    }
    
    public static String getCoordinateName(final NetcdfDataset ds, final AxisType a, final Dimension dim) {
        final String name = getCoordinateName(ds, a);
        if (name == null) {
            return null;
        }
        final Variable v = ds.findVariable(name);
        if (v == null) {
            return null;
        }
        if (v.isScalar()) {
            return null;
        }
        if (!v.getDimension(0).equals(dim)) {
            return null;
        }
        return name;
    }
    
    public static Variable getCoordinate(final NetcdfDataset ds, final AxisType a) {
        final List<Variable> varList = ds.getVariables();
        for (final Variable v : varList) {
            if (v instanceof Structure) {
                final List<Variable> vars = ((Structure)v).getVariables();
                for (final Variable vs : vars) {
                    final String axisType = ds.findAttValueIgnoreCase(vs, "_CoordinateAxisType", null);
                    if (axisType != null && axisType.equals(a.toString())) {
                        return vs;
                    }
                }
            }
            else {
                final String axisType2 = ds.findAttValueIgnoreCase(v, "_CoordinateAxisType", null);
                if (axisType2 != null && axisType2.equals(a.toString())) {
                    return v;
                }
                continue;
            }
        }
        if (a == AxisType.Lat) {
            return findVariable(ds, "latitude");
        }
        if (a == AxisType.Lon) {
            return findVariable(ds, "longitude");
        }
        if (a == AxisType.Time) {
            return findVariable(ds, "time");
        }
        if (a == AxisType.Height) {
            Variable v2 = findVariable(ds, "altitude");
            if (null == v2) {
                v2 = findVariable(ds, "depth");
            }
            if (v2 != null) {
                return v2;
            }
        }
        return null;
    }
    
    public static String findVariableName(final NetcdfFile ds, final String name) {
        final Variable result = findVariable(ds, name);
        return (result == null) ? null : result.getShortName();
    }
    
    public static Variable findVariable(final NetcdfFile ds, final String name) {
        Variable result = ds.findVariable(name);
        if (result == null) {
            String aname = ds.findAttValueIgnoreCase(null, name + "_coordinate", null);
            if (aname != null) {
                result = ds.findVariable(aname);
            }
            else {
                aname = ds.findAttValueIgnoreCase(null, name + "_variable", null);
                if (aname != null) {
                    result = ds.findVariable(aname);
                }
            }
        }
        return result;
    }
    
    public static Dimension findDimension(final NetcdfFile ds, final String name) {
        Dimension result = ds.findDimension(name);
        if (result == null) {
            final String aname = ds.findAttValueIgnoreCase(null, name + "Dimension", null);
            if (aname != null) {
                result = ds.findDimension(aname);
            }
        }
        return result;
    }
    
    public static Dimension findObsDimension(final NetcdfFile ds) {
        Dimension result = null;
        final String aname = ds.findAttValueIgnoreCase(null, "observationDimension", null);
        if (aname != null) {
            result = ds.findDimension(aname);
        }
        if (result == null) {
            result = ds.getUnlimitedDimension();
        }
        return result;
    }
}
