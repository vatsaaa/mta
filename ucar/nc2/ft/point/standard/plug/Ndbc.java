// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard.plug;

import ucar.nc2.dataset.CoordinateAxis;
import ucar.nc2.Dimension;
import ucar.nc2.ft.point.standard.Table;
import ucar.nc2.ft.point.standard.Evaluator;
import ucar.nc2.ft.point.standard.CoordSysEvaluator;
import ucar.nc2.constants.AxisType;
import ucar.nc2.ft.point.standard.TableConfig;
import java.util.Formatter;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.ft.point.standard.TableConfigurerImpl;

public class Ndbc extends TableConfigurerImpl
{
    public boolean isMine(final FeatureType wantFeatureType, final NetcdfDataset ds) {
        if (!ds.findAttValueIgnoreCase(null, "Conventions", "").equalsIgnoreCase("COARDS")) {
            return false;
        }
        String dataProvider = ds.findAttValueIgnoreCase(null, "data_provider", null);
        if (dataProvider == null) {
            dataProvider = ds.findAttValueIgnoreCase(null, "institution", "");
        }
        return dataProvider.contains("National Data Buoy Center") && null != ds.findAttValueIgnoreCase(null, "station", null) && null != ds.findAttValueIgnoreCase(null, "location", null) && ds.hasUnlimitedDimension();
    }
    
    public TableConfig getConfig(final FeatureType wantFeatureType, final NetcdfDataset ds, final Formatter errlog) {
        Dimension obsDim = ds.getUnlimitedDimension();
        if (obsDim == null) {
            final CoordinateAxis axis = CoordSysEvaluator.findCoordByType(ds, AxisType.Time);
            if (axis != null && axis.isScalar()) {
                obsDim = axis.getDimension(0);
            }
        }
        if (obsDim == null) {
            errlog.format("Must have an Observation dimension: unlimited dimension, or from Time Coordinate", new Object[0]);
            return null;
        }
        final boolean hasStruct = Evaluator.hasRecordStructure(ds);
        if (wantFeatureType == FeatureType.POINT) {
            final TableConfig nt = new TableConfig(Table.Type.Structure, hasStruct ? "record" : obsDim.getName());
            nt.structName = "record";
            nt.structureType = (hasStruct ? TableConfig.StructureType.Structure : TableConfig.StructureType.PsuedoStructure);
            nt.featureType = FeatureType.POINT;
            CoordSysEvaluator.findCoords(nt, ds);
            return nt;
        }
        final TableConfig nt = new TableConfig(Table.Type.Top, "station");
        nt.featureType = FeatureType.STATION;
        nt.lat = CoordSysEvaluator.findCoordNameByType(ds, AxisType.Lat);
        nt.lon = CoordSysEvaluator.findCoordNameByType(ds, AxisType.Lon);
        nt.stnId = ds.findAttValueIgnoreCase(null, "station", null);
        nt.stnDesc = ds.findAttValueIgnoreCase(null, "description", null);
        if (nt.stnDesc == null) {
            nt.stnDesc = ds.findAttValueIgnoreCase(null, "comment", null);
        }
        final TableConfig obs = new TableConfig(Table.Type.Structure, hasStruct ? "record" : obsDim.getName());
        obs.structName = "record";
        obs.structureType = (hasStruct ? TableConfig.StructureType.Structure : TableConfig.StructureType.PsuedoStructure);
        obs.dimName = obsDim.getName();
        obs.time = CoordSysEvaluator.findCoordNameByType(ds, AxisType.Time);
        nt.addChild(obs);
        return nt;
    }
}
