// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard.plug;

import ucar.nc2.Variable;
import ucar.nc2.Dimension;
import ucar.nc2.ft.point.standard.Table;
import ucar.nc2.ft.point.standard.Evaluator;
import ucar.nc2.ft.point.standard.TableConfig;
import java.util.Formatter;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.ft.point.standard.TableConfigurerImpl;

public class Madis extends TableConfigurerImpl
{
    public boolean isMine(final FeatureType wantFeatureType, final NetcdfDataset ds) {
        if (wantFeatureType != FeatureType.ANY_POINT && wantFeatureType != FeatureType.STATION && wantFeatureType != FeatureType.POINT && wantFeatureType != FeatureType.STATION_PROFILE) {
            return false;
        }
        if (!ds.hasUnlimitedDimension()) {
            return false;
        }
        if (ds.findDimension("recNum") == null) {
            return false;
        }
        if (ds.findVariable("staticIds") == null) {
            return false;
        }
        if (ds.findVariable("nStaticIds") == null) {
            return false;
        }
        if (ds.findVariable("lastRecord") == null) {
            return false;
        }
        if (ds.findVariable("prevRecord") == null) {
            return false;
        }
        final VNames vn = this.getVariableNames(ds, null);
        return ds.findVariable(vn.lat) != null && ds.findVariable(vn.lon) != null && ds.findVariable(vn.obsTime) != null;
    }
    
    public TableConfig getConfig(final FeatureType wantFeatureType, final NetcdfDataset ds, final Formatter errlog) {
        final Dimension obsDim = Evaluator.getDimension(ds, "recNum", errlog);
        if (obsDim == null) {
            errlog.format("MADIS: must have an Observation dimension: named recNum", new Object[0]);
            return null;
        }
        final VNames vn = this.getVariableNames(ds, errlog);
        String levVarName = null;
        String levDimName = null;
        final boolean hasStruct = Evaluator.hasRecordStructure(ds);
        FeatureType ft = Evaluator.getFeatureType(ds, ":thredds_data_type", errlog);
        if (null == ft) {
            if (ds.findDimension("manLevel") != null && ds.findVariable("prMan") != null) {
                ft = FeatureType.STATION_PROFILE;
                levVarName = "prMan";
                levDimName = "manLevel";
            }
            else if (ds.findDimension("level") != null && ds.findVariable("levels") != null) {
                ft = FeatureType.STATION_PROFILE;
                levVarName = "levels";
                levDimName = "level";
            }
        }
        if (null == ft) {
            ft = FeatureType.POINT;
        }
        if (wantFeatureType == FeatureType.POINT || ft == FeatureType.POINT) {
            final TableConfig ptTable = new TableConfig(Table.Type.Structure, hasStruct ? "record" : obsDim.getName());
            ptTable.structName = "record";
            ptTable.featureType = FeatureType.POINT;
            ptTable.structureType = (hasStruct ? TableConfig.StructureType.Structure : TableConfig.StructureType.PsuedoStructure);
            ptTable.dimName = obsDim.getName();
            ptTable.time = vn.obsTime;
            ptTable.timeNominal = vn.nominalTime;
            ptTable.lat = vn.lat;
            ptTable.lon = vn.lon;
            ptTable.elev = vn.elev;
            return ptTable;
        }
        if (ft == FeatureType.STATION) {
            final TableConfig stnTable = new TableConfig(Table.Type.Construct, "station");
            stnTable.featureType = FeatureType.STATION;
            final TableConfig obs = new TableConfig(Table.Type.ParentId, "record");
            obs.parentIndex = vn.stnId;
            obs.dimName = Evaluator.getDimensionName(ds, "recNum", errlog);
            obs.time = vn.obsTime;
            obs.timeNominal = vn.nominalTime;
            obs.stnId = vn.stnId;
            obs.stnDesc = vn.stnDesc;
            obs.lat = vn.lat;
            obs.lon = vn.lon;
            obs.elev = vn.elev;
            stnTable.addChild(obs);
            return stnTable;
        }
        if (ft == FeatureType.STATION_PROFILE) {
            final TableConfig stnTable = new TableConfig(Table.Type.Construct, "station");
            stnTable.featureType = FeatureType.STATION_PROFILE;
            final TableConfig obs = new TableConfig(Table.Type.ParentId, "record");
            obs.parentIndex = vn.stnId;
            obs.dimName = Evaluator.getDimensionName(ds, "recNum", errlog);
            obs.time = vn.obsTime;
            obs.timeNominal = vn.nominalTime;
            obs.stnId = vn.stnId;
            obs.stnDesc = vn.stnDesc;
            obs.lat = vn.lat;
            obs.lon = vn.lon;
            obs.stnAlt = vn.elev;
            stnTable.addChild(obs);
            final TableConfig lev = new TableConfig(Table.Type.MultidimInner, "mandatory");
            lev.elev = levVarName;
            lev.outerName = obs.dimName;
            lev.innerName = levDimName;
            obs.addChild(lev);
            return stnTable;
        }
        return null;
    }
    
    protected VNames getVariableNames(final NetcdfDataset ds, final Formatter errlog) {
        final VNames vn = new VNames();
        String val = ds.findAttValueIgnoreCase(null, "stationLocationVariables", null);
        if (val == null) {
            val = ds.findAttValueIgnoreCase(null, "latLonVars", null);
        }
        if (val == null) {
            if (errlog != null) {
                errlog.format(" Cant find global attribute stationLocationVariables\n", new Object[0]);
            }
            vn.lat = "latitude";
            vn.lon = "longitude";
        }
        else {
            final String[] vals = val.split(",");
            if (vals.length > 0) {
                vn.lat = vals[0];
            }
            if (vals.length > 1) {
                vn.lon = vals[1];
            }
            if (vals.length > 2) {
                vn.elev = vals[2];
            }
        }
        val = ds.findAttValueIgnoreCase(null, "timeVariables", null);
        if (val == null) {
            if (errlog != null) {
                errlog.format(" Cant find global attribute timeVariables\n", new Object[0]);
            }
            vn.obsTime = "observationTime";
            vn.nominalTime = "reportTime";
        }
        else {
            final String[] vals = val.split(",");
            if (vals.length > 0) {
                vn.obsTime = vals[0];
            }
            if (vals.length > 1) {
                vn.nominalTime = vals[1];
            }
        }
        val = ds.findAttValueIgnoreCase(null, "stationDescriptionVariable", null);
        if (val == null) {
            if (errlog != null) {
                errlog.format(" Cant find global attribute stationDescriptionVariable\n", new Object[0]);
            }
            vn.stnDesc = "stationName";
        }
        else {
            vn.stnDesc = val;
        }
        val = ds.findAttValueIgnoreCase(null, "stationIdVariable", null);
        if (val == null) {
            val = ds.findAttValueIgnoreCase(null, "idVariables", null);
        }
        if (val == null) {
            if (errlog != null) {
                errlog.format(" Cant find global attribute stationIdVariable\n", new Object[0]);
            }
            vn.stnId = "stationId";
        }
        else {
            vn.stnId = val;
        }
        if (null != ds.findVariable("altitude")) {
            vn.elev = "altitude";
        }
        else if (null != ds.findVariable("elevation")) {
            vn.elev = "elevation";
        }
        return vn;
    }
    
    protected class VNames
    {
        String lat;
        String lon;
        String elev;
        String obsTime;
        String nominalTime;
        String stnId;
        String stnDesc;
    }
}
