// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard;

import ucar.ma2.StructureData;

public class Cursor
{
    StructureData[] tableData;
    int[] recnum;
    int currentIndex;
    
    Cursor(final int nlevels) {
        this.tableData = new StructureData[nlevels];
        this.recnum = new int[nlevels];
    }
    
    private int getParentIndex() {
        int indx;
        for (indx = this.currentIndex; this.tableData[indx] == null && indx < this.tableData.length - 1; ++indx) {}
        return indx;
    }
    
    StructureData getParentStructure() {
        return this.tableData[this.getParentIndex()];
    }
    
    int getParentRecnum() {
        return this.recnum[this.getParentIndex()];
    }
    
    Cursor copy() {
        final Cursor clone = new Cursor(this.tableData.length);
        clone.currentIndex = this.currentIndex;
        System.arraycopy(this.tableData, 0, clone.tableData, 0, this.tableData.length);
        System.arraycopy(this.recnum, 0, clone.recnum, 0, this.tableData.length);
        return clone;
    }
}
