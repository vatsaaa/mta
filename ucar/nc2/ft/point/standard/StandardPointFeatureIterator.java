// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard;

import ucar.unidata.geoloc.Station;
import ucar.nc2.ft.point.StationPointFeature;
import ucar.nc2.ft.point.PointFeatureImpl;
import ucar.nc2.ft.PointFeature;
import ucar.ma2.StructureData;
import java.io.IOException;
import ucar.nc2.ft.point.PointIteratorAbstract;
import ucar.ma2.StructureDataIterator;
import ucar.nc2.units.DateUnit;
import ucar.nc2.ft.point.PointIteratorFromStructureData;

public class StandardPointFeatureIterator extends PointIteratorFromStructureData
{
    protected NestedTable ft;
    protected DateUnit timeUnit;
    protected Cursor cursor;
    
    StandardPointFeatureIterator(final NestedTable ft, final DateUnit timeUnit, final StructureDataIterator structIter, final Cursor cursor) throws IOException {
        super(structIter, null);
        this.ft = ft;
        this.timeUnit = timeUnit;
        this.cursor = cursor;
    }
    
    @Override
    protected PointFeature makeFeature(final int recnum, final StructureData sdata) throws IOException {
        this.cursor.recnum[0] = recnum;
        this.cursor.tableData[0] = sdata;
        this.cursor.currentIndex = 0;
        this.ft.addParentJoin(this.cursor);
        if (this.isMissing()) {
            return null;
        }
        final double obsTime = this.ft.getObsTime(this.cursor);
        return new StandardPointFeature(this.cursor.copy(), this.timeUnit, obsTime);
    }
    
    protected boolean isMissing() throws IOException {
        return this.ft.isTimeMissing(this.cursor) || this.ft.isMissing(this.cursor);
    }
    
    private class StandardPointFeature extends PointFeatureImpl implements StationPointFeature
    {
        protected Cursor cursor;
        
        public StandardPointFeature(final Cursor cursor, final DateUnit timeUnit, final double obsTime) {
            super(timeUnit);
            this.cursor = cursor;
            this.obsTime = obsTime;
            this.nomTime = StandardPointFeatureIterator.this.ft.getNomTime(this.cursor);
            if (Double.isNaN(this.nomTime)) {
                this.nomTime = obsTime;
            }
            this.location = StandardPointFeatureIterator.this.ft.getEarthLocation(this.cursor);
        }
        
        public StructureData getData() {
            return StandardPointFeatureIterator.this.ft.makeObsStructureData(this.cursor);
        }
        
        public Station getStation() {
            return StandardPointFeatureIterator.this.ft.makeStation(this.cursor.getParentStructure());
        }
    }
}
