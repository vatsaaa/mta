// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard;

import java.util.Date;
import ucar.nc2.ft.PointFeature;
import ucar.nc2.ft.PointFeatureIterator;
import ucar.nc2.ft.point.ProfileFeatureImpl;
import ucar.nc2.ft.PointFeatureCollection;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.ft.PointFeatureCollectionIterator;
import ucar.nc2.ft.point.SectionFeatureImpl;
import ucar.nc2.ft.NestedPointFeatureCollection;
import ucar.nc2.ft.SectionFeature;
import ucar.ma2.StructureData;
import ucar.ma2.StructureDataIterator;
import ucar.nc2.ft.NestedPointFeatureCollectionIterator;
import java.io.IOException;
import ucar.nc2.units.DateUnit;
import ucar.nc2.ft.point.SectionCollectionImpl;

public class StandardSectionCollectionImpl extends SectionCollectionImpl
{
    private DateUnit timeUnit;
    private NestedTable ft;
    
    StandardSectionCollectionImpl(final NestedTable ft, final DateUnit timeUnit) throws IOException {
        super(ft.getName());
        this.ft = ft;
        this.timeUnit = timeUnit;
    }
    
    public NestedPointFeatureCollectionIterator getNestedPointFeatureCollectionIterator(final int bufferSize) throws IOException {
        return new NestedPointFeatureCollectionIterator() {
            private StructureDataIterator sdataIter = StandardSectionCollectionImpl.this.ft.getRootFeatureDataIterator(-1);
            private StructureData nextSection;
            
            public SectionFeature next() throws IOException {
                final Cursor cursor = new Cursor(StandardSectionCollectionImpl.this.ft.getNumberOfLevels());
                cursor.recnum[2] = this.sdataIter.getCurrentRecno();
                cursor.tableData[2] = this.nextSection;
                cursor.currentIndex = 2;
                StandardSectionCollectionImpl.this.ft.addParentJoin(cursor);
                return new StandardSectionFeature(cursor);
            }
            
            public boolean hasNext() throws IOException {
                while (this.sdataIter.hasNext()) {
                    this.nextSection = this.sdataIter.next();
                    if (!StandardSectionCollectionImpl.this.ft.isFeatureMissing(this.nextSection)) {
                        return true;
                    }
                }
                return false;
            }
            
            public void setBufferSize(final int bytes) {
            }
        };
    }
    
    private class StandardSectionFeature extends SectionFeatureImpl
    {
        Cursor cursor;
        
        StandardSectionFeature(final Cursor cursor) {
            super(StandardSectionCollectionImpl.this.ft.getFeatureName(cursor));
            this.cursor = cursor;
        }
        
        public PointFeatureCollectionIterator getPointFeatureCollectionIterator(final int bufferSize) throws IOException {
            return new StandardSectionFeatureIterator(this.cursor.copy());
        }
        
        public NestedPointFeatureCollection subset(final LatLonRect boundingBox) throws IOException {
            return null;
        }
    }
    
    private class StandardSectionFeatureIterator implements PointFeatureCollectionIterator
    {
        Cursor cursor;
        private StructureDataIterator iter;
        
        StandardSectionFeatureIterator(final Cursor cursor) throws IOException {
            this.cursor = cursor;
            this.iter = StandardSectionCollectionImpl.this.ft.getMiddleFeatureDataIterator(cursor, -1);
        }
        
        public boolean hasNext() throws IOException {
            return this.iter.hasNext();
        }
        
        public PointFeatureCollection next() throws IOException {
            final Cursor cursorIter = this.cursor.copy();
            cursorIter.tableData[1] = this.iter.next();
            cursorIter.recnum[1] = this.iter.getCurrentRecno();
            cursorIter.currentIndex = 1;
            StandardSectionCollectionImpl.this.ft.addParentJoin(this.cursor);
            return new StandardSectionProfileFeature(cursorIter, StandardSectionCollectionImpl.this.ft.getObsTime(this.cursor));
        }
        
        public void setBufferSize(final int bytes) {
            this.iter.setBufferSize(bytes);
        }
        
        public void finish() {
        }
    }
    
    private class StandardSectionProfileFeature extends ProfileFeatureImpl
    {
        Cursor cursor;
        
        StandardSectionProfileFeature(final Cursor cursor, final double time) {
            super(StandardSectionCollectionImpl.this.timeUnit.makeStandardDateString(time), StandardSectionCollectionImpl.this.ft.getLatitude(cursor), StandardSectionCollectionImpl.this.ft.getLongitude(cursor), time, -1);
            this.cursor = cursor;
            if (Double.isNaN(time)) {
                try {
                    final PointFeatureIterator iter = this.getPointFeatureIterator(-1);
                    if (iter.hasNext()) {
                        final PointFeature pf = iter.next();
                        this.time = pf.getObservationTime();
                        this.name = StandardSectionCollectionImpl.this.timeUnit.makeStandardDateString(this.time);
                    }
                    else {
                        this.name = "empty";
                    }
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        public PointFeatureIterator getPointFeatureIterator(final int bufferSize) throws IOException {
            final Cursor cursorIter = this.cursor.copy();
            final StructureDataIterator siter = StandardSectionCollectionImpl.this.ft.getLeafFeatureDataIterator(cursorIter, bufferSize);
            final StandardPointFeatureIterator iter = new StandardSectionProfileFeatureIterator(StandardSectionCollectionImpl.this.ft, StandardSectionCollectionImpl.this.timeUnit, siter, cursorIter);
            if (this.boundingBox == null || this.dateRange == null || this.npts < 0) {
                iter.setCalculateBounds(this);
            }
            return iter;
        }
        
        public Date getTime() {
            return StandardSectionCollectionImpl.this.timeUnit.makeDate(this.time);
        }
    }
    
    private class StandardSectionProfileFeatureIterator extends StandardPointFeatureIterator
    {
        StandardSectionProfileFeatureIterator(final NestedTable ft, final DateUnit timeUnit, final StructureDataIterator structIter, final Cursor cursor) throws IOException {
            super(ft, timeUnit, structIter, cursor);
        }
        
        @Override
        protected boolean isMissing() throws IOException {
            return super.isMissing() || this.ft.isAltMissing(this.cursor);
        }
    }
}
