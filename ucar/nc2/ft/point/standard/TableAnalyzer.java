// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard;

import ucar.nc2.ft.point.standard.plug.Nldn;
import ucar.nc2.ft.point.standard.plug.RafNimbus;
import ucar.nc2.ft.point.standard.plug.BuoyShipSynop;
import ucar.nc2.ft.point.standard.plug.Suomi;
import ucar.nc2.ft.point.standard.plug.Ndbc;
import ucar.nc2.ft.point.standard.plug.Iridl;
import ucar.nc2.ft.point.standard.plug.FslRaob;
import ucar.nc2.ft.point.standard.plug.Madis;
import ucar.nc2.ft.point.standard.plug.MadisAcars;
import ucar.nc2.ft.point.standard.plug.FslWindProfiler;
import ucar.nc2.ft.point.standard.plug.Jason;
import ucar.nc2.ft.point.standard.plug.Cosmic;
import ucar.nc2.ft.point.standard.plug.UnidataPointObs;
import ucar.nc2.ft.point.standard.plug.GempakCdm;
import ucar.nc2.ft.point.standard.plug.BufrCdm;
import ucar.nc2.ft.point.standard.plug.CFpointObs;
import org.slf4j.LoggerFactory;
import ucar.nc2.VariableSimpleIF;
import org.jdom.Content;
import org.jdom.Element;
import org.jdom.Document;
import org.jdom.output.XMLOutputter;
import org.jdom.output.Format;
import ucar.nc2.dataset.CoordinateSystem;
import ucar.nc2.dataset.VariableDS;
import ucar.nc2.constants.AxisType;
import ucar.nc2.dataset.CoordinateAxis;
import ucar.nc2.Dimension;
import ucar.nc2.Structure;
import java.util.Collection;
import ucar.nc2.ft.FeatureDatasetFactoryManager;
import java.util.HashSet;
import java.util.HashMap;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.StringTokenizer;
import java.util.ArrayList;
import ucar.nc2.Variable;
import java.util.Iterator;
import java.util.Formatter;
import ucar.nc2.constants.FeatureType;
import java.util.Set;
import java.util.Map;
import ucar.nc2.dataset.NetcdfDataset;
import java.util.List;
import org.slf4j.Logger;

public class TableAnalyzer
{
    private static Logger log;
    private static List<Configurator> conventionList;
    private static boolean userMode;
    private static boolean debug;
    private TableConfigurer tc;
    private NetcdfDataset ds;
    private Map<String, TableConfig> tableFind;
    private Set<TableConfig> tableSet;
    private List<NestedTable> leaves;
    private FeatureType ft;
    private TableConfig configResult;
    private Formatter userAdvice;
    private Formatter errlog;
    private String conventionName;
    
    public static void registerAnalyzer(final String conventionName, final Class c, final ConventionNameOk match) {
        if (!TableConfigurer.class.isAssignableFrom(c)) {
            throw new IllegalArgumentException("Class " + c.getName() + " must implement TableConfigurer");
        }
        TableConfigurer tc;
        try {
            tc = (TableConfigurer)c.newInstance();
        }
        catch (InstantiationException e) {
            throw new IllegalArgumentException("TableConfigurer Class " + c.getName() + " cannot instantiate, probably need default Constructor");
        }
        catch (IllegalAccessException e2) {
            throw new IllegalArgumentException("TableConfigurer Class " + c.getName() + " is not accessible");
        }
        final Configurator anal = new Configurator(conventionName, c, tc, match);
        if (TableAnalyzer.userMode) {
            TableAnalyzer.conventionList.add(0, anal);
        }
        else {
            TableAnalyzer.conventionList.add(anal);
        }
    }
    
    private static Configurator matchConfigurator(final String convName) {
        for (final Configurator anal : TableAnalyzer.conventionList) {
            if (anal.match == null && anal.convName.equalsIgnoreCase(convName)) {
                return anal;
            }
            if (anal.match != null && anal.match.isMatch(convName, anal.convName)) {
                return anal;
            }
        }
        return null;
    }
    
    public static TableConfigurer getTableConfigurer(final FeatureType wantFeatureType, final NetcdfDataset ds) throws IOException {
        String convUsed = null;
        String convName = ds.findAttValueIgnoreCase(null, "Conventions", null);
        if (convName == null) {
            convName = ds.findAttValueIgnoreCase(null, "Convention", null);
        }
        Configurator anal = null;
        if (convName != null) {
            convName = convName.trim();
            anal = matchConfigurator(convName);
            if (anal != null) {
                convUsed = convName;
                if (TableAnalyzer.debug) {
                    System.out.println("  TableConfigurer found using convName " + convName);
                }
            }
            if (anal == null) {
                final List<String> names = new ArrayList<String>();
                if (convName.indexOf(44) > 0 || convName.indexOf(59) > 0) {
                    final StringTokenizer stoke = new StringTokenizer(convName, ",;");
                    while (stoke.hasMoreTokens()) {
                        final String name = stoke.nextToken();
                        names.add(name.trim());
                    }
                }
                else if (convName.indexOf(47) > 0) {
                    final StringTokenizer stoke = new StringTokenizer(convName, "/");
                    while (stoke.hasMoreTokens()) {
                        final String name = stoke.nextToken();
                        names.add(name.trim());
                    }
                }
                if (names.size() > 0) {
                    for (final Configurator conv : TableAnalyzer.conventionList) {
                        for (final String name2 : names) {
                            if (name2.equalsIgnoreCase(conv.convName)) {
                                anal = conv;
                                convUsed = name2;
                                if (!TableAnalyzer.debug) {
                                    continue;
                                }
                                System.out.println("  TableConfigurer found using convName " + convName);
                            }
                        }
                        if (anal != null) {
                            break;
                        }
                    }
                }
            }
        }
        if (anal == null) {
            for (final Configurator conv2 : TableAnalyzer.conventionList) {
                final Class c = conv2.confClass;
                Method isMineMethod;
                try {
                    isMineMethod = c.getMethod("isMine", FeatureType.class, NetcdfDataset.class);
                }
                catch (NoSuchMethodException ex2) {
                    continue;
                }
                try {
                    final Boolean result = (Boolean)isMineMethod.invoke(conv2.confInstance, wantFeatureType, ds);
                    if (TableAnalyzer.debug) {
                        System.out.println("  TableConfigurer.isMine " + c.getName() + " result = " + result);
                    }
                    if (result) {
                        anal = conv2;
                        convUsed = conv2.convName;
                        break;
                    }
                    continue;
                }
                catch (Exception ex) {
                    System.out.println("ERROR: Class " + c.getName() + " Exception invoking isMine method\n" + ex);
                }
            }
        }
        TableConfigurer tc = null;
        if (anal != null) {
            try {
                tc = anal.confClass.newInstance();
                tc.setConvName(convName);
                tc.setConvUsed(convUsed);
            }
            catch (InstantiationException e) {
                TableAnalyzer.log.error("TableConfigurer create failed", e);
            }
            catch (IllegalAccessException e2) {
                TableAnalyzer.log.error("TableConfigurer create failed", e2);
            }
        }
        return tc;
    }
    
    public static TableAnalyzer factory(final TableConfigurer tc, final FeatureType wantFeatureType, final NetcdfDataset ds) throws IOException {
        final TableAnalyzer analyzer = new TableAnalyzer(ds, tc);
        if (tc != null) {
            if (tc.getConvName() == null) {
                analyzer.userAdvice.format(" No 'Conventions' global attribute.\n", new Object[0]);
            }
            else {
                analyzer.userAdvice.format(" Conventions global attribute = %s %n", tc.getConvName());
            }
            if (tc.getConvUsed() != null) {
                analyzer.setConventionUsed(tc.getConvUsed());
                if (!tc.getConvUsed().equals(tc.getConvName())) {
                    analyzer.userAdvice.format(" TableConfigurer used = " + tc.getConvUsed() + ".\n", new Object[0]);
                }
            }
        }
        else {
            analyzer.userAdvice.format(" No TableConfigurer found, using default analysis.\n", new Object[0]);
        }
        analyzer.analyze(wantFeatureType);
        return analyzer;
    }
    
    private TableAnalyzer(final NetcdfDataset ds, final TableConfigurer tc) {
        this.tableFind = new HashMap<String, TableConfig>();
        this.tableSet = new HashSet<TableConfig>();
        this.leaves = new ArrayList<NestedTable>();
        this.userAdvice = new Formatter();
        this.errlog = new Formatter();
        this.tc = tc;
        this.ds = ds;
        if (tc == null) {
            this.userAdvice.format("Using default TableConfigurer.\n", new Object[0]);
        }
    }
    
    public List<NestedTable> getFlatTables() {
        return this.leaves;
    }
    
    public boolean featureTypeOk(final FeatureType ftype, final Formatter errlog) {
        for (final NestedTable nt : this.leaves) {
            if (!nt.hasCoords()) {
                errlog.format("Table %s featureType %s: lat/lon/time coord not found%n", nt.getName(), nt.getFeatureType());
            }
            if (!FeatureDatasetFactoryManager.featureTypeOk(ftype, nt.getFeatureType())) {
                errlog.format("Table %s featureType %s doesnt match desired type %s%n", nt.getName(), nt.getFeatureType(), ftype);
            }
            if (nt.hasCoords() && FeatureDatasetFactoryManager.featureTypeOk(ftype, nt.getFeatureType())) {
                return true;
            }
        }
        return false;
    }
    
    public String getName() {
        if (this.tc != null) {
            return this.tc.getClass().getName();
        }
        return "Default";
    }
    
    public FeatureType getFirstFeatureType() {
        for (final NestedTable nt : this.leaves) {
            if (nt.hasCoords()) {
                return nt.getFeatureType();
            }
        }
        return null;
    }
    
    public NetcdfDataset getNetcdfDataset() {
        return this.ds;
    }
    
    public String getUserAdvice() {
        return this.userAdvice.toString();
    }
    
    public String getErrlog() {
        return this.errlog.toString();
    }
    
    private void setConventionUsed(final String convName) {
        this.conventionName = convName;
    }
    
    TableConfig getTableConfig() {
        return this.configResult;
    }
    
    TableConfigurer getTableConfigurer() {
        return this.tc;
    }
    
    private void analyze(final FeatureType wantFeatureType) throws IOException {
        final boolean structAdded = (boolean)this.ds.sendIospMessage("AddRecordStructure");
        if (this.tc == null) {
            this.makeTablesDefault(structAdded);
            this.makeNestedTables();
        }
        else {
            this.configResult = this.tc.getConfig(wantFeatureType, this.ds, this.errlog);
            if (this.configResult != null) {
                this.addTableRecurse(this.configResult);
            }
            else {
                this.makeTablesDefault(structAdded);
                this.makeNestedTables();
            }
        }
        for (final TableConfig config : this.tableSet) {
            if (config.children == null) {
                final NestedTable flatTable = new NestedTable(this.ds, config, this.errlog);
                this.leaves.add(flatTable);
            }
        }
        if (PointDatasetStandardFactory.showTables) {
            this.getDetailInfo(new Formatter(System.out));
        }
    }
    
    private void addTable(final TableConfig t) {
        this.tableFind.put(t.name, t);
        if (t.dimName != null) {
            this.tableFind.put(t.dimName, t);
        }
        this.tableSet.add(t);
    }
    
    private void addTableRecurse(final TableConfig t) {
        this.addTable(t);
        if (t.children != null) {
            for (final TableConfig child : t.children) {
                this.addTableRecurse(child);
            }
        }
    }
    
    private void makeTablesDefault(final boolean structAdded) throws IOException {
        final List<Variable> vars = new ArrayList<Variable>(this.ds.getVariables());
        final Iterator<Variable> iter = vars.iterator();
        while (iter.hasNext()) {
            final Variable v = iter.next();
            if (v instanceof Structure) {
                final TableConfig st = new TableConfig(Table.Type.Structure, v.getName());
                CoordSysEvaluator.findCoords(st, this.ds);
                st.structName = v.getName();
                st.nestedTableName = v.getShortName();
                this.addTable(st);
                iter.remove();
                this.findNestedStructures((Structure)v, st);
            }
            else {
                if (!structAdded || !v.isUnlimited()) {
                    continue;
                }
                iter.remove();
            }
        }
        if (this.tableSet.size() > 0) {
            return;
        }
        final Set<Dimension> dimSet = new HashSet<Dimension>(10);
        for (final CoordinateAxis axis : this.ds.getCoordinateAxes()) {
            if (axis.getAxisType() == AxisType.Lat || axis.getAxisType() == AxisType.Lon || axis.getAxisType() == AxisType.Time) {
                for (final Dimension dim : axis.getDimensions()) {
                    dimSet.add(dim);
                }
            }
        }
        if (dimSet.size() == 1) {
            final Dimension obsDim = (Dimension)dimSet.toArray()[0];
            final TableConfig st2 = new TableConfig(Table.Type.Structure, obsDim.getName());
            st2.structureType = (obsDim.isUnlimited() ? TableConfig.StructureType.Structure : TableConfig.StructureType.PsuedoStructure);
            st2.structName = (obsDim.isUnlimited() ? "record" : obsDim.getName());
            st2.dimName = obsDim.getName();
            CoordSysEvaluator.findCoordWithDimension(st2, this.ds, obsDim);
            final CoordinateAxis time = CoordSysEvaluator.findCoordByType(this.ds, AxisType.Time);
            if (time != null && time.getRank() == 0) {
                st2.addJoin(new JoinArray(time, JoinArray.Type.scalar, 0));
                st2.time = time.getShortName();
            }
            this.addTable(st2);
        }
        if (this.tableSet.size() > 0) {
            return;
        }
        CoordinateAxis time2 = null;
        for (final CoordinateAxis axis2 : this.ds.getCoordinateAxes()) {
            if (axis2.getAxisType() == AxisType.Time && axis2.isCoordinateVariable()) {
                time2 = axis2;
                break;
            }
        }
        if (time2 != null) {
            final Dimension obsDim2 = time2.getDimension(0);
            final TableConfig st3 = new TableConfig(Table.Type.Structure, obsDim2.getName());
            st3.structureType = TableConfig.StructureType.PsuedoStructure;
            st3.dimName = obsDim2.getName();
            CoordSysEvaluator.findCoords(st3, this.ds);
            this.addTable(st3);
        }
    }
    
    private void findNestedStructures(final Structure s, final TableConfig parent) {
        for (final Variable v : s.getVariables()) {
            if (v instanceof Structure) {
                final TableConfig nestedTable = new TableConfig(Table.Type.NestedStructure, v.getName());
                nestedTable.structName = v.getName();
                nestedTable.nestedTableName = v.getShortName();
                this.addTable(nestedTable);
                parent.addChild(nestedTable);
                this.findNestedStructures((Structure)v, nestedTable);
            }
        }
    }
    
    private void makeNestedTables() {
    }
    
    public void showCoordSys(final Formatter sf) {
        sf.format("\nCoordinate Systems\n", new Object[0]);
        for (final CoordinateSystem cs : this.ds.getCoordinateSystems()) {
            sf.format(" %s\n", cs);
        }
    }
    
    public void showCoordAxes(final Formatter sf) {
        sf.format("\nAxes\n", new Object[0]);
        for (final CoordinateAxis axis : this.ds.getCoordinateAxes()) {
            sf.format(" %s %s\n", axis.getAxisType(), axis.getNameAndDimensions());
        }
    }
    
    public void showNestedTables(final Formatter sf) {
        for (final NestedTable nt : this.leaves) {
            nt.show(sf);
        }
    }
    
    public String getImplementationName() {
        return (this.tc != null) ? this.tc.getClass().getSimpleName() : "defaultAnalyser";
    }
    
    public void getDetailInfo(final Formatter sf) {
        sf.format("\nTableAnalyzer on Dataset %s\n", this.ds.getLocation());
        if (this.tc != null) {
            sf.format(" TableAnalyser = %s\n", this.tc.getClass().getName());
        }
        this.showNestedTables(sf);
        final String errlogS = this.errlog.toString();
        if (errlogS.length() > 0) {
            sf.format("\n Errlog=\n%s", errlogS);
        }
        final String userAdviceS = this.userAdvice.toString();
        if (userAdviceS.length() > 0) {
            sf.format("\n userAdvice=\n%s\n", userAdviceS);
        }
        try {
            this.writeConfigXML(sf);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private void writeConfigXML(final Formatter sf) throws IOException {
        if (this.configResult != null) {
            final PointConfigXML tcx = new PointConfigXML();
            tcx.writeConfigXML(this.configResult, this.tc.getClass().getName(), sf);
            return;
        }
        final XMLOutputter fmt = new XMLOutputter(Format.getPrettyFormat());
        sf.format("%s", fmt.outputString(this.makeDocument()));
    }
    
    private Document makeDocument() {
        final Element rootElem = new Element("featureDataset");
        final Document doc = new Document(rootElem);
        rootElem.setAttribute("location", this.ds.getLocation());
        if (this.tc != null) {
            rootElem.addContent(new Element("analyser").setAttribute("class", this.tc.getClass().getName()));
        }
        if (this.ft != null) {
            rootElem.setAttribute("featureType", this.ft.toString());
        }
        for (final NestedTable nt : this.leaves) {
            this.writeTable(rootElem, nt.getLeaf());
        }
        return doc;
    }
    
    private Element writeTable(Element parent, final Table table) {
        if (table.parent != null) {
            parent = this.writeTable(parent, table.parent);
        }
        final Element tableElem = new Element("table");
        parent.addContent(tableElem);
        if (table.getName() != null) {
            tableElem.setAttribute("name", table.getName());
        }
        if (table.getFeatureType() != null) {
            tableElem.setAttribute("featureType", table.getFeatureType().toString());
        }
        tableElem.setAttribute("class", table.getClass().toString());
        this.addCoordinates(tableElem, table);
        for (final VariableSimpleIF col : table.cols) {
            if (!table.nondataVars.contains(col.getShortName())) {
                tableElem.addContent(new Element("variable").addContent(col.getName()));
            }
        }
        if (table.extraJoins != null) {
            for (final Join j : table.extraJoins) {
                if (j instanceof JoinArray) {
                    tableElem.addContent(this.writeJoinArray((JoinArray)j));
                }
                else if (j instanceof JoinMuiltdimStructure) {
                    tableElem.addContent(this.writeJoinMuiltdimStructure((JoinMuiltdimStructure)j));
                }
                else {
                    if (!(j instanceof JoinParentIndex)) {
                        continue;
                    }
                    tableElem.addContent(this.writeJoinParentIndex((JoinParentIndex)j));
                }
            }
        }
        return tableElem;
    }
    
    private void addCoordinates(final Element tableElem, final Table table) {
        this.addCoord(tableElem, table.lat, "lat");
        this.addCoord(tableElem, table.lon, "lon");
        this.addCoord(tableElem, table.elev, "elev");
        this.addCoord(tableElem, table.time, "time");
        this.addCoord(tableElem, table.timeNominal, "timeNominal");
        this.addCoord(tableElem, table.stnId, "stnId");
        this.addCoord(tableElem, table.stnDesc, "stnDesc");
        this.addCoord(tableElem, table.stnNpts, "stnNpts");
        this.addCoord(tableElem, table.stnWmoId, "stnWmoId");
        this.addCoord(tableElem, table.stnAlt, "stnAlt");
        this.addCoord(tableElem, table.limit, "limit");
    }
    
    private void addCoord(final Element tableElem, final String name, final String kind) {
        if (name != null) {
            final Element elem = new Element("coordinate").setAttribute("kind", kind);
            elem.addContent(name);
            tableElem.addContent(elem);
        }
    }
    
    private Element writeJoinArray(final JoinArray join) {
        final Element joinElem = new Element("join");
        joinElem.setAttribute("class", join.getClass().toString());
        if (join.type != null) {
            joinElem.setAttribute("type", join.type.toString());
        }
        if (join.v != null) {
            joinElem.addContent(new Element("variable").setAttribute("name", join.v.getName()));
        }
        joinElem.addContent(new Element("param").setAttribute("value", Integer.toString(join.param)));
        return joinElem;
    }
    
    private Element writeJoinMuiltdimStructure(final JoinMuiltdimStructure join) {
        final Element joinElem = new Element("join");
        joinElem.setAttribute("class", join.getClass().toString());
        if (join.parentStructure != null) {
            joinElem.addContent(new Element("parentStructure").setAttribute("name", join.parentStructure.getName()));
        }
        joinElem.addContent(new Element("dimLength").setAttribute("value", Integer.toString(join.dimLength)));
        return joinElem;
    }
    
    private Element writeJoinParentIndex(final JoinParentIndex join) {
        final Element joinElem = new Element("join");
        joinElem.setAttribute("class", join.getClass().toString());
        if (join.parentStructure != null) {
            joinElem.addContent(new Element("parentStructure").setAttribute("name", join.parentStructure.getName()));
        }
        if (join.parentIndex != null) {
            joinElem.addContent(new Element("parentIndex").setAttribute("name", join.parentIndex));
        }
        return joinElem;
    }
    
    static void doit(final String filename) throws IOException {
        System.out.println(filename);
        final NetcdfDataset ncd = NetcdfDataset.openDataset(filename);
        final TableAnalyzer csa = factory(null, null, ncd);
        csa.getDetailInfo(new Formatter(System.out));
        System.out.println("\n-----------------");
    }
    
    public static void main(final String[] args) throws IOException {
        doit("D:/datasets/metars/Surface_METAR_20070513_0000.nc");
    }
    
    static {
        TableAnalyzer.log = LoggerFactory.getLogger(TableAnalyzer.class);
        TableAnalyzer.conventionList = new ArrayList<Configurator>();
        TableAnalyzer.userMode = false;
        TableAnalyzer.debug = false;
        registerAnalyzer("CF-1.", CFpointObs.class, new ConventionNameOk() {
            public boolean isMatch(final String convName, final String wantName) {
                return convName.startsWith(wantName);
            }
        });
        registerAnalyzer("BUFR/CDM", BufrCdm.class, null);
        registerAnalyzer("GEMPAK/CDM", GempakCdm.class, null);
        registerAnalyzer("Unidata Observation Dataset v1.0", UnidataPointObs.class, null);
        registerAnalyzer("Cosmic", Cosmic.class, null);
        registerAnalyzer("Jason", Jason.class, null);
        registerAnalyzer("FslWindProfiler", FslWindProfiler.class, null);
        registerAnalyzer("MADIS-ACARS", MadisAcars.class, null);
        registerAnalyzer("MADIS surface observations, v1.0", Madis.class, null);
        registerAnalyzer("FSL Raobs", FslRaob.class, null);
        registerAnalyzer("IRIDL", Iridl.class, null);
        registerAnalyzer("Ndbc", Ndbc.class, null);
        registerAnalyzer("Suomi-Station-CDM", Suomi.class, null);
        registerAnalyzer("BuoyShip-NetCDF", BuoyShipSynop.class, null);
        registerAnalyzer("NCAR-RAF/nimbus", RafNimbus.class, null);
        registerAnalyzer("NLDN-CDM", Nldn.class, null);
        TableAnalyzer.userMode = true;
    }
    
    private static class Configurator
    {
        String convName;
        Class confClass;
        TableConfigurer confInstance;
        ConventionNameOk match;
        
        Configurator(final String convName, final Class confClass, final TableConfigurer confInstance, final ConventionNameOk match) {
            this.convName = convName;
            this.confClass = confClass;
            this.confInstance = confInstance;
            this.match = match;
        }
    }
    
    private interface ConventionNameOk
    {
        boolean isMatch(final String p0, final String p1);
    }
}
