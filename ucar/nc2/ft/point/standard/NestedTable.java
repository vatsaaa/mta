// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard;

import org.slf4j.LoggerFactory;
import ucar.unidata.geoloc.StationImpl;
import ucar.unidata.geoloc.Station;
import ucar.nc2.Variable;
import ucar.nc2.ft.point.StructureDataIteratorLimited;
import ucar.ma2.StructureDataIterator;
import java.io.IOException;
import ucar.ma2.StructureDataFactory;
import ucar.ma2.StructureMembers;
import ucar.unidata.geoloc.EarthLocationImpl;
import ucar.unidata.geoloc.EarthLocation;
import java.util.Date;
import java.text.ParseException;
import ucar.ma2.StructureData;
import java.util.ArrayList;
import ucar.nc2.VariableSimpleIF;
import java.util.List;
import ucar.nc2.units.DateUnit;
import java.util.Iterator;
import ucar.nc2.dataset.VariableDS;
import ucar.nc2.ft.FeatureDatasetFactoryManager;
import ucar.nc2.units.DateFormatter;
import ucar.nc2.constants.FeatureType;
import java.util.Formatter;
import ucar.nc2.dataset.NetcdfDataset;
import org.slf4j.Logger;

public class NestedTable
{
    private static Logger log;
    private NetcdfDataset ds;
    private Formatter errlog;
    private Table leaf;
    private Table root;
    private FeatureType featureType;
    private CoordVarExtractor timeVE;
    private CoordVarExtractor nomTimeVE;
    private CoordVarExtractor latVE;
    private CoordVarExtractor lonVE;
    private CoordVarExtractor altVE;
    private CoordVarExtractor stnVE;
    private CoordVarExtractor stnDescVE;
    private CoordVarExtractor wmoVE;
    private CoordVarExtractor stnAltVE;
    private CoordVarExtractor idVE;
    private CoordVarExtractor missingVE;
    private int nlevels;
    private DateFormatter dateFormatter;
    
    NestedTable(final NetcdfDataset ds, final TableConfig config, final Formatter errlog) {
        this.dateFormatter = new DateFormatter();
        this.ds = ds;
        this.errlog = errlog;
        this.leaf = Table.factory(ds, config);
        this.root = this.getRoot();
        this.nlevels = 0;
        for (Table t = this.leaf; t != null; t = t.parent, ++this.nlevels) {
            if (t.getFeatureType() != null) {
                this.featureType = t.getFeatureType();
            }
        }
        if (this.featureType == null) {
            this.featureType = FeatureDatasetFactoryManager.findFeatureType(ds);
        }
        this.timeVE = this.findCoordinateAxis(Table.CoordName.Time, this.leaf, 0);
        this.latVE = this.findCoordinateAxis(Table.CoordName.Lat, this.leaf, 0);
        this.lonVE = this.findCoordinateAxis(Table.CoordName.Lon, this.leaf, 0);
        this.altVE = this.findCoordinateAxis(Table.CoordName.Elev, this.leaf, 0);
        this.nomTimeVE = this.findCoordinateAxis(Table.CoordName.TimeNominal, this.leaf, 0);
        this.stnVE = this.findCoordinateAxis(Table.CoordName.StnId, this.leaf, 0);
        this.stnDescVE = this.findCoordinateAxis(Table.CoordName.StnDesc, this.leaf, 0);
        this.wmoVE = this.findCoordinateAxis(Table.CoordName.WmoId, this.leaf, 0);
        this.stnAltVE = this.findCoordinateAxis(Table.CoordName.StnAlt, this.leaf, 0);
        this.missingVE = this.findCoordinateAxis(Table.CoordName.MissingVar, this.leaf, 0);
        this.idVE = this.findCoordinateAxis(Table.CoordName.FeatureId, this.root, this.nlevels - 1);
        if (this.featureType == null) {
            if (this.nlevels == 1) {
                this.featureType = FeatureType.POINT;
            }
            if (this.nlevels == 2) {
                this.featureType = FeatureType.STATION;
            }
            if (this.nlevels == 3) {
                this.featureType = FeatureType.STATION_PROFILE;
            }
        }
    }
    
    Table getRoot() {
        Table p;
        for (p = this.leaf; p.parent != null; p = p.parent) {}
        return p;
    }
    
    Table getLeaf() {
        return this.leaf;
    }
    
    private CoordVarExtractor findCoordinateAxis(final Table.CoordName coordName, final Table t, final int nestingLevel) {
        if (t == null) {
            return null;
        }
        final String axisName = t.findCoordinateVariableName(coordName);
        if (axisName != null) {
            VariableDS v = t.findVariable(axisName);
            if (v != null) {
                return new CoordVarExtractorVariable(v, axisName, nestingLevel);
            }
            if (t.extraJoins != null) {
                for (final Join j : t.extraJoins) {
                    v = j.findVariable(axisName);
                    if (v != null) {
                        return new CoordVarExtractorVariable(v, axisName, nestingLevel);
                    }
                }
            }
            if (t instanceof Table.TableSingleton) {
                final Table.TableSingleton ts = (Table.TableSingleton)t;
                return new CoordVarStructureData(axisName, ts.sdata);
            }
            if (t instanceof Table.TableTop) {
                v = (VariableDS)this.ds.findVariable(axisName);
                if (v != null) {
                    return new CoordVarTop(v);
                }
                return new CoordVarConstant(coordName.toString(), "", axisName);
            }
            else {
                this.errlog.format("NestedTable: cant find variable %s for coordinate type %s %n", axisName, coordName);
            }
        }
        return this.findCoordinateAxis(coordName, t.parent, nestingLevel + 1);
    }
    
    public FeatureType getFeatureType() {
        return this.featureType;
    }
    
    public int getNumberOfLevels() {
        return this.nlevels;
    }
    
    public boolean hasCoords() {
        return this.timeVE != null && this.latVE != null && this.lonVE != null;
    }
    
    public DateUnit getTimeUnit() {
        try {
            return new DateUnit(this.timeVE.getUnitsString());
        }
        catch (Exception e) {
            throw new IllegalArgumentException("Error on time string = " + this.timeVE.getUnitsString() + " == " + e.getMessage());
        }
    }
    
    public List<VariableSimpleIF> getDataVariables() {
        final List<VariableSimpleIF> data = new ArrayList<VariableSimpleIF>();
        this.addDataVariables(data, this.leaf);
        return data;
    }
    
    private void addDataVariables(final List<VariableSimpleIF> list, final Table t) {
        if (t.parent != null) {
            this.addDataVariables(list, t.parent);
        }
        for (final VariableSimpleIF col : t.cols) {
            if (!t.nondataVars.contains(col.getShortName())) {
                list.add(col);
            }
        }
    }
    
    public String getName() {
        final Formatter formatter = new Formatter();
        formatter.format("%s", this.root.getName());
        Table t = this.root;
        while (t.child != null) {
            t = t.child;
            formatter.format("/%s", t.getName());
        }
        return formatter.toString();
    }
    
    @Override
    public String toString() {
        final Formatter formatter = new Formatter();
        formatter.format("NestedTable = %s\n", this.getName());
        formatter.format("  Time= %s\n", this.timeVE);
        formatter.format("  Lat= %s\n", this.latVE);
        formatter.format("  Lon= %s\n", this.lonVE);
        formatter.format("  Height= %s\n", this.altVE);
        return formatter.toString();
    }
    
    public void show(final Formatter formatter) {
        formatter.format(" NestedTable = %s\n", this.getName());
        formatter.format("   nlevels = %d\n", this.nlevels);
        this.leaf.show(formatter, 2);
    }
    
    public double getObsTime(final Cursor cursor) {
        return this.getTime(this.timeVE, cursor.tableData);
    }
    
    public double getNomTime(final Cursor cursor) {
        return this.getTime(this.nomTimeVE, cursor.tableData);
    }
    
    private double getTime(final CoordVarExtractor cve, final StructureData[] tableData) {
        if (cve == null) {
            return Double.NaN;
        }
        if (tableData[cve.nestingLevel] == null) {
            return Double.NaN;
        }
        if (cve.isString()) {
            final String timeString = this.timeVE.getCoordValueString(tableData);
            Date date;
            try {
                date = this.dateFormatter.isoDateTimeFormat(timeString);
            }
            catch (ParseException e) {
                NestedTable.log.error("Cant parse date - not ISO formatted, = " + timeString);
                return 0.0;
            }
            return date.getTime() / 1000.0;
        }
        return cve.getCoordValue(tableData);
    }
    
    public double getLatitude(final Cursor cursor) {
        return this.latVE.getCoordValue(cursor.tableData);
    }
    
    public double getLongitude(final Cursor cursor) {
        return this.lonVE.getCoordValue(cursor.tableData);
    }
    
    public EarthLocation getEarthLocation(final Cursor cursor) {
        final double lat = this.latVE.getCoordValue(cursor.tableData);
        final double lon = this.lonVE.getCoordValue(cursor.tableData);
        double alt = (this.altVE == null) ? Double.NaN : this.altVE.getCoordValue(cursor.tableData);
        if (this.stnAltVE != null) {
            final double stnElev = this.stnAltVE.getCoordValue(cursor.tableData);
            if (this.altVE == null) {
                alt = stnElev;
            }
            else {
                alt += stnElev;
            }
        }
        return new EarthLocationImpl(lat, lon, alt);
    }
    
    public String getFeatureName(final Cursor cursor) {
        int count = 0;
        Table t = this.leaf;
        while (count++ < cursor.currentIndex) {
            t = t.parent;
        }
        if (t.feature_id == null) {
            return "unknown";
        }
        final StructureData sdata = cursor.getParentStructure();
        if (sdata == null) {
            return "unknown";
        }
        final StructureMembers.Member m = sdata.findMember(t.feature_id);
        if (m == null) {
            return "unknown";
        }
        if (m.getDataType().isString()) {
            return sdata.getScalarString(m);
        }
        if (m.getDataType().isIntegral()) {
            return Integer.toString(sdata.convertScalarInt(m));
        }
        return Double.toString(sdata.convertScalarDouble(m));
    }
    
    public boolean isFeatureMissing(final StructureData sdata) {
        return this.idVE != null && this.idVE.isMissing(sdata);
    }
    
    public boolean isTimeMissing(final Cursor cursor) {
        return this.timeVE != null && this.timeVE.isMissing(cursor.tableData);
    }
    
    public boolean isAltMissing(final Cursor cursor) {
        return this.altVE != null && this.altVE.isMissing(cursor.tableData);
    }
    
    public boolean isMissing(final Cursor cursor) {
        return this.missingVE != null && this.missingVE.isMissing(cursor.tableData);
    }
    
    public StructureData makeObsStructureData(final Cursor cursor) {
        return StructureDataFactory.make(cursor.tableData);
    }
    
    void addParentJoin(final Cursor cursor) throws IOException {
        final int level = cursor.currentIndex;
        final Table t = this.getTable(level);
        if (t.extraJoins != null) {
            final List<StructureData> sdata = new ArrayList<StructureData>(3);
            sdata.add(cursor.tableData[level]);
            for (final Join j : t.extraJoins) {
                sdata.add(j.getJoinData(cursor));
            }
            cursor.tableData[level] = StructureDataFactory.make(sdata.toArray(new StructureData[sdata.size()]));
        }
    }
    
    private Table getTable(final int level) {
        Table t = this.leaf;
        for (int count = 0; count < level; ++count, t = t.parent) {}
        return t;
    }
    
    public StructureDataIterator getObsDataIterator(final Cursor cursor, final int bufferSize) throws IOException {
        return this.root.getStructureDataIterator(cursor, bufferSize);
    }
    
    public StructureDataIterator getStationDataIterator(final int bufferSize) throws IOException {
        final Table stationTable = this.root;
        final StructureDataIterator siter = stationTable.getStructureDataIterator(null, bufferSize);
        if (stationTable.limit != null) {
            final Variable limitV = this.ds.findVariable(stationTable.limit);
            final int limit = limitV.readScalarInt();
            return new StructureDataIteratorLimited(siter, limit);
        }
        return siter;
    }
    
    public StructureDataIterator getRootFeatureDataIterator(final int bufferSize) throws IOException {
        return this.root.getStructureDataIterator(null, bufferSize);
    }
    
    public StructureDataIterator getLeafFeatureDataIterator(final Cursor cursor, final int bufferSize) throws IOException {
        return this.leaf.getStructureDataIterator(cursor, bufferSize);
    }
    
    public StructureDataIterator getMiddleFeatureDataIterator(final Cursor cursor, final int bufferSize) throws IOException {
        return this.leaf.parent.getStructureDataIterator(cursor, bufferSize);
    }
    
    Station makeStation(final StructureData stationData) {
        if (this.stnVE.isMissing(stationData)) {
            return null;
        }
        final String stationName = this.stnVE.getCoordValueAsString(stationData);
        final String stationDesc = (this.stnDescVE == null) ? "" : this.stnDescVE.getCoordValueAsString(stationData);
        final String stnWmoId = (this.wmoVE == null) ? "" : this.wmoVE.getCoordValueAsString(stationData);
        final double lat = this.latVE.getCoordValue(stationData);
        final double lon = this.lonVE.getCoordValue(stationData);
        final double elev = (this.stnAltVE == null) ? Double.NaN : this.stnAltVE.getCoordValue(stationData);
        if (Double.isNaN(lat) || Double.isNaN(lon)) {
            return null;
        }
        return new StationImpl(stationName, stationDesc, stnWmoId, lat, lon, elev);
    }
    
    static {
        NestedTable.log = LoggerFactory.getLogger(NestedTable.class);
    }
    
    private class CoordVarExtractorVariable extends CoordVarExtractor
    {
        protected VariableDS coordVar;
        
        CoordVarExtractorVariable(final VariableDS v, final String axisName, final int nestingLevel) {
            super(axisName, nestingLevel);
            this.coordVar = v;
        }
        
        @Override
        public String getCoordValueString(final StructureData sdata) {
            if (this.coordVar.getDataType().isString()) {
                return sdata.getScalarString(this.axisName);
            }
            if (this.coordVar.getDataType().isIntegral()) {
                return Integer.toString(sdata.convertScalarInt(this.axisName));
            }
            return Double.toString(sdata.convertScalarDouble(this.axisName));
        }
        
        @Override
        public String getUnitsString() {
            return this.coordVar.getUnitsString();
        }
        
        @Override
        public boolean isString() {
            return this.coordVar.getDataType().isString();
        }
        
        public boolean isMissing(final StructureData sdata) {
            if (this.isString()) {
                final String s = this.getCoordValueString(sdata);
                final double test = (s.length() == 0) ? 0.0 : s.charAt(0);
                return this.coordVar.isMissing(test);
            }
            final double val = this.getCoordValue(sdata);
            return this.coordVar.isMissing(val);
        }
        
        @Override
        public double getCoordValue(final StructureData sdata) {
            return sdata.convertScalarDouble(this.axisName);
        }
    }
    
    private class CoordVarTop extends CoordVarExtractor
    {
        protected VariableDS varTop;
        
        CoordVarTop(final VariableDS v) {
            super(v.getName(), 0);
            this.varTop = v;
        }
        
        @Override
        public double getCoordValue(final StructureData sdata) {
            try {
                return this.varTop.readScalarDouble();
            }
            catch (IOException e) {
                throw new RuntimeException(e.getMessage());
            }
        }
        
        @Override
        public String getUnitsString() {
            return this.varTop.getUnitsString();
        }
        
        @Override
        public boolean isString() {
            return this.varTop.getDataType().isString();
        }
        
        @Override
        public String getCoordValueString(final StructureData sdata) {
            try {
                return this.varTop.readScalarString();
            }
            catch (IOException e) {
                throw new RuntimeException(e.getMessage());
            }
        }
        
        public boolean isMissing(final StructureData sdata) {
            if (this.isString()) {
                return false;
            }
            final double val = this.getCoordValue(sdata);
            return this.varTop.isMissing(val);
        }
    }
    
    private class CoordVarStructureData extends CoordVarExtractor
    {
        protected StructureData sdata;
        
        CoordVarStructureData(final String axisName, final StructureData sdata) {
            super(axisName, 0);
            this.sdata = sdata;
        }
        
        @Override
        public double getCoordValue(final StructureData ignore) {
            return this.sdata.convertScalarDouble(this.axisName);
        }
        
        @Override
        public String getCoordValueString(final StructureData ignore) {
            return this.sdata.getScalarString(this.axisName);
        }
        
        @Override
        public String getUnitsString() {
            final StructureMembers.Member m = this.sdata.findMember(this.axisName);
            return m.getUnitsString();
        }
        
        @Override
        public boolean isString() {
            final StructureMembers.Member m = this.sdata.findMember(this.axisName);
            return m.getDataType().isString();
        }
        
        public boolean isMissing(final StructureData sdata) {
            return false;
        }
    }
    
    private class CoordVarConstant extends CoordVarExtractor
    {
        String units;
        String value;
        
        CoordVarConstant(final String name, final String units, final String value) {
            super(name, 0);
            this.units = units;
            this.value = value;
        }
        
        @Override
        public double getCoordValue(final StructureData sdata) {
            return Double.parseDouble(this.value);
        }
        
        @Override
        public String getCoordValueString(final StructureData sdata) {
            return this.value;
        }
        
        @Override
        public String getUnitsString() {
            return this.units;
        }
        
        @Override
        public boolean isString() {
            return true;
        }
        
        public boolean isMissing(final StructureData sdata) {
            return false;
        }
        
        @Override
        public String toString() {
            return "CoordVarConstant value= " + this.value;
        }
    }
}
