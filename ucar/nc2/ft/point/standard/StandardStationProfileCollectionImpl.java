// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard;

import ucar.nc2.ft.PointFeature;
import ucar.nc2.ft.PointFeatureIterator;
import ucar.nc2.ft.point.ProfileFeatureImpl;
import ucar.nc2.ft.PointFeatureCollection;
import ucar.nc2.ft.ProfileFeature;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import ucar.nc2.ft.PointFeatureCollectionIterator;
import ucar.nc2.ft.point.StationProfileFeatureImpl;
import ucar.nc2.ft.NestedPointFeatureCollection;
import java.util.Iterator;
import ucar.nc2.ft.NestedPointFeatureCollectionIterator;
import ucar.unidata.geoloc.Station;
import ucar.ma2.StructureData;
import ucar.ma2.StructureDataIterator;
import ucar.nc2.ft.point.StationHelper;
import java.io.IOException;
import ucar.nc2.units.DateUnit;
import ucar.nc2.ft.point.StationProfileCollectionImpl;

public class StandardStationProfileCollectionImpl extends StationProfileCollectionImpl
{
    private DateUnit timeUnit;
    private NestedTable ft;
    
    StandardStationProfileCollectionImpl(final NestedTable ft, final DateUnit timeUnit) throws IOException {
        super(ft.getName());
        this.ft = ft;
        this.timeUnit = timeUnit;
    }
    
    @Override
    protected void initStationHelper() {
        try {
            this.stationHelper = new StationHelper();
            final StructureDataIterator siter = this.ft.getStationDataIterator(-1);
            while (siter.hasNext()) {
                final StructureData stationData = siter.next();
                final Station s = this.makeStation(stationData, siter.getCurrentRecno());
                if (s != null) {
                    this.stationHelper.addStation(s);
                }
            }
        }
        catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }
    
    public Station makeStation(final StructureData stationData, final int recnum) {
        final Station s = this.ft.makeStation(stationData);
        if (s == null) {
            return null;
        }
        return new StandardStationProfileFeature(s, stationData, recnum);
    }
    
    public NestedPointFeatureCollectionIterator getNestedPointFeatureCollectionIterator(final int bufferSize) throws IOException {
        return new NestedPointFeatureCollectionIterator() {
            private Iterator iter = StandardStationProfileCollectionImpl.this.getStations().iterator();
            
            public boolean hasNext() throws IOException {
                return this.iter.hasNext();
            }
            
            public NestedPointFeatureCollection next() throws IOException {
                return this.iter.next();
            }
            
            public void setBufferSize(final int bytes) {
            }
        };
    }
    
    private class StandardStationProfileFeature extends StationProfileFeatureImpl
    {
        Station s;
        StructureData stationData;
        int recnum;
        final /* synthetic */ StandardStationProfileCollectionImpl this$0;
        
        StandardStationProfileFeature(final Station s, final StructureData stationData, final int recnum) {
            super(s, StandardStationProfileCollectionImpl.this.timeUnit, -1);
            this.s = s;
            this.stationData = stationData;
            this.recnum = recnum;
        }
        
        public PointFeatureCollectionIterator getPointFeatureCollectionIterator(final int bufferSize) throws IOException {
            final Cursor cursor = new Cursor(StandardStationProfileCollectionImpl.this.ft.getNumberOfLevels());
            cursor.recnum[2] = this.recnum;
            cursor.tableData[2] = this.stationData;
            cursor.currentIndex = 2;
            StandardStationProfileCollectionImpl.this.ft.addParentJoin(cursor);
            return new TimeSeriesOfProfileFeatureIterator(cursor);
        }
        
        public List<Date> getTimes() throws IOException {
            final List<Date> result = new ArrayList<Date>();
            this.resetIteration();
            while (this.hasNext()) {
                final ProfileFeature pf = this.next();
                result.add(pf.getTime());
            }
            return result;
        }
        
        public ProfileFeature getProfileByDate(final Date date) throws IOException {
            this.resetIteration();
            while (this.hasNext()) {
                final ProfileFeature pf = this.next();
                if (pf.getTime().equals(date)) {
                    return pf;
                }
            }
            return null;
        }
        
        private class TimeSeriesOfProfileFeatureIterator implements PointFeatureCollectionIterator
        {
            private Cursor cursor;
            private StructureDataIterator iter;
            private int count;
            
            TimeSeriesOfProfileFeatureIterator(final Cursor cursor) throws IOException {
                this.count = 0;
                this.cursor = cursor;
                this.iter = StandardStationProfileFeature.this.this$0.ft.getMiddleFeatureDataIterator(cursor, -1);
            }
            
            public boolean hasNext() throws IOException {
                while (this.iter.hasNext()) {
                    this.cursor.tableData[1] = this.iter.next();
                    this.cursor.recnum[1] = this.iter.getCurrentRecno();
                    this.cursor.currentIndex = 1;
                    StandardStationProfileCollectionImpl.this.ft.addParentJoin(this.cursor);
                    if (!StandardStationProfileCollectionImpl.this.ft.isMissing(this.cursor)) {
                        return true;
                    }
                }
                StandardStationProfileFeature.this.timeSeriesNpts = this.count;
                return false;
            }
            
            public PointFeatureCollection next() throws IOException {
                ++this.count;
                return new StandardProfileFeature(StandardStationProfileFeature.this.s, StandardStationProfileCollectionImpl.this.ft.getObsTime(this.cursor), this.cursor.copy());
            }
            
            public void setBufferSize(final int bytes) {
                this.iter.setBufferSize(bytes);
            }
            
            public void finish() {
            }
        }
    }
    
    private class StandardProfileFeature extends ProfileFeatureImpl
    {
        private Cursor cursor;
        
        StandardProfileFeature(final Station s, final double time, final Cursor cursor) throws IOException {
            super(StandardStationProfileCollectionImpl.this.timeUnit.makeStandardDateString(time), s.getLatitude(), s.getLongitude(), time, -1);
            this.cursor = cursor;
            if (Double.isNaN(time)) {
                try {
                    final PointFeatureIterator iter = this.getPointFeatureIterator(-1);
                    if (iter.hasNext()) {
                        final PointFeature pf = iter.next();
                        this.time = pf.getObservationTime();
                        this.name = StandardStationProfileCollectionImpl.this.timeUnit.makeStandardDateString(this.time);
                    }
                    else {
                        this.name = "empty";
                    }
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        public PointFeatureIterator getPointFeatureIterator(final int bufferSize) throws IOException {
            final Cursor cursorIter = this.cursor.copy();
            final StructureDataIterator structIter = StandardStationProfileCollectionImpl.this.ft.getLeafFeatureDataIterator(cursorIter, bufferSize);
            final StandardPointFeatureIterator iter = new StandardProfileFeatureIterator(StandardStationProfileCollectionImpl.this.ft, StandardStationProfileCollectionImpl.this.timeUnit, structIter, cursorIter);
            if (this.boundingBox == null || this.dateRange == null || this.npts < 0) {
                iter.setCalculateBounds(this);
            }
            return iter;
        }
        
        public Date getTime() {
            return StandardStationProfileCollectionImpl.this.timeUnit.makeDate(this.time);
        }
    }
    
    private class StandardProfileFeatureIterator extends StandardPointFeatureIterator
    {
        StandardProfileFeatureIterator(final NestedTable ft, final DateUnit timeUnit, final StructureDataIterator structIter, final Cursor cursor) throws IOException {
            super(ft, timeUnit, structIter, cursor);
        }
        
        @Override
        protected boolean isMissing() throws IOException {
            return super.isMissing() || this.ft.isAltMissing(this.cursor);
        }
    }
}
