// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard;

import ucar.nc2.ft.PointFeatureCollection;
import ucar.nc2.ft.PointFeatureIterator;
import ucar.nc2.ft.point.StationFeatureImpl;
import ucar.ma2.StructureDataIterator;
import ucar.nc2.ft.point.StationHelper;
import ucar.unidata.geoloc.Station;
import ucar.ma2.StructureData;
import java.io.IOException;
import ucar.nc2.units.DateUnit;
import ucar.nc2.ft.point.StationTimeSeriesCollectionImpl;

public class StandardStationCollectionImpl extends StationTimeSeriesCollectionImpl
{
    private DateUnit timeUnit;
    private NestedTable ft;
    
    StandardStationCollectionImpl(final NestedTable ft, final DateUnit timeUnit) throws IOException {
        super(ft.getName());
        this.timeUnit = timeUnit;
        this.ft = ft;
    }
    
    public Station makeStation(final StructureData stationData, final int recnum) {
        final Station s = this.ft.makeStation(stationData);
        if (s == null) {
            return null;
        }
        return new StandardStationFeatureImpl(s, this.timeUnit, stationData, recnum);
    }
    
    @Override
    protected void initStationHelper() {
        try {
            this.stationHelper = new StationHelper();
            final StructureDataIterator siter = this.ft.getStationDataIterator(-1);
            while (siter.hasNext()) {
                final StructureData stationData = siter.next();
                final Station s = this.makeStation(stationData, siter.getCurrentRecno());
                if (s != null) {
                    this.stationHelper.addStation(s);
                }
            }
        }
        catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }
    
    private class StandardStationFeatureImpl extends StationFeatureImpl
    {
        int recnum;
        StructureData stationData;
        
        StandardStationFeatureImpl(final Station s, final DateUnit dateUnit, final StructureData stationData, final int recnum) {
            super(s, dateUnit, -1);
            this.recnum = recnum;
            this.stationData = stationData;
        }
        
        public PointFeatureIterator getPointFeatureIterator(final int bufferSize) throws IOException {
            final Cursor cursor = new Cursor(StandardStationCollectionImpl.this.ft.getNumberOfLevels());
            cursor.recnum[1] = this.recnum;
            cursor.tableData[1] = this.stationData;
            cursor.currentIndex = 1;
            StandardStationCollectionImpl.this.ft.addParentJoin(cursor);
            final StructureDataIterator obsIter = StandardStationCollectionImpl.this.ft.getLeafFeatureDataIterator(cursor, bufferSize);
            final StandardPointFeatureIterator iter = new StandardPointFeatureIterator(StandardStationCollectionImpl.this.ft, this.timeUnit, obsIter, cursor);
            if (this.boundingBox == null || this.dateRange == null || this.npts < 0) {
                iter.setCalculateBounds(this);
            }
            return iter;
        }
    }
}
