// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard;

import ucar.nc2.dataset.VariableDS;
import java.io.IOException;
import ucar.ma2.StructureData;

public interface Join
{
    StructureData getJoinData(final Cursor p0) throws IOException;
    
    VariableDS findVariable(final String p0);
}
