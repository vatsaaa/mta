// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard;

import ucar.nc2.dataset.VariableDS;
import ucar.ma2.StructureData;
import java.io.IOException;
import ucar.ma2.ArrayStructure;
import ucar.nc2.dataset.StructureDS;

public class JoinMuiltdimStructure implements Join
{
    StructureDS parentStructure;
    ArrayStructure parentData;
    int dimLength;
    
    public JoinMuiltdimStructure(final StructureDS parentStructure, final int dimLength) {
        this.parentStructure = parentStructure;
        this.dimLength = dimLength;
        try {
            this.parentData = (ArrayStructure)parentStructure.read();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public StructureData getJoinData(final Cursor cursor) {
        final int recnum = cursor.recnum[0] / this.dimLength;
        return this.parentData.getStructureData(recnum);
    }
    
    public VariableDS findVariable(final String axisName) {
        return (VariableDS)this.parentStructure.findVariable(axisName);
    }
    
    @Override
    public String toString() {
        return "JoinMuiltdimStructure{parentStructure=" + this.parentStructure + ", dimLength='" + this.dimLength + '}';
    }
}
