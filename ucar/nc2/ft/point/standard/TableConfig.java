// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard;

import java.util.ArrayList;
import ucar.ma2.StructureData;
import ucar.ma2.ArrayStructure;
import ucar.nc2.constants.FeatureType;
import java.util.List;

public class TableConfig
{
    public Table.Type type;
    public String name;
    public TableConfig parent;
    public List<TableConfig> children;
    public List<Join> extraJoin;
    public String structName;
    public String nestedTableName;
    public StructureType structureType;
    public String start;
    public String next;
    public String numRecords;
    public FeatureType featureType;
    public List<String> vars;
    public String dimName;
    public String outerName;
    public String innerName;
    public ArrayStructure as;
    public StructureData sdata;
    public String parentIndex;
    public String lat;
    public String lon;
    public String elev;
    public String time;
    public String timeNominal;
    public String limit;
    public String stnId;
    public String stnDesc;
    public String stnNpts;
    public String stnWmoId;
    public String stnAlt;
    public String feature_id;
    public String missingVar;
    
    public TableConfig(final Table.Type type, final String name) {
        this.structureType = StructureType.Structure;
        this.type = type;
        this.name = name;
        this.structName = name;
    }
    
    public void addChild(final TableConfig t) {
        if (this.children == null) {
            this.children = new ArrayList<TableConfig>();
        }
        this.children.add(t);
        t.parent = this;
    }
    
    public void addJoin(final Join extra) {
        if (this.extraJoin == null) {
            this.extraJoin = new ArrayList<Join>(3);
        }
        this.extraJoin.add(extra);
    }
    
    public String findCoordinateVariableName(final Table.CoordName coordName) {
        switch (coordName) {
            case Elev: {
                return this.elev;
            }
            case Lat: {
                return this.lat;
            }
            case Lon: {
                return this.lon;
            }
            case Time: {
                return this.time;
            }
            case TimeNominal: {
                return this.timeNominal;
            }
            case StnId: {
                return this.stnId;
            }
            case StnDesc: {
                return this.stnDesc;
            }
            case WmoId: {
                return this.stnWmoId;
            }
            case StnAlt: {
                return this.stnAlt;
            }
            case FeatureId: {
                return this.feature_id;
            }
            case MissingVar: {
                return this.missingVar;
            }
            default: {
                return null;
            }
        }
    }
    
    public void setCoordinateVariableName(final Table.CoordName coordName, final String name) {
        switch (coordName) {
            case Elev: {
                this.elev = name;
                break;
            }
            case Lat: {
                this.lat = name;
                break;
            }
            case Lon: {
                this.lon = name;
            }
            case Time: {
                this.time = name;
                break;
            }
            case TimeNominal: {
                this.timeNominal = name;
                break;
            }
            case StnId: {
                this.stnId = name;
                break;
            }
            case StnDesc: {
                this.stnDesc = name;
                break;
            }
            case WmoId: {
                this.stnWmoId = name;
                break;
            }
            case StnAlt: {
                this.stnAlt = name;
                break;
            }
            case FeatureId: {
                this.feature_id = name;
                break;
            }
            case MissingVar: {
                this.missingVar = name;
                break;
            }
        }
    }
    
    public enum StructureType
    {
        Structure, 
        PsuedoStructure, 
        PsuedoStructure2D;
    }
}
