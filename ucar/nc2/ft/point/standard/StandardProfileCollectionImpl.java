// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard;

import ucar.nc2.ft.point.PointCollectionIteratorFiltered;
import java.util.Date;
import ucar.nc2.ft.PointFeature;
import ucar.nc2.ft.PointFeatureIterator;
import ucar.nc2.ft.point.ProfileFeatureImpl;
import ucar.nc2.ft.PointFeatureCollection;
import ucar.ma2.StructureData;
import ucar.ma2.StructureDataIterator;
import ucar.nc2.ft.NestedPointFeatureCollection;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.ft.ProfileFeature;
import java.io.IOException;
import ucar.nc2.ft.PointFeatureCollectionIterator;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.units.DateUnit;
import ucar.nc2.ft.ProfileFeatureCollection;
import ucar.nc2.ft.point.OneNestedPointCollectionImpl;

public class StandardProfileCollectionImpl extends OneNestedPointCollectionImpl implements ProfileFeatureCollection
{
    private DateUnit timeUnit;
    private NestedTable ft;
    private ProfileIterator localIterator;
    
    protected StandardProfileCollectionImpl(final String name) {
        super(name, FeatureType.PROFILE);
        this.localIterator = null;
    }
    
    StandardProfileCollectionImpl(final NestedTable ft, final DateUnit timeUnit) {
        super(ft.getName(), FeatureType.PROFILE);
        this.localIterator = null;
        this.ft = ft;
        this.timeUnit = timeUnit;
    }
    
    public PointFeatureCollectionIterator getPointFeatureCollectionIterator(final int bufferSize) throws IOException {
        return new ProfileIterator(this.ft.getRootFeatureDataIterator(bufferSize));
    }
    
    public boolean hasNext() throws IOException {
        if (this.localIterator == null) {
            this.resetIteration();
        }
        return this.localIterator.hasNext();
    }
    
    public ProfileFeature next() throws IOException {
        return this.localIterator.next();
    }
    
    public void resetIteration() throws IOException {
        this.localIterator = (ProfileIterator)this.getPointFeatureCollectionIterator(-1);
    }
    
    public ProfileFeatureCollection subset(final LatLonRect boundingBox) throws IOException {
        return new StandardProfileCollectionSubset(this, boundingBox);
    }
    
    private class ProfileIterator implements PointFeatureCollectionIterator
    {
        StructureDataIterator structIter;
        StructureData nextProfileData;
        
        ProfileIterator(final StructureDataIterator structIter) throws IOException {
            this.structIter = structIter;
        }
        
        public boolean hasNext() throws IOException {
            while (this.structIter.hasNext()) {
                this.nextProfileData = this.structIter.next();
                if (!StandardProfileCollectionImpl.this.ft.isFeatureMissing(this.nextProfileData)) {
                    return true;
                }
            }
            return false;
        }
        
        public ProfileFeature next() throws IOException {
            final Cursor cursor = new Cursor(StandardProfileCollectionImpl.this.ft.getNumberOfLevels());
            cursor.tableData[1] = this.nextProfileData;
            cursor.recnum[1] = this.structIter.getCurrentRecno();
            cursor.currentIndex = 1;
            StandardProfileCollectionImpl.this.ft.addParentJoin(cursor);
            return new StandardProfileFeature(cursor, StandardProfileCollectionImpl.this.ft.getObsTime(cursor));
        }
        
        public void setBufferSize(final int bytes) {
        }
        
        public void finish() {
        }
    }
    
    private class StandardProfileFeature extends ProfileFeatureImpl
    {
        Cursor cursor;
        
        StandardProfileFeature(final Cursor cursor, final double time) {
            super(StandardProfileCollectionImpl.this.timeUnit.makeStandardDateString(time), StandardProfileCollectionImpl.this.ft.getLatitude(cursor), StandardProfileCollectionImpl.this.ft.getLongitude(cursor), time, -1);
            this.cursor = cursor;
            if (Double.isNaN(time)) {
                try {
                    final PointFeatureIterator iter = this.getPointFeatureIterator(-1);
                    if (iter.hasNext()) {
                        final PointFeature pf = iter.next();
                        this.time = pf.getObservationTime();
                        this.name = StandardProfileCollectionImpl.this.timeUnit.makeStandardDateString(this.time);
                    }
                    else {
                        this.name = "empty";
                    }
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        public PointFeatureIterator getPointFeatureIterator(final int bufferSize) throws IOException {
            final Cursor cursorIter = this.cursor.copy();
            final StructureDataIterator siter = StandardProfileCollectionImpl.this.ft.getLeafFeatureDataIterator(cursorIter, bufferSize);
            final StandardPointFeatureIterator iter = new StandardProfileFeatureIterator(StandardProfileCollectionImpl.this.ft, StandardProfileCollectionImpl.this.timeUnit, siter, cursorIter);
            if (this.boundingBox == null || this.dateRange == null || this.npts < 0) {
                iter.setCalculateBounds(this);
            }
            return iter;
        }
        
        public Date getTime() {
            return StandardProfileCollectionImpl.this.timeUnit.makeDate(this.time);
        }
        
        class StandardProfileFeatureIterator extends StandardPointFeatureIterator
        {
            StandardProfileFeatureIterator(final NestedTable ft, final DateUnit timeUnit, final StructureDataIterator structIter, final Cursor cursor) throws IOException {
                super(ft, timeUnit, structIter, cursor);
            }
            
            @Override
            protected boolean isMissing() throws IOException {
                return super.isMissing() || this.ft.isAltMissing(this.cursor);
            }
        }
    }
    
    private class StandardProfileCollectionSubset extends StandardProfileCollectionImpl
    {
        StandardProfileCollectionImpl from;
        LatLonRect boundingBox;
        
        StandardProfileCollectionSubset(final StandardProfileCollectionImpl from, final LatLonRect boundingBox) {
            super(from.getName() + "-subset");
            this.from = from;
            this.boundingBox = boundingBox;
        }
        
        @Override
        public PointFeatureCollectionIterator getPointFeatureCollectionIterator(final int bufferSize) throws IOException {
            return new PointCollectionIteratorFiltered(this.from.getPointFeatureCollectionIterator(bufferSize), new Filter());
        }
        
        private class Filter implements PointFeatureCollectionIterator.Filter
        {
            public boolean filter(final PointFeatureCollection pointFeatureCollection) {
                final ProfileFeature profileFeature = (ProfileFeature)pointFeatureCollection;
                return StandardProfileCollectionSubset.this.boundingBox.contains(profileFeature.getLatLon());
            }
        }
    }
}
