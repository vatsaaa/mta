// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.standard;

import ucar.ma2.StructureDataFactory;
import ucar.ma2.StructureData;
import java.io.IOException;
import ucar.ma2.Array;
import ucar.nc2.dataset.VariableDS;

public class JoinArray implements Join
{
    VariableDS v;
    Array data;
    Type type;
    int param;
    
    public JoinArray(final VariableDS v, final Type type, final int param) {
        this.v = v;
        this.type = type;
        this.param = param;
        try {
            this.data = v.read();
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    public StructureData getJoinData(final Cursor cursor) {
        int recnum = -1;
        switch (this.type) {
            case modulo: {
                recnum = cursor.recnum[0] % this.param;
                break;
            }
            case divide: {
                recnum = cursor.recnum[0] / this.param;
                break;
            }
            case level: {
                recnum = cursor.recnum[this.param];
                break;
            }
            case raw: {
                recnum = cursor.recnum[0];
                break;
            }
            case scalar: {
                recnum = 0;
                break;
            }
        }
        return StructureDataFactory.make(this.v.getShortName(), this.data.getObject(recnum));
    }
    
    public VariableDS findVariable(final String varName) {
        return varName.equals(this.v.getName()) ? this.v : null;
    }
    
    @Override
    public String toString() {
        return "JoinArray{v=" + this.v.getName() + ", type=" + this.type + ", param=" + this.param + '}';
    }
    
    public enum Type
    {
        modulo, 
        divide, 
        level, 
        raw, 
        scalar;
    }
}
