// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point;

import java.io.IOException;
import ucar.nc2.ft.PointFeatureCollection;
import ucar.nc2.units.DateRange;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.ft.PointFeature;
import ucar.nc2.ft.PointFeatureIterator;
import ucar.nc2.ft.PointFeatureCollectionIterator;

public class PointIteratorFlatten extends PointIteratorAbstract
{
    private PointFeatureCollectionIterator collectionIter;
    private Filter filter;
    private PointFeatureIterator pfiter;
    private PointFeature pointFeature;
    private boolean finished;
    
    PointIteratorFlatten(final PointFeatureCollectionIterator collectionIter, final LatLonRect filter_bb, final DateRange filter_date) {
        this.filter = null;
        this.finished = false;
        this.collectionIter = collectionIter;
        if (filter_bb != null || filter_date != null) {
            this.filter = new Filter(filter_bb, filter_date);
        }
    }
    
    public void setBufferSize(final int bytes) {
        this.collectionIter.setBufferSize(bytes);
    }
    
    public void finish() {
        if (this.finished) {
            return;
        }
        if (this.pfiter != null) {
            this.pfiter.finish();
        }
        this.collectionIter.finish();
        this.finishCalcBounds();
        this.finished = true;
    }
    
    public boolean hasNext() throws IOException {
        this.pointFeature = this.nextFilteredDataPoint();
        if (this.pointFeature != null) {
            return true;
        }
        final PointFeatureCollection feature = this.nextCollection();
        if (feature == null) {
            this.finish();
            return false;
        }
        this.pfiter = feature.getPointFeatureIterator(-1);
        return this.hasNext();
    }
    
    public PointFeature next() throws IOException {
        if (this.pointFeature == null) {
            return null;
        }
        this.calcBounds(this.pointFeature);
        return this.pointFeature;
    }
    
    private PointFeatureCollection nextCollection() throws IOException {
        if (!this.collectionIter.hasNext()) {
            return null;
        }
        return this.collectionIter.next();
    }
    
    private PointFeature nextFilteredDataPoint() throws IOException {
        if (this.pfiter == null) {
            return null;
        }
        if (!this.pfiter.hasNext()) {
            return null;
        }
        PointFeature pdata = this.pfiter.next();
        if (this.filter == null) {
            return pdata;
        }
        while (!this.filter.filter(pdata)) {
            if (!this.pfiter.hasNext()) {
                return null;
            }
            pdata = this.pfiter.next();
        }
        return pdata;
    }
}
