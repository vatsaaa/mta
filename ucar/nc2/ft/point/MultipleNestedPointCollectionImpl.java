// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point;

import ucar.nc2.ft.PointFeatureIterator;
import ucar.nc2.ft.PointFeatureCollection;
import ucar.nc2.units.DateRange;
import ucar.unidata.geoloc.LatLonRect;
import java.io.IOException;
import ucar.nc2.ft.PointFeatureCollectionIterator;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.ft.NestedPointFeatureCollection;

public abstract class MultipleNestedPointCollectionImpl implements NestedPointFeatureCollection
{
    protected String name;
    private FeatureType collectionFeatureType;
    private int npts;
    
    protected MultipleNestedPointCollectionImpl(final String name, final FeatureType collectionFeatureType) {
        this.name = name;
        this.collectionFeatureType = collectionFeatureType;
        this.npts = -1;
    }
    
    public String getName() {
        return this.name;
    }
    
    public int size() {
        return this.npts;
    }
    
    protected void setSize(final int npts) {
        this.npts = npts;
    }
    
    public boolean isMultipleNested() {
        return true;
    }
    
    public FeatureType getCollectionFeatureType() {
        return this.collectionFeatureType;
    }
    
    public PointFeatureCollectionIterator getPointFeatureCollectionIterator(final int bufferSize) throws IOException {
        throw new UnsupportedOperationException(this.getClass().getName() + " (multipleNested) PointFeatureCollection does not implement getPointFeatureCollectionIterator()");
    }
    
    public PointFeatureCollection flatten(final LatLonRect boundingBox, final DateRange dateRange) throws IOException {
        return new NestedPointFeatureCollectionFlatten(this, boundingBox, dateRange);
    }
    
    private class NestedPointFeatureCollectionFlatten extends PointCollectionImpl
    {
        protected MultipleNestedPointCollectionImpl from;
        protected LatLonRect boundingBox;
        protected DateRange dateRange;
        
        NestedPointFeatureCollectionFlatten(final MultipleNestedPointCollectionImpl from, final LatLonRect filter_bb, final DateRange filter_date) {
            super(from.getName());
            this.from = from;
            this.boundingBox = filter_bb;
            this.dateRange = filter_date;
        }
        
        public PointFeatureIterator getPointFeatureIterator(final int bufferSize) throws IOException {
            return new PointIteratorFlatten(this.from.getPointFeatureCollectionIterator(bufferSize), this.boundingBox, this.dateRange);
        }
    }
}
