// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point;

import java.io.IOException;
import ucar.nc2.ft.PointFeatureCollection;
import ucar.nc2.ft.PointFeatureCollectionIterator;

public class PointCollectionIteratorFiltered implements PointFeatureCollectionIterator
{
    private PointFeatureCollectionIterator pfciter;
    private Filter filter;
    private PointFeatureCollection pointFeatureCollection;
    private boolean done;
    
    public PointCollectionIteratorFiltered(final PointFeatureCollectionIterator pfciter, final Filter filter) {
        this.done = false;
        this.pfciter = pfciter;
        this.filter = filter;
    }
    
    public void setBufferSize(final int bytes) {
        this.pfciter.setBufferSize(bytes);
    }
    
    public boolean hasNext() throws IOException {
        if (this.done) {
            return false;
        }
        this.pointFeatureCollection = this.nextFilteredPointFeatureCollection();
        return this.pointFeatureCollection != null;
    }
    
    public PointFeatureCollection next() throws IOException {
        return this.done ? null : this.pointFeatureCollection;
    }
    
    public void finish() {
        this.pfciter.finish();
    }
    
    private boolean filter(final PointFeatureCollection pdata) {
        return this.filter == null || this.filter.filter(pdata);
    }
    
    private PointFeatureCollection nextFilteredPointFeatureCollection() throws IOException {
        if (this.pfciter == null) {
            return null;
        }
        if (!this.pfciter.hasNext()) {
            return null;
        }
        PointFeatureCollection pdata;
        for (pdata = this.pfciter.next(); !this.filter(pdata); pdata = this.pfciter.next()) {
            if (!this.pfciter.hasNext()) {
                return null;
            }
        }
        return pdata;
    }
}
