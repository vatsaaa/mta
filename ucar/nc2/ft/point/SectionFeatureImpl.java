// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point;

import ucar.nc2.ft.ProfileFeature;
import java.io.IOException;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.ft.PointFeatureCollectionIterator;
import ucar.nc2.ft.SectionFeature;

public abstract class SectionFeatureImpl extends OneNestedPointCollectionImpl implements SectionFeature
{
    protected PointFeatureCollectionIterator localIterator;
    
    protected SectionFeatureImpl(final String name) {
        super(name, FeatureType.SECTION);
    }
    
    public boolean hasNext() throws IOException {
        if (this.localIterator == null) {
            this.resetIteration();
        }
        return this.localIterator.hasNext();
    }
    
    public ProfileFeature next() throws IOException {
        return (ProfileFeature)this.localIterator.next();
    }
    
    public void resetIteration() throws IOException {
        this.localIterator = this.getPointFeatureCollectionIterator(-1);
    }
}
