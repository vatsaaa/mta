// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point;

import java.util.Date;
import ucar.nc2.units.DateUnit;
import ucar.unidata.geoloc.EarthLocation;
import ucar.nc2.ft.PointFeature;

public abstract class PointFeatureImpl implements PointFeature, Comparable<PointFeature>
{
    protected EarthLocation location;
    protected double obsTime;
    protected double nomTime;
    protected DateUnit timeUnit;
    
    public PointFeatureImpl(final DateUnit timeUnit) {
        this.timeUnit = timeUnit;
    }
    
    public PointFeatureImpl(final EarthLocation location, final double obsTime, final double nomTime, final DateUnit timeUnit) {
        this.location = location;
        this.obsTime = obsTime;
        this.nomTime = ((nomTime == 0.0) ? obsTime : nomTime);
        this.timeUnit = timeUnit;
    }
    
    public EarthLocation getLocation() {
        return this.location;
    }
    
    public double getNominalTime() {
        return this.nomTime;
    }
    
    public double getObservationTime() {
        return this.obsTime;
    }
    
    public String getZcoordUnits() {
        return "meters";
    }
    
    public String getDescription() {
        return this.location.toString();
    }
    
    public Date getObservationTimeAsDate() {
        return this.timeUnit.makeDate(this.getObservationTime());
    }
    
    public Date getNominalTimeAsDate() {
        return this.timeUnit.makeDate(this.getNominalTime());
    }
    
    public DateUnit getTimeUnit() {
        return this.timeUnit;
    }
    
    public int compareTo(final PointFeature other) {
        if (this.obsTime < other.getObservationTime()) {
            return -1;
        }
        if (this.obsTime > other.getObservationTime()) {
            return 1;
        }
        return 0;
    }
    
    @Override
    public String toString() {
        return "PointFeatureImpl{location=" + this.location + ", obsTime=" + this.obsTime + ", nomTime=" + this.nomTime + ", timeUnit=" + this.timeUnit + '}';
    }
}
