// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point;

import ucar.nc2.ft.NestedPointFeatureCollection;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.ft.SectionFeature;
import java.io.IOException;
import ucar.nc2.ft.PointFeatureCollectionIterator;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.ft.NestedPointFeatureCollectionIterator;
import ucar.nc2.ft.SectionFeatureCollection;

public abstract class SectionCollectionImpl extends MultipleNestedPointCollectionImpl implements SectionFeatureCollection
{
    private NestedPointFeatureCollectionIterator localIterator;
    
    protected SectionCollectionImpl(final String name) {
        super(name, FeatureType.SECTION);
    }
    
    @Override
    public PointFeatureCollectionIterator getPointFeatureCollectionIterator(final int bufferSize) throws IOException {
        throw new UnsupportedOperationException("SectionCollectionImpl does not implement getPointFeatureCollectionIterator()");
    }
    
    public boolean hasNext() throws IOException {
        if (this.localIterator == null) {
            this.resetIteration();
        }
        return this.localIterator.hasNext();
    }
    
    public SectionFeature next() throws IOException {
        return (SectionFeature)this.localIterator.next();
    }
    
    public void resetIteration() throws IOException {
        this.localIterator = this.getNestedPointFeatureCollectionIterator(-1);
    }
    
    public SectionFeatureCollection subset(final LatLonRect boundingBox) throws IOException {
        return null;
    }
}
