// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point;

import ucar.nc2.ft.NestedPointFeatureCollection;
import ucar.nc2.ft.PointFeatureCollectionIterator;
import ucar.nc2.ft.StationProfileFeature;
import java.io.IOException;
import ucar.unidata.geoloc.LatLonRect;
import ucar.unidata.geoloc.Station;
import java.util.List;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.ft.NestedPointFeatureCollectionIterator;
import ucar.nc2.ft.StationProfileFeatureCollection;

public abstract class StationProfileCollectionImpl extends MultipleNestedPointCollectionImpl implements StationProfileFeatureCollection
{
    protected StationHelper stationHelper;
    protected NestedPointFeatureCollectionIterator localIterator;
    
    public StationProfileCollectionImpl(final String name) {
        super(name, FeatureType.STATION_PROFILE);
    }
    
    protected abstract void initStationHelper();
    
    public List<Station> getStations() {
        if (this.stationHelper == null) {
            this.initStationHelper();
        }
        return this.stationHelper.getStations();
    }
    
    public List<Station> getStations(final List<String> stnNames) {
        if (this.stationHelper == null) {
            this.initStationHelper();
        }
        return this.stationHelper.getStations(stnNames);
    }
    
    public List<Station> getStations(final LatLonRect boundingBox) throws IOException {
        if (this.stationHelper == null) {
            this.initStationHelper();
        }
        return this.stationHelper.getStations(boundingBox);
    }
    
    public Station getStation(final String name) {
        if (this.stationHelper == null) {
            this.initStationHelper();
        }
        return this.stationHelper.getStation(name);
    }
    
    public LatLonRect getBoundingBox() {
        if (this.stationHelper == null) {
            this.initStationHelper();
        }
        return this.stationHelper.getBoundingBox();
    }
    
    public StationProfileCollectionImpl subset(final List<Station> stations) throws IOException {
        if (stations == null) {
            return this;
        }
        return new StationProfileFeatureCollectionSubset(this, stations);
    }
    
    public StationProfileCollectionImpl subset(final LatLonRect boundingBox) throws IOException {
        return this.subset(this.getStations(boundingBox));
    }
    
    public StationProfileFeature getStationProfileFeature(final Station s) throws IOException {
        return (StationProfileFeature)s;
    }
    
    @Override
    public PointFeatureCollectionIterator getPointFeatureCollectionIterator(final int bufferSize) throws IOException {
        throw new UnsupportedOperationException("StationProfileFeatureCollection does not implement getPointFeatureCollectionIterator()");
    }
    
    public boolean hasNext() throws IOException {
        if (this.localIterator == null) {
            this.resetIteration();
        }
        return this.localIterator.hasNext();
    }
    
    public StationProfileFeature next() throws IOException {
        return (StationProfileFeature)this.localIterator.next();
    }
    
    public void resetIteration() throws IOException {
        this.localIterator = this.getNestedPointFeatureCollectionIterator(-1);
    }
    
    public int compareTo(final Station so) {
        return this.name.compareTo(so.getName());
    }
    
    private class StationProfileFeatureCollectionSubset extends StationProfileCollectionImpl
    {
        StationProfileCollectionImpl from;
        
        StationProfileFeatureCollectionSubset(final StationProfileCollectionImpl from, final List<Station> stations) {
            super(from.getName());
            this.from = from;
            (this.stationHelper = new StationHelper()).setStations(stations);
        }
        
        @Override
        protected void initStationHelper() {
        }
        
        public NestedPointFeatureCollectionIterator getNestedPointFeatureCollectionIterator(final int bufferSize) throws IOException {
            return new NestedPointCollectionIteratorFiltered(this.from.getNestedPointFeatureCollectionIterator(bufferSize), new Filter());
        }
        
        private class Filter implements NestedPointFeatureCollectionIterator.Filter
        {
            public boolean filter(final NestedPointFeatureCollection pointFeatureCollection) {
                final StationProfileFeature stationFeature = (StationProfileFeature)pointFeatureCollection;
                return StationProfileFeatureCollectionSubset.this.stationHelper.getStation(stationFeature.getName()) != null;
            }
        }
    }
}
