// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point;

import ucar.nc2.ft.NestedPointFeatureCollection;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.units.DateRange;
import ucar.nc2.ft.ProfileFeature;
import java.io.IOException;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.StationImpl;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.ft.PointFeatureCollectionIterator;
import ucar.unidata.geoloc.Station;
import ucar.nc2.units.DateUnit;
import ucar.nc2.ft.StationProfileFeature;

public abstract class StationProfileFeatureImpl extends OneNestedPointCollectionImpl implements StationProfileFeature
{
    protected DateUnit timeUnit;
    protected int timeSeriesNpts;
    protected Station s;
    protected PointFeatureCollectionIterator localIterator;
    
    public StationProfileFeatureImpl(final String name, final String desc, final String wmoId, final double lat, final double lon, final double alt, final DateUnit timeUnit, final int npts) {
        super(name, FeatureType.STATION_PROFILE);
        this.s = new StationImpl(name, desc, wmoId, lat, lon, alt);
        this.timeUnit = timeUnit;
        this.timeSeriesNpts = npts;
    }
    
    public StationProfileFeatureImpl(final Station s, final DateUnit timeUnit, final int npts) {
        super(s.getName(), FeatureType.STATION_PROFILE);
        this.s = s;
        this.timeUnit = timeUnit;
        this.timeSeriesNpts = npts;
    }
    
    public String getWmoId() {
        return this.s.getWmoId();
    }
    
    @Override
    public int size() {
        return this.timeSeriesNpts;
    }
    
    @Override
    public String getName() {
        return this.s.getName();
    }
    
    public String getDescription() {
        return this.s.getDescription();
    }
    
    public double getLatitude() {
        return this.s.getLatitude();
    }
    
    public double getLongitude() {
        return this.s.getLongitude();
    }
    
    public double getAltitude() {
        return this.s.getAltitude();
    }
    
    public LatLonPoint getLatLon() {
        return this.s.getLatLon();
    }
    
    public boolean isMissing() {
        return Double.isNaN(this.getLatitude()) || Double.isNaN(this.getLongitude());
    }
    
    public boolean hasNext() throws IOException {
        if (this.localIterator == null) {
            this.resetIteration();
        }
        return this.localIterator.hasNext();
    }
    
    public ProfileFeature next() throws IOException {
        return (ProfileFeature)this.localIterator.next();
    }
    
    public void resetIteration() throws IOException {
        this.localIterator = this.getPointFeatureCollectionIterator(-1);
    }
    
    public int compareTo(final Station so) {
        return this.s.getName().compareTo(so.getName());
    }
    
    public StationProfileFeature subset(final DateRange dateRange) throws IOException {
        return null;
    }
    
    public StationProfileFeature subset(final LatLonRect dateRange) throws IOException {
        return this;
    }
}
