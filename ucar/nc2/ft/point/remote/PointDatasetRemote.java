// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.remote;

import ucar.nc2.units.DateFormatter;
import java.io.IOException;
import ucar.nc2.ft.FeatureCollection;
import java.util.Collection;
import java.util.ArrayList;
import ucar.nc2.units.DateRange;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.VariableSimpleIF;
import java.util.List;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.ft.point.PointDatasetImpl;

public class PointDatasetRemote extends PointDatasetImpl
{
    public PointDatasetRemote(final FeatureType wantFeatureType, final String uri, final List<VariableSimpleIF> vars, final LatLonRect bb, final DateRange dr) throws IOException {
        super(wantFeatureType);
        this.setBoundingBox(bb);
        this.setDateRange(dr);
        this.dataVariables = new ArrayList<VariableSimpleIF>(vars);
        this.collectionList = new ArrayList<FeatureCollection>(1);
        switch (wantFeatureType) {
            case POINT: {
                this.collectionList.add(new RemotePointCollection(uri, null));
                break;
            }
            case STATION: {
                this.collectionList.add(new RemoteStationCollection(uri));
                break;
            }
            default: {
                throw new UnsupportedOperationException("No implemenattion for " + wantFeatureType);
            }
        }
    }
    
    static String makeQuery(final String station, final LatLonRect boundingBox, final DateRange dateRange) {
        final StringBuilder query = new StringBuilder();
        boolean needamp = false;
        if (station != null) {
            query.append(station);
            needamp = true;
        }
        if (boundingBox != null) {
            if (needamp) {
                query.append("&");
            }
            query.append("west=");
            query.append(boundingBox.getLonMin());
            query.append("&east=");
            query.append(boundingBox.getLonMax());
            query.append("&south=");
            query.append(boundingBox.getLatMin());
            query.append("&north=");
            query.append(boundingBox.getLatMax());
            needamp = true;
        }
        if (dateRange != null) {
            final DateFormatter df = new DateFormatter();
            if (needamp) {
                query.append("&");
            }
            query.append("time_start=");
            query.append(df.toDateTimeStringISO(dateRange.getStart().getDate()));
            query.append("&time_end=");
            query.append(df.toDateTimeStringISO(dateRange.getEnd().getDate()));
        }
        if (!needamp) {
            query.append("all");
        }
        return query.toString();
    }
}
