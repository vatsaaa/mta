// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.remote;

import ucar.nc2.stream.NcStreamProto;
import ucar.nc2.stream.NcStream;
import java.io.IOException;
import ucar.nc2.ft.PointFeature;
import java.io.InputStream;
import org.apache.commons.httpclient.HttpMethod;
import ucar.nc2.ft.point.PointIteratorAbstract;

public class RemotePointFeatureIterator extends PointIteratorAbstract
{
    private static final boolean debug = false;
    private HttpMethod method;
    private InputStream in;
    private FeatureMaker featureMaker;
    private PointFeature pf;
    private boolean finished;
    
    RemotePointFeatureIterator(final HttpMethod method, final InputStream in, final FeatureMaker featureMaker) throws IOException {
        this.finished = false;
        this.method = method;
        this.in = in;
        this.featureMaker = featureMaker;
    }
    
    public void finish() {
        if (this.finished) {
            return;
        }
        if (this.method != null) {
            this.method.releaseConnection();
        }
        this.method = null;
        this.finishCalcBounds();
        this.finished = true;
    }
    
    public boolean hasNext() throws IOException {
        if (this.finished) {
            return false;
        }
        final PointStream.MessageType mtype = PointStream.readMagic(this.in);
        if (mtype == PointStream.MessageType.PointFeature) {
            final int len = NcStream.readVInt(this.in);
            final byte[] b = new byte[len];
            NcStream.readFully(this.in, b);
            this.pf = this.featureMaker.make(b);
            return true;
        }
        if (mtype == PointStream.MessageType.End) {
            this.pf = null;
            this.finish();
            return false;
        }
        if (mtype == PointStream.MessageType.Error) {
            final int len = NcStream.readVInt(this.in);
            final byte[] b = new byte[len];
            NcStream.readFully(this.in, b);
            final NcStreamProto.Error proto = NcStreamProto.Error.parseFrom(b);
            final String errMessage = NcStream.decodeErrorMessage(proto);
            this.pf = null;
            this.finish();
            throw new IOException(errMessage);
        }
        this.pf = null;
        this.finish();
        throw new IOException("Illegal pointstream message type= " + mtype);
    }
    
    public PointFeature next() throws IOException {
        if (null == this.pf) {
            return null;
        }
        this.calcBounds(this.pf);
        return this.pf;
    }
    
    public void setBufferSize(final int bytes) {
    }
}
