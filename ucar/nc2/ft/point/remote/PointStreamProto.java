// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.remote;

import java.util.Collection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Collections;
import java.util.List;
import com.google.protobuf.AbstractMessage;
import com.google.protobuf.UnknownFieldSet;
import com.google.protobuf.UninitializedMessageException;
import com.google.protobuf.Message;
import com.google.protobuf.CodedInputStream;
import java.io.InputStream;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.ByteString;
import java.io.IOException;
import com.google.protobuf.CodedOutputStream;
import ucar.nc2.stream.NcStreamProto;
import com.google.protobuf.ExtensionRegistry;
import com.google.protobuf.GeneratedMessage;
import com.google.protobuf.Descriptors;

public final class PointStreamProto
{
    private static Descriptors.Descriptor internal_static_pointStream_Location_descriptor;
    private static GeneratedMessage.FieldAccessorTable internal_static_pointStream_Location_fieldAccessorTable;
    private static Descriptors.Descriptor internal_static_pointStream_PointFeature_descriptor;
    private static GeneratedMessage.FieldAccessorTable internal_static_pointStream_PointFeature_fieldAccessorTable;
    private static Descriptors.Descriptor internal_static_pointStream_Member_descriptor;
    private static GeneratedMessage.FieldAccessorTable internal_static_pointStream_Member_fieldAccessorTable;
    private static Descriptors.Descriptor internal_static_pointStream_PointFeatureCollection_descriptor;
    private static GeneratedMessage.FieldAccessorTable internal_static_pointStream_PointFeatureCollection_fieldAccessorTable;
    private static Descriptors.Descriptor internal_static_pointStream_Station_descriptor;
    private static GeneratedMessage.FieldAccessorTable internal_static_pointStream_Station_fieldAccessorTable;
    private static Descriptors.Descriptor internal_static_pointStream_StationList_descriptor;
    private static GeneratedMessage.FieldAccessorTable internal_static_pointStream_StationList_fieldAccessorTable;
    private static Descriptors.FileDescriptor descriptor;
    
    private PointStreamProto() {
    }
    
    public static void registerAllExtensions(final ExtensionRegistry registry) {
    }
    
    public static Descriptors.FileDescriptor getDescriptor() {
        return PointStreamProto.descriptor;
    }
    
    static {
        final String descriptorData = "\n*ucar/nc2/ft/point/remote/pointStream.proto\u0012\u000bpointStream\u001a\u001eucar/nc2/stream/ncStream.proto\"P\n\bLocation\u0012\f\n\u0004time\u0018\u0001 \u0002(\u0001\u0012\u000b\n\u0003lat\u0018\u0002 \u0002(\u0001\u0012\u000b\n\u0003lon\u0018\u0003 \u0002(\u0001\u0012\u000b\n\u0003alt\u0018\u0004 \u0001(\u0001\u0012\u000f\n\u0007nomTime\u0018\u0005 \u0001(\u0001\"O\n\fPointFeature\u0012\"\n\u0003loc\u0018\u0001 \u0002(\u000b2\u0015.pointStream.Location\u0012\f\n\u0004data\u0018\u0003 \u0002(\f\u0012\r\n\u0005sdata\u0018\u0004 \u0003(\t\"}\n\u0006Member\u0012\f\n\u0004name\u0018\u0001 \u0002(\t\u0012\f\n\u0004desc\u0018\u0002 \u0001(\t\u0012\r\n\u0005units\u0018\u0003 \u0001(\t\u0012$\n\bdataType\u0018\u0004 \u0002(\u000e2\u0012.ncstream.DataType\u0012\"\n\u0007section\u0018\u0005 \u0002(\u000b2\u0011.ncstream.Section\"^\n\u0016PointFeatureCollection\u0012\f\n\u0004name\u0018\u0001 \u0002(\t\u0012\u0010\n\btimeUnit\u0018\u0002 \u0002(\t\u0012$\n\u0007members\u0018\u0003 \u0003(\u000b2\u0013.pointStream.Member\"Y\n\u0007Station\u0012\n\n\u0002id\u0018\u0001 \u0002(\t\u0012\u000b\n\u0003lat\u0018\u0002 \u0002(\u0001\u0012\u000b\n\u0003lon\u0018\u0003 \u0002(\u0001\u0012\u000b\n\u0003alt\u0018\u0004 \u0001(\u0001\u0012\f\n\u0004desc\u0018\u0005 \u0001(\t\u0012\r\n\u0005wmoId\u0018\u0006 \u0001(\t\"5\n\u000bStationList\u0012&\n\bstations\u0018\u0001 \u0003(\u000b2\u0014.pointStream.StationB,\n\u0018ucar.nc2.ft.point.remoteB\u0010PointStreamProto";
        final Descriptors.FileDescriptor.InternalDescriptorAssigner assigner = (Descriptors.FileDescriptor.InternalDescriptorAssigner)new Descriptors.FileDescriptor.InternalDescriptorAssigner() {
            public ExtensionRegistry assignDescriptors(final Descriptors.FileDescriptor root) {
                PointStreamProto.descriptor = root;
                PointStreamProto.internal_static_pointStream_Location_descriptor = PointStreamProto.getDescriptor().getMessageTypes().get(0);
                PointStreamProto.internal_static_pointStream_Location_fieldAccessorTable = new GeneratedMessage.FieldAccessorTable(PointStreamProto.internal_static_pointStream_Location_descriptor, new String[] { "Time", "Lat", "Lon", "Alt", "NomTime" }, (Class)Location.class, (Class)Location.Builder.class);
                PointStreamProto.internal_static_pointStream_PointFeature_descriptor = PointStreamProto.getDescriptor().getMessageTypes().get(1);
                PointStreamProto.internal_static_pointStream_PointFeature_fieldAccessorTable = new GeneratedMessage.FieldAccessorTable(PointStreamProto.internal_static_pointStream_PointFeature_descriptor, new String[] { "Loc", "Data", "Sdata" }, (Class)PointFeature.class, (Class)PointFeature.Builder.class);
                PointStreamProto.internal_static_pointStream_Member_descriptor = PointStreamProto.getDescriptor().getMessageTypes().get(2);
                PointStreamProto.internal_static_pointStream_Member_fieldAccessorTable = new GeneratedMessage.FieldAccessorTable(PointStreamProto.internal_static_pointStream_Member_descriptor, new String[] { "Name", "Desc", "Units", "DataType", "Section" }, (Class)Member.class, (Class)Member.Builder.class);
                PointStreamProto.internal_static_pointStream_PointFeatureCollection_descriptor = PointStreamProto.getDescriptor().getMessageTypes().get(3);
                PointStreamProto.internal_static_pointStream_PointFeatureCollection_fieldAccessorTable = new GeneratedMessage.FieldAccessorTable(PointStreamProto.internal_static_pointStream_PointFeatureCollection_descriptor, new String[] { "Name", "TimeUnit", "Members" }, (Class)PointFeatureCollection.class, (Class)PointFeatureCollection.Builder.class);
                PointStreamProto.internal_static_pointStream_Station_descriptor = PointStreamProto.getDescriptor().getMessageTypes().get(4);
                PointStreamProto.internal_static_pointStream_Station_fieldAccessorTable = new GeneratedMessage.FieldAccessorTable(PointStreamProto.internal_static_pointStream_Station_descriptor, new String[] { "Id", "Lat", "Lon", "Alt", "Desc", "WmoId" }, (Class)Station.class, (Class)Station.Builder.class);
                PointStreamProto.internal_static_pointStream_StationList_descriptor = PointStreamProto.getDescriptor().getMessageTypes().get(5);
                PointStreamProto.internal_static_pointStream_StationList_fieldAccessorTable = new GeneratedMessage.FieldAccessorTable(PointStreamProto.internal_static_pointStream_StationList_descriptor, new String[] { "Stations" }, (Class)StationList.class, (Class)StationList.Builder.class);
                return null;
            }
        };
        Descriptors.FileDescriptor.internalBuildGeneratedFileFrom(descriptorData, new Descriptors.FileDescriptor[] { NcStreamProto.getDescriptor() }, assigner);
    }
    
    public static final class Location extends GeneratedMessage
    {
        private static final Location defaultInstance;
        public static final int TIME_FIELD_NUMBER = 1;
        private boolean hasTime;
        private double time_;
        public static final int LAT_FIELD_NUMBER = 2;
        private boolean hasLat;
        private double lat_;
        public static final int LON_FIELD_NUMBER = 3;
        private boolean hasLon;
        private double lon_;
        public static final int ALT_FIELD_NUMBER = 4;
        private boolean hasAlt;
        private double alt_;
        public static final int NOMTIME_FIELD_NUMBER = 5;
        private boolean hasNomTime;
        private double nomTime_;
        private int memoizedSerializedSize;
        
        private Location() {
            this.time_ = 0.0;
            this.lat_ = 0.0;
            this.lon_ = 0.0;
            this.alt_ = 0.0;
            this.nomTime_ = 0.0;
            this.memoizedSerializedSize = -1;
        }
        
        public static Location getDefaultInstance() {
            return Location.defaultInstance;
        }
        
        public Location getDefaultInstanceForType() {
            return Location.defaultInstance;
        }
        
        public static final Descriptors.Descriptor getDescriptor() {
            return PointStreamProto.internal_static_pointStream_Location_descriptor;
        }
        
        protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return PointStreamProto.internal_static_pointStream_Location_fieldAccessorTable;
        }
        
        public boolean hasTime() {
            return this.hasTime;
        }
        
        public double getTime() {
            return this.time_;
        }
        
        public boolean hasLat() {
            return this.hasLat;
        }
        
        public double getLat() {
            return this.lat_;
        }
        
        public boolean hasLon() {
            return this.hasLon;
        }
        
        public double getLon() {
            return this.lon_;
        }
        
        public boolean hasAlt() {
            return this.hasAlt;
        }
        
        public double getAlt() {
            return this.alt_;
        }
        
        public boolean hasNomTime() {
            return this.hasNomTime;
        }
        
        public double getNomTime() {
            return this.nomTime_;
        }
        
        public final boolean isInitialized() {
            return this.hasTime && this.hasLat && this.hasLon;
        }
        
        public void writeTo(final CodedOutputStream output) throws IOException {
            if (this.hasTime()) {
                output.writeDouble(1, this.getTime());
            }
            if (this.hasLat()) {
                output.writeDouble(2, this.getLat());
            }
            if (this.hasLon()) {
                output.writeDouble(3, this.getLon());
            }
            if (this.hasAlt()) {
                output.writeDouble(4, this.getAlt());
            }
            if (this.hasNomTime()) {
                output.writeDouble(5, this.getNomTime());
            }
            this.getUnknownFields().writeTo(output);
        }
        
        public int getSerializedSize() {
            int size = this.memoizedSerializedSize;
            if (size != -1) {
                return size;
            }
            size = 0;
            if (this.hasTime()) {
                size += CodedOutputStream.computeDoubleSize(1, this.getTime());
            }
            if (this.hasLat()) {
                size += CodedOutputStream.computeDoubleSize(2, this.getLat());
            }
            if (this.hasLon()) {
                size += CodedOutputStream.computeDoubleSize(3, this.getLon());
            }
            if (this.hasAlt()) {
                size += CodedOutputStream.computeDoubleSize(4, this.getAlt());
            }
            if (this.hasNomTime()) {
                size += CodedOutputStream.computeDoubleSize(5, this.getNomTime());
            }
            size += this.getUnknownFields().getSerializedSize();
            return this.memoizedSerializedSize = size;
        }
        
        public static Location parseFrom(final ByteString data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static Location parseFrom(final ByteString data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static Location parseFrom(final byte[] data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static Location parseFrom(final byte[] data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static Location parseFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input)).buildParsed();
        }
        
        public static Location parseFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static Location parseDelimitedFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input)).buildParsed();
        }
        
        public static Location parseDelimitedFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static Location parseFrom(final CodedInputStream input) throws IOException {
            return newBuilder().mergeFrom(input).buildParsed();
        }
        
        public static Location parseFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return newBuilder().mergeFrom(input, extensionRegistry).buildParsed();
        }
        
        public static Builder newBuilder() {
            return new Builder();
        }
        
        public Builder newBuilderForType() {
            return new Builder();
        }
        
        public static Builder newBuilder(final Location prototype) {
            return new Builder().mergeFrom(prototype);
        }
        
        public Builder toBuilder() {
            return newBuilder(this);
        }
        
        static {
            defaultInstance = new Location();
            PointStreamProto.getDescriptor();
        }
        
        public static final class Builder extends GeneratedMessage.Builder<Builder>
        {
            Location result;
            
            private Builder() {
                this.result = new Location();
            }
            
            protected Location internalGetResult() {
                return this.result;
            }
            
            public Builder clear() {
                this.result = new Location();
                return this;
            }
            
            public Builder clone() {
                return new Builder().mergeFrom(this.result);
            }
            
            public Descriptors.Descriptor getDescriptorForType() {
                return Location.getDescriptor();
            }
            
            public Location getDefaultInstanceForType() {
                return Location.getDefaultInstance();
            }
            
            public Location build() {
                if (this.result != null && !this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result);
                }
                return this.buildPartial();
            }
            
            private Location buildParsed() throws InvalidProtocolBufferException {
                if (!this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result).asInvalidProtocolBufferException();
                }
                return this.buildPartial();
            }
            
            public Location buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                final Location returnMe = this.result;
                this.result = null;
                return returnMe;
            }
            
            public Builder mergeFrom(final Message other) {
                if (other instanceof Location) {
                    return this.mergeFrom((Location)other);
                }
                super.mergeFrom(other);
                return this;
            }
            
            public Builder mergeFrom(final Location other) {
                if (other == Location.getDefaultInstance()) {
                    return this;
                }
                if (other.hasTime()) {
                    this.setTime(other.getTime());
                }
                if (other.hasLat()) {
                    this.setLat(other.getLat());
                }
                if (other.hasLon()) {
                    this.setLon(other.getLon());
                }
                if (other.hasAlt()) {
                    this.setAlt(other.getAlt());
                }
                if (other.hasNomTime()) {
                    this.setNomTime(other.getNomTime());
                }
                this.mergeUnknownFields(other.getUnknownFields());
                return this;
            }
            
            public Builder mergeFrom(final CodedInputStream input) throws IOException {
                return this.mergeFrom(input, ExtensionRegistry.getEmptyRegistry());
            }
            
            public Builder mergeFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
                final UnknownFieldSet.Builder unknownFields = UnknownFieldSet.newBuilder(this.getUnknownFields());
                while (true) {
                    final int tag = input.readTag();
                    switch (tag) {
                        case 0: {
                            this.setUnknownFields(unknownFields.build());
                            return this;
                        }
                        default: {
                            if (!this.parseUnknownField(input, unknownFields, extensionRegistry, tag)) {
                                this.setUnknownFields(unknownFields.build());
                                return this;
                            }
                            continue;
                        }
                        case 9: {
                            this.setTime(input.readDouble());
                            continue;
                        }
                        case 17: {
                            this.setLat(input.readDouble());
                            continue;
                        }
                        case 25: {
                            this.setLon(input.readDouble());
                            continue;
                        }
                        case 33: {
                            this.setAlt(input.readDouble());
                            continue;
                        }
                        case 41: {
                            this.setNomTime(input.readDouble());
                            continue;
                        }
                    }
                }
            }
            
            public boolean hasTime() {
                return this.result.hasTime();
            }
            
            public double getTime() {
                return this.result.getTime();
            }
            
            public Builder setTime(final double value) {
                this.result.hasTime = true;
                this.result.time_ = value;
                return this;
            }
            
            public Builder clearTime() {
                this.result.hasTime = false;
                this.result.time_ = 0.0;
                return this;
            }
            
            public boolean hasLat() {
                return this.result.hasLat();
            }
            
            public double getLat() {
                return this.result.getLat();
            }
            
            public Builder setLat(final double value) {
                this.result.hasLat = true;
                this.result.lat_ = value;
                return this;
            }
            
            public Builder clearLat() {
                this.result.hasLat = false;
                this.result.lat_ = 0.0;
                return this;
            }
            
            public boolean hasLon() {
                return this.result.hasLon();
            }
            
            public double getLon() {
                return this.result.getLon();
            }
            
            public Builder setLon(final double value) {
                this.result.hasLon = true;
                this.result.lon_ = value;
                return this;
            }
            
            public Builder clearLon() {
                this.result.hasLon = false;
                this.result.lon_ = 0.0;
                return this;
            }
            
            public boolean hasAlt() {
                return this.result.hasAlt();
            }
            
            public double getAlt() {
                return this.result.getAlt();
            }
            
            public Builder setAlt(final double value) {
                this.result.hasAlt = true;
                this.result.alt_ = value;
                return this;
            }
            
            public Builder clearAlt() {
                this.result.hasAlt = false;
                this.result.alt_ = 0.0;
                return this;
            }
            
            public boolean hasNomTime() {
                return this.result.hasNomTime();
            }
            
            public double getNomTime() {
                return this.result.getNomTime();
            }
            
            public Builder setNomTime(final double value) {
                this.result.hasNomTime = true;
                this.result.nomTime_ = value;
                return this;
            }
            
            public Builder clearNomTime() {
                this.result.hasNomTime = false;
                this.result.nomTime_ = 0.0;
                return this;
            }
        }
    }
    
    public static final class PointFeature extends GeneratedMessage
    {
        private static final PointFeature defaultInstance;
        public static final int LOC_FIELD_NUMBER = 1;
        private boolean hasLoc;
        private Location loc_;
        public static final int DATA_FIELD_NUMBER = 3;
        private boolean hasData;
        private ByteString data_;
        public static final int SDATA_FIELD_NUMBER = 4;
        private List<String> sdata_;
        private int memoizedSerializedSize;
        
        private PointFeature() {
            this.loc_ = Location.getDefaultInstance();
            this.data_ = ByteString.EMPTY;
            this.sdata_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }
        
        public static PointFeature getDefaultInstance() {
            return PointFeature.defaultInstance;
        }
        
        public PointFeature getDefaultInstanceForType() {
            return PointFeature.defaultInstance;
        }
        
        public static final Descriptors.Descriptor getDescriptor() {
            return PointStreamProto.internal_static_pointStream_PointFeature_descriptor;
        }
        
        protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return PointStreamProto.internal_static_pointStream_PointFeature_fieldAccessorTable;
        }
        
        public boolean hasLoc() {
            return this.hasLoc;
        }
        
        public Location getLoc() {
            return this.loc_;
        }
        
        public boolean hasData() {
            return this.hasData;
        }
        
        public ByteString getData() {
            return this.data_;
        }
        
        public List<String> getSdataList() {
            return this.sdata_;
        }
        
        public int getSdataCount() {
            return this.sdata_.size();
        }
        
        public String getSdata(final int index) {
            return this.sdata_.get(index);
        }
        
        public final boolean isInitialized() {
            return this.hasLoc && this.hasData && this.getLoc().isInitialized();
        }
        
        public void writeTo(final CodedOutputStream output) throws IOException {
            if (this.hasLoc()) {
                output.writeMessage(1, (Message)this.getLoc());
            }
            if (this.hasData()) {
                output.writeBytes(3, this.getData());
            }
            for (final String element : this.getSdataList()) {
                output.writeString(4, element);
            }
            this.getUnknownFields().writeTo(output);
        }
        
        public int getSerializedSize() {
            int size = this.memoizedSerializedSize;
            if (size != -1) {
                return size;
            }
            size = 0;
            if (this.hasLoc()) {
                size += CodedOutputStream.computeMessageSize(1, (Message)this.getLoc());
            }
            if (this.hasData()) {
                size += CodedOutputStream.computeBytesSize(3, this.getData());
            }
            int dataSize = 0;
            for (final String element : this.getSdataList()) {
                dataSize += CodedOutputStream.computeStringSizeNoTag(element);
            }
            size += dataSize;
            size += 1 * this.getSdataList().size();
            size += this.getUnknownFields().getSerializedSize();
            return this.memoizedSerializedSize = size;
        }
        
        public static PointFeature parseFrom(final ByteString data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static PointFeature parseFrom(final ByteString data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static PointFeature parseFrom(final byte[] data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static PointFeature parseFrom(final byte[] data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static PointFeature parseFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input)).buildParsed();
        }
        
        public static PointFeature parseFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static PointFeature parseDelimitedFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input)).buildParsed();
        }
        
        public static PointFeature parseDelimitedFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static PointFeature parseFrom(final CodedInputStream input) throws IOException {
            return newBuilder().mergeFrom(input).buildParsed();
        }
        
        public static PointFeature parseFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return newBuilder().mergeFrom(input, extensionRegistry).buildParsed();
        }
        
        public static Builder newBuilder() {
            return new Builder();
        }
        
        public Builder newBuilderForType() {
            return new Builder();
        }
        
        public static Builder newBuilder(final PointFeature prototype) {
            return new Builder().mergeFrom(prototype);
        }
        
        public Builder toBuilder() {
            return newBuilder(this);
        }
        
        static {
            defaultInstance = new PointFeature();
            PointStreamProto.getDescriptor();
        }
        
        public static final class Builder extends GeneratedMessage.Builder<Builder>
        {
            PointFeature result;
            
            private Builder() {
                this.result = new PointFeature();
            }
            
            protected PointFeature internalGetResult() {
                return this.result;
            }
            
            public Builder clear() {
                this.result = new PointFeature();
                return this;
            }
            
            public Builder clone() {
                return new Builder().mergeFrom(this.result);
            }
            
            public Descriptors.Descriptor getDescriptorForType() {
                return PointFeature.getDescriptor();
            }
            
            public PointFeature getDefaultInstanceForType() {
                return PointFeature.getDefaultInstance();
            }
            
            public PointFeature build() {
                if (this.result != null && !this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result);
                }
                return this.buildPartial();
            }
            
            private PointFeature buildParsed() throws InvalidProtocolBufferException {
                if (!this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result).asInvalidProtocolBufferException();
                }
                return this.buildPartial();
            }
            
            public PointFeature buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.sdata_ != Collections.EMPTY_LIST) {
                    this.result.sdata_ = (List<String>)Collections.unmodifiableList((List<?>)this.result.sdata_);
                }
                final PointFeature returnMe = this.result;
                this.result = null;
                return returnMe;
            }
            
            public Builder mergeFrom(final Message other) {
                if (other instanceof PointFeature) {
                    return this.mergeFrom((PointFeature)other);
                }
                super.mergeFrom(other);
                return this;
            }
            
            public Builder mergeFrom(final PointFeature other) {
                if (other == PointFeature.getDefaultInstance()) {
                    return this;
                }
                if (other.hasLoc()) {
                    this.mergeLoc(other.getLoc());
                }
                if (other.hasData()) {
                    this.setData(other.getData());
                }
                if (!other.sdata_.isEmpty()) {
                    if (this.result.sdata_.isEmpty()) {
                        this.result.sdata_ = (List<String>)new ArrayList();
                    }
                    this.result.sdata_.addAll(other.sdata_);
                }
                this.mergeUnknownFields(other.getUnknownFields());
                return this;
            }
            
            public Builder mergeFrom(final CodedInputStream input) throws IOException {
                return this.mergeFrom(input, ExtensionRegistry.getEmptyRegistry());
            }
            
            public Builder mergeFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
                final UnknownFieldSet.Builder unknownFields = UnknownFieldSet.newBuilder(this.getUnknownFields());
                while (true) {
                    final int tag = input.readTag();
                    switch (tag) {
                        case 0: {
                            this.setUnknownFields(unknownFields.build());
                            return this;
                        }
                        default: {
                            if (!this.parseUnknownField(input, unknownFields, extensionRegistry, tag)) {
                                this.setUnknownFields(unknownFields.build());
                                return this;
                            }
                            continue;
                        }
                        case 10: {
                            final Location.Builder subBuilder = Location.newBuilder();
                            if (this.hasLoc()) {
                                subBuilder.mergeFrom(this.getLoc());
                            }
                            input.readMessage((Message.Builder)subBuilder, extensionRegistry);
                            this.setLoc(subBuilder.buildPartial());
                            continue;
                        }
                        case 26: {
                            this.setData(input.readBytes());
                            continue;
                        }
                        case 34: {
                            this.addSdata(input.readString());
                            continue;
                        }
                    }
                }
            }
            
            public boolean hasLoc() {
                return this.result.hasLoc();
            }
            
            public Location getLoc() {
                return this.result.getLoc();
            }
            
            public Builder setLoc(final Location value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasLoc = true;
                this.result.loc_ = value;
                return this;
            }
            
            public Builder setLoc(final Location.Builder builderForValue) {
                this.result.hasLoc = true;
                this.result.loc_ = builderForValue.build();
                return this;
            }
            
            public Builder mergeLoc(final Location value) {
                if (this.result.hasLoc() && this.result.loc_ != Location.getDefaultInstance()) {
                    this.result.loc_ = Location.newBuilder(this.result.loc_).mergeFrom(value).buildPartial();
                }
                else {
                    this.result.loc_ = value;
                }
                this.result.hasLoc = true;
                return this;
            }
            
            public Builder clearLoc() {
                this.result.hasLoc = false;
                this.result.loc_ = Location.getDefaultInstance();
                return this;
            }
            
            public boolean hasData() {
                return this.result.hasData();
            }
            
            public ByteString getData() {
                return this.result.getData();
            }
            
            public Builder setData(final ByteString value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasData = true;
                this.result.data_ = value;
                return this;
            }
            
            public Builder clearData() {
                this.result.hasData = false;
                this.result.data_ = ByteString.EMPTY;
                return this;
            }
            
            public List<String> getSdataList() {
                return Collections.unmodifiableList((List<? extends String>)this.result.sdata_);
            }
            
            public int getSdataCount() {
                return this.result.getSdataCount();
            }
            
            public String getSdata(final int index) {
                return this.result.getSdata(index);
            }
            
            public Builder setSdata(final int index, final String value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.sdata_.set(index, value);
                return this;
            }
            
            public Builder addSdata(final String value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                if (this.result.sdata_.isEmpty()) {
                    this.result.sdata_ = (List<String>)new ArrayList();
                }
                this.result.sdata_.add(value);
                return this;
            }
            
            public Builder addAllSdata(final Iterable<? extends String> values) {
                if (this.result.sdata_.isEmpty()) {
                    this.result.sdata_ = (List<String>)new ArrayList();
                }
                super.addAll((Iterable)values, (Collection)this.result.sdata_);
                return this;
            }
            
            public Builder clearSdata() {
                this.result.sdata_ = (List<String>)Collections.emptyList();
                return this;
            }
        }
    }
    
    public static final class Member extends GeneratedMessage
    {
        private static final Member defaultInstance;
        public static final int NAME_FIELD_NUMBER = 1;
        private boolean hasName;
        private String name_;
        public static final int DESC_FIELD_NUMBER = 2;
        private boolean hasDesc;
        private String desc_;
        public static final int UNITS_FIELD_NUMBER = 3;
        private boolean hasUnits;
        private String units_;
        public static final int DATATYPE_FIELD_NUMBER = 4;
        private boolean hasDataType;
        private NcStreamProto.DataType dataType_;
        public static final int SECTION_FIELD_NUMBER = 5;
        private boolean hasSection;
        private NcStreamProto.Section section_;
        private int memoizedSerializedSize;
        
        private Member() {
            this.name_ = "";
            this.desc_ = "";
            this.units_ = "";
            this.dataType_ = NcStreamProto.DataType.CHAR;
            this.section_ = NcStreamProto.Section.getDefaultInstance();
            this.memoizedSerializedSize = -1;
        }
        
        public static Member getDefaultInstance() {
            return Member.defaultInstance;
        }
        
        public Member getDefaultInstanceForType() {
            return Member.defaultInstance;
        }
        
        public static final Descriptors.Descriptor getDescriptor() {
            return PointStreamProto.internal_static_pointStream_Member_descriptor;
        }
        
        protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return PointStreamProto.internal_static_pointStream_Member_fieldAccessorTable;
        }
        
        public boolean hasName() {
            return this.hasName;
        }
        
        public String getName() {
            return this.name_;
        }
        
        public boolean hasDesc() {
            return this.hasDesc;
        }
        
        public String getDesc() {
            return this.desc_;
        }
        
        public boolean hasUnits() {
            return this.hasUnits;
        }
        
        public String getUnits() {
            return this.units_;
        }
        
        public boolean hasDataType() {
            return this.hasDataType;
        }
        
        public NcStreamProto.DataType getDataType() {
            return this.dataType_;
        }
        
        public boolean hasSection() {
            return this.hasSection;
        }
        
        public NcStreamProto.Section getSection() {
            return this.section_;
        }
        
        public final boolean isInitialized() {
            return this.hasName && this.hasDataType && this.hasSection && this.getSection().isInitialized();
        }
        
        public void writeTo(final CodedOutputStream output) throws IOException {
            if (this.hasName()) {
                output.writeString(1, this.getName());
            }
            if (this.hasDesc()) {
                output.writeString(2, this.getDesc());
            }
            if (this.hasUnits()) {
                output.writeString(3, this.getUnits());
            }
            if (this.hasDataType()) {
                output.writeEnum(4, this.getDataType().getNumber());
            }
            if (this.hasSection()) {
                output.writeMessage(5, (Message)this.getSection());
            }
            this.getUnknownFields().writeTo(output);
        }
        
        public int getSerializedSize() {
            int size = this.memoizedSerializedSize;
            if (size != -1) {
                return size;
            }
            size = 0;
            if (this.hasName()) {
                size += CodedOutputStream.computeStringSize(1, this.getName());
            }
            if (this.hasDesc()) {
                size += CodedOutputStream.computeStringSize(2, this.getDesc());
            }
            if (this.hasUnits()) {
                size += CodedOutputStream.computeStringSize(3, this.getUnits());
            }
            if (this.hasDataType()) {
                size += CodedOutputStream.computeEnumSize(4, this.getDataType().getNumber());
            }
            if (this.hasSection()) {
                size += CodedOutputStream.computeMessageSize(5, (Message)this.getSection());
            }
            size += this.getUnknownFields().getSerializedSize();
            return this.memoizedSerializedSize = size;
        }
        
        public static Member parseFrom(final ByteString data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static Member parseFrom(final ByteString data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static Member parseFrom(final byte[] data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static Member parseFrom(final byte[] data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static Member parseFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input)).buildParsed();
        }
        
        public static Member parseFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static Member parseDelimitedFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input)).buildParsed();
        }
        
        public static Member parseDelimitedFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static Member parseFrom(final CodedInputStream input) throws IOException {
            return newBuilder().mergeFrom(input).buildParsed();
        }
        
        public static Member parseFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return newBuilder().mergeFrom(input, extensionRegistry).buildParsed();
        }
        
        public static Builder newBuilder() {
            return new Builder();
        }
        
        public Builder newBuilderForType() {
            return new Builder();
        }
        
        public static Builder newBuilder(final Member prototype) {
            return new Builder().mergeFrom(prototype);
        }
        
        public Builder toBuilder() {
            return newBuilder(this);
        }
        
        static {
            defaultInstance = new Member();
            PointStreamProto.getDescriptor();
        }
        
        public static final class Builder extends GeneratedMessage.Builder<Builder>
        {
            Member result;
            
            private Builder() {
                this.result = new Member();
            }
            
            protected Member internalGetResult() {
                return this.result;
            }
            
            public Builder clear() {
                this.result = new Member();
                return this;
            }
            
            public Builder clone() {
                return new Builder().mergeFrom(this.result);
            }
            
            public Descriptors.Descriptor getDescriptorForType() {
                return Member.getDescriptor();
            }
            
            public Member getDefaultInstanceForType() {
                return Member.getDefaultInstance();
            }
            
            public Member build() {
                if (this.result != null && !this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result);
                }
                return this.buildPartial();
            }
            
            private Member buildParsed() throws InvalidProtocolBufferException {
                if (!this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result).asInvalidProtocolBufferException();
                }
                return this.buildPartial();
            }
            
            public Member buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                final Member returnMe = this.result;
                this.result = null;
                return returnMe;
            }
            
            public Builder mergeFrom(final Message other) {
                if (other instanceof Member) {
                    return this.mergeFrom((Member)other);
                }
                super.mergeFrom(other);
                return this;
            }
            
            public Builder mergeFrom(final Member other) {
                if (other == Member.getDefaultInstance()) {
                    return this;
                }
                if (other.hasName()) {
                    this.setName(other.getName());
                }
                if (other.hasDesc()) {
                    this.setDesc(other.getDesc());
                }
                if (other.hasUnits()) {
                    this.setUnits(other.getUnits());
                }
                if (other.hasDataType()) {
                    this.setDataType(other.getDataType());
                }
                if (other.hasSection()) {
                    this.mergeSection(other.getSection());
                }
                this.mergeUnknownFields(other.getUnknownFields());
                return this;
            }
            
            public Builder mergeFrom(final CodedInputStream input) throws IOException {
                return this.mergeFrom(input, ExtensionRegistry.getEmptyRegistry());
            }
            
            public Builder mergeFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
                final UnknownFieldSet.Builder unknownFields = UnknownFieldSet.newBuilder(this.getUnknownFields());
                while (true) {
                    final int tag = input.readTag();
                    switch (tag) {
                        case 0: {
                            this.setUnknownFields(unknownFields.build());
                            return this;
                        }
                        default: {
                            if (!this.parseUnknownField(input, unknownFields, extensionRegistry, tag)) {
                                this.setUnknownFields(unknownFields.build());
                                return this;
                            }
                            continue;
                        }
                        case 10: {
                            this.setName(input.readString());
                            continue;
                        }
                        case 18: {
                            this.setDesc(input.readString());
                            continue;
                        }
                        case 26: {
                            this.setUnits(input.readString());
                            continue;
                        }
                        case 32: {
                            final int rawValue = input.readEnum();
                            final NcStreamProto.DataType value = NcStreamProto.DataType.valueOf(rawValue);
                            if (value == null) {
                                unknownFields.mergeVarintField(4, rawValue);
                                continue;
                            }
                            this.setDataType(value);
                            continue;
                        }
                        case 42: {
                            final NcStreamProto.Section.Builder subBuilder = NcStreamProto.Section.newBuilder();
                            if (this.hasSection()) {
                                subBuilder.mergeFrom(this.getSection());
                            }
                            input.readMessage((Message.Builder)subBuilder, extensionRegistry);
                            this.setSection(subBuilder.buildPartial());
                            continue;
                        }
                    }
                }
            }
            
            public boolean hasName() {
                return this.result.hasName();
            }
            
            public String getName() {
                return this.result.getName();
            }
            
            public Builder setName(final String value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasName = true;
                this.result.name_ = value;
                return this;
            }
            
            public Builder clearName() {
                this.result.hasName = false;
                this.result.name_ = "";
                return this;
            }
            
            public boolean hasDesc() {
                return this.result.hasDesc();
            }
            
            public String getDesc() {
                return this.result.getDesc();
            }
            
            public Builder setDesc(final String value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasDesc = true;
                this.result.desc_ = value;
                return this;
            }
            
            public Builder clearDesc() {
                this.result.hasDesc = false;
                this.result.desc_ = "";
                return this;
            }
            
            public boolean hasUnits() {
                return this.result.hasUnits();
            }
            
            public String getUnits() {
                return this.result.getUnits();
            }
            
            public Builder setUnits(final String value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasUnits = true;
                this.result.units_ = value;
                return this;
            }
            
            public Builder clearUnits() {
                this.result.hasUnits = false;
                this.result.units_ = "";
                return this;
            }
            
            public boolean hasDataType() {
                return this.result.hasDataType();
            }
            
            public NcStreamProto.DataType getDataType() {
                return this.result.getDataType();
            }
            
            public Builder setDataType(final NcStreamProto.DataType value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasDataType = true;
                this.result.dataType_ = value;
                return this;
            }
            
            public Builder clearDataType() {
                this.result.hasDataType = false;
                this.result.dataType_ = NcStreamProto.DataType.CHAR;
                return this;
            }
            
            public boolean hasSection() {
                return this.result.hasSection();
            }
            
            public NcStreamProto.Section getSection() {
                return this.result.getSection();
            }
            
            public Builder setSection(final NcStreamProto.Section value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasSection = true;
                this.result.section_ = value;
                return this;
            }
            
            public Builder setSection(final NcStreamProto.Section.Builder builderForValue) {
                this.result.hasSection = true;
                this.result.section_ = builderForValue.build();
                return this;
            }
            
            public Builder mergeSection(final NcStreamProto.Section value) {
                if (this.result.hasSection() && this.result.section_ != NcStreamProto.Section.getDefaultInstance()) {
                    this.result.section_ = NcStreamProto.Section.newBuilder(this.result.section_).mergeFrom(value).buildPartial();
                }
                else {
                    this.result.section_ = value;
                }
                this.result.hasSection = true;
                return this;
            }
            
            public Builder clearSection() {
                this.result.hasSection = false;
                this.result.section_ = NcStreamProto.Section.getDefaultInstance();
                return this;
            }
        }
    }
    
    public static final class PointFeatureCollection extends GeneratedMessage
    {
        private static final PointFeatureCollection defaultInstance;
        public static final int NAME_FIELD_NUMBER = 1;
        private boolean hasName;
        private String name_;
        public static final int TIMEUNIT_FIELD_NUMBER = 2;
        private boolean hasTimeUnit;
        private String timeUnit_;
        public static final int MEMBERS_FIELD_NUMBER = 3;
        private List<Member> members_;
        private int memoizedSerializedSize;
        
        private PointFeatureCollection() {
            this.name_ = "";
            this.timeUnit_ = "";
            this.members_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }
        
        public static PointFeatureCollection getDefaultInstance() {
            return PointFeatureCollection.defaultInstance;
        }
        
        public PointFeatureCollection getDefaultInstanceForType() {
            return PointFeatureCollection.defaultInstance;
        }
        
        public static final Descriptors.Descriptor getDescriptor() {
            return PointStreamProto.internal_static_pointStream_PointFeatureCollection_descriptor;
        }
        
        protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return PointStreamProto.internal_static_pointStream_PointFeatureCollection_fieldAccessorTable;
        }
        
        public boolean hasName() {
            return this.hasName;
        }
        
        public String getName() {
            return this.name_;
        }
        
        public boolean hasTimeUnit() {
            return this.hasTimeUnit;
        }
        
        public String getTimeUnit() {
            return this.timeUnit_;
        }
        
        public List<Member> getMembersList() {
            return this.members_;
        }
        
        public int getMembersCount() {
            return this.members_.size();
        }
        
        public Member getMembers(final int index) {
            return this.members_.get(index);
        }
        
        public final boolean isInitialized() {
            if (!this.hasName) {
                return false;
            }
            if (!this.hasTimeUnit) {
                return false;
            }
            for (final Member element : this.getMembersList()) {
                if (!element.isInitialized()) {
                    return false;
                }
            }
            return true;
        }
        
        public void writeTo(final CodedOutputStream output) throws IOException {
            if (this.hasName()) {
                output.writeString(1, this.getName());
            }
            if (this.hasTimeUnit()) {
                output.writeString(2, this.getTimeUnit());
            }
            for (final Member element : this.getMembersList()) {
                output.writeMessage(3, (Message)element);
            }
            this.getUnknownFields().writeTo(output);
        }
        
        public int getSerializedSize() {
            int size = this.memoizedSerializedSize;
            if (size != -1) {
                return size;
            }
            size = 0;
            if (this.hasName()) {
                size += CodedOutputStream.computeStringSize(1, this.getName());
            }
            if (this.hasTimeUnit()) {
                size += CodedOutputStream.computeStringSize(2, this.getTimeUnit());
            }
            for (final Member element : this.getMembersList()) {
                size += CodedOutputStream.computeMessageSize(3, (Message)element);
            }
            size += this.getUnknownFields().getSerializedSize();
            return this.memoizedSerializedSize = size;
        }
        
        public static PointFeatureCollection parseFrom(final ByteString data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static PointFeatureCollection parseFrom(final ByteString data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static PointFeatureCollection parseFrom(final byte[] data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static PointFeatureCollection parseFrom(final byte[] data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static PointFeatureCollection parseFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input)).buildParsed();
        }
        
        public static PointFeatureCollection parseFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static PointFeatureCollection parseDelimitedFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input)).buildParsed();
        }
        
        public static PointFeatureCollection parseDelimitedFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static PointFeatureCollection parseFrom(final CodedInputStream input) throws IOException {
            return newBuilder().mergeFrom(input).buildParsed();
        }
        
        public static PointFeatureCollection parseFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return newBuilder().mergeFrom(input, extensionRegistry).buildParsed();
        }
        
        public static Builder newBuilder() {
            return new Builder();
        }
        
        public Builder newBuilderForType() {
            return new Builder();
        }
        
        public static Builder newBuilder(final PointFeatureCollection prototype) {
            return new Builder().mergeFrom(prototype);
        }
        
        public Builder toBuilder() {
            return newBuilder(this);
        }
        
        static {
            defaultInstance = new PointFeatureCollection();
            PointStreamProto.getDescriptor();
        }
        
        public static final class Builder extends GeneratedMessage.Builder<Builder>
        {
            PointFeatureCollection result;
            
            private Builder() {
                this.result = new PointFeatureCollection();
            }
            
            protected PointFeatureCollection internalGetResult() {
                return this.result;
            }
            
            public Builder clear() {
                this.result = new PointFeatureCollection();
                return this;
            }
            
            public Builder clone() {
                return new Builder().mergeFrom(this.result);
            }
            
            public Descriptors.Descriptor getDescriptorForType() {
                return PointFeatureCollection.getDescriptor();
            }
            
            public PointFeatureCollection getDefaultInstanceForType() {
                return PointFeatureCollection.getDefaultInstance();
            }
            
            public PointFeatureCollection build() {
                if (this.result != null && !this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result);
                }
                return this.buildPartial();
            }
            
            private PointFeatureCollection buildParsed() throws InvalidProtocolBufferException {
                if (!this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result).asInvalidProtocolBufferException();
                }
                return this.buildPartial();
            }
            
            public PointFeatureCollection buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.members_ != Collections.EMPTY_LIST) {
                    this.result.members_ = (List<Member>)Collections.unmodifiableList((List<?>)this.result.members_);
                }
                final PointFeatureCollection returnMe = this.result;
                this.result = null;
                return returnMe;
            }
            
            public Builder mergeFrom(final Message other) {
                if (other instanceof PointFeatureCollection) {
                    return this.mergeFrom((PointFeatureCollection)other);
                }
                super.mergeFrom(other);
                return this;
            }
            
            public Builder mergeFrom(final PointFeatureCollection other) {
                if (other == PointFeatureCollection.getDefaultInstance()) {
                    return this;
                }
                if (other.hasName()) {
                    this.setName(other.getName());
                }
                if (other.hasTimeUnit()) {
                    this.setTimeUnit(other.getTimeUnit());
                }
                if (!other.members_.isEmpty()) {
                    if (this.result.members_.isEmpty()) {
                        this.result.members_ = (List<Member>)new ArrayList();
                    }
                    this.result.members_.addAll(other.members_);
                }
                this.mergeUnknownFields(other.getUnknownFields());
                return this;
            }
            
            public Builder mergeFrom(final CodedInputStream input) throws IOException {
                return this.mergeFrom(input, ExtensionRegistry.getEmptyRegistry());
            }
            
            public Builder mergeFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
                final UnknownFieldSet.Builder unknownFields = UnknownFieldSet.newBuilder(this.getUnknownFields());
                while (true) {
                    final int tag = input.readTag();
                    switch (tag) {
                        case 0: {
                            this.setUnknownFields(unknownFields.build());
                            return this;
                        }
                        default: {
                            if (!this.parseUnknownField(input, unknownFields, extensionRegistry, tag)) {
                                this.setUnknownFields(unknownFields.build());
                                return this;
                            }
                            continue;
                        }
                        case 10: {
                            this.setName(input.readString());
                            continue;
                        }
                        case 18: {
                            this.setTimeUnit(input.readString());
                            continue;
                        }
                        case 26: {
                            final Member.Builder subBuilder = Member.newBuilder();
                            input.readMessage((Message.Builder)subBuilder, extensionRegistry);
                            this.addMembers(subBuilder.buildPartial());
                            continue;
                        }
                    }
                }
            }
            
            public boolean hasName() {
                return this.result.hasName();
            }
            
            public String getName() {
                return this.result.getName();
            }
            
            public Builder setName(final String value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasName = true;
                this.result.name_ = value;
                return this;
            }
            
            public Builder clearName() {
                this.result.hasName = false;
                this.result.name_ = "";
                return this;
            }
            
            public boolean hasTimeUnit() {
                return this.result.hasTimeUnit();
            }
            
            public String getTimeUnit() {
                return this.result.getTimeUnit();
            }
            
            public Builder setTimeUnit(final String value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasTimeUnit = true;
                this.result.timeUnit_ = value;
                return this;
            }
            
            public Builder clearTimeUnit() {
                this.result.hasTimeUnit = false;
                this.result.timeUnit_ = "";
                return this;
            }
            
            public List<Member> getMembersList() {
                return Collections.unmodifiableList((List<? extends Member>)this.result.members_);
            }
            
            public int getMembersCount() {
                return this.result.getMembersCount();
            }
            
            public Member getMembers(final int index) {
                return this.result.getMembers(index);
            }
            
            public Builder setMembers(final int index, final Member value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.members_.set(index, value);
                return this;
            }
            
            public Builder setMembers(final int index, final Member.Builder builderForValue) {
                this.result.members_.set(index, builderForValue.build());
                return this;
            }
            
            public Builder addMembers(final Member value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                if (this.result.members_.isEmpty()) {
                    this.result.members_ = (List<Member>)new ArrayList();
                }
                this.result.members_.add(value);
                return this;
            }
            
            public Builder addMembers(final Member.Builder builderForValue) {
                if (this.result.members_.isEmpty()) {
                    this.result.members_ = (List<Member>)new ArrayList();
                }
                this.result.members_.add(builderForValue.build());
                return this;
            }
            
            public Builder addAllMembers(final Iterable<? extends Member> values) {
                if (this.result.members_.isEmpty()) {
                    this.result.members_ = (List<Member>)new ArrayList();
                }
                super.addAll((Iterable)values, (Collection)this.result.members_);
                return this;
            }
            
            public Builder clearMembers() {
                this.result.members_ = (List<Member>)Collections.emptyList();
                return this;
            }
        }
    }
    
    public static final class Station extends GeneratedMessage
    {
        private static final Station defaultInstance;
        public static final int ID_FIELD_NUMBER = 1;
        private boolean hasId;
        private String id_;
        public static final int LAT_FIELD_NUMBER = 2;
        private boolean hasLat;
        private double lat_;
        public static final int LON_FIELD_NUMBER = 3;
        private boolean hasLon;
        private double lon_;
        public static final int ALT_FIELD_NUMBER = 4;
        private boolean hasAlt;
        private double alt_;
        public static final int DESC_FIELD_NUMBER = 5;
        private boolean hasDesc;
        private String desc_;
        public static final int WMOID_FIELD_NUMBER = 6;
        private boolean hasWmoId;
        private String wmoId_;
        private int memoizedSerializedSize;
        
        private Station() {
            this.id_ = "";
            this.lat_ = 0.0;
            this.lon_ = 0.0;
            this.alt_ = 0.0;
            this.desc_ = "";
            this.wmoId_ = "";
            this.memoizedSerializedSize = -1;
        }
        
        public static Station getDefaultInstance() {
            return Station.defaultInstance;
        }
        
        public Station getDefaultInstanceForType() {
            return Station.defaultInstance;
        }
        
        public static final Descriptors.Descriptor getDescriptor() {
            return PointStreamProto.internal_static_pointStream_Station_descriptor;
        }
        
        protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return PointStreamProto.internal_static_pointStream_Station_fieldAccessorTable;
        }
        
        public boolean hasId() {
            return this.hasId;
        }
        
        public String getId() {
            return this.id_;
        }
        
        public boolean hasLat() {
            return this.hasLat;
        }
        
        public double getLat() {
            return this.lat_;
        }
        
        public boolean hasLon() {
            return this.hasLon;
        }
        
        public double getLon() {
            return this.lon_;
        }
        
        public boolean hasAlt() {
            return this.hasAlt;
        }
        
        public double getAlt() {
            return this.alt_;
        }
        
        public boolean hasDesc() {
            return this.hasDesc;
        }
        
        public String getDesc() {
            return this.desc_;
        }
        
        public boolean hasWmoId() {
            return this.hasWmoId;
        }
        
        public String getWmoId() {
            return this.wmoId_;
        }
        
        public final boolean isInitialized() {
            return this.hasId && this.hasLat && this.hasLon;
        }
        
        public void writeTo(final CodedOutputStream output) throws IOException {
            if (this.hasId()) {
                output.writeString(1, this.getId());
            }
            if (this.hasLat()) {
                output.writeDouble(2, this.getLat());
            }
            if (this.hasLon()) {
                output.writeDouble(3, this.getLon());
            }
            if (this.hasAlt()) {
                output.writeDouble(4, this.getAlt());
            }
            if (this.hasDesc()) {
                output.writeString(5, this.getDesc());
            }
            if (this.hasWmoId()) {
                output.writeString(6, this.getWmoId());
            }
            this.getUnknownFields().writeTo(output);
        }
        
        public int getSerializedSize() {
            int size = this.memoizedSerializedSize;
            if (size != -1) {
                return size;
            }
            size = 0;
            if (this.hasId()) {
                size += CodedOutputStream.computeStringSize(1, this.getId());
            }
            if (this.hasLat()) {
                size += CodedOutputStream.computeDoubleSize(2, this.getLat());
            }
            if (this.hasLon()) {
                size += CodedOutputStream.computeDoubleSize(3, this.getLon());
            }
            if (this.hasAlt()) {
                size += CodedOutputStream.computeDoubleSize(4, this.getAlt());
            }
            if (this.hasDesc()) {
                size += CodedOutputStream.computeStringSize(5, this.getDesc());
            }
            if (this.hasWmoId()) {
                size += CodedOutputStream.computeStringSize(6, this.getWmoId());
            }
            size += this.getUnknownFields().getSerializedSize();
            return this.memoizedSerializedSize = size;
        }
        
        public static Station parseFrom(final ByteString data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static Station parseFrom(final ByteString data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static Station parseFrom(final byte[] data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static Station parseFrom(final byte[] data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static Station parseFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input)).buildParsed();
        }
        
        public static Station parseFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static Station parseDelimitedFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input)).buildParsed();
        }
        
        public static Station parseDelimitedFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static Station parseFrom(final CodedInputStream input) throws IOException {
            return newBuilder().mergeFrom(input).buildParsed();
        }
        
        public static Station parseFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return newBuilder().mergeFrom(input, extensionRegistry).buildParsed();
        }
        
        public static Builder newBuilder() {
            return new Builder();
        }
        
        public Builder newBuilderForType() {
            return new Builder();
        }
        
        public static Builder newBuilder(final Station prototype) {
            return new Builder().mergeFrom(prototype);
        }
        
        public Builder toBuilder() {
            return newBuilder(this);
        }
        
        static {
            defaultInstance = new Station();
            PointStreamProto.getDescriptor();
        }
        
        public static final class Builder extends GeneratedMessage.Builder<Builder>
        {
            Station result;
            
            private Builder() {
                this.result = new Station();
            }
            
            protected Station internalGetResult() {
                return this.result;
            }
            
            public Builder clear() {
                this.result = new Station();
                return this;
            }
            
            public Builder clone() {
                return new Builder().mergeFrom(this.result);
            }
            
            public Descriptors.Descriptor getDescriptorForType() {
                return Station.getDescriptor();
            }
            
            public Station getDefaultInstanceForType() {
                return Station.getDefaultInstance();
            }
            
            public Station build() {
                if (this.result != null && !this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result);
                }
                return this.buildPartial();
            }
            
            private Station buildParsed() throws InvalidProtocolBufferException {
                if (!this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result).asInvalidProtocolBufferException();
                }
                return this.buildPartial();
            }
            
            public Station buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                final Station returnMe = this.result;
                this.result = null;
                return returnMe;
            }
            
            public Builder mergeFrom(final Message other) {
                if (other instanceof Station) {
                    return this.mergeFrom((Station)other);
                }
                super.mergeFrom(other);
                return this;
            }
            
            public Builder mergeFrom(final Station other) {
                if (other == Station.getDefaultInstance()) {
                    return this;
                }
                if (other.hasId()) {
                    this.setId(other.getId());
                }
                if (other.hasLat()) {
                    this.setLat(other.getLat());
                }
                if (other.hasLon()) {
                    this.setLon(other.getLon());
                }
                if (other.hasAlt()) {
                    this.setAlt(other.getAlt());
                }
                if (other.hasDesc()) {
                    this.setDesc(other.getDesc());
                }
                if (other.hasWmoId()) {
                    this.setWmoId(other.getWmoId());
                }
                this.mergeUnknownFields(other.getUnknownFields());
                return this;
            }
            
            public Builder mergeFrom(final CodedInputStream input) throws IOException {
                return this.mergeFrom(input, ExtensionRegistry.getEmptyRegistry());
            }
            
            public Builder mergeFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
                final UnknownFieldSet.Builder unknownFields = UnknownFieldSet.newBuilder(this.getUnknownFields());
                while (true) {
                    final int tag = input.readTag();
                    switch (tag) {
                        case 0: {
                            this.setUnknownFields(unknownFields.build());
                            return this;
                        }
                        default: {
                            if (!this.parseUnknownField(input, unknownFields, extensionRegistry, tag)) {
                                this.setUnknownFields(unknownFields.build());
                                return this;
                            }
                            continue;
                        }
                        case 10: {
                            this.setId(input.readString());
                            continue;
                        }
                        case 17: {
                            this.setLat(input.readDouble());
                            continue;
                        }
                        case 25: {
                            this.setLon(input.readDouble());
                            continue;
                        }
                        case 33: {
                            this.setAlt(input.readDouble());
                            continue;
                        }
                        case 42: {
                            this.setDesc(input.readString());
                            continue;
                        }
                        case 50: {
                            this.setWmoId(input.readString());
                            continue;
                        }
                    }
                }
            }
            
            public boolean hasId() {
                return this.result.hasId();
            }
            
            public String getId() {
                return this.result.getId();
            }
            
            public Builder setId(final String value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasId = true;
                this.result.id_ = value;
                return this;
            }
            
            public Builder clearId() {
                this.result.hasId = false;
                this.result.id_ = "";
                return this;
            }
            
            public boolean hasLat() {
                return this.result.hasLat();
            }
            
            public double getLat() {
                return this.result.getLat();
            }
            
            public Builder setLat(final double value) {
                this.result.hasLat = true;
                this.result.lat_ = value;
                return this;
            }
            
            public Builder clearLat() {
                this.result.hasLat = false;
                this.result.lat_ = 0.0;
                return this;
            }
            
            public boolean hasLon() {
                return this.result.hasLon();
            }
            
            public double getLon() {
                return this.result.getLon();
            }
            
            public Builder setLon(final double value) {
                this.result.hasLon = true;
                this.result.lon_ = value;
                return this;
            }
            
            public Builder clearLon() {
                this.result.hasLon = false;
                this.result.lon_ = 0.0;
                return this;
            }
            
            public boolean hasAlt() {
                return this.result.hasAlt();
            }
            
            public double getAlt() {
                return this.result.getAlt();
            }
            
            public Builder setAlt(final double value) {
                this.result.hasAlt = true;
                this.result.alt_ = value;
                return this;
            }
            
            public Builder clearAlt() {
                this.result.hasAlt = false;
                this.result.alt_ = 0.0;
                return this;
            }
            
            public boolean hasDesc() {
                return this.result.hasDesc();
            }
            
            public String getDesc() {
                return this.result.getDesc();
            }
            
            public Builder setDesc(final String value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasDesc = true;
                this.result.desc_ = value;
                return this;
            }
            
            public Builder clearDesc() {
                this.result.hasDesc = false;
                this.result.desc_ = "";
                return this;
            }
            
            public boolean hasWmoId() {
                return this.result.hasWmoId();
            }
            
            public String getWmoId() {
                return this.result.getWmoId();
            }
            
            public Builder setWmoId(final String value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasWmoId = true;
                this.result.wmoId_ = value;
                return this;
            }
            
            public Builder clearWmoId() {
                this.result.hasWmoId = false;
                this.result.wmoId_ = "";
                return this;
            }
        }
    }
    
    public static final class StationList extends GeneratedMessage
    {
        private static final StationList defaultInstance;
        public static final int STATIONS_FIELD_NUMBER = 1;
        private List<Station> stations_;
        private int memoizedSerializedSize;
        
        private StationList() {
            this.stations_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }
        
        public static StationList getDefaultInstance() {
            return StationList.defaultInstance;
        }
        
        public StationList getDefaultInstanceForType() {
            return StationList.defaultInstance;
        }
        
        public static final Descriptors.Descriptor getDescriptor() {
            return PointStreamProto.internal_static_pointStream_StationList_descriptor;
        }
        
        protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return PointStreamProto.internal_static_pointStream_StationList_fieldAccessorTable;
        }
        
        public List<Station> getStationsList() {
            return this.stations_;
        }
        
        public int getStationsCount() {
            return this.stations_.size();
        }
        
        public Station getStations(final int index) {
            return this.stations_.get(index);
        }
        
        public final boolean isInitialized() {
            for (final Station element : this.getStationsList()) {
                if (!element.isInitialized()) {
                    return false;
                }
            }
            return true;
        }
        
        public void writeTo(final CodedOutputStream output) throws IOException {
            for (final Station element : this.getStationsList()) {
                output.writeMessage(1, (Message)element);
            }
            this.getUnknownFields().writeTo(output);
        }
        
        public int getSerializedSize() {
            int size = this.memoizedSerializedSize;
            if (size != -1) {
                return size;
            }
            size = 0;
            for (final Station element : this.getStationsList()) {
                size += CodedOutputStream.computeMessageSize(1, (Message)element);
            }
            size += this.getUnknownFields().getSerializedSize();
            return this.memoizedSerializedSize = size;
        }
        
        public static StationList parseFrom(final ByteString data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static StationList parseFrom(final ByteString data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static StationList parseFrom(final byte[] data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static StationList parseFrom(final byte[] data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static StationList parseFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input)).buildParsed();
        }
        
        public static StationList parseFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static StationList parseDelimitedFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input)).buildParsed();
        }
        
        public static StationList parseDelimitedFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static StationList parseFrom(final CodedInputStream input) throws IOException {
            return newBuilder().mergeFrom(input).buildParsed();
        }
        
        public static StationList parseFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return newBuilder().mergeFrom(input, extensionRegistry).buildParsed();
        }
        
        public static Builder newBuilder() {
            return new Builder();
        }
        
        public Builder newBuilderForType() {
            return new Builder();
        }
        
        public static Builder newBuilder(final StationList prototype) {
            return new Builder().mergeFrom(prototype);
        }
        
        public Builder toBuilder() {
            return newBuilder(this);
        }
        
        static {
            defaultInstance = new StationList();
            PointStreamProto.getDescriptor();
        }
        
        public static final class Builder extends GeneratedMessage.Builder<Builder>
        {
            StationList result;
            
            private Builder() {
                this.result = new StationList();
            }
            
            protected StationList internalGetResult() {
                return this.result;
            }
            
            public Builder clear() {
                this.result = new StationList();
                return this;
            }
            
            public Builder clone() {
                return new Builder().mergeFrom(this.result);
            }
            
            public Descriptors.Descriptor getDescriptorForType() {
                return StationList.getDescriptor();
            }
            
            public StationList getDefaultInstanceForType() {
                return StationList.getDefaultInstance();
            }
            
            public StationList build() {
                if (this.result != null && !this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result);
                }
                return this.buildPartial();
            }
            
            private StationList buildParsed() throws InvalidProtocolBufferException {
                if (!this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result).asInvalidProtocolBufferException();
                }
                return this.buildPartial();
            }
            
            public StationList buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.stations_ != Collections.EMPTY_LIST) {
                    this.result.stations_ = (List<Station>)Collections.unmodifiableList((List<?>)this.result.stations_);
                }
                final StationList returnMe = this.result;
                this.result = null;
                return returnMe;
            }
            
            public Builder mergeFrom(final Message other) {
                if (other instanceof StationList) {
                    return this.mergeFrom((StationList)other);
                }
                super.mergeFrom(other);
                return this;
            }
            
            public Builder mergeFrom(final StationList other) {
                if (other == StationList.getDefaultInstance()) {
                    return this;
                }
                if (!other.stations_.isEmpty()) {
                    if (this.result.stations_.isEmpty()) {
                        this.result.stations_ = (List<Station>)new ArrayList();
                    }
                    this.result.stations_.addAll(other.stations_);
                }
                this.mergeUnknownFields(other.getUnknownFields());
                return this;
            }
            
            public Builder mergeFrom(final CodedInputStream input) throws IOException {
                return this.mergeFrom(input, ExtensionRegistry.getEmptyRegistry());
            }
            
            public Builder mergeFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
                final UnknownFieldSet.Builder unknownFields = UnknownFieldSet.newBuilder(this.getUnknownFields());
                while (true) {
                    final int tag = input.readTag();
                    switch (tag) {
                        case 0: {
                            this.setUnknownFields(unknownFields.build());
                            return this;
                        }
                        default: {
                            if (!this.parseUnknownField(input, unknownFields, extensionRegistry, tag)) {
                                this.setUnknownFields(unknownFields.build());
                                return this;
                            }
                            continue;
                        }
                        case 10: {
                            final Station.Builder subBuilder = Station.newBuilder();
                            input.readMessage((Message.Builder)subBuilder, extensionRegistry);
                            this.addStations(subBuilder.buildPartial());
                            continue;
                        }
                    }
                }
            }
            
            public List<Station> getStationsList() {
                return Collections.unmodifiableList((List<? extends Station>)this.result.stations_);
            }
            
            public int getStationsCount() {
                return this.result.getStationsCount();
            }
            
            public Station getStations(final int index) {
                return this.result.getStations(index);
            }
            
            public Builder setStations(final int index, final Station value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.stations_.set(index, value);
                return this;
            }
            
            public Builder setStations(final int index, final Station.Builder builderForValue) {
                this.result.stations_.set(index, builderForValue.build());
                return this;
            }
            
            public Builder addStations(final Station value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                if (this.result.stations_.isEmpty()) {
                    this.result.stations_ = (List<Station>)new ArrayList();
                }
                this.result.stations_.add(value);
                return this;
            }
            
            public Builder addStations(final Station.Builder builderForValue) {
                if (this.result.stations_.isEmpty()) {
                    this.result.stations_ = (List<Station>)new ArrayList();
                }
                this.result.stations_.add(builderForValue.build());
                return this;
            }
            
            public Builder addAllStations(final Iterable<? extends Station> values) {
                if (this.result.stations_.isEmpty()) {
                    this.result.stations_ = (List<Station>)new ArrayList();
                }
                super.addAll((Iterable)values, (Collection)this.result.stations_);
                return this;
            }
            
            public Builder clearStations() {
                this.result.stations_ = (List<Station>)Collections.emptyList();
                return this;
            }
        }
    }
}
