// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.remote;

import ucar.nc2.ft.PointFeatureIterator;
import ucar.nc2.ft.StationTimeSeriesFeature;
import ucar.nc2.units.DateUnit;
import ucar.nc2.ft.point.StationFeatureImpl;
import ucar.nc2.ft.NestedPointFeatureCollection;
import ucar.nc2.ft.PointFeatureCollection;
import ucar.nc2.ft.StationTimeSeriesFeatureCollection;
import java.util.List;
import ucar.nc2.ft.point.StationPointFeature;
import ucar.nc2.ft.PointFeature;
import java.util.Iterator;
import java.io.InputStream;
import org.apache.commons.httpclient.HttpMethod;
import java.io.IOException;
import ucar.unidata.geoloc.Station;
import ucar.unidata.geoloc.StationImpl;
import ucar.nc2.stream.NcStream;
import ucar.nc2.stream.CdmRemote;
import ucar.nc2.ft.point.StationHelper;
import ucar.nc2.units.DateRange;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.ft.point.StationTimeSeriesCollectionImpl;

public class RemoteStationCollection extends StationTimeSeriesCollectionImpl
{
    private String uri;
    protected LatLonRect boundingBoxSubset;
    protected DateRange dateRangeSubset;
    private boolean restrictedList;
    
    public RemoteStationCollection(final String uri) {
        super(uri);
        this.restrictedList = false;
        this.uri = uri;
    }
    
    @Override
    protected void initStationHelper() {
        this.stationHelper = new StationHelper();
        HttpMethod method = null;
        try {
            final String query = "req=stations";
            method = CdmRemote.sendQuery(this.uri, query);
            final InputStream in = method.getResponseBodyAsStream();
            final PointStream.MessageType mtype = PointStream.readMagic(in);
            if (mtype != PointStream.MessageType.StationList) {
                throw new RuntimeException("Station Request: bad response");
            }
            final int len = NcStream.readVInt(in);
            final byte[] b = new byte[len];
            NcStream.readFully(in, b);
            final PointStreamProto.StationList stationsp = PointStreamProto.StationList.parseFrom(b);
            for (final PointStreamProto.Station sp : stationsp.getStationsList()) {
                final Station s = new StationImpl(sp.getId(), sp.getDesc(), sp.getWmoId(), sp.getLat(), sp.getLon(), sp.getAlt());
                this.stationHelper.addStation(new RemoteStationFeatureImpl(s, null));
            }
        }
        catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
        finally {
            if (method != null) {
                method.releaseConnection();
            }
        }
    }
    
    protected RemoteStationCollection(final String uri, final StationHelper sh) {
        super(uri);
        this.restrictedList = false;
        this.uri = uri;
        this.stationHelper = sh;
        this.restrictedList = (sh != null);
    }
    
    @Override
    public Station getStation(final PointFeature feature) throws IOException {
        final StationPointFeature stationFeature = (StationPointFeature)feature;
        return stationFeature.getStation();
    }
    
    @Override
    public StationTimeSeriesFeatureCollection subset(final List<Station> stations) throws IOException {
        if (stations == null) {
            return this;
        }
        final StationHelper sh = new StationHelper();
        sh.setStations(stations);
        return new RemoteStationCollectionSubset(this, sh, null, null);
    }
    
    @Override
    public StationTimeSeriesFeatureCollection subset(final LatLonRect boundingBox) throws IOException {
        if (boundingBox == null) {
            return this;
        }
        return new RemoteStationCollectionSubset(this, null, boundingBox, null);
    }
    
    @Override
    public PointFeatureCollection flatten(final LatLonRect boundingBox, final DateRange dateRange) throws IOException {
        final QueryMaker queryMaker = this.restrictedList ? new QueryByStationList() : null;
        final RemotePointCollection pfc = new RemotePointCollection(this.uri, queryMaker);
        return pfc.subset(boundingBox, dateRange);
    }
    
    private class QueryByStationList implements QueryMaker
    {
        public String makeQuery() {
            final StringBuilder query = new StringBuilder("stns=");
            for (final Station s : RemoteStationCollection.this.stationHelper.getStations()) {
                query.append(s.getName());
                query.append(",");
            }
            return PointDatasetRemote.makeQuery(query.toString(), RemoteStationCollection.this.boundingBoxSubset, RemoteStationCollection.this.dateRangeSubset);
        }
    }
    
    private class RemoteStationCollectionSubset extends RemoteStationCollection
    {
        RemoteStationCollection from;
        
        RemoteStationCollectionSubset(final RemoteStationCollection from, final StationHelper sh, final LatLonRect filter_bb, final DateRange filter_date) throws IOException {
            super(from.uri, sh);
            this.from = from;
            if (filter_bb == null) {
                this.boundingBoxSubset = from.getBoundingBox();
            }
            else {
                this.boundingBoxSubset = ((from.getBoundingBox() == null) ? filter_bb : from.getBoundingBox().intersect(filter_bb));
            }
            if (filter_date == null) {
                this.dateRangeSubset = from.dateRangeSubset;
            }
            else {
                this.dateRangeSubset = ((from.dateRangeSubset == null) ? filter_date : from.dateRangeSubset.intersect(filter_date));
            }
        }
        
        @Override
        protected void initStationHelper() {
            this.from.initStationHelper();
            this.stationHelper = new StationHelper();
            try {
                this.stationHelper.setStations(this.stationHelper.getStations(this.boundingBoxSubset));
            }
            catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        
        @Override
        public Station getStation(final PointFeature feature) throws IOException {
            return this.from.getStation(feature);
        }
    }
    
    private class RemoteStationFeatureImpl extends StationFeatureImpl
    {
        RemotePointFeatureIterator riter;
        DateRange dateRange;
        
        RemoteStationFeatureImpl(final Station s, final DateRange dateRange) {
            super(s, null, -1);
            this.dateRange = dateRange;
        }
        
        @Override
        public StationTimeSeriesFeature subset(final DateRange dateRange) throws IOException {
            if (dateRange == null) {
                return this;
            }
            return new RemoteStationFeatureImpl(this.s, dateRange);
        }
        
        @Override
        public PointFeatureCollection subset(final LatLonRect boundingBox, final DateRange dateRange) throws IOException {
            if (boundingBox != null) {
                if (!boundingBox.contains(this.s.getLatLon())) {
                    return null;
                }
                if (dateRange == null) {
                    return this;
                }
            }
            return this.subset(dateRange);
        }
        
        public PointFeatureIterator getPointFeatureIterator(final int bufferSize) throws IOException {
            final String query = PointDatasetRemote.makeQuery("stn=" + this.s.getName(), null, this.dateRange);
            HttpMethod method = null;
            try {
                method = CdmRemote.sendQuery(RemoteStationCollection.this.uri, query);
                final InputStream in = method.getResponseBodyAsStream();
                final PointStream.MessageType mtype = PointStream.readMagic(in);
                if (mtype != PointStream.MessageType.PointFeatureCollection) {
                    throw new RuntimeException("Station Request: bad response");
                }
                final int len = NcStream.readVInt(in);
                final byte[] b = new byte[len];
                NcStream.readFully(in, b);
                final PointStreamProto.PointFeatureCollection pfc = PointStreamProto.PointFeatureCollection.parseFrom(b);
                (this.riter = new RemotePointFeatureIterator(method, in, new PointStream.ProtobufPointFeatureMaker(pfc))).setCalculateBounds(this);
                return this.riter;
            }
            catch (Throwable t) {
                if (method != null) {
                    method.releaseConnection();
                }
                throw new IOException(t.getMessage());
            }
        }
    }
}
