// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.remote;

import ucar.nc2.units.DateRange;
import ucar.unidata.geoloc.LatLonRect;
import java.io.InputStream;
import org.apache.commons.httpclient.HttpMethod;
import java.io.IOException;
import ucar.nc2.stream.NcStreamProto;
import ucar.nc2.ft.point.PointIteratorEmpty;
import ucar.nc2.ft.PointFeatureCollection;
import ucar.nc2.stream.NcStream;
import ucar.nc2.stream.CdmRemote;
import ucar.nc2.ft.PointFeatureIterator;
import ucar.nc2.ft.point.PointCollectionImpl;

class RemotePointCollection extends PointCollectionImpl implements QueryMaker
{
    private String uri;
    private QueryMaker queryMaker;
    
    RemotePointCollection(final String uri, final QueryMaker queryMaker) {
        super(uri);
        this.uri = uri;
        this.queryMaker = ((queryMaker == null) ? this : queryMaker);
    }
    
    public String makeQuery() {
        return PointDatasetRemote.makeQuery(null, this.boundingBox, this.dateRange);
    }
    
    public PointFeatureIterator getPointFeatureIterator(final int bufferSize) throws IOException {
        HttpMethod method = null;
        String errMessage = null;
        try {
            method = CdmRemote.sendQuery(this.uri, this.queryMaker.makeQuery());
            final InputStream in = method.getResponseBodyAsStream();
            final PointStream.MessageType mtype = PointStream.readMagic(in);
            if (mtype == PointStream.MessageType.PointFeatureCollection) {
                final int len = NcStream.readVInt(in);
                final byte[] b = new byte[len];
                NcStream.readFully(in, b);
                final PointStreamProto.PointFeatureCollection pfc = PointStreamProto.PointFeatureCollection.parseFrom(b);
                final PointFeatureIterator iter = new RemotePointFeatureIterator(method, in, new PointStream.ProtobufPointFeatureMaker(pfc));
                iter.setCalculateBounds(this);
                return iter;
            }
            if (mtype == PointStream.MessageType.End) {
                return new PointIteratorEmpty();
            }
            if (mtype == PointStream.MessageType.Error) {
                final int len = NcStream.readVInt(in);
                final byte[] b = new byte[len];
                NcStream.readFully(in, b);
                final NcStreamProto.Error proto = NcStreamProto.Error.parseFrom(b);
                errMessage = NcStream.decodeErrorMessage(proto);
            }
            else {
                errMessage = "Illegal pointstream message type= " + mtype;
            }
        }
        catch (Throwable t) {
            if (method != null) {
                method.releaseConnection();
            }
            throw new RuntimeException(t);
        }
        if (errMessage != null) {
            throw new IOException(errMessage);
        }
        return null;
    }
    
    @Override
    public PointFeatureCollection subset(final LatLonRect boundingBox, final DateRange dateRange) throws IOException {
        return new PointFeatureCollectionSubset(this, boundingBox, dateRange);
    }
    
    private class PointFeatureCollectionSubset extends RemotePointCollection
    {
        PointCollectionImpl from;
        
        PointFeatureCollectionSubset(final RemotePointCollection from, final LatLonRect filter_bb, final DateRange filter_date) throws IOException {
            super(from.uri, null);
            this.from = from;
            if (filter_bb == null) {
                this.boundingBox = from.getBoundingBox();
            }
            else {
                this.boundingBox = ((from.getBoundingBox() == null) ? filter_bb : from.getBoundingBox().intersect(filter_bb));
            }
            if (filter_date == null) {
                this.dateRange = from.getDateRange();
            }
            else {
                this.dateRange = ((from.getDateRange() == null) ? filter_date : from.getDateRange().intersect(filter_date));
            }
        }
    }
}
