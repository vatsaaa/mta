// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.remote;

import com.google.protobuf.InvalidProtocolBufferException;
import ucar.nc2.ft.PointFeature;

public interface FeatureMaker
{
    PointFeature make(final byte[] p0) throws InvalidProtocolBufferException;
}
