// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.remote;

import ucar.nc2.ft.point.PointFeatureImpl;
import com.google.protobuf.InvalidProtocolBufferException;
import ucar.unidata.geoloc.EarthLocationImpl;
import ucar.ma2.ArrayStructureBB;
import ucar.nc2.units.DateUnit;
import ucar.unidata.geoloc.Station;
import java.util.List;
import ucar.unidata.geoloc.EarthLocation;
import com.google.protobuf.ByteString;
import ucar.ma2.DataType;
import java.nio.ByteBuffer;
import java.util.Iterator;
import ucar.ma2.StructureData;
import ucar.ma2.Section;
import ucar.ma2.StructureMembers;
import ucar.nc2.ft.PointFeature;
import java.io.OutputStream;
import java.io.IOException;
import ucar.nc2.stream.NcStream;
import java.io.InputStream;

public class PointStream
{
    private static final byte[] MAGIC_StationList;
    private static final byte[] MAGIC_PointFeatureCollection;
    private static final byte[] MAGIC_PointFeature;
    
    public static MessageType readMagic(final InputStream is) throws IOException {
        final byte[] b = new byte[4];
        NcStream.readFully(is, b);
        if (test(b, PointStream.MAGIC_PointFeature)) {
            return MessageType.PointFeature;
        }
        if (test(b, PointStream.MAGIC_PointFeatureCollection)) {
            return MessageType.PointFeatureCollection;
        }
        if (test(b, PointStream.MAGIC_StationList)) {
            return MessageType.StationList;
        }
        if (test(b, NcStream.MAGIC_END)) {
            return MessageType.End;
        }
        if (test(b, NcStream.MAGIC_ERR)) {
            return MessageType.Error;
        }
        return null;
    }
    
    public static int writeMagic(final OutputStream out, final MessageType type) throws IOException {
        switch (type) {
            case PointFeature: {
                return NcStream.writeBytes(out, PointStream.MAGIC_PointFeature);
            }
            case PointFeatureCollection: {
                return NcStream.writeBytes(out, PointStream.MAGIC_PointFeatureCollection);
            }
            case StationList: {
                return NcStream.writeBytes(out, PointStream.MAGIC_StationList);
            }
            case End: {
                return NcStream.writeBytes(out, NcStream.MAGIC_END);
            }
            case Error: {
                return NcStream.writeBytes(out, NcStream.MAGIC_ERR);
            }
            default: {
                return 0;
            }
        }
    }
    
    private static boolean test(final byte[] b, final byte[] m) {
        if (b.length != m.length) {
            return false;
        }
        for (int i = 0; i < b.length; ++i) {
            if (b[i] != m[i]) {
                return false;
            }
        }
        return true;
    }
    
    public static PointStreamProto.PointFeatureCollection encodePointFeatureCollection(final String name, final PointFeature pf) throws IOException {
        final PointStreamProto.PointFeatureCollection.Builder builder = PointStreamProto.PointFeatureCollection.newBuilder();
        if (name == null) {
            System.out.printf("HEY null pointstream name%n", new Object[0]);
        }
        builder.setName(name);
        builder.setTimeUnit(pf.getTimeUnit().getUnitsString());
        final StructureData sdata = pf.getData();
        final StructureMembers sm = sdata.getStructureMembers();
        for (final StructureMembers.Member m : sm.getMembers()) {
            final PointStreamProto.Member.Builder mbuilder = PointStreamProto.Member.newBuilder();
            mbuilder.setName(m.getName());
            if (null != m.getDescription()) {
                mbuilder.setDesc(m.getDescription());
            }
            if (null != m.getUnitsString()) {
                mbuilder.setUnits(m.getUnitsString());
            }
            mbuilder.setDataType(NcStream.encodeDataType(m.getDataType()));
            mbuilder.setSection(NcStream.encodeSection(new Section(m.getShape())));
            builder.addMembers(mbuilder);
        }
        return builder.build();
    }
    
    public static PointStreamProto.PointFeature encodePointFeature(final PointFeature pf) throws IOException {
        final PointStreamProto.Location.Builder locBuilder = PointStreamProto.Location.newBuilder();
        locBuilder.setTime(pf.getObservationTime());
        if (!Double.isNaN(pf.getNominalTime()) && pf.getNominalTime() != pf.getObservationTime()) {
            locBuilder.setNomTime(pf.getNominalTime());
        }
        final EarthLocation loc = pf.getLocation();
        locBuilder.setLat(loc.getLatitude());
        locBuilder.setLon(loc.getLongitude());
        if (!Double.isNaN(loc.getAltitude())) {
            locBuilder.setAlt(loc.getAltitude());
        }
        final PointStreamProto.PointFeature.Builder builder = PointStreamProto.PointFeature.newBuilder();
        builder.setLoc(locBuilder);
        final StructureData sdata = pf.getData();
        final StructureMembers sm = sdata.getStructureMembers();
        final int size = sm.getStructureSize();
        final ByteBuffer bb = ByteBuffer.allocate(size);
        int stringno = 0;
        for (final StructureMembers.Member m : sm.getMembers()) {
            if (m.getDataType() == DataType.STRING) {
                builder.addSdata(sdata.getScalarString(m));
                bb.putInt(stringno++);
            }
            else if (m.getDataType() == DataType.DOUBLE) {
                bb.putDouble(sdata.getScalarDouble(m));
            }
            else if (m.getDataType() == DataType.FLOAT) {
                bb.putFloat(sdata.getScalarFloat(m));
            }
            else if (m.getDataType() == DataType.INT) {
                bb.putInt(sdata.getScalarInt(m));
            }
            else if (m.getDataType() == DataType.SHORT) {
                bb.putShort(sdata.getScalarShort(m));
            }
            else if (m.getDataType() == DataType.BYTE) {
                bb.put(sdata.getScalarByte(m));
            }
            else if (m.getDataType() == DataType.CHAR) {
                for (final char c : sdata.getJavaArrayChar(m)) {
                    bb.put((byte)c);
                }
            }
            else {
                System.out.println(" unimplemented type = " + m.getDataType());
            }
        }
        builder.setData(ByteString.copyFrom(bb.array()));
        return builder.build();
    }
    
    public static PointStreamProto.StationList encodeStations(final List<Station> stnList) throws IOException {
        final PointStreamProto.StationList.Builder stnBuilder = PointStreamProto.StationList.newBuilder();
        for (final Station loc : stnList) {
            final PointStreamProto.Station.Builder locBuilder = PointStreamProto.Station.newBuilder();
            locBuilder.setId(loc.getName());
            locBuilder.setLat(loc.getLatitude());
            locBuilder.setLon(loc.getLongitude());
            if (!Double.isNaN(loc.getAltitude())) {
                locBuilder.setAlt(loc.getAltitude());
            }
            if (loc.getDescription() != null) {
                locBuilder.setDesc(loc.getDescription());
            }
            if (loc.getWmoId() != null) {
                locBuilder.setWmoId(loc.getWmoId());
            }
            stnBuilder.addStations(locBuilder);
        }
        return stnBuilder.build();
    }
    
    static {
        MAGIC_StationList = new byte[] { -2, -2, -17, -17 };
        MAGIC_PointFeatureCollection = new byte[] { -6, -6, -81, -81 };
        MAGIC_PointFeature = new byte[] { -16, -16, 15, 15 };
    }
    
    public enum MessageType
    {
        StationList, 
        PointFeatureCollection, 
        PointFeature, 
        End, 
        Error;
    }
    
    static class ProtobufPointFeatureMaker implements FeatureMaker
    {
        private DateUnit dateUnit;
        private StructureMembers sm;
        
        ProtobufPointFeatureMaker(final PointStreamProto.PointFeatureCollection pfc) throws IOException {
            try {
                this.dateUnit = new DateUnit(pfc.getTimeUnit());
            }
            catch (Exception e) {
                e.printStackTrace();
                this.dateUnit = DateUnit.getUnixDateUnit();
            }
            this.sm = new StructureMembers(pfc.getName());
            for (final PointStreamProto.Member m : pfc.getMembersList()) {
                this.sm.addMember(m.getName(), m.getDesc(), m.getUnits(), NcStream.decodeDataType(m.getDataType()), NcStream.decodeSection(m.getSection()).getShape());
            }
            ArrayStructureBB.setOffsets(this.sm);
        }
        
        public PointFeature make(final byte[] rawBytes) throws InvalidProtocolBufferException {
            final PointStreamProto.PointFeature pfp = PointStreamProto.PointFeature.parseFrom(rawBytes);
            final PointStreamProto.Location locp = pfp.getLoc();
            final EarthLocationImpl location = new EarthLocationImpl(locp.getLat(), locp.getLon(), locp.getAlt());
            return new MyPointFeature(location, locp.getTime(), locp.getNomTime(), this.dateUnit, pfp);
        }
        
        private class MyPointFeature extends PointFeatureImpl
        {
            PointStreamProto.PointFeature pfp;
            
            MyPointFeature(final EarthLocation location, final double obsTime, final double nomTime, final DateUnit timeUnit, final PointStreamProto.PointFeature pfp) {
                super(location, obsTime, nomTime, timeUnit);
                this.pfp = pfp;
            }
            
            public StructureData getData() throws IOException {
                final ByteBuffer bb = ByteBuffer.wrap(this.pfp.getData().toByteArray());
                final ArrayStructureBB asbb = new ArrayStructureBB(ProtobufPointFeatureMaker.this.sm, new int[] { 1 }, bb, 0);
                for (final String s : this.pfp.getSdataList()) {
                    asbb.addObjectToHeap(s);
                }
                return asbb.getStructureData(0);
            }
            
            @Override
            public String toString() {
                return this.location + " obs=" + this.obsTime + " nom=" + this.nomTime;
            }
        }
    }
}
