// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point;

import ucar.nc2.ft.PointFeature;
import java.io.IOException;

public class PointIteratorEmpty extends PointIteratorAbstract
{
    public boolean hasNext() throws IOException {
        return false;
    }
    
    public PointFeature next() throws IOException {
        return null;
    }
    
    public void finish() {
    }
    
    public void setBufferSize(final int bytes) {
    }
}
