// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point;

import ucar.nc2.constants.FeatureType;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.nc2.ft.ProfileFeature;

public abstract class ProfileFeatureImpl extends PointCollectionImpl implements ProfileFeature
{
    private LatLonPoint latlonPoint;
    protected double time;
    
    public ProfileFeatureImpl(final String name, final double lat, final double lon, final double time, final int npts) {
        super(name);
        this.latlonPoint = new LatLonPointImpl(lat, lon);
        this.time = time;
        this.npts = npts;
    }
    
    public LatLonPoint getLatLon() {
        return this.latlonPoint;
    }
    
    public Object getId() {
        return this.getName();
    }
    
    @Override
    public FeatureType getCollectionFeatureType() {
        return FeatureType.PROFILE;
    }
}
