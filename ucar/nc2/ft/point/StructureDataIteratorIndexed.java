// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point;

import org.slf4j.LoggerFactory;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.StructureData;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import ucar.nc2.Structure;
import org.slf4j.Logger;
import ucar.ma2.StructureDataIterator;

public class StructureDataIteratorIndexed implements StructureDataIterator
{
    private static Logger log;
    private Structure s;
    private List<Integer> index;
    private Iterator<Integer> indexIter;
    private int currRecord;
    
    public StructureDataIteratorIndexed(final Structure s, final List<Integer> index) throws IOException {
        this.s = s;
        this.index = index;
        this.reset();
    }
    
    public StructureData next() throws IOException {
        this.currRecord = this.indexIter.next();
        StructureData sdata;
        try {
            sdata = this.s.readStructure(this.currRecord);
        }
        catch (InvalidRangeException e) {
            StructureDataIteratorIndexed.log.error("StructureDataIteratorIndexed.nextStructureData recno=" + this.currRecord, e);
            throw new IOException(e.getMessage());
        }
        return sdata;
    }
    
    public boolean hasNext() throws IOException {
        return this.indexIter.hasNext();
    }
    
    public StructureDataIterator reset() {
        this.indexIter = this.index.iterator();
        return this;
    }
    
    public void setBufferSize(final int bytes) {
    }
    
    public int getCurrentRecno() {
        return this.currRecord;
    }
    
    static {
        StructureDataIteratorIndexed.log = LoggerFactory.getLogger(StructureDataIteratorLinked.class);
    }
}
