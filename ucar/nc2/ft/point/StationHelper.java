// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point;

import java.io.IOException;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonPointImpl;
import java.util.Iterator;
import java.util.HashMap;
import java.util.ArrayList;
import ucar.unidata.geoloc.LatLonRect;
import java.util.Map;
import ucar.unidata.geoloc.Station;
import java.util.List;

public class StationHelper
{
    private List<Station> stations;
    private Map<String, Station> stationHash;
    private boolean debug;
    private LatLonRect rect;
    
    public StationHelper() {
        this.debug = false;
        this.stations = new ArrayList<Station>();
        this.stationHash = new HashMap<String, Station>();
    }
    
    public void addStation(final Station s) {
        this.stations.add(s);
        this.stationHash.put(s.getName(), s);
    }
    
    public void setStations(final List<Station> nstations) {
        this.stations = new ArrayList<Station>();
        this.stationHash = new HashMap<String, Station>();
        for (final Station s : nstations) {
            this.addStation(s);
        }
    }
    
    public LatLonRect getBoundingBox() {
        if (this.rect == null) {
            if (this.stations.size() == 0) {
                return null;
            }
            Station s = this.stations.get(0);
            final LatLonPointImpl llpt = new LatLonPointImpl();
            llpt.set(s.getLatitude(), s.getLongitude());
            this.rect = new LatLonRect(llpt, 0.001, 0.001);
            if (this.debug) {
                System.out.println("start=" + s.getLatitude() + " " + s.getLongitude() + " rect= " + this.rect.toString2());
            }
            for (int i = 1; i < this.stations.size(); ++i) {
                s = this.stations.get(i);
                llpt.set(s.getLatitude(), s.getLongitude());
                this.rect.extend(llpt);
                if (this.debug) {
                    System.out.println("add=" + s.getLatitude() + " " + s.getLongitude() + " rect= " + this.rect.toString2());
                }
            }
        }
        if (this.rect.crossDateline() && this.rect.getWidth() > 350.0) {
            final double lat_min = this.rect.getLowerLeftPoint().getLatitude();
            final double deltaLat = this.rect.getUpperLeftPoint().getLatitude() - lat_min;
            this.rect = new LatLonRect(new LatLonPointImpl(lat_min, -180.0), deltaLat, 360.0);
        }
        return this.rect;
    }
    
    public List<Station> getStations(final LatLonRect boundingBox) throws IOException {
        if (boundingBox == null) {
            return this.stations;
        }
        final LatLonPointImpl latlonPt = new LatLonPointImpl();
        final List<Station> result = new ArrayList<Station>();
        for (final Station s : this.stations) {
            latlonPt.set(s.getLatitude(), s.getLongitude());
            if (boundingBox.contains(latlonPt)) {
                result.add(s);
            }
        }
        return result;
    }
    
    public Station getStation(final String name) {
        return this.stationHash.get(name);
    }
    
    public List<Station> getStations() {
        return this.stations;
    }
    
    public List<Station> getStations(final List<String> stnNames) {
        final List<Station> result = new ArrayList<Station>(stnNames.size());
        for (final String ss : stnNames) {
            final Station s = this.stationHash.get(ss);
            if (s != null) {
                result.add(s);
            }
        }
        return result;
    }
}
