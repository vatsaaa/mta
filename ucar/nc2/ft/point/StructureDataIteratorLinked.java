// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point;

import org.slf4j.LoggerFactory;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.StructureData;
import java.io.IOException;
import ucar.nc2.Structure;
import org.slf4j.Logger;
import ucar.ma2.StructureDataIterator;

public class StructureDataIteratorLinked implements StructureDataIterator
{
    private static Logger log;
    private Structure s;
    private int firstRecord;
    private int nextRecno;
    private int numRecords;
    private String linkVarName;
    private int currRecno;
    
    public StructureDataIteratorLinked(final Structure s, final int firstRecord, final int numRecords, final String linkVarName) throws IOException {
        this.s = s;
        this.firstRecord = firstRecord;
        this.nextRecno = firstRecord;
        this.numRecords = numRecords;
        this.linkVarName = linkVarName;
    }
    
    public StructureData next() throws IOException {
        this.currRecno = this.nextRecno;
        StructureData sdata;
        try {
            sdata = this.s.readStructure(this.currRecno);
        }
        catch (InvalidRangeException e) {
            StructureDataIteratorLinked.log.error("StructureDataLinkedIterator.nextStructureData recno=" + this.currRecno, e);
            throw new IOException(e.getMessage());
        }
        if (this.numRecords > 0) {
            ++this.nextRecno;
            if (this.nextRecno >= this.firstRecord + this.numRecords) {
                this.nextRecno = -1;
            }
        }
        else {
            this.nextRecno = sdata.getScalarInt(this.linkVarName);
        }
        return sdata;
    }
    
    public boolean hasNext() throws IOException {
        return this.nextRecno >= 0;
    }
    
    public StructureDataIterator reset() {
        this.nextRecno = this.firstRecord;
        return this;
    }
    
    public void setBufferSize(final int bytes) {
    }
    
    public int getCurrentRecno() {
        return this.currRecno;
    }
    
    static {
        StructureDataIteratorLinked.log = LoggerFactory.getLogger(StructureDataIteratorLinked.class);
    }
}
