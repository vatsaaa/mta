// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point;

import ucar.ma2.InvalidRangeException;
import java.io.IOException;
import ucar.unidata.geoloc.EarthLocationImpl;
import ucar.unidata.geoloc.EarthLocation;
import org.slf4j.LoggerFactory;
import ucar.unidata.geoloc.Station;
import ucar.nc2.ft.PointFeature;
import ucar.unidata.geoloc.StationImpl;
import java.text.ParseException;
import java.util.Date;
import ucar.ma2.StructureMembers;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.ma2.StructureData;
import ucar.nc2.Structure;
import ucar.nc2.units.SimpleUnit;
import java.util.Iterator;
import ucar.nc2.Variable;
import ucar.nc2.Group;
import ucar.nc2.dataset.StructurePseudoDS;
import ucar.nc2.VariableSimpleIF;
import java.util.List;
import ucar.nc2.units.DateFormatter;
import java.util.Formatter;
import ucar.nc2.units.DateUnit;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.Dimension;
import ucar.nc2.dataset.StructureDS;
import ucar.ma2.DataType;
import ucar.nc2.dataset.NetcdfDataset;
import org.slf4j.Logger;

public class RecordDatasetHelper
{
    private static Logger log;
    protected NetcdfDataset ncfile;
    protected String obsTimeVName;
    protected String nomTimeVName;
    protected String latVName;
    protected String lonVName;
    protected String zcoordVName;
    protected String zcoordUnits;
    protected String stnIdVName;
    protected String stnIndexVName;
    protected String stnDescVName;
    protected StationHelper stationHelper;
    protected DataType stationIdType;
    protected StructureDS recordVar;
    protected Dimension obsDim;
    protected LatLonRect boundingBox;
    protected double minDate;
    protected double maxDate;
    protected DateUnit timeUnit;
    protected double altScaleFactor;
    protected Formatter errs;
    protected boolean showErrors;
    private DateFormatter formatter;
    
    public RecordDatasetHelper(final NetcdfDataset ncfile, final String obsTimeVName, final String nomTimeVName, final List<VariableSimpleIF> typedDataVariables, final String recDimName, final Formatter errBuffer) {
        this.altScaleFactor = 1.0;
        this.errs = null;
        this.showErrors = true;
        this.ncfile = ncfile;
        this.obsTimeVName = obsTimeVName;
        this.nomTimeVName = nomTimeVName;
        this.errs = errBuffer;
        if (this.ncfile.hasUnlimitedDimension()) {
            this.ncfile.sendIospMessage("AddRecordStructure");
            this.recordVar = (StructureDS)this.ncfile.getRootGroup().findVariable("record");
            this.obsDim = ncfile.getUnlimitedDimension();
        }
        else {
            if (recDimName == null) {
                throw new IllegalArgumentException("File <" + this.ncfile.getLocation() + "> has no unlimited dimension, specify psuedo record dimension with observationDimension global attribute.");
            }
            this.obsDim = this.ncfile.getRootGroup().findDimension(recDimName);
            this.recordVar = new StructurePseudoDS(this.ncfile, null, "record", null, this.obsDim);
        }
        final List<Variable> recordMembers = ncfile.getVariables();
        for (final Variable v : recordMembers) {
            if (v == this.recordVar) {
                continue;
            }
            if (v.isScalar()) {
                continue;
            }
            if (v.getDimension(0) != this.obsDim) {
                continue;
            }
            typedDataVariables.add(v);
        }
        final Variable timeVar = ncfile.findVariable(obsTimeVName);
        final String timeUnitString = ncfile.findAttValueIgnoreCase(timeVar, "units", "seconds since 1970-01-01");
        try {
            this.timeUnit = new DateUnit(timeUnitString);
        }
        catch (Exception e) {
            if (null != this.errs) {
                this.errs.format("Error on string = %s == %s\n", timeUnitString, e.getMessage());
            }
            try {
                this.timeUnit = new DateUnit("seconds since 1970-01-01");
            }
            catch (Exception ex) {}
        }
    }
    
    public void setStationInfo(final String stnIdVName, final String stnDescVName, final String stnIndexVName, final StationHelper stationHelper) {
        this.stnIdVName = stnIdVName;
        this.stnDescVName = stnDescVName;
        this.stnIndexVName = stnIndexVName;
        this.stationHelper = stationHelper;
        if (stnIdVName != null) {
            final Variable stationVar = this.ncfile.findVariable(stnIdVName);
            this.stationIdType = stationVar.getDataType();
        }
    }
    
    public void setLocationInfo(final String latVName, final String lonVName, final String zcoordVName) {
        this.latVName = latVName;
        this.lonVName = lonVName;
        this.zcoordVName = zcoordVName;
        if (zcoordVName != null) {
            final Variable v = this.ncfile.findVariable(zcoordVName);
            this.zcoordUnits = this.ncfile.findAttValueIgnoreCase(v, "units", null);
            if (this.zcoordUnits != null) {
                try {
                    this.altScaleFactor = getMetersConversionFactor(this.zcoordUnits);
                }
                catch (Exception e) {
                    if (this.errs != null) {
                        this.errs.format("%s", e.getMessage());
                    }
                }
            }
        }
    }
    
    public void setShortNames(final String latVName, final String lonVName, final String altVName, final String obsTimeVName, final String nomTimeVName) {
        this.latVName = latVName;
        this.lonVName = lonVName;
        this.zcoordVName = altVName;
        this.obsTimeVName = obsTimeVName;
        this.nomTimeVName = nomTimeVName;
    }
    
    protected static double getMetersConversionFactor(final String unitsString) throws Exception {
        final SimpleUnit unit = SimpleUnit.factoryWithExceptions(unitsString);
        return unit.convertTo(1.0, SimpleUnit.meterUnit);
    }
    
    public Structure getRecordVar() {
        return this.recordVar;
    }
    
    public int getRecordCount() {
        final Dimension unlimitedDim = this.ncfile.getUnlimitedDimension();
        return unlimitedDim.getLength();
    }
    
    public void setTimeUnit(final DateUnit timeUnit) {
        this.timeUnit = timeUnit;
    }
    
    public DateUnit getTimeUnit() {
        return this.timeUnit;
    }
    
    public LatLonPoint getLocation(final StructureData sdata) {
        final StructureMembers members = sdata.getStructureMembers();
        final double lat = sdata.convertScalarDouble(members.findMember(this.latVName));
        final double lon = sdata.convertScalarDouble(members.findMember(this.lonVName));
        return new LatLonPointImpl(lat, lon);
    }
    
    public double getLatitude(final StructureData sdata) {
        final StructureMembers members = sdata.getStructureMembers();
        return sdata.convertScalarDouble(members.findMember(this.latVName));
    }
    
    public double getLongitude(final StructureData sdata) {
        final StructureMembers members = sdata.getStructureMembers();
        return sdata.convertScalarDouble(members.findMember(this.lonVName));
    }
    
    public double getZcoordinate(final StructureData sdata) {
        final StructureMembers members = sdata.getStructureMembers();
        return (this.zcoordVName == null) ? Double.NaN : sdata.convertScalarDouble(members.findMember(this.zcoordVName));
    }
    
    public String getZcoordUnits() {
        return this.zcoordUnits;
    }
    
    public Date getObservationTimeAsDate(final StructureData sdata) {
        return this.timeUnit.makeDate(this.getObservationTime(sdata));
    }
    
    public double getObservationTime(final StructureData sdata) {
        return this.getTime(sdata.findMember(this.obsTimeVName), sdata);
    }
    
    private double getTime(final StructureMembers.Member timeVar, final StructureData sdata) {
        if (timeVar == null) {
            return 0.0;
        }
        if (timeVar.getDataType() == DataType.CHAR || timeVar.getDataType() == DataType.STRING) {
            final String time = sdata.getScalarString(timeVar);
            if (null == this.formatter) {
                this.formatter = new DateFormatter();
            }
            Date date;
            try {
                date = this.formatter.isoDateTimeFormat(time);
            }
            catch (ParseException e) {
                RecordDatasetHelper.log.error("Cant parse date - not ISO formatted, = " + time);
                return 0.0;
            }
            return date.getTime() / 1000.0;
        }
        return sdata.convertScalarDouble(timeVar);
    }
    
    public PointFeature factory(final StationImpl s, final StructureData sdata, final int recno) {
        if (s == null) {
            return new RecordPointObs(sdata, recno);
        }
        return new RecordStationObs(s, sdata, recno);
    }
    
    static {
        RecordDatasetHelper.log = LoggerFactory.getLogger(RecordDatasetHelper.class);
    }
    
    class RecordPointObs extends PointFeatureImpl
    {
        protected int recno;
        protected StructureData sdata;
        
        public RecordPointObs(final int recno) {
            super(RecordDatasetHelper.this.timeUnit);
            this.recno = recno;
        }
        
        protected RecordPointObs(final EarthLocation location, final double obsTime, final double nomTime, final DateUnit timeUnit, final int recno) {
            super(location, obsTime, nomTime, timeUnit);
            this.recno = recno;
        }
        
        public RecordPointObs(final StructureData sdata, final int recno) {
            super(RecordDatasetHelper.this.timeUnit);
            this.sdata = sdata;
            this.recno = recno;
            final StructureMembers members = sdata.getStructureMembers();
            this.obsTime = RecordDatasetHelper.this.getTime(members.findMember(RecordDatasetHelper.this.obsTimeVName), sdata);
            this.nomTime = ((RecordDatasetHelper.this.nomTimeVName == null) ? this.obsTime : RecordDatasetHelper.this.getTime(members.findMember(RecordDatasetHelper.this.nomTimeVName), sdata));
            final double lat = sdata.convertScalarDouble(members.findMember(RecordDatasetHelper.this.latVName));
            final double lon = sdata.convertScalarDouble(members.findMember(RecordDatasetHelper.this.lonVName));
            final double alt = (RecordDatasetHelper.this.zcoordVName == null) ? 0.0 : (RecordDatasetHelper.this.altScaleFactor * sdata.convertScalarDouble(members.findMember(RecordDatasetHelper.this.zcoordVName)));
            this.location = new EarthLocationImpl(lat, lon, alt);
        }
        
        public String getId() {
            return Integer.toString(this.recno);
        }
        
        public LatLonPoint getLatLon() {
            return new LatLonPointImpl(this.location.getLatitude(), this.location.getLongitude());
        }
        
        public StructureData getData() throws IOException {
            if (null == this.sdata) {
                try {
                    if (this.recno > RecordDatasetHelper.this.getRecordCount()) {
                        final int n = RecordDatasetHelper.this.getRecordCount();
                        RecordDatasetHelper.this.ncfile.syncExtend();
                        RecordDatasetHelper.log.info("RecordPointObs.getData recno=" + this.recno + " > " + n + "; after sync= " + RecordDatasetHelper.this.getRecordCount());
                    }
                    this.sdata = RecordDatasetHelper.this.recordVar.readStructure(this.recno);
                }
                catch (InvalidRangeException e) {
                    e.printStackTrace();
                    throw new IOException(e.getMessage());
                }
            }
            return this.sdata;
        }
    }
    
    class RecordStationObs extends RecordPointObs
    {
        private Station station;
        
        protected RecordStationObs(final Station station, final double obsTime, final double nomTime, final DateUnit timeUnit, final int recno) {
            super(station, obsTime, nomTime, timeUnit, recno);
            this.station = station;
        }
        
        protected RecordStationObs(final Station station, final double obsTime, final double nomTime, final StructureData sdata, final int recno) {
            super(recno);
            this.station = station;
            this.location = station;
            this.obsTime = obsTime;
            this.nomTime = nomTime;
            this.sdata = sdata;
        }
        
        protected RecordStationObs(final Station station, final StructureData sdata, final int recno) {
            super(recno);
            this.station = station;
            this.location = station;
            this.sdata = sdata;
            final StructureMembers members = sdata.getStructureMembers();
            this.obsTime = RecordDatasetHelper.this.getTime(members.findMember(RecordDatasetHelper.this.obsTimeVName), sdata);
            this.nomTime = ((RecordDatasetHelper.this.nomTimeVName == null) ? this.obsTime : RecordDatasetHelper.this.getTime(members.findMember(RecordDatasetHelper.this.nomTimeVName), sdata));
        }
        
        protected RecordStationObs(final StructureData sdata, final int recno, final boolean useId) {
            super(recno);
            this.recno = recno;
            this.sdata = sdata;
            this.timeUnit = RecordDatasetHelper.this.timeUnit;
            final StructureMembers members = sdata.getStructureMembers();
            this.obsTime = RecordDatasetHelper.this.getTime(members.findMember(RecordDatasetHelper.this.obsTimeVName), sdata);
            this.nomTime = ((RecordDatasetHelper.this.nomTimeVName == null) ? this.obsTime : RecordDatasetHelper.this.getTime(members.findMember(RecordDatasetHelper.this.nomTimeVName), sdata));
            if (useId) {
                String stationId;
                if (RecordDatasetHelper.this.stationIdType == DataType.INT) {
                    stationId = Integer.toString(sdata.getScalarInt(RecordDatasetHelper.this.stnIdVName));
                }
                else {
                    stationId = sdata.getScalarString(RecordDatasetHelper.this.stnIdVName).trim();
                }
                this.station = RecordDatasetHelper.this.stationHelper.getStation(stationId);
                if (null != RecordDatasetHelper.this.errs) {
                    RecordDatasetHelper.this.errs.format(" cant find station id = <%s> when reading record %d\n", stationId, recno);
                }
                RecordDatasetHelper.log.error(" cant find station id = <" + stationId + "> when reading record " + recno);
            }
            else {
                final List<Station> stations = RecordDatasetHelper.this.stationHelper.getStations();
                final int stationIndex = sdata.getScalarInt(RecordDatasetHelper.this.stnIndexVName);
                if (stationIndex < 0 || stationIndex >= stations.size()) {
                    if (null != RecordDatasetHelper.this.errs) {
                        RecordDatasetHelper.this.errs.format(" cant find station at index =%d when reading record %d\n", stationIndex, recno);
                    }
                    RecordDatasetHelper.log.error("cant find station at index = " + stationIndex + " when reading record " + recno);
                }
                else {
                    this.station = stations.get(stationIndex);
                }
            }
            this.location = this.station;
        }
    }
}
