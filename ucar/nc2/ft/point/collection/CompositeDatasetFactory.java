// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.collection;

import ucar.nc2.util.CancelTask;
import ucar.nc2.ft.FeatureDatasetFactoryManager;
import ucar.nc2.ft.FeatureDatasetPoint;
import ucar.nc2.NetcdfFile;
import ucar.nc2.ft.point.PointDatasetImpl;
import org.slf4j.LoggerFactory;
import ucar.nc2.ft.FeatureCollection;
import ucar.nc2.VariableSimpleIF;
import ucar.unidata.geoloc.Station;
import java.util.List;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.io.IOException;
import ucar.nc2.units.DateType;
import org.jdom.Element;
import org.jdom.Document;
import ucar.nc2.units.TimeDuration;
import ucar.nc2.units.DateRange;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonRect;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.nc2.constants.FeatureType;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import ucar.nc2.ft.FeatureDataset;
import java.util.Formatter;
import java.io.File;
import org.slf4j.Logger;

public class CompositeDatasetFactory
{
    public static final String SCHEME = "collection:";
    private static Logger log;
    static boolean debug;
    
    public static FeatureDataset factory(final String locationURI, final File configFile, final Formatter errlog) throws IOException {
        final SAXBuilder builder = new SAXBuilder();
        Document configDoc;
        try {
            configDoc = builder.build(configFile);
        }
        catch (JDOMException e) {
            errlog.format("CompositeDatasetFactory failed to read config document %s err= %s %n", configFile.getPath(), e.getMessage());
            return null;
        }
        final Element root = configDoc.getRootElement();
        final String type = root.getChild("type").getText();
        final FeatureType wantFeatureType = FeatureType.getType(type);
        final String location = root.getChild("location").getText();
        final String dateFormatMark = root.getChild("dateFormatMark").getText();
        final Element geo = root.getChild("geospatialCoverage");
        final Element northsouth = geo.getChild("northsouth");
        final double latStart = readDouble(northsouth.getChild("start"), errlog);
        final double latSize = readDouble(northsouth.getChild("size"), errlog);
        final Element eastwest = geo.getChild("eastwest");
        final double lonStart = readDouble(eastwest.getChild("start"), errlog);
        final double lonSize = readDouble(eastwest.getChild("size"), errlog);
        final LatLonRect llbb = new LatLonRect(new LatLonPointImpl(latStart, lonStart), latSize, lonSize);
        final Element timeCoverage = root.getChild("timeCoverage");
        final DateType start = readDate(timeCoverage.getChild("start"), errlog);
        final DateType end = readDate(timeCoverage.getChild("end"), errlog);
        final TimeDuration duration = readDuration(timeCoverage.getChild("duration"), errlog);
        DateRange dateRange = null;
        try {
            dateRange = new DateRange(start, end, duration, null);
        }
        catch (IllegalArgumentException e2) {
            errlog.format(" ** warning: TimeCoverage error = %s%n", e2.getMessage());
            return null;
        }
        final CompositePointDataset fd = (CompositePointDataset)factory(locationURI, wantFeatureType, location + "?" + dateFormatMark, errlog);
        if (fd == null) {
            return null;
        }
        fd.setBoundingBox(llbb);
        fd.setDateRange(dateRange);
        return fd;
    }
    
    static double readDouble(final Element elem, final Formatter errlog) {
        if (elem == null) {
            return Double.NaN;
        }
        final String text = elem.getText();
        try {
            return Double.parseDouble(text);
        }
        catch (NumberFormatException e) {
            errlog.format(" ** Parse error: Bad double format %s%n", text);
            return Double.NaN;
        }
    }
    
    static DateType readDate(final Element elem, final Formatter errlog) {
        if (elem == null) {
            return null;
        }
        final String format = elem.getAttributeValue("format");
        final String type = elem.getAttributeValue("type");
        final String text = elem.getText();
        if (text == null) {
            return null;
        }
        try {
            return new DateType(text, format, type);
        }
        catch (ParseException e) {
            errlog.format(" ** Parse error: Bad date format = %s%n", text);
            return null;
        }
    }
    
    static TimeDuration readDuration(final Element elem, final Formatter errlog) {
        if (elem == null) {
            return null;
        }
        String text = null;
        try {
            text = elem.getText();
            return new TimeDuration(text);
        }
        catch (ParseException e) {
            errlog.format(" ** Parse error: Bad duration format = %s%n", text);
            return null;
        }
    }
    
    public static FeatureDataset factory(final String location, final FeatureType wantFeatureType, String spec, final Formatter errlog) throws IOException {
        if (spec.startsWith("collection:")) {
            spec = spec.substring("collection:".length());
        }
        final TimedCollection collection = new TimedCollectionImpl(spec, errlog);
        if (collection.getDatasets().size() == 0) {
            throw new FileNotFoundException("Collection is empty; spec=" + spec);
        }
        LatLonRect bb = null;
        FeatureCollection fc = null;
        switch (wantFeatureType) {
            case POINT: {
                final CompositePointCollection pfc = new CompositePointCollection(spec, collection);
                bb = pfc.getBoundingBox();
                fc = pfc;
                break;
            }
            case STATION: {
                final CompositeStationCollection sfc = new CompositeStationCollection(spec, collection, null, null);
                bb = sfc.getBoundingBox();
                fc = sfc;
                break;
            }
            default: {
                return null;
            }
        }
        return new CompositePointDataset(location, wantFeatureType, fc, collection, bb);
    }
    
    static {
        CompositeDatasetFactory.log = LoggerFactory.getLogger(CompositeDatasetFactory.class);
        CompositeDatasetFactory.debug = false;
    }
    
    private static class CompositePointDataset extends PointDatasetImpl
    {
        private TimedCollection datasets;
        private FeatureCollection pfc;
        
        public CompositePointDataset(final String location, final FeatureType featureType, final FeatureCollection pfc, final TimedCollection datasets, final LatLonRect bb) {
            super(featureType);
            this.setLocationURI(location);
            this.setPointFeatureCollection(pfc);
            this.pfc = pfc;
            this.datasets = datasets;
            if (datasets.getDateRange() != null) {
                this.setDateRange(datasets.getDateRange());
            }
            if (bb != null) {
                this.setBoundingBox(bb);
            }
        }
        
        @Override
        public List<VariableSimpleIF> getDataVariables() {
            if (this.dataVariables == null) {
                if (this.pfc instanceof CompositePointCollection) {
                    this.dataVariables = ((CompositePointCollection)this.pfc).getDataVariables();
                }
                else if (this.pfc instanceof CompositeStationCollection) {
                    this.dataVariables = ((CompositeStationCollection)this.pfc).getDataVariables();
                }
            }
            return this.dataVariables;
        }
        
        @Override
        protected void setDateRange(final DateRange dateRange) {
            super.setDateRange(dateRange);
        }
        
        @Override
        protected void setBoundingBox(final LatLonRect boundingBox) {
            super.setBoundingBox(boundingBox);
        }
        
        @Override
        public NetcdfFile getNetcdfFile() {
            final TimedCollection.Dataset td = this.datasets.getPrototype();
            if (td == null) {
                return null;
            }
            final String loc = td.getLocation();
            final Formatter errlog = new Formatter();
            try {
                final FeatureDatasetPoint proto = (FeatureDatasetPoint)FeatureDatasetFactoryManager.open(FeatureType.ANY_POINT, loc, null, errlog);
                return proto.getNetcdfFile();
            }
            catch (IOException e) {
                CompositeDatasetFactory.log.error(errlog.toString());
                e.printStackTrace();
                return null;
            }
        }
    }
}
