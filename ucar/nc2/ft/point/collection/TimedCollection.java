// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.collection;

import ucar.nc2.units.DateRange;
import java.util.List;

public interface TimedCollection
{
    Dataset getPrototype();
    
    List<Dataset> getDatasets();
    
    TimedCollection subset(final DateRange p0);
    
    DateRange getDateRange();
    
    public interface Dataset
    {
        String getLocation();
        
        DateRange getDateRange();
    }
}
