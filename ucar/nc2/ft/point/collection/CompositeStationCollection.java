// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.collection;

import ucar.nc2.ft.PointFeature;
import ucar.nc2.ft.point.PointIteratorAbstract;
import ucar.nc2.ft.PointFeatureIterator;
import ucar.nc2.ft.point.StationFeatureImpl;
import ucar.nc2.ft.NestedPointFeatureCollection;
import ucar.nc2.ft.PointFeatureCollectionIterator;
import ucar.nc2.ft.PointFeatureCollection;
import ucar.nc2.units.DateRange;
import ucar.nc2.ft.StationTimeSeriesFeature;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.ft.StationTimeSeriesFeatureCollection;
import ucar.nc2.ft.FeatureCollection;
import ucar.nc2.util.CancelTask;
import ucar.nc2.ft.FeatureDatasetFactoryManager;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.ft.FeatureDatasetPoint;
import java.util.Formatter;
import java.io.IOException;
import java.util.Iterator;
import ucar.nc2.units.DateUnit;
import ucar.nc2.ft.point.StationHelper;
import ucar.unidata.geoloc.Station;
import ucar.nc2.VariableSimpleIF;
import java.util.List;
import ucar.nc2.ft.point.StationTimeSeriesCollectionImpl;

public class CompositeStationCollection extends StationTimeSeriesCollectionImpl
{
    private TimedCollection dataCollection;
    protected List<VariableSimpleIF> dataVariables;
    
    protected CompositeStationCollection(final String name, final TimedCollection dataCollection, final List<Station> stns, final List<VariableSimpleIF> dataVariables) throws IOException {
        super(name);
        this.dataCollection = dataCollection;
        final TimedCollection.Dataset td = dataCollection.getPrototype();
        if (td == null) {
            throw new RuntimeException("No datasets in the collection");
        }
        if (stns != null && stns.size() > 0) {
            this.stationHelper = new StationHelper();
            for (final Station s : stns) {
                this.stationHelper.addStation(new CompositeStationFeature(s, null, this.dataCollection));
            }
        }
        this.dataVariables = dataVariables;
    }
    
    @Override
    protected void initStationHelper() {
        final TimedCollection.Dataset td = this.dataCollection.getPrototype();
        if (td == null) {
            throw new RuntimeException("No datasets in the collection");
        }
        final Formatter errlog = new Formatter();
        FeatureDatasetPoint openDataset = null;
        try {
            openDataset = (FeatureDatasetPoint)FeatureDatasetFactoryManager.open(FeatureType.STATION, td.getLocation(), null, errlog);
            final List<FeatureCollection> fcList = openDataset.getPointFeatureCollectionList();
            final StationTimeSeriesCollectionImpl openCollection = fcList.get(0);
            final List<Station> stns = openCollection.getStations();
            this.stationHelper = new StationHelper();
            for (final Station s : stns) {
                this.stationHelper.addStation(new CompositeStationFeature(s, null, this.dataCollection));
            }
            this.dataVariables = openDataset.getDataVariables();
        }
        catch (Exception ioe) {
            throw new RuntimeException(ioe);
        }
        finally {
            try {
                if (openDataset != null) {
                    openDataset.close();
                }
            }
            catch (Throwable t) {}
        }
    }
    
    public List<VariableSimpleIF> getDataVariables() {
        if (this.dataVariables == null) {
            this.initStationHelper();
        }
        return this.dataVariables;
    }
    
    @Override
    public StationTimeSeriesFeatureCollection subset(final List<Station> stations) throws IOException {
        if (stations == null) {
            return this;
        }
        return new CompositeStationCollection(this.getName(), this.dataCollection, stations, this.dataVariables);
    }
    
    @Override
    public StationTimeSeriesFeatureCollection subset(final LatLonRect boundingBox) throws IOException {
        if (boundingBox == null) {
            return this;
        }
        final List<Station> stations = this.stationHelper.getStations(boundingBox);
        return new CompositeStationCollection(this.getName(), this.dataCollection, stations, this.dataVariables);
    }
    
    @Override
    public StationTimeSeriesFeature getStationFeature(final Station s) throws IOException {
        return new CompositeStationFeature(s, null, this.dataCollection);
    }
    
    @Override
    public PointFeatureCollection flatten(final LatLonRect boundingBox, final DateRange dateRange) throws IOException {
        final TimedCollection subsetCollection = (dateRange != null) ? this.dataCollection.subset(dateRange) : this.dataCollection;
        return new CompositeStationCollectionFlattened(this.getName(), boundingBox, dateRange, subsetCollection);
    }
    
    @Override
    public PointFeatureCollection flatten(final List<String> stations, final DateRange dateRange, final List<VariableSimpleIF> varList) throws IOException {
        final TimedCollection subsetCollection = (dateRange != null) ? this.dataCollection.subset(dateRange) : this.dataCollection;
        return new CompositeStationCollectionFlattened(this.getName(), stations, dateRange, varList, subsetCollection);
    }
    
    @Override
    public PointFeatureCollectionIterator getPointFeatureCollectionIterator(final int bufferSize) throws IOException {
        return new PointFeatureCollectionIterator() {
            Iterator<Station> stationIter = CompositeStationCollection.this.stationHelper.getStations().iterator();
            
            public boolean hasNext() throws IOException {
                return this.stationIter.hasNext();
            }
            
            public PointFeatureCollection next() throws IOException {
                return (PointFeatureCollection)this.stationIter.next();
            }
            
            public void setBufferSize(final int bytes) {
            }
            
            public void finish() {
            }
        };
    }
    
    private class CompositeStationFeature extends StationFeatureImpl
    {
        private TimedCollection collForFeature;
        
        CompositeStationFeature(final Station s, final DateUnit timeUnit, final TimedCollection collForFeature) {
            super(s, timeUnit, -1);
            this.setDateRange(collForFeature.getDateRange());
            this.collForFeature = collForFeature;
        }
        
        public PointFeatureIterator getPointFeatureIterator(final int bufferSize) throws IOException {
            final CompositeStationFeatureIterator iter = new CompositeStationFeatureIterator();
            if (this.boundingBox == null || this.dateRange == null || this.npts < 0) {
                iter.setCalculateBounds(this);
            }
            return iter;
        }
        
        @Override
        public StationTimeSeriesFeature subset(final DateRange dateRange) throws IOException {
            if (dateRange == null) {
                return this;
            }
            final CompositeStationFeature stnSubset = new CompositeStationFeature(this.s, this.timeUnit, this.collForFeature.subset(dateRange));
            return stnSubset.subset(dateRange);
        }
        
        @Override
        public PointFeatureCollection subset(final LatLonRect boundingBox, final DateRange dateRange) throws IOException {
            if (boundingBox != null) {
                if (!boundingBox.contains(this.s.getLatLon())) {
                    return null;
                }
                if (dateRange == null) {
                    return this;
                }
            }
            return this.subset(dateRange);
        }
        
        private class CompositeStationFeatureIterator extends PointIteratorAbstract
        {
            private int bufferSize;
            private Iterator<TimedCollection.Dataset> iter;
            private FeatureDatasetPoint currentDataset;
            private PointFeatureIterator pfIter;
            private boolean finished;
            
            CompositeStationFeatureIterator() {
                this.bufferSize = -1;
                this.pfIter = null;
                this.finished = false;
                this.iter = CompositeStationFeature.this.collForFeature.getDatasets().iterator();
            }
            
            private PointFeatureIterator getNextIterator() throws IOException {
                if (!this.iter.hasNext()) {
                    return null;
                }
                final TimedCollection.Dataset td = this.iter.next();
                final Formatter errlog = new Formatter();
                this.currentDataset = (FeatureDatasetPoint)FeatureDatasetFactoryManager.open(FeatureType.STATION, td.getLocation(), null, errlog);
                final List<FeatureCollection> fcList = this.currentDataset.getPointFeatureCollectionList();
                final StationTimeSeriesFeatureCollection stnCollection = fcList.get(0);
                final Station s = stnCollection.getStation(CompositeStationFeature.this.getName());
                if (s == null) {
                    System.out.printf("CompositeStationFeatureIterator dataset: %s missing station %s%n", td.getLocation(), CompositeStationFeature.this.getName());
                    return this.getNextIterator();
                }
                final StationTimeSeriesFeature stnFeature = stnCollection.getStationFeature(s);
                if (CompositeDatasetFactory.debug) {
                    System.out.printf("CompositeStationFeatureIterator open dataset: %s for %s%n", td.getLocation(), s.getName());
                }
                return stnFeature.getPointFeatureIterator(this.bufferSize);
            }
            
            public boolean hasNext() throws IOException {
                if (this.pfIter == null) {
                    this.pfIter = this.getNextIterator();
                    if (this.pfIter == null) {
                        this.finish();
                        return false;
                    }
                }
                if (!this.pfIter.hasNext()) {
                    this.pfIter.finish();
                    this.currentDataset.close();
                    if (CompositeDatasetFactory.debug) {
                        System.out.printf("CompositeStationFeatureIterator close dataset: %s%n", this.currentDataset.getLocation());
                    }
                    this.pfIter = this.getNextIterator();
                    return this.hasNext();
                }
                return true;
            }
            
            public PointFeature next() throws IOException {
                CompositeStationFeature.this.npts++;
                return this.pfIter.next();
            }
            
            public void finish() {
                if (this.finished) {
                    return;
                }
                if (this.pfIter != null) {
                    this.pfIter.finish();
                }
                if (this.currentDataset != null) {
                    try {
                        this.currentDataset.close();
                        if (CompositeDatasetFactory.debug) {
                            System.out.printf("CompositeStationFeatureIterator close dataset: %s%n", this.currentDataset.getLocation());
                        }
                    }
                    catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
                this.finishCalcBounds();
                this.finished = true;
            }
            
            public void setBufferSize(final int bytes) {
                this.bufferSize = bytes;
            }
        }
    }
}
