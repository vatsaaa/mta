// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.collection;

import ucar.nc2.units.DateFromString;
import java.util.Date;
import java.io.IOException;
import java.util.Iterator;
import thredds.inventory.CollectionManager;
import thredds.inventory.MFile;
import java.util.ArrayList;
import ucar.nc2.util.CancelTask;
import thredds.inventory.DatasetCollectionManager;
import java.util.Formatter;
import ucar.nc2.units.DateRange;
import java.util.List;
import thredds.inventory.CollectionSpecParser;

public class TimedCollectionImpl implements TimedCollection
{
    private static final boolean debug = false;
    private CollectionSpecParser sp;
    private List<TimedCollection.Dataset> datasets;
    private DateRange dateRange;
    
    public TimedCollectionImpl(final String collectionSpec, final Formatter errlog) throws IOException {
        this.sp = new CollectionSpecParser(collectionSpec, errlog);
        final CollectionManager manager = new DatasetCollectionManager(this.sp, errlog);
        manager.scan(null);
        final List<MFile> fileList = manager.getFiles();
        this.datasets = new ArrayList<TimedCollection.Dataset>(fileList.size());
        for (final MFile f : fileList) {
            this.datasets.add(new Dataset(f));
        }
        if (this.sp.getDateFormatMark() != null) {
            for (int i = 0; i < this.datasets.size() - 1; ++i) {
                final Dataset d1 = this.datasets.get(i);
                final Dataset d2 = this.datasets.get(i + 1);
                d1.setDateRange(new DateRange(d1.start, d2.start));
                if (i == this.datasets.size() - 2) {
                    d2.setDateRange(new DateRange(d2.start, d1.getDateRange().getDuration()));
                }
            }
            if (this.datasets.size() > 0) {
                final Dataset first = this.datasets.get(0);
                final Dataset last = this.datasets.get(this.datasets.size() - 1);
                this.dateRange = new DateRange(first.getDateRange().getStart().getDate(), last.getDateRange().getEnd().getDate());
            }
        }
    }
    
    private TimedCollectionImpl(final TimedCollectionImpl from, final DateRange want) {
        this.datasets = new ArrayList<TimedCollection.Dataset>(from.datasets.size());
        for (final TimedCollection.Dataset d : from.datasets) {
            if (want.intersects(d.getDateRange())) {
                this.datasets.add(d);
            }
        }
    }
    
    public TimedCollection.Dataset getPrototype() {
        return (this.datasets.size() > 0) ? this.datasets.get(0) : null;
    }
    
    public List<TimedCollection.Dataset> getDatasets() {
        return this.datasets;
    }
    
    public TimedCollection subset(final DateRange range) {
        return new TimedCollectionImpl(this, range);
    }
    
    public DateRange getDateRange() {
        return this.dateRange;
    }
    
    @Override
    public String toString() {
        final Formatter f = new Formatter();
        f.format("CollectionManager{%n", new Object[0]);
        for (final TimedCollection.Dataset d : this.datasets) {
            f.format(" %s%n", d);
        }
        f.format("}%n", new Object[0]);
        return f.toString();
    }
    
    public static void doit(final String spec, final Formatter errlog) throws IOException {
        final TimedCollectionImpl specp = new TimedCollectionImpl(spec, errlog);
        System.out.printf("spec= %s%n%s%n", spec, specp);
        final String err = errlog.toString();
        if (err.length() > 0) {
            System.out.printf("%s%n", err);
        }
        System.out.printf("-----------------------------------%n", new Object[0]);
    }
    
    public static void main(final String[] arg) throws IOException {
        doit("C:/data/formats/gempak/surface/#yyyyMMdd#_sao.gem", new Formatter());
    }
    
    private class Dataset implements TimedCollection.Dataset
    {
        String location;
        DateRange dateRange;
        Date start;
        
        Dataset(final MFile f) {
            this.location = f.getPath();
            if (TimedCollectionImpl.this.sp.getDateFormatMark() != null) {
                this.start = DateFromString.getDateUsingDemarkatedCount(f.getName(), TimedCollectionImpl.this.sp.getDateFormatMark(), '#');
            }
        }
        
        public String getLocation() {
            return this.location;
        }
        
        public DateRange getDateRange() {
            return this.dateRange;
        }
        
        public void setDateRange(final DateRange dateRange) {
            this.dateRange = dateRange;
        }
        
        @Override
        public String toString() {
            return "Dataset{location='" + this.location + '\'' + ", dateRange=" + this.dateRange + '}';
        }
    }
}
