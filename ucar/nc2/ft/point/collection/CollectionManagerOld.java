// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.collection;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import ucar.nc2.units.DateFromString;
import java.util.Date;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Arrays;
import java.io.FilenameFilter;
import java.io.File;
import java.util.Formatter;
import ucar.nc2.units.DateRange;
import java.util.List;

public class CollectionManagerOld
{
    private boolean show;
    private List<TimedCollection.Dataset> c;
    private DateRange dateRange;
    
    public static CollectionManagerOld factory(final String collectionDesc, final Formatter errlog) {
        return new CollectionManagerOld(collectionDesc, errlog);
    }
    
    private CollectionManagerOld(String collectionDesc, final Formatter errlog) {
        this.show = true;
        final int posWildcard = collectionDesc.lastIndexOf(47);
        final String dirName = collectionDesc.substring(0, posWildcard);
        final File dir = new File(dirName);
        final File locFile = new File(dirName);
        if (!locFile.exists()) {
            errlog.format(" Directory %s does not exist %n", dirName);
            return;
        }
        String dateFormatMark = null;
        final int posFormat = collectionDesc.lastIndexOf(63);
        if (posFormat > 0) {
            dateFormatMark = collectionDesc.substring(posFormat + 1);
            collectionDesc = collectionDesc.substring(0, posFormat);
        }
        String filter = null;
        if (posWildcard < collectionDesc.length() - 2) {
            filter = collectionDesc.substring(posWildcard + 1);
        }
        if (this.show) {
            System.out.printf("CollectionManager collectionDesc=%s filter=%s dateFormatMark=%s %n", collectionDesc, filter, dateFormatMark);
        }
        final File[] files = (filter == null) ? dir.listFiles() : dir.listFiles(new WildcardMatchOnNameFilter(filter));
        final List<File> fileList = Arrays.asList(files);
        Collections.sort(fileList);
        this.c = new ArrayList<TimedCollection.Dataset>(fileList.size());
        for (final File f : fileList) {
            this.c.add(new Dataset(f, dateFormatMark));
        }
        if (dateFormatMark != null) {
            for (int i = 0; i < this.c.size() - 1; ++i) {
                final Dataset d1 = this.c.get(i);
                final Dataset d2 = this.c.get(i + 1);
                d1.setDateRange(new DateRange(d1.start, d2.start));
                if (i == this.c.size() - 2) {
                    d2.setDateRange(new DateRange(d2.start, d1.getDateRange().getDuration()));
                }
            }
            final Dataset first = this.c.get(0);
            final Dataset last = this.c.get(this.c.size() - 1);
            this.dateRange = new DateRange(first.getDateRange().getStart().getDate(), last.getDateRange().getEnd().getDate());
        }
        if (this.show) {
            System.out.printf("%s %n", this);
        }
    }
    
    CollectionManagerOld(final CollectionManagerOld from, final DateRange want) {
        this.show = true;
        this.c = new ArrayList<TimedCollection.Dataset>(from.c.size());
        for (final TimedCollection.Dataset d : from.c) {
            if (want.intersects(d.getDateRange())) {
                this.c.add(d);
            }
        }
    }
    
    public TimedCollection.Dataset getPrototype() {
        return this.c.get(0);
    }
    
    public Iterator<TimedCollection.Dataset> getIterator() {
        return this.c.iterator();
    }
    
    public CollectionManagerOld subset(final DateRange range) {
        return new CollectionManagerOld(this, range);
    }
    
    public DateRange getDateRange() {
        return this.dateRange;
    }
    
    @Override
    public String toString() {
        final Formatter f = new Formatter();
        f.format("CollectionManager{%n", new Object[0]);
        for (final TimedCollection.Dataset d : this.c) {
            f.format(" %s%n", d);
        }
        f.format("}%n", new Object[0]);
        return f.toString();
    }
    
    private class Dataset implements TimedCollection.Dataset
    {
        String location;
        DateRange dateRange;
        Date start;
        
        Dataset(final File f, final String dateFormatMark) {
            this.location = f.getPath();
            if (dateFormatMark != null) {
                this.start = DateFromString.getDateUsingDemarkatedCount(f.getName(), dateFormatMark, '#');
            }
        }
        
        public String getLocation() {
            return this.location;
        }
        
        public DateRange getDateRange() {
            return this.dateRange;
        }
        
        public void setDateRange(final DateRange dateRange) {
            this.dateRange = dateRange;
        }
        
        @Override
        public String toString() {
            return "Dataset{location='" + this.location + '\'' + ", dateRange=" + this.dateRange + '}';
        }
    }
    
    private class WildcardMatchOnNameFilter implements FilenameFilter
    {
        protected Pattern pattern;
        
        public WildcardMatchOnNameFilter(final String wildcardString) {
            final String regExp = this.mapWildcardToRegExp(wildcardString);
            this.pattern = Pattern.compile(regExp);
        }
        
        private String mapWildcardToRegExp(String wildcardString) {
            wildcardString = wildcardString.replaceAll("\\.", "\\\\.");
            wildcardString = wildcardString.replaceAll("\\*", ".*");
            wildcardString = wildcardString.replaceAll("\\?", ".?");
            return wildcardString;
        }
        
        public boolean accept(final File dir, final String name) {
            final Matcher matcher = this.pattern.matcher(name);
            return matcher.matches();
        }
    }
}
