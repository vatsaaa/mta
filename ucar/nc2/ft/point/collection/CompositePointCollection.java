// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.collection;

import ucar.nc2.ft.PointFeature;
import ucar.nc2.ft.FeatureCollection;
import java.util.Iterator;
import ucar.nc2.ft.point.PointIteratorAbstract;
import ucar.nc2.ft.PointFeatureIterator;
import ucar.nc2.ft.PointFeatureCollection;
import ucar.nc2.units.DateRange;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.util.CancelTask;
import ucar.nc2.ft.FeatureDatasetFactoryManager;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.ft.FeatureDatasetPoint;
import java.util.Formatter;
import java.io.IOException;
import ucar.nc2.VariableSimpleIF;
import java.util.List;
import ucar.nc2.ft.point.PointCollectionImpl;

public class CompositePointCollection extends PointCollectionImpl
{
    private TimedCollection pointCollections;
    protected List<VariableSimpleIF> dataVariables;
    
    protected CompositePointCollection(final String name, final TimedCollection pointCollections) throws IOException {
        super(name);
        this.pointCollections = pointCollections;
    }
    
    public List<VariableSimpleIF> getDataVariables() {
        if (this.dataVariables == null) {
            final TimedCollection.Dataset td = this.pointCollections.getPrototype();
            if (td == null) {
                throw new RuntimeException("No datasets in the collection");
            }
            final Formatter errlog = new Formatter();
            FeatureDatasetPoint openDataset = null;
            try {
                openDataset = (FeatureDatasetPoint)FeatureDatasetFactoryManager.open(FeatureType.POINT, td.getLocation(), null, errlog);
                this.dataVariables = openDataset.getDataVariables();
            }
            catch (IOException ioe) {
                throw new RuntimeException(ioe);
            }
            finally {
                try {
                    if (openDataset != null) {
                        openDataset.close();
                    }
                }
                catch (Throwable t) {}
            }
        }
        return this.dataVariables;
    }
    
    @Override
    public PointFeatureCollection subset(final LatLonRect boundingBox, final DateRange dateRange) throws IOException {
        if (dateRange == null && boundingBox == null) {
            return this;
        }
        if (dateRange == null) {
            return new PointCollectionSubset(this, boundingBox, dateRange);
        }
        final CompositePointCollection dateSubset = new CompositePointCollection(this.name, this.pointCollections.subset(dateRange));
        return new PointCollectionSubset(dateSubset, boundingBox, dateRange);
    }
    
    public PointFeatureIterator getPointFeatureIterator(final int bufferSize) throws IOException {
        final CompositePointFeatureIterator iter = new CompositePointFeatureIterator();
        if (this.boundingBox == null || this.dateRange == null || this.npts < 0) {
            iter.setCalculateBounds(this);
        }
        return iter;
    }
    
    private class CompositePointFeatureIterator extends PointIteratorAbstract
    {
        private boolean finished;
        private int bufferSize;
        private Iterator<TimedCollection.Dataset> iter;
        private FeatureDatasetPoint currentDataset;
        private PointFeatureIterator pfIter;
        
        CompositePointFeatureIterator() {
            this.finished = false;
            this.bufferSize = -1;
            this.pfIter = null;
            this.iter = CompositePointCollection.this.pointCollections.getDatasets().iterator();
        }
        
        private PointFeatureIterator getNextIterator() throws IOException {
            if (!this.iter.hasNext()) {
                return null;
            }
            final TimedCollection.Dataset td = this.iter.next();
            final Formatter errlog = new Formatter();
            this.currentDataset = (FeatureDatasetPoint)FeatureDatasetFactoryManager.open(FeatureType.POINT, td.getLocation(), null, errlog);
            if (CompositeDatasetFactory.debug) {
                System.out.printf("CompositePointFeatureIterator open dataset %s%n", td.getLocation());
            }
            final List<FeatureCollection> fcList = this.currentDataset.getPointFeatureCollectionList();
            final PointFeatureCollection pc = fcList.get(0);
            return pc.getPointFeatureIterator(this.bufferSize);
        }
        
        public boolean hasNext() throws IOException {
            if (this.pfIter == null) {
                this.pfIter = this.getNextIterator();
                if (this.pfIter == null) {
                    this.finish();
                    return false;
                }
            }
            if (!this.pfIter.hasNext()) {
                this.pfIter.finish();
                this.currentDataset.close();
                this.pfIter = this.getNextIterator();
                return this.hasNext();
            }
            return true;
        }
        
        public PointFeature next() throws IOException {
            return this.pfIter.next();
        }
        
        public void finish() {
            if (this.finished) {
                return;
            }
            if (this.pfIter != null) {
                this.pfIter.finish();
            }
            this.finishCalcBounds();
            if (this.currentDataset != null) {
                try {
                    this.currentDataset.close();
                }
                catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            this.finished = true;
        }
        
        public void setBufferSize(final int bytes) {
            this.bufferSize = bytes;
        }
    }
    
    private class CompositePointFeatureIteratorMultithreaded extends PointIteratorAbstract
    {
        private boolean finished;
        private int bufferSize;
        private Iterator<TimedCollection.Dataset> iter;
        private FeatureDatasetPoint currentDataset;
        private PointFeatureIterator pfIter;
        
        CompositePointFeatureIteratorMultithreaded() {
            this.finished = false;
            this.bufferSize = -1;
            this.pfIter = null;
            this.iter = CompositePointCollection.this.pointCollections.getDatasets().iterator();
        }
        
        private PointFeatureIterator getNextIterator() throws IOException {
            if (!this.iter.hasNext()) {
                return null;
            }
            final TimedCollection.Dataset td = this.iter.next();
            final Formatter errlog = new Formatter();
            this.currentDataset = (FeatureDatasetPoint)FeatureDatasetFactoryManager.open(FeatureType.POINT, td.getLocation(), null, errlog);
            if (CompositeDatasetFactory.debug) {
                System.out.printf("CompositePointFeatureIterator open dataset %s%n", td.getLocation());
            }
            final List<FeatureCollection> fcList = this.currentDataset.getPointFeatureCollectionList();
            final PointFeatureCollection pc = fcList.get(0);
            return pc.getPointFeatureIterator(this.bufferSize);
        }
        
        public boolean hasNext() throws IOException {
            if (this.pfIter == null) {
                this.pfIter = this.getNextIterator();
                if (this.pfIter == null) {
                    this.finish();
                    return false;
                }
            }
            if (!this.pfIter.hasNext()) {
                this.pfIter.finish();
                this.currentDataset.close();
                this.pfIter = this.getNextIterator();
                return this.hasNext();
            }
            return true;
        }
        
        public PointFeature next() throws IOException {
            return this.pfIter.next();
        }
        
        public void finish() {
            if (this.finished) {
                return;
            }
            if (this.pfIter != null) {
                this.pfIter.finish();
            }
            this.finishCalcBounds();
            if (this.currentDataset != null) {
                try {
                    this.currentDataset.close();
                }
                catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            this.finished = true;
        }
        
        public void setBufferSize(final int bytes) {
            this.bufferSize = bytes;
        }
    }
}
