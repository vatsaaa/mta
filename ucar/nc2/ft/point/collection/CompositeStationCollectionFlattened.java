// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.collection;

import ucar.nc2.ft.PointFeature;
import ucar.nc2.ft.FeatureCollection;
import ucar.nc2.ft.StationTimeSeriesFeatureCollection;
import ucar.nc2.util.CancelTask;
import ucar.nc2.ft.FeatureDatasetFactoryManager;
import ucar.nc2.constants.FeatureType;
import java.util.Formatter;
import ucar.nc2.ft.FeatureDatasetPoint;
import java.util.Iterator;
import ucar.nc2.ft.point.PointIteratorAbstract;
import org.slf4j.LoggerFactory;
import ucar.nc2.ft.PointFeatureCollection;
import ucar.nc2.ft.PointFeatureIterator;
import java.io.IOException;
import ucar.nc2.VariableSimpleIF;
import ucar.nc2.units.DateRange;
import java.util.List;
import ucar.unidata.geoloc.LatLonRect;
import org.slf4j.Logger;
import ucar.nc2.ft.point.PointCollectionImpl;

public class CompositeStationCollectionFlattened extends PointCollectionImpl
{
    private static Logger logger;
    private TimedCollection stnCollections;
    private LatLonRect bbSubset;
    private List<String> stationsSubset;
    private DateRange dateRange;
    private List<VariableSimpleIF> varList;
    private boolean wantStationsubset;
    
    protected CompositeStationCollectionFlattened(final String name, final List<String> stations, final DateRange dateRange, final List<VariableSimpleIF> varList, final TimedCollection stnCollections) throws IOException {
        super(name);
        this.wantStationsubset = false;
        this.stationsSubset = stations;
        this.dateRange = dateRange;
        this.varList = varList;
        this.stnCollections = stnCollections;
        this.wantStationsubset = (stations != null && stations.size() > 0);
    }
    
    protected CompositeStationCollectionFlattened(final String name, final LatLonRect bbSubset, final DateRange dateRange, final TimedCollection stnCollections) throws IOException {
        super(name);
        this.wantStationsubset = false;
        this.bbSubset = bbSubset;
        this.dateRange = dateRange;
        this.stnCollections = stnCollections;
    }
    
    public PointFeatureIterator getPointFeatureIterator(final int bufferSize) throws IOException {
        final PointIterator iter = new PointIterator();
        if (this.boundingBox == null || this.dateRange == null || this.npts < 0) {
            iter.setCalculateBounds(this);
        }
        return iter;
    }
    
    static {
        CompositeStationCollectionFlattened.logger = LoggerFactory.getLogger(CompositeStationCollectionFlattened.class);
    }
    
    private class PointIterator extends PointIteratorAbstract
    {
        private boolean finished;
        private int bufferSize;
        private Iterator<TimedCollection.Dataset> iter;
        private FeatureDatasetPoint currentDataset;
        private PointFeatureIterator pfIter;
        
        PointIterator() {
            this.finished = false;
            this.bufferSize = -1;
            this.pfIter = null;
            this.iter = CompositeStationCollectionFlattened.this.stnCollections.getDatasets().iterator();
        }
        
        private PointFeatureIterator getNextIterator() throws IOException {
            if (!this.iter.hasNext()) {
                return null;
            }
            final TimedCollection.Dataset td = this.iter.next();
            final Formatter errlog = new Formatter();
            this.currentDataset = (FeatureDatasetPoint)FeatureDatasetFactoryManager.open(FeatureType.STATION, td.getLocation(), null, errlog);
            if (this.currentDataset == null) {
                CompositeStationCollectionFlattened.logger.error("FeatureDatasetFactoryManager failed to open: " + td.getLocation() + " \nerrlog = " + errlog);
                return this.getNextIterator();
            }
            if (CompositeDatasetFactory.debug) {
                System.out.printf("CompositeStationCollectionFlattened.Iterator open new dataset: %s%n", td.getLocation());
            }
            final List<FeatureCollection> fcList = this.currentDataset.getPointFeatureCollectionList();
            final StationTimeSeriesFeatureCollection stnCollection = fcList.get(0);
            PointFeatureCollection pc = null;
            if (CompositeStationCollectionFlattened.this.wantStationsubset) {
                pc = stnCollection.flatten(CompositeStationCollectionFlattened.this.stationsSubset, CompositeStationCollectionFlattened.this.dateRange, CompositeStationCollectionFlattened.this.varList);
            }
            else {
                pc = stnCollection.flatten(CompositeStationCollectionFlattened.this.bbSubset, CompositeStationCollectionFlattened.this.dateRange);
            }
            return pc.getPointFeatureIterator(this.bufferSize);
        }
        
        public boolean hasNext() throws IOException {
            if (this.pfIter == null) {
                this.pfIter = this.getNextIterator();
                if (this.pfIter == null) {
                    this.finish();
                    return false;
                }
            }
            if (!this.pfIter.hasNext()) {
                this.pfIter.finish();
                this.currentDataset.close();
                if (CompositeDatasetFactory.debug) {
                    System.out.printf("CompositeStationCollectionFlattened.Iterator close dataset: %s%n", this.currentDataset.getLocation());
                }
                this.pfIter = this.getNextIterator();
                return this.hasNext();
            }
            return true;
        }
        
        public PointFeature next() throws IOException {
            return this.pfIter.next();
        }
        
        public void finish() {
            if (this.finished) {
                return;
            }
            if (this.pfIter != null) {
                this.pfIter.finish();
            }
            this.finishCalcBounds();
            if (this.currentDataset != null) {
                try {
                    this.currentDataset.close();
                    if (CompositeDatasetFactory.debug) {
                        System.out.printf("CompositeStationCollectionFlattened close dataset: %s%n", this.currentDataset.getLocation());
                    }
                }
                catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            this.finished = true;
        }
        
        public void setBufferSize(final int bytes) {
            this.bufferSize = bytes;
        }
    }
}
