// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point;

import ucar.nc2.ft.PointFeatureCollection;
import ucar.unidata.geoloc.LatLonRect;
import java.io.IOException;
import ucar.nc2.ft.PointFeatureIterator;
import ucar.nc2.units.DateRange;

public class StationTimeSeriesCollectionFlattened extends PointCollectionImpl
{
    protected StationTimeSeriesCollectionImpl from;
    
    public StationTimeSeriesCollectionFlattened(final StationTimeSeriesCollectionImpl from, final DateRange dateRange) {
        super(from.getName());
        this.dateRange = dateRange;
        this.from = from;
    }
    
    public PointFeatureIterator getPointFeatureIterator(final int bufferSize) throws IOException {
        return new PointIteratorFlatten(this.from.getPointFeatureCollectionIterator(bufferSize), this.boundingBox, this.dateRange);
    }
    
    @Override
    public PointFeatureCollection subset(final LatLonRect boundingBox, final DateRange dateRange) throws IOException {
        return null;
    }
}
