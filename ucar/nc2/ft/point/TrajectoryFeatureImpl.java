// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point;

import ucar.nc2.constants.FeatureType;
import ucar.nc2.ft.TrajectoryFeature;

public abstract class TrajectoryFeatureImpl extends PointCollectionImpl implements TrajectoryFeature
{
    public TrajectoryFeatureImpl(final String name, final int npts) {
        super(name);
        this.npts = npts;
    }
    
    @Override
    public FeatureType getCollectionFeatureType() {
        return FeatureType.TRAJECTORY;
    }
}
