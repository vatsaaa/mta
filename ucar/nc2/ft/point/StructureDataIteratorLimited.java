// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point;

import ucar.ma2.StructureData;
import java.io.IOException;
import ucar.ma2.StructureDataIterator;

public class StructureDataIteratorLimited implements StructureDataIterator
{
    private StructureDataIterator org;
    private int limit;
    private int count;
    
    public StructureDataIteratorLimited(final StructureDataIterator org, final int limit) throws IOException {
        this.org = org;
        this.limit = limit;
        this.count = 0;
    }
    
    public StructureData next() throws IOException {
        return this.org.next();
    }
    
    public boolean hasNext() throws IOException {
        return this.count < this.limit && this.org.hasNext();
    }
    
    public StructureDataIterator reset() {
        this.count = 0;
        this.org = this.org.reset();
        return this;
    }
    
    public void setBufferSize(final int bytes) {
        this.org.setBufferSize(bytes);
    }
    
    public int getCurrentRecno() {
        return this.org.getCurrentRecno();
    }
}
