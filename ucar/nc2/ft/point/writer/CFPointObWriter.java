// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.writer;

import ucar.nc2.Dimension;
import ucar.ma2.DataType;
import java.io.File;
import ucar.nc2.ft.PointFeature;
import java.io.BufferedOutputStream;
import ucar.nc2.ft.PointFeatureCollection;
import ucar.nc2.ft.FeatureCollection;
import ucar.nc2.ft.FeatureDataset;
import ucar.nc2.ft.FeatureDatasetPoint;
import ucar.nc2.ft.FeatureDatasetFactoryManager;
import java.util.Formatter;
import ucar.ma2.Array;
import ucar.ma2.StructureData;
import ucar.unidata.geoloc.EarthLocation;
import ucar.nc2.dt.DataIterator;
import ucar.ma2.ArrayChar;
import java.io.OutputStream;
import java.io.FileOutputStream;
import ucar.nc2.dt.PointObsDatatype;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dt.TypedDatasetFactory;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.dt.PointObsDataset;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.NetcdfFile;
import java.util.Date;
import java.io.IOException;
import java.util.Iterator;
import ucar.nc2.VariableSimpleIF;
import java.util.ArrayList;
import ucar.nc2.Attribute;
import java.util.List;
import java.io.DataOutputStream;

public class CFPointObWriter
{
    private WriterCFPointDataset ncWriter;
    
    public CFPointObWriter(final DataOutputStream stream, final List<Attribute> globalAtts, final String altUnits, final List<PointObVar> dataVars, final int numrec) throws IOException {
        this.ncWriter = new WriterCFPointDataset(stream, globalAtts, altUnits);
        final List<VariableSimpleIF> vars = new ArrayList<VariableSimpleIF>(dataVars.size());
        for (final PointObVar pvar : dataVars) {
            vars.add(new PointObVarAdapter(pvar));
        }
        this.ncWriter.writeHeader(vars, numrec);
    }
    
    public void addPoint(final double lat, final double lon, final double alt, final Date time, final double[] vals, final String[] svals) throws IOException {
        this.ncWriter.writeRecord(lat, lon, alt, time, vals, svals);
    }
    
    public void finish() throws IOException {
        this.ncWriter.finish();
    }
    
    public static boolean rewritePointObsDataset(final String fileIn, final String fileOut, final boolean inMemory) throws IOException {
        System.out.println("Rewrite2 .nc files from " + fileIn + " to " + fileOut + " inMemory= " + inMemory);
        final long start = System.currentTimeMillis();
        final NetcdfFile ncfile = inMemory ? NetcdfFile.openInMemory(fileIn) : NetcdfFile.open(fileIn);
        final NetcdfDataset ncd = new NetcdfDataset(ncfile);
        final StringBuilder errlog = new StringBuilder();
        final PointObsDataset pobsDataset = (PointObsDataset)TypedDatasetFactory.open(FeatureType.POINT, ncd, null, errlog);
        if (pobsDataset == null) {
            return false;
        }
        writePointObsDataset(pobsDataset, fileOut);
        pobsDataset.close();
        final long took = System.currentTimeMillis() - start;
        System.out.println(" that took " + (took - start) + " msecs");
        return true;
    }
    
    public static void writePointObsDataset(final PointObsDataset pobsDataset, final String fileOut) throws IOException {
        String altUnits = null;
        final DataIterator iterOne = pobsDataset.getDataIterator(-1);
        if (iterOne.hasNext()) {
            final PointObsDatatype pobsData = (PointObsDatatype)iterOne.nextData();
            final EarthLocation loc = pobsData.getLocation();
            altUnits = (Double.isNaN(loc.getAltitude()) ? null : "meters");
        }
        final List<VariableSimpleIF> vars = pobsDataset.getDataVariables();
        final List<PointObVar> nvars = new ArrayList<PointObVar>(vars.size());
        for (final VariableSimpleIF v : vars) {
            if (v.getDataType().isNumeric()) {
                nvars.add(new PointObVar(v));
            }
        }
        final int ndoubles = vars.size();
        final double[] dvals = new double[ndoubles];
        for (final VariableSimpleIF v2 : vars) {
            if (v2.getDataType().isString()) {
                nvars.add(new PointObVar(v2));
            }
        }
        final String[] svals = new String[vars.size() - ndoubles];
        final FileOutputStream fos = new FileOutputStream(fileOut);
        final DataOutputStream out = new DataOutputStream(fos);
        final CFPointObWriter writer = new CFPointObWriter(out, pobsDataset.getGlobalAttributes(), altUnits, nvars, -1);
        final DataIterator iter = pobsDataset.getDataIterator(1000000);
        while (iter.hasNext()) {
            final PointObsDatatype pobsData2 = (PointObsDatatype)iter.nextData();
            final StructureData sdata = pobsData2.getData();
            int dcount = 0;
            int scount = 0;
            for (final PointObVar v3 : nvars) {
                if (v3.getDataType().isNumeric()) {
                    final Array data = sdata.getArray(v3.getName());
                    data.resetLocalIterator();
                    if (!data.hasNext()) {
                        continue;
                    }
                    dvals[dcount++] = data.nextDouble();
                }
                else {
                    if (!v3.getDataType().isString()) {
                        continue;
                    }
                    final ArrayChar data2 = (ArrayChar)sdata.getArray(v3.getName());
                    svals[scount++] = data2.getString();
                }
            }
            final EarthLocation loc2 = pobsData2.getLocation();
            writer.addPoint(loc2.getLatitude(), loc2.getLongitude(), loc2.getAltitude(), pobsData2.getObservationTimeAsDate(), dvals, svals);
        }
        writer.finish();
    }
    
    public static boolean rewritePointFeatureDataset(final String fileIn, final String fileOut, final boolean inMemory) throws IOException {
        System.out.println("Rewrite2 .nc files from " + fileIn + " to " + fileOut + " inMemory= " + inMemory);
        final long start = System.currentTimeMillis();
        final NetcdfFile ncfile = inMemory ? NetcdfFile.openInMemory(fileIn) : NetcdfFile.open(fileIn);
        final NetcdfDataset ncd = new NetcdfDataset(ncfile);
        final Formatter errlog = new Formatter();
        final FeatureDataset fd = FeatureDatasetFactoryManager.wrap(FeatureType.ANY_POINT, ncd, null, errlog);
        if (fd == null) {
            return false;
        }
        if (fd instanceof FeatureDatasetPoint) {
            writePointFeatureCollection((FeatureDatasetPoint)fd, fileOut);
            fd.close();
            final long took = System.currentTimeMillis() - start;
            System.out.println(" that took " + (took - start) + " msecs");
            return true;
        }
        return false;
    }
    
    public static int writePointFeatureCollection(final FeatureDatasetPoint pfDataset, final String fileOut) throws IOException {
        PointFeatureCollection pointFeatureCollection = null;
        final List<FeatureCollection> featureCollectionList = pfDataset.getPointFeatureCollectionList();
        for (final FeatureCollection featureCollection : featureCollectionList) {
            if (featureCollection instanceof PointFeatureCollection) {
                pointFeatureCollection = (PointFeatureCollection)featureCollection;
            }
        }
        if (null == pointFeatureCollection) {
            throw new IOException("There is no PointFeatureCollection in  " + pfDataset.getLocation());
        }
        final long start = System.currentTimeMillis();
        final FileOutputStream fos = new FileOutputStream(fileOut);
        final DataOutputStream out = new DataOutputStream(new BufferedOutputStream(fos, 10000));
        WriterCFPointDataset writer = null;
        int count = 0;
        pointFeatureCollection.resetIteration();
        while (pointFeatureCollection.hasNext()) {
            final PointFeature pointFeature = pointFeatureCollection.next();
            final StructureData data = pointFeature.getData();
            if (count == 0) {
                final EarthLocation loc = pointFeature.getLocation();
                final String altUnits = Double.isNaN(loc.getAltitude()) ? null : "meters";
                writer = new WriterCFPointDataset(out, pfDataset.getGlobalAttributes(), altUnits);
                writer.writeHeader(pfDataset.getDataVariables(), -1);
            }
            writer.writeRecord(pointFeature, data);
            ++count;
        }
        writer.finish();
        out.flush();
        out.close();
        final long took = System.currentTimeMillis() - start;
        System.out.printf("Write %d records from %s to %s took %d msecs %n", count, pfDataset.getLocation(), fileOut, took);
        return count;
    }
    
    public static void main2(final String[] args) throws IOException {
        final String location = "R:/testdata/point/netcdf/madis.nc";
        final File file = new File(location);
        rewritePointFeatureDataset(location, "C:/TEMP/" + file.getName(), true);
    }
    
    public static void main(final String[] args) throws IOException {
        final List<PointObVar> dataVars = new ArrayList<PointObVar>();
        dataVars.add(new PointObVar("test1", "units1", "desc1", DataType.CHAR, 4));
        dataVars.add(new PointObVar("test2", "units2", "desc3", DataType.CHAR, 4));
        final FileOutputStream fos = new FileOutputStream("C:/temp/test.out");
        final DataOutputStream out = new DataOutputStream(new BufferedOutputStream(fos, 10000));
        final CFPointObWriter writer = new CFPointObWriter(out, new ArrayList<Attribute>(), "meters", dataVars, 1);
        final double[] dvals = new double[0];
        final String[] svals = { "valu", "value" };
        writer.addPoint(1.0, 2.0, 3.0, new Date(), dvals, svals);
        writer.finish();
    }
    
    private class PointObVarAdapter implements VariableSimpleIF
    {
        PointObVar pov;
        List<Attribute> atts;
        
        PointObVarAdapter(final PointObVar pov) {
            this.atts = new ArrayList<Attribute>(2);
            this.pov = pov;
        }
        
        public String getName() {
            return this.pov.getName();
        }
        
        public String getShortName() {
            return this.pov.getName();
        }
        
        public String getDescription() {
            return this.pov.getDesc();
        }
        
        public String getUnitsString() {
            return this.pov.getUnits();
        }
        
        public int getRank() {
            return (this.pov.getLen() > 1) ? 1 : 0;
        }
        
        public int[] getShape() {
            return (this.pov.getLen() > 1) ? new int[] { this.pov.getLen() } : new int[0];
        }
        
        public List<Dimension> getDimensions() {
            if (this.pov.getLen() > 1) {
                final List<Dimension> dims = new ArrayList<Dimension>(1);
                final String suffix = (this.pov.getDataType() == DataType.STRING || this.pov.getDataType() == DataType.CHAR) ? "_strlen" : "_len";
                dims.add(new Dimension(this.pov.getName() + suffix, this.pov.getLen(), false, false, false));
                return dims;
            }
            return new ArrayList<Dimension>(0);
        }
        
        public DataType getDataType() {
            return (this.pov.getDataType() == DataType.STRING) ? DataType.CHAR : this.pov.getDataType();
        }
        
        public List<Attribute> getAttributes() {
            if (this.atts == null) {
                this.atts = new ArrayList<Attribute>(2);
            }
            if (this.pov.getDesc() != null) {
                this.atts.add(new Attribute("long_name", this.pov.getDesc()));
            }
            if (this.pov.getUnits() != null) {
                this.atts.add(new Attribute("units", this.pov.getUnits()));
            }
            return this.atts;
        }
        
        public Attribute findAttributeIgnoreCase(final String name) {
            for (final Attribute att : this.getAttributes()) {
                if (att.getName().equalsIgnoreCase(name)) {
                    return att;
                }
            }
            return null;
        }
        
        public int compareTo(final VariableSimpleIF o) {
            return -1;
        }
    }
}
