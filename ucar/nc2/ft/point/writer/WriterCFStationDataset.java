// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.writer;

import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.ma2.ArrayStructureW;
import ucar.ma2.StructureData;
import ucar.nc2.ft.PointFeature;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Array;
import ucar.ma2.ArrayObject;
import java.util.Iterator;
import java.util.Collection;
import ucar.nc2.Variable;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.Attribute;
import ucar.ma2.DataType;
import ucar.nc2.units.DateUnit;
import ucar.nc2.VariableSimpleIF;
import ucar.unidata.geoloc.Station;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import ucar.ma2.ArrayInt;
import ucar.ma2.ArrayDouble;
import java.util.HashMap;
import java.util.Date;
import java.util.List;
import ucar.nc2.Dimension;
import java.util.Set;
import ucar.nc2.NetcdfFileWriteable;
import ucar.nc2.units.DateFormatter;

public class WriterCFStationDataset
{
    private static final String recordDimName = "record";
    private static final String stationDimName = "station";
    private static final String latName = "latitude";
    private static final String lonName = "longitude";
    private static final String altName = "altitude";
    private static final String timeName = "time";
    private static final String idName = "station_id";
    private static final String descName = "station_description";
    private static final String wmoName = "wmo_id";
    private static final String stationIndexName = "stationIndex";
    private DateFormatter dateFormatter;
    private int name_strlen;
    private int desc_strlen;
    private int wmo_strlen;
    private NetcdfFileWriteable ncfile;
    private String title;
    private Set<Dimension> dimSet;
    private List<Dimension> stationDims;
    private Date minDate;
    private Date maxDate;
    private boolean useAlt;
    private boolean useWmoId;
    private boolean debug;
    private HashMap<String, Integer> stationMap;
    private int recno;
    private ArrayDouble.D1 timeArray;
    private ArrayInt.D1 parentArray;
    private int[] origin;
    
    public WriterCFStationDataset(final String fileOut, final String title) throws IOException {
        this.dateFormatter = new DateFormatter();
        this.name_strlen = 1;
        this.desc_strlen = 1;
        this.wmo_strlen = 1;
        this.dimSet = new HashSet<Dimension>(20);
        this.stationDims = new ArrayList<Dimension>(1);
        this.minDate = null;
        this.maxDate = null;
        this.useAlt = false;
        this.useWmoId = false;
        this.debug = false;
        this.recno = 0;
        this.timeArray = new ArrayDouble.D1(1);
        this.parentArray = new ArrayInt.D1(1);
        this.origin = new int[1];
        (this.ncfile = NetcdfFileWriteable.createNew(fileOut, false)).setFill(false);
        this.title = title;
    }
    
    public void setLength(final long size) {
        this.ncfile.setLength(size);
    }
    
    public void writeHeader(final List<Station> stns, final List<VariableSimpleIF> vars, final DateUnit timeUnit) throws IOException {
        this.createGlobalAttributes();
        this.createStations(stns);
        this.createObsVariables(timeUnit);
        this.ncfile.addGlobalAttribute("time_coverage_start", this.dateFormatter.toDateTimeStringISO(new Date()));
        this.ncfile.addGlobalAttribute("time_coverage_end", this.dateFormatter.toDateTimeStringISO(new Date()));
        this.createDataVariables(vars);
        this.ncfile.create();
        this.writeStationData(stns);
        if (!(boolean)this.ncfile.sendIospMessage("AddRecordStructure")) {
            throw new IllegalStateException("can't add record variable");
        }
    }
    
    private void createGlobalAttributes() {
        this.ncfile.addGlobalAttribute("Conventions", "CF-1.4");
        this.ncfile.addGlobalAttribute("CF:featureType", "stationTimeSeries");
        this.ncfile.addGlobalAttribute("title", this.title);
        this.ncfile.addGlobalAttribute("desc", "Written by THREDDS/CDM Remote Subset Service");
    }
    
    private void createStations(final List<Station> stnList) throws IOException {
        final int nstns = stnList.size();
        for (int i = 0; i < nstns; ++i) {
            final Station stn = stnList.get(i);
            if (!Double.isNaN(stn.getAltitude())) {
                this.useAlt = true;
            }
            if (stn.getWmoId() != null && stn.getWmoId().trim().length() > 0) {
                this.useWmoId = true;
            }
        }
        for (int i = 0; i < nstns; ++i) {
            final Station station = stnList.get(i);
            this.name_strlen = Math.max(this.name_strlen, station.getName().length());
            this.desc_strlen = Math.max(this.desc_strlen, station.getDescription().length());
            if (this.useWmoId) {
                this.wmo_strlen = Math.max(this.wmo_strlen, station.getWmoId().length());
            }
        }
        final LatLonRect llbb = this.getBoundingBox(stnList);
        this.ncfile.addGlobalAttribute("geospatial_lat_min", Double.toString(llbb.getLowerLeftPoint().getLatitude()));
        this.ncfile.addGlobalAttribute("geospatial_lat_max", Double.toString(llbb.getUpperRightPoint().getLatitude()));
        this.ncfile.addGlobalAttribute("geospatial_lon_min", Double.toString(llbb.getLowerLeftPoint().getLongitude()));
        this.ncfile.addGlobalAttribute("geospatial_lon_max", Double.toString(llbb.getUpperRightPoint().getLongitude()));
        this.ncfile.addUnlimitedDimension("record");
        final Dimension stationDim = this.ncfile.addDimension("station", nstns);
        this.stationDims.add(stationDim);
        Variable v = this.ncfile.addVariable("latitude", DataType.DOUBLE, "station");
        this.ncfile.addVariableAttribute(v, new Attribute("units", "degrees_north"));
        this.ncfile.addVariableAttribute(v, new Attribute("long_name", "station latitude"));
        v = this.ncfile.addVariable("longitude", DataType.DOUBLE, "station");
        this.ncfile.addVariableAttribute(v, new Attribute("units", "degrees_east"));
        this.ncfile.addVariableAttribute(v, new Attribute("long_name", "station longitude"));
        if (this.useAlt) {
            v = this.ncfile.addVariable("altitude", DataType.DOUBLE, "station");
            this.ncfile.addVariableAttribute(v, new Attribute("units", "meters"));
            this.ncfile.addVariableAttribute(v, new Attribute("positive", "up"));
            this.ncfile.addVariableAttribute(v, new Attribute("long_name", "station altitude"));
        }
        v = this.ncfile.addStringVariable("station_id", this.stationDims, this.name_strlen);
        this.ncfile.addVariableAttribute(v, new Attribute("long_name", "station identifier"));
        this.ncfile.addVariableAttribute(v, new Attribute("standard_name", "station_id"));
        v = this.ncfile.addStringVariable("station_description", this.stationDims, this.desc_strlen);
        this.ncfile.addVariableAttribute(v, new Attribute("long_name", "station description"));
        if (this.useWmoId) {
            v = this.ncfile.addStringVariable("wmo_id", this.stationDims, this.wmo_strlen);
            this.ncfile.addVariableAttribute(v, new Attribute("long_name", "station WMO id"));
        }
    }
    
    private void createObsVariables(final DateUnit timeUnit) throws IOException {
        final Variable timeVar = this.ncfile.addVariable("time", DataType.DOUBLE, "record");
        this.ncfile.addVariableAttribute(timeVar, new Attribute("units", timeUnit.getUnitsString()));
        this.ncfile.addVariableAttribute(timeVar, new Attribute("long_name", "time of measurement"));
        final Variable v = this.ncfile.addVariable("stationIndex", DataType.INT, "record");
        this.ncfile.addVariableAttribute(v, new Attribute("long_name", "station index for this observation record"));
        this.ncfile.addVariableAttribute(v, new Attribute("standard_name", "ragged_parentIndex"));
    }
    
    private void createDataVariables(final List<VariableSimpleIF> dataVars) throws IOException {
        final String coordNames = "latitude longitude altitude time";
        for (final VariableSimpleIF var : dataVars) {
            final List<Dimension> dims = var.getDimensions();
            this.dimSet.addAll(dims);
        }
        for (final Dimension d : this.dimSet) {
            if (!d.isUnlimited()) {
                this.ncfile.addDimension(d.getName(), d.getLength(), d.isShared(), false, d.isVariableLength());
            }
        }
        for (final VariableSimpleIF oldVar : dataVars) {
            final List<Dimension> dims = oldVar.getDimensions();
            final StringBuffer dimNames = new StringBuffer("record");
            for (final Dimension d2 : dims) {
                if (!d2.isUnlimited()) {
                    dimNames.append(" ").append(d2.getName());
                }
            }
            final Variable newVar = this.ncfile.addVariable(oldVar.getShortName(), oldVar.getDataType(), dimNames.toString());
            final List<Attribute> atts = oldVar.getAttributes();
            for (final Attribute att : atts) {
                newVar.addAttribute(att);
            }
            newVar.addAttribute(new Attribute("coordinates", coordNames));
        }
    }
    
    private void writeStationData(final List<Station> stnList) throws IOException {
        final int nstns = stnList.size();
        this.stationMap = new HashMap<String, Integer>(2 * nstns);
        if (this.debug) {
            System.out.println("stationMap created");
        }
        final ArrayDouble.D1 latArray = new ArrayDouble.D1(nstns);
        final ArrayDouble.D1 lonArray = new ArrayDouble.D1(nstns);
        final ArrayDouble.D1 altArray = new ArrayDouble.D1(nstns);
        final ArrayObject.D1 idArray = new ArrayObject.D1(String.class, nstns);
        final ArrayObject.D1 descArray = new ArrayObject.D1(String.class, nstns);
        final ArrayObject.D1 wmoArray = new ArrayObject.D1(String.class, nstns);
        for (int i = 0; i < stnList.size(); ++i) {
            final Station stn = stnList.get(i);
            this.stationMap.put(stn.getName(), i);
            latArray.set(i, stn.getLatitude());
            lonArray.set(i, stn.getLongitude());
            if (this.useAlt) {
                altArray.set(i, stn.getAltitude());
            }
            idArray.set(i, stn.getName());
            descArray.set(i, stn.getDescription());
            if (this.useWmoId) {
                wmoArray.set(i, stn.getWmoId());
            }
        }
        try {
            this.ncfile.write("latitude", latArray);
            this.ncfile.write("longitude", lonArray);
            if (this.useAlt) {
                this.ncfile.write("altitude", altArray);
            }
            this.ncfile.writeStringData("station_id", idArray);
            this.ncfile.writeStringData("station_description", descArray);
            if (this.useWmoId) {
                this.ncfile.writeStringData("wmo_id", wmoArray);
            }
        }
        catch (InvalidRangeException e) {
            e.printStackTrace();
            throw new IllegalStateException(e);
        }
    }
    
    private void writeDataFinish() throws IOException {
        if (this.minDate == null) {
            this.minDate = new Date();
        }
        if (this.maxDate == null) {
            this.maxDate = new Date();
        }
        this.ncfile.updateAttribute(null, new Attribute("time_coverage_start", this.dateFormatter.toDateTimeStringISO(this.minDate)));
        this.ncfile.updateAttribute(null, new Attribute("time_coverage_end", this.dateFormatter.toDateTimeStringISO(this.maxDate)));
    }
    
    public void writeRecord(final Station s, final PointFeature sobs, final StructureData sdata) throws IOException {
        this.writeRecord(s.getName(), sobs.getObservationTime(), sobs.getObservationTimeAsDate(), sdata);
    }
    
    public void writeRecord(final String stnName, final double timeCoordValue, final Date obsDate, final StructureData sdata) throws IOException {
        final Integer parentIndex = this.stationMap.get(stnName);
        if (parentIndex == null) {
            throw new RuntimeException("Cant find station " + stnName);
        }
        final ArrayStructureW sArray = new ArrayStructureW(sdata.getStructureMembers(), new int[] { 1 });
        sArray.setStructureData(sdata, 0);
        if (this.minDate == null || this.minDate.after(obsDate)) {
            this.minDate = obsDate;
        }
        if (this.maxDate == null || this.maxDate.before(obsDate)) {
            this.maxDate = obsDate;
        }
        this.timeArray.set(0, timeCoordValue);
        this.parentArray.set(0, parentIndex);
        this.origin[0] = this.recno;
        try {
            this.ncfile.write("record", this.origin, sArray);
            this.ncfile.write("time", this.origin, this.timeArray);
            this.ncfile.write("stationIndex", this.origin, this.parentArray);
        }
        catch (InvalidRangeException e) {
            e.printStackTrace();
            throw new IllegalStateException(e);
        }
        ++this.recno;
    }
    
    public void finish() throws IOException {
        this.writeDataFinish();
        this.ncfile.close();
    }
    
    private LatLonRect getBoundingBox(final List stnList) {
        Station s = stnList.get(0);
        final LatLonPointImpl llpt = new LatLonPointImpl();
        llpt.set(s.getLatitude(), s.getLongitude());
        final LatLonRect rect = new LatLonRect(llpt, 0.001, 0.001);
        for (int i = 1; i < stnList.size(); ++i) {
            s = stnList.get(i);
            llpt.set(s.getLatitude(), s.getLongitude());
            rect.extend(llpt);
        }
        return rect;
    }
}
