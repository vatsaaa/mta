// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.writer;

import ucar.nc2.VariableSimpleIF;
import ucar.ma2.DataType;

public class PointObVar
{
    private String name;
    private String units;
    private String desc;
    private DataType dtype;
    private int len;
    
    public PointObVar() {
    }
    
    public PointObVar(final String name, final String units, final String desc, final DataType dtype, final int len) {
        this.name = name;
        this.units = units;
        this.desc = desc;
        this.dtype = dtype;
        this.len = len;
    }
    
    public PointObVar(final VariableSimpleIF v) {
        this.setName(v.getShortName());
        this.setUnits(v.getUnitsString());
        this.setDesc(v.getDescription());
        this.setDataType(v.getDataType());
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public String getUnits() {
        return this.units;
    }
    
    public void setUnits(final String units) {
        this.units = units;
    }
    
    public String getDesc() {
        return this.desc;
    }
    
    public void setDesc(final String desc) {
        this.desc = desc;
    }
    
    public DataType getDataType() {
        return this.dtype;
    }
    
    public void setDataType(final DataType dtype) {
        this.dtype = dtype;
    }
    
    public int getLen() {
        return this.len;
    }
    
    public void setLen(final int len) {
        this.len = len;
    }
}
