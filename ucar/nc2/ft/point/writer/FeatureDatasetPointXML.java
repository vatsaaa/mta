// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.writer;

import ucar.nc2.Dimension;
import ucar.ma2.Array;
import ucar.nc2.ncml.NcMLReader;
import ucar.nc2.ft.FeatureDataset;
import java.io.FileOutputStream;
import java.io.File;
import ucar.nc2.util.CancelTask;
import ucar.nc2.ft.FeatureDatasetFactoryManager;
import java.util.Formatter;
import ucar.nc2.constants.FeatureType;
import java.util.ArrayList;
import java.util.Date;
import ucar.nc2.units.TimeDuration;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.ma2.DataType;
import org.jdom.Namespace;
import ucar.nc2.ncml.NcMLWriter;
import ucar.nc2.Attribute;
import ucar.nc2.units.DateRange;
import ucar.nc2.units.DateFormatter;
import ucar.nc2.VariableSimpleIF;
import java.util.Collections;
import java.util.Iterator;
import ucar.nc2.ft.FeatureCollection;
import java.util.List;
import org.jdom.Content;
import ucar.unidata.geoloc.Station;
import java.util.Arrays;
import org.jdom.Element;
import ucar.nc2.ft.StationTimeSeriesFeatureCollection;
import ucar.nc2.ft.NestedPointFeatureCollection;
import org.jdom.Document;
import ucar.unidata.geoloc.LatLonRect;
import java.io.IOException;
import java.io.OutputStream;
import org.jdom.output.XMLOutputter;
import org.jdom.output.Format;
import ucar.nc2.ft.FeatureDatasetPoint;

public class FeatureDatasetPointXML
{
    private FeatureDatasetPoint fdp;
    private String path;
    
    public FeatureDatasetPointXML(final FeatureDatasetPoint fdp, final String path) {
        this.fdp = fdp;
        this.path = path;
    }
    
    public String getCapabilities() {
        final XMLOutputter fmt = new XMLOutputter(Format.getPrettyFormat());
        return fmt.outputString(this.getCapabilitiesDocument());
    }
    
    public void getCapabilities(final OutputStream os) throws IOException {
        final XMLOutputter fmt = new XMLOutputter(Format.getPrettyFormat());
        fmt.output(this.getCapabilitiesDocument(), os);
    }
    
    public Document makeStationCollectionDocument(final LatLonRect bb, final String[] names) throws IOException {
        final List<FeatureCollection> list = this.fdp.getPointFeatureCollectionList();
        final FeatureCollection fc = list.get(0);
        if (!(fc instanceof StationTimeSeriesFeatureCollection)) {
            throw new UnsupportedOperationException(fc.getClass().getName() + " not a StationTimeSeriesFeatureCollection");
        }
        final StationTimeSeriesFeatureCollection sobs = (StationTimeSeriesFeatureCollection)fc;
        final Element rootElem = new Element("stationCollection");
        final Document doc = new Document(rootElem);
        List<Station> stations;
        if (bb != null) {
            stations = sobs.getStations(bb);
        }
        else if (names != null) {
            stations = sobs.getStations(Arrays.asList(names));
        }
        else {
            stations = sobs.getStations();
        }
        for (final Station s : stations) {
            final Element sElem = new Element("station");
            sElem.setAttribute("name", s.getName());
            if (s.getWmoId() != null) {
                sElem.setAttribute("wmo_id", s.getWmoId());
            }
            if (s.getDescription() != null && s.getDescription().length() > 0) {
                sElem.addContent(new Element("description").addContent(s.getDescription()));
            }
            sElem.addContent(new Element("longitude").addContent(Double.toString(s.getLongitude())));
            sElem.addContent(new Element("latitide").addContent(Double.toString(s.getLatitude())));
            if (!Double.isNaN(s.getAltitude())) {
                sElem.addContent(new Element("altitude").addContent(Double.toString(s.getAltitude())));
            }
            rootElem.addContent(sElem);
        }
        return doc;
    }
    
    public Document getCapabilitiesDocument() {
        final Element rootElem = new Element("capabilities");
        final Document doc = new Document(rootElem);
        if (null != this.path) {
            rootElem.setAttribute("location", this.path);
            final Element elem = new Element("featureDataset");
            elem.setAttribute("type", this.fdp.getFeatureType().toString().toLowerCase());
            elem.setAttribute("url", this.path + "/" + this.fdp.getFeatureType().toString().toLowerCase());
            rootElem.addContent(elem);
        }
        final List<? extends VariableSimpleIF> vars = this.fdp.getDataVariables();
        Collections.sort(vars);
        for (final VariableSimpleIF v : vars) {
            rootElem.addContent(this.writeVariable(v));
        }
        final LatLonRect bb = this.fdp.getBoundingBox();
        if (bb != null) {
            rootElem.addContent(this.writeBoundingBox(bb));
        }
        final DateRange dateRange = this.fdp.getDateRange();
        if (dateRange != null) {
            final DateFormatter format = new DateFormatter();
            final Element drElem = new Element("TimeSpan");
            drElem.addContent(new Element("begin").addContent(dateRange.getStart().toDateTimeStringISO()));
            drElem.addContent(new Element("end").addContent(dateRange.getEnd().toDateTimeStringISO()));
            if (dateRange.getResolution() != null) {
                drElem.addContent(new Element("resolution").addContent(dateRange.getResolution().toString()));
            }
            rootElem.addContent(drElem);
        }
        final Element elem2 = new Element("AcceptList");
        elem2.addContent(new Element("accept").addContent("csv"));
        elem2.addContent(new Element("accept").addContent("xml"));
        elem2.addContent(new Element("accept").addContent("netcdf"));
        elem2.addContent(new Element("accept").addContent("ncstream"));
        rootElem.addContent(elem2);
        return doc;
    }
    
    private Element writeBoundingBox(final LatLonRect bb) {
        final Element bbElem = new Element("LatLonBox");
        bbElem.addContent(new Element("west").addContent(ucar.unidata.util.Format.dfrac(bb.getLonMin(), 6)));
        bbElem.addContent(new Element("east").addContent(ucar.unidata.util.Format.dfrac(bb.getLonMax(), 6)));
        bbElem.addContent(new Element("south").addContent(ucar.unidata.util.Format.dfrac(bb.getLatMin(), 6)));
        bbElem.addContent(new Element("north").addContent(ucar.unidata.util.Format.dfrac(bb.getLatMax(), 6)));
        return bbElem;
    }
    
    private Element writeVariable(final VariableSimpleIF v) {
        final Element varElem = new Element("variable");
        varElem.setAttribute("name", v.getShortName());
        final DataType dt = v.getDataType();
        if (dt != null) {
            varElem.setAttribute("type", dt.toString());
        }
        for (final Attribute att : v.getAttributes()) {
            varElem.addContent(NcMLWriter.writeAttribute(att, "attribute", null));
        }
        return varElem;
    }
    
    public static LatLonRect getSpatialExtent(final Document doc) throws IOException {
        final Element root = doc.getRootElement();
        final Element latlonBox = root.getChild("LatLonBox");
        if (latlonBox == null) {
            return null;
        }
        final String westS = latlonBox.getChildText("west");
        final String eastS = latlonBox.getChildText("east");
        final String northS = latlonBox.getChildText("north");
        final String southS = latlonBox.getChildText("south");
        if (westS == null || eastS == null || northS == null || southS == null) {
            return null;
        }
        try {
            final double west = Double.parseDouble(westS);
            final double east = Double.parseDouble(eastS);
            final double south = Double.parseDouble(southS);
            final double north = Double.parseDouble(northS);
            return new LatLonRect(new LatLonPointImpl(south, east), new LatLonPointImpl(north, west));
        }
        catch (Exception e) {
            return null;
        }
    }
    
    public static DateRange getTimeSpan(final Document doc) throws IOException {
        final Element root = doc.getRootElement();
        final Element timeSpan = root.getChild("TimeSpan");
        if (timeSpan == null) {
            return null;
        }
        final String beginS = timeSpan.getChildText("begin");
        final String endS = timeSpan.getChildText("end");
        final String resS = timeSpan.getChildText("resolution");
        if (beginS == null || endS == null) {
            return null;
        }
        final DateFormatter format = new DateFormatter();
        try {
            final Date start = format.getISODate(beginS);
            final Date end = format.getISODate(endS);
            final DateRange dr = new DateRange(start, end);
            if (resS != null) {
                dr.setResolution(new TimeDuration(resS));
            }
            return dr;
        }
        catch (Exception e) {
            return null;
        }
    }
    
    public static List<VariableSimpleIF> getDataVariables(final Document doc) throws IOException {
        final Element root = doc.getRootElement();
        final List<VariableSimpleIF> dataVars = new ArrayList<VariableSimpleIF>();
        final List<Element> varElems = (List<Element>)root.getChildren("variable");
        for (final Element varElem : varElems) {
            dataVars.add(new VariableSimple(varElem));
        }
        return dataVars;
    }
    
    public static void main(final String[] args) throws IOException {
        final String location = "D:/datasets/metars/Surface_METAR_20070516_0000.nc";
        final String path = "http://motherlode.ucar.edu:9080/thredds/cdmremote/idd/metar/gempak/collection";
        final FeatureDataset fd = FeatureDatasetFactoryManager.open(FeatureType.ANY_POINT, location, null, new Formatter(System.out));
        final FeatureDatasetPointXML xml = new FeatureDatasetPointXML((FeatureDatasetPoint)fd, path);
        xml.getCapabilities(System.out);
        final File f = new File("C:/TEMP/stationCapabilities.xml");
        final FileOutputStream fos = new FileOutputStream(f);
        xml.getCapabilities(fos);
        fos.close();
    }
    
    private static class VariableSimple implements VariableSimpleIF
    {
        String name;
        String desc;
        String units;
        DataType dt;
        List<Attribute> atts;
        
        VariableSimple(final Element velem) {
            this.name = velem.getAttributeValue("name");
            final String type = velem.getAttributeValue("type");
            this.dt = DataType.getType(type);
            this.atts = new ArrayList<Attribute>();
            final List<Element> attElems = (List<Element>)velem.getChildren("attribute");
            for (final Element attElem : attElems) {
                final String attName = attElem.getAttributeValue("name");
                final Array values = NcMLReader.readAttributeValues(attElem);
                this.atts.add(new Attribute(attName, values));
            }
            for (final Attribute att : this.atts) {
                if (att.getName().equals("units")) {
                    this.units = att.getStringValue();
                }
                if (att.getName().equals("long_name")) {
                    this.desc = att.getStringValue();
                }
                if (this.desc == null && att.getName().equals("description")) {
                    this.desc = att.getStringValue();
                }
                if (this.desc == null && att.getName().equals("standard_name")) {
                    this.desc = att.getStringValue();
                }
            }
        }
        
        public String getName() {
            return this.name;
        }
        
        public String getShortName() {
            return this.name;
        }
        
        public String getDescription() {
            return this.desc;
        }
        
        public String getUnitsString() {
            return this.units;
        }
        
        public int getRank() {
            return 0;
        }
        
        public int[] getShape() {
            return new int[0];
        }
        
        public List<Dimension> getDimensions() {
            return null;
        }
        
        public DataType getDataType() {
            return this.dt;
        }
        
        public List<Attribute> getAttributes() {
            return this.atts;
        }
        
        public Attribute findAttributeIgnoreCase(final String name) {
            for (final Attribute att : this.atts) {
                if (att.getName().equalsIgnoreCase(name)) {
                    return att;
                }
            }
            return null;
        }
        
        public int compareTo(final VariableSimpleIF o) {
            return this.name.compareTo(o.getName());
        }
    }
}
