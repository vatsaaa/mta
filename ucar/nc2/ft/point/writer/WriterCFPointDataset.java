// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point.writer;

import ucar.nc2.iosp.netcdf3.N3outputStreamWriter;
import java.io.File;
import ucar.nc2.dt.DataIterator;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dt.TypedDatasetFactory;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.dt.PointObsDataset;
import ucar.nc2.NetcdfFile;
import ucar.nc2.dataset.NetcdfDataset;
import java.io.OutputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import ucar.nc2.ft.PointFeatureCollection;
import ucar.nc2.ft.FeatureCollection;
import ucar.nc2.ft.FeatureDatasetPoint;
import ucar.nc2.dt.PointObsDatatype;
import ucar.unidata.geoloc.EarthLocation;
import ucar.ma2.StructureData;
import ucar.nc2.ft.PointFeature;
import ucar.ma2.ArrayInt;
import ucar.ma2.ArrayChar;
import ucar.ma2.Array;
import ucar.ma2.ArrayDouble;
import java.util.Date;
import java.util.Collection;
import ucar.nc2.dataset.conv.CF1Convention;
import ucar.ma2.DataType;
import java.util.Iterator;
import ucar.nc2.constants.CF;
import ucar.nc2.Group;
import java.io.IOException;
import ucar.nc2.VariableSimpleIF;
import java.util.ArrayList;
import java.util.HashSet;
import java.io.DataOutputStream;
import ucar.nc2.Variable;
import ucar.nc2.Dimension;
import java.util.Set;
import ucar.nc2.Attribute;
import java.util.List;

public class WriterCFPointDataset
{
    private static final String recordDimName = "record";
    private static final String latName = "latitude";
    private static final String lonName = "longitude";
    private static final String altName = "altitude";
    private static final String timeName = "time";
    private NetcdfFileStream ncfileOut;
    private List<Attribute> globalAtts;
    private String altUnits;
    private Set<Dimension> dimSet;
    private List<Variable> recordVars;
    private boolean useAlt;
    private boolean debug;
    
    public WriterCFPointDataset(final DataOutputStream stream, final List<Attribute> globalAtts, final String altUnits) {
        this.dimSet = new HashSet<Dimension>();
        this.recordVars = new ArrayList<Variable>();
        this.useAlt = false;
        this.debug = false;
        this.ncfileOut = new NetcdfFileStream(stream);
        this.globalAtts = globalAtts;
        this.altUnits = altUnits;
        this.useAlt = (altUnits != null);
    }
    
    public void writeHeader(final List<? extends VariableSimpleIF> vars, final int numrec) throws IOException {
        this.createGlobalAttributes();
        this.createRecordVariables(vars);
        this.ncfileOut.finish();
        this.ncfileOut.writeHeader(numrec);
    }
    
    private void createGlobalAttributes() {
        if (this.globalAtts != null) {
            for (final Attribute att : this.globalAtts) {
                if (att.getName().equalsIgnoreCase("cdm_data_type")) {
                    continue;
                }
                if (att.getName().equalsIgnoreCase("cdm_datatype")) {
                    continue;
                }
                if (att.getName().equalsIgnoreCase("thredds_data_type")) {
                    continue;
                }
                this.ncfileOut.addAttribute(null, att);
            }
        }
        this.ncfileOut.addAttribute(null, new Attribute("Conventions", "CF-1"));
        this.ncfileOut.addAttribute(null, new Attribute("CF:featureType", CF.FeatureType.point.name()));
    }
    
    private void createRecordVariables(final List<? extends VariableSimpleIF> dataVars) {
        this.ncfileOut.addDimension(null, new Dimension("record", 0, true, true, false));
        final Variable timeVar = this.ncfileOut.addVariable(null, "time", DataType.INT, "record");
        timeVar.addAttribute(new Attribute("units", "secs since 1970-01-01 00:00:00"));
        timeVar.addAttribute(new Attribute("long_name", "date/time of observation"));
        this.recordVars.add(timeVar);
        final Variable latVar = this.ncfileOut.addVariable(null, "latitude", DataType.DOUBLE, "record");
        latVar.addAttribute(new Attribute("units", "degrees_north"));
        latVar.addAttribute(new Attribute("long_name", "latitude of observation"));
        latVar.addAttribute(new Attribute("standard_name", "latitude"));
        this.recordVars.add(latVar);
        final Variable lonVar = this.ncfileOut.addVariable(null, "longitude", DataType.DOUBLE, "record");
        lonVar.addAttribute(new Attribute("units", "degrees_east"));
        lonVar.addAttribute(new Attribute("long_name", "longitude of observation"));
        lonVar.addAttribute(new Attribute("standard_name", "longitude"));
        this.recordVars.add(lonVar);
        if (this.useAlt) {
            final Variable altVar = this.ncfileOut.addVariable(null, "altitude", DataType.DOUBLE, "record");
            altVar.addAttribute(new Attribute("units", this.altUnits));
            altVar.addAttribute(new Attribute("long_name", "altitude of observation"));
            altVar.addAttribute(new Attribute("standard_name", "longitude"));
            altVar.addAttribute(new Attribute("positive", CF1Convention.getZisPositive("altitude", this.altUnits)));
            this.recordVars.add(altVar);
        }
        String coordinates = "time latitude longitude";
        if (this.useAlt) {
            coordinates = coordinates + " " + "altitude";
        }
        final Attribute coordAtt = new Attribute("coordinates", coordinates);
        for (final VariableSimpleIF var : dataVars) {
            final List<Dimension> dims = var.getDimensions();
            this.dimSet.addAll(dims);
        }
        for (final Dimension d : this.dimSet) {
            if (this.isExtraDimension(d)) {
                this.ncfileOut.addDimension(null, new Dimension(d.getName(), d.getLength(), true, false, d.isVariableLength()));
            }
        }
        for (final VariableSimpleIF oldVar : dataVars) {
            if (this.ncfileOut.findVariable(oldVar.getShortName()) != null) {
                continue;
            }
            final List<Dimension> dims = oldVar.getDimensions();
            final StringBuffer dimNames = new StringBuffer("record");
            for (final Dimension d2 : dims) {
                if (this.isExtraDimension(d2)) {
                    dimNames.append(" ").append(d2.getName());
                }
            }
            final Variable newVar = this.ncfileOut.addVariable(null, oldVar.getShortName(), oldVar.getDataType(), dimNames.toString());
            this.recordVars.add(newVar);
            final List<Attribute> atts = oldVar.getAttributes();
            for (final Attribute att : atts) {
                newVar.addAttribute(att);
            }
            newVar.addAttribute(coordAtt);
        }
    }
    
    private boolean isExtraDimension(final Dimension d) {
        return !d.isUnlimited() && !d.getName().equalsIgnoreCase("time");
    }
    
    public void writeRecord(final double lat, final double lon, final double alt, final Date time, final double[] vals, final String[] svals) throws IOException {
        int count = this.writeCoordinates(lat, lon, alt, time);
        for (final double val : vals) {
            final ArrayDouble.D0 data = new ArrayDouble.D0();
            data.set(val);
            final Variable v = this.recordVars.get(count++);
            v.setCachedData(data, false);
        }
        for (final String sval : svals) {
            final Variable v = this.recordVars.get(count++);
            final int strlen = v.getShape(1);
            final ArrayChar data2 = ArrayChar.makeFromString(sval, strlen);
            v.setCachedData(data2, false);
        }
        this.ncfileOut.writeRecordData(this.recordVars);
    }
    
    private int writeCoordinates(final double lat, final double lon, final double alt, final Date time) {
        int count = 0;
        final ArrayInt.D0 tdata = new ArrayInt.D0();
        final int secs = (int)(time.getTime() / 1000L);
        tdata.set(secs);
        Variable v = this.recordVars.get(count++);
        v.setCachedData(tdata, false);
        final ArrayDouble.D0 latData = new ArrayDouble.D0();
        latData.set(lat);
        v = this.recordVars.get(count++);
        v.setCachedData(latData, false);
        final ArrayDouble.D0 lonData = new ArrayDouble.D0();
        lonData.set(lon);
        v = this.recordVars.get(count++);
        v.setCachedData(lonData, false);
        if (this.useAlt) {
            final ArrayDouble.D0 altData = new ArrayDouble.D0();
            altData.set(alt);
            v = this.recordVars.get(count++);
            v.setCachedData(altData, false);
        }
        return count;
    }
    
    public void writeRecord(final PointFeature pf, final StructureData sdata) throws IOException {
        if (this.debug) {
            System.out.println("PointFeature= " + pf);
        }
        final EarthLocation loc = pf.getLocation();
        int i;
        for (int count = i = this.writeCoordinates(loc.getLatitude(), loc.getLongitude(), loc.getAltitude(), pf.getObservationTimeAsDate()); i < this.recordVars.size(); ++i) {
            final Variable v = this.recordVars.get(i);
            v.setCachedData(sdata.getArray(v.getShortName()), false);
        }
        this.ncfileOut.writeRecordData(this.recordVars);
    }
    
    public void writeRecord(final PointObsDatatype pobs, final StructureData sdata) throws IOException {
        if (this.debug) {
            System.out.println("pobs= " + pobs);
        }
        final EarthLocation loc = pobs.getLocation();
        int i;
        for (int count = i = this.writeCoordinates(loc.getLatitude(), loc.getLongitude(), loc.getAltitude(), pobs.getObservationTimeAsDate()); i < this.recordVars.size(); ++i) {
            final Variable v = this.recordVars.get(i);
            if (this.debug) {
                System.out.println(" var= " + v.getShortName());
            }
            v.setCachedData(sdata.getArray(v.getShortName()), false);
        }
        this.ncfileOut.writeRecordData(this.recordVars);
    }
    
    public void finish() throws IOException {
        this.ncfileOut.close();
        this.ncfileOut.stream.flush();
        this.ncfileOut.stream.close();
    }
    
    public static int writePointFeatureCollection(final FeatureDatasetPoint pfDataset, final String fileOut) throws IOException {
        PointFeatureCollection pointFeatureCollection = null;
        final List<FeatureCollection> featureCollectionList = pfDataset.getPointFeatureCollectionList();
        for (final FeatureCollection featureCollection : featureCollectionList) {
            if (featureCollection instanceof PointFeatureCollection) {
                pointFeatureCollection = (PointFeatureCollection)featureCollection;
            }
        }
        if (null == pointFeatureCollection) {
            throw new IOException("There is no PointFeatureCollection in  " + pfDataset.getLocation());
        }
        final long start = System.currentTimeMillis();
        final FileOutputStream fos = new FileOutputStream(fileOut);
        final DataOutputStream out = new DataOutputStream(new BufferedOutputStream(fos, 10000));
        WriterCFPointDataset writer = null;
        final List<VariableSimpleIF> dataVars = new ArrayList<VariableSimpleIF>();
        final NetcdfFile ncfile = pfDataset.getNetcdfFile();
        if (ncfile == null || !(ncfile instanceof NetcdfDataset)) {
            dataVars.addAll(pfDataset.getDataVariables());
        }
        else {
            final NetcdfDataset ncd = (NetcdfDataset)ncfile;
            for (final VariableSimpleIF vs : pfDataset.getDataVariables()) {
                if (ncd.findCoordinateAxis(vs.getName()) == null) {
                    dataVars.add(vs);
                }
            }
        }
        int count = 0;
        pointFeatureCollection.resetIteration();
        while (pointFeatureCollection.hasNext()) {
            final PointFeature pointFeature = pointFeatureCollection.next();
            final StructureData data = pointFeature.getData();
            if (count == 0) {
                final EarthLocation loc = pointFeature.getLocation();
                final String altUnits = Double.isNaN(loc.getAltitude()) ? null : "meters";
                writer = new WriterCFPointDataset(out, pfDataset.getGlobalAttributes(), altUnits);
                writer.writeHeader(dataVars, -1);
            }
            writer.writeRecord(pointFeature, data);
            ++count;
        }
        writer.finish();
        out.flush();
        out.close();
        final long took = System.currentTimeMillis() - start;
        System.out.printf("Write %d records from %s to %s took %d msecs %n", count, pfDataset.getLocation(), fileOut, took);
        return count;
    }
    
    public static void rewritePointObsDataset(final String fileIn, final String fileOut, final boolean inMemory) throws IOException {
        System.out.println("Rewrite .nc files from " + fileIn + " to " + fileOut + "inMem= " + inMemory);
        final long start = System.currentTimeMillis();
        final NetcdfFile ncfile = inMemory ? NetcdfFile.openInMemory(fileIn) : NetcdfFile.open(fileIn);
        final NetcdfDataset ncd = new NetcdfDataset(ncfile);
        final StringBuilder errlog = new StringBuilder();
        final PointObsDataset pobsDataset = (PointObsDataset)TypedDatasetFactory.open(FeatureType.POINT, ncd, null, errlog);
        final FileOutputStream fos = new FileOutputStream(fileOut);
        final DataOutputStream out = new DataOutputStream(fos);
        WriterCFPointDataset writer = null;
        final DataIterator iter = pobsDataset.getDataIterator(1000000);
        while (iter.hasNext()) {
            final PointObsDatatype pobsData = (PointObsDatatype)iter.nextData();
            final StructureData sdata = pobsData.getData();
            if (writer == null) {
                final EarthLocation loc = pobsData.getLocation();
                final String altUnits = Double.isNaN(loc.getAltitude()) ? null : "meters";
                writer = new WriterCFPointDataset(out, ncfile.getGlobalAttributes(), altUnits);
                writer.writeHeader(pobsDataset.getDataVariables(), -1);
            }
            writer.writeRecord(pobsData, sdata);
        }
        writer.finish();
        final long took = System.currentTimeMillis() - start;
        System.out.println("Rewrite " + fileIn + " to " + fileOut + " took = " + took);
    }
    
    public static void main(final String[] args) throws IOException {
        final String location = "R:/testdata/point/netcdf/02092412_metar.nc";
        final File file = new File(location);
        rewritePointObsDataset(location, "C:/TEMP/" + file.getName(), true);
    }
    
    private class NetcdfFileStream extends NetcdfFile
    {
        N3outputStreamWriter swriter;
        DataOutputStream stream;
        
        NetcdfFileStream(final DataOutputStream stream) {
            this.stream = stream;
            this.swriter = new N3outputStreamWriter(this);
        }
        
        void writeHeader(final int numrec) throws IOException {
            this.swriter.writeHeader(this.stream, numrec);
        }
        
        void writeNonRecordData(final String varName, final Array data) throws IOException {
            this.swriter.writeNonRecordData(this.findVariable(varName), this.stream, data);
        }
        
        void writeRecordData(final List<Variable> varList) throws IOException {
            this.swriter.writeRecordData(this.stream, varList);
        }
    }
}
