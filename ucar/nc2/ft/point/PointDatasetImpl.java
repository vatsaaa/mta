// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft.point;

import java.util.Formatter;
import java.io.IOException;
import java.util.Iterator;
import ucar.nc2.ft.StationTimeSeriesFeatureCollection;
import ucar.nc2.ft.PointFeatureCollection;
import java.util.ArrayList;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.units.DateRange;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.ft.FeatureCollection;
import java.util.List;
import ucar.nc2.ft.FeatureDatasetPoint;
import ucar.nc2.ft.FeatureDatasetImpl;

public class PointDatasetImpl extends FeatureDatasetImpl implements FeatureDatasetPoint
{
    protected List<FeatureCollection> collectionList;
    protected FeatureType featureType;
    
    protected PointDatasetImpl(final FeatureType featureType) {
        this.featureType = featureType;
    }
    
    protected PointDatasetImpl(final PointDatasetImpl from, final LatLonRect filter_bb, final DateRange filter_date) {
        super(from);
        this.collectionList = from.collectionList;
        this.featureType = from.featureType;
        if (filter_bb == null) {
            this.boundingBox = from.boundingBox;
        }
        else {
            this.boundingBox = ((from.boundingBox == null) ? filter_bb : from.boundingBox.intersect(filter_bb));
        }
        if (filter_date == null) {
            this.dateRange = from.dateRange;
        }
        else {
            this.dateRange = ((from.dateRange == null) ? filter_date : from.dateRange.intersect(filter_date));
        }
    }
    
    public PointDatasetImpl(final NetcdfDataset ds, final FeatureType featureType) {
        super(ds);
        this.featureType = featureType;
    }
    
    protected void setPointFeatureCollection(final List<FeatureCollection> collectionList) {
        this.collectionList = collectionList;
    }
    
    protected void setPointFeatureCollection(final FeatureCollection collection) {
        (this.collectionList = new ArrayList<FeatureCollection>(1)).add(collection);
    }
    
    public FeatureType getFeatureType() {
        return this.featureType;
    }
    
    protected void setFeatureType(final FeatureType ftype) {
        this.featureType = ftype;
    }
    
    public List<FeatureCollection> getPointFeatureCollectionList() {
        return this.collectionList;
    }
    
    public void calcBounds() throws IOException {
        if (this.boundingBox != null && this.dateRange != null) {
            return;
        }
        LatLonRect bb = null;
        DateRange dates = null;
        for (final FeatureCollection fc : this.collectionList) {
            if (fc instanceof PointFeatureCollection) {
                final PointFeatureCollection pfc = (PointFeatureCollection)fc;
                pfc.calcBounds();
                if (bb == null) {
                    bb = pfc.getBoundingBox();
                }
                else {
                    bb.extend(pfc.getBoundingBox());
                }
                if (dates == null) {
                    dates = pfc.getDateRange();
                }
                else {
                    dates.extend(pfc.getDateRange());
                }
            }
            else {
                if (!(fc instanceof StationTimeSeriesFeatureCollection)) {
                    continue;
                }
                final StationTimeSeriesFeatureCollection sc = (StationTimeSeriesFeatureCollection)fc;
                if (bb == null) {
                    bb = sc.getBoundingBox();
                }
                else {
                    bb.extend(sc.getBoundingBox());
                }
                final PointFeatureCollection pfc2 = sc.flatten(null, null);
                pfc2.calcBounds();
                if (dates == null) {
                    dates = pfc2.getDateRange();
                }
                else {
                    dates.extend(pfc2.getDateRange());
                }
            }
        }
        if (this.boundingBox == null) {
            this.boundingBox = bb;
        }
        if (this.dateRange == null) {
            this.dateRange = dates;
        }
    }
    
    @Override
    public void getDetailInfo(final Formatter sf) {
        super.getDetailInfo(sf);
        int count = 0;
        for (final FeatureCollection fc : this.collectionList) {
            sf.format("%nFeatureCollection %d %n", count++);
            if (fc instanceof PointFeatureCollection) {
                final PointFeatureCollection pfc = (PointFeatureCollection)fc;
                sf.format(" %s %s\n", pfc.getCollectionFeatureType(), pfc.getName());
                sf.format("   npts = %d %n", pfc.size());
                sf.format("     bb = %s %n", (pfc.getBoundingBox() == null) ? "" : pfc.getBoundingBox().toString2());
                sf.format("  dates = %s %n", (pfc.getDateRange() == null) ? "" : pfc.getDateRange().toString());
            }
            else {
                if (!(fc instanceof StationTimeSeriesFeatureCollection)) {
                    continue;
                }
                final StationTimeSeriesFeatureCollection npfc = (StationTimeSeriesFeatureCollection)fc;
                sf.format(" %s %s\n", npfc.getCollectionFeatureType(), npfc.getName());
                sf.format("   npts = %d %n", npfc.size());
                sf.format("     bb = %s %n", (npfc.getBoundingBox() == null) ? "" : npfc.getBoundingBox().toString2());
            }
        }
    }
}
