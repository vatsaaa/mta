// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft;

import ucar.nc2.units.DateRange;
import ucar.unidata.geoloc.LatLonRect;
import java.io.IOException;

public interface PointFeatureIterator
{
    boolean hasNext() throws IOException;
    
    PointFeature next() throws IOException;
    
    void finish();
    
    void setBufferSize(final int p0);
    
    void setCalculateBounds(final PointFeatureCollection p0);
    
    LatLonRect getBoundingBox();
    
    DateRange getDateRange();
    
    int getCount();
    
    public interface Filter
    {
        boolean filter(final PointFeature p0);
    }
}
