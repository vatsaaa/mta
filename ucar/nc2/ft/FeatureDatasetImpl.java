// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ft;

import java.io.IOException;
import ucar.nc2.util.cache.FileCacheable;
import java.util.Date;
import ucar.nc2.units.DateFormatter;
import ucar.nc2.Attribute;
import ucar.nc2.NetcdfFile;
import java.util.Iterator;
import ucar.nc2.Variable;
import java.util.Collection;
import java.util.ArrayList;
import ucar.nc2.util.cache.FileCache;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.units.DateRange;
import java.util.Formatter;
import ucar.nc2.VariableSimpleIF;
import java.util.List;
import ucar.nc2.dataset.NetcdfDataset;

public abstract class FeatureDatasetImpl implements FeatureDataset
{
    protected NetcdfDataset ncfile;
    protected String title;
    protected String desc;
    protected String location;
    protected List<VariableSimpleIF> dataVariables;
    protected Formatter parseInfo;
    protected DateRange dateRange;
    protected LatLonRect boundingBox;
    protected FileCache fileCache;
    
    protected FeatureDatasetImpl(final FeatureDatasetImpl from) {
        this.parseInfo = new Formatter();
        this.ncfile = from.ncfile;
        this.title = from.title;
        this.desc = from.desc;
        this.location = from.location;
        this.dataVariables = new ArrayList<VariableSimpleIF>(from.dataVariables);
        this.parseInfo = new Formatter();
        final String fromInfo = from.parseInfo.toString().trim();
        if (fromInfo.length() > 0) {
            this.parseInfo.format("%s\n", fromInfo);
        }
        this.parseInfo.format("Subsetted from original\n", new Object[0]);
    }
    
    public FeatureDatasetImpl() {
        this.parseInfo = new Formatter();
    }
    
    public FeatureDatasetImpl(final String title, final String description, final String location) {
        this.parseInfo = new Formatter();
        this.title = title;
        this.desc = description;
        this.location = location;
    }
    
    public FeatureDatasetImpl(final NetcdfDataset ncfile) {
        this.parseInfo = new Formatter();
        this.ncfile = ncfile;
        this.location = ncfile.getLocation();
        this.title = ncfile.getTitle();
        if (this.title == null) {
            this.title = ncfile.findAttValueIgnoreCase(null, "title", null);
        }
        if (this.desc == null) {
            this.desc = ncfile.findAttValueIgnoreCase(null, "description", null);
        }
    }
    
    protected void setTitle(final String title) {
        this.title = title;
    }
    
    protected void setDescription(final String desc) {
        this.desc = desc;
    }
    
    protected void setLocationURI(final String location) {
        this.location = location;
    }
    
    protected void setDateRange(final DateRange dateRange) {
        this.dateRange = dateRange;
    }
    
    protected void setBoundingBox(final LatLonRect boundingBox) {
        this.boundingBox = boundingBox;
    }
    
    protected void removeDataVariable(final String varName) {
        if (this.dataVariables == null) {
            return;
        }
        final Iterator iter = this.dataVariables.iterator();
        while (iter.hasNext()) {
            final VariableSimpleIF v = iter.next();
            if (v.getName().equals(varName)) {
                iter.remove();
            }
        }
    }
    
    public NetcdfFile getNetcdfFile() {
        return this.ncfile;
    }
    
    public String getTitle() {
        return this.title;
    }
    
    public String getDescription() {
        return this.desc;
    }
    
    public String getLocation() {
        return this.location;
    }
    
    public List<Attribute> getGlobalAttributes() {
        if (this.ncfile == null) {
            return new ArrayList<Attribute>();
        }
        return this.ncfile.getGlobalAttributes();
    }
    
    public Attribute findGlobalAttributeIgnoreCase(final String name) {
        if (this.ncfile == null) {
            return null;
        }
        return this.ncfile.findGlobalAttributeIgnoreCase(name);
    }
    
    public void getDetailInfo(final Formatter sf) {
        final DateFormatter formatter = new DateFormatter();
        sf.format("FeatureDataset on location= %s\n", this.getLocation());
        sf.format("  featureType= %s\n", this.getFeatureType());
        sf.format("  title= %s\n", this.getTitle());
        sf.format("  desc= %s\n", this.getDescription());
        sf.format("  range= %s\n", this.getDateRange());
        sf.format("  start= %s\n", formatter.toDateTimeString(this.getStartDate()));
        sf.format("  end  = %s\n", formatter.toDateTimeString(this.getEndDate()));
        final LatLonRect bb = this.getBoundingBox();
        sf.format("  bb   = %s\n", bb);
        if (bb != null) {
            sf.format("  bb   = %s\n", this.getBoundingBox().toString2());
        }
        sf.format("  has netcdf = %b\n", this.getNetcdfFile() != null);
        final List<Attribute> ga = this.getGlobalAttributes();
        if (ga.size() > 0) {
            sf.format("  Attributes\n", new Object[0]);
            for (final Attribute a : ga) {
                sf.format("    %s\n", a);
            }
        }
        final List<VariableSimpleIF> vars = this.getDataVariables();
        sf.format("  Data Variables (%d)\n", vars.size());
        for (final VariableSimpleIF v : vars) {
            sf.format("    name='%s' desc='%s' units=%s' type='%s'\n", v.getName(), v.getDescription(), v.getUnitsString(), v.getDataType());
        }
        sf.format("\nparseInfo=\n%s\n", this.parseInfo);
    }
    
    public DateRange getDateRange() {
        return this.dateRange;
    }
    
    public Date getStartDate() {
        return (this.dateRange == null) ? null : this.dateRange.getStart().getDate();
    }
    
    public Date getEndDate() {
        return (this.dateRange == null) ? null : this.dateRange.getEnd().getDate();
    }
    
    public LatLonRect getBoundingBox() {
        return this.boundingBox;
    }
    
    public List<VariableSimpleIF> getDataVariables() {
        return (this.dataVariables == null) ? new ArrayList<VariableSimpleIF>() : this.dataVariables;
    }
    
    public VariableSimpleIF getDataVariable(final String shortName) {
        for (final VariableSimpleIF s : this.getDataVariables()) {
            final String ss = s.getShortName();
            if (shortName.equals(ss)) {
                return s;
            }
        }
        return null;
    }
    
    public String getImplementationName() {
        return this.getClass().getName();
    }
    
    public synchronized void close() throws IOException {
        if (this.fileCache != null) {
            this.fileCache.release(this);
        }
        else {
            try {
                if (this.ncfile != null) {
                    this.ncfile.close();
                }
            }
            finally {
                this.ncfile = null;
            }
        }
    }
    
    public boolean sync() throws IOException {
        return false;
    }
    
    public void setFileCache(final FileCache fileCache) {
        this.fileCache = fileCache;
    }
}
