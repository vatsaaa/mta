// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2;

import ucar.ma2.Range;
import java.util.List;
import ucar.ma2.InvalidRangeException;
import java.util.StringTokenizer;
import ucar.ma2.Section;

public class ParsedSectionSpec
{
    public Variable v;
    public Section section;
    public ParsedSectionSpec child;
    private static boolean debugSelector;
    
    private ParsedSectionSpec(final Variable v, final Section section) {
        this.v = v;
        this.section = section;
        this.child = null;
    }
    
    public static ParsedSectionSpec parseVariableSection(final NetcdfFile ncfile, final String variableSection) throws InvalidRangeException {
        final StringTokenizer stoke = new StringTokenizer(variableSection, ".");
        String selector = stoke.nextToken();
        if (selector == null) {
            throw new IllegalArgumentException("empty sectionSpec = " + variableSection);
        }
        ParsedSectionSpec current;
        final ParsedSectionSpec outerV = current = parseVariableSelector(ncfile, selector);
        while (stoke.hasMoreTokens()) {
            selector = stoke.nextToken();
            current.child = parseVariableSelector(current.v, selector);
            current = current.child;
        }
        return outerV;
    }
    
    private static ParsedSectionSpec parseVariableSelector(final Object parent, String selector) throws InvalidRangeException {
        String indexSelect = null;
        selector = NetcdfFile.unescapeName(selector);
        final int pos1 = selector.indexOf(40);
        String varName;
        if (pos1 < 0) {
            varName = selector;
        }
        else {
            varName = selector.substring(0, pos1);
            final int pos2 = selector.indexOf(41);
            indexSelect = selector.substring(pos1, pos2);
        }
        if (ParsedSectionSpec.debugSelector) {
            System.out.println(" parseVariableSection <" + selector + "> = <" + varName + ">, <" + indexSelect + ">");
        }
        Variable v = null;
        if (parent instanceof NetcdfFile) {
            final NetcdfFile ncfile = (NetcdfFile)parent;
            v = ncfile.findVariable(varName);
        }
        else if (parent instanceof Structure) {
            final Structure s = (Structure)parent;
            v = s.findVariable(varName);
        }
        if (v == null) {
            throw new IllegalArgumentException(" cant find variable: " + varName + " in selector=" + selector);
        }
        Section section;
        if (indexSelect != null) {
            section = new Section(indexSelect);
            section.setDefaults(v.getShape());
        }
        else {
            section = new Section(v.getShape());
        }
        return new ParsedSectionSpec(v, section);
    }
    
    public static String makeSectionSpecString(final Variable v, final List<Range> ranges) throws InvalidRangeException {
        final StringBuilder sb = new StringBuilder();
        makeSpec(sb, v, ranges);
        return sb.toString();
    }
    
    private static List<Range> makeSpec(final StringBuilder sb, final Variable v, List<Range> orgRanges) throws InvalidRangeException {
        if (v.isMemberOfStructure()) {
            orgRanges = makeSpec(sb, v.getParentStructure(), orgRanges);
            sb.append('.');
        }
        final List<Range> ranges = (orgRanges == null) ? v.getRanges() : orgRanges;
        sb.append(v.isMemberOfStructure() ? NetcdfFile.escapeName(v.getShortName()) : v.getNameEscaped());
        if (!v.isVariableLength() && !v.isScalar()) {
            sb.append('(');
            for (int count = 0; count < v.getRank(); ++count) {
                Range r = ranges.get(count);
                if (r == null) {
                    r = new Range(0, v.getDimension(count).getLength());
                }
                if (count > 0) {
                    sb.append(", ");
                }
                sb.append(r.first());
                sb.append(':');
                sb.append(r.last());
                sb.append(':');
                sb.append(r.stride());
            }
            sb.append(')');
        }
        return (orgRanges == null) ? null : ranges.subList(v.getRank(), ranges.size());
    }
    
    static {
        ParsedSectionSpec.debugSelector = false;
    }
}
