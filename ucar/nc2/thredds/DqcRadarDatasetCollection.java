// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.thredds;

import ucar.unidata.util.DatedThing;
import java.util.TimeZone;
import java.text.SimpleDateFormat;
import ucar.unidata.util.DateSelection;
import ucar.nc2.dt.DataIterator;
import thredds.catalog.InvCatalogImpl;
import thredds.catalog.InvCatalogFactory;
import java.net.URISyntaxException;
import ucar.unidata.util.DateUtil;
import thredds.catalog.InvAccess;
import java.net.URI;
import ucar.nc2.dt.RadialDatasetSweep;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.util.CancelTask;
import java.util.Iterator;
import ucar.unidata.util.Product;
import ucar.nc2.units.DateUnit;
import java.util.Date;
import thredds.catalog.query.Choice;
import thredds.catalog.query.Station;
import java.util.List;
import java.util.ArrayList;
import thredds.catalog.query.Selector;
import thredds.catalog.query.DqcFactory;
import java.io.IOException;
import thredds.catalog.InvDataset;
import java.util.HashMap;
import thredds.catalog.query.SelectGeoRegion;
import thredds.catalog.query.SelectList;
import thredds.catalog.query.SelectStation;
import thredds.catalog.query.SelectService;
import thredds.catalog.query.QueryCapability;
import ucar.nc2.dt.radial.StationRadarCollectionImpl;

public class DqcRadarDatasetCollection extends StationRadarCollectionImpl
{
    private QueryCapability dqc;
    private SelectService selService;
    private SelectStation selStation;
    private SelectList selTime;
    private SelectGeoRegion selRegion;
    private SelectService.ServiceChoice service;
    private HashMap dqcStations;
    private boolean debugQuery;
    
    public static DqcRadarDatasetCollection factory(final InvDataset ds, final String dqc_location, final StringBuffer errlog) throws IOException {
        return factory(ds.getDocumentation("summary"), dqc_location, errlog);
    }
    
    public static DqcRadarDatasetCollection factory(final String desc, final String dqc_location, final StringBuffer errlog) throws IOException {
        final DqcFactory dqcFactory = new DqcFactory(true);
        final QueryCapability dqc = dqcFactory.readXML(dqc_location + "?returns=dqc");
        if (dqc.hasFatalError()) {
            errlog.append(dqc.getErrorMessages());
            return null;
        }
        SelectStation selStation = null;
        SelectList selTime = null;
        SelectService selService = null;
        final ArrayList selectors = dqc.getSelectors();
        for (int i = 0; i < selectors.size(); ++i) {
            final Selector s = selectors.get(i);
            if (s instanceof SelectStation) {
                selStation = (SelectStation)s;
            }
            if (s instanceof SelectList) {
                selTime = (SelectList)s;
            }
            if (s instanceof SelectService) {
                selService = (SelectService)s;
            }
        }
        if (selService == null) {
            errlog.append("DqcStationaryRadarDataset must have Service selector");
            return null;
        }
        if (selStation == null) {
            errlog.append("DqcStationaryRadarDataset must have Station selector");
            return null;
        }
        if (selTime == null) {
            errlog.append("DqcStationaryRadarDataset must have Date selector");
            return null;
        }
        SelectService.ServiceChoice wantServiceChoice = null;
        final List services = selService.getChoices();
        for (int j = 0; j < services.size(); ++j) {
            final SelectService.ServiceChoice serviceChoice = services.get(j);
            if (serviceChoice.getService().equals("HTTPServer") && serviceChoice.getDataFormat().equals("text/xml")) {
                wantServiceChoice = serviceChoice;
            }
        }
        if (wantServiceChoice == null) {
            errlog.append("DqcStationObsDataset must have HTTPServer Service with DataFormat=text/plain, and returns=data");
            return null;
        }
        return new DqcRadarDatasetCollection(desc, dqc, selService, wantServiceChoice, selStation, null, selTime);
    }
    
    private DqcRadarDatasetCollection(final String desc, final QueryCapability dqc, final SelectService selService, final SelectService.ServiceChoice service, final SelectStation selStation, final SelectGeoRegion selRegion, final SelectList selTime) {
        this.debugQuery = false;
        this.desc = desc;
        this.dqc = dqc;
        this.selService = selService;
        this.selStation = selStation;
        this.selRegion = selRegion;
        this.selTime = selTime;
        this.service = service;
        final ArrayList stationList = selStation.getStations();
        this.stations = new HashMap(stationList.size());
        for (int i = 0; i < stationList.size(); ++i) {
            final Station station = stationList.get(i);
            this.stations.put(station.getValue(), station);
        }
        final ArrayList timeList = selTime.getChoices();
        this.relTimesList = new HashMap(timeList.size());
        for (int j = 0; j < timeList.size(); ++j) {
            final Choice tt = timeList.get(j);
            this.relTimesList.put(tt.getValue(), tt);
        }
        final String ql = dqc.getQuery().getUriResolved().toString();
        this.startDate = new Date();
        this.endDate = new Date();
        try {
            this.timeUnit = new DateUnit("hours since 1991-01-01T00:00");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    protected void setTimeUnits() {
    }
    
    @Override
    protected void setStartDate() {
    }
    
    @Override
    protected void setEndDate() {
    }
    
    @Override
    protected void setBoundingBox() {
    }
    
    @Override
    public String getTitle() {
        return this.dqc.getName();
    }
    
    @Override
    public String getLocation() {
        return this.dqc.getCreateFrom();
    }
    
    @Override
    public String getDescription() {
        return this.desc;
    }
    
    @Override
    public boolean checkStationProduct(final String sName, final Product product) {
        return this.dqc.getName().contains("Level2") && (product.getID().equals("Reflectivity") || product.getID().equals("RadialVelocity") || product.getID().equals("SpectrumWidth"));
    }
    
    public boolean checkStationProduct(final Product product) {
        return this.checkStationProduct(null, product);
    }
    
    public int getStationProductCount(final String sName) {
        if (this.dqc.getName().contains("Level2")) {
            return 3;
        }
        return 0;
    }
    
    @Override
    public List getStations() throws IOException {
        return this.getRadarStations();
    }
    
    public List getRadarStations() {
        final List sl = this.selStation.getStations();
        final ArrayList dsl = new ArrayList();
        for (final Station s : sl) {
            dsl.add(s);
        }
        return dsl;
    }
    
    @Override
    public List getStations(final CancelTask cancel) throws IOException {
        return this.getStations(null, cancel);
    }
    
    @Override
    public List getStations(final LatLonRect boundingBox) throws IOException {
        return this.getStations(boundingBox, null);
    }
    
    @Override
    public List getStations(final LatLonRect boundingBox, final CancelTask cancel) throws IOException {
        final List sl = this.selStation.getStations();
        final ArrayList dsl = new ArrayList();
        for (final Station s : sl) {
            final LatLonPointImpl latlonPt = new LatLonPointImpl();
            latlonPt.set(s.getLocation().getLatitude(), s.getLocation().getLongitude());
            if (boundingBox.contains(latlonPt)) {
                dsl.add(s);
            }
            if (cancel != null && cancel.isCancel()) {
                return null;
            }
        }
        return dsl;
    }
    
    public RadialDatasetSweep getRadarDataset(final String stnName, final Date absTime) throws IOException {
        final InvDataset invdata = this.queryRadarStation(stnName, absTime);
        if (invdata == null) {
            throw new IOException("Invalid time selected: " + absTime.toString() + "\n");
        }
        final ThreddsDataFactory tdFactory = new ThreddsDataFactory();
        final ThreddsDataFactory.Result result = tdFactory.openFeatureDataset(invdata, null);
        return (RadialDatasetSweep)result.featureDataset;
    }
    
    public URI getRadarDatasetURI(final String stnName, final Date absTime) throws IOException {
        final InvDataset invdata = this.queryRadarStation(stnName, absTime);
        final List acess = invdata.getAccess();
        final InvAccess ia = acess.get(0);
        final URI ui = ia.getStandardUri();
        if (ui == null) {
            throw new IOException("Invalid time selected: " + absTime.toString() + "\n");
        }
        return ui;
    }
    
    private InvDataset queryRadarStation(final String stnName, final Date absTime) throws IOException {
        final String stime = DateUtil.getTimeAsISO8601(absTime);
        final StringBuffer queryb = new StringBuffer();
        queryb.append(this.dqc.getQuery().getUriResolved().toString());
        queryb.append("serviceType=OPENDAP");
        queryb.append("&stn=" + stnName);
        queryb.append("&dtime=" + stime);
        URI catalogURI;
        try {
            catalogURI = new URI(queryb.toString());
        }
        catch (URISyntaxException e) {
            throw new IOException("** MalformedURLException on URL <>\n" + e.getMessage() + "\n");
        }
        final InvCatalogFactory factory = new InvCatalogFactory("default", false);
        final InvCatalogImpl catalog = factory.readXML(catalogURI);
        final StringBuilder buff = new StringBuilder();
        if (!catalog.check(buff)) {
            throw new IOException("Invalid catalog <" + catalogURI + ">\n" + buff.toString());
        }
        final List datasets = catalog.getDatasets();
        final InvDataset idata = datasets.get(0);
        final ArrayList dsets = (ArrayList)idata.getDatasets();
        final InvDataset tdata = dsets.get(0);
        return tdata;
    }
    
    private DqcRadarDatasetInfo queryRadarStation(final String stnName, final Date start, final Date end) throws IOException {
        final StringBuffer queryb = new StringBuffer();
        queryb.append(this.dqc.getQuery().getUriResolved().toString());
        queryb.append("serviceType=OPENDAP");
        queryb.append("&stn=" + stnName);
        if (start == null && end == null) {
            queryb.append("&dtime=all");
        }
        else {
            final String stime = DateUtil.getTimeAsISO8601(start);
            final String etime = DateUtil.getTimeAsISO8601(end);
            queryb.append("&dateStart=" + stime);
            queryb.append("&dateEnd=" + etime);
        }
        URI catalogURI;
        try {
            catalogURI = new URI(queryb.toString());
        }
        catch (URISyntaxException e) {
            throw new IOException("** MalformedURLException on URL <>\n" + e.getMessage() + "\n");
        }
        final InvCatalogFactory factory = new InvCatalogFactory("default", false);
        final InvCatalogImpl catalog = factory.readXML(catalogURI);
        final StringBuilder buff = new StringBuilder();
        if (!catalog.check(buff)) {
            throw new IOException("Invalid catalog <" + catalogURI + ">\n" + buff.toString());
        }
        final List datasets = catalog.getDatasets();
        final InvDataset idata = datasets.get(0);
        final ArrayList dsets = (ArrayList)idata.getDatasets();
        final ArrayList absTimeList = new ArrayList();
        final ArrayList dURIList = new ArrayList();
        final ArrayList dInvList = new ArrayList();
        for (int i = 0; i < dsets.size(); ++i) {
            final InvDataset tdata = dsets.get(i);
            final List acess = tdata.getAccess();
            final List dates = tdata.getDates();
            final InvAccess ia = acess.get(0);
            final URI d = ia.getStandardUri();
            final Date date = DateUnit.getStandardOrISO(dates.get(0).toString());
            absTimeList.add(date);
            dURIList.add(new DatasetURIInfo(d, date));
            dInvList.add(new InvDatasetInfo(tdata, date));
        }
        final DqcRadarDatasetInfo dri = new DqcRadarDatasetInfo(absTimeList, dURIList, dInvList);
        return dri;
    }
    
    public ArrayList getRadarStationURIs(final String stnName, final Date start, final Date end) throws IOException {
        final DqcRadarDatasetInfo dri = this.queryRadarStation(stnName, start, end);
        final ArrayList uList = dri.getURIList();
        final int size = uList.size();
        final ArrayList datasetsURI = new ArrayList();
        for (int i = 0; i < size; ++i) {
            final DatasetURIInfo du = uList.get(i);
            datasetsURI.add(du.uri);
        }
        return datasetsURI;
    }
    
    public ArrayList getRadarStationDatasets(final String stnName, final Date start, final Date end) throws IOException {
        final ArrayList datasetList = new ArrayList();
        final DqcRadarDatasetInfo dri = this.queryRadarStation(stnName, start, end);
        final ArrayList iList = dri.getInvList();
        for (int size = iList.size(), i = 0; i < size; ++i) {
            final InvDatasetInfo iv = iList.get(i);
            final InvDataset tdata = iv.inv;
            final ThreddsDataFactory tdFactory = new ThreddsDataFactory();
            final ThreddsDataFactory.Result result = tdFactory.openFeatureDataset(tdata, null);
            datasetList.add(result.featureDataset);
        }
        return datasetList;
    }
    
    public ArrayList getRadarStationTimes(final String stnName, final Date start, final Date end) throws IOException {
        final DqcRadarDatasetInfo dri = this.queryRadarStation(stnName, start, end);
        return dri.absTimeList;
    }
    
    public DataIterator getDataIterator(final int bufferSize) throws IOException {
        return null;
    }
    
    private List queryRadarStationRTimes(final String stn) throws IOException {
        return this.selTime.getChoices();
    }
    
    public ArrayList getDataURIs(final String sName, final DateSelection dateInfo) throws IOException {
        return this.getDataURIs(sName, dateInfo, null);
    }
    
    public ArrayList getData(final String sName, final DateSelection dateInfo) throws IOException {
        return this.getData(sName, dateInfo, null);
    }
    
    public ArrayList getData(final String sName, final DateSelection dateSelect, final CancelTask cancel) throws IOException {
        if (cancel != null && cancel.isCancel()) {
            return null;
        }
        final DqcRadarDatasetInfo dri = this.queryRadarStation(sName, dateSelect.getStartFixedDate(), dateSelect.getEndFixedDate());
        final ArrayList datasetList = new ArrayList();
        final List datasetINVs = dateSelect.apply(dri.getInvList());
        for (final InvDatasetInfo ifo : datasetINVs) {
            final InvDataset tdata = ifo.inv;
            final ThreddsDataFactory tdFactory = new ThreddsDataFactory();
            final ThreddsDataFactory.Result result = tdFactory.openFeatureDataset(tdata, null);
            datasetList.add(result.featureDataset);
            if (cancel != null && cancel.isCancel()) {
                return null;
            }
        }
        return datasetList;
    }
    
    public ArrayList getDataURIs(final String sName, final DateSelection dateSelect, final CancelTask cancel) throws IOException {
        if (cancel != null && cancel.isCancel()) {
            return null;
        }
        final DqcRadarDatasetInfo dri = this.queryRadarStation(sName, dateSelect.getStartFixedDate(), dateSelect.getEndFixedDate());
        final List datasetsURIs = dateSelect.apply(dri.getURIList());
        final ArrayList uriList = new ArrayList();
        for (final DatasetURIInfo ufo : datasetsURIs) {
            final URI u = ufo.uri;
            uriList.add(u);
            if (cancel != null && cancel.isCancel()) {
                return null;
            }
        }
        return uriList;
    }
    
    public static long roundTo(final long roundTo, final long seconds) {
        final int roundToSeconds = (int)roundTo;
        if (roundToSeconds == 0) {
            return seconds;
        }
        return seconds - (int)seconds % roundToSeconds;
    }
    
    public static void main(final String[] args) throws IOException {
        final StringBuffer errlog = new StringBuffer();
        final String dqc_location = "http://motherlode.ucar.edu:8080/thredds/idd/radarLevel2";
        final DqcRadarDatasetCollection ds = factory("test", dqc_location, errlog);
        System.out.println(" errs= " + (Object)errlog);
        final List stns = ds.getStations();
        System.out.println(" nstns= " + stns.size());
        Station stn = stns.get(2);
        final Date ts1 = DateUnit.getStandardOrISO("2007-06-9T12:12:00");
        final Date ts2 = DateUnit.getStandardOrISO("2007-06-9T23:12:00");
        final List tlist = ds.getRadarStationTimes(stn.getValue(), ts1, ts2);
        final int sz = tlist.size();
        final Date ts3 = DateUnit.getStandardOrISO(tlist.get(1));
        final RadialDatasetSweep rds = ds.getRadarDataset(stn.getValue(), ts3);
        final URI stURL = ds.getRadarDatasetURI(stn.getValue(), ts3);
        assert null != stURL;
        assert 0 != sz;
        final DateSelection dateS = new DateSelection(ts1, ts2);
        dateS.setInterval(3600000.0);
        dateS.setRoundTo(3600000.0);
        dateS.setPreRange(500000.0);
        dateS.setPostRange(500000.0);
        for (int i = 0; i < stns.size(); ++i) {
            stn = stns.get(i);
            final List times = ds.getRadarStationTimes(stn.getValue(), new Date(System.currentTimeMillis() - 50065408L), new Date(System.currentTimeMillis()));
            if (times.size() > 0) {
                System.err.println(stn + " times:" + times.size() + " " + times.get(0) + " - " + times.get(times.size() - 1));
            }
            else {
                System.err.println(stn + " no times");
            }
        }
        System.exit(0);
        final List jList = ds.getDataURIs("KABX", dateS);
        assert null != jList;
        final List mList = ds.getData("KABX", dateS, null);
        assert null != mList;
        final Date ts4 = DateUnit.getStandardOrISO(tlist.get(1));
        final SimpleDateFormat isoDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        isoDateTimeFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        final String st = isoDateTimeFormat.format(ts4);
    }
    
    public class DatasetURIInfo implements DatedThing
    {
        private URI uri;
        private Date date;
        
        public DatasetURIInfo(final URI u, final Date date) {
            this.uri = null;
            this.date = null;
            this.uri = u;
            this.date = date;
        }
        
        public Date getDate() {
            return this.date;
        }
    }
    
    public class InvDatasetInfo implements DatedThing
    {
        private InvDataset inv;
        private Date date;
        
        public InvDatasetInfo(final InvDataset u, final Date date) {
            this.inv = null;
            this.date = null;
            this.inv = u;
            this.date = date;
        }
        
        public Date getDate() {
            return this.date;
        }
    }
    
    public class DqcRadarDatasetInfo
    {
        private ArrayList absTimeList;
        private ArrayList datasetInfoList;
        private ArrayList invDatasetList;
        
        public DqcRadarDatasetInfo() {
        }
        
        public DqcRadarDatasetInfo(final ArrayList absTimeList, final ArrayList datasetInfoList, final ArrayList invDatasetList) {
            this.absTimeList = absTimeList;
            this.datasetInfoList = datasetInfoList;
            this.invDatasetList = invDatasetList;
        }
        
        public ArrayList getTimeList() {
            return this.absTimeList;
        }
        
        public ArrayList getURIList() {
            return this.datasetInfoList;
        }
        
        public ArrayList getInvList() {
            return this.invDatasetList;
        }
    }
}
