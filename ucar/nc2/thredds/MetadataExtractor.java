// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.thredds;

import org.slf4j.LoggerFactory;
import java.util.Date;
import ucar.nc2.dataset.CoordinateAxis;
import ucar.nc2.dataset.CoordinateAxis1DTime;
import ucar.nc2.units.DateUnit;
import ucar.nc2.units.DateRange;
import ucar.nc2.dt.GridDatatype;
import thredds.catalog.DataFormatType;
import ucar.nc2.Attribute;
import ucar.nc2.VariableSimpleIF;
import ucar.nc2.dt.GridCoordSystem;
import java.util.Iterator;
import ucar.nc2.dataset.CoordinateAxis1D;
import ucar.unidata.geoloc.LatLonRect;
import java.io.IOException;
import ucar.nc2.dt.StationObsDataset;
import ucar.nc2.dt.PointObsDataset;
import ucar.nc2.dt.GridDataset;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.util.CancelTask;
import thredds.catalog.InvDataset;
import thredds.catalog.ThreddsMetadata;
import thredds.catalog.InvDatasetImpl;
import org.slf4j.Logger;

public class MetadataExtractor
{
    private static Logger logger;
    
    public static ThreddsMetadata.GeospatialCoverage extractGeospatial(final InvDatasetImpl threddsDataset) throws IOException {
        ThreddsDataFactory.Result result = null;
        try {
            result = new ThreddsDataFactory().openFeatureDataset(threddsDataset, null);
            if (result.fatalError) {
                System.out.println(" openDatatype errs=" + result.errLog);
                return null;
            }
            if (result.featureType == FeatureType.GRID) {
                System.out.println(" GRID=" + result.location);
                final GridDataset gridDataset = (GridDataset)result.featureDataset;
                return extractGeospatial(gridDataset);
            }
            if (result.featureType == FeatureType.POINT) {
                final PointObsDataset pobsDataset = (PointObsDataset)result.featureDataset;
                final LatLonRect llbb = pobsDataset.getBoundingBox();
                if (null != llbb) {
                    final ThreddsMetadata.GeospatialCoverage gc = new ThreddsMetadata.GeospatialCoverage();
                    gc.setBoundingBox(llbb);
                    return gc;
                }
            }
            else if (result.featureType == FeatureType.STATION) {
                final StationObsDataset sobsDataset = (StationObsDataset)result.featureDataset;
                final LatLonRect llbb = sobsDataset.getBoundingBox();
                if (null != llbb) {
                    final ThreddsMetadata.GeospatialCoverage gc = new ThreddsMetadata.GeospatialCoverage();
                    gc.setBoundingBox(llbb);
                    return gc;
                }
            }
        }
        finally {
            try {
                if (result != null && result.featureDataset != null) {
                    result.featureDataset.close();
                }
            }
            catch (IOException ioe) {
                MetadataExtractor.logger.error("Closing dataset " + result.featureDataset, ioe);
            }
        }
        return null;
    }
    
    public static ThreddsMetadata.GeospatialCoverage extractGeospatial(final GridDataset gridDataset) {
        final ThreddsMetadata.GeospatialCoverage gc = new ThreddsMetadata.GeospatialCoverage();
        LatLonRect llbb = null;
        CoordinateAxis1D vaxis = null;
        for (final GridDataset.Gridset gridset : gridDataset.getGridsets()) {
            final GridCoordSystem gsys = gridset.getGeoCoordSystem();
            if (llbb == null) {
                llbb = gsys.getLatLonBoundingBox();
            }
            final CoordinateAxis1D vaxis2 = gsys.getVerticalAxis();
            if (vaxis == null) {
                vaxis = vaxis2;
            }
            else {
                if (vaxis2 == null || vaxis2.getSize() <= vaxis.getSize()) {
                    continue;
                }
                vaxis = vaxis2;
            }
        }
        if (llbb != null) {
            gc.setBoundingBox(llbb);
        }
        if (vaxis != null) {
            gc.setVertical(vaxis);
        }
        return gc;
    }
    
    public static ThreddsMetadata.Variables extractVariables(final InvDatasetImpl threddsDataset) throws IOException {
        ThreddsDataFactory.Result result = null;
        try {
            result = new ThreddsDataFactory().openFeatureDataset(threddsDataset, null);
            if (result.fatalError) {
                System.out.println(" openDatatype errs=" + result.errLog);
                return null;
            }
            if (result.featureType == FeatureType.GRID) {
                final GridDataset gridDataset = (GridDataset)result.featureDataset;
                return extractVariables(threddsDataset, gridDataset);
            }
            if (result.featureType == FeatureType.STATION || result.featureType == FeatureType.POINT) {
                final PointObsDataset pobsDataset = (PointObsDataset)result.featureDataset;
                final ThreddsMetadata.Variables vars = new ThreddsMetadata.Variables("CF-1.0");
                for (final VariableSimpleIF vs : pobsDataset.getDataVariables()) {
                    final ThreddsMetadata.Variable v = new ThreddsMetadata.Variable();
                    vars.addVariable(v);
                    v.setName(vs.getName());
                    v.setDescription(vs.getDescription());
                    v.setUnits(vs.getUnitsString());
                    final Attribute att = vs.findAttributeIgnoreCase("standard_name");
                    v.setVocabularyName((att != null) ? att.getStringValue() : "N/A");
                }
                vars.sort();
                return vars;
            }
        }
        finally {
            try {
                if (result != null && result.featureDataset != null) {
                    result.featureDataset.close();
                }
            }
            catch (IOException ioe) {
                MetadataExtractor.logger.error("Closing dataset " + result.featureDataset, ioe);
            }
        }
        return null;
    }
    
    public static ThreddsMetadata.Variables extractVariables(final InvDatasetImpl threddsDataset, final GridDataset gridDataset) {
        final DataFormatType fileFormat = threddsDataset.getDataFormatType();
        if (fileFormat != null && (fileFormat.equals(DataFormatType.GRIB1) || fileFormat.equals(DataFormatType.GRIB2))) {
            final boolean isGrib1 = fileFormat.equals(DataFormatType.GRIB1);
            final ThreddsMetadata.Variables vars = new ThreddsMetadata.Variables(fileFormat.toString());
            for (final GridDatatype grid : gridDataset.getGrids()) {
                final ThreddsMetadata.Variable v = new ThreddsMetadata.Variable();
                v.setName(grid.getName());
                v.setDescription(grid.getDescription());
                v.setUnits(grid.getUnitsString());
                if (isGrib1) {
                    v.setVocabularyName(grid.findAttValueIgnoreCase("GRIB_param_name", "ERROR"));
                    v.setVocabularyId(grid.findAttributeIgnoreCase("GRIB_param_id"));
                }
                else {
                    final String paramDisc = grid.findAttValueIgnoreCase("GRIB_param_discipline", "");
                    final String paramCategory = grid.findAttValueIgnoreCase("GRIB_param_category", "");
                    final String paramName = grid.findAttValueIgnoreCase("GRIB_param_name", "");
                    v.setVocabularyName(paramDisc + " / " + paramCategory + " / " + paramName);
                    v.setVocabularyId(grid.findAttributeIgnoreCase("GRIB_param_id"));
                }
                vars.addVariable(v);
            }
            vars.sort();
            return vars;
        }
        final ThreddsMetadata.Variables vars2 = new ThreddsMetadata.Variables("CF-1.0");
        for (final GridDatatype grid2 : gridDataset.getGrids()) {
            final ThreddsMetadata.Variable v2 = new ThreddsMetadata.Variable();
            vars2.addVariable(v2);
            v2.setName(grid2.getName());
            v2.setDescription(grid2.getDescription());
            v2.setUnits(grid2.getUnitsString());
            final Attribute att = grid2.findAttributeIgnoreCase("standard_name");
            v2.setVocabularyName((att != null) ? att.getStringValue() : "N/A");
        }
        vars2.sort();
        return vars2;
    }
    
    public static DateRange extractDateRange(final GridDataset gridDataset) {
        DateRange maxDateRange = null;
        for (final GridDataset.Gridset gridset : gridDataset.getGridsets()) {
            final GridCoordSystem gsys = gridset.getGeoCoordSystem();
            final CoordinateAxis1DTime time1D = gsys.getTimeAxis1D();
            DateRange dateRange;
            if (time1D != null) {
                dateRange = time1D.getDateRange();
            }
            else {
                final CoordinateAxis time = gsys.getTimeAxis();
                if (time == null) {
                    continue;
                }
                try {
                    final DateUnit du = new DateUnit(time.getUnitsString());
                    final Date minDate = du.makeDate(time.getMinValue());
                    final Date maxDate = du.makeDate(time.getMaxValue());
                    dateRange = new DateRange(minDate, maxDate);
                }
                catch (Exception e) {
                    MetadataExtractor.logger.warn("Illegal Date Unit " + time.getUnitsString());
                    continue;
                }
            }
            if (maxDateRange == null) {
                maxDateRange = dateRange;
            }
            else {
                maxDateRange.extend(dateRange);
            }
        }
        return maxDateRange;
    }
    
    static {
        MetadataExtractor.logger = LoggerFactory.getLogger(MetadataExtractor.class);
    }
}
