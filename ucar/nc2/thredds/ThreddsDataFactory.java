// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.thredds;

import ucar.nc2.ft.FeatureDataset;
import java.util.Iterator;
import ucar.nc2.Group;
import ucar.nc2.Attribute;
import thredds.catalog.InvProperty;
import thredds.catalog.InvCatalogImpl;
import thredds.catalog.DataFormatType;
import ucar.nc2.ncml.NcMLReader;
import thredds.catalog.InvMetadata;
import thredds.catalog.MetadataType;
import ucar.nc2.stream.CdmRemote;
import ucar.nc2.dods.DODSNetcdfFile;
import thredds.catalog.InvDatasetImpl;
import java.util.List;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Formatter;
import ucar.nc2.stream.CdmRemoteFeatureDataset;
import ucar.nc2.dataset.NetcdfDataset;
import thredds.catalog.InvAccess;
import ucar.nc2.ft.FeatureDatasetFactoryManager;
import thredds.catalog.ServiceType;
import thredds.catalog.InvCatalog;
import thredds.catalog.InvCatalogFactory;
import ucar.unidata.util.StringUtil;
import java.io.IOException;
import thredds.catalog.InvDataset;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.util.CancelTask;
import ucar.nc2.util.DebugFlags;

public class ThreddsDataFactory
{
    public static final String SCHEME = "thredds:";
    public static boolean preferCdm;
    private static boolean debugOpen;
    private static boolean debugTypeOpen;
    private static boolean enhanceMode;
    
    public static void setDebugFlags(final DebugFlags debugFlag) {
        ThreddsDataFactory.debugOpen = debugFlag.isSet("thredds/debugOpen");
        ThreddsDataFactory.debugTypeOpen = debugFlag.isSet("thredds/openDatatype");
    }
    
    public Result openFeatureDataset(final String urlString, final CancelTask task) throws IOException {
        final Result result = new Result();
        final InvDataset invDataset = this.processLocation(urlString, task, result);
        if (result.fatalError) {
            return result;
        }
        return this.openFeatureDataset(null, invDataset, task, result);
    }
    
    public Result openFeatureDataset(final FeatureType wantFeatureType, final String urlString, final CancelTask task) throws IOException {
        final Result result = new Result();
        final InvDataset invDataset = this.processLocation(urlString, task, result);
        if (result.fatalError) {
            return result;
        }
        return this.openFeatureDataset(wantFeatureType, invDataset, task, result);
    }
    
    private InvDataset processLocation(String location, final CancelTask task, final Result result) {
        location = location.trim();
        location = StringUtil.replace(location, '\\', "/");
        if (location.startsWith("thredds:")) {
            location = location.substring(8);
        }
        if (location.startsWith("resolve:")) {
            location = location.substring(8);
            return this.openResolver(location, task, result);
        }
        if (!location.startsWith("http:") && !location.startsWith("file:")) {
            location = "http:" + location;
        }
        final int pos = location.indexOf(35);
        if (pos < 0) {
            result.fatalError = true;
            result.errLog.format("Must have the form catalog.xml#datasetId%n", new Object[0]);
            return null;
        }
        final InvCatalogFactory catFactory = new InvCatalogFactory("", false);
        final String catalogLocation = location.substring(0, pos);
        final InvCatalog catalog = catFactory.readXML(catalogLocation);
        final StringBuilder buff = new StringBuilder();
        if (!catalog.check(buff)) {
            result.errLog.format("Invalid catalog from Resolver <%s>%n%s%n", catalogLocation, buff.toString());
            result.fatalError = true;
            return null;
        }
        final String datasetId = location.substring(pos + 1);
        final InvDataset invDataset = catalog.findDatasetByID(datasetId);
        if (invDataset == null) {
            result.fatalError = true;
            result.errLog.format("Could not find dataset %s in %s %n", datasetId, catalogLocation);
            return null;
        }
        return invDataset;
    }
    
    public Result openFeatureDataset(final InvDataset invDataset, final CancelTask task) throws IOException {
        return this.openFeatureDataset(null, invDataset, task, new Result());
    }
    
    public Result openFeatureDataset(final FeatureType wantFeatureType, final InvDataset invDataset, final CancelTask task, final Result result) throws IOException {
        result.featureType = invDataset.getDataType();
        if (result.featureType == null) {
            result.featureType = wantFeatureType;
        }
        if (result.featureType != null && result.featureType.isPointFeatureType()) {
            final InvAccess access = this.findAccessByServiceType(invDataset.getAccess(), ServiceType.CdmRemote);
            if (access != null) {
                return this.openFeatureDataset(result.featureType, access, task, result);
            }
        }
        if (result.featureType == FeatureType.IMAGE) {
            final InvAccess access = this.getImageAccess(invDataset, task, result);
            if (access != null) {
                return this.openFeatureDataset(result.featureType, access, task, result);
            }
            result.fatalError = true;
            return result;
        }
        else {
            final InvAccess qc = invDataset.getAccess(ServiceType.QC);
            if (qc != null) {
                final String dqc_location = qc.getStandardUrlName();
                if (result.featureType == FeatureType.STATION) {
                    result.featureDataset = null;
                    result.fatalError = (result.featureDataset == null);
                }
                else {
                    result.errLog.format("DQC must be station DQC, dataset = %s %n", invDataset.getName());
                    result.fatalError = true;
                }
                return result;
            }
            final NetcdfDataset ncd = this.openDataset(invDataset, true, task, result.errLog);
            if (null != ncd) {
                result.featureDataset = FeatureDatasetFactoryManager.wrap(result.featureType, ncd, task, result.errLog);
            }
            if (null == result.featureDataset) {
                result.fatalError = true;
            }
            else {
                result.location = result.featureDataset.getLocation();
                if (result.featureType == null && result.featureDataset != null) {
                    result.featureType = result.featureDataset.getFeatureType();
                }
            }
            return result;
        }
    }
    
    public Result openFeatureDataset(final InvAccess access, final CancelTask task) throws IOException {
        final InvDataset invDataset = access.getDataset();
        final Result result = new Result();
        if (invDataset.getDataType() == null) {
            result.errLog.format("InvDatasert must specify a FeatureType%n", new Object[0]);
            result.fatalError = true;
            return result;
        }
        return this.openFeatureDataset(invDataset.getDataType(), access, task, result);
    }
    
    private Result openFeatureDataset(final FeatureType wantFeatureType, final InvAccess access, final CancelTask task, final Result result) throws IOException {
        result.featureType = wantFeatureType;
        result.accessUsed = access;
        if (result.featureType == FeatureType.IMAGE) {
            result.imageURL = access.getStandardUrlName();
            result.location = result.imageURL;
            return result;
        }
        if (access.getService().getServiceType() == ServiceType.CdmRemote) {
            result.featureDataset = CdmRemoteFeatureDataset.factory(wantFeatureType, access.getStandardUrlName());
        }
        else {
            final NetcdfDataset ncd = this.openDataset(access, true, task, result);
            if (null != ncd) {
                result.featureDataset = FeatureDatasetFactoryManager.wrap(result.featureType, ncd, task, result.errLog);
            }
        }
        if (null == result.featureDataset) {
            result.fatalError = true;
        }
        else {
            result.location = result.featureDataset.getLocation();
            if (result.featureType == null && result.featureDataset != null) {
                result.featureType = result.featureDataset.getFeatureType();
            }
        }
        return result;
    }
    
    public NetcdfDataset openDataset(final String location, final boolean acquire, final CancelTask task, final Formatter log) throws IOException {
        final Result result = new Result();
        final InvDataset invDataset = this.processLocation(location, task, result);
        if (result.fatalError) {
            if (log != null) {
                log.format("%s", result.errLog);
            }
            return null;
        }
        return this.openDataset(invDataset, acquire, task, result);
    }
    
    public NetcdfDataset openDataset(final InvDataset invDataset, final boolean acquire, final CancelTask task, final Formatter log) throws IOException {
        final Result result = new Result();
        final NetcdfDataset ncd = this.openDataset(invDataset, acquire, task, result);
        if (log != null) {
            log.format("%s", result.errLog);
        }
        return result.fatalError ? null : ncd;
    }
    
    private NetcdfDataset openDataset(final InvDataset invDataset, final boolean acquire, final CancelTask task, final Result result) throws IOException {
        IOException saveException = null;
        List<InvAccess> accessList = new ArrayList<InvAccess>(invDataset.getAccess());
        while (accessList.size() > 0) {
            final InvAccess access = this.chooseDatasetAccess(accessList);
            if (access == null) {
                result.errLog.format("No access that could be used in dataset %s %n", invDataset);
                if (saveException != null) {
                    throw saveException;
                }
                return null;
            }
            else {
                final String datasetLocation = access.getStandardUrlName();
                final ServiceType serviceType = access.getService().getServiceType();
                if (ThreddsDataFactory.debugOpen) {
                    System.out.println("ThreddsDataset.openDataset try " + datasetLocation + " " + serviceType);
                }
                if (serviceType != ServiceType.RESOLVER) {
                    NetcdfDataset ds;
                    try {
                        ds = this.openDataset(access, acquire, task, result);
                    }
                    catch (IOException e) {
                        result.errLog.format("Cant open %s %n err=%s%n", datasetLocation, e.getMessage());
                        if (ThreddsDataFactory.debugOpen) {
                            System.out.println("Cant open= " + datasetLocation + " " + serviceType);
                            e.printStackTrace();
                        }
                        accessList.remove(access);
                        saveException = e;
                        continue;
                    }
                    result.accessUsed = access;
                    return ds;
                }
                final InvDatasetImpl rds = this.openResolver(datasetLocation, task, result);
                if (rds == null) {
                    return null;
                }
                accessList = new ArrayList<InvAccess>(rds.getAccess());
            }
        }
        if (saveException != null) {
            throw saveException;
        }
        return null;
    }
    
    public NetcdfDataset openDataset(final InvAccess access, final boolean acquire, final CancelTask task, final Formatter log) throws IOException {
        final Result result = new Result();
        final NetcdfDataset ncd = this.openDataset(access, acquire, task, result);
        if (log != null) {
            log.format("%s", result.errLog);
        }
        return result.fatalError ? null : ncd;
    }
    
    private NetcdfDataset openDataset(final InvAccess access, final boolean acquire, final CancelTask task, final Result result) throws IOException {
        final InvDataset invDataset = access.getDataset();
        final String datasetId = invDataset.getID();
        final String title = invDataset.getName();
        final String datasetLocation = access.getStandardUrlName();
        final ServiceType serviceType = access.getService().getServiceType();
        if (ThreddsDataFactory.debugOpen) {
            System.out.println("ThreddsDataset.openDataset= " + datasetLocation);
        }
        if (serviceType != ServiceType.RESOLVER) {
            NetcdfDataset ds;
            if (serviceType == ServiceType.OPENDAP || serviceType == ServiceType.DODS) {
                final String curl = DODSNetcdfFile.canonicalURL(datasetLocation);
                ds = (acquire ? NetcdfDataset.acquireDataset(curl, ThreddsDataFactory.enhanceMode, task) : NetcdfDataset.openDataset(curl, ThreddsDataFactory.enhanceMode, task));
            }
            else if (serviceType == ServiceType.CdmRemote) {
                final String curl = CdmRemote.canonicalURL(datasetLocation);
                ds = (acquire ? NetcdfDataset.acquireDataset(curl, ThreddsDataFactory.enhanceMode, task) : NetcdfDataset.openDataset(curl, ThreddsDataFactory.enhanceMode, task));
            }
            else {
                ds = (acquire ? NetcdfDataset.acquireDataset(datasetLocation, ThreddsDataFactory.enhanceMode, task) : NetcdfDataset.openDataset(datasetLocation, ThreddsDataFactory.enhanceMode, task));
            }
            if (ds != null) {
                ds.setId(datasetId);
                ds.setTitle(title);
                annotate(invDataset, ds);
            }
            final List list = invDataset.getMetadata(MetadataType.NcML);
            if (list.size() > 0) {
                final InvMetadata ncmlMetadata = list.get(0);
                NcMLReader.wrapNcML(ds, ncmlMetadata.getXlinkHref(), null);
            }
            result.accessUsed = access;
            return ds;
        }
        final InvDatasetImpl rds = this.openResolver(datasetLocation, task, result);
        if (rds == null) {
            return null;
        }
        return this.openDataset(rds, acquire, task, result);
    }
    
    public InvAccess chooseDatasetAccess(final List<InvAccess> accessList) {
        if (accessList.size() == 0) {
            return null;
        }
        InvAccess access = null;
        if (ThreddsDataFactory.preferCdm) {
            access = this.findAccessByServiceType(accessList, ServiceType.CdmRemote);
        }
        if (access == null) {
            access = this.findAccessByServiceType(accessList, ServiceType.FILE);
        }
        if (access == null) {
            access = this.findAccessByServiceType(accessList, ServiceType.NETCDF);
        }
        if (access == null) {
            access = this.findAccessByServiceType(accessList, ServiceType.DODS);
        }
        if (access == null) {
            access = this.findAccessByServiceType(accessList, ServiceType.OPENDAP);
        }
        if (access == null) {
            access = this.findAccessByServiceType(accessList, ServiceType.CdmRemote);
        }
        if (access == null) {
            InvAccess tryAccess = this.findAccessByServiceType(accessList, ServiceType.HTTPServer);
            if (tryAccess == null) {
                tryAccess = this.findAccessByServiceType(accessList, ServiceType.HTTP);
            }
            if (tryAccess != null) {
                final DataFormatType format = tryAccess.getDataFormatType();
                if (DataFormatType.BUFR == format || DataFormatType.GINI == format || DataFormatType.GRIB1 == format || DataFormatType.GRIB2 == format || DataFormatType.HDF5 == format || DataFormatType.NCML == format || DataFormatType.NETCDF == format || DataFormatType.NEXRAD2 == format || DataFormatType.NIDS == format) {
                    access = tryAccess;
                }
            }
        }
        if (access == null) {
            access = this.findAccessByServiceType(accessList, ServiceType.ADDE);
        }
        if (access == null) {
            access = this.findAccessByServiceType(accessList, ServiceType.RESOLVER);
        }
        return access;
    }
    
    private InvDatasetImpl openResolver(final String urlString, final CancelTask task, final Result result) {
        final InvCatalogFactory catFactory = new InvCatalogFactory("", false);
        final InvCatalogImpl catalog = catFactory.readXML(urlString);
        if (catalog == null) {
            result.errLog.format("Couldnt open Resolver %s %n ", urlString);
            return null;
        }
        final StringBuilder buff = new StringBuilder();
        if (!catalog.check(buff)) {
            result.errLog.format("Invalid catalog from Resolver <%s>%n%s%n", urlString, buff.toString());
            result.fatalError = true;
            return null;
        }
        final InvDataset top = catalog.getDataset();
        if (top.hasAccess()) {
            return (InvDatasetImpl)top;
        }
        final List datasets = top.getDatasets();
        return datasets.get(0);
    }
    
    public static void annotate(final InvDataset ds, final NetcdfDataset ncDataset) {
        ncDataset.setTitle(ds.getName());
        ncDataset.setId(ds.getID());
        for (final InvProperty p : ds.getProperties()) {
            final String name = p.getName();
            if (null == ncDataset.findGlobalAttribute(name)) {
                ncDataset.addAttribute(null, new Attribute(name, p.getValue()));
            }
        }
        ncDataset.finish();
    }
    
    private InvAccess getImageAccess(final InvDataset invDataset, final CancelTask task, final Result result) {
        for (List<InvAccess> accessList = new ArrayList<InvAccess>(invDataset.getAccess()); accessList.size() > 0; accessList = new ArrayList<InvAccess>(invDataset.getAccess())) {
            InvAccess access = this.chooseImageAccess(accessList);
            if (access != null) {
                return access;
            }
            access = invDataset.getAccess(ServiceType.RESOLVER);
            if (access == null) {
                result.errLog.format("No access that could be used for Image Type %s %n", invDataset);
                return null;
            }
            final String datasetLocation = access.getStandardUrlName();
            final InvDatasetImpl rds = this.openResolver(datasetLocation, task, result);
            if (rds == null) {
                return null;
            }
        }
        return null;
    }
    
    private InvAccess chooseImageAccess(final List<InvAccess> accessList) {
        InvAccess access = this.findAccessByDataFormatType(accessList, DataFormatType.JPEG);
        if (access != null) {
            return access;
        }
        access = this.findAccessByDataFormatType(accessList, DataFormatType.GIF);
        if (access != null) {
            return access;
        }
        access = this.findAccessByDataFormatType(accessList, DataFormatType.TIFF);
        if (access != null) {
            return access;
        }
        access = this.findAccessByServiceType(accessList, ServiceType.ADDE);
        if (access != null) {
            final String datasetLocation = access.getStandardUrlName();
            if (datasetLocation.indexOf("image") > 0) {
                return access;
            }
        }
        return access;
    }
    
    private InvAccess findAccessByServiceType(final List<InvAccess> accessList, final ServiceType type) {
        for (final InvAccess a : accessList) {
            if (type.toString().equalsIgnoreCase(a.getService().getServiceType().toString())) {
                return a;
            }
        }
        return null;
    }
    
    private InvAccess findAccessByDataFormatType(final List<InvAccess> accessList, final DataFormatType type) {
        for (final InvAccess a : accessList) {
            if (type.toString().equalsIgnoreCase(a.getDataFormatType().toString())) {
                return a;
            }
        }
        return null;
    }
    
    static {
        ThreddsDataFactory.preferCdm = false;
        ThreddsDataFactory.debugOpen = false;
        ThreddsDataFactory.debugTypeOpen = false;
        ThreddsDataFactory.enhanceMode = false;
    }
    
    public static class Result
    {
        public boolean fatalError;
        public Formatter errLog;
        public FeatureType featureType;
        public FeatureDataset featureDataset;
        public String imageURL;
        public String location;
        public InvAccess accessUsed;
        
        public Result() {
            this.errLog = new Formatter();
        }
    }
}
