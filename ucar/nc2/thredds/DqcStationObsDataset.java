// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.thredds;

import ucar.ma2.Array;
import ucar.ma2.StructureDataW;
import ucar.nc2.dt.StationImpl;
import ucar.ma2.StructureData;
import ucar.nc2.dt.point.StationObsDatatypeImpl;
import ucar.nc2.dt.DataIterator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.net.URLConnection;
import java.io.InputStream;
import java.net.ConnectException;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import ucar.nc2.util.CancelTask;
import ucar.ma2.DataType;
import ucar.nc2.units.DateUnit;
import java.util.Date;
import thredds.catalog.query.Station;
import java.util.List;
import java.util.ArrayList;
import thredds.catalog.query.Selector;
import thredds.catalog.query.DqcFactory;
import java.io.IOException;
import thredds.catalog.InvDataset;
import ucar.nc2.dt.point.decode.MetarParseReport;
import ucar.ma2.StructureMembers;
import thredds.catalog.query.SelectGeoRegion;
import thredds.catalog.query.SelectRangeDate;
import thredds.catalog.query.SelectStation;
import thredds.catalog.query.SelectService;
import thredds.catalog.query.QueryCapability;
import ucar.nc2.dt.point.StationObsDatasetImpl;

public class DqcStationObsDataset extends StationObsDatasetImpl
{
    private QueryCapability dqc;
    private SelectService selService;
    private SelectStation selStation;
    private SelectRangeDate selDate;
    private SelectGeoRegion selRegion;
    private SelectService.ServiceChoice service;
    private boolean debugQuery;
    private StructureMembers members;
    private MetarParseReport parser;
    
    public static DqcStationObsDataset factory(final InvDataset ds, final String dqc_location, final StringBuilder errlog) throws IOException {
        return factory(ds.getDocumentation("summary"), dqc_location, errlog);
    }
    
    public static DqcStationObsDataset factory(final String desc, final String dqc_location, final StringBuilder errlog) throws IOException {
        final DqcFactory dqcFactory = new DqcFactory(true);
        final QueryCapability dqc = dqcFactory.readXML(dqc_location);
        if (dqc.hasFatalError()) {
            errlog.append(dqc.getErrorMessages());
            return null;
        }
        SelectStation selStation = null;
        SelectRangeDate selDate = null;
        SelectService selService = null;
        SelectGeoRegion selRegion = null;
        final ArrayList selectors = dqc.getSelectors();
        for (int i = 0; i < selectors.size(); ++i) {
            final Selector s = selectors.get(i);
            if (s instanceof SelectStation) {
                selStation = (SelectStation)s;
            }
            if (s instanceof SelectRangeDate) {
                selDate = (SelectRangeDate)s;
            }
            if (s instanceof SelectService) {
                selService = (SelectService)s;
            }
            if (s instanceof SelectGeoRegion) {
                selRegion = (SelectGeoRegion)s;
            }
        }
        if (selService == null) {
            errlog.append("DqcStationObsDataset must have Service selector");
            return null;
        }
        if (selStation == null) {
            errlog.append("DqcStationObsDataset must have Station selector");
            return null;
        }
        if (selDate == null) {
            errlog.append("DqcStationObsDataset must have Date selector");
            return null;
        }
        if (selRegion == null) {
            errlog.append("DqcStationObsDataset must have GeoRegion selector");
            return null;
        }
        SelectService.ServiceChoice wantServiceChoice = null;
        final List services = selService.getChoices();
        for (int j = 0; j < services.size(); ++j) {
            final SelectService.ServiceChoice serviceChoice = services.get(j);
            if (serviceChoice.getService().equals("HTTPServer") && serviceChoice.getDataFormat().equals("text/plain") && serviceChoice.getReturns().equals("data")) {
                wantServiceChoice = serviceChoice;
            }
        }
        if (wantServiceChoice == null) {
            errlog.append("DqcStationObsDataset must have HTTPServer Service with DataFormat=text/plain, and returns=data");
            return null;
        }
        return new DqcStationObsDataset(desc, dqc, selService, wantServiceChoice, selStation, selRegion, selDate);
    }
    
    private DqcStationObsDataset(final String desc, final QueryCapability dqc, final SelectService selService, final SelectService.ServiceChoice service, final SelectStation selStation, final SelectGeoRegion selRegion, final SelectRangeDate selDate) {
        this.debugQuery = false;
        this.desc = desc;
        this.dqc = dqc;
        this.selService = selService;
        this.selStation = selStation;
        this.selRegion = selRegion;
        this.selDate = selDate;
        this.service = service;
        final ArrayList stationList = selStation.getStations();
        for (int i = 0; i < stationList.size(); ++i) {
            final Station station = stationList.get(i);
            this.stations.add(new DqcStation(station));
        }
        this.startDate = new Date();
        this.endDate = new Date();
        try {
            this.timeUnit = new DateUnit("hours since 1991-01-01T00:00");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        this.parser = new MetarParseReport();
        (this.members = new StructureMembers("fake")).addMember("line", null, null, DataType.STRING, new int[] { 1 });
    }
    
    @Override
    protected void setTimeUnits() {
    }
    
    @Override
    protected void setStartDate() {
    }
    
    @Override
    protected void setEndDate() {
    }
    
    @Override
    protected void setBoundingBox() {
    }
    
    @Override
    public String getTitle() {
        return this.dqc.getName();
    }
    
    @Override
    public String getLocation() {
        return this.dqc.getCreateFrom();
    }
    
    @Override
    public String getDescription() {
        return this.desc;
    }
    
    public List getData(final ucar.unidata.geoloc.Station s, final CancelTask cancel) throws IOException {
        return ((DqcStation)s).getObservations();
    }
    
    private ArrayList makeQuery(final ucar.unidata.geoloc.Station s) throws IOException {
        final StringBuffer queryb = new StringBuffer();
        queryb.append(this.dqc.getQuery().getUriResolved().toString());
        final ArrayList choices = new ArrayList();
        queryb.append("returns=text&dtime=3day&");
        choices.clear();
        choices.add("{value}");
        choices.add(s.getName());
        this.selStation.appendQuery(queryb, choices);
        final String queryString = queryb.toString();
        if (this.debugQuery) {
            System.out.println("dqc makeQuery= " + queryString);
        }
        return this.readText(s, queryString);
    }
    
    private ArrayList readText(final ucar.unidata.geoloc.Station s, final String urlString) throws IOException {
        final ArrayList obsList = new ArrayList();
        System.out.println("readText= " + urlString);
        InputStream is = null;
        URL url;
        try {
            url = new URL(urlString);
        }
        catch (MalformedURLException e) {
            throw new IOException("** MalformedURLException on URL <" + urlString + ">\n" + e.getMessage() + "\n");
        }
        try {
            final URLConnection connection = url.openConnection();
            if (connection instanceof HttpURLConnection) {
                final HttpURLConnection httpConnection = (HttpURLConnection)connection;
                final int responseCode = httpConnection.getResponseCode();
                if (responseCode / 100 != 2) {
                    throw new IOException("** Cant open URL <" + urlString + ">\n Response code = " + responseCode + "\n" + httpConnection.getResponseMessage() + "\n");
                }
            }
            boolean first = true;
            is = connection.getInputStream();
            final BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            while (true) {
                final String line = reader.readLine();
                if (line == null) {
                    break;
                }
                if (first) {
                    this.show(line);
                }
                first = false;
                obsList.add(new DqcObsImpl(s, 0.0, line));
            }
        }
        catch (ConnectException e2) {
            throw new IOException("** ConnectException on URL: <" + urlString + ">\n" + e2.getMessage() + "\nServer probably not running");
        }
        finally {
            if (is != null) {
                is.close();
            }
        }
        return obsList;
    }
    
    private void show(final String line) {
        System.out.println(line);
        if (!this.parser.parseReport(line)) {
            return;
        }
        final LinkedHashMap map = this.parser.getFields();
        for (final Object key : map.keySet()) {
            final Object value = map.get(key);
            System.out.println(" " + key + "==(" + value + ") ");
        }
    }
    
    public List getData(final CancelTask cancel) throws IOException {
        return null;
    }
    
    public int getDataCount() {
        return -1;
    }
    
    public DataIterator getDataIterator(final int bufferSize) throws IOException {
        return null;
    }
    
    public static void main(final String[] args) throws IOException {
        final StringBuilder errlog = new StringBuilder();
        final String dqc_location = "file:///C:/data/dqc/metarNew.xml";
        final DqcStationObsDataset ds = factory("test", dqc_location, errlog);
        System.out.println(" errs= " + (Object)errlog);
        final List stns = ds.getStations();
        System.out.println(" nstns= " + stns.size());
        final ucar.unidata.geoloc.Station stn = stns.get(13);
        final List data = ds.getData(stn, null);
        for (int i = 0; i < data.size(); ++i) {
            final StationObsDatatypeImpl obs = data.get(i);
            final StructureData sdata = obs.getData();
            System.out.println(i + " " + sdata.getScalarString("line"));
        }
    }
    
    private class DqcStation extends StationImpl
    {
        private DqcStation(final thredds.catalog.query.Station s) {
            this.name = s.getValue();
            this.desc = s.getName();
            this.lat = s.getLocation().getLatitude();
            this.lon = s.getLocation().getLongitude();
            this.alt = s.getLocation().getElevation();
        }
        
        @Override
        protected ArrayList readObservations() throws IOException {
            return DqcStationObsDataset.this.makeQuery(this);
        }
    }
    
    public class DqcObsImpl extends StationObsDatatypeImpl
    {
        String[] lineData;
        StructureDataW sdata;
        
        private DqcObsImpl(final ucar.unidata.geoloc.Station station, final double dateValue, final String line) {
            super(station, dateValue, dateValue);
            (this.lineData = new String[1])[0] = line;
        }
        
        public StructureData getData() throws IOException {
            if (this.sdata == null) {
                this.sdata = new StructureDataW(DqcStationObsDataset.this.members);
                final Array array = Array.factory(String.class, new int[] { 1 }, this.lineData);
                this.sdata.setMemberData("line", array);
            }
            return this.sdata;
        }
        
        public Date getNominalTimeAsDate() {
            return DqcStationObsDataset.this.timeUnit.makeDate(this.getNominalTime());
        }
        
        public Date getObservationTimeAsDate() {
            return DqcStationObsDataset.this.timeUnit.makeDate(this.getObservationTime());
        }
    }
}
