// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.thredds;

import ucar.unidata.util.DatedThing;
import java.util.TimeZone;
import java.text.SimpleDateFormat;
import ucar.unidata.util.DateSelection;
import ucar.nc2.dt.DataIterator;
import ucar.nc2.units.DateType;
import thredds.catalog.InvCatalogImpl;
import thredds.catalog.InvCatalogFactory;
import ucar.unidata.util.DateUtil;
import thredds.catalog.InvAccess;
import ucar.nc2.dt.RadialDatasetSweep;
import java.util.Date;
import java.util.Collection;
import ucar.nc2.util.CancelTask;
import java.util.ArrayList;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.StationImpl;
import java.util.Iterator;
import ucar.nc2.units.DateUnit;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.Document;
import org.jdom.JDOMException;
import thredds.catalog.XMLEntityResolver;
import java.net.URISyntaxException;
import java.io.IOException;
import thredds.catalog.InvDataset;
import java.net.URI;
import org.jdom.Namespace;
import ucar.unidata.util.Product;
import ucar.unidata.geoloc.LatLonRect;
import java.util.List;
import ucar.unidata.geoloc.Station;
import java.util.HashMap;
import ucar.nc2.dt.radial.StationRadarCollectionImpl;

public class TDSRadarDatasetCollection extends StationRadarCollectionImpl
{
    protected HashMap<String, Station> stationHMap;
    private List<String> radarTimeSpan;
    private LatLonRect radarRegion;
    private String dsc_location;
    private List<Product> radarProducts;
    private String summary;
    protected static final Namespace defNS;
    private URI docURI;
    
    public static TDSRadarDatasetCollection factory(final InvDataset ds, final String dsc_location, final StringBuffer errlog) throws IOException, URISyntaxException {
        return factory(ds.getDocumentation("summary"), dsc_location, errlog);
    }
    
    public static TDSRadarDatasetCollection factory(final String desc, final String dsc_location, final StringBuffer errlog) throws IOException {
        Document doc = null;
        final XMLEntityResolver jaxp = new XMLEntityResolver(true);
        final SAXBuilder builder = jaxp.getSAXBuilder();
        try {
            doc = builder.build(dsc_location);
        }
        catch (JDOMException e) {
            errlog.append(e.toString());
        }
        final Element qcElem = doc.getRootElement();
        final Namespace ns = qcElem.getNamespace();
        return new TDSRadarDatasetCollection(desc, dsc_location, qcElem, ns, errlog);
    }
    
    private TDSRadarDatasetCollection(final String desc, final String dsc_location, final Element elem, final Namespace ns, final StringBuffer errlog) throws IOException {
        final Element serviceElem = this.readElements(elem, "service");
        final Element dsElem = this.readElements(elem, "dataset");
        final Element metaElem = this.readElements(dsElem, "metadata");
        final String sts = dsc_location.replaceFirst("dataset.xml", "stations.xml");
        final HashMap stationHMap = this.readRadarStations(sts);
        final LatLonRect radarRegion = this.readSelectRegion(metaElem, ns);
        final List<String> radarTimeSpan = this.readSelectTime(metaElem, ns);
        final List<Product> productList = this.readSelectVariable(metaElem, ns);
        final String summary = this.readSelectDocument(metaElem, ns);
        if (stationHMap == null) {
            errlog.append("TDSRadarDatasetCollection must have station selected");
            return;
        }
        if (radarRegion == null) {
            errlog.append("TDSRadarDatasetCollection must have region selected");
            return;
        }
        if (radarTimeSpan == null) {
            errlog.append("TDSRadarDatasetCollection must have time span selected");
            return;
        }
        this.desc = desc;
        this.dsc_location = dsc_location;
        this.radarProducts = productList;
        this.summary = summary;
        this.stationHMap = (HashMap<String, Station>)stationHMap;
        this.radarRegion = radarRegion;
        this.radarTimeSpan = radarTimeSpan;
        this.startDate = DateUnit.getStandardOrISO(radarTimeSpan.get(0));
        this.endDate = DateUnit.getStandardOrISO(radarTimeSpan.get(1));
        try {
            this.timeUnit = new DateUnit("hours since 1991-01-01T00:00");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public HashMap readRadarStations(final String stsXML_location) throws IOException {
        Document doc = null;
        final XMLEntityResolver jaxp = new XMLEntityResolver(true);
        final SAXBuilder builder = jaxp.getSAXBuilder();
        final HashMap stations = new HashMap();
        try {
            doc = builder.build(stsXML_location);
        }
        catch (JDOMException e) {
            e.printStackTrace();
        }
        final Element rootElem = doc.getRootElement();
        final List<Element> children = (List<Element>)rootElem.getChildren();
        for (final Element child : children) {
            final Station s;
            if (null != (s = this.readStation(child))) {
                stations.put(s.getName(), s);
            }
        }
        return stations;
    }
    
    public Element readElements(final Element elem, final String eleName) {
        final List<Element> children = (List<Element>)elem.getChildren();
        for (final Element child : children) {
            final String childName = child.getName();
            if (childName.equals(eleName)) {
                return child;
            }
        }
        return null;
    }
    
    private Station readStation(final Element elem) {
        final String name = elem.getAttributeValue("id");
        final Element desc = elem.getChild("name");
        final String descv = desc.getValue();
        final Element lat = elem.getChild("latitude");
        final String latv = lat.getValue();
        final Element lon = elem.getChild("longitude");
        final String lonv = lon.getValue();
        final Element alt = elem.getChild("elevation");
        final String altv = alt.getValue();
        final StationImpl station = new StationImpl(name, descv, "", Double.parseDouble(latv), Double.parseDouble(lonv), Double.parseDouble(altv));
        return station;
    }
    
    public LatLonRect readSelectRegion(final Element elem, final Namespace ns) {
        final Element region = elem.getChild("LatLonBox", ns);
        final Element north = region.getChild("north", ns);
        final String nv = north.getValue();
        final Element south = region.getChild("south", ns);
        final String sv = south.getValue();
        final Element east = region.getChild("east", ns);
        final String ev = east.getValue();
        final Element west = region.getChild("west", ns);
        final String wv = west.getValue();
        final LatLonPointImpl p1 = new LatLonPointImpl(Double.valueOf(sv), Double.valueOf(wv));
        final LatLonPointImpl p2 = new LatLonPointImpl(Double.valueOf(nv), Double.valueOf(ev));
        final LatLonRect llr = new LatLonRect(p1, p2);
        return llr;
    }
    
    public List<String> readSelectTime(final Element elem, final Namespace ns) {
        final Element region = elem.getChild("TimeSpan", ns);
        final List regionInfo = region.getChildren();
        final Element start = region.getChild("start", ns);
        final String sv = start.getValue();
        final Element end = region.getChild("end", ns);
        final String ev = end.getValue();
        final List<String> ll = new ArrayList<String>();
        ll.add(sv);
        ll.add(ev);
        return ll;
    }
    
    public List<Product> readSelectVariable(final Element elem, final Namespace ns) {
        final List<Product> varlist = new ArrayList<Product>();
        final Element v = elem.getChild("Variables", ns);
        final List<Element> varInfo = (List<Element>)v.getChildren();
        for (final Element p : varInfo) {
            final String id = p.getAttributeValue("name");
            if (id.contains("/")) {
                final String[] c = id.split("/");
                final Product s = new Product(c[0], c[1]);
                varlist.add(s);
            }
            else {
                final String name = p.getAttributeValue("vocabulary_name");
                final Product s = new Product(id, name);
                varlist.add(s);
            }
        }
        return varlist;
    }
    
    private String readSelectDocument(final Element elem, final Namespace ns) {
        final Element doc = elem.getChild("documentation", ns);
        return doc.getValue();
    }
    
    protected void setTimeUnits() {
    }
    
    @Override
    protected void setStartDate() {
    }
    
    @Override
    protected void setEndDate() {
    }
    
    @Override
    protected void setBoundingBox() {
    }
    
    @Override
    public String getTitle() {
        return this.summary;
    }
    
    @Override
    public String getLocation() {
        return this.dsc_location;
    }
    
    @Override
    public String getDescription() {
        return this.desc;
    }
    
    public LatLonRect getRadarsBoundingBox() {
        return this.radarRegion;
    }
    
    public List getRadarTimeSpan() {
        return this.radarTimeSpan;
    }
    
    public List getRadarProducts() {
        return this.radarProducts;
    }
    
    @Override
    public boolean checkStationProduct(final String sName, final Product product) {
        for (final Product s : this.radarProducts) {
            if (s.equals(product)) {
                return true;
            }
        }
        return false;
    }
    
    public boolean checkStationProduct(final Product product) {
        return this.checkStationProduct(null, product);
    }
    
    public int getStationProductCount(final String sName) {
        return this.radarProducts.size();
    }
    
    @Override
    public List<Station> getStations() throws IOException {
        return this.getRadarStations();
    }
    
    public List<Station> getRadarStations() {
        final List<Station> slist = new ArrayList<Station>();
        final Iterator it = this.stationHMap.values().iterator();
        while (it.hasNext()) {
            slist.add(it.next());
        }
        return slist;
    }
    
    public Station getRadarStation(final String sName) {
        return this.stationHMap.get(sName);
    }
    
    @Override
    public List<Station> getStations(final CancelTask cancel) throws IOException {
        return this.getStations(null, cancel);
    }
    
    @Override
    public List<Station> getStations(final LatLonRect boundingBox) throws IOException {
        return this.getStations(boundingBox, null);
    }
    
    @Override
    public List<Station> getStations(final LatLonRect boundingBox, final CancelTask cancel) throws IOException {
        final Collection<Station> sl = this.stationHMap.values();
        final List<Station> dsl = new ArrayList<Station>();
        if (!boundingBox.containedIn(this.radarRegion)) {
            return null;
        }
        for (final Station s : sl) {
            if (boundingBox.contains(s.getLatLon())) {
                dsl.add(s);
            }
            if (cancel != null && cancel.isCancel()) {
                return null;
            }
        }
        return dsl;
    }
    
    public RadialDatasetSweep getRadarDataset(final String stnName, final Date absTime) throws IOException {
        final InvDataset invdata = this.queryRadarStation(stnName, absTime);
        if (invdata == null) {
            throw new IOException("Invalid time selected: " + absTime.toString() + "\n");
        }
        final ThreddsDataFactory tdFactory = new ThreddsDataFactory();
        final ThreddsDataFactory.Result result = tdFactory.openFeatureDataset(invdata, null);
        return (RadialDatasetSweep)result.featureDataset;
    }
    
    public RadialDatasetSweep getRadarDataset(final String stnName, final String productID, final Date absTime) throws IOException {
        final InvDataset invdata = this.queryRadarStation(stnName, productID, absTime);
        if (invdata == null) {
            throw new IOException("Invalid time selected: " + absTime.toString() + "\n");
        }
        final ThreddsDataFactory tdFactory = new ThreddsDataFactory();
        final ThreddsDataFactory.Result result = tdFactory.openFeatureDataset(invdata, null);
        return (RadialDatasetSweep)result.featureDataset;
    }
    
    public URI getRadarDatasetURI(final String stnName, final Date absTime) throws IOException {
        final InvDataset invdata = this.queryRadarStation(stnName, absTime);
        final List<InvAccess> acess = invdata.getAccess();
        final InvAccess ia = acess.get(0);
        final URI ui = ia.getStandardUri();
        if (ui == null) {
            throw new IOException("Invalid time selected: " + absTime.toString() + "\n");
        }
        return ui;
    }
    
    private InvDataset queryRadarStation(final String stnName, final Date absTime) throws IOException {
        return this.queryRadarStation(stnName, (String)null, absTime);
    }
    
    private InvDataset queryRadarStation(final String stnName, final String productID, final Date absTime) throws IOException {
        final String stime = DateUtil.getTimeAsISO8601(absTime).replaceAll("GMT", "");
        final StringBuilder queryb = new StringBuilder();
        final String baseURI = this.dsc_location.replaceFirst("/dataset.xml", "?");
        queryb.append(baseURI);
        queryb.append("&stn=" + stnName);
        if (productID != null) {
            queryb.append("&var=" + productID);
        }
        if (absTime == null) {
            queryb.append("&time=present");
        }
        else {
            queryb.append("&time=" + stime);
        }
        URI catalogURI;
        try {
            catalogURI = new URI(queryb.toString());
        }
        catch (URISyntaxException e) {
            throw new IOException("** MalformedURLException on URL <>\n" + e.getMessage() + "\n");
        }
        final InvCatalogFactory factory = new InvCatalogFactory("default", false);
        final InvCatalogImpl catalog = factory.readXML(catalogURI);
        final StringBuilder buff = new StringBuilder();
        if (!catalog.check(buff)) {
            throw new IOException("Invalid catalog <" + catalogURI + ">\n" + buff.toString());
        }
        final List<InvDataset> datasets = catalog.getDatasets();
        final InvDataset idata = datasets.get(0);
        final List<InvDataset> dsets = idata.getDatasets();
        final InvDataset tdata = dsets.get(0);
        return tdata;
    }
    
    public URI getRadarDatasetURI(final String stnName, final String productID, final Date absTime) throws IOException {
        if (productID == null) {
            return this.getRadarDatasetURI(stnName, absTime);
        }
        final InvDataset invdata = this.queryRadarStation(stnName, productID, absTime);
        final List acess = invdata.getAccess();
        final InvAccess ia = acess.get(0);
        final URI ui = ia.getStandardUri();
        if (ui == null) {
            throw new IOException("Invalid time selected: " + absTime.toString() + "\n");
        }
        return ui;
    }
    
    private TDSRadarDatasetInfo queryRadarStation(final String stnName, final Date start, final Date end) throws IOException {
        return this.queryRadarStation(stnName, null, start, end);
    }
    
    private TDSRadarDatasetInfo queryRadarStation(final String stnName, final String productID, final Date start, final Date end) throws IOException {
        final StringBuilder queryb = new StringBuilder();
        final String baseURI = this.dsc_location.replaceFirst("/dataset.xml", "?");
        queryb.append(baseURI);
        queryb.append("&stn=" + stnName);
        if (productID != null) {
            queryb.append("&var=" + productID);
        }
        if (start == null && end == null) {
            queryb.append("&time=present");
        }
        else if (end == null) {
            final String stime = DateUtil.getTimeAsISO8601(start).replaceAll("GMT", "");
            queryb.append("&time_start=" + stime);
            queryb.append("&time_end=present");
        }
        else {
            final String stime = DateUtil.getTimeAsISO8601(start).replaceAll("GMT", "");
            final String etime = DateUtil.getTimeAsISO8601(end).replaceAll("GMT", "");
            queryb.append("&time_start=" + stime);
            queryb.append("&time_end=" + etime);
        }
        URI catalogURI;
        try {
            catalogURI = new URI(queryb.toString());
        }
        catch (URISyntaxException e) {
            throw new IOException("** MalformedURLException on URL <>\n" + e.getMessage() + "\n");
        }
        final InvCatalogFactory factory = new InvCatalogFactory("default", false);
        final InvCatalogImpl catalog = factory.readXML(catalogURI);
        final StringBuilder buff = new StringBuilder();
        if (!catalog.check(buff)) {
            throw new IOException("Invalid catalog <" + catalogURI + ">\n" + buff.toString());
        }
        final List<InvDataset> datasets = catalog.getDatasets();
        final InvDataset idata = datasets.get(0);
        final List<InvDataset> dsets = idata.getDatasets();
        final List<Date> absTimeList = new ArrayList<Date>();
        final List<DatasetURIInfo> dURIList = new ArrayList<DatasetURIInfo>();
        final List<InvDatasetInfo> dInvList = new ArrayList<InvDatasetInfo>();
        for (final InvDataset tdata : dsets) {
            final List<InvAccess> acess = tdata.getAccess();
            final List<DateType> dates = tdata.getDates();
            final InvAccess ia = acess.get(0);
            final URI d = ia.getStandardUri();
            final Date date = dates.get(0).getDate();
            absTimeList.add(date);
            dURIList.add(new DatasetURIInfo(d, date));
            dInvList.add(new InvDatasetInfo(tdata, date));
        }
        final TDSRadarDatasetInfo dri = new TDSRadarDatasetInfo(absTimeList, dURIList, dInvList);
        return dri;
    }
    
    public List getRadarStationURIs(final String stnName, final Date start, final Date end) throws IOException {
        final TDSRadarDatasetInfo dri = this.queryRadarStation(stnName, start, end);
        final List<DatasetURIInfo> uList = dri.getURIList();
        final List<URI> datasetsURI = new ArrayList<URI>();
        for (final DatasetURIInfo du : uList) {
            datasetsURI.add(du.uri);
        }
        return datasetsURI;
    }
    
    public List getRadarStationDatasets(final String stnName, final Date start, final Date end) throws IOException {
        final List datasetList = new ArrayList();
        final TDSRadarDatasetInfo dri = this.queryRadarStation(stnName, start, end);
        final List<InvDatasetInfo> iList = dri.getInvList();
        for (final InvDatasetInfo iv : iList) {
            final InvDataset tdata = iv.inv;
            final ThreddsDataFactory tdFactory = new ThreddsDataFactory();
            final ThreddsDataFactory.Result result = tdFactory.openFeatureDataset(tdata, null);
            datasetList.add(result.featureDataset);
        }
        return datasetList;
    }
    
    public List<Date> getRadarStationTimes(final String stnName, final Date start, final Date end) throws IOException {
        return this.getRadarStationTimes(stnName, null, start, end);
    }
    
    public List<Date> getRadarStationTimes(final String stnName, final String productID, final Date start, final Date end) throws IOException {
        final TDSRadarDatasetInfo dri = this.queryRadarStation(stnName, productID, start, end);
        return dri.getTimeList();
    }
    
    public DataIterator getDataIterator(final int bufferSize) throws IOException {
        return null;
    }
    
    private List queryRadarStationRTimes(final String stn) throws IOException {
        return this.radarTimeSpan;
    }
    
    public List getDataURIs(final String sName, final DateSelection dateInfo) throws IOException {
        return this.getDataURIs(sName, dateInfo, null);
    }
    
    public List getData(final String sName, final DateSelection dateInfo) throws IOException {
        return this.getData(sName, dateInfo, null);
    }
    
    public List getData(final String sName, final DateSelection dateSelect, final CancelTask cancel) throws IOException {
        if (cancel != null && cancel.isCancel()) {
            return null;
        }
        final TDSRadarDatasetInfo dri = this.queryRadarStation(sName, dateSelect.getStartFixedDate(), dateSelect.getEndFixedDate());
        final List datasetList = new ArrayList();
        final List datasetINVs = dateSelect.apply(dri.getInvList());
        for (final InvDatasetInfo ifo : datasetINVs) {
            final InvDataset tdata = ifo.inv;
            final ThreddsDataFactory tdFactory = new ThreddsDataFactory();
            final ThreddsDataFactory.Result result = tdFactory.openFeatureDataset(tdata, null);
            datasetList.add(result.featureDataset);
            if (cancel != null && cancel.isCancel()) {
                return null;
            }
        }
        return datasetList;
    }
    
    public List<URI> getDataURIs(final String sName, final DateSelection dateSelect, final CancelTask cancel) throws IOException {
        if (cancel != null && cancel.isCancel()) {
            return null;
        }
        final TDSRadarDatasetInfo dri = this.queryRadarStation(sName, dateSelect.getStartFixedDate(), dateSelect.getEndFixedDate());
        final List<DatasetURIInfo> datasetsURIs = (List<DatasetURIInfo>)dateSelect.apply(dri.getURIList());
        final List<URI> uriList = new ArrayList<URI>();
        for (final DatasetURIInfo ufo : datasetsURIs) {
            final URI u = ufo.uri;
            uriList.add(u);
            if (cancel != null && cancel.isCancel()) {
                return null;
            }
        }
        return uriList;
    }
    
    public static long roundTo(final long roundTo, final long seconds) {
        final int roundToSeconds = (int)roundTo;
        if (roundToSeconds == 0) {
            return seconds;
        }
        return seconds - (int)seconds % roundToSeconds;
    }
    
    public static void main(final String[] args) throws IOException {
        final StringBuffer errlog = new StringBuffer();
        String ds_location = null;
        TDSRadarDatasetCollection dsc = null;
        List stns = null;
        ds_location = "http://motherlode.ucar.edu:9080/thredds/radarServer/nexrad/level3/CCS039/dataset.xml";
        dsc = factory("test", ds_location, errlog);
        System.out.println(" errs= " + (Object)errlog);
        stns = dsc.getStations();
        System.out.println(" nstns= " + stns.size());
        stns = dsc.getStations();
        System.out.println(" nstns= " + stns.size());
        Station stn = dsc.getRadarStation("DVN");
        System.out.println("stn = " + stn);
        final List tl = dsc.getRadarTimeSpan();
        final Date ts1 = DateUnit.getStandardOrISO("1998-06-28T01:01:21Z");
        final Date ts2 = DateUnit.getStandardOrISO("1998-07-30T19:01:21Z");
        final List pd = dsc.getRadarProducts();
        final List<Date> tlist = dsc.getRadarStationTimes(stn.getName(), "BREF1", ts1, ts2);
        final int sz = tlist.size();
        for (int i = 0; i < 3; ++i) {
            final Date ts3 = tlist.get(i);
            final RadialDatasetSweep rds = dsc.getRadarDataset(stn.getName(), "BREF1", ts3);
            final int tt = 0;
        }
        final Date ts4 = tlist.get(0);
        final URI stURL = dsc.getRadarDatasetURI(stn.getName(), "BREF1", ts4);
        assert null != stURL;
        final DateSelection dateS = new DateSelection(ts1, ts2);
        dateS.setInterval(3600000.0);
        dateS.setRoundTo(3600000.0);
        dateS.setPreRange(500000.0);
        dateS.setPostRange(500000.0);
        for (int j = 0; j < stns.size(); ++j) {
            stn = stns.get(j);
            final List<Date> times = dsc.getRadarStationTimes(stn.getName(), new Date(System.currentTimeMillis() - 50065408L), new Date(System.currentTimeMillis()));
            if (times.size() > 0) {
                System.err.println(stn + " times:" + times.size() + " " + times.get(0) + " - " + times.get(times.size() - 1));
            }
            else {
                System.err.println(stn + " no times");
            }
        }
        final List jList = dsc.getDataURIs("KABX", dateS);
        assert null != jList;
        final List mList = dsc.getData("KABX", dateS, null);
        assert null != mList;
        final Date ts5 = tlist.get(1);
        final SimpleDateFormat isoDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        isoDateTimeFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        final String st = isoDateTimeFormat.format(ts5);
    }
    
    static {
        defNS = Namespace.getNamespace("http://www.unidata.ucar.edu/namespaces/thredds/queryCapability/v0.4");
    }
    
    public class DatasetURIInfo implements DatedThing
    {
        private URI uri;
        private Date date;
        
        public DatasetURIInfo(final URI u, final Date date) {
            this.uri = null;
            this.date = null;
            this.uri = u;
            this.date = date;
        }
        
        public Date getDate() {
            return this.date;
        }
    }
    
    public class InvDatasetInfo implements DatedThing
    {
        private InvDataset inv;
        private Date date;
        
        public InvDatasetInfo(final InvDataset u, final Date date) {
            this.inv = null;
            this.date = null;
            this.inv = u;
            this.date = date;
        }
        
        public Date getDate() {
            return this.date;
        }
    }
    
    public class TDSRadarDatasetInfo
    {
        private List absTimeList;
        private List datasetInfoList;
        private List invDatasetList;
        
        public TDSRadarDatasetInfo() {
        }
        
        public TDSRadarDatasetInfo(final List absTimeList, final List datasetInfoList, final List invDatasetList) {
            this.absTimeList = absTimeList;
            this.datasetInfoList = datasetInfoList;
            this.invDatasetList = invDatasetList;
        }
        
        public List<Date> getTimeList() {
            return (List<Date>)this.absTimeList;
        }
        
        public List<DatasetURIInfo> getURIList() {
            return (List<DatasetURIInfo>)this.datasetInfoList;
        }
        
        public List<InvDatasetInfo> getInvList() {
            return (List<InvDatasetInfo>)this.invDatasetList;
        }
    }
}
