// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.util.net;

import org.slf4j.LoggerFactory;
import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.protocol.ControllerThreadSocketFactory;
import org.apache.commons.httpclient.params.HttpConnectionParams;
import java.net.UnknownHostException;
import java.io.IOException;
import java.net.Socket;
import java.net.InetAddress;
import org.apache.commons.httpclient.HttpClientError;
import java.security.SecureRandom;
import javax.net.ssl.KeyManager;
import java.security.KeyStore;
import javax.net.ssl.TrustManager;
import javax.net.ssl.SSLContext;
import org.slf4j.Logger;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;

public class EasySSLProtocolSocketFactory implements ProtocolSocketFactory
{
    private static Logger logger;
    private SSLContext sslcontext;
    
    public EasySSLProtocolSocketFactory() {
        this.sslcontext = null;
    }
    
    private static SSLContext createEasySSLContext() {
        try {
            final SSLContext context = SSLContext.getInstance("SSL");
            context.init(null, new TrustManager[] { new EasyX509TrustManager(null) }, null);
            return context;
        }
        catch (Exception e) {
            EasySSLProtocolSocketFactory.logger.error("createEasySSLContext", e);
            throw new HttpClientError(e.toString());
        }
    }
    
    private SSLContext getSSLContext() {
        if (this.sslcontext == null) {
            this.sslcontext = createEasySSLContext();
        }
        return this.sslcontext;
    }
    
    public Socket createSocket(final String host, final int port, final InetAddress clientHost, final int clientPort) throws IOException, UnknownHostException {
        return this.getSSLContext().getSocketFactory().createSocket(host, port, clientHost, clientPort);
    }
    
    public Socket createSocket(final String host, final int port, final InetAddress localAddress, final int localPort, final HttpConnectionParams params) throws IOException, UnknownHostException, ConnectTimeoutException {
        if (params == null) {
            throw new IllegalArgumentException("Parameters may not be null");
        }
        final int timeout = params.getConnectionTimeout();
        if (timeout == 0) {
            return this.createSocket(host, port, localAddress, localPort);
        }
        return ControllerThreadSocketFactory.createSocket((ProtocolSocketFactory)this, host, port, localAddress, localPort, timeout);
    }
    
    public Socket createSocket(final String host, final int port) throws IOException, UnknownHostException {
        return this.getSSLContext().getSocketFactory().createSocket(host, port);
    }
    
    public Socket createSocket(final Socket socket, final String host, final int port, final boolean autoClose) throws IOException, UnknownHostException {
        return this.getSSLContext().getSocketFactory().createSocket(socket, host, port, autoClose);
    }
    
    static {
        EasySSLProtocolSocketFactory.logger = LoggerFactory.getLogger(EasySSLProtocolSocketFactory.class);
    }
}
