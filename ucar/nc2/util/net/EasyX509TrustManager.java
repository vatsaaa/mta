// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.util.net;

import org.slf4j.LoggerFactory;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.KeyStoreException;
import javax.net.ssl.TrustManager;
import java.security.NoSuchAlgorithmException;
import javax.net.ssl.TrustManagerFactory;
import java.security.KeyStore;
import org.slf4j.Logger;
import javax.net.ssl.X509TrustManager;

public class EasyX509TrustManager implements X509TrustManager
{
    private X509TrustManager standardTrustManager;
    private static Logger logger;
    
    public EasyX509TrustManager(final KeyStore keystore) throws NoSuchAlgorithmException, KeyStoreException {
        this.standardTrustManager = null;
        final TrustManagerFactory factory = TrustManagerFactory.getInstance("SunX509");
        factory.init(keystore);
        final TrustManager[] trustmanagers = factory.getTrustManagers();
        if (trustmanagers.length == 0) {
            throw new NoSuchAlgorithmException("SunX509 trust manager not supported");
        }
        this.standardTrustManager = (X509TrustManager)trustmanagers[0];
    }
    
    public void checkClientTrusted(final X509Certificate[] certificates, final String authType) throws CertificateException {
        this.standardTrustManager.checkClientTrusted(certificates, authType);
    }
    
    public void checkServerTrusted(final X509Certificate[] certificates, final String authType) throws CertificateException {
        if (certificates != null && EasyX509TrustManager.logger.isDebugEnabled()) {
            EasyX509TrustManager.logger.debug("Server certificate chain:");
            for (int i = 0; i < certificates.length; ++i) {
                EasyX509TrustManager.logger.debug("X509Certificate[" + i + "]=" + certificates[i]);
            }
        }
        if (certificates != null && certificates.length == 1) {
            final X509Certificate certificate = certificates[0];
            try {
                certificate.checkValidity();
            }
            catch (CertificateException e) {
                EasyX509TrustManager.logger.error(e.toString());
                throw e;
            }
            return;
        }
        this.standardTrustManager.checkServerTrusted(certificates, authType);
    }
    
    public X509Certificate[] getAcceptedIssuers() {
        return this.standardTrustManager.getAcceptedIssuers();
    }
    
    static {
        EasyX509TrustManager.logger = LoggerFactory.getLogger(EasyX509TrustManager.class);
    }
}
