// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.util.net;

import java.net.URISyntaxException;
import java.net.URI;
import java.net.MalformedURLException;
import java.net.URLStreamHandler;
import java.net.URL;
import java.util.HashMap;

public class URLStreamHandlerFactory implements java.net.URLStreamHandlerFactory
{
    private static HashMap hash;
    private static boolean installed;
    
    public static void install() {
        try {
            if (!URLStreamHandlerFactory.installed) {
                URL.setURLStreamHandlerFactory(new URLStreamHandlerFactory());
                URLStreamHandlerFactory.installed = true;
            }
        }
        catch (Error e) {
            System.out.println("Error installing URLStreamHandlerFactory " + e.getMessage());
        }
    }
    
    public static void register(final String protocol, final URLStreamHandler sh) {
        URLStreamHandlerFactory.hash.put(protocol.toLowerCase(), sh);
    }
    
    public static URL makeURL(final String urlString) throws MalformedURLException {
        return URLStreamHandlerFactory.installed ? new URL(urlString) : makeURL(null, urlString);
    }
    
    public static URL makeURL(final URL parent, final String urlString) throws MalformedURLException {
        if (URLStreamHandlerFactory.installed) {
            return new URL(parent, urlString);
        }
        try {
            final URI uri = new URI(urlString);
            final URLStreamHandler h = URLStreamHandlerFactory.hash.get(uri.getScheme().toLowerCase());
            return new URL(parent, urlString, h);
        }
        catch (URISyntaxException e) {
            throw new MalformedURLException(e.getMessage());
        }
    }
    
    public URLStreamHandler createURLStreamHandler(final String protocol) {
        return URLStreamHandlerFactory.hash.get(protocol.toLowerCase());
    }
    
    static {
        URLStreamHandlerFactory.hash = new HashMap();
        URLStreamHandlerFactory.installed = false;
    }
}
