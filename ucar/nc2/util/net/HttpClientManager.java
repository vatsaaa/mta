// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.util.net;

import org.apache.commons.httpclient.params.HttpMethodParams;
import java.util.Formatter;
import java.io.File;
import java.io.OutputStream;
import ucar.nc2.util.IO;
import java.io.ByteArrayOutputStream;
import org.apache.commons.httpclient.HttpMethodBase;
import java.util.zip.GZIPInputStream;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.zip.InflaterInputStream;
import java.io.ByteArrayInputStream;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.methods.PutMethod;
import java.io.IOException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.auth.CredentialsProvider;
import org.apache.commons.httpclient.HttpClient;

public class HttpClientManager
{
    private static boolean debug;
    private static HttpClient _client;
    private static int timeout;
    
    public static HttpClient init(final CredentialsProvider provider, final String userAgent) {
        initHttpClient();
        if (provider != null) {
            HttpClientManager._client.getParams().setParameter("http.authentication.credential-provider", (Object)provider);
        }
        if (userAgent != null) {
            HttpClientManager._client.getParams().setParameter("http.useragent", (Object)(userAgent + "/NetcdfJava/HttpClient"));
        }
        else {
            HttpClientManager._client.getParams().setParameter("http.useragent", (Object)"NetcdfJava/HttpClient");
        }
        final String proxyHost = System.getProperty("http.proxyHost");
        final String proxyPort = System.getProperty("http.proxyPort");
        if (proxyHost != null && proxyPort != null && !proxyPort.trim().equals("")) {
            HttpClientManager._client.getHostConfiguration().setProxy(proxyHost, Integer.parseInt(proxyPort));
        }
        return HttpClientManager._client;
    }
    
    public static HttpClient getHttpClient() {
        return HttpClientManager._client;
    }
    
    private static synchronized void initHttpClient() {
        if (HttpClientManager._client != null) {
            return;
        }
        final MultiThreadedHttpConnectionManager connectionManager = new MultiThreadedHttpConnectionManager();
        HttpClientManager._client = new HttpClient((HttpConnectionManager)connectionManager);
        final HttpClientParams params = HttpClientManager._client.getParams();
        params.setParameter("http.socket.timeout", (Object)HttpClientManager.timeout);
        params.setParameter("http.protocol.allow-circular-redirects", (Object)Boolean.TRUE);
        params.setParameter("http.protocol.cookie-policy", (Object)"rfc2109");
        Protocol.registerProtocol("https", new Protocol("https", (ProtocolSocketFactory)new EasySSLProtocolSocketFactory(), 8443));
    }
    
    public static void clearState() {
        HttpClientManager._client.getState().clearCookies();
        HttpClientManager._client.getState().clearCredentials();
    }
    
    public static String getContent(final String urlString) throws IOException {
        final GetMethod m = new GetMethod(urlString);
        m.setFollowRedirects(true);
        try {
            HttpClientManager._client.executeMethod((HttpMethod)m);
            return m.getResponseBodyAsString();
        }
        finally {
            m.releaseConnection();
        }
    }
    
    public static int putContent(final String urlString, final String content) throws IOException {
        final PutMethod m = new PutMethod(urlString);
        m.setDoAuthentication(true);
        try {
            m.setRequestEntity((RequestEntity)new StringRequestEntity(content));
            HttpClientManager._client.executeMethod((HttpMethod)m);
            int resultCode = m.getStatusCode();
            if (resultCode == 302) {
                final Header locationHeader = m.getResponseHeader("location");
                if (locationHeader != null) {
                    final String redirectLocation = locationHeader.getValue();
                    if (HttpClientManager.debug) {
                        System.out.println("***Follow Redirection = " + redirectLocation);
                    }
                    resultCode = putContent(redirectLocation, content);
                }
            }
            return resultCode;
        }
        finally {
            m.releaseConnection();
        }
    }
    
    public static String getUrlContents(final String urlString, final int maxKbytes) {
        final HttpMethodBase m = (HttpMethodBase)new GetMethod(urlString);
        m.setFollowRedirects(true);
        m.setRequestHeader("Accept-Encoding", "gzip,deflate");
        try {
            final int status = HttpClientManager._client.executeMethod((HttpMethod)m);
            if (status != 200) {
                throw new RuntimeException("failed status = " + status);
            }
            String charset = m.getResponseCharSet();
            if (charset == null) {
                charset = "UTF-8";
            }
            final Header h = m.getResponseHeader("content-encoding");
            final String encoding = (h == null) ? null : h.getValue();
            if (encoding != null && encoding.equals("deflate")) {
                final byte[] body = m.getResponseBody();
                final InputStream is = new BufferedInputStream(new InflaterInputStream(new ByteArrayInputStream(body)), 10000);
                return readContents(is, charset, maxKbytes);
            }
            if (encoding != null && encoding.equals("gzip")) {
                final byte[] body = m.getResponseBody();
                final InputStream is = new BufferedInputStream(new GZIPInputStream(new ByteArrayInputStream(body)), 10000);
                return readContents(is, charset, maxKbytes);
            }
            final byte[] body = m.getResponseBody(maxKbytes * 1000);
            return new String(body, charset);
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        finally {
            m.releaseConnection();
        }
    }
    
    private static String readContents(final InputStream is, final String charset, final int maxKbytes) throws IOException {
        final ByteArrayOutputStream bout = new ByteArrayOutputStream(1000 * maxKbytes);
        IO.copy(is, bout, 1000 * maxKbytes);
        return bout.toString(charset);
    }
    
    public static void copyUrlContentsToFile(final String urlString, final File file) {
        final HttpMethodBase m = (HttpMethodBase)new GetMethod(urlString);
        m.setFollowRedirects(true);
        m.setRequestHeader("Accept-Encoding", "gzip,deflate");
        try {
            final int status = HttpClientManager._client.executeMethod((HttpMethod)m);
            if (status != 200) {
                throw new RuntimeException("failed status = " + status);
            }
            String charset = m.getResponseCharSet();
            if (charset == null) {
                charset = "UTF-8";
            }
            final Header h = m.getResponseHeader("content-encoding");
            final String encoding = (h == null) ? null : h.getValue();
            if (encoding != null && encoding.equals("deflate")) {
                final InputStream is = new BufferedInputStream(new InflaterInputStream(m.getResponseBodyAsStream()), 10000);
                IO.writeToFile(is, file.getPath());
            }
            else if (encoding != null && encoding.equals("gzip")) {
                final InputStream is = new BufferedInputStream(new GZIPInputStream(m.getResponseBodyAsStream()), 10000);
                IO.writeToFile(is, file.getPath());
            }
            else {
                IO.writeToFile(m.getResponseBodyAsStream(), file.getPath());
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            m.releaseConnection();
        }
    }
    
    public static long appendUrlContentsToFile(final String urlString, final File file, final long start, final long end) {
        long nbytes = 0L;
        final HttpMethodBase m = (HttpMethodBase)new GetMethod(urlString);
        m.setRequestHeader("Accept-Encoding", "gzip,deflate");
        m.setFollowRedirects(true);
        m.setRequestHeader("Range", "bytes=" + start + "-" + end);
        try {
            final int status = HttpClientManager._client.executeMethod((HttpMethod)m);
            if (status != 200 && status != 206) {
                throw new RuntimeException("failed status = " + status);
            }
            String charset = m.getResponseCharSet();
            if (charset == null) {
                charset = "UTF-8";
            }
            final Header h = m.getResponseHeader("content-encoding");
            final String encoding = (h == null) ? null : h.getValue();
            if (encoding != null && encoding.equals("deflate")) {
                final InputStream is = new BufferedInputStream(new InflaterInputStream(m.getResponseBodyAsStream()), 10000);
                nbytes = IO.appendToFile(is, file.getPath());
            }
            else if (encoding != null && encoding.equals("gzip")) {
                final InputStream is = new BufferedInputStream(new GZIPInputStream(m.getResponseBodyAsStream()), 10000);
                nbytes = IO.appendToFile(is, file.getPath());
            }
            else {
                nbytes = IO.appendToFile(m.getResponseBodyAsStream(), file.getPath());
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            m.releaseConnection();
        }
        return nbytes;
    }
    
    public static void showHttpRequestInfo(final Formatter f, final HttpMethodBase m) {
        f.format("HttpClient request %s %s %n", m.getName(), m.getPath());
        f.format("   do Authentication=%s%n", m.getDoAuthentication());
        f.format("   follow Redirects =%s%n", m.getFollowRedirects());
        f.format("   effectiveVersion =%s%n", m.getEffectiveVersion());
        f.format("   hostAuthState    =%s%n", m.getHostAuthState());
        final HttpMethodParams p = m.getParams();
        f.format("   cookie policy    =%s%n", p.getCookiePolicy());
        f.format("   http version     =%s%n", p.getVersion());
        f.format("   timeout (msecs)  =%d%n", p.getSoTimeout());
        f.format("   virtual host     =%s%n", p.getVirtualHost());
        f.format("Request Headers = %n", new Object[0]);
        final Header[] heads = m.getRequestHeaders();
        for (int i = 0; i < heads.length; ++i) {
            f.format("  %s", heads[i]);
        }
        f.format("%n", new Object[0]);
    }
    
    public static void showHttpResponseInfo(final Formatter f, final HttpMethodBase m) {
        f.format("HttpClient response status = %s%n", m.getStatusLine());
        f.format("Reponse Headers = %n", new Object[0]);
        final Header[] heads = m.getResponseHeaders();
        for (int i = 0; i < heads.length; ++i) {
            f.format("  %s", heads[i]);
        }
        f.format("%n", new Object[0]);
    }
    
    static {
        HttpClientManager.debug = false;
        HttpClientManager.timeout = 0;
    }
}
