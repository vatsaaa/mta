// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.util;

import java.util.Map;
import java.util.LinkedHashMap;

public class HashMapLRU extends LinkedHashMap
{
    private int max_entries;
    
    public HashMapLRU(final int initialCapacity, final int max_entries) {
        super(initialCapacity, 0.5f, true);
        this.max_entries = 100;
        this.max_entries = max_entries;
    }
    
    @Override
    protected boolean removeEldestEntry(final Map.Entry eldest) {
        return this.size() > this.max_entries;
    }
}
