// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.util;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Date;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.io.PrintStream;
import java.util.Random;
import java.io.File;
import ucar.unidata.util.StringUtil;
import java.util.TimerTask;
import java.util.Calendar;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import java.util.Timer;

public class DiskCache2
{
    public static int CACHEPATH_POLICY_ONE_DIRECTORY;
    public static int CACHEPATH_POLICY_NESTED_DIRECTORY;
    public static int CACHEPATH_POLICY_NESTED_TRUNCATE;
    private int cachePathPolicy;
    private String cachePathPolicyParam;
    private String root;
    private int persistMinutes;
    private Timer timer;
    private Logger cacheLog;
    
    public DiskCache2(String root, final boolean reletiveToHome, final int persistMinutes, final int scourEveryMinutes) {
        this.cachePathPolicyParam = null;
        this.root = null;
        this.cacheLog = LoggerFactory.getLogger("cacheLogger");
        this.persistMinutes = persistMinutes;
        if (reletiveToHome) {
            String home = System.getProperty("nj22.cachePersistRoot");
            if (home == null) {
                home = System.getProperty("user.home");
            }
            if (home == null) {
                home = System.getProperty("user.dir");
            }
            if (home == null) {
                home = ".";
            }
            if (!home.endsWith("/")) {
                home += "/";
            }
            root = home + root;
        }
        this.setRootDirectory(root);
        if (scourEveryMinutes > 0) {
            this.timer = new Timer("DiskCache-" + root);
            final Calendar c = Calendar.getInstance();
            c.add(12, scourEveryMinutes);
            this.timer.scheduleAtFixedRate(new CacheScourTask(), c.getTime(), 60000L * scourEveryMinutes);
        }
    }
    
    public void exit() {
        if (this.timer != null) {
            this.timer.cancel();
        }
    }
    
    public void setLogger(final Logger cacheLog) {
        this.cacheLog = cacheLog;
    }
    
    public void setRootDirectory(String cacheDir) {
        if (!cacheDir.endsWith("/")) {
            cacheDir += "/";
        }
        this.root = StringUtil.replace(cacheDir, '\\', "/");
        final File dir = new File(this.root);
        dir.mkdirs();
        if (!dir.exists()) {
            this.cacheLog.error("Failed to create directory " + this.root);
        }
    }
    
    public String getRootDirectory() {
        return this.root;
    }
    
    public File getCacheFile(final String fileLocation) {
        final File f = new File(this.makeCachePath(fileLocation));
        if (this.cachePathPolicy == DiskCache2.CACHEPATH_POLICY_NESTED_DIRECTORY) {
            final File dir = f.getParentFile();
            dir.mkdirs();
        }
        return f;
    }
    
    public synchronized File createUniqueFile(final String prefix, String suffix) {
        if (suffix == null) {
            suffix = ".tmp";
        }
        Random random;
        File result;
        for (random = new Random(System.currentTimeMillis()), result = new File(this.getRootDirectory(), prefix + Integer.toString(random.nextInt()) + suffix); result.exists(); result = new File(this.getRootDirectory(), prefix + Integer.toString(random.nextInt()) + suffix)) {}
        return result;
    }
    
    public void setCachePathPolicy(final int cachePathPolicy, final String cachePathPolicyParam) {
        this.cachePathPolicy = cachePathPolicy;
        this.cachePathPolicyParam = cachePathPolicyParam;
    }
    
    public void setPolicy(final int cachePathPolicy) {
        this.cachePathPolicy = cachePathPolicy;
    }
    
    private String makeCachePath(final String fileLocation) {
        String cachePath = StringUtil.remove(fileLocation, 58);
        cachePath = StringUtil.remove(cachePath, 63);
        cachePath = StringUtil.remove(cachePath, 61);
        cachePath = StringUtil.replace(cachePath, '\\', "/");
        if (cachePath.startsWith("/")) {
            cachePath = cachePath.substring(1);
        }
        if (cachePath.endsWith("/")) {
            cachePath = cachePath.substring(0, cachePath.length() - 1);
        }
        if (this.cachePathPolicy == DiskCache2.CACHEPATH_POLICY_ONE_DIRECTORY) {
            cachePath = StringUtil.replace(cachePath, '/', "-");
        }
        else if (this.cachePathPolicy == DiskCache2.CACHEPATH_POLICY_NESTED_TRUNCATE) {
            final int pos = cachePath.indexOf(this.cachePathPolicyParam);
            if (pos >= 0) {
                cachePath = cachePath.substring(pos + this.cachePathPolicyParam.length());
            }
            if (cachePath.startsWith("/")) {
                cachePath = cachePath.substring(1);
            }
        }
        if (this.cachePathPolicy != DiskCache2.CACHEPATH_POLICY_ONE_DIRECTORY) {
            final File file = new File(this.root + cachePath);
            final File parent = file.getParentFile();
            if (!parent.exists()) {
                parent.mkdirs();
            }
        }
        return this.root + cachePath;
    }
    
    public void showCache(final PrintStream pw) {
        pw.println("Cache files");
        pw.println("Size   LastModified       Filename");
        final File dir = new File(this.root);
        final File[] arr$;
        final File[] files = arr$ = dir.listFiles();
        for (final File file : arr$) {
            String org = null;
            try {
                org = URLDecoder.decode(file.getName(), "UTF8");
            }
            catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            pw.println(" " + file.length() + " " + new Date(file.lastModified()) + " " + org);
        }
    }
    
    public void cleanCache(final File dir, final StringBuilder sbuff, final boolean isRoot) {
        final long now = System.currentTimeMillis();
        final File[] files = dir.listFiles();
        if (files == null) {
            throw new IllegalStateException("DiskCache2: not a directory or I/O error on dir=" + dir.getAbsolutePath());
        }
        if (!isRoot && files.length == 0) {
            long duration = now - dir.lastModified();
            duration /= 60000L;
            if (duration > this.persistMinutes) {
                dir.delete();
                if (sbuff != null) {
                    sbuff.append(" deleted ").append(dir.getPath()).append(" last= ").append(new Date(dir.lastModified())).append("\n");
                }
            }
            return;
        }
        for (final File file : files) {
            if (file.isDirectory()) {
                this.cleanCache(file, sbuff, false);
            }
            else {
                long duration2 = now - file.lastModified();
                duration2 /= 60000L;
                if (duration2 > this.persistMinutes) {
                    file.delete();
                    if (sbuff != null) {
                        sbuff.append(" deleted ").append(file.getPath()).append(" last= ").append(new Date(file.lastModified())).append("\n");
                    }
                }
            }
        }
    }
    
    static void make(final DiskCache2 dc, final String filename) throws IOException {
        final File want = dc.getCacheFile(filename);
        System.out.println("make=" + want.getPath() + "; exists = " + want.exists());
        if (!want.exists()) {
            want.createNewFile();
        }
        System.out.println(" canRead= " + want.canRead() + " canWrite = " + want.canWrite() + " lastMod = " + new Date(want.lastModified()));
        try {
            final String enc = URLEncoder.encode(filename, "UTF8");
            System.out.println(" original=" + URLDecoder.decode(enc, "UTF8"));
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
    
    public static void main(final String[] args) throws IOException {
        final DiskCache2 dc = new DiskCache2("C:/TEMP/test/", false, 0, 0);
        dc.setRootDirectory("C:/temp/chill/");
        make(dc, "C:/junk.txt");
        make(dc, "C:/some/enchanted/evening/joots+3478.txt");
        make(dc, "http://www.unidata.ucar.edu/some/enc hanted/eve'ning/nowrite.gibberish");
        dc.showCache(System.out);
        final StringBuilder sbuff = new StringBuilder();
        dc.cleanCache(new File(dc.getRootDirectory()), sbuff, true);
        System.out.println(sbuff);
    }
    
    static {
        DiskCache2.CACHEPATH_POLICY_ONE_DIRECTORY = 0;
        DiskCache2.CACHEPATH_POLICY_NESTED_DIRECTORY = 1;
        DiskCache2.CACHEPATH_POLICY_NESTED_TRUNCATE = 2;
    }
    
    private class CacheScourTask extends TimerTask
    {
        CacheScourTask() {
        }
        
        @Override
        public void run() {
            final StringBuilder sbuff = new StringBuilder();
            sbuff.append("CacheScourTask on").append(DiskCache2.this.root).append("\n");
            DiskCache2.this.cleanCache(new File(DiskCache2.this.root), sbuff, true);
            sbuff.append("----------------------\n");
            if (DiskCache2.this.cacheLog != null) {
                DiskCache2.this.cacheLog.info(sbuff.toString());
            }
        }
    }
}
