// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.util.xml;

import java.util.List;
import ucar.nc2.iosp.bufr.tables.BufrTables;
import ucar.grib.grib2.ParameterTable;
import ucar.grib.grib1.GribPDSParamTable;
import ucar.nc2.dt.TypedDatasetFactory;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.dataset.CoordTransBuilder;
import ucar.nc2.dataset.CoordSysBuilder;
import ucar.nc2.NetcdfFile;
import org.jdom.Element;
import org.jdom.Document;
import org.jdom.JDOMException;
import java.io.IOException;
import org.jdom.input.SAXBuilder;
import java.io.InputStream;

public class RuntimeConfigParser
{
    public static void read(final InputStream is, final StringBuilder errlog) throws IOException {
        final SAXBuilder saxBuilder = new SAXBuilder();
        Document doc;
        try {
            doc = saxBuilder.build(is);
        }
        catch (JDOMException e) {
            throw new IOException(e.getMessage());
        }
        read(doc.getRootElement(), errlog);
    }
    
    public static void read(final Element root, final StringBuilder errlog) {
        final List children = root.getChildren();
        for (int i = 0; i < children.size(); ++i) {
            final Element elem = children.get(i);
            if (elem.getName().equals("ioServiceProvider")) {
                final String className = elem.getAttributeValue("class");
                try {
                    NetcdfFile.registerIOProvider(className);
                }
                catch (ClassNotFoundException e4) {
                    errlog.append("CoordSysBuilder class not found= " + className + "; check your classpath\n");
                }
                catch (Exception e) {
                    errlog.append("IOServiceProvider " + className + " failed= " + e.getMessage() + "\n");
                }
            }
            else if (elem.getName().equals("coordSysBuilder")) {
                final String conventionName = elem.getAttributeValue("convention");
                final String className2 = elem.getAttributeValue("class");
                try {
                    CoordSysBuilder.registerConvention(conventionName, className2);
                }
                catch (ClassNotFoundException e5) {
                    errlog.append("CoordSysBuilder class not found= " + className2 + "; check your classpath\n");
                }
                catch (Exception e2) {
                    errlog.append("CoordSysBuilder " + className2 + " failed= " + e2.getMessage() + "\n");
                }
            }
            else if (elem.getName().equals("coordTransBuilder")) {
                final String transformName = elem.getAttributeValue("name");
                final String className2 = elem.getAttributeValue("class");
                try {
                    CoordTransBuilder.registerTransform(transformName, className2);
                }
                catch (ClassNotFoundException e5) {
                    errlog.append("CoordSysBuilder class not found= " + className2 + "; check your classpath\n");
                }
                catch (Exception e2) {
                    errlog.append("CoordTransBuilder " + className2 + " failed= " + e2.getMessage() + "\n");
                }
            }
            else if (elem.getName().equals("typedDatasetFactory")) {
                final String typeName = elem.getAttributeValue("datatype");
                final String className2 = elem.getAttributeValue("class");
                final FeatureType datatype = FeatureType.valueOf(typeName.toUpperCase());
                if (null == datatype) {
                    errlog.append("TypedDatasetFactory " + className2 + " unknown datatype= " + typeName + "\n");
                }
                else {
                    try {
                        TypedDatasetFactory.registerFactory(datatype, className2);
                    }
                    catch (ClassNotFoundException e6) {
                        errlog.append("CoordSysBuilder class not found= " + className2 + "; check your classpath\n");
                    }
                    catch (Exception e3) {
                        errlog.append("TypedDatasetFactory " + className2 + " failed= " + e3.getMessage() + "\n");
                    }
                }
            }
            else if (elem.getName().equals("table")) {
                final String type = elem.getAttributeValue("type");
                final String filename = elem.getAttributeValue("filename");
                if (type == null || filename == null) {
                    errlog.append("table element must have both type and filename attributes\n");
                }
                else {
                    try {
                        if (type.equalsIgnoreCase("GRIB1")) {
                            GribPDSParamTable.addParameterUserLookup(filename);
                        }
                        else if (type.equalsIgnoreCase("GRIB2")) {
                            ParameterTable.addParametersUser(filename);
                        }
                        else {
                            errlog.append("Unknown table type " + type + "\n");
                        }
                    }
                    catch (Exception e2) {
                        errlog.append("table read failed on  " + filename + " = " + e2.getMessage() + "\n");
                    }
                }
            }
            else if (elem.getName().equals("bufrtable")) {
                final String filename2 = elem.getAttributeValue("filename");
                try {
                    BufrTables.addLookupFile(filename2);
                }
                catch (Exception e) {
                    errlog.append("bufrtable read failed on  " + filename2 + " = " + e.getMessage() + "\n");
                }
            }
        }
    }
}
