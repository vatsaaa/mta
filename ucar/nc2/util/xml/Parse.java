// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.util.xml;

import ucar.unidata.util.StringUtil;
import org.jdom.Verifier;
import org.jdom.Document;
import org.jdom.JDOMException;
import java.io.IOException;
import org.jdom.input.SAXBuilder;
import org.jdom.Element;

public class Parse
{
    private static char[] xmlInC;
    private static String[] xmlOutC;
    
    public static Element readRootElement(final String location) throws IOException {
        Document doc;
        try {
            final SAXBuilder builder = new SAXBuilder();
            doc = builder.build(location);
        }
        catch (JDOMException e) {
            throw new IOException(e.getMessage());
        }
        return doc.getRootElement();
    }
    
    public static String cleanCharacterData(final String text) {
        if (text == null) {
            return text;
        }
        boolean bad = false;
        for (int i = 0, len = text.length(); i < len; ++i) {
            final int ch = text.charAt(i);
            if (!Verifier.isXMLCharacter(ch)) {
                bad = true;
                break;
            }
        }
        if (!bad) {
            return text;
        }
        final StringBuilder sbuff = new StringBuilder(text.length());
        for (int j = 0, len2 = text.length(); j < len2; ++j) {
            final int ch2 = text.charAt(j);
            if (Verifier.isXMLCharacter(ch2)) {
                sbuff.append(ch2);
            }
        }
        return sbuff.toString();
    }
    
    public static String quoteXmlContent(final String x) {
        return StringUtil.replace(x, Parse.xmlInC, Parse.xmlOutC);
    }
    
    public static String unquoteXmlContent(final String x) {
        return StringUtil.unreplace(x, Parse.xmlOutC, Parse.xmlInC);
    }
    
    static {
        Parse.xmlInC = new char[] { '&', '<', '>' };
        Parse.xmlOutC = new String[] { "&amp;", "&lt;", "&gt;" };
    }
}
