// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.util;

import java.util.StringTokenizer;
import java.util.HashMap;
import java.util.Map;

public class DebugFlagsImpl implements DebugFlags
{
    private Map<String, Boolean> map;
    
    public DebugFlagsImpl(final String flagsOn) {
        this.map = new HashMap<String, Boolean>();
        final StringTokenizer stoke = new StringTokenizer(flagsOn);
        while (stoke.hasMoreTokens()) {
            this.set(stoke.nextToken(), true);
        }
    }
    
    public boolean isSet(final String flagName) {
        final Boolean b = this.map.get(flagName);
        return b != null && b;
    }
    
    public void set(final String flagName, final boolean value) {
        this.map.put(flagName, value);
    }
}
