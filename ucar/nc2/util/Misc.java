// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.util;

import java.util.Formatter;

public class Misc
{
    private static boolean show;
    private static double maxReletiveError;
    
    public static boolean closeEnough(final double v1, final double v2, final double tol) {
        if (Misc.show) {
            final double d1 = Math.abs(v1 - v2);
            final double d2 = Math.abs(v1 / v2);
            final double d3 = Math.abs(v1 / v2 - 1.0);
            System.out.println("v1= " + v1 + " v2=" + v2 + " diff=" + d1 + " abs(v1/v2)=" + d2 + " abs(v1/v2-1)=" + d3);
        }
        final double diff = (v2 == 0.0) ? Math.abs(v1 - v2) : Math.abs(v1 / v2 - 1.0);
        return diff < tol;
    }
    
    public static boolean closeEnough(final double v1, final double v2) {
        if (v1 == v2) {
            return true;
        }
        final double diff = (v2 == 0.0) ? Math.abs(v1 - v2) : Math.abs(v1 / v2 - 1.0);
        return diff < Misc.maxReletiveError;
    }
    
    public static boolean closeEnough(final float v1, final float v2) {
        if (v1 == v2) {
            return true;
        }
        final double diff = (v2 == 0.0) ? Math.abs(v1 - v2) : ((double)Math.abs(v1 / v2 - 1.0f));
        return diff < Misc.maxReletiveError;
    }
    
    public static void main(final String[] args) {
        final long val1 = -1L;
        final long val2 = 234872309L;
        final int val3 = 2348;
        final int val4 = 32;
        final Formatter f = new Formatter(System.out);
        f.format("  address            dataPos            offset size\n", new Object[0]);
        f.format("  %#-18x %#-18x %5d  %4d%n", val1, val2, val3, val4);
    }
    
    static {
        Misc.show = false;
        Misc.maxReletiveError = 1.0E-6;
    }
}
