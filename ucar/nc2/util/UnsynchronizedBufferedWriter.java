// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.util;

import java.io.Reader;
import java.io.IOException;
import java.io.Writer;

public class UnsynchronizedBufferedWriter extends Writer
{
    private static final int CAPACITY = 8192;
    private char[] buffer;
    private int position;
    private Writer out;
    private boolean closed;
    
    public UnsynchronizedBufferedWriter(final Writer out) {
        this.buffer = new char[8192];
        this.position = 0;
        this.closed = false;
        this.out = out;
    }
    
    @Override
    public void write(final char[] text, int offset, int length) throws IOException {
        this.checkClosed();
        while (length > 0) {
            final int n = Math.min(8192 - this.position, length);
            System.arraycopy(text, offset, this.buffer, this.position, n);
            this.position += n;
            offset += n;
            length -= n;
            if (this.position >= 8192) {
                this.flushInternal();
            }
        }
    }
    
    public void write(final Reader reader) throws IOException {
        this.checkClosed();
        while (true) {
            final int n = reader.read(this.buffer);
            if (n < 0) {
                break;
            }
            this.out.write(this.buffer, 0, n);
        }
        this.out.flush();
    }
    
    @Override
    public void write(final String s) throws IOException {
        this.write(s, 0, s.length());
    }
    
    @Override
    public void write(final String s, int offset, int length) throws IOException {
        this.checkClosed();
        while (length > 0) {
            final int n = Math.min(8192 - this.position, length);
            s.getChars(offset, offset + n, this.buffer, this.position);
            this.position += n;
            offset += n;
            length -= n;
            if (this.position >= 8192) {
                this.flushInternal();
            }
        }
    }
    
    @Override
    public void write(final int c) throws IOException {
        this.checkClosed();
        if (this.position >= 8192) {
            this.flushInternal();
        }
        this.buffer[this.position] = (char)c;
        ++this.position;
    }
    
    @Override
    public void flush() throws IOException {
        this.flushInternal();
        this.out.flush();
    }
    
    private void flushInternal() throws IOException {
        if (this.position != 0) {
            this.out.write(this.buffer, 0, this.position);
            this.position = 0;
        }
    }
    
    @Override
    public void close() throws IOException {
        this.closed = true;
        this.flush();
        this.out.close();
    }
    
    private void checkClosed() throws IOException {
        if (this.closed) {
            throw new IOException("Writer is closed");
        }
    }
}
