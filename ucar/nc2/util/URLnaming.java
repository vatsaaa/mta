// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.util;

import java.net.URL;
import java.io.File;
import java.net.URISyntaxException;
import java.net.URI;

public class URLnaming
{
    static final /* synthetic */ boolean $assertionsDisabled;
    
    public static String escapeQuery(String urlString) throws URISyntaxException {
        urlString = urlString.trim();
        final URI uri = new URI(urlString);
        return uri.toASCIIString();
    }
    
    public static String resolve(final String baseUri, final String relativeUri) {
        if (baseUri == null || relativeUri == null) {
            return relativeUri;
        }
        if (relativeUri.startsWith("file:")) {
            return relativeUri;
        }
        if (baseUri.startsWith("file:")) {
            try {
                final URI reletiveURI = URI.create(relativeUri);
                if (reletiveURI.isAbsolute()) {
                    return relativeUri;
                }
            }
            catch (Exception ex) {}
            if (relativeUri.length() > 0 && relativeUri.charAt(0) == '#') {
                return baseUri + relativeUri;
            }
            if (relativeUri.length() > 0 && relativeUri.charAt(0) == '/') {
                return relativeUri;
            }
            final int pos = baseUri.lastIndexOf(47);
            if (pos > 0) {
                return baseUri.substring(0, pos + 1) + relativeUri;
            }
        }
        final URI reletiveURI = URI.create(relativeUri);
        if (reletiveURI.isAbsolute()) {
            return relativeUri;
        }
        final URI baseURI = URI.create(baseUri);
        final URI resolvedURI = baseURI.resolve(reletiveURI);
        return resolvedURI.toASCIIString();
    }
    
    public static String canonicalizeRead(final String location) {
        try {
            final URI refURI = URI.create(location);
            if (refURI.isAbsolute()) {
                return location;
            }
        }
        catch (Exception e) {
            return "file:" + location;
        }
        return location;
    }
    
    public static String canonicalizeWrite(final String location) {
        try {
            final URI refURI = URI.create(location);
            if (refURI.isAbsolute()) {
                return location;
            }
        }
        catch (Exception ex) {}
        return "file:" + location;
    }
    
    public static String resolveFile(final String baseDir, final String filepath) {
        if (baseDir == null) {
            return filepath;
        }
        final File file = new File(filepath);
        if (file.isAbsolute()) {
            return filepath;
        }
        return baseDir + filepath;
    }
    
    private static void test(final String uriS) {
        System.out.println(uriS);
        final URI uri = URI.create(uriS);
        System.out.println(" scheme=" + uri.getScheme());
        System.out.println(" getSchemeSpecificPart=" + uri.getSchemeSpecificPart());
        System.out.println(" getAuthority=" + uri.getAuthority());
        System.out.println(" getPath=" + uri.getPath());
        System.out.println(" getQuery=" + uri.getQuery());
        System.out.println();
    }
    
    public static void main(final String[] args) {
        testResolve("file:/test/me/", "blank in dir", "file:/test/me/blank in dir");
    }
    
    public static void main2(final String[] args) {
        test("file:test/dir");
        test("file:/test/dir");
        test("file://test/dir");
        test("file:///test/dir");
        test("file:C:/Program Files (x86)/Apache Software Foundation/Tomcat 5.0/content/thredds/cache");
        test("file:C:\\Program Files (x86)\\Apache Software Foundation\\Tomcat 5.0\\content\\thredds\\cache");
        test("http://localhost:8080/thredds/catalog.html?hi=lo");
    }
    
    private static void testResolve(final String base, final String rel, final String result) {
        System.out.println("\nbase= " + base);
        System.out.println("rel= " + rel);
        System.out.println("resolve= " + resolve(base, rel));
        if (result != null && !URLnaming.$assertionsDisabled && !resolve(base, rel).equals(result)) {
            throw new AssertionError();
        }
    }
    
    public static void main3(final String[] args) {
        testResolve("http://test/me/", "wanna", "http://test/me/wanna");
        testResolve("http://test/me/", "/wanna", "http://test/wanna");
        testResolve("file:/test/me/", "wanna", "file:/test/me/wanna");
        testResolve("file:/test/me/", "/wanna", "/wanna");
        testResolve("file://test/me/", "http:/wanna", "http:/wanna");
        testResolve("file://test/me/", "file:/wanna", "file:/wanna");
        testResolve("file://test/me/", "C:/wanna", "C:/wanna");
        testResolve("http://test/me/", "file:wanna", "file:wanna");
    }
    
    public static void main4(final String[] args) {
        try {
            final URL url = new URL("file:src/test/data/ncml/nc/");
            final URI uri = new URI("file:src/test/data/ncml/nc/");
            final File f = new File(uri);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void main5(final String[] args) throws URISyntaxException {
        final String uriString = "http://test.opendap.org:8080/dods/dts/test.53.dods?types[0:1:9]";
        final URI uri = new URI(uriString);
    }
}
