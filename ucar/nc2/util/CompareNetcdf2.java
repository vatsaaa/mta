// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.util;

import ucar.nc2.dataset.NetcdfDataset;
import ucar.ma2.StructureMembers;
import ucar.ma2.IndexIterator;
import ucar.ma2.StructureData;
import ucar.ma2.DataType;
import ucar.ma2.Array;
import ucar.nc2.Structure;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.ByteArrayOutputStream;
import ucar.nc2.dataset.VariableEnhanced;
import java.util.ArrayList;
import ucar.nc2.Group;
import ucar.nc2.Dimension;
import java.util.List;
import java.util.Iterator;
import ucar.nc2.Variable;
import ucar.nc2.NetcdfFile;
import java.util.Formatter;

public class CompareNetcdf2
{
    private Formatter f;
    private boolean showCompare;
    private boolean showEach;
    private boolean compareData;
    private static final double TOL = 1.0E-5;
    private static final float TOLF = 1.0E-5f;
    
    public static void compareFiles(final NetcdfFile org, final NetcdfFile copy, final Formatter f) {
        compareFiles(org, copy, f, false, false, false);
    }
    
    public static void compareFiles(final NetcdfFile org, final NetcdfFile copy, final Formatter f, final boolean _compareData, final boolean _showCompare, final boolean _showEach) {
        final CompareNetcdf2 tc = new CompareNetcdf2(f, _showCompare, _showEach, _compareData);
        tc.compare(org, copy);
    }
    
    public CompareNetcdf2() {
        this(new Formatter(System.out), false, false, false);
    }
    
    public CompareNetcdf2(final Formatter f, final boolean showCompare, final boolean showEach, final boolean compareData) {
        this.showCompare = false;
        this.showEach = false;
        this.compareData = false;
        this.f = f;
        this.compareData = compareData;
        this.showCompare = showCompare;
        this.showEach = showEach;
    }
    
    public boolean compare(final NetcdfFile org, final NetcdfFile copy) {
        return this.compare(org, copy, this.showCompare, this.showEach, this.compareData);
    }
    
    public boolean compare(final NetcdfFile org, final NetcdfFile copy, final boolean showCompare, final boolean showEach, final boolean compareData) {
        this.compareData = compareData;
        this.showCompare = showCompare;
        this.showEach = showEach;
        this.f.format("First file = %s%n", org.getLocation());
        this.f.format("Second file= %s%n", copy.getLocation());
        final long start = System.currentTimeMillis();
        final boolean ok = this.compareGroups(org.getRootGroup(), copy.getRootGroup());
        this.f.format(" Files are the same = %s%n", ok);
        final long took = System.currentTimeMillis() - start;
        this.f.format(" Time to compare = %d msecs%n", took);
        return ok;
    }
    
    public boolean compareVariables(final NetcdfFile org, final NetcdfFile copy) {
        this.f.format("Original = %s%n", org.getLocation());
        this.f.format("CompareTo= %s%n", copy.getLocation());
        boolean ok = true;
        for (final Variable orgV : org.getVariables()) {
            if (orgV.isCoordinateVariable()) {
                continue;
            }
            final Variable copyVar = copy.findVariable(orgV.getShortName());
            if (copyVar == null) {
                this.f.format(" MISSING '%s' in 2nd file%n", orgV.getName());
                ok = false;
            }
            else {
                final List<Dimension> dims1 = orgV.getDimensions();
                final List<Dimension> dims2 = copyVar.getDimensions();
                if (this.compare(dims1, dims2)) {
                    continue;
                }
                this.f.format(" %s != %s%n", orgV.getNameAndDimensions(), copyVar.getNameAndDimensions());
            }
        }
        this.f.format("%n", new Object[0]);
        for (final Variable orgV : copy.getVariables()) {
            if (orgV.isCoordinateVariable()) {
                continue;
            }
            final Variable copyVar = org.findVariable(orgV.getShortName());
            if (copyVar != null) {
                continue;
            }
            this.f.format(" MISSING '%s' in 1st file%n", orgV.getName());
            ok = false;
        }
        return ok;
    }
    
    private boolean compare(final List<Dimension> dims1, final List<Dimension> dims2) {
        if (dims1.size() != dims2.size()) {
            return false;
        }
        for (int i = 0; i < dims1.size(); ++i) {
            final Dimension dim1 = dims1.get(i);
            final Dimension dim2 = dims2.get(i);
            if (dim1.getLength() != dim2.getLength()) {
                return false;
            }
        }
        return true;
    }
    
    private boolean compareGroups(final Group org, final Group copy) {
        if (this.showCompare) {
            this.f.format("compare Group %s to %s %n", org.getName(), copy.getName());
        }
        boolean ok = true;
        if (!org.getName().equals(copy.getName())) {
            this.f.format(" ** names are different %s != %s %n", org.getName(), copy.getName());
            ok = false;
        }
        ok &= this.checkAll(org.getDimensions(), copy.getDimensions(), null);
        ok &= this.checkAll(org.getAttributes(), copy.getAttributes(), null);
        for (final Variable orgV : org.getVariables()) {
            final Variable copyVar = copy.findVariable(orgV.getShortName());
            if (copyVar == null) {
                this.f.format(" ** cant find variable %s in 2nd file%n", orgV.getName());
                ok = false;
            }
            else {
                ok &= this.compareVariables(orgV, copyVar, this.compareData, true);
            }
        }
        for (final Variable copyV : copy.getVariables()) {
            final Variable orgV2 = org.findVariable(copyV.getShortName());
            if (orgV2 == null) {
                this.f.format(" ** cant find variable %s in 1st file%n", copyV.getName());
                ok = false;
            }
        }
        final List groups = new ArrayList();
        ok &= this.checkAll(org.getGroups(), copy.getGroups(), groups);
        for (int i = 0; i < groups.size(); i += 2) {
            final Group orgGroup = groups.get(i);
            final Group ncmlGroup = groups.get(i + 1);
            ok &= this.compareGroups(orgGroup, ncmlGroup);
        }
        return ok;
    }
    
    public boolean compareVariable(final Variable org, final Variable copy) {
        return this.compareVariables(org, copy, this.compareData, true);
    }
    
    public boolean compareVariables(final Variable org, final Variable copy, final boolean compareData, final boolean justOne) {
        boolean ok = true;
        if (this.showCompare) {
            this.f.format("compare Variable %s to %s %n", org.getName(), copy.getName());
        }
        if (!org.getName().equals(copy.getName())) {
            this.f.format(" ** names are different %s != %s %n", org.getName(), copy.getName());
            ok = false;
        }
        ok &= this.checkAll(org.getDimensions(), copy.getDimensions(), null);
        ok &= this.checkAll(org.getAttributes(), copy.getAttributes(), null);
        if (org instanceof VariableEnhanced && copy instanceof VariableEnhanced) {
            final VariableEnhanced orge = (VariableEnhanced)org;
            final VariableEnhanced copye = (VariableEnhanced)copy;
            ok &= this.checkAll(orge.getCoordinateSystems(), copye.getCoordinateSystems(), null);
        }
        if (compareData) {
            try {
                this.compareVariableData(org, copy, this.showCompare, justOne);
            }
            catch (IOException e) {
                final ByteArrayOutputStream bos = new ByteArrayOutputStream(10000);
                e.printStackTrace(new PrintStream(bos));
                this.f.format("%s", bos.toString());
            }
        }
        if (org instanceof Structure) {
            if (!(copy instanceof Structure)) {
                this.f.format("  ** %s not Structure%n", org);
                ok = false;
            }
            else {
                final Structure orgS = (Structure)org;
                final Structure ncmlS = (Structure)copy;
                final List vars = new ArrayList();
                ok &= this.checkAll(orgS.getVariables(), ncmlS.getVariables(), vars);
                for (int i = 0; i < vars.size(); i += 2) {
                    final Variable orgV = vars.get(i);
                    final Variable ncmlV = vars.get(i + 1);
                    ok &= this.compareVariables(orgV, ncmlV, false, true);
                }
            }
        }
        return ok;
    }
    
    private boolean checkContains(final List container, final List wantList) {
        boolean ok = true;
        for (final Object want1 : wantList) {
            final int index2 = container.indexOf(want1);
            if (index2 < 0) {
                this.f.format("  ** %s %s missing %n", want1.getClass().getName(), want1);
                ok = false;
            }
        }
        return ok;
    }
    
    private boolean checkAll(final List list1, final List list2, final List result) {
        boolean ok = true;
        for (final Object aList1 : list1) {
            ok &= this.checkEach(aList1, "file1", list1, "file2", list2, result);
        }
        for (final Object aList2 : list2) {
            ok &= this.checkEach(aList2, "file2", list2, "file1", list1, result);
        }
        return ok;
    }
    
    private boolean checkEach(final Object want1, final String name1, final List list1, final String name2, final List list2, final List result) {
        boolean ok = true;
        try {
            final int index2 = list2.indexOf(want1);
            if (index2 < 0) {
                this.f.format("  ** %s %s 0x%x (%s) not in %s %n", want1.getClass().getName(), want1, want1.hashCode(), name1, name2);
                ok = false;
            }
            else {
                final Object want2 = list2.get(index2);
                final int index3 = list1.indexOf(want2);
                if (index3 < 0) {
                    this.f.format("  ** %s %s 0x%x (%s) not in %s %n", want2.getClass().getName(), want2, want2.hashCode(), name2, name1);
                    ok = false;
                }
                else {
                    final Object want3 = list1.get(index3);
                    if (!want3.equals(want1)) {
                        this.f.format("  ** %s %s 0x%x (%s) not equal to %s 0x%x (%s) %n", want1.getClass().getName(), want1, want1.hashCode(), name1, want2, want2.hashCode(), name2);
                        ok = false;
                    }
                    else {
                        if (this.showEach) {
                            this.f.format("  OK <%s> equals <%s>%n", want1, want2);
                        }
                        if (result != null) {
                            result.add(want1);
                            result.add(want2);
                        }
                    }
                }
            }
        }
        catch (Throwable t) {
            this.f.format(" *** Throwable= %s %n", t.getMessage());
        }
        return ok;
    }
    
    private void compareVariableData(final Variable var1, final Variable var2, final boolean showCompare, final boolean justOne) throws IOException {
        final Array data1 = var1.read();
        final Array data2 = var2.read();
        if (showCompare) {
            this.f.format(" compareArrays %s unlimited=%s size=%d%n", var1.getNameAndDimensions(), var1.isUnlimited(), data1.getSize());
        }
        this.compareData(var1.getName(), data1, data2, justOne);
        if (showCompare) {
            this.f.format("   ok%n", new Object[0]);
        }
    }
    
    public boolean compareData(final String name, final Array data1, final Array data2, final boolean justOne) {
        return this.compareData(name, data1, data2, 1.0E-5, justOne);
    }
    
    private boolean compareData(final String name, final Array data1, final Array data2, final double tol, final boolean justOne) {
        boolean ok = true;
        if (data1.getSize() != data2.getSize()) {
            this.f.format(" DIFF %s: size %d !== %d%n", name, data1.getSize(), data2.getSize());
            ok = false;
        }
        if (data1.getElementType() != data2.getElementType()) {
            this.f.format(" DIFF %s: element type %s !== %s%n", name, data1.getElementType(), data2.getElementType());
            ok = false;
        }
        if (!ok) {
            return false;
        }
        final DataType dt = DataType.getType(data1.getElementType());
        final IndexIterator iter1 = data1.getIndexIterator();
        final IndexIterator iter2 = data2.getIndexIterator();
        if (dt == DataType.DOUBLE) {
            while (iter1.hasNext() && iter2.hasNext()) {
                final double v1 = iter1.getDoubleNext();
                final double v2 = iter2.getDoubleNext();
                if ((!Double.isNaN(v1) || !Double.isNaN(v2)) && !closeEnough(v1, v2, tol)) {
                    this.f.format(" DIFF %s: %f != %f count=%s diff = %f pdiff = %f %n", name, v1, v2, iter1, diff(v1, v2), pdiff(v1, v2));
                    ok = false;
                    if (justOne) {
                        break;
                    }
                    continue;
                }
            }
        }
        else if (dt == DataType.FLOAT) {
            while (iter1.hasNext() && iter2.hasNext()) {
                final float v3 = iter1.getFloatNext();
                final float v4 = iter2.getFloatNext();
                if ((!Float.isNaN(v3) || !Float.isNaN(v4)) && !closeEnough(v3, v4, (float)tol)) {
                    this.f.format(" DIFF %s: %f != %f count=%s diff = %f pdiff = %f %n", name, v3, v4, iter1, diff(v3, v4), pdiff(v3, v4));
                    ok = false;
                    if (justOne) {
                        break;
                    }
                    continue;
                }
            }
        }
        else if (dt == DataType.INT) {
            while (iter1.hasNext() && iter2.hasNext()) {
                final int v5 = iter1.getIntNext();
                final int v6 = iter2.getIntNext();
                if (v5 != v6) {
                    this.f.format(" DIFF %s: %d != %d count=%s diff = %f pdiff = %f %n", name, v5, v6, iter1, diff(v5, v6), pdiff(v5, v6));
                    ok = false;
                    if (justOne) {
                        break;
                    }
                    continue;
                }
            }
        }
        else if (dt == DataType.SHORT) {
            while (iter1.hasNext() && iter2.hasNext()) {
                final short v7 = iter1.getShortNext();
                final short v8 = iter2.getShortNext();
                if (v7 != v8) {
                    this.f.format(" DIFF %s: %d != %d count=%s diff = %f pdiff = %f %n", name, v7, v8, iter1, diff(v7, v8), pdiff(v7, v8));
                    ok = false;
                    if (justOne) {
                        break;
                    }
                    continue;
                }
            }
        }
        else if (dt == DataType.BYTE) {
            while (iter1.hasNext() && iter2.hasNext()) {
                final byte v9 = iter1.getByteNext();
                final byte v10 = iter2.getByteNext();
                if (v9 != v10) {
                    this.f.format(" DIFF %s: %d != %d count=%s diff = %f pdiff = %f %n", name, v9, v10, iter1, diff(v9, v10), pdiff(v9, v10));
                    ok = false;
                    if (justOne) {
                        break;
                    }
                    continue;
                }
            }
        }
        else if (dt == DataType.STRUCTURE) {
            while (iter1.hasNext() && iter2.hasNext()) {
                this.compareStructureData((StructureData)iter1.next(), (StructureData)iter2.next(), tol, justOne);
            }
        }
        return ok;
    }
    
    public boolean compareStructureData(final StructureData sdata1, final StructureData sdata2, final double tol, final boolean justOne) {
        boolean ok = true;
        final StructureMembers sm1 = sdata1.getStructureMembers();
        final StructureMembers sm2 = sdata2.getStructureMembers();
        if (sm1.getMembers().size() != sm2.getMembers().size()) {
            this.f.format(" size %d !== %d%n", sm1.getMembers().size(), sm2.getMembers().size());
            ok = false;
        }
        for (final StructureMembers.Member m1 : sm1.getMembers()) {
            if (m1.getName().equals("time")) {
                continue;
            }
            final StructureMembers.Member m2 = sm2.findMember(m1.getName());
            final Array data1 = sdata1.getArray(m1);
            final Array data2 = sdata2.getArray(m2);
            ok &= this.compareData(m1.getName(), data1, data2, tol, justOne);
        }
        return ok;
    }
    
    public static boolean closeEnoughP(final double d1, final double d2) {
        if (Math.abs(d1) < 1.0E-5) {
            return Math.abs(d1 - d2) < 1.0E-5;
        }
        return Math.abs((d1 - d2) / d1) < 1.0E-5;
    }
    
    public static boolean closeEnough(final double d1, final double d2) {
        return Math.abs(d1 - d2) < 1.0E-5;
    }
    
    public static boolean closeEnough(final double d1, final double d2, final double tol) {
        return Math.abs(d1 - d2) < tol;
    }
    
    public static boolean closeEnoughP(final double d1, final double d2, final double tol) {
        if (Math.abs(d1) < tol) {
            return Math.abs(d1 - d2) < tol;
        }
        return Math.abs((d1 - d2) / d1) < tol;
    }
    
    public static double diff(final double d1, final double d2) {
        return Math.abs(d1 - d2);
    }
    
    public static double pdiff(final double d1, final double d2) {
        return Math.abs((d1 - d2) / d1);
    }
    
    public static boolean closeEnough(final float d1, final float d2) {
        return Math.abs(d1 - d2) < 1.0E-5f;
    }
    
    public static boolean closeEnoughP(final float d1, final float d2) {
        if (Math.abs(d1) < 1.0E-5f) {
            return Math.abs(d1 - d2) < 1.0E-5f;
        }
        return Math.abs((d1 - d2) / d1) < 1.0E-5f;
    }
    
    public static void main(final String[] arg) throws IOException {
        final NetcdfFile ncfile1 = NetcdfDataset.openFile("dods://thredds.cise-nsf.gov:8080/thredds/dodsC/satellite/SFC-T/SUPER-NATIONAL_1km/20090516/SUPER-NATIONAL_1km_SFC-T_20090516_2200.gini", null);
        final NetcdfFile ncfile2 = NetcdfDataset.openFile("dods://motherlode.ucar.edu:8080/thredds/dodsC/satellite/SFC-T/SUPER-NATIONAL_1km/20090516/SUPER-NATIONAL_1km_SFC-T_20090516_2200.gini", null);
        compareFiles(ncfile1, ncfile2, new Formatter(System.out), false, true, false);
    }
}
