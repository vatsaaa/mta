// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.util.cache;

import java.io.File;

public class TestFileSystem
{
    static long touch;
    static boolean readLast;
    
    int readDir(final File f) {
        final File[] subs = f.listFiles();
        if (subs == null) {
            return 0;
        }
        int count = subs.length;
        for (final File sub : subs) {
            if (sub.isDirectory()) {
                count += this.readDir(sub);
            }
            if (TestFileSystem.readLast) {
                TestFileSystem.touch += sub.lastModified();
            }
            TestFileSystem.touch += sub.getPath().hashCode();
        }
        return count;
    }
    
    int timeDir(final File f) {
        int total = 0;
        System.out.printf("file                     count  msecs%n", new Object[0]);
        final File[] arr$;
        final File[] subs = arr$ = f.listFiles();
        for (final File sub : arr$) {
            if (sub.isDirectory()) {
                final long start = System.nanoTime();
                final int count = this.readDir(sub);
                final long end = System.nanoTime();
                System.out.printf("%-20s, %8d, %f%n", sub.getPath(), count, (end - start) / 1000L / 1000.0);
                total += count;
            }
        }
        return total;
    }
    
    public static void main(final String[] args) {
        String root = "C:/";
        for (int i = 0; i < args.length; ++i) {
            if (args[i].startsWith("root")) {
                final int pos = args[i].indexOf(61);
                if (pos > 0) {
                    root = args[i].substring(pos + 1);
                    System.out.printf("root=%s ", root);
                }
                if (args[i].equals("usage")) {
                    System.out.printf("java -classpath {jar} ucar.nc2.util.cache.TestFileSystem root={rootDir} [readLastModified] -%n", new Object[0]);
                    System.exit(0);
                }
            }
            if (args[i].equals("readLastModified")) {
                TestFileSystem.readLast = true;
                System.out.printf(" readLastModified ", new Object[0]);
            }
        }
        System.out.printf(" %n%n", new Object[0]);
        final TestFileSystem test = new TestFileSystem();
        final long start = System.nanoTime();
        final int total = test.timeDir(new File(root));
        final long end = System.nanoTime();
        System.out.printf("%n%-20s, %8d, %f usecs%n", root, total, (end - start) / 1000L / 1000.0);
        System.out.printf("%n %d %n", TestFileSystem.touch);
    }
    
    static {
        TestFileSystem.touch = 0L;
        TestFileSystem.readLast = false;
    }
}
