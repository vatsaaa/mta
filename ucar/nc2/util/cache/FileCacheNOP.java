// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.util.cache;

import java.io.IOException;

public class FileCacheNOP extends FileCache
{
    public FileCacheNOP() {
        super("FileCacheNOP", 0, 0, 0, 0);
    }
    
    @Override
    public void release(final FileCacheable ncfile) throws IOException {
    }
}
