// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.util.cache;

import ucar.unidata.io.RandomAccessFile;
import java.io.IOException;
import ucar.nc2.util.CancelTask;
import java.util.Collection;

public class FileCacheRaf
{
    private FileCache cache;
    private FileFactory factory;
    
    public FileCacheRaf(final int minElementsInMemory, final int maxElementsInMemory, final int period) {
        this.cache = new FileCache("FileCacheRaf", minElementsInMemory, maxElementsInMemory, -1, period);
        this.factory = new RafFactory();
    }
    
    public void clearCache(final boolean force) {
        this.cache.clearCache(force);
    }
    
    public Collection getCache() {
        return this.cache.getCache().values();
    }
    
    public static void shutdown() {
        FileCache.shutdown();
    }
    
    public Raf acquire(final String filename) throws IOException {
        return (Raf)this.cache.acquire(this.factory, filename, null);
    }
    
    public void release(final Raf craf) throws IOException {
        this.cache.release(craf);
    }
    
    private class RafFactory implements FileFactory
    {
        public FileCacheable open(final String location, final int buffer_size, final CancelTask cancelTask, final Object iospMessage) throws IOException {
            return new Raf(location);
        }
    }
    
    public class Raf implements FileCacheable
    {
        private RandomAccessFile raf;
        
        Raf(final String location) throws IOException {
            this.raf = new RandomAccessFile(location, "r");
        }
        
        public String getLocation() {
            return this.raf.getLocation();
        }
        
        public RandomAccessFile getRaf() {
            return this.raf;
        }
        
        public void close() throws IOException {
            this.raf.close();
        }
        
        public boolean sync() throws IOException {
            return false;
        }
        
        public void setFileCache(final FileCache fileCache) {
        }
    }
}
