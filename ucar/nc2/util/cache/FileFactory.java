// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.util.cache;

import java.io.IOException;
import ucar.nc2.util.CancelTask;

public interface FileFactory
{
    FileCacheable open(final String p0, final int p1, final CancelTask p2, final Object p3) throws IOException;
}
