// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.util.cache;

import java.util.LinkedList;
import net.jcip.annotations.GuardedBy;
import org.slf4j.LoggerFactory;
import java.util.Collections;
import java.util.Formatter;
import java.util.List;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Map;
import java.util.Iterator;
import java.util.Date;
import java.io.IOException;
import ucar.nc2.util.CancelTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledExecutorService;
import org.slf4j.Logger;
import net.jcip.annotations.ThreadSafe;

@ThreadSafe
public class FileCache
{
    private static Logger log;
    private static Logger cacheLog;
    private static ScheduledExecutorService exec;
    static boolean debug;
    static boolean debugPrint;
    static boolean debugCleanup;
    private String name;
    private final int softLimit;
    private final int minElements;
    private final int hardLimit;
    private final ConcurrentHashMap<Object, CacheElement> cache;
    private final ConcurrentHashMap<Object, CacheElement.CacheFile> files;
    private final AtomicBoolean hasScheduled;
    private final AtomicBoolean disabled;
    private final AtomicInteger cleanups;
    private final AtomicInteger hits;
    private final AtomicInteger miss;
    
    public static void shutdown() {
        if (FileCache.exec != null) {
            FileCache.exec.shutdown();
        }
        FileCache.exec = null;
    }
    
    public FileCache(final int minElementsInMemory, final int maxElementsInMemory, final int period) {
        this("", minElementsInMemory, maxElementsInMemory, -1, period);
    }
    
    public FileCache(final int minElementsInMemory, final int softLimit, final int hardLimit, final int period) {
        this("", minElementsInMemory, softLimit, hardLimit, period);
    }
    
    public FileCache(final String name, final int minElementsInMemory, final int softLimit, final int hardLimit, final int period) {
        this.hasScheduled = new AtomicBoolean(false);
        this.disabled = new AtomicBoolean(false);
        this.cleanups = new AtomicInteger();
        this.hits = new AtomicInteger();
        this.miss = new AtomicInteger();
        this.name = name;
        this.minElements = minElementsInMemory;
        this.softLimit = softLimit;
        this.hardLimit = hardLimit;
        this.cache = new ConcurrentHashMap<Object, CacheElement>(2 * softLimit, 0.75f, 8);
        this.files = new ConcurrentHashMap<Object, CacheElement.CacheFile>(4 * softLimit, 0.75f, 8);
        if (period > 0) {
            if (FileCache.exec == null) {
                FileCache.exec = Executors.newSingleThreadScheduledExecutor();
            }
            FileCache.exec.scheduleAtFixedRate(new CleanupTask(), period, period, TimeUnit.SECONDS);
            FileCache.cacheLog.debug("FileCache " + name + " cleanup every " + period + " secs");
        }
    }
    
    public void disable() {
        this.disabled.set(true);
        this.clearCache(true);
    }
    
    public void enable() {
        this.disabled.set(false);
    }
    
    public FileCacheable acquire(final FileFactory factory, final String location, final CancelTask cancelTask) throws IOException {
        return this.acquire(factory, location, location, -1, cancelTask, null);
    }
    
    public FileCacheable acquire(final FileFactory factory, Object hashKey, final String location, final int buffer_size, final CancelTask cancelTask, final Object spiObject) throws IOException {
        if (null == hashKey) {
            hashKey = location;
        }
        FileCacheable ncfile = this.acquireCacheOnly(hashKey);
        if (ncfile != null) {
            this.hits.incrementAndGet();
            return ncfile;
        }
        this.miss.incrementAndGet();
        ncfile = factory.open(location, buffer_size, cancelTask, spiObject);
        if (FileCache.cacheLog.isDebugEnabled()) {
            FileCache.cacheLog.debug("FileCache " + this.name + " acquire " + hashKey + " " + ncfile.getLocation());
        }
        if (FileCache.debugPrint) {
            System.out.println("  FileCache " + this.name + " acquire " + hashKey + " " + ncfile.getLocation());
        }
        if (cancelTask != null && cancelTask.isCancel()) {
            if (ncfile != null) {
                ncfile.close();
            }
            return null;
        }
        if (this.disabled.get()) {
            return ncfile;
        }
        final CacheElement elem;
        synchronized (this.cache) {
            elem = this.cache.get(hashKey);
            if (elem == null) {
                this.cache.put(hashKey, new CacheElement(ncfile, hashKey));
            }
        }
        if (elem != null) {
            synchronized (elem) {
                elem.addFile(ncfile);
            }
        }
        boolean needHard = false;
        boolean needSoft = false;
        synchronized (this.hasScheduled) {
            if (!this.hasScheduled.get()) {
                final int count = this.files.size();
                if (count > this.hardLimit && this.hardLimit > 0) {
                    needHard = true;
                    this.hasScheduled.getAndSet(true);
                }
                else if (count > this.softLimit) {
                    this.hasScheduled.getAndSet(true);
                    needSoft = true;
                }
            }
        }
        if (needHard) {
            if (FileCache.debugCleanup) {
                System.out.println("CleanupTask due to hard limit time=" + new Date().getTime());
            }
            this.cleanup(this.hardLimit);
        }
        else if (needSoft) {
            FileCache.exec.schedule(new CleanupTask(), 100L, TimeUnit.MILLISECONDS);
            if (FileCache.debugCleanup) {
                System.out.println("CleanupTask scheduled due to soft limit time=" + new Date());
            }
        }
        return ncfile;
    }
    
    private FileCacheable acquireCacheOnly(final Object hashKey) {
        if (this.disabled.get()) {
            return null;
        }
        FileCacheable ncfile = null;
        final CacheElement elem = this.cache.get(hashKey);
        if (elem != null) {
            synchronized (elem) {
                for (final CacheElement.CacheFile file : elem.list) {
                    if (file.isLocked.compareAndSet(false, true)) {
                        ncfile = file.ncfile;
                        break;
                    }
                }
            }
        }
        if (ncfile != null) {
            try {
                final boolean changed = ncfile.sync();
                if (FileCache.cacheLog.isDebugEnabled()) {
                    FileCache.cacheLog.debug("FileCache " + this.name + " aquire from cache " + hashKey + " " + ncfile.getLocation() + " changed = " + changed);
                }
                if (FileCache.debugPrint) {
                    System.out.println("  FileCache " + this.name + " aquire from cache " + hashKey + " " + ncfile.getLocation() + " changed = " + changed);
                }
            }
            catch (IOException e) {
                FileCache.log.error("FileCache " + this.name + " synch failed on " + ncfile.getLocation() + " " + e.getMessage());
                return null;
            }
        }
        return ncfile;
    }
    
    public void release(final FileCacheable ncfile) throws IOException {
        if (ncfile == null) {
            return;
        }
        if (this.disabled.get()) {
            ncfile.setFileCache(null);
            ncfile.close();
            return;
        }
        final CacheElement.CacheFile file = this.files.get(ncfile);
        if (file != null) {
            if (!file.isLocked.get()) {
                FileCache.cacheLog.warn("FileCache " + this.name + " release " + ncfile.getLocation() + " not locked");
            }
            file.lastAccessed = System.currentTimeMillis();
            final CacheElement.CacheFile cacheFile = file;
            ++cacheFile.countAccessed;
            file.isLocked.set(false);
            if (FileCache.cacheLog.isDebugEnabled()) {
                FileCache.cacheLog.debug("FileCache " + this.name + " release " + ncfile.getLocation());
            }
            if (FileCache.debugPrint) {
                System.out.println("  FileCache " + this.name + " release " + ncfile.getLocation());
            }
            return;
        }
        throw new IOException("FileCache " + this.name + " release does not have file in cache = " + ncfile.getLocation());
    }
    
    public String getInfo(final FileCacheable ncfile) throws IOException {
        if (ncfile == null) {
            return "";
        }
        final CacheElement.CacheFile file = this.files.get(ncfile);
        if (file != null) {
            return "File is in cache= " + file;
        }
        return "File not in cache";
    }
    
    Map<Object, CacheElement> getCache() {
        return this.cache;
    }
    
    public synchronized void clearCache(final boolean force) {
        final List<CacheElement.CacheFile> deleteList = new ArrayList<CacheElement.CacheFile>(2 * this.cache.size());
        if (force) {
            this.cache.clear();
            deleteList.addAll(this.files.values());
            this.files.clear();
        }
        else {
            final Iterator<CacheElement.CacheFile> iter = this.files.values().iterator();
            while (iter.hasNext()) {
                final CacheElement.CacheFile file = iter.next();
                if (file.isLocked.compareAndSet(false, true)) {
                    file.remove();
                    deleteList.add(file);
                    iter.remove();
                }
            }
            synchronized (this.cache) {
                for (final CacheElement elem : this.cache.values()) {
                    synchronized (elem) {
                        if (elem.list.size() != 0) {
                            continue;
                        }
                        this.cache.remove(elem.hashKey);
                    }
                }
            }
        }
        final Iterator i$2 = deleteList.iterator();
        while (i$2.hasNext()) {
            final CacheElement.CacheFile file = i$2.next();
            if (force && file.isLocked.get()) {
                FileCache.cacheLog.warn("FileCache " + this.name + " force close locked file= " + file);
            }
            try {
                file.ncfile.setFileCache(null);
                file.ncfile.close();
                file.ncfile = null;
            }
            catch (IOException e) {
                FileCache.log.error("FileCache " + this.name + " close failed on " + file);
            }
        }
        FileCache.cacheLog.debug("*FileCache " + this.name + " clearCache force= " + force + " deleted= " + deleteList.size() + " left=" + this.files.size());
    }
    
    public void showCache(final Formatter format) {
        final ArrayList<CacheElement.CacheFile> allFiles = new ArrayList<CacheElement.CacheFile>(this.files.size());
        for (final CacheElement elem : this.cache.values()) {
            synchronized (elem) {
                allFiles.addAll(elem.list);
            }
        }
        Collections.sort(allFiles);
        format.format("FileCache %s (%d):%n", this.name, allFiles.size());
        format.format("isLocked  accesses lastAccess                   location %n", new Object[0]);
        for (final CacheElement.CacheFile file : allFiles) {
            final String loc = (file.ncfile != null) ? file.ncfile.getLocation() : "null";
            format.format("%8s %9d %s %s %n", file.isLocked, file.countAccessed, new Date(file.lastAccessed), loc);
        }
    }
    
    public List<String> showCache() {
        final ArrayList<CacheElement.CacheFile> allFiles = new ArrayList<CacheElement.CacheFile>(this.files.size());
        for (final CacheElement elem : this.cache.values()) {
            synchronized (elem) {
                allFiles.addAll(elem.list);
            }
        }
        Collections.sort(allFiles);
        final ArrayList<String> result = new ArrayList<String>(allFiles.size());
        for (final CacheElement.CacheFile file : allFiles) {
            result.add(file.toString());
        }
        return result;
    }
    
    public void showStats(final Formatter format) {
        format.format("  hits= %d miss= %d nfiles= %d elems= %d\n", this.hits.get(), this.miss.get(), this.files.size(), this.cache.values().size());
    }
    
    synchronized void cleanup(final int maxElements) {
        if (this.disabled.get()) {
            return;
        }
        try {
            final int size = this.files.size();
            if (size <= this.minElements) {
                return;
            }
            FileCache.cacheLog.debug(" FileCache " + this.name + " cleanup started at " + new Date() + " for cleanup maxElements=" + maxElements);
            if (FileCache.debugCleanup) {
                System.out.println(" FileCache " + this.name + "cleanup started at " + new Date() + " for cleanup maxElements=" + maxElements);
            }
            this.cleanups.incrementAndGet();
            final ArrayList<CacheElement.CacheFile> allFiles = new ArrayList<CacheElement.CacheFile>(size + 10);
            for (final CacheElement.CacheFile file : this.files.values()) {
                if (!file.isLocked.get()) {
                    allFiles.add(file);
                }
            }
            Collections.sort(allFiles);
            final int need2delete = size - this.minElements;
            final int minDelete = size - maxElements;
            final ArrayList<CacheElement.CacheFile> deleteList = new ArrayList<CacheElement.CacheFile>(need2delete);
            int count = 0;
            for (Iterator<CacheElement.CacheFile> iter = allFiles.iterator(); iter.hasNext() && count < need2delete; ++count) {
                final CacheElement.CacheFile file2 = iter.next();
                if (file2.isLocked.compareAndSet(false, true)) {
                    file2.remove();
                    deleteList.add(file2);
                }
            }
            if (count < minDelete) {
                FileCache.cacheLog.warn("FileCache " + this.name + " cleanup couldnt remove enough to keep under the maximum= " + maxElements + " due to locked files; currently at = " + (size - count));
                if (FileCache.debugCleanup) {
                    System.out.println("FileCache " + this.name + "cleanup couldnt remove enough to keep under the maximum= " + maxElements + " due to locked files; currently at = " + (size - count));
                }
            }
            synchronized (this.cache) {
                for (final CacheElement elem : this.cache.values()) {
                    synchronized (elem) {
                        if (elem.list.size() != 0) {
                            continue;
                        }
                        this.cache.remove(elem.hashKey);
                    }
                }
            }
            final long start = System.currentTimeMillis();
            for (final CacheElement.CacheFile file3 : deleteList) {
                this.files.remove(file3.ncfile);
                try {
                    file3.ncfile.setFileCache(null);
                    file3.ncfile.close();
                    file3.ncfile = null;
                }
                catch (IOException e) {
                    FileCache.log.error("FileCache " + this.name + " close failed on " + file3.getCacheName());
                }
            }
            final long took = System.currentTimeMillis() - start;
            FileCache.cacheLog.debug(" FileCache " + this.name + " cleanup had= " + size + " removed= " + deleteList.size() + " took=" + took + " msec");
            if (FileCache.debugCleanup) {
                System.out.println(" FileCache " + this.name + "cleanup had= " + size + " removed= " + deleteList.size() + " took=" + took + " msec");
            }
        }
        finally {
            this.hasScheduled.set(false);
        }
    }
    
    static {
        FileCache.log = LoggerFactory.getLogger(FileCache.class);
        FileCache.cacheLog = LoggerFactory.getLogger("cacheLogger");
        FileCache.debug = false;
        FileCache.debugPrint = false;
        FileCache.debugCleanup = false;
    }
    
    class CacheElement
    {
        @GuardedBy("this")
        List<CacheFile> list;
        final Object hashKey;
        final /* synthetic */ FileCache this$0;
        
        CacheElement(final FileCacheable ncfile, final Object hashKey) {
            this.list = new LinkedList<CacheFile>();
            this.hashKey = hashKey;
            final CacheFile file = new CacheFile(ncfile);
            this.list.add(file);
            if (FileCache.debug && FileCache.this.files.get(ncfile) != null) {
                FileCache.cacheLog.error("files already has " + hashKey + " " + FileCache.this.name);
            }
            FileCache.this.files.put(ncfile, file);
            if (FileCache.cacheLog.isDebugEnabled()) {
                FileCache.cacheLog.debug("CacheElement add to cache " + hashKey + " " + FileCache.this.name);
            }
        }
        
        CacheFile addFile(final FileCacheable ncfile) {
            final CacheFile file = new CacheFile(ncfile);
            synchronized (this) {
                this.list.add(file);
            }
            if (FileCache.debug && FileCache.this.files.get(ncfile) != null) {
                FileCache.cacheLog.error("files (2) already has " + this.hashKey + " " + FileCache.this.name);
            }
            FileCache.this.files.put(ncfile, file);
            return file;
        }
        
        @Override
        public String toString() {
            return this.hashKey + " count=" + this.list.size();
        }
        
        class CacheFile implements Comparable<CacheFile>
        {
            FileCacheable ncfile;
            final AtomicBoolean isLocked;
            int countAccessed;
            long lastAccessed;
            
            private CacheFile(final FileCacheable ncfile) {
                this.isLocked = new AtomicBoolean(true);
                this.countAccessed = 1;
                this.lastAccessed = 0L;
                this.ncfile = ncfile;
                this.lastAccessed = System.currentTimeMillis();
                ncfile.setFileCache(CacheElement.this.this$0);
                if (FileCache.cacheLog.isDebugEnabled()) {
                    FileCache.cacheLog.debug("FileCache " + CacheElement.this.this$0.name + " add to cache " + CacheElement.this.hashKey);
                }
                if (FileCache.debugPrint) {
                    System.out.println("  FileCache " + CacheElement.this.this$0.name + " add to cache " + CacheElement.this.hashKey);
                }
            }
            
            String getCacheName() {
                return this.ncfile.getLocation();
            }
            
            void remove() {
                synchronized (CacheElement.this) {
                    if (!CacheElement.this.list.remove(this)) {
                        FileCache.cacheLog.warn("FileCache " + FileCache.this.name + " could not remove " + this.ncfile.getLocation());
                    }
                }
                if (FileCache.cacheLog.isDebugEnabled()) {
                    FileCache.cacheLog.debug("FileCache " + FileCache.this.name + " remove " + this.ncfile.getLocation());
                }
                if (FileCache.debugPrint) {
                    System.out.println("  FileCache " + FileCache.this.name + " remove " + this.ncfile.getLocation());
                }
            }
            
            @Override
            public String toString() {
                return this.isLocked + " " + this.countAccessed + " " + new Date(this.lastAccessed) + " " + this.ncfile.getLocation();
            }
            
            public int compareTo(final CacheFile o) {
                return (int)(this.lastAccessed - o.lastAccessed);
            }
        }
    }
    
    private class CleanupTask implements Runnable
    {
        public void run() {
            FileCache.this.cleanup(FileCache.this.softLimit);
        }
    }
}
