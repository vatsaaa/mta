// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.util.cache;

import java.io.IOException;

public interface FileCacheable
{
    String getLocation();
    
    void close() throws IOException;
    
    boolean sync() throws IOException;
    
    void setFileCache(final FileCache p0);
}
