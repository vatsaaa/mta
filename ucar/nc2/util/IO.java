// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.util;

import java.util.ArrayList;
import ucar.unidata.util.StringUtil;
import java.util.Iterator;
import java.util.Map;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.net.ConnectException;
import java.util.zip.GZIPInputStream;
import java.util.List;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.io.StringWriter;
import java.io.InputStreamReader;
import ucar.unidata.io.RandomAccessFile;
import java.io.File;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.io.IOException;
import java.io.OutputStream;
import java.security.AccessControlException;
import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Formatter;

public class IO
{
    public static int default_file_buffersize;
    public static int default_socket_buffersize;
    private static boolean showStackTrace;
    private static boolean debug;
    private static boolean showResponse;
    private static boolean showHeaders;
    private static Class cl;
    static Formatter fout;
    
    public static InputStream getFileResource(final String resourcePath) {
        if (IO.cl == null) {
            IO.cl = new IO().getClass();
        }
        InputStream is = IO.cl.getResourceAsStream(resourcePath);
        if (is != null) {
            if (IO.debug) {
                System.out.println("Resource.getResourceAsStream ok on " + resourcePath);
            }
            return is;
        }
        if (IO.debug) {
            System.out.println("Resource.getResourceAsStream failed on (" + resourcePath + ")");
        }
        try {
            is = new FileInputStream(resourcePath);
            if (IO.debug) {
                System.out.println("Resource.FileInputStream ok on " + resourcePath);
            }
        }
        catch (FileNotFoundException e) {
            if (IO.debug) {
                System.out.println("  FileNotFoundException: Resource.getFile failed on " + resourcePath);
            }
        }
        catch (AccessControlException e2) {
            if (IO.debug) {
                System.out.println("  AccessControlException: Resource.getFile failed on " + resourcePath);
            }
        }
        return is;
    }
    
    public static long copy(final InputStream in, final OutputStream out) throws IOException {
        long totalBytesRead = 0L;
        final byte[] buffer = new byte[IO.default_file_buffersize];
        while (true) {
            final int bytesRead = in.read(buffer);
            if (bytesRead == -1) {
                break;
            }
            out.write(buffer, 0, bytesRead);
            totalBytesRead += bytesRead;
        }
        return totalBytesRead;
    }
    
    public static long copy2null(final InputStream in, int buffersize) throws IOException {
        long totalBytesRead = 0L;
        if (buffersize <= 0) {
            buffersize = IO.default_file_buffersize;
        }
        final byte[] buffer = new byte[buffersize];
        while (true) {
            final int n = in.read(buffer);
            if (n == -1) {
                break;
            }
            totalBytesRead += n;
        }
        return totalBytesRead;
    }
    
    public static long touch(final InputStream in, int buffersize) throws IOException {
        long touch = 0L;
        if (buffersize <= 0) {
            buffersize = IO.default_file_buffersize;
        }
        final byte[] buffer = new byte[buffersize];
        while (true) {
            final int n = in.read(buffer);
            if (n == -1) {
                break;
            }
            for (int i = 0; i < buffersize; ++i) {
                touch += buffer[i];
            }
        }
        return touch;
    }
    
    public static long copy2null(final FileChannel in, int buffersize) throws IOException {
        long totalBytesRead = 0L;
        if (buffersize <= 0) {
            buffersize = IO.default_file_buffersize;
        }
        final ByteBuffer buffer = ByteBuffer.allocate(buffersize);
        while (true) {
            final int n = in.read(buffer);
            if (n == -1) {
                break;
            }
            totalBytesRead += n;
            buffer.flip();
        }
        return totalBytesRead;
    }
    
    public static long touch(final FileChannel in, int buffersize) throws IOException {
        long touch = 0L;
        if (buffersize <= 0) {
            buffersize = IO.default_file_buffersize;
        }
        final ByteBuffer buffer = ByteBuffer.allocate(buffersize);
        while (true) {
            final int n = in.read(buffer);
            if (n == -1) {
                break;
            }
            final byte[] result = buffer.array();
            for (int i = 0; i < buffersize; ++i) {
                touch += result[i];
            }
            buffer.flip();
        }
        return touch;
    }
    
    public static long copyB(final InputStream in, final OutputStream out, final int bufferSize) throws IOException {
        long totalBytesRead = 0L;
        int done = 0;
        int next = 1;
        final boolean show = false;
        final byte[] buffer = new byte[bufferSize];
        while (true) {
            final int n = in.read(buffer);
            if (n == -1) {
                break;
            }
            out.write(buffer, 0, n);
            totalBytesRead += n;
            if (!show) {
                continue;
            }
            done += n;
            if (done <= 1000000 * next) {
                continue;
            }
            System.out.println(next + " Mb");
            ++next;
        }
        return totalBytesRead;
    }
    
    public static void copy(final InputStream in, final OutputStream out, final int n) throws IOException {
        final byte[] buffer = new byte[IO.default_file_buffersize];
        int count = 0;
        while (true) {
            final int bytesRead = in.read(buffer);
            if (bytesRead == -1) {
                return;
            }
            out.write(buffer, 0, bytesRead);
            count += bytesRead;
            if (count > n) {
                return;
            }
        }
    }
    
    public static String readContents(final InputStream is) throws IOException {
        return readContents(is, "UTF-8");
    }
    
    public static String readContents(final InputStream is, final String charset) throws IOException {
        final ByteArrayOutputStream bout = new ByteArrayOutputStream(10 * IO.default_file_buffersize);
        copy(is, bout);
        return bout.toString(charset);
    }
    
    public static byte[] readContentsToByteArray(final InputStream is) throws IOException {
        final ByteArrayOutputStream bout = new ByteArrayOutputStream(10 * IO.default_file_buffersize);
        copy(is, bout);
        return bout.toByteArray();
    }
    
    public static void writeContents(final String contents, final OutputStream os) throws IOException {
        final ByteArrayInputStream bin = new ByteArrayInputStream(contents.getBytes());
        copy(bin, os);
    }
    
    public static void copyFile(final String fileInName, final String fileOutName) throws IOException {
        InputStream in = null;
        OutputStream out = null;
        try {
            in = new BufferedInputStream(new FileInputStream(fileInName));
            out = new BufferedOutputStream(new FileOutputStream(fileOutName));
            copy(in, out);
        }
        finally {
            if (null != in) {
                in.close();
            }
            if (null != out) {
                out.close();
            }
        }
    }
    
    public static void copyFile(final File fileIn, final File fileOut) throws IOException {
        InputStream in = null;
        OutputStream out = null;
        try {
            in = new BufferedInputStream(new FileInputStream(fileIn));
            out = new BufferedOutputStream(new FileOutputStream(fileOut));
            copy(in, out);
        }
        finally {
            if (null != in) {
                in.close();
            }
            if (null != out) {
                out.close();
            }
        }
    }
    
    public static void copyFile(final String fileInName, final OutputStream out) throws IOException {
        copyFileB(new File(fileInName), out, IO.default_file_buffersize);
    }
    
    public static void copyFileB(final File fileIn, final OutputStream out, final int bufferSize) throws IOException {
        InputStream in = null;
        try {
            in = new BufferedInputStream(new FileInputStream(fileIn));
            copyB(in, out, bufferSize);
        }
        finally {
            if (null != in) {
                in.close();
            }
        }
    }
    
    public static long copyRafB(final RandomAccessFile raf, final long offset, final long length, final OutputStream out, final byte[] buffer) throws IOException {
        final int bufferSize = buffer.length;
        long want = length;
        raf.seek(offset);
        while (want > 0L) {
            final int len = (int)Math.min(want, bufferSize);
            final int bytesRead = raf.read(buffer, 0, len);
            if (bytesRead <= 0) {
                break;
            }
            out.write(buffer, 0, bytesRead);
            want -= bytesRead;
        }
        return length - want;
    }
    
    public static void copyDirTree(final String fromDirName, final String toDirName) throws IOException {
        final File fromDir = new File(fromDirName);
        final File toDir = new File(toDirName);
        if (!fromDir.exists()) {
            return;
        }
        if (!toDir.exists()) {
            toDir.mkdirs();
        }
        for (final File f : fromDir.listFiles()) {
            if (f.isDirectory()) {
                copyDirTree(f.getAbsolutePath(), toDir.getAbsolutePath() + "/" + f.getName());
            }
            else {
                copyFile(f.getAbsolutePath(), toDir.getAbsolutePath() + "/" + f.getName());
            }
        }
    }
    
    public static byte[] readFileToByteArray(final String filename) throws IOException {
        InputStream in = null;
        try {
            in = new BufferedInputStream(new FileInputStream(filename));
            return readContentsToByteArray(in);
        }
        finally {
            if (in != null) {
                in.close();
            }
        }
    }
    
    public static String readFile(final String filename) throws IOException {
        final InputStreamReader reader = new InputStreamReader(new FileInputStream(filename), "UTF-8");
        try {
            final StringWriter swriter = new StringWriter(50000);
            final UnsynchronizedBufferedWriter writer = new UnsynchronizedBufferedWriter(swriter);
            writer.write(reader);
            return swriter.toString();
        }
        finally {
            if (reader != null) {
                reader.close();
            }
        }
    }
    
    public static void writeToFile(final String contents, final File file) throws IOException {
        final OutputStreamWriter fw = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");
        final UnsynchronizedBufferedWriter writer = new UnsynchronizedBufferedWriter(fw);
        try {
            writer.write(contents);
            writer.flush();
        }
        finally {
            if (null != writer) {
                writer.close();
            }
        }
    }
    
    public static void writeToFile(final byte[] contents, final File file) throws IOException {
        final FileOutputStream fw = new FileOutputStream(file);
        try {
            fw.write(contents);
            fw.flush();
        }
        finally {
            if (null != fw) {
                fw.close();
            }
        }
    }
    
    public static void writeToFile(final String contents, final String fileOutName) throws IOException {
        writeToFile(contents, new File(fileOutName));
    }
    
    public static long writeToFile(final InputStream in, final String fileOutName) throws IOException {
        OutputStream out = null;
        try {
            out = new BufferedOutputStream(new FileOutputStream(fileOutName));
            return copy(in, out);
        }
        finally {
            if (null != in) {
                in.close();
            }
            if (null != out) {
                out.close();
            }
        }
    }
    
    public static long appendToFile(final InputStream in, final String fileOutName) throws IOException {
        OutputStream out = null;
        try {
            out = new BufferedOutputStream(new FileOutputStream(fileOutName, true));
            return copy(in, out);
        }
        finally {
            if (null != in) {
                in.close();
            }
            if (null != out) {
                out.close();
            }
        }
    }
    
    public static long copyUrlB(final String urlString, final OutputStream out, final int bufferSize) throws IOException {
        InputStream is = null;
        URL url;
        try {
            url = new URL(urlString);
        }
        catch (MalformedURLException e) {
            throw new IOException("** MalformedURLException on URL <" + urlString + ">\n" + e.getMessage() + "\n");
        }
        long count;
        try {
            final URLConnection connection = url.openConnection();
            HttpURLConnection httpConnection = null;
            if (connection instanceof HttpURLConnection) {
                httpConnection = (HttpURLConnection)connection;
                httpConnection.addRequestProperty("Accept-Encoding", "gzip");
            }
            if (IO.showHeaders) {
                System.out.println("\nREQUEST Properties for " + urlString + ": ");
                final Map reqs = connection.getRequestProperties();
                for (final String key : reqs.keySet()) {
                    final List values = reqs.get(key);
                    System.out.print(" " + key + ": ");
                    for (int i = 0; i < values.size(); ++i) {
                        final String v = values.get(i);
                        System.out.print(v + " ");
                    }
                    System.out.println("");
                }
            }
            if (httpConnection != null) {
                final int responseCode = httpConnection.getResponseCode();
                if (responseCode / 100 != 2) {
                    throw new IOException("** Cant open URL <" + urlString + ">\n Response code = " + responseCode + "\n" + httpConnection.getResponseMessage() + "\n");
                }
            }
            if (IO.showHeaders && httpConnection != null) {
                final int code = httpConnection.getResponseCode();
                final String response = httpConnection.getResponseMessage();
                System.out.println("\nRESPONSE for " + urlString + ": ");
                System.out.println(" HTTP/1.x " + code + " " + response);
                System.out.println("Headers: ");
                int j = 1;
                while (true) {
                    final String header = connection.getHeaderField(j);
                    final String key2 = connection.getHeaderFieldKey(j);
                    if (header == null) {
                        break;
                    }
                    if (key2 == null) {
                        break;
                    }
                    System.out.println(" " + key2 + ": " + header);
                    ++j;
                }
            }
            is = connection.getInputStream();
            if ("gzip".equalsIgnoreCase(connection.getContentEncoding())) {
                is = new BufferedInputStream(new GZIPInputStream(is), 1000);
            }
            if (out == null) {
                count = copy2null(is, bufferSize);
            }
            else {
                count = copyB(is, out, bufferSize);
            }
        }
        catch (ConnectException e2) {
            if (IO.showStackTrace) {
                e2.printStackTrace();
            }
            throw new IOException("** ConnectException on URL: <" + urlString + ">\n" + e2.getMessage() + "\nServer probably not running");
        }
        catch (UnknownHostException e3) {
            if (IO.showStackTrace) {
                e3.printStackTrace();
            }
            throw new IOException("** UnknownHostException on URL: <" + urlString + ">\n");
        }
        catch (Exception e4) {
            if (IO.showStackTrace) {
                e4.printStackTrace();
            }
            throw new IOException("** Exception on URL: <" + urlString + ">\n" + e4);
        }
        finally {
            if (is != null) {
                is.close();
            }
        }
        return count;
    }
    
    public static String readURLtoFile(final String urlString, final File file) {
        OutputStream out;
        try {
            out = new BufferedOutputStream(new FileOutputStream(file));
        }
        catch (IOException e) {
            if (IO.showStackTrace) {
                e.printStackTrace();
            }
            return "** IOException opening file: <" + file + ">\n" + e.getMessage() + "\n";
        }
        try {
            copyUrlB(urlString, out, 20000);
            return "ok";
        }
        catch (IOException e) {
            if (IO.showStackTrace) {
                e.printStackTrace();
            }
            return "** IOException reading URL: <" + urlString + ">\n" + e.getMessage() + "\n";
        }
        finally {
            try {
                out.close();
            }
            catch (IOException e2) {
                return "** IOException closing file : <" + file + ">\n" + e2.getMessage() + "\n";
            }
        }
    }
    
    public static byte[] readURLContentsToByteArray(final String urlString) throws IOException {
        final ByteArrayOutputStream bout = new ByteArrayOutputStream(200000);
        copyUrlB(urlString, bout, 200000);
        return bout.toByteArray();
    }
    
    public static String readURLtoFileWithExceptions(final String urlString, final File file) throws IOException {
        return readURLtoFileWithExceptions(urlString, file, IO.default_socket_buffersize);
    }
    
    public static String readURLtoFileWithExceptions(final String urlString, final File file, final int buffer_size) throws IOException {
        final OutputStream out = new BufferedOutputStream(new FileOutputStream(file));
        try {
            copyUrlB(urlString, out, buffer_size);
            return "ok";
        }
        finally {
            try {
                out.close();
            }
            catch (IOException e) {
                return "** IOException closing file : <" + file + ">\n" + e.getMessage() + "\n";
            }
        }
    }
    
    public static String readURLcontentsWithException(final String urlString) throws IOException {
        final ByteArrayOutputStream bout = new ByteArrayOutputStream(20000);
        copyUrlB(urlString, bout, 20000);
        return bout.toString();
    }
    
    public static String readURLcontents(final String urlString) {
        try {
            return readURLcontentsWithException(urlString);
        }
        catch (IOException e) {
            return e.getMessage();
        }
    }
    
    public static HttpResult putToURL(final String urlString, final String contents) {
        URL url;
        try {
            url = new URL(urlString);
        }
        catch (MalformedURLException e) {
            return new HttpResult(-1, "** MalformedURLException on URL (" + urlString + ")\n" + e.getMessage());
        }
        try {
            final HttpURLConnection c = (HttpURLConnection)url.openConnection();
            c.setDoOutput(true);
            c.setRequestMethod("PUT");
            final OutputStream out = new BufferedOutputStream(c.getOutputStream());
            copy(new ByteArrayInputStream(contents.getBytes()), out);
            out.flush();
            out.close();
            final int code = c.getResponseCode();
            final String mess = c.getResponseMessage();
            return new HttpResult(code, mess);
        }
        catch (ConnectException e2) {
            if (IO.showStackTrace) {
                e2.printStackTrace();
            }
            return new HttpResult(-2, "** ConnectException on URL: <" + urlString + ">\n" + e2.getMessage() + "\nServer probably not running");
        }
        catch (IOException e3) {
            if (IO.showStackTrace) {
                e3.printStackTrace();
            }
            return new HttpResult(-3, "** IOException on URL: (" + urlString + ")\n" + e3.getMessage());
        }
    }
    
    public static void testRead() {
        final String baseUrl = "http://moca.virtual.museum/legac/legac01.htm";
        final String baseDir = "";
        final File dir = new File(baseDir);
        dir.mkdirs();
        for (int i = 1; i < 159; ++i) {
            final String n = StringUtil.padZero(i, 3);
            final String filename = n + ".jpg";
            System.out.println("Open " + baseDir + filename);
            final File file = new File(baseDir + filename);
            readURLtoFile(baseUrl + filename, file);
        }
    }
    
    public static void main4(final String[] args) {
        final String url = "http://whoopee:8080/thredds/dodsC/test/2005052412_NAM.wmo.dods?Best_4-layer_lifted_index";
        final String filenameOut = "C:/temp/tempFile4.compress";
        final File f = new File(filenameOut);
        final long start = System.currentTimeMillis();
        final String result = readURLtoFile(url, f);
        final double took = 0.001 * (System.currentTimeMillis() - start);
        System.out.println(result);
        System.out.println(" that took = " + took + "sec");
        System.out.println(" file size = " + f.length());
    }
    
    public static void mainn(final String[] args) {
        long start = System.currentTimeMillis();
        final ArrayList fileList = new ArrayList();
        final ArrayList rafList = new ArrayList();
        int count = 0;
        while (true) {
            Label_0074: {
                try {
                    final File temp = File.createTempFile("test", "tmp");
                    fileList.add(temp);
                    final java.io.RandomAccessFile raf = new java.io.RandomAccessFile(temp, "r");
                    rafList.add(raf);
                    break Label_0074;
                }
                catch (IOException e) {
                    e.printStackTrace();
                    long took = System.currentTimeMillis() - start;
                    System.out.println(" Created and opened " + count + " files in " + took + " msecs");
                    start = System.currentTimeMillis();
                    for (final java.io.RandomAccessFile raf2 : rafList) {
                        try {
                            raf2.close();
                        }
                        catch (IOException e2) {
                            e2.printStackTrace();
                        }
                    }
                    took = System.currentTimeMillis() - start;
                    System.out.println(" Closed " + count + " files in " + took + " msecs");
                    for (final File file : fileList) {
                        file.delete();
                    }
                    took = System.currentTimeMillis() - start;
                    System.out.println(" Deleted " + count + " files in " + took + " msecs");
                    count = 0;
                    took = System.currentTimeMillis() - start;
                    final File dir = new File("/var/tmp/");
                    final File[] list = dir.listFiles();
                    for (int i = 0; i < list.length; ++i) {
                        final File file2 = list[i];
                        if (file2.getName().endsWith("tmp")) {
                            file2.delete();
                            ++count;
                        }
                    }
                    took = System.currentTimeMillis() - start;
                    System.out.println(" Deleted " + count + " files in " + took + " msecs");
                    return;
                    // iftrue(Label_0024:, ++count % 50 != 0)
                    System.out.println(" " + count);
                    continue;
                }
            }
            break;
        }
    }
    
    public static void main(final String[] args) throws IOException {
        final FileOutputStream f = new FileOutputStream("C:/TEMP/read.txt");
        IO.fout = new Formatter(f);
        final String url = "http://newmotherlode.ucar.edu:8081/thredds/fileServer/fmrc/NCEP/GFS/Global_onedeg/files/GFS_Global_onedeg_20080922_0600.grib2";
        final long start = System.currentTimeMillis();
        copyUrlB(url, null, 1000000);
        final double took = 0.001 * (System.currentTimeMillis() - start);
        IO.fout.flush();
        f.close();
        System.out.println(" that took = " + took + " sec");
    }
    
    static {
        IO.default_file_buffersize = 9200;
        IO.default_socket_buffersize = 64000;
        IO.showStackTrace = false;
        IO.debug = false;
        IO.showResponse = false;
        IO.showHeaders = false;
    }
    
    public static class HttpResult
    {
        public int statusCode;
        public String message;
        
        HttpResult(final int code, final String message) {
            this.statusCode = code;
            this.message = message;
        }
    }
}
