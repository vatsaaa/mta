// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.util;

public interface DebugFlags
{
    boolean isSet(final String p0);
    
    void set(final String p0, final boolean p1);
}
