// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.util;

import java.util.Iterator;
import java.util.List;
import java.util.Collections;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.net.URLDecoder;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.io.IOException;
import java.io.File;
import ucar.unidata.util.StringUtil;

public class DiskCache
{
    private static String root;
    private static boolean standardPolicy;
    private static boolean checkExist;
    public static boolean simulateUnwritableDir;
    
    public static void setRootDirectory(String cacheDir) {
        if (!cacheDir.endsWith("/")) {
            cacheDir += "/";
        }
        DiskCache.root = StringUtil.replace(cacheDir, '\\', "/");
        makeRootDirectory();
    }
    
    public static void makeRootDirectory() {
        final File dir = new File(DiskCache.root);
        if (!dir.exists() && !dir.mkdirs()) {
            throw new IllegalStateException("DiskCache.setRootDirectory(): could not create root directory <" + DiskCache.root + ">.");
        }
        DiskCache.checkExist = true;
    }
    
    public static String getRootDirectory() {
        return DiskCache.root;
    }
    
    public static void setCachePolicy(final boolean alwaysInCache) {
        DiskCache.standardPolicy = alwaysInCache;
    }
    
    public static File getFileStandardPolicy(final String fileLocation) {
        return getFile(fileLocation, DiskCache.standardPolicy);
    }
    
    public static File getFile(final String fileLocation, final boolean alwaysInCache) {
        if (alwaysInCache) {
            return getCacheFile(fileLocation);
        }
        final File f = new File(fileLocation);
        if (f.exists()) {
            return f;
        }
        try {
            if (!DiskCache.simulateUnwritableDir && f.createNewFile()) {
                f.delete();
                return f;
            }
        }
        catch (IOException ex) {}
        return getCacheFile(fileLocation);
    }
    
    public static File getCacheFile(final String fileLocation) {
        final File f = new File(makeCachePath(fileLocation));
        if (f.exists()) {
            f.setLastModified(System.currentTimeMillis());
        }
        if (!DiskCache.checkExist) {
            final File dir = f.getParentFile();
            dir.mkdirs();
            DiskCache.checkExist = true;
        }
        return f;
    }
    
    private static String makeCachePath(String fileLocation) {
        String cachePath;
        try {
            fileLocation = fileLocation.replace('\\', '/');
            cachePath = URLEncoder.encode(fileLocation, "UTF8");
        }
        catch (UnsupportedEncodingException e) {
            cachePath = URLEncoder.encode(fileLocation);
        }
        return DiskCache.root + cachePath;
    }
    
    public static void showCache(final PrintStream pw) {
        pw.println("Cache files");
        pw.println("Size   LastModified       Filename");
        final File dir = new File(DiskCache.root);
        for (final File file : dir.listFiles()) {
            String org = null;
            try {
                org = URLDecoder.decode(file.getName(), "UTF8");
            }
            catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            pw.println(" " + file.length() + " " + new Date(file.lastModified()) + " " + org);
        }
    }
    
    public static void cleanCache(final Date cutoff, final StringBuilder sbuff) {
        if (sbuff != null) {
            sbuff.append("CleanCache files before ").append(cutoff).append("\n");
        }
        final File dir = new File(DiskCache.root);
        for (final File file : dir.listFiles()) {
            final Date lastMod = new Date(file.lastModified());
            if (lastMod.before(cutoff)) {
                file.delete();
                if (sbuff != null) {
                    sbuff.append(" delete ").append(file).append(" (").append(lastMod).append(")\n");
                }
            }
        }
    }
    
    public static void cleanCache(final long maxBytes, final StringBuilder sbuff) {
        cleanCache(maxBytes, new FileAgeComparator(), sbuff);
    }
    
    public static void cleanCache(final long maxBytes, final Comparator<File> fileComparator, final StringBuilder sbuff) {
        if (sbuff != null) {
            sbuff.append("DiskCache clean maxBytes= " + maxBytes + "on dir " + DiskCache.root + "\n");
        }
        final File dir = new File(DiskCache.root);
        final File[] files = dir.listFiles();
        final List<File> fileList = Arrays.asList(files);
        Collections.sort(fileList, fileComparator);
        long total = 0L;
        long total_delete = 0L;
        for (final File file : fileList) {
            if (file.length() + total > maxBytes) {
                total_delete += file.length();
                if (sbuff != null) {
                    sbuff.append(" delete " + file + " (" + file.length() + ")\n");
                }
                file.delete();
            }
            else {
                total += file.length();
            }
        }
        if (sbuff != null) {
            sbuff.append("Total bytes deleted= " + total_delete + "\n");
            sbuff.append("Total bytes left in cache= " + total + "\n");
        }
    }
    
    static void make(final String filename) throws IOException {
        final File want = getCacheFile(filename);
        System.out.println("make=" + want.getPath() + "; exists = " + want.exists());
        if (!want.exists()) {
            want.createNewFile();
        }
        System.out.println(" canRead= " + want.canRead() + " canWrite = " + want.canWrite() + " lastMod = " + new Date(want.lastModified()));
        try {
            final String enc = URLEncoder.encode(filename, "UTF8");
            System.out.println(" original=" + URLDecoder.decode(enc, "UTF8"));
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
    
    public static void main(final String[] args) throws IOException {
        setRootDirectory("C:/temp/chill/");
        make("C:/junk.txt");
        make("C:/some/enchanted/evening/joots+3478.txt");
        make("http://www.unidata.ucar.edu/some/enc hanted/eve'ning/nowrite.gibberish");
        showCache(System.out);
        final StringBuilder sbuff = new StringBuilder();
        cleanCache(10000000L, sbuff);
        System.out.println(sbuff);
    }
    
    static {
        DiskCache.root = null;
        DiskCache.standardPolicy = false;
        DiskCache.checkExist = false;
        DiskCache.simulateUnwritableDir = false;
        DiskCache.root = System.getProperty("nj22.cache");
        if (DiskCache.root == null) {
            String home = System.getProperty("user.home");
            if (home == null) {
                home = System.getProperty("user.dir");
            }
            if (home == null) {
                home = ".";
            }
            DiskCache.root = home + "/.unidata/cache/";
        }
        final String policy = System.getProperty("nj22.cachePolicy");
        if (policy != null) {
            DiskCache.standardPolicy = policy.equalsIgnoreCase("true");
        }
    }
    
    private static class FileSizeComparator implements Comparator<File>
    {
        public int compare(final File f1, final File f2) {
            return (f1.length() < f2.length()) ? -1 : ((f1.length() == f2.length()) ? 0 : 1);
        }
    }
    
    private static class FileAgeComparator implements Comparator<File>
    {
        public int compare(final File f1, final File f2) {
            return (f1.lastModified() < f2.lastModified()) ? 1 : ((f1.lastModified() == f2.lastModified()) ? 0 : -1);
        }
    }
}
