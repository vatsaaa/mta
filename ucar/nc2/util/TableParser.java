// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.util;

import java.util.Iterator;
import ucar.unidata.util.StringUtil;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
import java.util.ArrayList;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileInputStream;
import java.net.URL;
import java.util.List;

public class TableParser
{
    static String testName;
    static String testName2;
    static String testName3;
    
    public static List<Record> readTable(final String urlString, final String format, final int maxLines) throws IOException, NumberFormatException {
        InputStream ios;
        if (urlString.startsWith("http:")) {
            final URL url = new URL(urlString);
            ios = url.openStream();
        }
        else {
            ios = new FileInputStream(urlString);
        }
        return readTable(ios, format, maxLines);
    }
    
    public static List<Record> readTable(final InputStream ios, final String format, final int maxLines) throws IOException, NumberFormatException {
        final List<Field> fields = new ArrayList<Field>();
        int start = 0;
        final StringTokenizer stoker = new StringTokenizer(format, " ,");
        while (stoker.hasMoreTokens()) {
            String tok = stoker.nextToken();
            Class type = String.class;
            final char last = tok.charAt(tok.length() - 1);
            if (last == 'i') {
                type = Integer.TYPE;
            }
            if (last == 'd') {
                type = Double.TYPE;
            }
            if (type != String.class) {
                tok = tok.substring(0, tok.length() - 1);
            }
            final int end = Integer.parseInt(tok);
            fields.add(new Field(start, end, type));
            start = end;
        }
        final List<Record> records = new ArrayList<Record>();
        final BufferedReader dataIS = new BufferedReader(new InputStreamReader(ios));
        int count = 0;
        while (maxLines < 0 || count < maxLines) {
            final String line = dataIS.readLine();
            if (line == null) {
                break;
            }
            if (line.startsWith("#")) {
                continue;
            }
            if (line.trim().length() == 0) {
                continue;
            }
            final Record r = Record.make(line, fields);
            if (r != null) {
                records.add(r);
            }
            ++count;
        }
        dataIS.close();
        return records;
    }
    
    public static void main(final String[] args) throws IOException {
        final List recs = readTable(TableParser.testName3, "3,15,54,60d,67d,73d", 50000);
        for (int i = 0; i < recs.size(); ++i) {
            final Record record = recs.get(i);
            for (int j = 0; j < record.values.size(); ++j) {
                final Object s = record.values.get(j);
                System.out.print(" " + s.toString());
            }
            System.out.println();
        }
    }
    
    static {
        TableParser.testName = "C:/data/station/adde/STNDB.TXT";
        TableParser.testName2 = "http://localhost:8080/test/STNDB.TXT";
        TableParser.testName3 = "C:/dev/thredds/cdm/src/main/resources/resources/nj22/tables/nexrad.tbl";
    }
    
    private static class Field
    {
        int start;
        int end;
        Class type;
        
        Field(final int start, final int end, final Class type) {
            this.start = start;
            this.end = end;
            this.type = type;
        }
        
        Object parse(final String line) throws NumberFormatException {
            String svalue = (this.end > line.length()) ? line.substring(this.start) : line.substring(this.start, this.end);
            if (this.type == String.class) {
                return svalue;
            }
            try {
                svalue = StringUtil.remove(svalue, 32);
                if (this.type == Double.TYPE) {
                    return new Double(svalue);
                }
                if (this.type == Integer.TYPE) {
                    return new Integer(svalue);
                }
            }
            catch (NumberFormatException e) {
                System.out.printf("  [%d,%d) = <%s> %n", this.start, this.end, svalue);
                throw e;
            }
            return null;
        }
    }
    
    public static class Record
    {
        private List<Object> values;
        
        public Record() {
            this.values = new ArrayList<Object>();
        }
        
        static Record make(final String line, final List fields) {
            try {
                final Record r = new Record();
                for (final Object field : fields) {
                    final Field f = (Field)field;
                    r.values.add(f.parse(line));
                }
                return r;
            }
            catch (NumberFormatException e) {
                System.out.printf("Bad line=%s %n", line);
                return null;
            }
        }
        
        public int nfields() {
            return this.values.size();
        }
        
        public Object get(final int k) {
            return this.values.get(k);
        }
    }
}
