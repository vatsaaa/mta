// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.util;

public class NamedAnything implements NamedObject
{
    private String name;
    private String desc;
    
    public NamedAnything(final String name, final String desc) {
        this.name = name;
        this.desc = desc;
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getDescription() {
        return this.desc;
    }
    
    @Override
    public String toString() {
        return this.name;
    }
}
