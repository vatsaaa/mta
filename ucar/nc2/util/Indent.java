// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.util;

public class Indent
{
    private int nspaces;
    private int level;
    private StringBuilder blanks;
    private String indent;
    
    public Indent(final int nspaces) {
        this.nspaces = 0;
        this.level = 0;
        this.indent = "";
        this.nspaces = nspaces;
        this.blanks = new StringBuilder();
        for (int i = 0; i < 100 * nspaces; ++i) {
            this.blanks.append(" ");
        }
    }
    
    public Indent incr() {
        this.setIndentLevel(++this.level);
        return this;
    }
    
    public Indent decr() {
        this.setIndentLevel(--this.level);
        return this;
    }
    
    @Override
    public String toString() {
        return this.indent;
    }
    
    public void setIndentLevel(final int level) {
        this.level = level;
        this.indent = this.blanks.substring(0, level * this.nspaces);
    }
}
