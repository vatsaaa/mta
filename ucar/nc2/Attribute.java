// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2;

import java.nio.ByteBuffer;
import ucar.ma2.ArrayChar;
import ucar.unidata.util.Parameter;
import java.util.List;
import ucar.ma2.Index;
import ucar.ma2.Array;
import ucar.ma2.DataType;
import net.jcip.annotations.Immutable;

@Immutable
public class Attribute
{
    private final String name;
    private String svalue;
    private DataType dataType;
    private int nelems;
    private Array values;
    private int hashCode;
    
    public String getName() {
        return this.name;
    }
    
    public DataType getDataType() {
        return this.dataType;
    }
    
    public boolean isString() {
        return this.dataType == DataType.STRING;
    }
    
    public boolean isArray() {
        return this.getLength() > 1;
    }
    
    public int getLength() {
        return this.nelems;
    }
    
    public boolean isUnsigned() {
        return this.values != null && this.values.isUnsigned();
    }
    
    public Array getValues() {
        if (this.values == null && this.svalue != null) {
            (this.values = Array.factory(String.class, new int[] { 1 })).setObject(this.values.getIndex(), this.svalue);
        }
        return this.values;
    }
    
    public Object getValue(final int index) {
        if (this.isString()) {
            return this.getStringValue(index);
        }
        return this.getNumericValue(index);
    }
    
    public String getStringValue() {
        return (this.svalue != null) ? this.svalue : this.getStringValue(0);
    }
    
    public String getStringValue(final int index) {
        if (!this.isString() || index < 0 || index >= this.nelems) {
            return null;
        }
        if (this.svalue != null && index == 0) {
            return this.svalue;
        }
        return (String)this.values.getObject(index);
    }
    
    public Number getNumericValue() {
        return this.getNumericValue(0);
    }
    
    public Number getNumericValue(final int index) {
        if (this.isString() || index < 0 || index >= this.nelems) {
            return null;
        }
        if (this.dataType == DataType.STRING) {
            try {
                return new Double(this.getStringValue(index));
            }
            catch (NumberFormatException e) {
                return null;
            }
        }
        if (this.dataType == DataType.BYTE) {
            return this.values.getByte(index);
        }
        if (this.dataType == DataType.SHORT) {
            return this.values.getShort(index);
        }
        if (this.dataType == DataType.INT) {
            return this.values.getInt(index);
        }
        if (this.dataType == DataType.FLOAT) {
            return this.values.getFloat(index);
        }
        if (this.dataType == DataType.DOUBLE) {
            return this.values.getDouble(index);
        }
        if (this.dataType == DataType.LONG) {
            return this.values.getLong(index);
        }
        return null;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || !(o instanceof Attribute)) {
            return false;
        }
        final Attribute att = (Attribute)o;
        if (!this.name.equals(att.name)) {
            return false;
        }
        if (this.nelems != att.nelems) {
            return false;
        }
        if (!this.dataType.equals(att.dataType)) {
            return false;
        }
        if (this.svalue != null) {
            return this.svalue.equals(att.getStringValue());
        }
        if (this.values != null) {
            for (int i = 0; i < this.getLength(); ++i) {
                final int r1 = this.isString() ? this.getStringValue(i).hashCode() : this.getNumericValue(i).hashCode();
                final int r2 = att.isString() ? att.getStringValue(i).hashCode() : att.getNumericValue(i).hashCode();
                if (r1 != r2) {
                    return false;
                }
            }
        }
        return true;
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int result = 17;
            result = 37 * result + this.getName().hashCode();
            result = 37 * result + this.nelems;
            result = 37 * result + this.getDataType().hashCode();
            if (this.svalue != null) {
                result = 37 * result + this.svalue.hashCode();
            }
            else if (this.values != null) {
                for (int i = 0; i < this.getLength(); ++i) {
                    final int h = this.isString() ? this.getStringValue(i).hashCode() : this.getNumericValue(i).hashCode();
                    result = 37 * result + h;
                }
            }
            this.hashCode = result;
        }
        return this.hashCode;
    }
    
    @Override
    public String toString() {
        return this.toString(false);
    }
    
    public String toString(final boolean strict) {
        final StringBuilder buff = new StringBuilder();
        buff.append(strict ? NetcdfFile.escapeName(this.getName()) : this.getName());
        if (this.isString()) {
            buff.append(" = ");
            for (int i = 0; i < this.getLength(); ++i) {
                if (i != 0) {
                    buff.append(", ");
                }
                final String val = this.getStringValue(i);
                if (val != null) {
                    buff.append("\"").append(NCdumpW.encodeString(val)).append("\"");
                }
            }
        }
        else {
            buff.append(" = ");
            for (int i = 0; i < this.getLength(); ++i) {
                if (i != 0) {
                    buff.append(", ");
                }
                buff.append(this.getNumericValue(i));
                if (this.dataType == DataType.FLOAT) {
                    buff.append("f");
                }
                else if (this.dataType == DataType.SHORT) {
                    if (this.isUnsigned()) {
                        buff.append("US");
                    }
                    else {
                        buff.append("S");
                    }
                }
                else if (this.dataType == DataType.BYTE) {
                    if (this.isUnsigned()) {
                        buff.append("UB");
                    }
                    else {
                        buff.append("B");
                    }
                }
                else if (this.dataType == DataType.LONG) {
                    if (this.isUnsigned()) {
                        buff.append("UL");
                    }
                    else {
                        buff.append("L");
                    }
                }
                else if (this.dataType == DataType.INT && this.isUnsigned()) {
                    buff.append("U");
                }
            }
        }
        return buff.toString();
    }
    
    public Attribute(final String name, final Attribute from) {
        this.hashCode = 0;
        this.name = name;
        this.dataType = from.dataType;
        this.nelems = from.nelems;
        this.svalue = from.svalue;
        this.values = from.values;
    }
    
    public Attribute(final String name, final String val) {
        this.hashCode = 0;
        this.name = name;
        this.setStringValue(val);
    }
    
    public Attribute(final String name, final Number val) {
        this.hashCode = 0;
        this.name = name;
        final int[] shape = { 1 };
        final DataType dt = DataType.getType(val.getClass());
        final Array vala = Array.factory(dt.getPrimitiveClassType(), shape);
        final Index ima = vala.getIndex();
        vala.setDouble(ima.set0(0), val.doubleValue());
        this.setValues(vala);
    }
    
    public Attribute(final String name, final Array values) {
        this.hashCode = 0;
        this.name = name;
        this.setValues(values);
    }
    
    public Attribute(final String name, final DataType dataType) {
        this.hashCode = 0;
        this.name = name;
        this.dataType = ((dataType == DataType.CHAR) ? DataType.STRING : dataType);
        this.nelems = 0;
    }
    
    public Attribute(final String name, final List values) {
        this.hashCode = 0;
        this.name = name;
        final int n = values.size();
        Object pa = null;
        final Class c = values.get(0).getClass();
        if (c == String.class) {
            final String[] va = (String[])(pa = new String[n]);
            for (int i = 0; i < n; ++i) {
                va[i] = (String)values.get(i);
            }
        }
        else if (c == Integer.class) {
            final int[] va2 = (int[])(pa = new int[n]);
            for (int i = 0; i < n; ++i) {
                va2[i] = (Integer)(Object)values.get(i);
            }
        }
        else if (c == Double.class) {
            final double[] va3 = (double[])(pa = new double[n]);
            for (int i = 0; i < n; ++i) {
                va3[i] = values.get(i);
            }
        }
        else if (c == Float.class) {
            final float[] va4 = (float[])(pa = new float[n]);
            for (int i = 0; i < n; ++i) {
                va4[i] = values.get(i);
            }
        }
        else if (c == Short.class) {
            final short[] va5 = (short[])(pa = new short[n]);
            for (int i = 0; i < n; ++i) {
                va5[i] = (Short)(Object)values.get(i);
            }
        }
        else if (c == Byte.class) {
            final byte[] va6 = (byte[])(pa = new byte[n]);
            for (int i = 0; i < n; ++i) {
                va6[i] = (Byte)(Object)values.get(i);
            }
        }
        else {
            if (c != Long.class) {
                throw new IllegalArgumentException("unknown type for Attribute = " + c.getName());
            }
            final long[] va7 = (long[])(pa = new long[n]);
            for (int i = 0; i < n; ++i) {
                va7[i] = values.get(i);
            }
        }
        this.setValues(Array.factory(c, new int[] { n }, pa));
    }
    
    public Attribute(final Parameter param) {
        this.hashCode = 0;
        this.name = param.getName();
        if (param.isString()) {
            this.setStringValue(param.getStringValue());
        }
        else {
            final double[] values = param.getNumericValues();
            final int n = values.length;
            final Array vala = Array.factory(DataType.DOUBLE.getPrimitiveClassType(), new int[] { n }, values);
            this.setValues(vala);
        }
    }
    
    protected Attribute(final String name) {
        this.hashCode = 0;
        this.name = name;
    }
    
    private void setStringValue(String val) {
        if (val == null) {
            throw new IllegalArgumentException("Attribute value cannot be null");
        }
        int len;
        for (len = val.length(); len > 0 && val.charAt(len - 1) == '\0'; --len) {}
        if (len != val.length()) {
            val = val.substring(0, len);
        }
        this.svalue = val;
        this.nelems = 1;
        this.dataType = DataType.STRING;
    }
    
    protected void setValues(Array arr) {
        if (DataType.getType(arr.getElementType()) == null) {
            throw new IllegalArgumentException("Cant set Attribute with type " + arr.getElementType());
        }
        if (arr.getElementType() == Character.TYPE) {
            final ArrayChar carr = (ArrayChar)arr;
            if (carr.getRank() == 1) {
                this.svalue = carr.getString();
                this.nelems = 1;
                this.dataType = DataType.STRING;
                return;
            }
            arr = carr.make1DStringArray();
        }
        if (arr.getElementType() == ByteBuffer.class) {
            int totalLen = 0;
            arr.resetLocalIterator();
            while (arr.hasNext()) {
                final ByteBuffer bb = (ByteBuffer)arr.next();
                totalLen += bb.limit();
            }
            final byte[] ba = new byte[totalLen];
            int pos = 0;
            arr.resetLocalIterator();
            while (arr.hasNext()) {
                final ByteBuffer bb2 = (ByteBuffer)arr.next();
                System.arraycopy(bb2.array(), 0, ba, pos, bb2.limit());
                pos += bb2.limit();
            }
            arr = Array.factory(DataType.BYTE, new int[] { totalLen }, ba);
        }
        if (arr.getRank() > 1) {
            arr = arr.reshape(new int[] { (int)arr.getSize() });
        }
        this.values = arr;
        this.nelems = (int)arr.getSize();
        this.dataType = DataType.getType(arr.getElementType());
        this.hashCode = 0;
    }
}
