// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2;

import org.slf4j.LoggerFactory;
import java.io.IOException;
import ucar.ma2.Array;
import ucar.nc2.util.CancelTask;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Section;
import org.slf4j.Logger;

class SliceReader implements ProxyReader
{
    private static Logger log;
    private Variable orgClient;
    private int sliceDim;
    private Section slice;
    
    SliceReader(final Variable orgClient, final int dim, final Section slice) throws InvalidRangeException {
        this.orgClient = orgClient;
        this.sliceDim = dim;
        this.slice = slice;
    }
    
    public Array reallyRead(final Variable client, final CancelTask cancelTask) throws IOException {
        Array data;
        try {
            data = this.orgClient._read(this.slice);
        }
        catch (InvalidRangeException e) {
            SliceReader.log.error("InvalidRangeException in slice, var=" + client);
            throw new IllegalStateException(e.getMessage());
        }
        data = data.reduce(this.sliceDim);
        return data;
    }
    
    public Array reallyRead(final Variable client, final Section section, final CancelTask cancelTask) throws IOException, InvalidRangeException {
        final Section orgSection = new Section(section.getRanges());
        orgSection.insertRange(this.sliceDim, this.slice.getRange(this.sliceDim));
        final Array data = this.orgClient._read(orgSection);
        data.reduce(this.sliceDim);
        return data;
    }
    
    static {
        SliceReader.log = LoggerFactory.getLogger(SliceReader.class);
    }
}
