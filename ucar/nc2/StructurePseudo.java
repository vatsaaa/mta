// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2;

import ucar.ma2.Range;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Section;
import java.io.IOException;
import ucar.ma2.StructureMembers;
import ucar.ma2.ArrayStructureMA;
import ucar.ma2.Array;
import ucar.nc2.util.CancelTask;
import java.util.Iterator;
import java.util.Collection;
import ucar.ma2.DataType;
import java.util.ArrayList;
import java.util.List;

public class StructurePseudo extends Structure
{
    private static boolean debugRecord;
    private List<Variable> orgVariables;
    
    public StructurePseudo(final NetcdfFile ncfile, Group group, final String shortName, final Dimension dim) {
        super(ncfile, group, null, shortName);
        this.orgVariables = new ArrayList<Variable>();
        this.dataType = DataType.STRUCTURE;
        this.setDimensions(dim.getName());
        if (group == null) {
            group = ncfile.getRootGroup();
        }
        final List<Variable> vars = group.getVariables();
        for (final Variable orgV : vars) {
            final Dimension dim2 = orgV.getDimension(0);
            if (dim2 != null && dim2.equals(dim)) {
                final Variable memberV = new Variable(ncfile, group, this, orgV.getShortName());
                memberV.setDataType(orgV.getDataType());
                memberV.setSPobject(orgV.getSPobject());
                memberV.attributes.addAll(orgV.getAttributes());
                final List<Dimension> dims = new ArrayList<Dimension>(orgV.dimensions);
                dims.remove(0);
                memberV.setDimensions(dims);
                this.addMemberVariable(memberV);
                this.orgVariables.add(orgV);
            }
        }
        this.calcElementSize();
    }
    
    public StructurePseudo(final NetcdfFile ncfile, Group group, final String shortName, final List<String> varNames, final Dimension dim) {
        super(ncfile, group, null, shortName);
        this.orgVariables = new ArrayList<Variable>();
        this.dataType = DataType.STRUCTURE;
        this.setDimensions(dim.getName());
        if (group == null) {
            group = ncfile.getRootGroup();
        }
        for (final String name : varNames) {
            final Variable orgV = group.findVariable(name);
            if (orgV == null) {
                StructurePseudo.log.warn("StructurePseudo cannot find variable " + name);
            }
            else {
                final Dimension dim2 = orgV.getDimension(0);
                if (!dim2.equals(dim)) {
                    throw new IllegalArgumentException("Variable " + orgV.getNameAndDimensions() + " must have outermost dimension=" + dim);
                }
                final Variable memberV = new Variable(ncfile, group, this, orgV.getShortName());
                memberV.setDataType(orgV.getDataType());
                memberV.setSPobject(orgV.getSPobject());
                memberV.attributes.addAll(orgV.getAttributes());
                final List<Dimension> dims = new ArrayList<Dimension>(orgV.dimensions);
                dims.remove(0);
                memberV.setDimensions(dims);
                this.addMemberVariable(memberV);
                this.orgVariables.add(orgV);
            }
        }
        this.calcElementSize();
    }
    
    @Override
    public boolean removeMemberVariable(final Variable v) {
        if (super.removeMemberVariable(v)) {
            final java.util.Iterator<Variable> iter = this.orgVariables.iterator();
            while (iter.hasNext()) {
                final Variable mv = iter.next();
                if (mv.getShortName().equals(v.getShortName())) {
                    iter.remove();
                    return true;
                }
            }
        }
        return false;
    }
    
    @Override
    public Array reallyRead(final Variable mainv, final CancelTask cancelTask) throws IOException {
        if (StructurePseudo.debugRecord) {
            System.out.println(" read all psuedo records ");
        }
        final StructureMembers smembers = this.makeStructureMembers();
        final ArrayStructureMA asma = new ArrayStructureMA(smembers, this.getShape());
        for (final Variable v : this.orgVariables) {
            final Array data = v.read();
            final StructureMembers.Member m = smembers.findMember(v.getShortName());
            m.setDataArray(data);
        }
        return asma;
    }
    
    @Override
    public Array reallyRead(final Variable mainv, final Section section, final CancelTask cancelTask) throws IOException, InvalidRangeException {
        if (null == section) {
            return this._read();
        }
        if (StructurePseudo.debugRecord) {
            System.out.println(" read psuedo records " + section.getRange(0));
        }
        final String err = section.checkInRange(this.getShape());
        if (err != null) {
            throw new InvalidRangeException(err);
        }
        final Range r = section.getRange(0);
        final StructureMembers smembers = this.makeStructureMembers();
        final ArrayStructureMA asma = new ArrayStructureMA(smembers, section.getShape());
        for (final Variable v : this.orgVariables) {
            final List<Range> vsection = new ArrayList<Range>(v.getRanges());
            vsection.set(0, r);
            final Array data = v.read(vsection);
            final StructureMembers.Member m = smembers.findMember(v.getShortName());
            m.setDataArray(data);
        }
        return asma;
    }
    
    static {
        StructurePseudo.debugRecord = false;
    }
}
