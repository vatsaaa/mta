// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2;

import ucar.ma2.ArrayStructure;
import ucar.ma2.StructureData;
import ucar.ma2.Section;
import ucar.ma2.Range;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Array;
import java.io.IOException;
import ucar.ma2.StructureDataIterator;
import java.util.List;
import ucar.ma2.DataType;
import java.util.ArrayList;

public class Sequence extends Structure
{
    public Sequence(final NetcdfFile ncfile, final Group group, final Structure parent, final String shortName) {
        super(ncfile, group, parent, shortName);
        final List<Dimension> dims = new ArrayList<Dimension>();
        dims.add(Dimension.VLEN);
        this.setDimensions(dims);
        this.dataType = DataType.SEQUENCE;
    }
    
    @Override
    public StructureDataIterator getStructureIterator(final int bufferSize) throws IOException {
        return this.ncfile.getStructureIterator(this, bufferSize);
    }
    
    @Override
    public Array read(final int[] origin, final int[] shape) throws IOException, InvalidRangeException {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public Array read(final String sectionSpec) throws IOException, InvalidRangeException {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public Array read(final List<Range> ranges) throws IOException, InvalidRangeException {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public Array read(final Section section) throws IOException, InvalidRangeException {
        return this.read();
    }
    
    @Override
    public StructureData readStructure() throws IOException {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public StructureData readStructure(final int index) throws IOException, InvalidRangeException {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public ArrayStructure readStructure(final int start, final int count) throws IOException, InvalidRangeException {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public Variable slice(final int dim, final int value) throws InvalidRangeException {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public Variable section(final Section subsection) throws InvalidRangeException {
        throw new UnsupportedOperationException();
    }
}
