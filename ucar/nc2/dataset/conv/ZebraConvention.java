// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.conv;

import ucar.ma2.IndexIterator;
import ucar.ma2.Array;
import ucar.nc2.Dimension;
import java.io.IOException;
import ucar.nc2.Attribute;
import ucar.nc2.ncml.NcMLReader;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.Variable;
import ucar.nc2.NetcdfFile;

public class ZebraConvention extends ATDRadarConvention
{
    public static boolean isMine(final NetcdfFile ncfile) {
        final String s = ncfile.findAttValueIgnoreCase(null, "Convention", "none");
        return s.startsWith("Zebra");
    }
    
    public ZebraConvention() {
        this.conventionName = "Zebra";
    }
    
    @Override
    public void augmentDataset(final NetcdfDataset ds, final CancelTask cancelTask) throws IOException {
        NcMLReader.wrapNcMLresource(ds, "resources/nj22/coords/Zebra.ncml", cancelTask);
        final Dimension timeDim = ds.findDimension("time");
        final Variable base_time = ds.findVariable("base_time");
        final Variable time_offset = ds.findVariable("time_offset");
        final Variable time = ds.findVariable("time");
        if (timeDim == null || base_time == null || time_offset == null || time == null) {
            return;
        }
        final Attribute att = base_time.findAttribute("units");
        final String units = (att != null) ? att.getStringValue() : "seconds since 1970-01-01 00:00 UTC";
        time.addAttribute(new Attribute("units", units));
        Array data;
        try {
            final double baseValue = base_time.readScalarDouble();
            data = time_offset.read();
            final IndexIterator iter = data.getIndexIterator();
            while (iter.hasNext()) {
                iter.setDoubleCurrent(iter.getDoubleNext() + baseValue);
            }
        }
        catch (IOException ioe) {
            this.parseInfo.format("ZebraConvention failed to create time Coord Axis for file %s err= %s\n", ds.getLocation(), ioe);
            return;
        }
        time.setCachedData(data, true);
    }
}
