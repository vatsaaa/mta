// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.conv;

import ucar.nc2.dataset.CoordinateAxis1D;
import ucar.ma2.DataType;
import ucar.nc2.dataset.CoordinateAxis;
import ucar.unidata.geoloc.projection.Mercator;
import java.util.NoSuchElementException;
import ucar.unidata.geoloc.ProjectionImpl;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.ProjectionPointImpl;
import ucar.unidata.geoloc.projection.LambertConformal;
import ucar.nc2.units.SimpleUnit;
import ucar.nc2.constants.AxisType;
import ucar.nc2.dataset.VariableEnhanced;
import ucar.nc2.Dimension;
import ucar.nc2.Group;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.ma2.Array;
import ucar.ma2.ArrayByte;
import ucar.nc2.Attribute;
import ucar.nc2.dataset.VariableDS;
import ucar.nc2.Variable;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.NetcdfFile;
import ucar.nc2.dataset.ProjectionCT;
import ucar.nc2.dataset.CoordSysBuilder;

public class AWIPSsatConvention extends CoordSysBuilder
{
    private final boolean debugProj = false;
    private ProjectionCT projCT;
    private double startx;
    private double starty;
    private double dx;
    private double dy;
    
    public static boolean isMine(final NetcdfFile ncfile) {
        return null != ncfile.findGlobalAttribute("projName") && null != ncfile.findGlobalAttribute("lon00") && null != ncfile.findGlobalAttribute("lat00") && null != ncfile.findGlobalAttribute("lonNxNy") && null != ncfile.findGlobalAttribute("latNxNy") && null != ncfile.findGlobalAttribute("centralLon") && null != ncfile.findGlobalAttribute("centralLat") && null != ncfile.findDimension("x") && null != ncfile.findDimension("y") && null != ncfile.findVariable("image");
    }
    
    public AWIPSsatConvention() {
        this.projCT = null;
        this.conventionName = "AWIPS-Sat";
    }
    
    @Override
    public void augmentDataset(final NetcdfDataset ds, final CancelTask cancelTask) {
        if (null != ds.findVariable("x")) {
            return;
        }
        final Dimension dimx = ds.findDimension("x");
        final int nx = dimx.getLength();
        final Dimension dimy = ds.findDimension("y");
        final int ny = dimy.getLength();
        final String projName = ds.findAttValueIgnoreCase(null, "projName", "none");
        if (projName.equalsIgnoreCase("LAMBERT_CONFORMAL")) {
            this.projCT = this.makeLCProjection(ds, projName, nx, ny);
        }
        if (projName.equalsIgnoreCase("MERCATOR")) {
            this.projCT = this.makeMercatorProjection(ds, projName, nx, ny);
        }
        ds.addCoordinateAxis(this.makeXCoordAxis(ds, nx, "x"));
        ds.addCoordinateAxis(this.makeYCoordAxis(ds, ny, "y"));
        final Variable datav = ds.findVariable("image");
        final String long_name = ds.findAttValueIgnoreCase(null, "channel", null);
        if (null != long_name) {
            datav.addAttribute(new Attribute("long_name", long_name));
        }
        final ArrayByte.D1 missing_values = new ArrayByte.D1(2);
        missing_values.set(0, (byte)0);
        missing_values.set(1, (byte)(-127));
        datav.addAttribute(new Attribute("missing_values", missing_values));
        datav.addAttribute(new Attribute("_Unsigned", "true"));
        if (this.projCT != null) {
            final VariableDS v = this.makeCoordinateTransformVariable(ds, this.projCT);
            v.addAttribute(new Attribute("_CoordinateAxes", "x y"));
            ds.addVariable(null, v);
        }
        ds.finish();
    }
    
    @Override
    protected AxisType getAxisType(final NetcdfDataset ds, final VariableEnhanced ve) {
        final Variable v = (Variable)ve;
        final String vname = v.getName();
        if (vname.equalsIgnoreCase("x")) {
            return AxisType.GeoX;
        }
        if (vname.equalsIgnoreCase("lon")) {
            return AxisType.Lon;
        }
        if (vname.equalsIgnoreCase("y")) {
            return AxisType.GeoY;
        }
        if (vname.equalsIgnoreCase("lat")) {
            return AxisType.Lat;
        }
        if (vname.equalsIgnoreCase("record")) {
            return AxisType.Time;
        }
        final Dimension dim = v.getDimension(0);
        if (dim != null && dim.getName().equalsIgnoreCase("record")) {
            return AxisType.Time;
        }
        final String unit = ve.getUnitsString();
        if (unit != null) {
            if (SimpleUnit.isCompatible("millibar", unit)) {
                return AxisType.Pressure;
            }
            if (SimpleUnit.isCompatible("m", unit)) {
                return AxisType.Height;
            }
        }
        return AxisType.GeoZ;
    }
    
    @Override
    protected void makeCoordinateTransforms(final NetcdfDataset ds) {
        if (this.projCT != null) {
            final VarProcess vp = this.findVarProcess(this.projCT.getName());
            vp.isCoordinateTransform = true;
            vp.ct = this.projCT;
        }
        super.makeCoordinateTransforms(ds);
    }
    
    private ProjectionCT makeLCProjection(final NetcdfDataset ds, final String name, final int nx, final int ny) throws NoSuchElementException {
        final double centralLat = this.findAttributeDouble(ds, "centralLat");
        final double centralLon = this.findAttributeDouble(ds, "centralLon");
        final double rotation = this.findAttributeDouble(ds, "rotation");
        final LambertConformal proj = new LambertConformal(rotation, centralLon, centralLat, centralLat);
        final double lat0 = this.findAttributeDouble(ds, "lat00");
        final double lon0 = this.findAttributeDouble(ds, "lon00");
        final ProjectionPointImpl start = (ProjectionPointImpl)proj.latLonToProj(new LatLonPointImpl(lat0, lon0));
        this.startx = start.getX();
        this.starty = start.getY();
        final double latN = this.findAttributeDouble(ds, "latNxNy");
        final double lonN = this.findAttributeDouble(ds, "lonNxNy");
        final ProjectionPointImpl end = (ProjectionPointImpl)proj.latLonToProj(new LatLonPointImpl(latN, lonN));
        this.dx = (end.getX() - this.startx) / nx;
        this.dy = (end.getY() - this.starty) / ny;
        return new ProjectionCT(name, "FGDC", proj);
    }
    
    private ProjectionCT makeMercatorProjection(final NetcdfDataset ds, final String name, final int nx, final int ny) throws NoSuchElementException {
        final double centralLat = this.findAttributeDouble(ds, "centralLat");
        final double centralLon = this.findAttributeDouble(ds, "centralLon");
        final double latDxDy = this.findAttributeDouble(ds, "latDxDy");
        final double lonDxDy = this.findAttributeDouble(ds, "lonDxDy");
        final Mercator proj = new Mercator(lonDxDy, latDxDy);
        final double lat0 = this.findAttributeDouble(ds, "lat00");
        final double lon0 = this.findAttributeDouble(ds, "lon00");
        final ProjectionPointImpl start = (ProjectionPointImpl)proj.latLonToProj(new LatLonPointImpl(lat0, lon0));
        this.startx = start.getX();
        this.starty = start.getY();
        final double latN = this.findAttributeDouble(ds, "latNxNy");
        final double lonN = this.findAttributeDouble(ds, "lonNxNy");
        final ProjectionPointImpl end = (ProjectionPointImpl)proj.latLonToProj(new LatLonPointImpl(latN, lonN));
        this.dx = (end.getX() - this.startx) / nx;
        this.dy = (end.getY() - this.starty) / ny;
        return new ProjectionCT(name, "FGDC", proj);
    }
    
    private CoordinateAxis makeXCoordAxis(final NetcdfDataset ds, final int nx, final String xname) {
        final CoordinateAxis v = new CoordinateAxis1D(ds, null, xname, DataType.DOUBLE, xname, "km", "x on projection");
        ds.setValues(v, nx, this.startx, this.dx);
        this.parseInfo.format("Created X Coordinate Axis = ", new Object[0]);
        v.getNameAndDimensions(this.parseInfo, true, false);
        this.parseInfo.format("\n", new Object[0]);
        return v;
    }
    
    private CoordinateAxis makeYCoordAxis(final NetcdfDataset ds, final int ny, final String yname) {
        final CoordinateAxis v = new CoordinateAxis1D(ds, null, yname, DataType.DOUBLE, yname, "km", "y on projection");
        ds.setValues(v, ny, this.starty, this.dy);
        this.parseInfo.format("Created Y Coordinate Axis = ", new Object[0]);
        v.getNameAndDimensions(this.parseInfo, true, false);
        this.parseInfo.format("\n", new Object[0]);
        return v;
    }
    
    private double findAttributeDouble(final NetcdfDataset ds, final String attname) {
        final Attribute att = ds.findGlobalAttributeIgnoreCase(attname);
        return att.getNumericValue().doubleValue();
    }
}
