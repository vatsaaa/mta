// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.conv;

import ucar.nc2.Attribute;
import ucar.nc2.constants.AxisType;
import java.util.Iterator;
import ucar.nc2.Structure;
import java.io.IOException;
import ucar.nc2.Variable;
import java.util.List;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dataset.CoordSysBuilder;

public class EpicInsitu extends CoordSysBuilder
{
    public EpicInsitu() {
        this.conventionName = "EpicInsitu";
    }
    
    @Override
    public void augmentDataset(final NetcdfDataset ds, final CancelTask cancelTask) throws IOException {
        final List<Variable> vars = ds.getVariables();
        this.findAxes(vars);
        ds.finish();
    }
    
    private void findAxes(final List<Variable> vars) {
        for (final Variable v : vars) {
            this.checkIfAxis(v);
            if (v instanceof Structure) {
                final List<Variable> nested = ((Structure)v).getVariables();
                this.findAxes(nested);
            }
        }
    }
    
    private void checkIfAxis(final Variable v) {
        final Attribute att = v.findAttributeIgnoreCase("axis");
        if (att == null) {
            return;
        }
        final String axisType = att.getStringValue();
        if (axisType.equalsIgnoreCase("X")) {
            v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lon.toString()));
        }
        else if (axisType.equalsIgnoreCase("Y")) {
            v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lat.toString()));
        }
        else if (axisType.equalsIgnoreCase("Z")) {
            v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Height.toString()));
        }
        else if (axisType.equalsIgnoreCase("T")) {
            v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Time.toString()));
        }
    }
}
