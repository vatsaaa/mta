// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.conv;

import ucar.nc2.Attribute;
import ucar.nc2.constants.AxisType;
import java.io.IOException;
import java.util.Calendar;
import java.util.Iterator;
import ucar.ma2.Array;
import ucar.ma2.ArrayLong;
import ucar.nc2.Structure;
import ucar.nc2.Group;
import ucar.nc2.dataset.VariableDS;
import ucar.ma2.DataType;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.Variable;
import ucar.nc2.NetcdfFile;
import ucar.nc2.dataset.CoordSysBuilder;

public class ModisSatellite extends CoordSysBuilder
{
    public static boolean isMine(final NetcdfFile ncfile) {
        final String satName = ncfile.findAttValueIgnoreCase(null, "SATNAME", null);
        if (satName == null || !satName.equalsIgnoreCase("Aqua")) {
            return false;
        }
        final String instName = ncfile.findAttValueIgnoreCase(null, "INTRUMENT_NAME", null);
        return instName != null && instName.equalsIgnoreCase("modis");
    }
    
    public ModisSatellite() {
        this.conventionName = "ModisSatellite";
    }
    
    @Override
    public void augmentDataset(final NetcdfDataset ds, final CancelTask cancelTask) throws IOException {
        for (final Variable v : ds.getVariables()) {
            this.checkIfAxis(v);
        }
        final int year = ds.readAttributeInteger(null, "YEAR", -1);
        final int doy = ds.readAttributeInteger(null, "DAY", -1);
        double time = ds.readAttributeDouble(null, "TIME", Double.NaN);
        if (year > 0 && doy > 0 && !Double.isNaN(time)) {
            final Calendar cal = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
            cal.clear();
            cal.set(1, year);
            cal.set(6, doy);
            final int hour = (int)time;
            cal.set(11, hour);
            time -= hour;
            time *= 60.0;
            final int minute = (int)time;
            cal.set(12, minute);
            time -= minute;
            time *= 60.0;
            cal.set(13, (int)time);
            final VariableDS var = new VariableDS(ds, null, null, "timeFromAtts", DataType.LONG, "", "seconds since 1970-01-01 00:00", "time generated from global attributes");
            ds.addVariable(null, var);
            final ArrayLong.D0 data = new ArrayLong.D0();
            data.set(cal.getTime().getTime() / 1000L);
            var.setCachedData(data, true);
        }
        ds.finish();
    }
    
    private void checkIfAxis(final Variable v) {
        final String name = v.getName();
        if (name.equalsIgnoreCase("Longitude")) {
            v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lon.toString()));
        }
        else if (name.equalsIgnoreCase("Latitude")) {
            v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lat.toString()));
        }
    }
}
