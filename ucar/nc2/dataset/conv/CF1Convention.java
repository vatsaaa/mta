// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.conv;

import ucar.nc2.dataset.VariableEnhanced;
import ucar.ma2.IndexIterator;
import ucar.nc2.dataset.CoordinateAxis1D;
import ucar.ma2.DataType;
import ucar.ma2.Array;
import java.util.StringTokenizer;
import java.io.IOException;
import ucar.nc2.Group;
import java.util.Iterator;
import java.util.List;
import ucar.nc2.dataset.TransformType;
import ucar.nc2.Attribute;
import ucar.nc2.constants.AxisType;
import ucar.nc2.Variable;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.units.SimpleUnit;

public class CF1Convention extends CSMConvention
{
    private static String[] vertical_coords;
    private boolean avhrr_oiv2;
    
    public static String getZisPositive(final String zaxisName, final String vertCoordUnits) {
        if (vertCoordUnits == null) {
            return "up";
        }
        if (SimpleUnit.isCompatible("millibar", vertCoordUnits)) {
            return "down";
        }
        if (SimpleUnit.isCompatible("m", vertCoordUnits)) {
            return "up";
        }
        return "up";
    }
    
    public CF1Convention() {
        this.avhrr_oiv2 = false;
        this.conventionName = "CF-1.X";
    }
    
    @Override
    public void augmentDataset(final NetcdfDataset ds, final CancelTask cancelTask) throws IOException {
        boolean got_grid_mapping = false;
        final List<Variable> vars = ds.getVariables();
        for (final Variable v : vars) {
            String sname = ds.findAttValueIgnoreCase(v, "standard_name", null);
            if (sname != null) {
                sname = sname.trim();
                if (sname.equalsIgnoreCase("atmosphere_ln_pressure_coordinate")) {
                    this.makeAtmLnCoordinate(ds, v);
                    continue;
                }
                if (sname.equalsIgnoreCase("forecast_reference_time")) {
                    v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.RunTime.toString()));
                    continue;
                }
                if (sname.equalsIgnoreCase("ensemble")) {
                    v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Ensemble.toString()));
                    continue;
                }
                for (final String vertical_coord : CF1Convention.vertical_coords) {
                    if (sname.equalsIgnoreCase(vertical_coord)) {
                        v.addAttribute(new Attribute("_CoordinateTransformType", TransformType.Vertical.toString()));
                        if (v.findAttribute("_CoordinateAxes") == null) {
                            v.addAttribute(new Attribute("_CoordinateAxes", v.getName()));
                        }
                    }
                }
            }
            final String grid_mapping = ds.findAttValueIgnoreCase(v, "grid_mapping", null);
            if (grid_mapping != null) {
                Variable gridMap = ds.findVariable(grid_mapping);
                if (gridMap == null) {
                    final Group g = v.getParentGroup();
                    gridMap = g.findVariable(grid_mapping);
                }
                if (gridMap == null) {
                    continue;
                }
                gridMap.addAttribute(new Attribute("_CoordinateTransformType", TransformType.Projection.toString()));
                gridMap.addAttribute(new Attribute("_CoordinateAxisTypes", "GeoX GeoY"));
                got_grid_mapping = true;
            }
        }
        if (!got_grid_mapping) {
            for (final Variable v : ds.getVariables()) {
                final String grid_mapping_name = ds.findAttValueIgnoreCase(v, "grid_mapping_name", null);
                if (grid_mapping_name != null) {
                    v.addAttribute(new Attribute("_CoordinateTransformType", TransformType.Projection.toString()));
                    v.addAttribute(new Attribute("_CoordinateAxisTypes", "GeoX GeoY"));
                }
            }
        }
        final String src = ds.findAttValueIgnoreCase(null, "Source", "");
        if (src.equals("NOAA/National Climatic Data Center")) {
            final String title = ds.findAttValueIgnoreCase(null, "title", "");
            this.avhrr_oiv2 = (title.indexOf("OI-V2") > 0);
        }
        ds.finish();
    }
    
    private void makeAtmLnCoordinate(final NetcdfDataset ds, final Variable v) {
        final String formula = ds.findAttValueIgnoreCase(v, "formula_terms", null);
        if (null == formula) {
            final String msg = " Need attribute 'formula_terms' on Variable " + v.getName() + "\n";
            this.parseInfo.format(msg, new Object[0]);
            this.userAdvice.format(msg, new Object[0]);
            return;
        }
        Variable p0Var = null;
        Variable levelVar = null;
        final StringTokenizer stoke = new StringTokenizer(formula, " :");
        while (stoke.hasMoreTokens()) {
            final String toke = stoke.nextToken();
            if (toke.equalsIgnoreCase("p0")) {
                final String name = stoke.nextToken();
                p0Var = ds.findVariable(name);
            }
            else {
                if (!toke.equalsIgnoreCase("lev")) {
                    continue;
                }
                final String name = stoke.nextToken();
                levelVar = ds.findVariable(name);
            }
        }
        if (null == p0Var) {
            final String msg2 = " Need p0:varName on Variable " + v.getName() + " formula_terms\n";
            this.parseInfo.format(msg2, new Object[0]);
            this.userAdvice.format(msg2, new Object[0]);
            return;
        }
        if (null == levelVar) {
            final String msg2 = " Need lev:varName on Variable " + v.getName() + " formula_terms\n";
            this.parseInfo.format(msg2, new Object[0]);
            this.userAdvice.format(msg2, new Object[0]);
            return;
        }
        final String units = ds.findAttValueIgnoreCase(p0Var, "units", "hPa");
        try {
            final double p0 = p0Var.readScalarDouble();
            final Array levelData = levelVar.read();
            final Array pressureData = Array.factory(Double.TYPE, levelData.getShape());
            final IndexIterator ii = levelData.getIndexIterator();
            final IndexIterator iip = pressureData.getIndexIterator();
            while (ii.hasNext()) {
                final double val = p0 * Math.exp(-1.0 * ii.getDoubleNext());
                iip.setDoubleNext(val);
            }
            final CoordinateAxis1D p2 = new CoordinateAxis1D(ds, null, v.getShortName() + "_pressure", DataType.DOUBLE, levelVar.getDimensionsString(), units, "Vertical Pressure coordinate synthesized from atmosphere_ln_pressure_coordinate formula");
            p2.setCachedData(pressureData, false);
            p2.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Pressure.toString()));
            p2.addAttribute(new Attribute("_CoordinateAliasForDimension", p2.getDimensionsString()));
            ds.addVariable(null, p2);
            this.parseInfo.format(" added Vertical Pressure coordinate %s\n", p2.getName());
        }
        catch (IOException e) {
            final String msg3 = " Unable to read variables from " + v.getName() + " formula_terms\n";
            this.parseInfo.format(msg3, new Object[0]);
            this.userAdvice.format(msg3, new Object[0]);
        }
    }
    
    @Override
    protected AxisType getAxisType(final NetcdfDataset ncDataset, final VariableEnhanced v) {
        String sname = ncDataset.findAttValueIgnoreCase((Variable)v, "standard_name", null);
        if (sname != null) {
            sname = sname.trim();
            if (sname.equalsIgnoreCase("latitude")) {
                return AxisType.Lat;
            }
            if (sname.equalsIgnoreCase("longitude")) {
                return AxisType.Lon;
            }
            if (sname.equalsIgnoreCase("projection_x_coordinate") || sname.equalsIgnoreCase("grid_longitude")) {
                return AxisType.GeoX;
            }
            if (sname.equalsIgnoreCase("projection_y_coordinate") || sname.equalsIgnoreCase("grid_latitude")) {
                return AxisType.GeoY;
            }
            for (final String vertical_coord : CF1Convention.vertical_coords) {
                if (sname.equalsIgnoreCase(vertical_coord)) {
                    return AxisType.GeoZ;
                }
            }
        }
        final AxisType at = super.getAxisType(ncDataset, v);
        if (at == null) {
            String axis = ncDataset.findAttValueIgnoreCase((Variable)v, "axis", null);
            if (axis != null) {
                axis = axis.trim();
                final String unit = v.getUnitsString();
                if (axis.equalsIgnoreCase("X")) {
                    if (SimpleUnit.isCompatible("m", unit)) {
                        return AxisType.GeoX;
                    }
                }
                else if (axis.equalsIgnoreCase("Y")) {
                    if (SimpleUnit.isCompatible("m", unit)) {
                        return AxisType.GeoY;
                    }
                }
                else if (axis.equalsIgnoreCase("Z")) {
                    if (unit == null) {
                        return AxisType.GeoZ;
                    }
                    if (SimpleUnit.isCompatible("m", unit)) {
                        return AxisType.Height;
                    }
                    if (SimpleUnit.isCompatible("mbar", unit)) {
                        return AxisType.Pressure;
                    }
                    return AxisType.GeoZ;
                }
            }
        }
        if (at == null && this.avhrr_oiv2 && v.getShortName().equals("zlev")) {
            return AxisType.Height;
        }
        return at;
    }
    
    static {
        CF1Convention.vertical_coords = new String[] { "atmosphere_sigma_coordinate", "atmosphere_hybrid_sigma_pressure_coordinate", "atmosphere_hybrid_height_coordinate", "atmosphere_sleve_coordinate", "ocean_sigma_coordinate", "ocean_s_coordinate", "ocean_sigma_z_coordinate", "ocean_double_sigma_coordinate", "ocean_s_coordinate_g1", "ocean_s_coordinate_g2" };
    }
}
