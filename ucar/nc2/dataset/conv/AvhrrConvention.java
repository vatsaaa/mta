// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.conv;

import java.util.Date;
import java.util.Iterator;
import java.text.ParseException;
import java.io.IOException;
import ucar.ma2.Array;
import ucar.ma2.ArrayLong;
import ucar.nc2.units.DateFormatter;
import ucar.nc2.Structure;
import ucar.nc2.dataset.VariableDS;
import ucar.ma2.DataType;
import java.util.TimeZone;
import java.text.SimpleDateFormat;
import ucar.nc2.Variable;
import ucar.nc2.Dimension;
import ucar.nc2.constants.AxisType;
import ucar.nc2.Attribute;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.Group;
import ucar.nc2.NetcdfFile;
import ucar.nc2.dataset.CoordSysBuilder;

public class AvhrrConvention extends CoordSysBuilder
{
    public static boolean isMine(final NetcdfFile ncfile) {
        if (!ncfile.getFileTypeId().equals("HDF5")) {
            return false;
        }
        final Group loc = ncfile.findGroup("VHRR/Geo-Location");
        return null != loc && null != loc.findVariable("Latitude") && null != loc.findVariable("Longitude") && null != ncfile.findGroup("VHRR/Image Data");
    }
    
    public AvhrrConvention() {
        this.conventionName = "AvhrrSatellite";
    }
    
    @Override
    public void augmentDataset(final NetcdfDataset ds, final CancelTask cancelTask) throws IOException {
        ds.addAttribute(null, new Attribute("FeatureType", FeatureType.IMAGE.toString()));
        final Group vhrr = ds.findGroup("VHRR");
        final Group loc = vhrr.findGroup("Geo-Location");
        final Variable lat = loc.findVariable("Latitude");
        lat.addAttribute(new Attribute("units", "degrees_north"));
        lat.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lat.toString()));
        final Variable lon = loc.findVariable("Longitude");
        lon.addAttribute(new Attribute("units", "degrees_east"));
        lon.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lon.toString()));
        final int[] shape = lat.getShape();
        assert shape.length == 2;
        final Dimension scan = new Dimension("scan", shape[0]);
        final Dimension xscan = new Dimension("xscan", shape[1]);
        vhrr.addDimension(scan);
        vhrr.addDimension(xscan);
        lat.setDimensions("scan xscan");
        lon.setDimensions("scan xscan");
        final Group data = vhrr.findGroup("Image Data");
        for (final Variable v : data.getVariables()) {
            final int[] vs = v.getShape();
            if (vs.length == 2 && vs[0] == shape[0] && vs[1] == shape[1]) {
                v.setDimensions("scan xscan");
                v.addAttribute(new Attribute("_CoordinateAxes", "lat lon time"));
            }
        }
        final Group info = ds.findGroup("PRODUCT_METADATA/PRODUCT_DETAILS");
        final String dateS = info.findAttribute("ACQUISITION_DATE").getStringValue();
        final String timeS = info.findAttribute("ACQUISITION_TIME_IN_GMT").getStringValue();
        final SimpleDateFormat format = new SimpleDateFormat("ddMMMyyyyHHmm");
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        try {
            final Date d = format.parse(dateS + timeS);
            final VariableDS time = new VariableDS(ds, vhrr, null, "time", DataType.LONG, "", "seconds since 1970-01-01 00:00", "time generated from PRODUCT_METADATA/PRODUCT_DETAILS");
            time.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Time.toString()));
            time.addAttribute(new Attribute("IsoDate", new DateFormatter().toDateTimeStringISO(d)));
            ds.addVariable(vhrr, time);
            final ArrayLong.D0 timeData = new ArrayLong.D0();
            timeData.set(d.getTime() / 1000L);
            time.setCachedData(timeData, true);
        }
        catch (ParseException e) {
            e.printStackTrace();
            throw new IOException(e.getMessage());
        }
        ds.finish();
    }
}
