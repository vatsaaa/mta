// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.conv;

import ucar.nc2.dataset.VariableEnhanced;
import ucar.unidata.geoloc.projection.UtmProjection;
import ucar.unidata.geoloc.projection.LambertAzimuthalEqualArea;
import ucar.unidata.geoloc.projection.AlbersEqualArea;
import ucar.unidata.geoloc.projection.TransverseMercator;
import ucar.unidata.geoloc.projection.Mercator;
import ucar.unidata.geoloc.projection.Stereographic;
import ucar.unidata.geoloc.projection.LambertConformal;
import ucar.unidata.geoloc.ProjectionImpl;
import ucar.nc2.dataset.ProjectionCT;
import ucar.unidata.geoloc.projection.LatLonProjection;
import ucar.unidata.geoloc.ProjectionRect;
import java.util.Calendar;
import ucar.ma2.ArrayInt;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import java.util.GregorianCalendar;
import java.util.SimpleTimeZone;
import ucar.nc2.constants.AxisType;
import ucar.ma2.Array;
import ucar.ma2.ArrayDouble;
import ucar.nc2.dataset.CoordinateAxis1D;
import ucar.ma2.DataType;
import ucar.nc2.dataset.CoordinateAxis;
import ucar.nc2.Dimension;
import ucar.nc2.Attribute;
import ucar.nc2.Variable;
import ucar.nc2.Group;
import ucar.nc2.dataset.VariableDS;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.NetcdfFile;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.nc2.dataset.CoordSysBuilder;

public class M3IOConvention extends CoordSysBuilder
{
    private CoordinateTransform ct;
    
    public static boolean isMine(final NetcdfFile ncfile) {
        return null != ncfile.findGlobalAttribute("XORIG") && null != ncfile.findGlobalAttribute("YORIG") && null != ncfile.findGlobalAttribute("XCELL") && null != ncfile.findGlobalAttribute("YCELL") && null != ncfile.findGlobalAttribute("NCOLS") && null != ncfile.findGlobalAttribute("NROWS");
    }
    
    public M3IOConvention() {
        this.ct = null;
        this.conventionName = "M3IO";
    }
    
    @Override
    public void augmentDataset(final NetcdfDataset ncd, final CancelTask cancelTask) {
        if (null != ncd.findVariable("x")) {
            return;
        }
        if (null != ncd.findVariable("lon")) {
            return;
        }
        this.constructCoordAxes(ncd);
        ncd.finish();
    }
    
    protected void constructCoordAxes(final NetcdfDataset ds) {
        final Dimension dimx = ds.findDimension("COL");
        final int nx = dimx.getLength();
        final Dimension dimy = ds.findDimension("ROW");
        final int ny = dimy.getLength();
        final int projType = this.findAttributeInt(ds, "GDTYP");
        final boolean isLatLon = projType == 1;
        if (isLatLon) {
            ds.addCoordinateAxis(this.makeCoordLLAxis(ds, "lon", "COL", nx, "XORIG", "XCELL", "degrees east"));
            ds.addCoordinateAxis(this.makeCoordLLAxis(ds, "lat", "ROW", ny, "YORIG", "YCELL", "degrees north"));
            this.ct = this.makeLatLongProjection(ds);
            final VariableDS v = this.makeCoordinateTransformVariable(ds, this.ct);
            ds.addVariable(null, v);
            v.addAttribute(new Attribute("_CoordinateAxes", "lon lat"));
        }
        else {
            ds.addCoordinateAxis(this.makeCoordAxis(ds, "x", "COL", nx, "XORIG", "XCELL", "km"));
            ds.addCoordinateAxis(this.makeCoordAxis(ds, "y", "ROW", ny, "YORIG", "YCELL", "km"));
            if (projType == 2) {
                this.ct = this.makeLCProjection(ds);
            }
            else if (projType == 3) {
                this.ct = this.makeTMProjection(ds);
            }
            else if (projType == 4) {
                this.ct = this.makeSTProjection(ds);
            }
            else if (projType == 5) {
                this.ct = this.makeUTMProjection(ds);
            }
            else if (projType == 6) {
                this.ct = this.makePolarStereographicProjection(ds);
            }
            else if (projType == 7) {
                this.ct = this.makeEquitorialMercatorProjection(ds);
            }
            else if (projType == 8) {
                this.ct = this.makeTransverseMercatorProjection(ds);
            }
            else if (projType == 9) {
                this.ct = this.makeAlbersProjection(ds);
            }
            else if (projType == 10) {
                this.ct = this.makeLambertAzimuthalProjection(ds);
            }
            if (this.ct != null) {
                final VariableDS v = this.makeCoordinateTransformVariable(ds, this.ct);
                ds.addVariable(null, v);
                v.addAttribute(new Attribute("_CoordinateAxes", "x y"));
            }
        }
        this.makeZCoordAxis(ds, "LAY", "VGLVLS", "sigma");
        this.makeTimeCoordAxis(ds, "TSTEP");
    }
    
    private CoordinateAxis makeCoordAxis(final NetcdfDataset ds, final String name, final String dimName, final int n, final String startName, final String incrName, final String unitName) {
        final double start = 0.001 * this.findAttributeDouble(ds, startName);
        final double incr = 0.001 * this.findAttributeDouble(ds, incrName);
        final CoordinateAxis v = new CoordinateAxis1D(ds, null, name, DataType.DOUBLE, dimName, unitName, "synthesized coordinate from " + startName + " " + incrName + " global attributes");
        ds.setValues(v, n, start, incr);
        return v;
    }
    
    private CoordinateAxis makeCoordLLAxis(final NetcdfDataset ds, final String name, final String dimName, final int n, final String startName, final String incrName, final String unitName) {
        final double start = this.findAttributeDouble(ds, startName);
        final double incr = this.findAttributeDouble(ds, incrName);
        final CoordinateAxis v = new CoordinateAxis1D(ds, null, name, DataType.DOUBLE, dimName, unitName, "synthesized coordinate from " + startName + " " + incrName + " global attributes");
        ds.setValues(v, n, start, incr);
        return v;
    }
    
    private void makeZCoordAxis(final NetcdfDataset ds, final String dimName, final String levelsName, final String unitName) {
        final Dimension dimz = ds.findDimension(dimName);
        final int nz = dimz.getLength();
        final ArrayDouble.D1 dataLev = new ArrayDouble.D1(nz);
        final ArrayDouble.D1 dataLayers = new ArrayDouble.D1(nz + 1);
        final Attribute layers = ds.findGlobalAttribute("VGLVLS");
        for (int i = 0; i <= nz; ++i) {
            dataLayers.set(i, layers.getNumericValue(i).doubleValue());
        }
        for (int i = 0; i < nz; ++i) {
            final double midpoint = (dataLayers.get(i) + dataLayers.get(i + 1)) / 2.0;
            dataLev.set(i, midpoint);
        }
        final CoordinateAxis v = new CoordinateAxis1D(ds, null, "level", DataType.DOUBLE, dimName, unitName, "synthesized coordinate from " + levelsName + " global attributes");
        v.setCachedData(dataLev, true);
        v.addAttribute(new Attribute("positive", "down"));
        v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.GeoZ.toString()));
        final String edge_name = "layer";
        final Dimension lay_edge = new Dimension(edge_name, nz + 1);
        ds.addDimension(null, lay_edge);
        final CoordinateAxis vedge = new CoordinateAxis1D(ds, null, edge_name, DataType.DOUBLE, edge_name, unitName, "synthesized coordinate from " + levelsName + " global attributes");
        vedge.setCachedData(dataLayers, true);
        v.setBoundaryRef(edge_name);
        ds.addCoordinateAxis(v);
        ds.addCoordinateAxis(vedge);
    }
    
    private void makeTimeCoordAxis(final NetcdfDataset ds, final String timeName) {
        final int start_date = this.findAttributeInt(ds, "SDATE");
        int start_time = this.findAttributeInt(ds, "STIME");
        int time_step = this.findAttributeInt(ds, "TSTEP");
        final int year = start_date / 1000;
        final int doy = start_date % 1000;
        int hour = start_time / 10000;
        start_time %= 10000;
        int min = start_time / 100;
        int sec = start_time % 100;
        final Calendar cal = new GregorianCalendar(new SimpleTimeZone(0, "GMT"));
        cal.clear();
        cal.set(1, year);
        cal.set(6, doy);
        cal.set(11, hour);
        cal.set(12, min);
        cal.set(13, sec);
        final SimpleDateFormat dateFormatOut = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormatOut.setTimeZone(TimeZone.getTimeZone("GMT"));
        final String units = "seconds since " + dateFormatOut.format(cal.getTime()) + " UTC";
        hour = time_step / 10000;
        time_step %= 10000;
        min = time_step / 100;
        sec = time_step % 100;
        time_step = hour * 3600 + min * 60 + sec;
        final Dimension dimt = ds.findDimension(timeName);
        final int nt = dimt.getLength();
        final ArrayInt.D1 data = new ArrayInt.D1(nt);
        for (int i = 0; i < nt; ++i) {
            data.set(i, i * time_step);
        }
        final CoordinateAxis1D timeCoord = new CoordinateAxis1D(ds, null, "time", DataType.INT, timeName, units, "synthesized time coordinate from SDATE, STIME, STEP global attributes");
        timeCoord.setCachedData(data, true);
        timeCoord.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Time.toString()));
        ds.addCoordinateAxis(timeCoord);
    }
    
    private CoordinateTransform makeLatLongProjection(final NetcdfDataset ds) {
        final double x1 = this.findAttributeDouble(ds, "XORIG");
        final double y1 = this.findAttributeDouble(ds, "YORIG");
        final double x2 = x1 + this.findAttributeDouble(ds, "XCELL") * this.findAttributeDouble(ds, "NCOLS");
        final double y2 = y1 + this.findAttributeDouble(ds, "YCELL") * this.findAttributeDouble(ds, "NROWS");
        final LatLonProjection ll = new LatLonProjection("LatitudeLongitudeProjection", new ProjectionRect(x1, y1, x2, y2));
        return new ProjectionCT("LatitudeLongitudeProjection", "FGDC", ll);
    }
    
    private CoordinateTransform makeLCProjection(final NetcdfDataset ds) {
        final double par1 = this.findAttributeDouble(ds, "P_ALP");
        final double par2 = this.findAttributeDouble(ds, "P_BET");
        final double lon0 = this.findAttributeDouble(ds, "XCENT");
        final double lat0 = this.findAttributeDouble(ds, "YCENT");
        final LambertConformal lc = new LambertConformal(lat0, lon0, par1, par2);
        return new ProjectionCT("LambertConformalProjection", "FGDC", lc);
    }
    
    private CoordinateTransform makePolarStereographicProjection(final NetcdfDataset ds) {
        final boolean n_polar = this.findAttributeDouble(ds, "P_ALP") == 1.0;
        final double centeral_meridian = this.findAttributeDouble(ds, "P_GAM");
        final double lon0 = this.findAttributeDouble(ds, "XCENT");
        final double lat0 = this.findAttributeDouble(ds, "YCENT");
        final double latts = this.findAttributeDouble(ds, "P_BET");
        final Stereographic sg = new Stereographic(latts, lat0, lon0, n_polar);
        sg.setCentralMeridian(centeral_meridian);
        final CoordinateTransform ct = new ProjectionCT("PolarStereographic", "FGDC", sg);
        return ct;
    }
    
    private CoordinateTransform makeEquitorialMercatorProjection(final NetcdfDataset ds) {
        final double lon0 = this.findAttributeDouble(ds, "XCENT");
        final double par = this.findAttributeDouble(ds, "P_ALP");
        final Mercator p = new Mercator(lon0, par);
        final CoordinateTransform ct = new ProjectionCT("EquitorialMercator", "FGDC", p);
        return ct;
    }
    
    private CoordinateTransform makeTransverseMercatorProjection(final NetcdfDataset ds) {
        final double lat0 = this.findAttributeDouble(ds, "P_ALP");
        final double tangentLon = this.findAttributeDouble(ds, "P_GAM");
        final TransverseMercator p = new TransverseMercator(lat0, tangentLon, 1.0);
        final CoordinateTransform ct = new ProjectionCT("TransverseMercator", "FGDC", p);
        return ct;
    }
    
    private CoordinateTransform makeAlbersProjection(final NetcdfDataset ds) {
        final double lat0 = this.findAttributeDouble(ds, "YCENT");
        final double lon0 = this.findAttributeDouble(ds, "XCENT");
        final double par1 = this.findAttributeDouble(ds, "P_ALP");
        final double par2 = this.findAttributeDouble(ds, "P_BET");
        final AlbersEqualArea p = new AlbersEqualArea(lat0, lon0, par1, par2);
        final CoordinateTransform ct = new ProjectionCT("Albers", "FGDC", p);
        return ct;
    }
    
    private CoordinateTransform makeLambertAzimuthalProjection(final NetcdfDataset ds) {
        final double lat0 = this.findAttributeDouble(ds, "YCENT");
        final double lon0 = this.findAttributeDouble(ds, "XCENT");
        final LambertAzimuthalEqualArea p = new LambertAzimuthalEqualArea(lat0, lon0, 0.0, 0.0, 6370000.0);
        final CoordinateTransform ct = new ProjectionCT("LambertAzimuthal", "FGDC", p);
        return ct;
    }
    
    private CoordinateTransform makeSTProjection(final NetcdfDataset ds) {
        double latt = this.findAttributeDouble(ds, "PROJ_ALPHA");
        if (Double.isNaN(latt)) {
            latt = this.findAttributeDouble(ds, "P_ALP");
        }
        double lont = this.findAttributeDouble(ds, "PROJ_BETA");
        if (Double.isNaN(lont)) {
            lont = this.findAttributeDouble(ds, "P_BET");
        }
        final Stereographic st = new Stereographic(latt, lont, 1.0);
        return new ProjectionCT("StereographicProjection", "FGDC", st);
    }
    
    private CoordinateTransform makeTMProjection(final NetcdfDataset ds) {
        double lat0 = this.findAttributeDouble(ds, "PROJ_ALPHA");
        if (Double.isNaN(lat0)) {
            lat0 = this.findAttributeDouble(ds, "P_ALP");
        }
        double tangentLon = this.findAttributeDouble(ds, "PROJ_BETA");
        if (Double.isNaN(tangentLon)) {
            tangentLon = this.findAttributeDouble(ds, "P_BET");
        }
        final TransverseMercator tm = new TransverseMercator(lat0, tangentLon, 1.0);
        return new ProjectionCT("MercatorProjection", "FGDC", tm);
    }
    
    private CoordinateTransform makeUTMProjection(final NetcdfDataset ds) {
        final int zone = (int)this.findAttributeDouble(ds, "P_ALP");
        final double ycent = this.findAttributeDouble(ds, "YCENT");
        boolean isNorth = true;
        if (ycent < 0.0) {
            isNorth = false;
        }
        final UtmProjection utm = new UtmProjection(zone, isNorth);
        final CoordinateTransform ct = new ProjectionCT("UTM", "EPSG", utm);
        return ct;
    }
    
    @Override
    protected AxisType getAxisType(final NetcdfDataset ds, final VariableEnhanced ve) {
        final Variable v = (Variable)ve;
        final String vname = v.getName();
        if (vname.equalsIgnoreCase("x")) {
            return AxisType.GeoX;
        }
        if (vname.equalsIgnoreCase("y")) {
            return AxisType.GeoY;
        }
        if (vname.equalsIgnoreCase("lat")) {
            return AxisType.Lat;
        }
        if (vname.equalsIgnoreCase("lon")) {
            return AxisType.Lon;
        }
        if (vname.equalsIgnoreCase("time")) {
            return AxisType.Time;
        }
        if (vname.equalsIgnoreCase("level")) {
            return AxisType.GeoZ;
        }
        return null;
    }
    
    @Override
    protected void makeCoordinateTransforms(final NetcdfDataset ds) {
        if (this.ct != null) {
            final VarProcess vp = this.findVarProcess(this.ct.getName());
            if (vp != null) {
                vp.ct = this.ct;
            }
        }
        super.makeCoordinateTransforms(ds);
    }
    
    private double findAttributeDouble(final NetcdfDataset ds, final String attname) {
        final Attribute att = ds.findGlobalAttributeIgnoreCase(attname);
        if (att == null || att.isString()) {
            return Double.NaN;
        }
        return att.getNumericValue().doubleValue();
    }
    
    private int findAttributeInt(final NetcdfDataset ds, final String attname) {
        final Attribute att = ds.findGlobalAttributeIgnoreCase(attname);
        return att.getNumericValue().intValue();
    }
}
