// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.conv;

import java.io.IOException;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dataset.CoordSysBuilder;

public class BUFRConvention extends CoordSysBuilder
{
    public BUFRConvention() {
        this.conventionName = "BUFR/CDM";
    }
    
    @Override
    public void augmentDataset(final NetcdfDataset ds, final CancelTask cancelTask) throws IOException {
        if (null != ds.findVariable("x")) {
            return;
        }
    }
}
