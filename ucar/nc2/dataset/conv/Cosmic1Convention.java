// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.conv;

import ucar.nc2.dataset.VariableEnhanced;
import java.util.GregorianCalendar;
import ucar.unidata.util.DateUtil;
import visad.DateTime;
import java.io.IOException;
import ucar.ma2.IndexIterator;
import ucar.nc2.Dimension;
import ucar.ma2.ArrayFloat;
import ucar.ma2.Array;
import ucar.ma2.ArrayDouble;
import ucar.nc2.constants.AxisType;
import ucar.nc2.Attribute;
import ucar.nc2.Structure;
import ucar.nc2.Group;
import ucar.nc2.dataset.VariableDS;
import ucar.ma2.DataType;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.Variable;
import ucar.nc2.NetcdfFile;
import ucar.nc2.dataset.CoordSysBuilder;

public class Cosmic1Convention extends CoordSysBuilder
{
    protected static final double RTD = 57.29577951308232;
    protected static final double DTR = 0.017453292519943295;
    
    public static boolean isMine(final NetcdfFile ncfile) {
        if (null == ncfile.findDimension("MSL_alt") && null == ncfile.findDimension("time")) {
            return false;
        }
        final String center = ncfile.findAttValueIgnoreCase(null, "center", null);
        return center != null && center.equals("UCAR/CDAAC");
    }
    
    public Cosmic1Convention() {
        this.conventionName = "Cosmic1";
    }
    
    @Override
    public void augmentDataset(final NetcdfDataset ds, final CancelTask cancelTask) throws IOException {
        final Attribute leoAtt = ds.findGlobalAttribute("leoId");
        if (leoAtt == null) {
            if (ds.findVariable("time") == null) {
                double start = ds.readAttributeDouble(null, "start_time", Double.NaN);
                double stop = ds.readAttributeDouble(null, "stop_time", Double.NaN);
                if (Double.isNaN(start) && Double.isNaN(stop)) {
                    final double top = ds.readAttributeDouble(null, "toptime", Double.NaN);
                    final double bot = ds.readAttributeDouble(null, "bottime", Double.NaN);
                    this.conventionName = "Cosmic2";
                    if (top > bot) {
                        stop = top;
                        start = bot;
                    }
                    else {
                        stop = bot;
                        start = top;
                    }
                }
                final Dimension dim = ds.findDimension("MSL_alt");
                final Variable dimV = ds.findVariable("MSL_alt");
                final Array dimU = dimV.read();
                final int inscr = (dimU.getFloat(1) - dimU.getFloat(0) > 0.0f) ? 1 : 0;
                final int n = dim.getLength();
                final double incr = (stop - start) / n;
                final String timeUnits = "seconds since 1980-01-06 00:00:00";
                final Variable timeVar = new VariableDS(ds, null, null, "time", DataType.DOUBLE, dim.getName(), timeUnits, null);
                ds.addVariable(null, timeVar);
                timeVar.addAttribute(new Attribute("units", timeUnits));
                timeVar.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Time.toString()));
                final int dir = ds.readAttributeInteger(null, "irs", 1);
                final ArrayDouble.D1 data = (ArrayDouble.D1)Array.factory(DataType.DOUBLE, new int[] { n });
                if (inscr == 0) {
                    if (dir == 1) {
                        for (int i = 0; i < n; ++i) {
                            data.set(i, start + i * incr);
                        }
                    }
                    else {
                        for (int i = 0; i < n; ++i) {
                            data.set(i, stop - i * incr);
                        }
                    }
                }
                else {
                    for (int i = 0; i < n; ++i) {
                        data.set(i, stop - i * incr);
                    }
                }
                timeVar.setCachedData(data, false);
            }
            Variable v = ds.findVariable("Lat");
            if (v == null) {
                v = ds.findVariable("GEO_lat");
            }
            v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lat.toString()));
            Variable v2 = ds.findVariable("Lon");
            if (v2 == null) {
                v2 = ds.findVariable("GEO_lon");
            }
            v2.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lon.toString()));
        }
        else {
            final Dimension dim2 = ds.findDimension("time");
            final int n2 = dim2.getLength();
            final Variable latVar = new VariableDS(ds, null, null, "Lat", DataType.FLOAT, dim2.getName(), "degree", null);
            latVar.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lat.toString()));
            ds.addVariable(null, latVar);
            final Variable lonVar = new VariableDS(ds, null, null, "Lon", DataType.FLOAT, dim2.getName(), "degree", null);
            lonVar.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lon.toString()));
            ds.addVariable(null, lonVar);
            final Variable altVar = new VariableDS(ds, null, null, "MSL_alt", DataType.FLOAT, dim2.getName(), "meter", null);
            altVar.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Height.toString()));
            ds.addVariable(null, altVar);
            final ArrayFloat.D1 latData = (ArrayFloat.D1)Array.factory(DataType.FLOAT, new int[] { n2 });
            final ArrayFloat.D1 lonData = (ArrayFloat.D1)Array.factory(DataType.FLOAT, new int[] { n2 });
            final ArrayFloat.D1 altData = (ArrayFloat.D1)Array.factory(DataType.FLOAT, new int[] { n2 });
            final ArrayDouble.D1 timeData = (ArrayDouble.D1)Array.factory(DataType.DOUBLE, new int[] { n2 });
            this.conventionName = "Cosmic3";
            final int iyr = ds.readAttributeInteger(null, "year", 2009);
            final int mon = ds.readAttributeInteger(null, "month", 0);
            final int iday = ds.readAttributeInteger(null, "day", 0);
            final int ihr = ds.readAttributeInteger(null, "hour", 0);
            final int min = ds.readAttributeInteger(null, "minute", 0);
            final int sec = ds.readAttributeInteger(null, "second", 0);
            final double start2 = ds.readAttributeDouble(null, "startTime", Double.NaN);
            final double stop2 = ds.readAttributeDouble(null, "stopTime", Double.NaN);
            final double incr2 = (stop2 - start2) / n2;
            final int t = 0;
            final double dtheta = this.gast(iyr, mon, iday, ihr, min, sec, t);
            final Variable tVar = ds.findVariable("time");
            final String timeUnits2 = "seconds since 1980-01-06 00:00:00";
            tVar.removeAttributeIgnoreCase("valid_range");
            tVar.removeAttributeIgnoreCase("units");
            tVar.addAttribute(new Attribute("units", timeUnits2));
            tVar.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Time.toString()));
            Variable v3 = ds.findVariable("xLeo");
            final Array xLeo = v3.read();
            v3 = ds.findVariable("yLeo");
            final Array yLeo = v3.read();
            v3 = ds.findVariable("zLeo");
            final Array zLeo = v3.read();
            final Array tArray = tVar.read();
            final double pi = 3.1415926;
            final double a = 6378.137;
            final double b = 6356.7523142;
            final IndexIterator iiter0 = xLeo.getIndexIterator();
            final IndexIterator iiter2 = yLeo.getIndexIterator();
            final IndexIterator iiter3 = zLeo.getIndexIterator();
            int j = 0;
            while (iiter0.hasNext()) {
                final double[] v_inertial = { iiter0.getDoubleNext(), iiter2.getDoubleNext(), iiter3.getDoubleNext() };
                final double[] uvz = { 0.0, 0.0, 1.0 };
                final double[] v_ecf = this.spin(v_inertial, uvz, -1.0 * dtheta);
                final double[] llh = this.xyzell(a, b, v_ecf);
                final double llt = tArray.getDouble(j);
                latData.set(j, (float)llh[0]);
                lonData.set(j, (float)llh[1]);
                altData.set(j, (float)llh[2]);
                timeData.set(j, start2 + j * incr2);
                ++j;
            }
            latVar.setCachedData(latData, false);
            lonVar.setCachedData(lonData, false);
            altVar.setCachedData(altData, false);
            tVar.setCachedData(timeData, false);
        }
        ds.finish();
    }
    
    private DateTime getDateTime(final int year, final int month, final int day, final int hour, final int min, final int sec) throws Exception {
        final GregorianCalendar convertCal = new GregorianCalendar(DateUtil.TIMEZONE_GMT);
        convertCal.clear();
        convertCal.set(1, year);
        convertCal.set(2, month - 1);
        convertCal.set(5, day);
        convertCal.set(11, hour);
        convertCal.set(12, min);
        convertCal.set(13, sec);
        return new DateTime(convertCal.getTime());
    }
    
    @Override
    protected AxisType getAxisType(final NetcdfDataset ncDataset, final VariableEnhanced v) {
        final String name = v.getShortName();
        if (name.equals("time")) {
            return AxisType.Time;
        }
        if (name.equals("Lat") || name.equals("GEO_lat")) {
            return AxisType.Lat;
        }
        if (name.equals("Lon") || name.equals("GEO_lon")) {
            return AxisType.Lon;
        }
        if (name.equals("MSL_alt")) {
            return AxisType.Height;
        }
        return null;
    }
    
    public double[] xyzell(final double a, final double b, final double[] xstat) {
        final double[] dxell = new double[3];
        final double[] xstell = new double[3];
        final double[] xp = new double[3];
        final double e2 = (a * a - b * b) / (a * a);
        final double s = Math.sqrt(xstat[0] * xstat[0] + xstat[1] * xstat[1]);
        double rlam = Math.atan2(xstat[1], xstat[0]);
        final double zps = xstat[2] / s;
        double h = Math.sqrt(xstat[0] * xstat[0] + xstat[1] * xstat[1] + xstat[2] * xstat[2]) - a;
        double phi = Math.atan(zps / (1.0 - e2 * a / (a + h)));
        int niter = 0;
        for (int i = 1; i <= 10000000; ++i) {
            final double n = a / Math.sqrt(1.0 - e2 * Math.sin(phi) * Math.sin(phi));
            final double hp = h;
            final double phip = phi;
            h = s / Math.cos(phi) - n;
            phi = Math.atan(zps / (1.0 - e2 * n / (n + h)));
            ++niter;
            if (Math.abs(phip - phi) <= 1.0E-11 && Math.abs(hp - h) <= 1.0E-5) {
                break;
            }
            if (niter >= 10) {
                phi = -999.0;
                rlam = -999.0;
                h = -999.0;
                break;
            }
        }
        xstell[0] = phi * 180.0 / 3.1415926;
        xstell[1] = rlam * 180.0 / 3.1415926;
        xstell[2] = h;
        return xstell;
    }
    
    public double gast(final int iyr, final int imon, final int iday, final int ihr, final int imin, final double sec, final double dsec) {
        final double djd = this.juday(imon, iday, iyr);
        final double tu = (djd - 2451545.0) / 36525.0;
        final double gmst = 24110.54841 + 8640184.812866 * tu + 0.093104 * tu * tu - 6.2E-6 * Math.pow(tu, 3.0);
        final double utco = ihr * 3600 + imin * 60 + sec;
        final double theta = this.togreenw(dsec, utco, gmst);
        return theta;
    }
    
    public double juday(final int M, final int D, final int Y) {
        final double IY = Y - (12 - M) / 10;
        final double IM = M + 1 + 12 * ((12 - M) / 10);
        final double I = IY / 100.0;
        final double J = 2.0 - I + I / 4.0 + Math.round(365.25 * IY) + Math.round(30.6001 * IM);
        final double JD = J + D + 1720994.5;
        return JD;
    }
    
    public double togreenw(final double rectt, final double utco, double gmst) {
        final double pi = Math.acos(-1.0);
        final double utc = (utco + rectt) * 1.0027379093;
        for (gmst += utc; gmst < 0.0; gmst += 86400.0) {}
        while (gmst > 86400.0) {
            gmst -= 86400.0;
        }
        final double theta = gmst * 2.0 * pi / 86400.0;
        return theta;
    }
    
    public double[] spin(final double[] v1, final double[] vs, final double a) {
        final double[] v2 = new double[3];
        final double[] vsn = new double[3];
        final double[] v3 = new double[3];
        final double vsabs = Math.sqrt(vs[0] * vs[0] + vs[1] * vs[1] + vs[2] * vs[2]);
        for (int i = 0; i < 3; ++i) {
            vsn[i] = vs[i] / vsabs;
        }
        final double a2 = Math.cos(a);
        final double a3 = 1.0 - a2;
        final double a4 = Math.sin(a);
        final double[][] s = new double[3][3];
        s[0][0] = a3 * vsn[0] * vsn[0] + a2;
        s[0][1] = a3 * vsn[0] * vsn[1] - a4 * vsn[2];
        s[0][2] = a3 * vsn[0] * vsn[2] + a4 * vsn[1];
        s[1][0] = a3 * vsn[1] * vsn[0] + a4 * vsn[2];
        s[1][1] = a3 * vsn[1] * vsn[1] + a2;
        s[1][2] = a3 * vsn[1] * vsn[2] - a4 * vsn[0];
        s[2][0] = a3 * vsn[2] * vsn[0] - a4 * vsn[1];
        s[2][1] = a3 * vsn[2] * vsn[1] + a4 * vsn[0];
        s[2][2] = a3 * vsn[2] * vsn[2] + a2;
        for (int j = 0; j < 3; ++j) {
            v3[j] = s[j][0] * v1[0] + s[j][1] * v1[1] + s[j][2] * v1[2];
        }
        for (int j = 0; j < 3; ++j) {
            v2[j] = v3[j];
        }
        return v2;
    }
    
    public double[] execute(final double[] eci, final double julian) {
        final double Xi = eci[0];
        final double Yi = eci[1];
        final double Zi = eci[2];
        final double[] ecef = new double[3];
        final double tday = (int)(julian / 86400.0);
        final double tsec = julian - tday * 86400.0;
        final double t = tday - 10957.5;
        final double tfrac = tsec / 86400.0;
        final double dat = t;
        double d__1;
        final double tu = d__1 = dat / 36525.0;
        final double d__3;
        final double d__2 = d__3 = tu;
        double gmst = tu * 8640184.812866 + 24110.54841 + d__1 * d__1 * 0.093104 - d__3 * (d__2 * d__2) * 6.2E-6;
        d__1 = tu;
        final double omega = tu * 5.098097E-6 + 86636.55536790872 - d__1 * d__1 * 5.09E-10;
        final double da = 0.0;
        gmst = gmst + omega * tfrac + da * 57.29577951308232 * 86400.0 / 360.0;
        gmst %= 86400.0;
        if (gmst < 0.0) {
            gmst += 86400.0;
        }
        gmst = gmst / 86400.0 * 360.0;
        final double GHA;
        gmst = (GHA = gmst * 0.017453292519943295);
        final double c = Math.cos(GHA);
        final double s = Math.sin(GHA);
        final double X = c * Xi + s * Yi;
        final double Y = -s * Xi + c * Yi;
        ecef[0] = X;
        ecef[1] = Y;
        ecef[2] = Zi;
        return ecef;
    }
    
    public static final double[] ECFtoLLA(final double x, final double y, final double z, final double a, final double b) {
        double longitude = Math.atan2(y, x);
        final double ePrimeSquared = (a * a - b * b) / (b * b);
        final double p = Math.sqrt(x * x + y * y);
        final double theta = Math.atan(z * a / (p * b));
        final double sineTheta = Math.sin(theta);
        final double cosTheta = Math.cos(theta);
        final double f = 0.0033528106647474805;
        final double e2 = 2.0 * f - f * f;
        final double top = z + ePrimeSquared * b * sineTheta * sineTheta * sineTheta;
        final double bottom = p - e2 * a * cosTheta * cosTheta * cosTheta;
        final double geodeticLat = Math.atan(top / bottom);
        final double sineLat = Math.sin(geodeticLat);
        final double N = a / Math.sqrt(1.0 - e2 * sineLat * sineLat);
        final double altitude = p / Math.cos(geodeticLat) - N;
        if (longitude > 3.141592653589793) {
            longitude -= 6.283185307179586;
        }
        else if (longitude < -3.141592653589793) {
            longitude += 6.283185307179586;
        }
        return new double[] { geodeticLat, longitude, altitude };
    }
}
