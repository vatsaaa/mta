// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.conv;

import ucar.unidata.geoloc.projection.Stereographic;
import ucar.unidata.geoloc.ProjectionImpl;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.ProjectionPointImpl;
import ucar.unidata.geoloc.projection.LambertConformal;
import ucar.nc2.dataset.CoordinateAxis1D;
import ucar.nc2.dataset.ProjectionCT;
import ucar.unidata.util.Format;
import ucar.ma2.DataType;
import java.util.Formatter;
import ucar.nc2.dataset.CoordinateAxis;
import ucar.nc2.units.SimpleUnit;
import ucar.nc2.constants.AxisType;
import ucar.nc2.dataset.VariableEnhanced;
import java.util.StringTokenizer;
import java.util.ArrayList;
import ucar.ma2.IndexIterator;
import ucar.ma2.Array;
import java.util.Iterator;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.unidata.util.StringUtil;
import ucar.nc2.dataset.VariableDS;
import ucar.nc2.Dimension;
import java.util.NoSuchElementException;
import java.util.Comparator;
import java.util.List;
import java.util.Collections;
import java.io.IOException;
import ucar.nc2.Variable;
import ucar.nc2.Group;
import ucar.nc2.Attribute;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dataset.CoordSysBuilder;

public class NUWGConvention extends CoordSysBuilder
{
    private NavInfoList navInfo;
    private String xaxisName;
    private String yaxisName;
    private Grib1 grib;
    private final boolean dumpNav = false;
    private StringBuilder buf;
    
    public NUWGConvention() {
        this.navInfo = new NavInfoList();
        this.xaxisName = "";
        this.yaxisName = "";
        this.buf = new StringBuilder(2000);
        this.conventionName = "NUWG";
    }
    
    @Override
    public void augmentDataset(final NetcdfDataset ds, final CancelTask cancelTask) {
        if (null != ds.findGlobalAttribute("_enhanced")) {
            return;
        }
        ds.addAttribute(null, new Attribute("_enhanced", ""));
        final List<Variable> vars = ds.getVariables();
        for (final Variable v : vars) {
            if (0 <= v.findDimensionIndex("nav")) {
                try {
                    this.navInfo.add(new NavInfo(v));
                }
                catch (IOException ex) {
                    this.parseInfo.format("ERROR NUWG reading NAV var = %s\n", v);
                }
            }
        }
        Collections.sort((List<Object>)this.navInfo, (Comparator<? super Object>)new NavComparator());
        this.parseInfo.format("%s\n\n", this.navInfo);
        int mode = 3;
        try {
            mode = this.navInfo.getInt("grid_type_code");
        }
        catch (NoSuchElementException e) {
            NUWGConvention.log.warn("No mode in navInfo - assume 3");
        }
        try {
            if (mode == 0) {
                this.xaxisName = this.navInfo.getString("i_dim");
                this.yaxisName = this.navInfo.getString("j_dim");
            }
            else {
                this.xaxisName = this.navInfo.getString("x_dim");
                this.yaxisName = this.navInfo.getString("y_dim");
            }
        }
        catch (NoSuchElementException e) {
            NUWGConvention.log.warn("No mode in navInfo - assume = 1");
        }
        this.grib = new Grib1(mode);
        if (null == ds.findVariable(this.xaxisName)) {
            this.grib.makeXCoordAxis(ds, this.xaxisName);
            this.parseInfo.format("Generated x axis from NUWG nav= %s\n", this.xaxisName);
        }
        else if (this.xaxisName.equalsIgnoreCase("lon")) {
            try {
                boolean ok = true;
                final Variable dc = ds.findVariable(this.xaxisName);
                final Array coordVal = dc.read();
                final IndexIterator coordIndex = coordVal.getIndexIterator();
                double coord1 = coordIndex.getDoubleNext();
                double coord2 = coordIndex.getDoubleNext();
                final boolean increase = coord1 > coord2;
                coord1 = coord2;
                while (coordIndex.hasNext()) {
                    coord2 = coordIndex.getDoubleNext();
                    if (coord1 > coord2 ^ increase) {
                        ok = false;
                        break;
                    }
                    coord1 = coord2;
                }
                if (!ok) {
                    this.parseInfo.format("ERROR lon axis is not monotonic, regen from nav\n", new Object[0]);
                    this.grib.makeXCoordAxis(ds, this.xaxisName);
                }
            }
            catch (IOException ioe) {
                NUWGConvention.log.warn("IOException when reading xaxis = " + this.xaxisName);
            }
        }
        if (null == ds.findVariable(this.yaxisName)) {
            this.grib.makeYCoordAxis(ds, this.yaxisName);
            this.parseInfo.format("Generated y axis from NUWG nav=%s\n", this.yaxisName);
        }
        final List<Dimension> dims = ds.getRootGroup().getDimensions();
        for (final Dimension dim : dims) {
            final String dimName = dim.getName();
            if (null != ds.findVariable(dimName)) {
                continue;
            }
            final List<Variable> ncvars = this.searchAliasedDimension(ds, dim);
            if (ncvars == null) {
                continue;
            }
            if (ncvars.size() == 0) {
                continue;
            }
            if (ncvars.size() == 1) {
                final Variable ncvar = ncvars.get(0);
                if (!(ncvar instanceof VariableDS)) {
                    continue;
                }
                if (this.makeCoordinateAxis(ncvar, dim)) {
                    this.parseInfo.format("Added referential coordAxis = ", new Object[0]);
                    ncvar.getNameAndDimensions(this.parseInfo, true, false);
                    this.parseInfo.format("\n", new Object[0]);
                }
                else {
                    this.parseInfo.format("Couldnt add referential coordAxis = %s\n", ncvar.getName());
                }
            }
            else {
                if (ncvars.size() != 2) {
                    continue;
                }
                if (dimName.equals("record")) {
                    final Variable ncvar2 = ncvars.get(0);
                    final Variable ncvar3 = ncvars.get(1);
                    final Variable ncvar4 = ncvar2.getName().equalsIgnoreCase("valtime") ? ncvar2 : ncvar3;
                    if (this.makeCoordinateAxis(ncvar4, dim)) {
                        this.parseInfo.format("Added referential coordAxis (2) = ", new Object[0]);
                        ncvar4.getNameAndDimensions(this.parseInfo, true, false);
                        this.parseInfo.format("\n", new Object[0]);
                        String units = ncvar4.getUnitsString();
                        if (units == null) {
                            continue;
                        }
                        units = StringUtil.remove(units, 40);
                        units = StringUtil.remove(units, 41);
                        ncvar4.addAttribute(new Attribute("units", units));
                    }
                    else {
                        this.parseInfo.format("Couldnt add referential coordAxis = %s\n", ncvar4.getName());
                    }
                }
                else {
                    final Variable ncvar = ncvars.get(0);
                    if (!(ncvar instanceof VariableDS)) {
                        continue;
                    }
                    if (this.makeCoordinateAxis(ncvar, dim)) {
                        this.parseInfo.format("Added referential boundary coordAxis (2) = ", new Object[0]);
                        ncvar.getNameAndDimensions(this.parseInfo, true, false);
                        this.parseInfo.format("\n", new Object[0]);
                    }
                    else {
                        this.parseInfo.format("Couldnt add referential coordAxis = %s\n", ncvar.getName());
                    }
                }
            }
        }
        if (this.grib.ct != null) {
            final VariableDS v2 = this.makeCoordinateTransformVariable(ds, this.grib.ct);
            v2.addAttribute(new Attribute("_CoordinateAxes", this.xaxisName + " " + this.yaxisName));
            ds.addVariable(null, v2);
        }
        ds.finish();
    }
    
    private boolean makeCoordinateAxis(final Variable ncvar, final Dimension dim) {
        if (ncvar.getRank() != 1) {
            return false;
        }
        final Dimension vdim = ncvar.getDimension(0);
        if (!vdim.equals(dim)) {
            return false;
        }
        if (!dim.getName().equals(ncvar.getShortName())) {
            ncvar.addAttribute(new Attribute("_CoordinateAliasForDimension", dim.getName()));
        }
        return true;
    }
    
    private List<Variable> searchAliasedDimension(final NetcdfDataset ds, final Dimension dim) {
        final String dimName = dim.getName();
        final String alias = ds.findAttValueIgnoreCase(null, dimName, null);
        if (alias == null) {
            return null;
        }
        final List<Variable> vars = new ArrayList<Variable>();
        final StringTokenizer parser = new StringTokenizer(alias, " ,");
        while (parser.hasMoreTokens()) {
            final String token = parser.nextToken();
            final Variable ncvar = ds.findVariable(token);
            if (ncvar == null) {
                continue;
            }
            if (ncvar.getRank() != 1) {
                continue;
            }
            final Iterator dimIter = ncvar.getDimensions().iterator();
            final Dimension dim2 = dimIter.next();
            if (!dimName.equals(dim2.getName())) {
                continue;
            }
            vars.add(ncvar);
            if (!this.debug) {
                continue;
            }
            System.out.print(" " + token);
        }
        if (this.debug) {
            System.out.println();
        }
        return vars;
    }
    
    public String extraInfo() {
        this.buf.setLength(0);
        this.buf.append(this.navInfo).append("\n");
        return this.buf.toString();
    }
    
    @Override
    protected void makeCoordinateTransforms(final NetcdfDataset ds) {
        if (this.grib != null && this.grib.ct != null) {
            final VarProcess vp = this.findVarProcess(this.grib.ct.getName());
            vp.isCoordinateTransform = true;
            vp.ct = this.grib.ct;
        }
        super.makeCoordinateTransforms(ds);
    }
    
    @Override
    protected AxisType getAxisType(final NetcdfDataset ds, final VariableEnhanced ve) {
        final Variable v = (Variable)ve;
        final String vname = v.getName();
        if (vname.equalsIgnoreCase("lat")) {
            return AxisType.Lat;
        }
        if (vname.equalsIgnoreCase("lon")) {
            return AxisType.Lon;
        }
        if (vname.equalsIgnoreCase(this.xaxisName)) {
            return AxisType.GeoX;
        }
        if (vname.equalsIgnoreCase(this.yaxisName)) {
            return AxisType.GeoY;
        }
        if (vname.equalsIgnoreCase("record")) {
            return AxisType.Time;
        }
        final Dimension dim = v.getDimension(0);
        if (dim != null && dim.getName().equalsIgnoreCase("record")) {
            return AxisType.Time;
        }
        final String unit = ve.getUnitsString();
        if (unit != null) {
            if (SimpleUnit.isCompatible("millibar", unit)) {
                return AxisType.Pressure;
            }
            if (SimpleUnit.isCompatible("m", unit)) {
                return AxisType.Height;
            }
            if (SimpleUnit.isCompatible("sec", unit)) {
                return null;
            }
        }
        return AxisType.GeoZ;
    }
    
    public String getZisPositive(final CoordinateAxis v) {
        final String unit = v.getUnitsString();
        if (unit != null && SimpleUnit.isCompatible("m", unit)) {
            return "up";
        }
        return "down";
    }
    
    private class NavComparator implements Comparator<NavInfo>
    {
        public int compare(final NavInfo n1, final NavInfo n2) {
            return n1.getName().compareTo(n2.getName());
        }
        
        @Override
        public boolean equals(final Object obj) {
            return this == obj;
        }
    }
    
    private class NavInfo
    {
        Variable ncvar;
        DataType valueType;
        String svalue;
        byte bvalue;
        int ivalue;
        double dvalue;
        private StringBuilder buf;
        
        public NavInfo(final Variable ncvar) throws IOException {
            this.buf = new StringBuilder(200);
            this.ncvar = ncvar;
            this.valueType = ncvar.getDataType();
            try {
                if (this.valueType == DataType.CHAR || this.valueType == DataType.STRING) {
                    this.svalue = ncvar.readScalarString();
                }
                else if (this.valueType == DataType.BYTE) {
                    this.bvalue = ncvar.readScalarByte();
                }
                else if (this.valueType == DataType.INT || this.valueType == DataType.SHORT) {
                    this.ivalue = ncvar.readScalarInt();
                }
                else {
                    this.dvalue = ncvar.readScalarDouble();
                }
            }
            catch (UnsupportedOperationException e) {
                NUWGConvention.this.parseInfo.format("Nav variable %s  not a scalar\n", this.getName());
            }
        }
        
        public String getName() {
            return this.ncvar.getName();
        }
        
        public String getDescription() {
            final Attribute att = this.ncvar.findAttributeIgnoreCase("long_name");
            return (att == null) ? this.getName() : att.getStringValue();
        }
        
        public DataType getValueType() {
            return this.valueType;
        }
        
        public String getStringValue() {
            if (this.valueType == DataType.CHAR || this.valueType == DataType.STRING) {
                return this.svalue;
            }
            if (this.valueType == DataType.BYTE) {
                return Byte.toString(this.bvalue);
            }
            if (this.valueType == DataType.INT || this.valueType == DataType.SHORT) {
                return Integer.toString(this.ivalue);
            }
            return Double.toString(this.dvalue);
        }
        
        @Override
        public String toString() {
            this.buf.setLength(0);
            this.buf.append(this.getName());
            this.buf.append(" ");
            Format.tab(this.buf, 15, true);
            this.buf.append(this.getStringValue());
            this.buf.append(" ");
            Format.tab(this.buf, 35, true);
            this.buf.append(this.getDescription());
            return this.buf.toString();
        }
    }
    
    private class NavInfoList extends ArrayList<NavInfo>
    {
        private StringBuilder buf;
        
        private NavInfoList() {
            this.buf = new StringBuilder(2000);
        }
        
        public NavInfo findInfo(final String name) {
            for (final NavInfo nav : this) {
                if (name.equalsIgnoreCase(nav.getName())) {
                    return nav;
                }
            }
            return null;
        }
        
        public double getDouble(final String name) throws NoSuchElementException {
            final NavInfo nav = this.findInfo(name);
            if (nav == null) {
                throw new NoSuchElementException("GRIB1 " + name);
            }
            if (nav.valueType == DataType.DOUBLE || nav.valueType == DataType.FLOAT) {
                return nav.dvalue;
            }
            if (nav.valueType == DataType.INT || nav.valueType == DataType.SHORT) {
                return nav.ivalue;
            }
            if (nav.valueType == DataType.BYTE) {
                return nav.bvalue;
            }
            throw new IllegalArgumentException("NUWGConvention.GRIB1.getDouble " + name + " type = " + nav.valueType);
        }
        
        public int getInt(final String name) throws NoSuchElementException {
            final NavInfo nav = this.findInfo(name);
            if (nav == null) {
                throw new NoSuchElementException("GRIB1 " + name);
            }
            if (nav.valueType == DataType.INT || nav.valueType == DataType.SHORT) {
                return nav.ivalue;
            }
            if (nav.valueType == DataType.DOUBLE || nav.valueType == DataType.FLOAT) {
                return (int)nav.dvalue;
            }
            if (nav.valueType == DataType.BYTE) {
                return nav.bvalue;
            }
            throw new IllegalArgumentException("NUWGConvention.GRIB1.getInt " + name + " type = " + nav.valueType);
        }
        
        public String getString(final String name) throws NoSuchElementException {
            final NavInfo nav = this.findInfo(name);
            if (nav == null) {
                throw new NoSuchElementException("GRIB1 " + name);
            }
            return nav.svalue;
        }
        
        @Override
        public String toString() {
            this.buf.setLength(0);
            this.buf.append("\nNav Info\n");
            this.buf.append("Name___________Value_____________________Description\n");
            for (final NavInfo nava : this) {
                this.buf.append(nava).append("\n");
            }
            this.buf.append("\n");
            return this.buf.toString();
        }
    }
    
    private class Grib1
    {
        private String grid_name;
        private int grid_code;
        private ProjectionCT ct;
        private int nx;
        private int ny;
        private double startx;
        private double starty;
        private double dx;
        private double dy;
        
        Grib1(final int mode) {
            this.grid_code = 0;
            this.grid_name = "Projection";
            if (this.grid_name.length() == 0) {
                this.grid_name = "grid_var";
            }
            this.grid_code = mode;
            if (0 == this.grid_code) {
                this.processLatLonProjection();
            }
            else if (3 == this.grid_code) {
                this.ct = this.makeLCProjection();
            }
            else {
                if (5 != this.grid_code) {
                    throw new IllegalArgumentException("NUWGConvention: unknown grid_code= " + this.grid_code);
                }
                this.ct = this.makePSProjection();
            }
        }
        
        CoordinateAxis makeXCoordAxis(final NetcdfDataset ds, final String xname) {
            final CoordinateAxis v = new CoordinateAxis1D(ds, null, xname, DataType.DOUBLE, xname, (0 == this.grid_code) ? "degrees_east" : "km", "synthesized X coord");
            v.addAttribute(new Attribute("_CoordinateAxisType", (0 == this.grid_code) ? AxisType.Lon.toString() : AxisType.GeoX.toString()));
            ds.setValues(v, this.nx, this.startx, this.dx);
            ds.addCoordinateAxis(v);
            return v;
        }
        
        CoordinateAxis makeYCoordAxis(final NetcdfDataset ds, final String yname) {
            final CoordinateAxis v = new CoordinateAxis1D(ds, null, yname, DataType.DOUBLE, yname, (0 == this.grid_code) ? "degrees_north" : "km", "synthesized Y coord");
            v.addAttribute(new Attribute("_CoordinateAxisType", (0 == this.grid_code) ? AxisType.Lat.toString() : AxisType.GeoY.toString()));
            ds.setValues(v, this.ny, this.starty, this.dy);
            ds.addCoordinateAxis(v);
            return v;
        }
        
        private ProjectionCT makeLCProjection() throws NoSuchElementException {
            final double latin1 = NUWGConvention.this.navInfo.getDouble("Latin1");
            final double latin2 = NUWGConvention.this.navInfo.getDouble("Latin2");
            final double lov = NUWGConvention.this.navInfo.getDouble("Lov");
            final double la1 = NUWGConvention.this.navInfo.getDouble("La1");
            final double lo1 = NUWGConvention.this.navInfo.getDouble("Lo1");
            final LambertConformal lc = new LambertConformal(latin1, lov, latin1, latin2);
            final ProjectionPointImpl start = (ProjectionPointImpl)lc.latLonToProj(new LatLonPointImpl(la1, lo1));
            if (NUWGConvention.this.debug) {
                System.out.println("start at proj coord " + start);
            }
            this.startx = start.getX();
            this.starty = start.getY();
            this.nx = NUWGConvention.this.navInfo.getInt("Nx");
            this.ny = NUWGConvention.this.navInfo.getInt("Ny");
            this.dx = NUWGConvention.this.navInfo.getDouble("Dx") / 1000.0;
            this.dy = NUWGConvention.this.navInfo.getDouble("Dy") / 1000.0;
            return new ProjectionCT(this.grid_name, "FGDC", lc);
        }
        
        private ProjectionCT makePSProjection() throws NoSuchElementException {
            final double lov = NUWGConvention.this.navInfo.getDouble("Lov");
            final double la1 = NUWGConvention.this.navInfo.getDouble("La1");
            final double lo1 = NUWGConvention.this.navInfo.getDouble("Lo1");
            final Stereographic ps = new Stereographic(90.0, lov, 0.933);
            final ProjectionPointImpl start = (ProjectionPointImpl)ps.latLonToProj(new LatLonPointImpl(la1, lo1));
            if (NUWGConvention.this.debug) {
                System.out.println("start at proj coord " + start);
            }
            this.startx = start.getX();
            this.starty = start.getY();
            this.nx = NUWGConvention.this.navInfo.getInt("Nx");
            this.ny = NUWGConvention.this.navInfo.getInt("Ny");
            this.dx = NUWGConvention.this.navInfo.getDouble("Dx") / 1000.0;
            this.dy = NUWGConvention.this.navInfo.getDouble("Dy") / 1000.0;
            return new ProjectionCT(this.grid_name, "FGDC", ps);
        }
        
        private void processLatLonProjection() throws NoSuchElementException {
            this.starty = NUWGConvention.this.navInfo.getDouble("La1");
            this.startx = NUWGConvention.this.navInfo.getDouble("Lo1");
            this.nx = NUWGConvention.this.navInfo.getInt("Ni");
            this.ny = NUWGConvention.this.navInfo.getInt("Nj");
            this.dx = NUWGConvention.this.navInfo.getDouble("Di");
            this.dy = NUWGConvention.this.navInfo.getDouble("Dj");
        }
    }
}
