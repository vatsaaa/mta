// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.conv;

import java.io.IOException;
import ucar.nc2.dataset.VariableEnhanced;
import ucar.nc2.units.DateUnit;
import ucar.nc2.constants.AxisType;
import ucar.nc2.Group;
import ucar.nc2.Attribute;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.Variable;
import ucar.nc2.NetcdfFile;

public class Nimbus extends COARDSConvention
{
    public static boolean isMine(final NetcdfFile ncfile) {
        final String s = ncfile.findAttValueIgnoreCase(null, "Convention", "none");
        return s.equalsIgnoreCase("NCAR-RAF/nimbus");
    }
    
    public Nimbus() {
        this.conventionName = "NCAR-RAF/nimbus";
    }
    
    @Override
    public void augmentDataset(final NetcdfDataset ds, final CancelTask cancelTask) throws IOException {
        ds.addAttribute(null, new Attribute("cdm_data_type", FeatureType.TRAJECTORY.name()));
        if (!this.setAxisType(ds, "LATC", AxisType.Lat) && !this.setAxisType(ds, "LAT", AxisType.Lat)) {
            this.setAxisType(ds, "GGLAT", AxisType.Lat);
        }
        if (!this.setAxisType(ds, "LONC", AxisType.Lon) && !this.setAxisType(ds, "LON", AxisType.Lon)) {
            this.setAxisType(ds, "GGLON", AxisType.Lon);
        }
        if (!this.setAxisType(ds, "PALT", AxisType.Height)) {
            this.setAxisType(ds, "GGALT", AxisType.Height);
        }
        boolean hasTime = this.setAxisType(ds, "Time", AxisType.Time);
        if (!hasTime) {
            hasTime = this.setAxisType(ds, "time", AxisType.Time);
        }
        if (!hasTime) {
            final Variable time = ds.findVariable("time_offset");
            if (time != null) {
                final Variable base = ds.findVariable("base_time");
                final int base_time = base.readScalarInt();
                try {
                    final DateUnit dunit = new DateUnit("seconds since 1970-01-01 00:00");
                    final String time_units = "seconds since " + dunit.makeStandardDateString(base_time);
                    time.addAttribute(new Attribute("units", time_units));
                    time.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Time.name()));
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        final String coordinates = ds.findAttValueIgnoreCase(null, "coordinates", null);
        if (coordinates != null) {
            final String[] arr$;
            final String[] vars = arr$ = coordinates.split(" ");
            for (final String vname : arr$) {
                final Variable v = ds.findVariable(vname);
                if (v != null) {
                    final AxisType atype = this.getAxisType(ds, (VariableEnhanced)v);
                    if (atype != null) {
                        v.addAttribute(new Attribute("_CoordinateAxisType", atype.name()));
                    }
                }
            }
        }
    }
    
    private boolean setAxisType(final NetcdfDataset ds, final String varName, final AxisType atype) {
        final Variable v = ds.findVariable(varName);
        if (v == null) {
            return false;
        }
        v.addAttribute(new Attribute("_CoordinateAxisType", atype.toString()));
        return true;
    }
}
