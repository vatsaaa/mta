// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.conv;

import ucar.unidata.geoloc.vertical.AtmosSigma;
import ucar.unidata.geoloc.vertical.HybridSigmaPressure;
import ucar.unidata.geoloc.vertical.VerticalTransform;
import ucar.nc2.Dimension;
import ucar.nc2.dataset.CoordTransBuilderIF;
import ucar.nc2.dataset.VerticalCT;
import ucar.nc2.dataset.transform.AbstractCoordTransBuilder;
import java.util.Formatter;
import ucar.ma2.Array;
import ucar.unidata.util.Parameter;
import ucar.nc2.NetcdfFile;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.nc2.units.SimpleUnit;
import ucar.nc2.dataset.VariableEnhanced;
import ucar.nc2.dataset.CoordSysBuilder;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import ucar.nc2.dataset.TransformType;
import ucar.nc2.Attribute;
import ucar.nc2.constants.AxisType;
import ucar.nc2.Variable;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import java.util.HashMap;

public class CSMConvention extends COARDSConvention
{
    protected HashMap ctHash;
    
    public CSMConvention() {
        this.ctHash = new HashMap();
        this.conventionName = "NCAR-CSM";
    }
    
    @Override
    public void augmentDataset(final NetcdfDataset ds, final CancelTask cancelTask) throws IOException {
        final List<Variable> vars = ds.getVariables();
        for (final Variable var : vars) {
            final String unit = var.getUnitsString();
            if (unit != null && (unit.equalsIgnoreCase("hybrid_sigma_pressure") || unit.equalsIgnoreCase("sigma_level"))) {
                var.addAttribute(new Attribute("_CoordinateAxisType", AxisType.GeoZ.toString()));
                var.addAttribute(new Attribute("_CoordinateTransformType", TransformType.Vertical.toString()));
                var.addAttribute(new Attribute("_CoordinateAxes", var.getName()));
            }
        }
    }
    
    @Override
    protected void findCoordinateAxes(final NetcdfDataset ds) {
        for (final VarProcess vp : this.varList) {
            if (vp.coordAxes == null) {
                final String coordsString = ds.findAttValueIgnoreCase(vp.v, "coordinates", null);
                if (coordsString == null) {
                    continue;
                }
                vp.coordinates = coordsString;
            }
        }
        super.findCoordinateAxes(ds);
    }
    
    @Override
    protected AxisType getAxisType(final NetcdfDataset ncd, final VariableEnhanced v) {
        final AxisType atype = super.getAxisType(ncd, v);
        if (atype != null) {
            return atype;
        }
        final String unit = v.getUnitsString();
        if (unit == null) {
            return null;
        }
        if (SimpleUnit.isTimeUnit(unit)) {
            return AxisType.Time;
        }
        return null;
    }
    
    @Override
    protected CoordinateTransform makeCoordinateTransform(final NetcdfDataset ds, final Variable ctv) {
        CoordinateTransform ct = null;
        final String unit = ctv.getUnitsString();
        if (unit != null) {
            if (unit.equalsIgnoreCase("hybrid_sigma_pressure")) {
                final HybridSigmaPressureBuilder b = new HybridSigmaPressureBuilder();
                ct = b.makeCoordinateTransform(ds, ctv);
            }
            else if (unit.equalsIgnoreCase("sigma_level")) {
                final SigmaBuilder b2 = new SigmaBuilder();
                ct = b2.makeCoordinateTransform(ds, ctv);
            }
        }
        if (ct != null) {
            return ct;
        }
        return super.makeCoordinateTransform(ds, ctv);
    }
    
    protected boolean addParameter2(final CoordinateTransform rs, final String paramName, final NetcdfFile ds, final Variable v, final String attName, final boolean readData) {
        String varName;
        if (null == (varName = ds.findAttValueIgnoreCase(v, attName, null))) {
            this.parseInfo.format("CSMConvention No Attribute named %s\n", attName);
            return false;
        }
        varName = varName.trim();
        final Variable dataVar;
        if (null == (dataVar = ds.findVariable(varName))) {
            this.parseInfo.format("CSMConvention No Variable named %s\n", varName);
            return false;
        }
        if (readData) {
            Array data;
            try {
                data = dataVar.read();
            }
            catch (IOException e) {
                this.parseInfo.format("CSMConvention failed on read of %s err= %s\n", varName, e.getMessage());
                return false;
            }
            final double[] vals = (double[])data.get1DJavaArray(Double.TYPE);
            rs.addParameter(new Parameter(paramName, vals));
        }
        else {
            rs.addParameter(new Parameter(paramName, varName));
        }
        return true;
    }
    
    private class HybridSigmaPressureBuilder extends AbstractCoordTransBuilder
    {
        public String getTransformName() {
            return "csm_hybrid_sigma_pressure";
        }
        
        public TransformType getTransformType() {
            return TransformType.Vertical;
        }
        
        public CoordinateTransform makeCoordinateTransform(final NetcdfDataset ds, final Variable ctv) {
            final CoordinateTransform rs = new VerticalCT(ctv.getName(), this.getTransformName(), VerticalCT.Type.HybridSigmaPressure, this);
            rs.addParameter(new Parameter("formula", "pressure(x,y,z) = a(z)*p0 + b(z)*surfacePressure(x,y)"));
            if (!CSMConvention.this.addParameter2(rs, "SurfacePressure_variableName", ds, ctv, "PS_var", false)) {
                return null;
            }
            if (!CSMConvention.this.addParameter2(rs, "A_variableName", ds, ctv, "A_var", false)) {
                return null;
            }
            if (!CSMConvention.this.addParameter2(rs, "B_variableName", ds, ctv, "B_var", false)) {
                return null;
            }
            if (!CSMConvention.this.addParameter2(rs, "P0_variableName", ds, ctv, "P0_var", false)) {
                return null;
            }
            CSMConvention.this.parseInfo.format("CSMConvention made SigmaPressureCT %s\n", ctv.getName());
            return rs;
        }
        
        @Override
        public VerticalTransform makeMathTransform(final NetcdfDataset ds, final Dimension timeDim, final VerticalCT vCT) {
            return new HybridSigmaPressure(ds, timeDim, vCT.getParameters());
        }
    }
    
    private class SigmaBuilder extends AbstractCoordTransBuilder
    {
        public String getTransformName() {
            return "csm_sigma_level";
        }
        
        public TransformType getTransformType() {
            return TransformType.Vertical;
        }
        
        public CoordinateTransform makeCoordinateTransform(final NetcdfDataset ds, final Variable ctv) {
            final CoordinateTransform rs = new VerticalCT("sigma-" + ctv.getName(), CSMConvention.this.conventionName, VerticalCT.Type.Sigma, this);
            rs.addParameter(new Parameter("formula", "pressure(x,y,z) = ptop + sigma(z)*(surfacePressure(x,y)-ptop)"));
            if (!CSMConvention.this.addParameter2(rs, "SurfacePressure_variableName", ds, ctv, "PS_var", false)) {
                return null;
            }
            if (!CSMConvention.this.addParameter2(rs, "Sigma_variableName", ds, ctv, "B_var", false)) {
                return null;
            }
            if (!CSMConvention.this.addParameter2(rs, "PressureTop_variableName", ds, ctv, "P0_var", false)) {
                return null;
            }
            CSMConvention.this.parseInfo.format("CSMConvention made SigmaCT %s\n", ctv.getName());
            return rs;
        }
        
        @Override
        public VerticalTransform makeMathTransform(final NetcdfDataset ds, final Dimension timeDim, final VerticalCT vCT) {
            return new AtmosSigma(ds, timeDim, vCT.getParameters());
        }
    }
}
