// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.conv;

import ucar.unidata.geoloc.ProjectionImpl;
import ucar.unidata.geoloc.projection.Stereographic;
import ucar.unidata.geoloc.projection.TransverseMercator;
import ucar.unidata.geoloc.projection.LambertConformal;
import java.util.StringTokenizer;
import java.util.List;
import ucar.nc2.dataset.VariableEnhanced;
import java.util.HashMap;
import ucar.nc2.Dimension;
import java.util.Iterator;
import ucar.nc2.dataset.CoordSysBuilder;
import ucar.nc2.dataset.VariableDS;
import ucar.nc2.Attribute;
import ucar.nc2.constants.AxisType;
import ucar.nc2.Variable;
import ucar.nc2.Group;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dataset.ProjectionCT;

public class GDVConvention extends CSMConvention
{
    protected ProjectionCT projCT;
    
    public GDVConvention() {
        this.projCT = null;
        this.conventionName = "GDV";
        this.checkForMeter = false;
    }
    
    @Override
    public void augmentDataset(final NetcdfDataset ds, final CancelTask cancelTask) {
        this.projCT = this.makeProjectionCT(ds);
        if (this.projCT != null) {
            final VariableDS v = this.makeCoordinateTransformVariable(ds, this.projCT);
            ds.addVariable(null, v);
            final String xname = this.findCoordinateName(ds, AxisType.GeoX);
            final String yname = this.findCoordinateName(ds, AxisType.GeoY);
            if (xname != null && yname != null) {
                v.addAttribute(new Attribute("_CoordinateAxes", xname + " " + yname));
            }
        }
        ds.finish();
    }
    
    @Override
    protected void findCoordinateAxes(final NetcdfDataset ds) {
        for (final VarProcess vp : this.varList) {
            if (vp.isCoordinateVariable) {
                continue;
            }
            final Variable ncvar = vp.v;
            if (!(ncvar instanceof VariableDS)) {
                continue;
            }
            final String dimName = this.findAlias(ds, ncvar);
            if (dimName.equals("")) {
                continue;
            }
            final Dimension dim = ds.findDimension(dimName);
            if (null == dim) {
                continue;
            }
            vp.isCoordinateAxis = true;
            this.parseInfo.format(" Coordinate Axis added (GDV alias) = %s for dimension %s\n", vp.v.getName(), dimName);
        }
        super.findCoordinateAxes(ds);
        this.findCoordinateAxesForce(ds);
    }
    
    private void findCoordinateAxesForce(final NetcdfDataset ds) {
        final HashMap<AxisType, VarProcess> map = new HashMap<AxisType, VarProcess>();
        for (final VarProcess vp : this.varList) {
            if (vp.isCoordinateAxis) {
                final AxisType atype = this.getAxisType(ds, (VariableEnhanced)vp.v);
                if (atype == null) {
                    continue;
                }
                map.put(atype, vp);
            }
        }
        for (final VarProcess vp : this.varList) {
            if (vp.isCoordinateVariable) {
                continue;
            }
            final Variable ncvar = vp.v;
            if (!(ncvar instanceof VariableDS)) {
                continue;
            }
            final AxisType atype2 = this.getAxisType(ds, (VariableEnhanced)vp.v);
            if (atype2 == null || map.get(atype2) != null) {
                continue;
            }
            vp.isCoordinateAxis = true;
            this.parseInfo.format(" Coordinate Axis added (GDV forced) = %s  for axis %s\n", vp.v.getName(), atype2);
        }
    }
    
    private String findCoordinateName(final NetcdfDataset ds, final AxisType axisType) {
        final List<Variable> vlist = ds.getVariables();
        for (final Variable aVlist : vlist) {
            final VariableEnhanced ve = (VariableEnhanced)aVlist;
            if (axisType == this.getAxisType(ds, ve)) {
                return ve.getName();
            }
        }
        return null;
    }
    
    @Override
    protected void makeCoordinateTransforms(final NetcdfDataset ds) {
        if (this.projCT != null) {
            final VarProcess vp = this.findVarProcess(this.projCT.getName());
            if (vp != null) {
                vp.ct = this.projCT;
            }
        }
        super.makeCoordinateTransforms(ds);
    }
    
    @Override
    protected AxisType getAxisType(final NetcdfDataset ds, final VariableEnhanced ve) {
        final Variable v = (Variable)ve;
        final String vname = v.getName();
        if (vname.equalsIgnoreCase("x") || this.findAlias(ds, v).equalsIgnoreCase("x")) {
            return AxisType.GeoX;
        }
        if (vname.equalsIgnoreCase("lon") || vname.equalsIgnoreCase("longitude") || this.findAlias(ds, v).equalsIgnoreCase("lon")) {
            return AxisType.Lon;
        }
        if (vname.equalsIgnoreCase("y") || this.findAlias(ds, v).equalsIgnoreCase("y")) {
            return AxisType.GeoY;
        }
        if (vname.equalsIgnoreCase("lat") || vname.equalsIgnoreCase("latitude") || this.findAlias(ds, v).equalsIgnoreCase("lat")) {
            return AxisType.Lat;
        }
        if (vname.equalsIgnoreCase("lev") || this.findAlias(ds, v).equalsIgnoreCase("lev") || vname.equalsIgnoreCase("level") || this.findAlias(ds, v).equalsIgnoreCase("level")) {
            return AxisType.GeoZ;
        }
        if (vname.equalsIgnoreCase("z") || this.findAlias(ds, v).equalsIgnoreCase("z") || vname.equalsIgnoreCase("altitude") || vname.equalsIgnoreCase("depth")) {
            return AxisType.Height;
        }
        if (vname.equalsIgnoreCase("time") || this.findAlias(ds, v).equalsIgnoreCase("time")) {
            return AxisType.Time;
        }
        return super.getAxisType(ds, ve);
    }
    
    private String findAlias(final NetcdfDataset ds, final Variable v) {
        String alias = ds.findAttValueIgnoreCase(v, "coord_axis", null);
        if (alias == null) {
            alias = ds.findAttValueIgnoreCase(v, "coord_alias", "");
        }
        return alias;
    }
    
    private ProjectionCT makeProjectionCT(final NetcdfDataset ds) {
        final String projection = ds.findAttValueIgnoreCase(null, "projection", null);
        if (null == projection) {
            this.parseInfo.format("GDV Conventions error: NO projection name found \n", new Object[0]);
            return null;
        }
        String params = ds.findAttValueIgnoreCase(null, "projection_params", null);
        if (null == params) {
            params = ds.findAttValueIgnoreCase(null, "proj_params", null);
        }
        if (null == params) {
            this.parseInfo.format("GDV Conventions error: NO projection parameters found \n", new Object[0]);
            return null;
        }
        int count = 0;
        final double[] p = new double[4];
        try {
            for (StringTokenizer stoke = new StringTokenizer(params, " ,"); stoke.hasMoreTokens() && count < 4; p[count++] = Double.parseDouble(stoke.nextToken())) {}
        }
        catch (NumberFormatException e) {
            for (StringTokenizer stoke2 = new StringTokenizer(params, " ,"); stoke2.hasMoreTokens() && count < 4; p[count++] = Double.parseDouble(stoke2.nextToken())) {
                stoke2.nextToken();
            }
        }
        this.parseInfo.format("GDV Conventions projection %s params = %f %f %f %f\n", projection, p[0], p[1], p[2], p[3]);
        ProjectionImpl proj;
        if (projection.equalsIgnoreCase("LambertConformal")) {
            proj = new LambertConformal(p[0], p[1], p[2], p[3]);
        }
        else if (projection.equalsIgnoreCase("TransverseMercator")) {
            proj = new TransverseMercator(p[0], p[1], p[2]);
        }
        else {
            if (!projection.equalsIgnoreCase("Stereographic") && !projection.equalsIgnoreCase("Oblique_Stereographic")) {
                this.parseInfo.format("GDV Conventions error: Unknown projection %s\n", projection);
                return null;
            }
            proj = new Stereographic(p[0], p[1], p[2]);
        }
        return new ProjectionCT(proj.getClassName(), "FGDC", proj);
    }
}
