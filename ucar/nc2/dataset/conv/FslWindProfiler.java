// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.conv;

import java.io.IOException;
import java.util.Iterator;
import ucar.nc2.constants.AxisType;
import ucar.nc2.Attribute;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.Variable;
import ucar.nc2.NetcdfFile;
import ucar.nc2.dataset.CoordSysBuilder;

public class FslWindProfiler extends CoordSysBuilder
{
    public static boolean isMine(final NetcdfFile ncfile) {
        final String title = ncfile.findAttValueIgnoreCase(null, "title", null);
        return title != null && title.startsWith("WPDN data");
    }
    
    public FslWindProfiler() {
        this.conventionName = "FslWindProfiler";
    }
    
    @Override
    public void augmentDataset(final NetcdfDataset ds, final CancelTask cancelTask) throws IOException {
        for (final Variable v : ds.getVariables()) {
            if (v.getShortName().equals("staName")) {
                v.addAttribute(new Attribute("standard_name", "station_name"));
            }
            else if (v.getShortName().equals("staLat")) {
                v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lat.toString()));
            }
            else if (v.getShortName().equals("staLon")) {
                v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lon.toString()));
            }
            else if (v.getShortName().equals("staElev")) {
                v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Height.toString()));
            }
            else if (v.getShortName().equals("levels")) {
                v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Height.toString()));
            }
            else {
                if (!v.getShortName().equals("timeObs")) {
                    continue;
                }
                v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Time.toString()));
            }
        }
        ds.finish();
    }
}
