// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.conv;

import java.util.TimeZone;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Index;
import ucar.nc2.NCdump;
import ucar.nc2.dataset.transform.WRFEtaTransformBuilder;
import ucar.nc2.dataset.VerticalCT;
import ucar.nc2.dataset.CoordinateSystem;
import java.util.Date;
import java.text.ParseException;
import ucar.ma2.ArrayChar;
import ucar.ma2.ArrayDouble;
import ucar.nc2.dataset.CoordinateAxis1D;
import ucar.nc2.units.SimpleUnit;
import ucar.nc2.dataset.VariableEnhanced;
import ucar.unidata.util.StringUtil;
import ucar.ma2.IndexIterator;
import java.io.IOException;
import ucar.nc2.dataset.CoordinateAxis;
import ucar.unidata.geoloc.ProjectionPoint;
import ucar.nc2.Dimension;
import ucar.unidata.geoloc.ProjectionImpl;
import java.util.Iterator;
import java.util.List;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.ProjectionPointImpl;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.projection.Mercator;
import ucar.unidata.geoloc.projection.Stereographic;
import ucar.unidata.geoloc.projection.LambertConformal;
import ucar.unidata.geoloc.projection.FlatEarth;
import ucar.ma2.Array;
import ucar.nc2.Structure;
import ucar.nc2.Group;
import ucar.nc2.dataset.VariableDS;
import ucar.ma2.DataType;
import ucar.nc2.constants.AxisType;
import ucar.nc2.Variable;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.Attribute;
import ucar.nc2.NetcdfFile;
import ucar.nc2.dataset.ProjectionCT;
import java.text.SimpleDateFormat;
import ucar.nc2.dataset.CoordSysBuilder;

public class WRFConvention extends CoordSysBuilder
{
    private static SimpleDateFormat dateFormat;
    private double centerX;
    private double centerY;
    private ProjectionCT projCT;
    
    public static boolean isMine(final NetcdfFile ncfile) {
        if (null == ncfile.findDimension("south_north")) {
            return false;
        }
        Attribute att = ncfile.findGlobalAttribute("DYN_OPT");
        if (att != null) {
            if (att.getNumericValue().intValue() != 2) {
                return false;
            }
        }
        else {
            att = ncfile.findGlobalAttribute("GRIDTYPE");
            if (att == null) {
                return false;
            }
            if (!att.getStringValue().equalsIgnoreCase("C")) {
                return false;
            }
        }
        att = ncfile.findGlobalAttribute("MAP_PROJ");
        return att != null;
    }
    
    public WRFConvention() {
        this.centerX = 0.0;
        this.centerY = 0.0;
        this.projCT = null;
        this.conventionName = "WRF";
    }
    
    @Override
    public void augmentDataset(final NetcdfDataset ds, final CancelTask cancelTask) {
        if (null != ds.findVariable("x")) {
            return;
        }
        final List<Variable> vlist = ds.getVariables();
        for (final Variable v : vlist) {
            final Attribute att = v.findAttributeIgnoreCase("units");
            if (att != null) {
                final String units = att.getStringValue();
                v.addAttribute(new Attribute("units", this.normalize(units)));
            }
        }
        final Attribute att2 = ds.findGlobalAttribute("MAP_PROJ");
        final int projType = att2.getNumericValue().intValue();
        boolean isLatLon = false;
        if (projType == 203) {
            final Variable glat = ds.findVariable("GLAT");
            if (glat == null) {
                this.parseInfo.format("Projection type 203 - expected GLAT variable not found\n", new Object[0]);
            }
            else {
                glat.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lat.toString()));
                glat.setDimensions("south_north west_east");
                glat.setCachedData(this.convertToDegrees(glat), false);
                glat.addAttribute(new Attribute("units", "degrees_north"));
            }
            final Variable glon = ds.findVariable("GLON");
            if (glon == null) {
                this.parseInfo.format("Projection type 203 - expected GLON variable not found\n", new Object[0]);
            }
            else {
                glon.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lon.toString()));
                glon.setDimensions("south_north west_east");
                glon.setCachedData(this.convertToDegrees(glon), false);
                glon.addAttribute(new Attribute("units", "degrees_east"));
            }
            final VariableDS v2 = new VariableDS(ds, null, null, "LatLonCoordSys", DataType.CHAR, "", null, null);
            v2.addAttribute(new Attribute("_CoordinateAxes", "GLAT GLON Time"));
            final Array data = Array.factory(DataType.CHAR.getPrimitiveClassType(), new int[0], new char[] { ' ' });
            v2.setCachedData(data, true);
            ds.addVariable(null, v2);
            final Variable dataVar = ds.findVariable("LANDMASK");
            dataVar.addAttribute(new Attribute("_CoordinateSystems", "LatLonCoordSys"));
        }
        else {
            final double lat1 = this.findAttributeDouble(ds, "TRUELAT1");
            final double lat2 = this.findAttributeDouble(ds, "TRUELAT2");
            final double centralLat = this.findAttributeDouble(ds, "CEN_LAT");
            final double centralLon = this.findAttributeDouble(ds, "CEN_LON");
            final double standardLon = this.findAttributeDouble(ds, "STAND_LON");
            final double standardLat = this.findAttributeDouble(ds, "MOAD_CEN_LAT");
            ProjectionImpl proj = null;
            switch (projType) {
                case 0: {
                    proj = new FlatEarth();
                    this.projCT = new ProjectionCT("flat_earth", "FGDC", proj);
                    break;
                }
                case 1: {
                    proj = new LambertConformal(standardLat, standardLon, lat1, lat2);
                    this.projCT = new ProjectionCT("Lambert", "FGDC", proj);
                    break;
                }
                case 2: {
                    final double lon0 = Double.isNaN(standardLon) ? centralLon : standardLon;
                    final double lat3 = Double.isNaN(centralLat) ? lat2 : centralLat;
                    final double scaleFactor = (1.0 + Math.abs(Math.sin(Math.toRadians(lat1)))) / 2.0;
                    proj = new Stereographic(lat3, lon0, scaleFactor);
                    this.projCT = new ProjectionCT("Stereographic", "FGDC", proj);
                    break;
                }
                case 3: {
                    proj = new Mercator(standardLon, standardLat);
                    this.projCT = new ProjectionCT("Mercator", "FGDC", proj);
                    break;
                }
                case 6: {
                    isLatLon = true;
                    for (final Variable v3 : vlist) {
                        if (v3.getShortName().startsWith("XLAT")) {
                            v3.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lat.toString()));
                            final int[] shape = v3.getShape();
                            if (v3.getRank() != 3 || shape[0] != 1) {
                                continue;
                            }
                            final List<Dimension> dims = v3.getDimensions();
                            dims.remove(0);
                            v3.setDimensions(dims);
                        }
                        else if (v3.getShortName().startsWith("XLONG")) {
                            v3.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lon.toString()));
                            final int[] shape = v3.getShape();
                            if (v3.getRank() != 3 || shape[0] != 1) {
                                continue;
                            }
                            final List<Dimension> dims = v3.getDimensions();
                            dims.remove(0);
                            v3.setDimensions(dims);
                        }
                        else if (v3.getShortName().equals("T")) {
                            v3.addAttribute(new Attribute("_CoordinateAxes", "Time XLAT XLONG z"));
                        }
                        else if (v3.getShortName().equals("U")) {
                            v3.addAttribute(new Attribute("_CoordinateAxes", "Time XLAT_U XLONG_U z"));
                        }
                        else if (v3.getShortName().equals("V")) {
                            v3.addAttribute(new Attribute("_CoordinateAxes", "Time XLAT_V XLONG_V z"));
                        }
                        else {
                            if (!v3.getShortName().equals("W")) {
                                continue;
                            }
                            v3.addAttribute(new Attribute("_CoordinateAxes", "Time XLAT XLONG z_stag"));
                        }
                    }
                    break;
                }
                default: {
                    this.parseInfo.format("ERROR: unknown projection type = %s\n", projType);
                    break;
                }
            }
            if (proj != null) {
                final LatLonPointImpl lpt1 = new LatLonPointImpl(centralLat, centralLon);
                final ProjectionPoint ppt1 = proj.latLonToProj(lpt1, new ProjectionPointImpl());
                this.centerX = ppt1.getX();
                this.centerY = ppt1.getY();
                if (this.debug) {
                    System.out.println("centerX=" + this.centerX);
                    System.out.println("centerY=" + this.centerY);
                }
            }
            if (!isLatLon) {
                ds.addCoordinateAxis(this.makeXCoordAxis(ds, "x", ds.findDimension("west_east")));
                ds.addCoordinateAxis(this.makeXCoordAxis(ds, "x_stag", ds.findDimension("west_east_stag")));
                ds.addCoordinateAxis(this.makeYCoordAxis(ds, "y", ds.findDimension("south_north")));
                ds.addCoordinateAxis(this.makeYCoordAxis(ds, "y_stag", ds.findDimension("south_north_stag")));
            }
            ds.addCoordinateAxis(this.makeZCoordAxis(ds, "z", ds.findDimension("bottom_top")));
            ds.addCoordinateAxis(this.makeZCoordAxis(ds, "z_stag", ds.findDimension("bottom_top_stag")));
            if (this.projCT != null) {
                final VariableDS v4 = this.makeCoordinateTransformVariable(ds, this.projCT);
                v4.addAttribute(new Attribute("_CoordinateAxisTypes", "GeoX GeoY"));
                ds.addVariable(null, v4);
            }
        }
        if (ds.findVariable("Time") == null) {
            CoordinateAxis taxis = this.makeTimeCoordAxis(ds, "Time", ds.findDimension("Time"));
            if (taxis == null) {
                taxis = this.makeTimeCoordAxis(ds, "Time", ds.findDimension("Times"));
            }
            if (taxis != null) {
                ds.addCoordinateAxis(taxis);
            }
        }
        ds.addCoordinateAxis(this.makeSoilDepthCoordAxis(ds, "ZS"));
        ds.finish();
    }
    
    private Array convertToDegrees(final Variable v) {
        Array data;
        try {
            data = v.read();
            data = data.reduce();
        }
        catch (IOException ioe) {
            throw new RuntimeException("data read failed on " + v.getName() + "=" + ioe.getMessage());
        }
        final IndexIterator ii = data.getIndexIterator();
        while (ii.hasNext()) {
            ii.setDoubleCurrent(Math.toDegrees(ii.getDoubleNext()));
        }
        return data;
    }
    
    private String normalize(String units) {
        if (units.equals("fraction")) {
            units = "";
        }
        else if (units.equals("dimensionless")) {
            units = "";
        }
        else if (units.equals("NA")) {
            units = "";
        }
        else if (units.equals("-")) {
            units = "";
        }
        else {
            units = StringUtil.substitute(units, "**", "^");
            units = StringUtil.remove(units, 125);
            units = StringUtil.remove(units, 123);
        }
        return units;
    }
    
    @Override
    protected void makeCoordinateTransforms(final NetcdfDataset ds) {
        if (this.projCT != null) {
            final VarProcess vp = this.findVarProcess(this.projCT.getName());
            vp.isCoordinateTransform = true;
            vp.ct = this.projCT;
        }
        super.makeCoordinateTransforms(ds);
    }
    
    @Override
    protected AxisType getAxisType(final NetcdfDataset ds, final VariableEnhanced ve) {
        final Variable v = (Variable)ve;
        final String vname = v.getName();
        if (vname.equalsIgnoreCase("x") || vname.equalsIgnoreCase("x_stag")) {
            return AxisType.GeoX;
        }
        if (vname.equalsIgnoreCase("lon")) {
            return AxisType.Lon;
        }
        if (vname.equalsIgnoreCase("y") || vname.equalsIgnoreCase("y_stag")) {
            return AxisType.GeoY;
        }
        if (vname.equalsIgnoreCase("lat")) {
            return AxisType.Lat;
        }
        if (vname.equalsIgnoreCase("z") || vname.equalsIgnoreCase("z_stag")) {
            return AxisType.GeoZ;
        }
        if (vname.equalsIgnoreCase("Z")) {
            return AxisType.Height;
        }
        if (vname.equalsIgnoreCase("time") || vname.equalsIgnoreCase("times")) {
            return AxisType.Time;
        }
        final String unit = ve.getUnitsString();
        if (unit != null) {
            if (SimpleUnit.isCompatible("millibar", unit)) {
                return AxisType.Pressure;
            }
            if (SimpleUnit.isCompatible("m", unit)) {
                return AxisType.Height;
            }
        }
        return null;
    }
    
    public String getZisPositive(final CoordinateAxis v) {
        return "down";
    }
    
    private CoordinateAxis makeLonCoordAxis(final NetcdfDataset ds, final String axisName, final Dimension dim) {
        if (dim == null) {
            return null;
        }
        final double dx = 4.0 * this.findAttributeDouble(ds, "DX");
        final int nx = dim.getLength();
        final double startx = this.centerX - dx * (nx - 1) / 2.0;
        final CoordinateAxis v = new CoordinateAxis1D(ds, null, axisName, DataType.DOUBLE, dim.getName(), "degrees_east", "synthesized longitude coordinate");
        ds.setValues(v, nx, startx, dx);
        v.addAttribute(new Attribute("_CoordinateAxisType", "Lon"));
        if (!axisName.equals(dim.getName())) {
            v.addAttribute(new Attribute("_CoordinateAliasForDimension", dim.getName()));
        }
        return v;
    }
    
    private CoordinateAxis makeLatCoordAxis(final NetcdfDataset ds, final String axisName, final Dimension dim) {
        if (dim == null) {
            return null;
        }
        final double dy = this.findAttributeDouble(ds, "DY");
        final int ny = dim.getLength();
        final double starty = this.centerY - dy * (ny - 1) / 2.0;
        final CoordinateAxis v = new CoordinateAxis1D(ds, null, axisName, DataType.DOUBLE, dim.getName(), "degrees_north", "synthesized latitude coordinate");
        ds.setValues(v, ny, starty, dy);
        v.addAttribute(new Attribute("_CoordinateAxisType", "Lat"));
        if (!axisName.equals(dim.getName())) {
            v.addAttribute(new Attribute("_CoordinateAliasForDimension", dim.getName()));
        }
        return v;
    }
    
    private CoordinateAxis makeXCoordAxis(final NetcdfDataset ds, final String axisName, final Dimension dim) {
        if (dim == null) {
            return null;
        }
        final double dx = this.findAttributeDouble(ds, "DX") / 1000.0;
        final int nx = dim.getLength();
        final double startx = this.centerX - dx * (nx - 1) / 2.0;
        final CoordinateAxis v = new CoordinateAxis1D(ds, null, axisName, DataType.DOUBLE, dim.getName(), "km", "synthesized GeoX coordinate from DX attribute");
        ds.setValues(v, nx, startx, dx);
        v.addAttribute(new Attribute("_CoordinateAxisType", "GeoX"));
        if (!axisName.equals(dim.getName())) {
            v.addAttribute(new Attribute("_CoordinateAliasForDimension", dim.getName()));
        }
        return v;
    }
    
    private CoordinateAxis makeYCoordAxis(final NetcdfDataset ds, final String axisName, final Dimension dim) {
        if (dim == null) {
            return null;
        }
        final double dy = this.findAttributeDouble(ds, "DY") / 1000.0;
        final int ny = dim.getLength();
        final double starty = this.centerY - dy * (ny - 1) / 2.0;
        final CoordinateAxis v = new CoordinateAxis1D(ds, null, axisName, DataType.DOUBLE, dim.getName(), "km", "synthesized GeoY coordinate from DY attribute");
        ds.setValues(v, ny, starty, dy);
        v.addAttribute(new Attribute("_CoordinateAxisType", "GeoY"));
        if (!axisName.equals(dim.getName())) {
            v.addAttribute(new Attribute("_CoordinateAliasForDimension", dim.getName()));
        }
        return v;
    }
    
    private CoordinateAxis makeZCoordAxis(final NetcdfDataset ds, final String axisName, final Dimension dim) {
        if (dim == null) {
            return null;
        }
        final String fromWhere = axisName.endsWith("stag") ? "ZNW" : "ZNU";
        final CoordinateAxis v = new CoordinateAxis1D(ds, null, axisName, DataType.DOUBLE, dim.getName(), "", "eta values from variable " + fromWhere);
        v.addAttribute(new Attribute("_CoordinateAxisType", "GeoZ"));
        if (!axisName.equals(dim.getName())) {
            v.addAttribute(new Attribute("_CoordinateAliasForDimension", dim.getName()));
        }
        final Variable etaVar = ds.findVariable(fromWhere);
        if (etaVar == null) {
            return this.makeFakeCoordAxis(ds, axisName, dim);
        }
        final int n = etaVar.getShape(1);
        final int[] origin = { 0, 0 };
        final int[] shape = { 1, n };
        try {
            final Array array = etaVar.read(origin, shape);
            final ArrayDouble.D1 newArray = new ArrayDouble.D1(n);
            final IndexIterator it = array.getIndexIterator();
            int count = 0;
            while (it.hasNext()) {
                final double d = it.getDoubleNext();
                newArray.set(count++, d);
            }
            v.setCachedData(newArray, true);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return v;
    }
    
    private CoordinateAxis makeFakeCoordAxis(final NetcdfDataset ds, final String axisName, final Dimension dim) {
        if (dim == null) {
            return null;
        }
        final CoordinateAxis v = new CoordinateAxis1D(ds, null, axisName, DataType.SHORT, dim.getName(), "", "synthesized coordinate: only an index");
        v.addAttribute(new Attribute("_CoordinateAxisType", "GeoZ"));
        if (!axisName.equals(dim.getName())) {
            v.addAttribute(new Attribute("_CoordinateAliasForDimension", dim.getName()));
        }
        ds.setValues(v, dim.getLength(), 0.0, 1.0);
        return v;
    }
    
    private CoordinateAxis makeTimeCoordAxis(final NetcdfDataset ds, final String axisName, final Dimension dim) {
        if (dim == null) {
            return null;
        }
        final int nt = dim.getLength();
        final Variable timeV = ds.findVariable("Times");
        if (timeV == null) {
            return null;
        }
        Array timeData;
        try {
            timeData = timeV.read();
        }
        catch (IOException ioe) {
            return null;
        }
        final ArrayDouble.D1 values = new ArrayDouble.D1(nt);
        int count = 0;
        if (timeData instanceof ArrayChar) {
            final ArrayChar.StringIterator iter = ((ArrayChar)timeData).getStringIterator();
            while (iter.hasNext()) {
                final String dateS = iter.next();
                try {
                    final Date d = WRFConvention.dateFormat.parse(dateS);
                    values.set(count++, d.getTime() / 1000.0);
                }
                catch (ParseException e) {
                    this.parseInfo.format("ERROR: cant parse Time string = <%s> err= %s\n", dateS, e.getMessage());
                    final String startAtt = ds.findAttValueIgnoreCase(null, "START_DATE", null);
                    if (nt != 1 || null == startAtt) {
                        continue;
                    }
                    try {
                        final Date d2 = WRFConvention.dateFormat.parse(startAtt);
                        values.set(0, d2.getTime() / 1000.0);
                    }
                    catch (ParseException e2) {
                        this.parseInfo.format("ERROR: cant parse global attribute START_DATE = <%s> err=%s\n", startAtt, e2.getMessage());
                    }
                }
            }
        }
        else {
            final IndexIterator iter2 = timeData.getIndexIterator();
            while (iter2.hasNext()) {
                final String dateS = (String)iter2.next();
                try {
                    final Date d = WRFConvention.dateFormat.parse(dateS);
                    values.set(count++, d.getTime() / 1000.0);
                }
                catch (ParseException e) {
                    this.parseInfo.format("ERROR: cant parse Time string = %s\n", dateS);
                }
            }
        }
        final CoordinateAxis v = new CoordinateAxis1D(ds, null, axisName, DataType.DOUBLE, dim.getName(), "secs since 1970-01-01 00:00:00", "synthesized time coordinate from Times(time)");
        v.addAttribute(new Attribute("_CoordinateAxisType", "Time"));
        if (!axisName.equals(dim.getName())) {
            v.addAttribute(new Attribute("_CoordinateAliasForDimension", dim.getName()));
        }
        v.setCachedData(values, true);
        return v;
    }
    
    private VariableDS makeSoilDepthCoordAxis(final NetcdfDataset ds, final String coordVarName) {
        final Variable coordVar = ds.findVariable(coordVarName);
        if (null == coordVar) {
            return null;
        }
        Dimension soilDim = null;
        final List<Dimension> dims = coordVar.getDimensions();
        for (final Dimension d : dims) {
            if (d.getName().startsWith("soil_layers")) {
                soilDim = d;
            }
        }
        if (null == soilDim) {
            return null;
        }
        if (coordVar.getRank() == 1) {
            coordVar.addAttribute(new Attribute("_CoordinateAxisType", "GeoZ"));
            if (!coordVarName.equals(soilDim.getName())) {
                coordVar.addAttribute(new Attribute("_CoordinateAliasForDimension", soilDim.getName()));
            }
            return (VariableDS)coordVar;
        }
        final String units = ds.findAttValueIgnoreCase(coordVar, "units", "");
        final CoordinateAxis v = new CoordinateAxis1D(ds, null, "soilDepth", DataType.DOUBLE, soilDim.getName(), units, "soil depth");
        v.addAttribute(new Attribute("_CoordinateAxisType", "GeoZ"));
        v.addAttribute(new Attribute("units", "units"));
        if (!v.getShortName().equals(soilDim.getName())) {
            v.addAttribute(new Attribute("_CoordinateAliasForDimension", soilDim.getName()));
        }
        final int n = coordVar.getShape(1);
        final int[] origin = { 0, 0 };
        final int[] shape = { 1, n };
        try {
            final Array array = coordVar.read(origin, shape);
            final ArrayDouble.D1 newArray = new ArrayDouble.D1(n);
            final IndexIterator it = array.getIndexIterator();
            int count = 0;
            while (it.hasNext()) {
                final double d2 = it.getDoubleNext();
                newArray.set(count++, d2);
            }
            v.setCachedData(newArray, true);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return v;
    }
    
    private double findAttributeDouble(final NetcdfDataset ds, final String attname) {
        final Attribute att = ds.findGlobalAttributeIgnoreCase(attname);
        if (att == null) {
            return Double.NaN;
        }
        return att.getNumericValue().doubleValue();
    }
    
    @Override
    protected void assignCoordinateTransforms(final NetcdfDataset ncDataset) {
        super.assignCoordinateTransforms(ncDataset);
        final List<CoordinateSystem> csys = ncDataset.getCoordinateSystems();
        for (final CoordinateSystem cs : csys) {
            if (cs.getZaxis() != null) {
                final String units = cs.getZaxis().getUnitsString();
                if (units != null && units.trim().length() != 0) {
                    continue;
                }
                final VerticalCT vct = this.makeWRFEtaVerticalCoordinateTransform(ncDataset, cs);
                if (vct != null) {
                    cs.addCoordinateTransform(vct);
                }
                this.parseInfo.format("***Added WRFEta verticalCoordinateTransform to %s\n", cs.getName());
            }
        }
    }
    
    private VerticalCT makeWRFEtaVerticalCoordinateTransform(final NetcdfDataset ds, final CoordinateSystem cs) {
        if (null == ds.findVariable("PH") || null == ds.findVariable("PHB") || null == ds.findVariable("P") || null == ds.findVariable("PB")) {
            return null;
        }
        final WRFEtaTransformBuilder builder = new WRFEtaTransformBuilder(cs);
        return (VerticalCT)builder.makeCoordinateTransform(ds, null);
    }
    
    public static void main(final String[] args) throws IOException, InvalidRangeException {
        final NetcdfFile ncd = NetcdfDataset.openFile("R:/testdata/wrf/WRFOU~C@", null);
        final Variable glat = ncd.findVariable("GLAT");
        final Array glatData = glat.read();
        IndexIterator ii = glatData.getIndexIterator();
        while (ii.hasNext()) {
            ii.setDoubleCurrent(Math.toDegrees(ii.getDoubleNext()));
        }
        NCdump.printArray(glatData, "GLAT", System.out, null);
        final Variable glon = ncd.findVariable("GLON");
        final Array glonData = glon.read();
        ii = glonData.getIndexIterator();
        while (ii.hasNext()) {
            ii.setDoubleCurrent(Math.toDegrees(ii.getDoubleNext()));
        }
        NCdump.printArray(glonData, "GLON", System.out, null);
        final Index index = glatData.getIndex();
        final Index index2 = glatData.getIndex();
        final int[] vshape = glatData.getShape();
        final int ny = vshape[1];
        final int nx = vshape[2];
        final ArrayDouble.D1 diff_y = (ArrayDouble.D1)Array.factory(DataType.DOUBLE, new int[] { ny });
        final ArrayDouble.D1 diff_x = (ArrayDouble.D1)Array.factory(DataType.DOUBLE, new int[] { nx });
        for (int y = 0; y < ny - 1; ++y) {
            final double val = glatData.getDouble(index.set(0, y, 0)) - glatData.getDouble(index2.set(0, y + 1, 0));
            diff_y.set(y, val);
        }
        for (int x = 0; x < nx - 1; ++x) {
            final double val = glatData.getDouble(index.set(0, 0, x)) - glatData.getDouble(index2.set(0, 0, x + 1));
            diff_x.set(x, val);
        }
        NCdump.printArray(diff_y, "diff_y", System.out, null);
        NCdump.printArray(diff_x, "diff_x", System.out, null);
        ncd.close();
    }
    
    static {
        (WRFConvention.dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss")).setTimeZone(TimeZone.getTimeZone("GMT"));
    }
}
