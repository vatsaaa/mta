// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.conv;

import java.io.IOException;
import ucar.nc2.Attribute;
import ucar.nc2.constants.AxisType;
import java.util.StringTokenizer;
import ucar.nc2.Variable;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dataset.CoordSysBuilder;

public class MADISStation extends CoordSysBuilder
{
    public MADISStation() {
        this.conventionName = "MADIS_Station_1.0";
    }
    
    @Override
    public void augmentDataset(final NetcdfDataset ds, final CancelTask cancelTask) throws IOException {
        final String timeVars = ds.findAttValueIgnoreCase(null, "timeVariables", "");
        StringTokenizer stoker = new StringTokenizer(timeVars, ", ");
        while (stoker.hasMoreTokens()) {
            final String vname = stoker.nextToken();
            final Variable v = ds.findVariable(vname);
            if (v != null) {
                v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Time.toString()));
            }
            else {
                this.parseInfo.format(" cant find time variable %s\n", vname);
            }
        }
        final String locVars = ds.findAttValueIgnoreCase(null, "stationLocationVariables", "");
        stoker = new StringTokenizer(locVars, ", ");
        int count = 0;
        while (stoker.hasMoreTokens()) {
            final String vname2 = stoker.nextToken();
            final Variable v2 = ds.findVariable(vname2);
            if (v2 != null) {
                final AxisType atype = (count == 0) ? AxisType.Lat : ((count == 1) ? AxisType.Lon : AxisType.Height);
                v2.addAttribute(new Attribute("_CoordinateAxisType", atype.toString()));
            }
            else {
                this.parseInfo.format(" cant find time variable %s\n", vname2);
            }
            ++count;
        }
    }
}
