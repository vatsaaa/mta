// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.conv;

import ucar.nc2.constants.AxisType;
import ucar.nc2.dataset.VariableEnhanced;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.Variable;
import ucar.nc2.NetcdfFile;
import ucar.nc2.dataset.CoordSysBuilder;

public class Jason2Convention extends CoordSysBuilder
{
    public static boolean isMine(final NetcdfFile ncfile) {
        if (null == ncfile.findDimension("time")) {
            return false;
        }
        final String center = ncfile.findAttValueIgnoreCase(null, "processing_center", null);
        if (center != null && center.equals("ESPC")) {
            final String mission = ncfile.findAttValueIgnoreCase(null, "mission_name", null);
            return mission != null && mission.equals("OSTM/Jason-2");
        }
        return false;
    }
    
    public Jason2Convention() {
        this.conventionName = "Jason2";
    }
    
    @Override
    protected AxisType getAxisType(final NetcdfDataset ncDataset, final VariableEnhanced v) {
        final String name = v.getShortName();
        if (name.equals("time")) {
            return AxisType.Time;
        }
        if (name.equals("lat")) {
            return AxisType.Lat;
        }
        if (name.equals("lon")) {
            return AxisType.Lon;
        }
        if (name.equals("alt")) {
            return AxisType.Height;
        }
        return null;
    }
}
