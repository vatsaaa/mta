// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.conv;

import ucar.nc2.Variable;
import ucar.nc2.units.SimpleUnit;
import ucar.nc2.constants.AxisType;
import ucar.nc2.dataset.VariableEnhanced;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dataset.CoordSysBuilder;

public class COARDSConvention extends CoordSysBuilder
{
    protected boolean checkForMeter;
    
    public COARDSConvention() {
        this.checkForMeter = true;
        this.conventionName = "COARDS";
    }
    
    @Override
    protected AxisType getAxisType(final NetcdfDataset ncDataset, final VariableEnhanced v) {
        String unit = v.getUnitsString();
        if (unit == null) {
            return null;
        }
        unit = unit.trim();
        if (unit.equalsIgnoreCase("degrees_east") || unit.equalsIgnoreCase("degrees_E") || unit.equalsIgnoreCase("degreesE") || unit.equalsIgnoreCase("degree_east") || unit.equalsIgnoreCase("degree_E") || unit.equalsIgnoreCase("degreeE")) {
            return AxisType.Lon;
        }
        if (unit.equalsIgnoreCase("degrees_north") || unit.equalsIgnoreCase("degrees_N") || unit.equalsIgnoreCase("degreesN") || unit.equalsIgnoreCase("degree_north") || unit.equalsIgnoreCase("degree_N") || unit.equalsIgnoreCase("degreeN")) {
            return AxisType.Lat;
        }
        if (SimpleUnit.isDateUnit(unit)) {
            return AxisType.Time;
        }
        if (SimpleUnit.isCompatible("mbar", unit)) {
            return AxisType.Pressure;
        }
        if (unit.equalsIgnoreCase("level") || unit.equalsIgnoreCase("layer") || unit.equalsIgnoreCase("sigma_level")) {
            return AxisType.GeoZ;
        }
        final String positive = ncDataset.findAttValueIgnoreCase((Variable)v, "positive", null);
        if (positive == null) {
            return null;
        }
        if (SimpleUnit.isCompatible("m", unit)) {
            return AxisType.Height;
        }
        return AxisType.GeoZ;
    }
}
