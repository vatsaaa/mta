// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.conv;

import ucar.nc2.constants.AxisType;
import ucar.nc2.dataset.VariableEnhanced;
import java.io.IOException;
import ucar.nc2.Group;
import java.util.Date;
import ucar.nc2.Attribute;
import java.text.ParseException;
import ucar.nc2.units.DateFormatter;
import java.text.SimpleDateFormat;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.Variable;
import ucar.nc2.NetcdfFile;
import ucar.nc2.dataset.CoordSysBuilder;

public class Suomi extends CoordSysBuilder
{
    public static boolean isMine(final NetcdfFile ncfile) {
        final Variable v = ncfile.findVariable("time_offset");
        if (v == null || !v.isCoordinateVariable()) {
            return false;
        }
        final String desc = v.getDescription();
        return desc != null && desc.equals("Time delta from start_time") && null != ncfile.findGlobalAttribute("start_date") && null != ncfile.findGlobalAttribute("start_time");
    }
    
    public Suomi() {
        this.conventionName = "Suomi";
    }
    
    @Override
    public void augmentDataset(final NetcdfDataset ds, final CancelTask cancelTask) throws IOException {
        final String start_date = ds.findAttValueIgnoreCase(null, "start_date", null);
        final SimpleDateFormat df = new SimpleDateFormat("yyyy.DDD.HH.mm.ss");
        final DateFormatter dfo = new DateFormatter();
        Date start = null;
        try {
            start = df.parse(start_date);
        }
        catch (ParseException e) {
            throw new RuntimeException("Cant read start_date=" + start_date);
        }
        final Variable v = ds.findVariable("time_offset");
        v.addAttribute(new Attribute("units", "seconds since " + dfo.toDateTimeString(start)));
        final Group root = ds.getRootGroup();
        root.addAttribute(new Attribute("Convention", "Suomi-Station-CDM"));
        ds.finish();
    }
    
    @Override
    protected AxisType getAxisType(final NetcdfDataset ncDataset, final VariableEnhanced v) {
        final String name = v.getShortName();
        if (name.equals("time_offset")) {
            return AxisType.Time;
        }
        if (name.equals("lat")) {
            return AxisType.Lat;
        }
        if (name.equals("lon")) {
            return AxisType.Lon;
        }
        if (name.equals("height")) {
            return AxisType.Height;
        }
        return null;
    }
}
