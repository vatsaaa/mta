// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.conv;

import java.io.IOException;
import java.util.Date;
import ucar.ma2.Array;
import ucar.ma2.ArrayDouble;
import ucar.unidata.util.DateUtil;
import ucar.nc2.Attribute;
import ucar.nc2.ncml.NcMLReader;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.Variable;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFile;

public class CEDRICRadarConvention extends CF1Convention
{
    public static boolean isMine(final NetcdfFile ncfile) {
        final Dimension s = ncfile.findDimension("cedric_general_scaling_factor");
        final Variable v = ncfile.findVariable("cedric_run_date");
        return v != null && s != null;
    }
    
    public CEDRICRadarConvention() {
        this.conventionName = "CEDRICRadar";
    }
    
    @Override
    public void augmentDataset(final NetcdfDataset ncDataset, final CancelTask cancelTask) throws IOException {
        NcMLReader.wrapNcMLresource(ncDataset, "resources/nj22/coords/CEDRICRadar.ncml", cancelTask);
        final Variable lat = ncDataset.findVariable("radar_latitude");
        final Variable lon = ncDataset.findVariable("radar_longitude");
        final float latv = (float)lat.readScalarDouble();
        final float lonv = (float)lon.readScalarDouble();
        final Variable pv = ncDataset.findVariable("Projection");
        pv.addAttribute(new Attribute("longitude_of_projection_origin", lonv));
        pv.addAttribute(new Attribute("latitude_of_projection_origin", latv));
        final Variable sdate = ncDataset.findVariable("start_date");
        final Variable stime = ncDataset.findVariable("start_time");
        final Variable tvar = ncDataset.findVariable("time");
        final String dateStr = sdate.readScalarString();
        final String timeStr = stime.readScalarString();
        Date dt = null;
        try {
            dt = DateUtil.parse(dateStr + " " + timeStr);
        }
        catch (Exception ex) {}
        final int nt = 1;
        final ArrayDouble.D1 data = new ArrayDouble.D1(nt);
        data.setDouble(0, (double)(dt.getTime() / 1000L));
        tvar.setCachedData(data, false);
        super.augmentDataset(ncDataset, cancelTask);
    }
}
