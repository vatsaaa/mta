// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.conv;

import java.io.IOException;
import ucar.nc2.ncml.NcMLReader;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.Variable;
import ucar.nc2.NetcdfFile;
import ucar.nc2.dataset.CoordSysBuilder;

public class ATDRadarConvention extends CoordSysBuilder
{
    public static boolean isMine(final NetcdfFile ncfile) {
        final String s = ncfile.findAttValueIgnoreCase(null, "sensor_name", "none");
        return s.equalsIgnoreCase("CRAFT/NEXRAD");
    }
    
    public ATDRadarConvention() {
        this.conventionName = "ATDRadar";
    }
    
    @Override
    public void augmentDataset(final NetcdfDataset ncDataset, final CancelTask cancelTask) throws IOException {
        NcMLReader.wrapNcMLresource(ncDataset, "resources/nj22/coords/ATDRadar.ncml", cancelTask);
    }
}
