// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.conv;

import ucar.nc2.Attribute;
import java.util.StringTokenizer;
import java.util.Iterator;
import java.util.List;
import java.io.IOException;
import ucar.nc2.units.SimpleUnit;
import ucar.nc2.dataset.VariableEnhanced;
import ucar.nc2.Variable;
import ucar.nc2.constants.AxisType;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dataset.CoordSysBuilder;

public class UnidataObsConvention extends CoordSysBuilder
{
    public UnidataObsConvention() {
        this.conventionName = "Unidata Observation Dataset v1.0";
    }
    
    @Override
    public void augmentDataset(final NetcdfDataset ds, final CancelTask cancelTask) throws IOException {
        if (!this.hasAxisType(ds, AxisType.Lat) && !this.addAxisType(ds, "latitude", AxisType.Lat)) {
            final String vname = ds.findAttValueIgnoreCase(null, "latitude_coordinate", null);
            if (!this.addAxisType(ds, vname, AxisType.Lat)) {
                final Variable v = this.hasUnits(ds, "degrees_north,degrees_N,degreesN,degree_north,degree_N,degreeN");
                if (v != null) {
                    this.addAxisType(v, AxisType.Lat);
                }
            }
        }
        if (!this.hasAxisType(ds, AxisType.Lon) && !this.addAxisType(ds, "longitude", AxisType.Lon)) {
            final String vname = ds.findAttValueIgnoreCase(null, "longitude_coordinate", null);
            if (!this.addAxisType(ds, vname, AxisType.Lon)) {
                final Variable v = this.hasUnits(ds, "degrees_east,degrees_E,degreesE,degree_east,degree_E,degreeE");
                if (v != null) {
                    this.addAxisType(v, AxisType.Lon);
                }
            }
        }
        if (!this.hasAxisType(ds, AxisType.Height) && !this.addAxisType(ds, "altitude", AxisType.Height) && !this.addAxisType(ds, "depth", AxisType.Height)) {
            final String vname = ds.findAttValueIgnoreCase(null, "altitude_coordinate", null);
            if (!this.addAxisType(ds, vname, AxisType.Height)) {
                for (int i = 0; i < ds.getVariables().size(); ++i) {
                    final VariableEnhanced ve = (VariableEnhanced)ds.getVariables().get(i);
                    final String positive = ds.findAttValueIgnoreCase((Variable)ve, "positive", null);
                    if (positive != null) {
                        this.addAxisType((Variable)ve, AxisType.Height);
                        break;
                    }
                }
            }
        }
        if (!this.hasAxisType(ds, AxisType.Time) && !this.addAxisType(ds, "time", AxisType.Time)) {
            final String vname = ds.findAttValueIgnoreCase(null, "time_coordinate", null);
            if (!this.addAxisType(ds, vname, AxisType.Time)) {
                for (int i = 0; i < ds.getVariables().size(); ++i) {
                    final VariableEnhanced ve = (VariableEnhanced)ds.getVariables().get(i);
                    final String unit = ve.getUnitsString();
                    if (unit != null) {
                        if (SimpleUnit.isDateUnit(unit)) {
                            this.addAxisType((Variable)ve, AxisType.Time);
                            break;
                        }
                    }
                }
            }
        }
    }
    
    private boolean hasAxisType(final NetcdfDataset ds, final AxisType a) {
        final List<Variable> varList = ds.getVariables();
        for (final Variable v : varList) {
            final String axisType = ds.findAttValueIgnoreCase(v, "CoordinateAxisType", null);
            if (axisType != null && axisType.equals(a.toString())) {
                return true;
            }
        }
        return false;
    }
    
    private Variable hasUnits(final NetcdfDataset ds, final String unitList) {
        final List<Variable> varList = ds.getVariables();
        final StringTokenizer stoker = new StringTokenizer(unitList, ",");
        while (stoker.hasMoreTokens()) {
            final String unit = stoker.nextToken();
            for (final Variable ve : varList) {
                final String hasUnit = ve.getUnitsString();
                if (hasUnit == null) {
                    continue;
                }
                if (hasUnit.equalsIgnoreCase(unit)) {
                    return ve;
                }
            }
        }
        return null;
    }
    
    private boolean addAxisType(final NetcdfDataset ds, final String vname, final AxisType a) {
        if (vname == null) {
            return false;
        }
        final Variable v = ds.findVariable(vname);
        if (v == null) {
            return false;
        }
        this.addAxisType(v, a);
        return true;
    }
    
    private void addAxisType(final Variable v, final AxisType a) {
        v.addAttribute(new Attribute("_CoordinateAxisType", a.toString()));
    }
}
