// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.conv;

import ucar.nc2.dataset.transform.WRFEtaTransformBuilder;
import ucar.nc2.dataset.VerticalCT;
import java.util.Iterator;
import java.util.List;
import ucar.nc2.dataset.CoordinateSystem;
import ucar.ma2.Index;
import ucar.nc2.dataset.CoordinateAxis1D;
import ucar.ma2.DataType;
import ucar.ma2.Array;
import ucar.nc2.dataset.CoordinateAxis;
import ucar.nc2.dataset.VariableEnhanced;
import java.io.IOException;
import ucar.unidata.geoloc.ProjectionPoint;
import ucar.unidata.geoloc.ProjectionImpl;
import ucar.nc2.constants.AxisType;
import ucar.nc2.dataset.VariableDS;
import ucar.nc2.Group;
import ucar.nc2.Attribute;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.ma2.ArrayFloat;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.ProjectionPointImpl;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.Projection;
import ucar.unidata.geoloc.projection.LambertConformal;
import ucar.nc2.units.SimpleUnit;
import ucar.nc2.Variable;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dataset.ProjectionCT;
import ucar.nc2.dataset.CoordSysBuilder;

public class ADASConvention extends CoordSysBuilder
{
    private ProjectionCT projCT;
    private boolean debugProj;
    
    public ADASConvention() {
        this.projCT = null;
        this.debugProj = false;
        this.conventionName = "ARPS/ADAS";
    }
    
    @Override
    public void augmentDataset(final NetcdfDataset ds, final CancelTask cancelTask) throws IOException {
        if (null != ds.findVariable("x")) {
            return;
        }
        final Attribute att = ds.findGlobalAttribute("MAPPROJ");
        final int projType = att.getNumericValue().intValue();
        double lat1 = this.findAttributeDouble(ds, "TRUELAT1", Double.NaN);
        double lat2 = this.findAttributeDouble(ds, "TRUELAT2", Double.NaN);
        double lat_origin = lat1;
        double lon_origin = this.findAttributeDouble(ds, "TRUELON", Double.NaN);
        double false_easting = 0.0;
        double false_northing = 0.0;
        String projName = ds.findAttValueIgnoreCase(null, "grid_mapping_name", null);
        if (projName != null) {
            projName = projName.trim();
            lat_origin = this.findAttributeDouble(ds, "latitude_of_projection_origin", Double.NaN);
            lon_origin = this.findAttributeDouble(ds, "longitude_of_central_meridian", Double.NaN);
            false_easting = this.findAttributeDouble(ds, "false_easting", 0.0);
            false_northing = this.findAttributeDouble(ds, "false_northing", 0.0);
            final Attribute att2 = ds.findGlobalAttributeIgnoreCase("standard_parallel");
            if (att2 != null) {
                lat1 = att2.getNumericValue().doubleValue();
                lat2 = ((att2.getLength() > 1) ? att2.getNumericValue(1).doubleValue() : lat1);
            }
        }
        else if (projType == 2) {
            projName = "lambert_conformal_conic";
        }
        final Variable coord_var = ds.findVariable("x_stag");
        if (!Double.isNaN(false_easting) || !Double.isNaN(false_northing)) {
            final String units = ds.findAttValueIgnoreCase(coord_var, "units", null);
            double scalef = 1.0;
            try {
                scalef = SimpleUnit.getConversionFactor(units, "km");
            }
            catch (IllegalArgumentException e) {
                ADASConvention.log.error(units + " not convertible to km");
            }
            false_easting *= scalef;
            false_northing *= scalef;
        }
        ProjectionImpl proj = null;
        if (projName != null && projName.equalsIgnoreCase("lambert_conformal_conic")) {
            proj = new LambertConformal(lat_origin, lon_origin, lat1, lat2, false_easting, false_northing);
            this.projCT = new ProjectionCT("Projection", "FGDC", proj);
            if (false_easting == 0.0) {
                this.calcCenterPoints(ds, proj);
            }
        }
        else {
            this.parseInfo.format("ERROR: unknown projection type = %s\n", projName);
        }
        if (this.debugProj) {
            if (proj != null) {
                System.out.println(" using LC " + proj.paramsToString());
            }
            final double lat_check = this.findAttributeDouble(ds, "CTRLAT", Double.NaN);
            final double lon_check = this.findAttributeDouble(ds, "CTRLON", Double.NaN);
            LatLonPointImpl lpt0 = new LatLonPointImpl(lat_check, lon_check);
            ProjectionPoint ppt0 = proj.latLonToProj(lpt0, new ProjectionPointImpl());
            System.out.println("CTR lpt0= " + lpt0 + " ppt0=" + ppt0);
            final Variable xstag = ds.findVariable("x_stag");
            final ArrayFloat.D1 xstagData = (ArrayFloat.D1)xstag.read();
            final float center_x = xstagData.get((int)xstag.getSize() - 1);
            final Variable ystag = ds.findVariable("y_stag");
            final ArrayFloat.D1 ystagData = (ArrayFloat.D1)ystag.read();
            final float center_y = ystagData.get((int)ystag.getSize() - 1);
            System.out.println("CTR should be x,y= " + center_x / 2000.0f + ", " + center_y / 2000.0f);
            lpt0 = new LatLonPointImpl(lat_origin, lon_origin);
            ppt0 = proj.latLonToProj(lpt0, new ProjectionPointImpl());
            System.out.println("ORIGIN lpt0= " + lpt0 + " ppt0=" + ppt0);
            lpt0 = new LatLonPointImpl(lat_origin, lon_origin);
            ppt0 = proj.latLonToProj(lpt0, new ProjectionPointImpl());
            System.out.println("TRUE ORIGIN lpt0= " + lpt0 + " ppt0=" + ppt0);
        }
        if (this.projCT != null) {
            final VariableDS v = this.makeCoordinateTransformVariable(ds, this.projCT);
            v.addAttribute(new Attribute("_CoordinateAxisTypes", "GeoX GeoY"));
            ds.addVariable(null, v);
        }
        if (ds.findVariable("x_stag") != null) {
            ds.addCoordinateAxis(this.makeCoordAxis(ds, "x"));
        }
        if (ds.findVariable("y_stag") != null) {
            ds.addCoordinateAxis(this.makeCoordAxis(ds, "y"));
        }
        if (ds.findVariable("z_stag") != null) {
            ds.addCoordinateAxis(this.makeCoordAxis(ds, "z"));
        }
        final Variable zsoil = ds.findVariable("ZPSOIL");
        if (zsoil != null) {
            zsoil.addAttribute(new Attribute("_CoordinateAxisType", AxisType.GeoZ.toString()));
        }
        ds.finish();
    }
    
    private void calcCenterPoints(final NetcdfDataset ds, final Projection proj) throws IOException {
        final double lat_check = this.findAttributeDouble(ds, "CTRLAT", Double.NaN);
        final double lon_check = this.findAttributeDouble(ds, "CTRLON", Double.NaN);
        final LatLonPointImpl lpt0 = new LatLonPointImpl(lat_check, lon_check);
        final ProjectionPoint ppt0 = proj.latLonToProj(lpt0, new ProjectionPointImpl());
        System.out.println("CTR lpt0= " + lpt0 + " ppt0=" + ppt0);
        final Variable xstag = ds.findVariable("x_stag");
        final int nxpts = (int)xstag.getSize();
        final ArrayFloat.D1 xstagData = (ArrayFloat.D1)xstag.read();
        final float center_x = xstagData.get(nxpts - 1);
        final double false_easting = center_x / 2000.0f - ppt0.getX() * 1000.0;
        System.out.println("false_easting= " + false_easting);
        final Variable ystag = ds.findVariable("y_stag");
        final int nypts = (int)ystag.getSize();
        final ArrayFloat.D1 ystagData = (ArrayFloat.D1)ystag.read();
        final float center_y = ystagData.get(nypts - 1);
        final double false_northing = center_y / 2000.0f - ppt0.getY() * 1000.0;
        System.out.println("false_northing= " + false_northing);
        final double dx = this.findAttributeDouble(ds, "DX", Double.NaN);
        final double dy = this.findAttributeDouble(ds, "DY", Double.NaN);
        final double w = dx * (nxpts - 1);
        final double h = dy * (nypts - 1);
        final double startx = ppt0.getX() * 1000.0 - w / 2.0;
        final double starty = ppt0.getY() * 1000.0 - h / 2.0;
        ds.setValues(xstag, nxpts, startx, dx);
        ds.setValues(ystag, nypts, starty, dy);
    }
    
    @Override
    protected void makeCoordinateTransforms(final NetcdfDataset ds) {
        if (this.projCT != null) {
            final VarProcess vp = this.findVarProcess(this.projCT.getName());
            vp.isCoordinateTransform = true;
            vp.ct = this.projCT;
        }
        super.makeCoordinateTransforms(ds);
    }
    
    @Override
    protected AxisType getAxisType(final NetcdfDataset ds, final VariableEnhanced ve) {
        final Variable v = (Variable)ve;
        final String vname = v.getName();
        if (vname.equalsIgnoreCase("x") || vname.equalsIgnoreCase("x_stag")) {
            return AxisType.GeoX;
        }
        if (vname.equalsIgnoreCase("lon")) {
            return AxisType.Lon;
        }
        if (vname.equalsIgnoreCase("y") || vname.equalsIgnoreCase("y_stag")) {
            return AxisType.GeoY;
        }
        if (vname.equalsIgnoreCase("lat")) {
            return AxisType.Lat;
        }
        if (vname.equalsIgnoreCase("z") || vname.equalsIgnoreCase("z_stag")) {
            return AxisType.GeoZ;
        }
        if (vname.equalsIgnoreCase("Z")) {
            return AxisType.Height;
        }
        if (vname.equalsIgnoreCase("time")) {
            return AxisType.Time;
        }
        final String unit = ve.getUnitsString();
        if (unit != null) {
            if (SimpleUnit.isCompatible("millibar", unit)) {
                return AxisType.Pressure;
            }
            if (SimpleUnit.isCompatible("m", unit)) {
                return AxisType.Height;
            }
        }
        return null;
    }
    
    public String getZisPositive(final CoordinateAxis v) {
        return "down";
    }
    
    private CoordinateAxis makeCoordAxis(final NetcdfDataset ds, final String axisName) throws IOException {
        final Variable stagV = ds.findVariable(axisName + "_stag");
        final Array data_stag = stagV.read();
        final int n = (int)data_stag.getSize() - 1;
        final Array data = Array.factory(data_stag.getElementType(), new int[] { n });
        final Index stagIndex = data_stag.getIndex();
        final Index dataIndex = data.getIndex();
        for (int i = 0; i < n; ++i) {
            final double val = data_stag.getDouble(stagIndex.set(i)) + data_stag.getDouble(stagIndex.set(i + 1));
            data.setDouble(dataIndex.set(i), 0.5 * val);
        }
        final DataType dtype = DataType.getType(data.getElementType());
        final String units = ds.findAttValueIgnoreCase(stagV, "units", "m");
        final CoordinateAxis v = new CoordinateAxis1D(ds, null, axisName, dtype, axisName, units, "synthesized non-staggered " + axisName + " coordinate");
        v.setCachedData(data, true);
        return v;
    }
    
    private double findAttributeDouble(final NetcdfDataset ds, final String attname, final double defValue) {
        final Attribute att = ds.findGlobalAttributeIgnoreCase(attname);
        if (att == null) {
            return defValue;
        }
        return att.getNumericValue().doubleValue();
    }
    
    @Override
    protected void assignCoordinateTransforms(final NetcdfDataset ncDataset) {
        super.assignCoordinateTransforms(ncDataset);
        final List<CoordinateSystem> csys = ncDataset.getCoordinateSystems();
        for (final CoordinateSystem cs : csys) {
            if (cs.getZaxis() != null) {
                final String units = cs.getZaxis().getUnitsString();
                if (units != null && units.trim().length() != 0) {
                    continue;
                }
                final VerticalCT vct = this.makeWRFEtaVerticalCoordinateTransform(ncDataset, cs);
                if (vct == null) {
                    continue;
                }
                cs.addCoordinateTransform(vct);
                this.parseInfo.format("***Added WRFEta verticalCoordinateTransform to %s\n", cs.getName());
            }
        }
    }
    
    private VerticalCT makeWRFEtaVerticalCoordinateTransform(final NetcdfDataset ds, final CoordinateSystem cs) {
        if (null == ds.findVariable("PH") || null == ds.findVariable("PHB") || null == ds.findVariable("P") || null == ds.findVariable("PB")) {
            return null;
        }
        final WRFEtaTransformBuilder builder = new WRFEtaTransformBuilder(cs);
        return (VerticalCT)builder.makeCoordinateTransform(ds, null);
    }
}
