// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.conv;

import java.io.IOException;
import java.util.Iterator;
import ucar.nc2.Variable;
import ucar.nc2.Dimension;
import ucar.nc2.constants.AxisType;
import ucar.nc2.Attribute;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.Group;
import ucar.nc2.NetcdfFile;
import ucar.nc2.dataset.CoordSysBuilder;

public class NppConvention extends CoordSysBuilder
{
    public static boolean isMine(final NetcdfFile ncfile) {
        if (!ncfile.getFileTypeId().equals("HDF5")) {
            return false;
        }
        Group loc = ncfile.findGroup("All_Data/VIIRS-MOD-GEO-TC_All");
        if (loc == null) {
            loc = ncfile.findGroup("All_Data/VIIRS-CLD-AGG-GEO_All");
        }
        return null != loc && null != loc.findVariable("Latitude") && null != loc.findVariable("Longitude");
    }
    
    public NppConvention() {
        this.conventionName = "NPP/NPOESS";
    }
    
    @Override
    public void augmentDataset(final NetcdfDataset ds, final CancelTask cancelTask) throws IOException {
        ds.addAttribute(null, new Attribute("FeatureType", FeatureType.IMAGE.toString()));
        Group loc = ds.findGroup("All_Data/VIIRS-MOD-GEO-TC_All");
        if (loc == null) {
            loc = ds.findGroup("All_Data/VIIRS-CLD-AGG-GEO_All");
        }
        final Variable lat = loc.findVariable("Latitude");
        lat.addAttribute(new Attribute("units", "degrees_north"));
        lat.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lat.toString()));
        final Variable lon = loc.findVariable("Longitude");
        lon.addAttribute(new Attribute("units", "degrees_east"));
        lon.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lon.toString()));
        final int[] shape = lat.getShape();
        assert shape.length == 2;
        final Dimension scan = new Dimension("scan", shape[0]);
        final Dimension xscan = new Dimension("xscan", shape[1]);
        loc.addDimension(scan);
        loc.addDimension(xscan);
        lat.setDimensions("scan xscan");
        lon.setDimensions("scan xscan");
        for (final Variable v : loc.getVariables()) {
            final int[] vs = v.getShape();
            if (vs.length == 2 && vs[0] == shape[0] && vs[1] == shape[1]) {
                v.setDimensions("scan xscan");
                v.addAttribute(new Attribute("_CoordinateAxes", "Latitude Longitude"));
            }
        }
        ds.finish();
    }
}
