// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.conv;

import java.io.IOException;
import ucar.nc2.Dimension;
import java.util.Iterator;
import java.util.List;
import ucar.nc2.Attribute;
import ucar.nc2.Variable;
import ucar.nc2.ncml.NcMLReader;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dataset.CoordSysBuilder;

public class GIEFConvention extends CoordSysBuilder
{
    public GIEFConvention() {
        this.conventionName = "GIEF";
    }
    
    @Override
    public void augmentDataset(final NetcdfDataset ds, final CancelTask cancelTask) throws IOException {
        NcMLReader.wrapNcMLresource(ds, "resources/nj22/coords/GIEF.ncml", cancelTask);
        final Variable timeVar = ds.findVariable("time");
        final String time_units = ds.findAttValueIgnoreCase(null, "time_units", null);
        timeVar.addAttribute(new Attribute("units", time_units));
        final Variable levelVar = ds.findVariable("level");
        final String level_units = ds.findAttValueIgnoreCase(null, "level_units", null);
        final String level_name = ds.findAttValueIgnoreCase(null, "level_name", null);
        levelVar.addAttribute(new Attribute("units", level_units));
        levelVar.addAttribute(new Attribute("long_name", level_name));
        final String unit_name = ds.findAttValueIgnoreCase(null, "unit_name", null);
        final String parameter_name = ds.findAttValueIgnoreCase(null, "parameter_name", null);
        final List<Variable> vlist = ds.getVariables();
        for (final Variable v : vlist) {
            if (v.getRank() > 1) {
                v.addAttribute(new Attribute("units", unit_name));
                v.addAttribute(new Attribute("long_name", v.getName() + " " + parameter_name));
                v.addAttribute(new Attribute("_CoordinateAxes", "time level latitude longitude"));
            }
        }
        final Attribute translation = ds.findGlobalAttributeIgnoreCase("translation");
        final Attribute affine = ds.findGlobalAttributeIgnoreCase("affine_transformation");
        final double startLat = translation.getNumericValue(1).doubleValue();
        final double incrLat = affine.getNumericValue(6).doubleValue();
        final Dimension latDim = ds.findDimension("row");
        final Variable latVar = ds.findVariable("latitude");
        ds.setValues(latVar, latDim.getLength(), startLat, incrLat);
        final double startLon = translation.getNumericValue(0).doubleValue();
        final double incrLon = affine.getNumericValue(3).doubleValue();
        final Dimension lonDim = ds.findDimension("column");
        final Variable lonVar = ds.findVariable("longitude");
        ds.setValues(lonVar, lonDim.getLength(), startLon, incrLon);
    }
}
