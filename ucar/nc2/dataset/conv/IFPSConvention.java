// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.conv;

import ucar.ma2.Index;
import ucar.nc2.Structure;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.ProjectionPointImpl;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.unidata.geoloc.ProjectionImpl;
import ucar.nc2.dataset.ProjectionCT;
import ucar.unidata.geoloc.projection.LambertConformal;
import ucar.nc2.dataset.CoordinateAxis;
import ucar.ma2.Array;
import ucar.nc2.dataset.VariableDS;
import ucar.nc2.dataset.CoordinateAxis1D;
import ucar.nc2.Group;
import ucar.nc2.Dimension;
import ucar.ma2.DataType;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Range;
import java.util.ArrayList;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import ucar.unidata.geoloc.Projection;
import ucar.nc2.Attribute;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;
import ucar.nc2.dataset.CoordSysBuilder;

public class IFPSConvention extends CoordSysBuilder
{
    private Variable projVar;
    
    public static boolean isMine(final NetcdfFile ncfile) {
        final Variable v = ncfile.findVariable("latitude");
        return null != ncfile.findDimension("DIM_0") && null != ncfile.findVariable("longitude") && null != v && null != ncfile.findAttValueIgnoreCase(v, "projectionType", null);
    }
    
    public IFPSConvention() {
        this.projVar = null;
        this.conventionName = "IFPS";
    }
    
    @Override
    public void augmentDataset(final NetcdfDataset ds, final CancelTask cancelTask) throws IOException {
        if (null != ds.findVariable("xCoord")) {
            return;
        }
        this.parseInfo.format("IFPS augmentDataset \n", new Object[0]);
        final Variable lonVar = ds.findVariable("longitude");
        lonVar.addAttribute(new Attribute("units", "degrees_east"));
        lonVar.addAttribute(new Attribute("_CoordinateAxisType", "Lon"));
        final Variable latVar = ds.findVariable("latitude");
        latVar.addAttribute(new Attribute("_CoordinateAxisType", "Lat"));
        latVar.addAttribute(new Attribute("units", "degrees_north"));
        this.projVar = latVar;
        final String projName = ds.findAttValueIgnoreCase(this.projVar, "projectionType", null);
        if (projName.equals("LAMBERT_CONFORMAL")) {
            final Projection proj = this.makeLCProjection(ds);
            this.makeXYcoords(ds, proj, latVar, lonVar);
        }
        final List<Variable> vars = ds.getVariables();
        for (final Variable ncvar : vars) {
            if (!ncvar.getDimension(0).getName().equals("DIM_0") && !ncvar.getName().endsWith("History") && ncvar.getRank() > 2 && !ncvar.getName().startsWith("Tool")) {
                this.createTimeCoordinate(ds, ncvar);
            }
            else {
                if (!ncvar.getName().equals("Topo")) {
                    continue;
                }
                ncvar.addAttribute(new Attribute("long_name", "Topography"));
                ncvar.addAttribute(new Attribute("units", "ft"));
            }
        }
        ds.finish();
    }
    
    private void createTimeCoordinate(final NetcdfDataset ds, final Variable ncVar) {
        final Attribute timesAtt = ncVar.findAttribute("validTimes");
        if (timesAtt == null) {
            return;
        }
        Array timesArray = timesAtt.getValues();
        try {
            final int n = (int)timesArray.getSize();
            final List<Range> list = new ArrayList<Range>();
            list.add(new Range(0, n - 1, 2));
            timesArray = timesArray.section(list);
        }
        catch (InvalidRangeException e) {
            throw new IllegalStateException(e);
        }
        final DataType dtype = DataType.getType(timesArray.getElementType());
        final int nTimesAtt = (int)timesArray.getSize();
        final Dimension dimTime = ncVar.getDimension(0);
        final int nTimesDim = dimTime.getLength();
        if (nTimesDim != nTimesAtt) {
            this.parseInfo.format(" **error ntimes in attribute (%d) doesnt match dimension length (%d) for variable %s\n", nTimesAtt, nTimesDim, ncVar.getName());
            return;
        }
        final String dimName = ncVar.getName() + "_timeCoord";
        final Dimension newDim = new Dimension(dimName, nTimesDim);
        ds.addDimension(null, newDim);
        final String units = "seconds since 1970-1-1 00:00:00";
        final String desc = "time coordinate for " + ncVar.getName();
        final CoordinateAxis1D timeCoord = new CoordinateAxis1D(ds, null, dimName, dtype, dimName, units, desc);
        timeCoord.setCachedData(timesArray, true);
        timeCoord.addAttribute(new Attribute("long_name", desc));
        timeCoord.addAttribute(new Attribute("units", units));
        timeCoord.addAttribute(new Attribute("_CoordinateAxisType", "Time"));
        ds.addCoordinateAxis(timeCoord);
        this.parseInfo.format(" added coordinate variable %s\n", dimName);
        final List<Dimension> dimsList = ncVar.getDimensions();
        dimsList.set(0, newDim);
        ncVar.setDimensions(dimsList);
        ncVar.addAttribute(new Attribute("_CoordinateAxes", dimName + " yCoord xCoord"));
        Attribute att = ncVar.findAttribute("fillValue");
        if (att != null) {
            ncVar.addAttribute(new Attribute("_FillValue", att.getNumericValue()));
        }
        att = ncVar.findAttribute("descriptiveName");
        if (null != att) {
            ncVar.addAttribute(new Attribute("long_name", att.getStringValue()));
        }
    }
    
    protected String getZisPositive(final NetcdfDataset ds, final CoordinateAxis v) {
        return "up";
    }
    
    private Projection makeLCProjection(final NetcdfDataset ds) {
        final Attribute latLonOrigin = this.projVar.findAttributeIgnoreCase("latLonOrigin");
        final double centralLon = latLonOrigin.getNumericValue(0).doubleValue();
        final double centralLat = latLonOrigin.getNumericValue(1).doubleValue();
        final double par1 = this.findAttributeDouble("stdParallelOne");
        final double par2 = this.findAttributeDouble("stdParallelTwo");
        final LambertConformal lc = new LambertConformal(centralLat, centralLon, par1, par2);
        final ProjectionCT ct = new ProjectionCT("lambertConformalProjection", "FGDC", lc);
        final VariableDS ctVar = this.makeCoordinateTransformVariable(ds, ct);
        ctVar.addAttribute(new Attribute("_CoordinateAxes", "xCoord yCoord"));
        ds.addVariable(null, ctVar);
        return lc;
    }
    
    private void makeXYcoords(final NetcdfDataset ds, final Projection proj, final Variable latVar, final Variable lonVar) throws IOException {
        final Array latData = latVar.read();
        final Array lonData = lonVar.read();
        final Dimension y_dim = latVar.getDimension(0);
        final Dimension x_dim = latVar.getDimension(1);
        final Array xData = Array.factory(Float.TYPE, new int[] { x_dim.getLength() });
        final Array yData = Array.factory(Float.TYPE, new int[] { y_dim.getLength() });
        final LatLonPointImpl latlon = new LatLonPointImpl();
        final ProjectionPointImpl pp = new ProjectionPointImpl();
        final Index latlonIndex = latData.getIndex();
        final Index xIndex = xData.getIndex();
        final Index yIndex = yData.getIndex();
        for (int i = 0; i < x_dim.getLength(); ++i) {
            final double lat = latData.getDouble(latlonIndex.set1(i));
            final double lon = lonData.getDouble(latlonIndex);
            latlon.set(lat, lon);
            proj.latLonToProj(latlon, pp);
            xData.setDouble(xIndex.set(i), pp.getX());
        }
        for (int i = 0; i < y_dim.getLength(); ++i) {
            final double lat = latData.getDouble(latlonIndex.set0(i));
            final double lon = lonData.getDouble(latlonIndex);
            latlon.set(lat, lon);
            proj.latLonToProj(latlon, pp);
            yData.setDouble(yIndex.set(i), pp.getY());
        }
        final VariableDS xaxis = new VariableDS(ds, null, null, "xCoord", DataType.FLOAT, x_dim.getName(), "km", "x on projection");
        xaxis.addAttribute(new Attribute("units", "km"));
        xaxis.addAttribute(new Attribute("long_name", "x on projection"));
        xaxis.addAttribute(new Attribute("_CoordinateAxisType", "GeoX"));
        final VariableDS yaxis = new VariableDS(ds, null, null, "yCoord", DataType.FLOAT, y_dim.getName(), "km", "y on projection");
        yaxis.addAttribute(new Attribute("units", "km"));
        yaxis.addAttribute(new Attribute("long_name", "y on projection"));
        yaxis.addAttribute(new Attribute("_CoordinateAxisType", "GeoY"));
        xaxis.setCachedData(xData, true);
        yaxis.setCachedData(yData, true);
        ds.addVariable(null, xaxis);
        ds.addVariable(null, yaxis);
    }
    
    private double findAttributeDouble(final String attname) {
        final Attribute att = this.projVar.findAttributeIgnoreCase(attname);
        return att.getNumericValue().doubleValue();
    }
}
