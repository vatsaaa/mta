// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.conv;

import ucar.nc2.dataset.CoordinateAxis;
import ucar.nc2.Dimension;
import ucar.nc2.dataset.VariableDS;
import ucar.nc2.dataset.CoordinateAxis1D;
import java.util.Iterator;
import ucar.ma2.Array;
import ucar.ma2.DataType;
import ucar.nc2.constants.AxisType;
import ucar.nc2.Group;
import ucar.nc2.Attribute;
import java.io.IOException;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.Variable;
import ucar.nc2.NetcdfFile;
import ucar.nc2.dataset.CoordSysBuilder;

public class NsslRadarMosaicConvention extends CoordSysBuilder
{
    public static boolean isMine(final NetcdfFile ncfile) {
        final String cs = ncfile.findAttValueIgnoreCase(null, "Conventions", null);
        if (cs != null) {
            return false;
        }
        final String s = ncfile.findAttValueIgnoreCase(null, "DataType", null);
        return s != null && (s.equalsIgnoreCase("LatLonGrid") || s.equalsIgnoreCase("LatLonHeightGrid")) && null != ncfile.findGlobalAttribute("Latitude") && null != ncfile.findGlobalAttribute("Longitude") && null != ncfile.findGlobalAttribute("LatGridSpacing") && null != ncfile.findGlobalAttribute("LonGridSpacing") && null != ncfile.findGlobalAttribute("Time") && null != ncfile.findDimension("Lat") && null != ncfile.findDimension("Lon");
    }
    
    public NsslRadarMosaicConvention() {
        this.conventionName = "NSSL National Reflectivity Mosaic";
    }
    
    @Override
    public void augmentDataset(final NetcdfDataset ds, final CancelTask cancelTask) throws IOException {
        if (null != ds.findVariable("Lat")) {
            return;
        }
        final String s = ds.findAttValueIgnoreCase(null, "DataType", null);
        if (s.equalsIgnoreCase("LatLonGrid")) {
            this.augment2D(ds, cancelTask);
        }
        else {
            this.augment3D(ds, cancelTask);
        }
        ds.finish();
    }
    
    private void augment3D(final NetcdfDataset ds, final CancelTask cancelTask) throws IOException {
        ds.addAttribute(null, new Attribute("Conventions", "NSSL National Reflectivity Mosaic"));
        this.addLongName(ds, "mrefl_mosaic", "3-D reflectivity mosaic grid");
        this.addCoordinateAxisType(ds, "Height", AxisType.Height);
        this.addCoordSystem(ds);
        final Variable var = ds.findVariable("mrefl_mosaic");
        assert var != null;
        float scale_factor = Float.NaN;
        Attribute att = var.findAttributeIgnoreCase("Scale");
        if (att != null) {
            scale_factor = att.getNumericValue().floatValue();
            var.addAttribute(new Attribute("scale_factor", 1.0f / scale_factor));
        }
        att = ds.findGlobalAttributeIgnoreCase("MissingData");
        if (null != att) {
            float val = att.getNumericValue().floatValue();
            if (!Float.isNaN(scale_factor)) {
                val *= scale_factor;
            }
            var.addAttribute(new Attribute("missing_value", (short)val));
        }
        final Array missingData = Array.factory(DataType.SHORT.getPrimitiveClassType(), new int[] { 2 }, new short[] { -990, -9990 });
        var.addAttribute(new Attribute("missing_value", missingData));
        var.addAttribute(new Attribute("_CoordinateAxes", "Height Lat Lon"));
    }
    
    private void augment2D(final NetcdfDataset ds, final CancelTask cancelTask) throws IOException {
        ds.addAttribute(null, new Attribute("Conventions", "NSSL National Reflectivity Mosaic"));
        this.addLongName(ds, "cref", "composite reflectivity");
        this.addLongName(ds, "hgt_cref", "height associated with the composite reflectivity");
        this.addLongName(ds, "etp18", "scho top");
        this.addLongName(ds, "shi", "csevere hail index");
        this.addLongName(ds, "posh", "probability of severe hail");
        this.addLongName(ds, "mehs", "maximum estimated hail size");
        this.addLongName(ds, "hsr", "hybrid scan reflectivity");
        this.addLongName(ds, "hsrh", "height associated with the hybrid scan reflectivity");
        this.addLongName(ds, "vil", "vertically integrated liquid");
        this.addLongName(ds, "vilD", "vertically integrated liquid density");
        this.addLongName(ds, "pcp_flag", "Radar precipitation flag");
        this.addLongName(ds, "pcp_type", "Surface precipitation type");
        this.addCoordSystem(ds);
        for (final Variable var : ds.getVariables()) {
            float scale_factor = Float.NaN;
            Attribute att = var.findAttributeIgnoreCase("Scale");
            if (att != null) {
                scale_factor = att.getNumericValue().floatValue();
                var.addAttribute(new Attribute("scale_factor", 1.0f / scale_factor));
            }
            att = var.findAttributeIgnoreCase("MissingData");
            if (null != att) {
                float val = att.getNumericValue().floatValue();
                if (!Float.isNaN(scale_factor)) {
                    val *= scale_factor;
                }
                var.addAttribute(new Attribute("missing_value", (short)val));
            }
        }
        ds.finish();
    }
    
    private void addLongName(final NetcdfDataset ds, final String varName, final String longName) {
        final Variable v = ds.findVariable(varName);
        if (v != null) {
            v.addAttribute(new Attribute("long_name", longName));
        }
    }
    
    private void addCoordinateAxisType(final NetcdfDataset ds, final String varName, final AxisType type) {
        final Variable v = ds.findVariable(varName);
        if (v != null) {
            v.addAttribute(new Attribute("_CoordinateAxisType", type.name()));
        }
    }
    
    private void addCoordSystem(final NetcdfDataset ds) throws IOException {
        final double lat = ds.findGlobalAttributeIgnoreCase("Latitude").getNumericValue().doubleValue();
        final double lon = ds.findGlobalAttributeIgnoreCase("Longitude").getNumericValue().doubleValue();
        final double dlat = ds.findGlobalAttributeIgnoreCase("LatGridSpacing").getNumericValue().doubleValue();
        final double dlon = ds.findGlobalAttributeIgnoreCase("LonGridSpacing").getNumericValue().doubleValue();
        final int time = ds.findGlobalAttributeIgnoreCase("Time").getNumericValue().intValue();
        if (this.debug) {
            System.out.println(ds.getLocation() + " Lat/Lon=" + lat + "/" + lon);
        }
        final int nlat = ds.findDimension("Lat").getLength();
        final int nlon = ds.findDimension("Lon").getLength();
        CoordinateAxis v = new CoordinateAxis1D(ds, null, "Lat", DataType.FLOAT, "Lat", "degrees_north", "latitude coordinate");
        ds.setValues(v, nlat, lat, -dlat);
        v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lat.toString()));
        ds.addCoordinateAxis(v);
        v = new CoordinateAxis1D(ds, null, "Lon", DataType.FLOAT, "Lon", "degrees_east", "longitude coordinate");
        ds.setValues(v, nlon, lon, dlon);
        v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lon.toString()));
        ds.addCoordinateAxis(v);
        ds.addDimension(null, new Dimension("Time", 1));
        v = new CoordinateAxis1D(ds, null, "Time", DataType.INT, "Time", "seconds since 1970-1-1 00:00:00", "time coordinate");
        ds.setValues(v, 1, time, 1.0);
        v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Time.toString()));
        ds.addCoordinateAxis(v);
    }
}
