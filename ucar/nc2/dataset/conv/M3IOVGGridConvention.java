// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.conv;

import java.util.Iterator;
import ucar.nc2.dataset.CoordinateSystem;
import ucar.nc2.units.SimpleUnit;
import ucar.nc2.dataset.VariableEnhanced;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import java.util.GregorianCalendar;
import java.util.SimpleTimeZone;
import ucar.ma2.Array;
import ucar.ma2.ArrayInt;
import java.util.List;
import java.util.ArrayList;
import ucar.nc2.dataset.CoordinateAxis;
import ucar.nc2.Dimension;
import ucar.nc2.constants.AxisType;
import ucar.nc2.dataset.CoordinateAxis1D;
import ucar.ma2.DataType;
import ucar.nc2.dataset.VariableDS;
import ucar.unidata.geoloc.ProjectionImpl;
import ucar.nc2.Variable;
import ucar.nc2.Group;
import ucar.nc2.Attribute;
import ucar.nc2.dataset.ProjectionCT;
import ucar.unidata.geoloc.projection.LambertAzimuthalEqualArea;
import ucar.unidata.geoloc.projection.AlbersEqualArea;
import ucar.unidata.geoloc.projection.Mercator;
import ucar.unidata.geoloc.projection.UtmProjection;
import ucar.unidata.geoloc.projection.Stereographic;
import ucar.unidata.geoloc.projection.TransverseMercator;
import ucar.unidata.geoloc.projection.LambertConformal;
import ucar.unidata.geoloc.projection.LatLonProjection;
import ucar.nc2.util.CancelTask;
import ucar.nc2.NetcdfFile;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.nc2.dataset.CoordSysBuilder;

public class M3IOVGGridConvention extends CoordSysBuilder
{
    private static final int GRDDED3 = 1;
    private static final int IDDATA3 = 3;
    private static final int PTRFLY3 = 8;
    private static final int MXLAYS3 = 100;
    private static final int MXVARS3 = 120;
    private static final int NAMLEN3 = 16;
    private static final int LATGRD3 = 1;
    private static final int LAMGRD3 = 2;
    private static final int MERGRD3 = 3;
    private static final int STEGRD3 = 4;
    private static final int UTMGRD3 = 5;
    private static final int POLGRD3 = 6;
    private static final int EQMGRD3 = 7;
    private static final int TRMGRD3 = 8;
    private static final int ALBGRD3 = 9;
    private static final int LEQGRD3 = 10;
    private static final int VGSGPH3 = 1;
    private static final int VGSGPN3 = 2;
    private static final int VGSIGZ3 = 3;
    private static final int VGPRES3 = 4;
    private static final int VGZVAL3 = 5;
    private static final int VGHVAL3 = 6;
    private static final int IMISS3 = -9999;
    private static final double BADVAL3 = -9.999E36;
    private static final double AMISS3 = -9.0E36;
    private static final double SURFACE_PRESSURE_IN_MB = 1012.5;
    private CoordinateTransform ct;
    private NetcdfDataset ncd;
    
    public static boolean isMine(final NetcdfFile ncFile) {
        return ncFile.findGlobalAttribute("VGLVLS") != null && isValidM3IOFile_(ncFile);
    }
    
    public M3IOVGGridConvention() {
        this.ct = null;
        this.ncd = null;
        this.conventionName = "M3IOVGGrid";
    }
    
    @Override
    public void augmentDataset(final NetcdfDataset ncd, final CancelTask cancelTask) {
        this.constructCoordAxes(this.ncd = ncd);
        ncd.finish();
    }
    
    protected void constructCoordAxes(final NetcdfDataset ds) {
        if (null != this.ncd.findVariable("x")) {
            return;
        }
        final int gdtyp = this.intAttribute("GDTYP");
        final double p_alp = this.doubleAttribute("P_ALP");
        final double p_bet = this.doubleAttribute("P_BET");
        final double p_gam = this.doubleAttribute("P_GAM");
        final double xcent = this.doubleAttribute("XCENT");
        final double ycent = this.doubleAttribute("YCENT");
        final String xUnits = (gdtyp == 1) ? "degrees east" : "m";
        final String yUnits = (gdtyp == 1) ? "degrees north" : "m";
        ProjectionImpl p = null;
        switch (gdtyp) {
            case 1: {
                p = new LatLonProjection();
                break;
            }
            case 2: {
                p = new LambertConformal(ycent, p_gam, p_alp, p_bet);
                break;
            }
            case 3: {
                p = new TransverseMercator(p_alp, p_bet, 1.0);
                break;
            }
            case 4: {
                p = new Stereographic(p_alp, p_bet, 1.0);
                break;
            }
            case 5: {
                p = new UtmProjection(6370000.0, 1.0E30, (int)p_alp, ycent >= 0.0);
                break;
            }
            case 6: {
                p = new Stereographic(ycent, xcent, 1.0);
                break;
            }
            case 7: {
                p = new Mercator(p_gam, p_alp);
                break;
            }
            case 8: {
                p = new TransverseMercator(p_alp, p_gam, 1.0);
                break;
            }
            case 9: {
                p = new AlbersEqualArea(ycent, xcent, p_alp, p_bet);
                break;
            }
            case 10: {
                p = new LambertAzimuthalEqualArea(ycent, xcent, 0.0, 0.0, 6370000.0);
                break;
            }
        }
        if (p != null) {
            this.ct = new ProjectionCT(p.getClassName(), "FGDC", p);
            final VariableDS v = this.makeCoordinateTransformVariable(ds, this.ct);
            v.addAttribute(new Attribute("_CoordinateAxisTypes", "GeoX GeoY"));
            ds.addVariable(null, v);
        }
        this.makeXCoordAxis(ds, xUnits);
        this.makeYCoordAxis(ds, yUnits);
        this.makeZCoordAxis(ds);
        this.makeTimeCoordAxis(ds);
    }
    
    private void makeXCoordAxis(final NetcdfDataset ds, final String units) {
        final Dimension dim = ds.findDimension("COL");
        final String newUnits = units.equals("m") ? "km" : units;
        final String desc = "synthesized x coordinate from XORIG, XCELL global attributes";
        final CoordinateAxis axis = new CoordinateAxis1D(ds, null, "x", DataType.DOUBLE, dim.getName(), newUnits, desc);
        final double scale = units.equals("m") ? 0.001 : 1.0;
        final double xorig = this.doubleAttribute("XORIG") * scale;
        final double xcell = this.doubleAttribute("XCELL") * scale;
        ds.setValues(axis, dim.getLength(), xorig, xcell);
        axis.addAttribute(new Attribute("units", newUnits));
        axis.addAttribute(new Attribute("long_name", desc));
        axis.addAttribute(new Attribute("_CoordinateAxisType", AxisType.GeoX.toString()));
        ds.addCoordinateAxis(axis);
    }
    
    private void makeYCoordAxis(final NetcdfDataset ds, final String units) {
        final Dimension dim = ds.findDimension("ROW");
        final String newUnits = units.equals("m") ? "km" : units;
        final String desc = "synthesized y coordinate from YORIG, YCELL global attributes";
        final CoordinateAxis axis = new CoordinateAxis1D(ds, null, "y", DataType.DOUBLE, dim.getName(), newUnits, desc);
        final double scale = units.equals("m") ? 0.001 : 1.0;
        final double yorig = this.doubleAttribute("YORIG") * scale;
        final double ycell = this.doubleAttribute("YCELL") * scale;
        ds.setValues(axis, dim.getLength(), yorig, ycell);
        axis.addAttribute(new Attribute("units", newUnits));
        axis.addAttribute(new Attribute("long_name", desc));
        axis.addAttribute(new Attribute("_CoordinateAxisType", AxisType.GeoY.toString()));
        ds.addCoordinateAxis(axis);
    }
    
    private void makeZCoordAxis(final NetcdfDataset ds) {
        final Dimension dim = ds.findDimension("LAY");
        final String desc = "synthesized z coordinate from VGTYP, VGTOP, VGLVLS global attributes";
        final CoordinateAxis axis = new CoordinateAxis1D(ds, null, "z", DataType.DOUBLE, dim.getName(), "km", desc);
        final int vgtyp = this.intAttribute("VGTYP");
        final double vgtop = this.doubleAttribute("VGTOP");
        final double[] vglvls = this.doubleArrayAttribute("VGLVLS");
        final double[] vgLevelsInMeters = convertVGLevels_(vgtyp, vgtop, vglvls);
        final int count = vgLevelsInMeters.length;
        final List<String> vgArray = new ArrayList<String>(count);
        for (int index = 0; index < count; ++index) {
            vgArray.add(Double.toString(vgLevelsInMeters[index] * 0.001));
        }
        ds.setValues(axis, vgArray);
        axis.addAttribute(new Attribute("units", "km"));
        axis.addAttribute(new Attribute("long_name", desc));
        axis.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Height.toString()));
        ds.addCoordinateAxis(axis);
    }
    
    private void makeTimeCoordAxis(final NetcdfDataset ds) {
        final int hhmmss = this.intAttribute("TSTEP");
        final int hh = hhmmss / 10000;
        final int hh2 = hh * 10000;
        final int mmss = hhmmss - hh2;
        final int mm = mmss / 100;
        final int mm2 = mm * 100;
        final int ss = mmss - mm2;
        final int mm_hh = 60;
        final int ss_mm = 60;
        final int totalSeconds = hh * 60 * 60 + mm * 60 + ss;
        final int timeSteps = dimension_(ds, "TSTEP");
        final ArrayInt.D1 data = new ArrayInt.D1(timeSteps);
        for (int timeStep = 0; timeStep < timeSteps; ++timeStep) {
            data.set(timeStep, timeStep * totalSeconds);
        }
        final String desc = "synthesized time coordinate from SDATE, STIME, STEP global attributes";
        final String units = this.timeUnits();
        final CoordinateAxis1D axis = new CoordinateAxis1D(ds, null, "time", DataType.INT, "TSTEP", units, desc);
        axis.setCachedData(data, true);
        axis.addAttribute(new Attribute("long_name", desc));
        axis.addAttribute(new Attribute("units", units));
        axis.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Time.toString()));
        ds.addCoordinateAxis(axis);
    }
    
    private String timeUnits() {
        final int yyyddd = this.intAttribute("SDATE");
        final int hhmmss = this.intAttribute("STIME");
        final int yyyy = yyyddd / 1000;
        final int ddd = yyyddd % 1000;
        final int hh = hhmmss / 10000;
        final int hh2 = hh * 10000;
        final int mmss = hhmmss - hh2;
        final int mm = mmss / 100;
        final int mm2 = mm * 100;
        final int ss = mmss - mm2;
        final Calendar cal = new GregorianCalendar(new SimpleTimeZone(0, "GMT"));
        cal.clear();
        cal.set(1, yyyy);
        cal.set(6, ddd);
        cal.set(11, hh);
        cal.set(12, mm);
        cal.set(13, ss);
        final SimpleDateFormat dateFormatOut = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormatOut.setTimeZone(TimeZone.getTimeZone("GMT"));
        return "seconds since " + dateFormatOut.format(cal.getTime()) + " UTC";
    }
    
    @Override
    protected AxisType getAxisType(final NetcdfDataset ds, final VariableEnhanced ve) {
        final Variable v = (Variable)ve;
        final String vname = v.getName();
        if (vname.equalsIgnoreCase("x")) {
            return AxisType.GeoX;
        }
        if (vname.equalsIgnoreCase("lon")) {
            return AxisType.Lon;
        }
        if (vname.equalsIgnoreCase("y")) {
            return AxisType.GeoY;
        }
        if (vname.equalsIgnoreCase("lat")) {
            return AxisType.Lat;
        }
        if (vname.equalsIgnoreCase("time")) {
            return AxisType.Time;
        }
        if (vname.equalsIgnoreCase("z")) {
            return AxisType.Height;
        }
        return null;
    }
    
    protected String getZPositive(final CoordinateAxis axis) {
        final String unit = axis.getUnitsString();
        final boolean isup = unit != null && SimpleUnit.isCompatible("m", unit);
        return isup ? "up" : "";
    }
    
    protected List<CoordinateTransform> getCoordinateTransforms(final CoordinateSystem cs) {
        final List<CoordinateTransform> list = new ArrayList<CoordinateTransform>();
        if (cs.getXaxis() != null && cs.getYaxis() != null) {
            list.add(this.ct);
        }
        return list;
    }
    
    protected boolean hasMissingData(final Variable v) {
        return true;
    }
    
    protected double getMissingDataValue(final Variable unused) {
        return -9.999E36;
    }
    
    private int intAttribute(final String name) {
        return intAttribute_(this.ncd, name);
    }
    
    private double doubleAttribute(final String name) {
        return doubleAttribute_(this.ncd, name);
    }
    
    private double[] doubleArrayAttribute(final String name) {
        return doubleArrayAttribute_(this.ncd, name);
    }
    
    private static int intAttribute_(final NetcdfFile ncFile, final String name) {
        int result = 0;
        final Attribute attribute = ncFile.findGlobalAttribute(name);
        if (attribute != null) {
            result = attribute.getNumericValue().intValue();
        }
        return result;
    }
    
    private static double doubleAttribute_(final NetcdfFile ncFile, final String name) {
        double result = 0.0;
        final Attribute attribute = ncFile.findGlobalAttribute(name);
        if (attribute != null) {
            result = attribute.getNumericValue().doubleValue();
        }
        return result;
    }
    
    private static double[] doubleArrayAttribute_(final NetcdfFile ncFile, final String name) {
        double[] result = null;
        final Attribute attribute = ncFile.findGlobalAttribute(name);
        if (attribute != null && attribute.isArray() && attribute.getLength() > 1) {
            final int count = attribute.getLength();
            result = new double[count];
            for (int index = 0; index < count; ++index) {
                result[index] = attribute.getNumericValue(index).doubleValue();
            }
        }
        return result;
    }
    
    private static boolean isValidM3IOFile_(final NetcdfFile ncFile) {
        final int[] ftypes = { 1, 3, 8 };
        final String[] dims = { "TSTEP", "DATE-TIME", "LAY", "VAR", "ROW", "COL" };
        boolean result = hasDimensions_(ncFile, dims);
        result = (result && hasStringAttribute_(ncFile, "EXEC_ID", 80));
        result = (result && hasIntAttributeIn_(ncFile, "FTYPE", ftypes));
        result = (result && hasYYYYDDDAttribute_(ncFile, "CDATE"));
        result = (result && hasHHMMSSAttribute_(ncFile, "CTIME"));
        result = (result && hasYYYYDDDAttribute_(ncFile, "SDATE"));
        result = (result && hasHHMMSSAttribute_(ncFile, "STIME"));
        result = (result && hasHHMMSSAttribute_(ncFile, "TSTEP"));
        result = (result && hasIntAttribute_(ncFile, "NTHIK", 1, Integer.MAX_VALUE));
        result = (result && hasIntAttribute_(ncFile, "NCOLS", 1, Integer.MAX_VALUE));
        result = (result && hasIntAttribute_(ncFile, "NROWS", 1, Integer.MAX_VALUE));
        result = (result && hasIntAttribute_(ncFile, "NLAYS", 1, 100));
        result = (result && hasIntAttribute_(ncFile, "NVARS", 1, 120));
        result = (result && hasDimension_(ncFile, "TSTEP", 1, Integer.MAX_VALUE));
        if (result) {
            final int mxrec = dimension_(ncFile, "TSTEP");
            final int nvars = intAttribute_(ncFile, "NVARS");
            final int nlays = intAttribute_(ncFile, "NLAYS");
            final int nrows = intAttribute_(ncFile, "NROWS");
            final int ncols = intAttribute_(ncFile, "NCOLS");
            final int nthik = intAttribute_(ncFile, "NTHIK");
            final int min = Math.min(nrows, ncols);
            final int max = 21474836 / nrows;
            result = (nthik <= min && ncols <= max);
        }
        final int[] gdtypes = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        result = (result && hasIntAttributeIn_(ncFile, "GDTYP", gdtypes));
        result = (result && hasDoubleAttribute_(ncFile, "P_ALP", -90.0, 90.0));
        result = (result && hasDoubleAttribute_(ncFile, "P_BET", -90.0, 90.0));
        result = (result && hasDoubleAttribute_(ncFile, "P_GAM", -180.0, 180.0));
        result = (result && hasDoubleAttribute_(ncFile, "XCENT", -180.0, 180.0));
        result = (result && hasDoubleAttribute_(ncFile, "YCENT", -90.0, 90.0));
        result = (result && hasDoubleAttribute_(ncFile, "XORIG", -3.4028234663852886E38, 3.4028234663852886E38));
        result = (result && hasDoubleAttribute_(ncFile, "YORIG", -3.4028234663852886E38, 3.4028234663852886E38));
        result = (result && hasDoubleAttribute_(ncFile, "XCELL", 1.401298464324817E-45, 3.4028234663852886E38));
        result = (result && hasDoubleAttribute_(ncFile, "YCELL", 1.401298464324817E-45, 3.4028234663852886E38));
        if (result) {
            final int gdtyp = intAttribute_(ncFile, "GDTYP");
            final double p_alp = doubleAttribute_(ncFile, "P_ALP");
            final double p_bet = doubleAttribute_(ncFile, "P_BET");
            final double p_gam = doubleAttribute_(ncFile, "P_GAM");
            final double xcent = doubleAttribute_(ncFile, "XCENT");
            final double ycent = doubleAttribute_(ncFile, "YCENT");
            final double xorig = doubleAttribute_(ncFile, "XORIG");
            final double yorig = doubleAttribute_(ncFile, "YORIG");
            final double xcell = doubleAttribute_(ncFile, "XCELL");
            final double ycell = doubleAttribute_(ncFile, "YCELL");
            final int nrows2 = intAttribute_(ncFile, "NROWS");
            final int ncols2 = intAttribute_(ncFile, "NCOLS");
            result = isValidM3IOProjection_(gdtyp, p_alp, p_bet, p_gam, xcent, ycent, xorig, yorig, xcell, ycell, nrows2, ncols2);
        }
        final int[] vgtypes = { 1, 2, 3, 4, 5, 6 };
        result = (result && hasIntAttributeIn_(ncFile, "VGTYP", vgtypes));
        result = (result && hasDoubleAttribute_(ncFile, "VGTOP", 0.0, 3.4028234663852886E38));
        result = (result && ncFile.findGlobalAttribute("VGLVLS") != null);
        if (result) {
            final int nlays = intAttribute_(ncFile, "NLAYS");
            final int vgtyp = intAttribute_(ncFile, "VGTYP");
            final double vgtop = doubleAttribute_(ncFile, "VGTOP");
            final Attribute a = ncFile.findGlobalAttribute("VGLVLS");
            result = (a != null && a.isArray() && a.getLength() == nlays + 1);
            if (result) {
                final double[] vglvls = doubleArrayAttribute_(ncFile, "VGLVLS");
                result = (vglvls != null && vglvls.length == nlays + 1);
                result = (result && isValidVG_(vgtyp, vgtop, vglvls));
            }
        }
        result = (result && hasStringAttribute_(ncFile, "GDNAM", 16));
        result = (result && hasStringAttribute_(ncFile, "UPNAM", 16));
        final int nvars2 = intAttribute_(ncFile, "NVARS");
        final int varListLen = nvars2 * 16;
        result = (result && hasStringAttribute_(ncFile, "VAR-LIST", varListLen));
        result = (result && ncFile.findGlobalAttribute("FILEDESC") != null);
        result = (result && ncFile.findGlobalAttribute("HISTORY") != null);
        return result;
    }
    
    private static boolean hasDimensions_(final NetcdfFile ncFile, final String[] dims) {
        boolean result = true;
        final Iterator iter = ncFile.getDimensions().iterator();
        Dimension d;
        for (int count = dims.length, index = 0; result && iter.hasNext(); result = (index < count && d.getName().equals(dims[index])), ++index) {
            d = iter.next();
        }
        return result;
    }
    
    private static boolean hasStringAttribute_(final NetcdfFile ncFile, final String name, final int length) {
        final Attribute a = ncFile.findGlobalAttribute(name);
        return a != null && a.isString() && a.getStringValue().length() == length;
    }
    
    private static boolean hasDoubleAttribute_(final NetcdfFile ncFile, final String name, final double min, final double max) {
        boolean result = false;
        final Attribute a = ncFile.findGlobalAttribute(name);
        if (a != null) {
            if (a.getDataType() == DataType.FLOAT) {
                final float v = a.getNumericValue().floatValue();
                result = (v >= min && v <= max);
            }
            else if (a.getDataType() == DataType.DOUBLE) {
                final double v2 = a.getNumericValue().doubleValue();
                result = (v2 >= min && v2 <= max);
            }
        }
        return result;
    }
    
    private static boolean hasDimension_(final NetcdfFile ncFile, final String name, final int min, final int max) {
        boolean result = false;
        final Dimension d = ncFile.findDimension(name);
        if (d != null) {
            final int size = d.getLength();
            result = inRangeI_(size, min, max);
        }
        return result;
    }
    
    private static int dimension_(final NetcdfFile ncFile, final String name) {
        final Dimension d = ncFile.findDimension(name);
        return d.getLength();
    }
    
    private static boolean hasIntAttribute_(final NetcdfFile ncFile, final String name, final int min, final int max) {
        boolean result = false;
        final Attribute a = ncFile.findGlobalAttribute(name);
        if (a != null && a.getDataType() == DataType.INT) {
            final int v = a.getNumericValue().intValue();
            result = (v >= min && v <= max);
        }
        return result;
    }
    
    private static boolean hasIntAttributeIn_(final NetcdfFile ncFile, final String name, final int[] match) {
        boolean result = false;
        final Attribute a = ncFile.findGlobalAttribute(name);
        if (a != null && a.getDataType() == DataType.INT) {
            for (int value = a.getNumericValue().intValue(), count = match.length, index = 0; !result && index < count; result = (value == match[index]), ++index) {}
        }
        return result;
    }
    
    private static boolean hasYYYYDDDAttribute_(final NetcdfFile ncFile, final String name) {
        boolean result = hasIntAttribute_(ncFile, name, 1001, 9999366);
        if (result) {
            final int yyyyddd = intAttribute_(ncFile, name);
            final int ddd = yyyyddd - yyyyddd / 1000 * 1000;
            result = inRangeI_(ddd, 1, 366);
        }
        return result;
    }
    
    private static boolean hasHHMMSSAttribute_(final NetcdfFile ncFile, final String name) {
        boolean result = hasIntAttribute_(ncFile, name, 0, 235959);
        if (result) {
            final int hhmmss = intAttribute_(ncFile, name);
            final int hh = hhmmss / 10000;
            result = (isValidTimestepSize_(hhmmss) && inRangeI_(hh, 0, 23));
        }
        return result;
    }
    
    private static boolean isValidTimestepSize_(final int hhmmss) {
        final int hh = hhmmss / 10000;
        final int hh2 = hh * 10000;
        final int mmss = hhmmss - hh2;
        final int mm = mmss / 100;
        final int mm2 = mm * 100;
        final int ss = mmss - mm2;
        return hh >= 0 && inRangeI_(mm, 0, 59) && inRangeI_(ss, 0, 59);
    }
    
    private static boolean isValidM3IOProjection_(final int gdtyp, final double p_alp, final double p_bet, final double p_gam, final double xcent, final double ycent, final double xorig, final double yorig, final double xcell, final double ycell, final int nrows, final int ncols) {
        boolean result = false;
        switch (gdtyp) {
            case 1: {
                result = (inRange_(xorig, -180.0, 180.0) && inRange_(yorig, -90.0, 90.0) && inRange_(xcell, 0.0, 360.0) && inRange_(ycell, 0.0, 180.0) && inRange_(xorig + ncols * xcell, -180.0, 540.0) && inRange_(yorig + nrows * ycell, -90.0, 90.0));
                break;
            }
            case 2: {
                result = (inRange_(p_alp, -90.0, 90.0) && inRange_(p_bet, p_alp, (p_alp > 0.0) ? 90.0 : 0.0) && inRange_(p_gam, -180.0, 180.0) && inRange_(xcent, -180.0, 180.0) && inRange_(ycent, -90.0, 90.0));
                break;
            }
            case 3: {
                result = (inRange_(p_alp, -90.0, 90.0) && inRange_(p_bet, -180.0, 180.0) && p_gam == 0.0 && inRange_(xcent, -180.0, 180.0) && inRange_(ycent, -90.0, 90.0));
                break;
            }
            case 4: {
                result = (inRange_(p_alp, -90.0, 90.0) && inRange_(p_bet, p_alp, (p_alp > 0.0) ? 90.0 : 0.0) && inRange_(p_gam, -180.0, 180.0));
                break;
            }
            case 5: {
                result = inRange_(p_alp, 1.0, 60.0);
                break;
            }
            case 6: {
                result = ((p_alp == -1.0 || p_alp == 1.0) && inRange_(p_bet, -90.0, 90.0) && inRange_(p_gam, -180.0, 180.0) && inRange_(xcent, -180.0, 180.0) && inRange_(ycent, -90.0, 90.0));
                break;
            }
            case 7: {
                result = (inRange_(p_alp, -90.0, 90.0) && p_gam == xcent && inRange_(xcent, -180.0, 180.0) && inRange_(ycent, -90.0, 90.0));
                break;
            }
            case 8: {
                result = (inRange_(p_alp, -90.0, 90.0) && p_gam == xcent && inRange_(xcent, -180.0, 180.0) && inRange_(ycent, -90.0, 90.0));
                break;
            }
            case 9: {
                result = (inRange_(p_alp, -90.0, 90.0) && inRange_(p_bet, p_alp, (p_alp > 0.0) ? 90.0 : 0.0) && p_gam == xcent && inRange_(xcent, -180.0, 180.0) && inRange_(ycent, -90.0, 90.0));
            }
            case 10: {
                result = (inRange_(p_alp, -90.0, 90.0) && p_gam == xcent && inRange_(xcent, -180.0, 180.0) && inRange_(ycent, -90.0, 90.0));
                break;
            }
        }
        return result;
    }
    
    private static boolean isValidVG_(final int vgtyp, final double vgtop, final double[] vglvls) {
        boolean result = false;
        switch (vgtyp) {
            case 1:
            case 2:
            case 3: {
                result = orderedFromTo_(vglvls, 1.0, 0.0);
                break;
            }
            case 4: {
                result = decreasesToward_(vglvls, vgtop);
                break;
            }
            case 5: {
                result = orderedWithin_(vglvls, -200.0, 100000.0);
                break;
            }
            case 6: {
                result = orderedWithin_(vglvls, 0.0, 100000.0);
                break;
            }
            default: {
                result = (vgtyp == -9999 && vglvls.length == 2);
                break;
            }
        }
        return result;
    }
    
    private static double[] convertVGLevels_(final int vgtyp, final double vgtop, final double[] vglvls) {
        final boolean computeZAtLevels = false;
        final int numberOfLevels = vglvls.length - 1;
        final double[] result = new double[numberOfLevels];
        for (int level = 0; level < numberOfLevels; ++level) {
            final double valueAtLevel = (vglvls[level] + vglvls[level + 1]) * 0.5;
            final double HEIGHT_OF_TERRAIN_IN_METERS = 0.0;
            switch (vgtyp) {
                case 1:
                case 2: {
                    final double pressure = pressureAtSigmaLevel_(valueAtLevel, vgtop * 0.01);
                    result[level] = heightAtPressure_(pressure);
                    break;
                }
                case 3: {
                    result[level] = 0.0 + valueAtLevel * (vgtop - 0.0);
                    break;
                }
                case 4: {
                    result[level] = heightAtPressure_(valueAtLevel * 0.01);
                    break;
                }
                case 5: {
                    result[level] = valueAtLevel;
                    break;
                }
                case 6: {
                    result[level] = valueAtLevel + 0.0;
                    break;
                }
                default: {
                    result[level] = level;
                    break;
                }
            }
        }
        return result;
    }
    
    private static double pressureAtSigmaLevel_(final double sigmaLevel, final double pressureAtTop) {
        return pressureAtTop + sigmaLevel * (1012.5 - pressureAtTop);
    }
    
    private static double heightAtPressure_(final double pressure) {
        final double pressureToHeightScaleFactor = -7200.0;
        return -7200.0 * Math.log(pressure / 1012.5);
    }
    
    private static boolean orderedFromTo_(final double[] a, final double first, final double last) {
        final int count = a.length;
        final boolean increasing = first <= last;
        boolean result = increasing ? (a[0] >= first && a[count - 1] <= last) : (a[0] >= last && a[count - 1] <= first);
        if (increasing) {
            for (int index = 1; result && index < count; result = inRange_(a[index], a[index - 1], last), ++index) {}
        }
        else {
            for (int index = 1; result && index < count; result = inRange_(a[index], last, a[index - 1]), ++index) {}
        }
        return result;
    }
    
    private static boolean orderedWithin_(final double[] a, final double first, final double last) {
        final int count = a.length;
        final boolean increasing = first < last;
        boolean result = increasing ? (a[0] > first && a[count - 1] < last) : (a[0] > last && a[count - 1] < first);
        if (increasing) {
            double a_index;
            for (int index = 1; result && index < count; result = (a_index > a[index - 1] && a_index < last), ++index) {
                a_index = a[index];
            }
        }
        else {
            double a_index;
            for (int index = 1; result && index < count; result = (a_index > last && a_index < a[index - 1]), ++index) {
                a_index = a[index];
            }
        }
        return result;
    }
    
    private static boolean decreasesToward_(final double[] a, final double last) {
        final int count = a.length;
        boolean result = true;
        for (int index = 1; result && index < count; result = inRange_(a[index], last, a[index - 1]), ++index) {}
        return result;
    }
    
    private static boolean inRangeI_(final int x, final int min, final int max) {
        return x >= min && x <= max;
    }
    
    private static boolean inRange_(final double x, final double min, final double max) {
        return x >= min && x <= max;
    }
}
