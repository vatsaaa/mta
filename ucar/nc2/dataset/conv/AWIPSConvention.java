// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.conv;

import ucar.ma2.IndexIterator;
import ucar.unidata.geoloc.projection.Stereographic;
import java.util.NoSuchElementException;
import ucar.unidata.geoloc.ProjectionImpl;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.ProjectionPointImpl;
import ucar.unidata.geoloc.projection.LambertConformal;
import ucar.nc2.units.SimpleUnit;
import ucar.nc2.constants.AxisType;
import ucar.nc2.dataset.VariableEnhanced;
import ucar.ma2.Section;
import ucar.nc2.dataset.CoordinateAxis1D;
import ucar.ma2.MAMath;
import ucar.ma2.Array;
import java.util.StringTokenizer;
import ucar.ma2.ArrayChar;
import ucar.unidata.util.StringUtil;
import java.util.Iterator;
import ucar.nc2.dataset.CoordinateAxis;
import ucar.nc2.Dimension;
import ucar.nc2.Group;
import ucar.nc2.dataset.CoordinateTransform;
import java.io.IOException;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.DataType;
import ucar.nc2.Attribute;
import ucar.nc2.dataset.VariableDS;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import java.util.ArrayList;
import ucar.nc2.NetcdfFile;
import ucar.nc2.dataset.ProjectionCT;
import ucar.nc2.Variable;
import java.util.List;
import ucar.nc2.dataset.CoordSysBuilder;

public class AWIPSConvention extends CoordSysBuilder
{
    private final boolean debugProj = false;
    private final boolean debugBreakup = false;
    private List<Variable> mungedList;
    private ProjectionCT projCT;
    private double startx;
    private double starty;
    
    public static boolean isMine(final NetcdfFile ncfile) {
        return null != ncfile.findGlobalAttribute("projName") && null != ncfile.findDimension("charsPerLevel") && null != ncfile.findDimension("x") && null != ncfile.findDimension("y");
    }
    
    public AWIPSConvention() {
        this.mungedList = new ArrayList<Variable>();
        this.projCT = null;
        this.conventionName = "AWIPS";
    }
    
    @Override
    public void augmentDataset(final NetcdfDataset ds, final CancelTask cancelTask) {
        if (null != ds.findVariable("x")) {
            return;
        }
        final Dimension dimx = ds.findDimension("x");
        final int nx = dimx.getLength();
        final Dimension dimy = ds.findDimension("y");
        final int ny = dimy.getLength();
        final String projName = ds.findAttValueIgnoreCase(null, "projName", "none");
        if (projName.equalsIgnoreCase("LATLON")) {
            ds.addCoordinateAxis(this.makeLonCoordAxis(ds, nx, "x"));
            ds.addCoordinateAxis(this.makeLatCoordAxis(ds, ny, "y"));
        }
        else if (projName.equalsIgnoreCase("LAMBERT_CONFORMAL")) {
            this.projCT = this.makeLCProjection(ds, projName);
            ds.addCoordinateAxis(this.makeXCoordAxis(ds, nx, "x"));
            ds.addCoordinateAxis(this.makeYCoordAxis(ds, ny, "y"));
        }
        else if (projName.equalsIgnoreCase("STEREOGRAPHIC")) {
            this.projCT = this.makeStereoProjection(ds, projName);
            ds.addCoordinateAxis(this.makeXCoordAxis(ds, nx, "x"));
            ds.addCoordinateAxis(this.makeYCoordAxis(ds, ny, "y"));
        }
        final CoordinateAxis timeCoord = this.makeTimeCoordAxis(ds);
        if (timeCoord != null) {
            ds.addCoordinateAxis(timeCoord);
            final Dimension d = timeCoord.getDimension(0);
            if (!d.getName().equals(timeCoord.getShortName())) {
                timeCoord.addAttribute(new Attribute("_CoordinateAliasForDimension", d.getName()));
            }
        }
        for (final Variable ncvar : ds.getVariables()) {
            final String levelName = ncvar.getName() + "Levels";
            final Variable levelVar = ds.findVariable(levelName);
            if (levelVar == null) {
                continue;
            }
            if (levelVar.getRank() != 2) {
                continue;
            }
            if (levelVar.getDataType() != DataType.CHAR) {
                continue;
            }
            try {
                final List<Dimension> levels = this.breakupLevels(ds, levelVar);
                this.createNewVariables(ds, ncvar, levels, levelVar.getDimension(0));
            }
            catch (InvalidRangeException ex) {
                this.parseInfo.format("createNewVariables InvalidRangeException\n", new Object[0]);
            }
            catch (IOException ioe) {
                this.parseInfo.format("createNewVariables IOException\n", new Object[0]);
            }
            this.mungedList.add(ncvar);
        }
        if (this.projCT != null) {
            final VariableDS v = this.makeCoordinateTransformVariable(ds, this.projCT);
            v.addAttribute(new Attribute("_CoordinateAxes", "x y"));
            ds.addVariable(null, v);
        }
        ds.finish();
        final List<Variable> vlist = ds.getVariables();
        for (final Variable v2 : vlist) {
            final Attribute att = v2.findAttributeIgnoreCase("units");
            if (att != null) {
                final String units = att.getStringValue();
                v2.addAttribute(new Attribute("units", this.normalize(units)));
            }
        }
    }
    
    private String normalize(String units) {
        if (units.equals("/second")) {
            units = "1/sec";
        }
        if (units.equals("degrees K")) {
            units = "K";
        }
        else {
            units = StringUtil.substitute(units, "**", "^");
            units = StringUtil.remove(units, 41);
            units = StringUtil.remove(units, 40);
        }
        return units;
    }
    
    private List<Dimension> breakupLevels(final NetcdfDataset ds, final Variable levelVar) throws IOException {
        final List<Dimension> dimList = new ArrayList<Dimension>();
        ArrayChar levelVarData;
        try {
            levelVarData = (ArrayChar)levelVar.read();
        }
        catch (IOException ioe) {
            return dimList;
        }
        List<String> values = null;
        String currentUnits = null;
        final ArrayChar.StringIterator iter = levelVarData.getStringIterator();
        while (iter.hasNext()) {
            final String s = iter.next();
            final StringTokenizer stoke = new StringTokenizer(s);
            if (!stoke.hasMoreTokens()) {
                continue;
            }
            final String units = stoke.nextToken().trim();
            if (!units.equals(currentUnits)) {
                if (values != null) {
                    dimList.add(this.makeZCoordAxis(ds, values, currentUnits));
                }
                values = new ArrayList<String>();
                currentUnits = units;
            }
            if (stoke.hasMoreTokens()) {
                values.add(stoke.nextToken());
            }
            else {
                values.add("0");
            }
        }
        if (values != null) {
            dimList.add(this.makeZCoordAxis(ds, values, currentUnits));
        }
        return dimList;
    }
    
    private Dimension makeZCoordAxis(final NetcdfDataset ds, final List<String> values, final String units) throws IOException {
        final int len = values.size();
        String name = this.makeZCoordName(units);
        if (len > 1) {
            name += Integer.toString(len);
        }
        else {
            name += values.get(0);
        }
        StringUtil.replace(name, ' ', "-");
        Dimension dim;
        if (null != (dim = ds.getRootGroup().findDimension(name)) && dim.getLength() == len) {
            final Variable coord = ds.getRootGroup().findVariable(name);
            final Array coordData = coord.read();
            final Array newData = Array.makeArray(coord.getDataType(), values);
            if (MAMath.isEqual(coordData, newData)) {
                return dim;
            }
        }
        final String orgName = name;
        for (int count = 1; ds.getRootGroup().findDimension(name) != null; name = orgName + "-" + count, ++count) {}
        dim = new Dimension(name, len);
        ds.addDimension(null, dim);
        final CoordinateAxis v = new CoordinateAxis1D(ds, null, name, DataType.DOUBLE, name, this.makeUnitsName(units), this.makeLongName(name));
        final String positive = this.getZisPositive(ds, v);
        if (null != positive) {
            v.addAttribute(new Attribute("_CoordinateZisPositive", positive));
        }
        ds.setValues(v, values);
        ds.addCoordinateAxis(v);
        this.parseInfo.format("Created Z Coordinate Axis = ", new Object[0]);
        v.getNameAndDimensions(this.parseInfo, true, false);
        this.parseInfo.format("\n", new Object[0]);
        return dim;
    }
    
    private String makeZCoordName(final String units) {
        if (units.equalsIgnoreCase("MB")) {
            return "PressureLevels";
        }
        if (units.equalsIgnoreCase("K")) {
            return "PotTempLevels";
        }
        if (units.equalsIgnoreCase("BL")) {
            return "BoundaryLayers";
        }
        if (units.equalsIgnoreCase("FHAG")) {
            return "FixedHeightAboveGround";
        }
        if (units.equalsIgnoreCase("FH")) {
            return "FixedHeight";
        }
        if (units.equalsIgnoreCase("SFC")) {
            return "Surface";
        }
        if (units.equalsIgnoreCase("MSL")) {
            return "MeanSeaLevel";
        }
        if (units.equalsIgnoreCase("FRZ")) {
            return "FreezingLevel";
        }
        if (units.equalsIgnoreCase("TROP")) {
            return "Tropopause";
        }
        if (units.equalsIgnoreCase("MAXW")) {
            return "MaxWindLevel";
        }
        return units;
    }
    
    private String makeUnitsName(final String units) {
        if (units.equalsIgnoreCase("MB")) {
            return "hPa";
        }
        if (units.equalsIgnoreCase("BL")) {
            return "hPa";
        }
        if (units.equalsIgnoreCase("FHAG")) {
            return "m";
        }
        if (units.equalsIgnoreCase("FH")) {
            return "m";
        }
        return "";
    }
    
    private String makeLongName(final String name) {
        if (name.equalsIgnoreCase("PotTempLevels")) {
            return "Potential Temperature Level";
        }
        if (name.equalsIgnoreCase("BoundaryLayers")) {
            return "BoundaryLayer hectoPascals above ground";
        }
        return name;
    }
    
    private void createNewVariables(final NetcdfDataset ds, final Variable ncVar, final List<Dimension> newDims, final Dimension levelDim) throws InvalidRangeException {
        final List<Dimension> dims = ncVar.getDimensions();
        final int newDimIndex = dims.indexOf(levelDim);
        final int[] origin = new int[ncVar.getRank()];
        final int[] shape = ncVar.getShape();
        int count = 0;
        for (final Dimension dim : newDims) {
            final String name = ncVar.getName() + "-" + dim.getName();
            origin[newDimIndex] = count;
            shape[newDimIndex] = dim.getLength();
            final Variable varNew = ncVar.section(new Section(origin, shape));
            varNew.setName(name);
            varNew.setDimension(newDimIndex, dim);
            String long_name = ds.findAttValueIgnoreCase(ncVar, "long_name", ncVar.getName());
            long_name = long_name + "-" + dim.getName();
            ds.addVariableAttribute(varNew, new Attribute("long_name", long_name));
            ds.addVariable(null, varNew);
            this.parseInfo.format("Created New Variable as section = ", new Object[0]);
            varNew.getNameAndDimensions(this.parseInfo, true, false);
            this.parseInfo.format("\n", new Object[0]);
            count += dim.getLength();
        }
    }
    
    @Override
    protected AxisType getAxisType(final NetcdfDataset ds, final VariableEnhanced ve) {
        final Variable v = (Variable)ve;
        final String vname = v.getName();
        if (vname.equalsIgnoreCase("x")) {
            return AxisType.GeoX;
        }
        if (vname.equalsIgnoreCase("lon")) {
            return AxisType.Lon;
        }
        if (vname.equalsIgnoreCase("y")) {
            return AxisType.GeoY;
        }
        if (vname.equalsIgnoreCase("lat")) {
            return AxisType.Lat;
        }
        if (vname.equalsIgnoreCase("record")) {
            return AxisType.Time;
        }
        final Dimension dim = v.getDimension(0);
        if (dim != null && dim.getName().equalsIgnoreCase("record")) {
            return AxisType.Time;
        }
        final String unit = ve.getUnitsString();
        if (unit != null) {
            if (SimpleUnit.isCompatible("millibar", unit)) {
                return AxisType.Pressure;
            }
            if (SimpleUnit.isCompatible("m", unit)) {
                return AxisType.Height;
            }
        }
        return AxisType.GeoZ;
    }
    
    @Override
    protected void makeCoordinateTransforms(final NetcdfDataset ds) {
        if (this.projCT != null) {
            final VarProcess vp = this.findVarProcess(this.projCT.getName());
            vp.isCoordinateTransform = true;
            vp.ct = this.projCT;
        }
        super.makeCoordinateTransforms(ds);
    }
    
    private String getZisPositive(final NetcdfDataset ds, final CoordinateAxis v) {
        final String attValue = ds.findAttValueIgnoreCase(v, "positive", null);
        if (null != attValue) {
            return attValue.equalsIgnoreCase("up") ? "up" : "down";
        }
        final String unit = v.getUnitsString();
        if (unit != null && SimpleUnit.isCompatible("millibar", unit)) {
            return "down";
        }
        if (unit != null && SimpleUnit.isCompatible("m", unit)) {
            return "up";
        }
        return null;
    }
    
    private ProjectionCT makeLCProjection(final NetcdfDataset ds, final String name) throws NoSuchElementException {
        final double centralLat = this.findAttributeDouble(ds, "centralLat");
        final double centralLon = this.findAttributeDouble(ds, "centralLon");
        final double rotation = this.findAttributeDouble(ds, "rotation");
        final LambertConformal lc = new LambertConformal(rotation, centralLon, centralLat, centralLat);
        final double lat0 = this.findAttributeDouble(ds, "lat00");
        final double lon0 = this.findAttributeDouble(ds, "lon00");
        final ProjectionPointImpl start = (ProjectionPointImpl)lc.latLonToProj(new LatLonPointImpl(lat0, lon0));
        this.startx = start.getX();
        this.starty = start.getY();
        return new ProjectionCT(name, "FGDC", lc);
    }
    
    private ProjectionCT makeStereoProjection(final NetcdfDataset ds, final String name) throws NoSuchElementException {
        final double centralLat = this.findAttributeDouble(ds, "centralLat");
        final double centralLon = this.findAttributeDouble(ds, "centralLon");
        final double rotation = this.findAttributeDouble(ds, "rotation");
        final double latDxDy = this.findAttributeDouble(ds, "latDxDy");
        final double latR = Math.toRadians(latDxDy);
        final double scale = (1.0 + Math.abs(Math.sin(latR))) / 2.0;
        final Stereographic proj = new Stereographic(centralLat, centralLon, scale);
        final double lat0 = this.findAttributeDouble(ds, "lat00");
        final double lon0 = this.findAttributeDouble(ds, "lon00");
        final ProjectionPointImpl start = (ProjectionPointImpl)proj.latLonToProj(new LatLonPointImpl(lat0, lon0));
        this.startx = start.getX();
        this.starty = start.getY();
        this.parseInfo.format("---makeStereoProjection start at proj coord %s\n", start);
        final double latN = this.findAttributeDouble(ds, "latNxNy");
        final double lonN = this.findAttributeDouble(ds, "lonNxNy");
        final ProjectionPointImpl pt = (ProjectionPointImpl)proj.latLonToProj(new LatLonPointImpl(latN, lonN));
        this.parseInfo.format("                        end at proj coord %s\n", pt);
        this.parseInfo.format("                        scale= %f\n", scale);
        return new ProjectionCT(name, "FGDC", proj);
    }
    
    private CoordinateAxis makeXCoordAxis(final NetcdfDataset ds, final int nx, final String xname) {
        final double dx = this.findAttributeDouble(ds, "dxKm");
        final CoordinateAxis v = new CoordinateAxis1D(ds, null, xname, DataType.DOUBLE, xname, "km", "x on projection");
        ds.setValues(v, nx, this.startx, dx);
        this.parseInfo.format("Created X Coordinate Axis = ", new Object[0]);
        v.getNameAndDimensions(this.parseInfo, true, false);
        this.parseInfo.format("\n", new Object[0]);
        return v;
    }
    
    private CoordinateAxis makeYCoordAxis(final NetcdfDataset ds, final int ny, final String yname) {
        final double dy = this.findAttributeDouble(ds, "dyKm");
        final CoordinateAxis v = new CoordinateAxis1D(ds, null, yname, DataType.DOUBLE, yname, "km", "y on projection");
        ds.setValues(v, ny, this.starty, dy);
        this.parseInfo.format("Created Y Coordinate Axis = ", new Object[0]);
        v.getNameAndDimensions(this.parseInfo, true, false);
        this.parseInfo.format("\n", new Object[0]);
        return v;
    }
    
    private CoordinateAxis makeLonCoordAxis(final NetcdfDataset ds, final int n, final String xname) {
        final double min = this.findAttributeDouble(ds, "xMin");
        final double max = this.findAttributeDouble(ds, "xMax");
        final double d = this.findAttributeDouble(ds, "dx");
        if (Double.isNaN(min) || Double.isNaN(max) || Double.isNaN(d)) {
            return null;
        }
        final CoordinateAxis v = new CoordinateAxis1D(ds, null, xname, DataType.DOUBLE, xname, "degrees_east", "longitude");
        ds.setValues(v, n, min, d);
        v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lon.toString()));
        final double maxCalc = min + d * n;
        this.parseInfo.format("Created Lon Coordinate Axis (max calc= %f shoule be = %f)\n", maxCalc, max);
        v.getNameAndDimensions(this.parseInfo, true, false);
        this.parseInfo.format("\n", new Object[0]);
        return v;
    }
    
    private CoordinateAxis makeLatCoordAxis(final NetcdfDataset ds, final int n, final String xname) {
        final double min = this.findAttributeDouble(ds, "yMin");
        final double max = this.findAttributeDouble(ds, "yMax");
        final double d = this.findAttributeDouble(ds, "dy");
        if (Double.isNaN(min) || Double.isNaN(max) || Double.isNaN(d)) {
            return null;
        }
        final CoordinateAxis v = new CoordinateAxis1D(ds, null, xname, DataType.DOUBLE, xname, "degrees_north", "latitude");
        ds.setValues(v, n, min, d);
        v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lat.toString()));
        final double maxCalc = min + d * n;
        this.parseInfo.format("Created Lat Coordinate Axis (max calc= %f should be = %f)\n", maxCalc, max);
        v.getNameAndDimensions(this.parseInfo, true, false);
        this.parseInfo.format("\n", new Object[0]);
        return v;
    }
    
    private CoordinateAxis makeTimeCoordAxis(final NetcdfDataset ds) {
        final Variable timeVar = ds.findVariable("valtimeMINUSreftime");
        final Dimension recordDim = ds.findDimension("record");
        Array vals;
        try {
            vals = timeVar.read();
        }
        catch (IOException ioe) {
            return null;
        }
        final int recLen = recordDim.getLength();
        final int valLen = (int)vals.getSize();
        if (recLen != valLen) {
            try {
                vals = vals.sectionNoReduce(new int[] { 0 }, new int[] { recordDim.getLength() }, null);
                this.parseInfo.format(" corrected the TimeCoordAxis length\n", new Object[0]);
            }
            catch (InvalidRangeException e) {
                this.parseInfo.format("makeTimeCoordAxis InvalidRangeException\n", new Object[0]);
            }
        }
        final String units = this.makeTimeUnitFromFilename(ds.getLocation());
        if (units == null) {
            return this.makeTimeCoordAxisFromReference(ds, timeVar, vals);
        }
        final String desc = "synthesized time coordinate from valtimeMINUSreftime and filename YYYYMMDD_HHMM";
        final CoordinateAxis1D timeCoord = new CoordinateAxis1D(ds, null, "timeCoord", DataType.INT, "record", units, desc);
        timeCoord.setCachedData(vals, true);
        this.parseInfo.format("Created Time Coordinate Axis = ", new Object[0]);
        timeCoord.getNameAndDimensions(this.parseInfo, true, false);
        this.parseInfo.format("\n", new Object[0]);
        return timeCoord;
    }
    
    private String makeTimeUnitFromFilename(String dsName) {
        dsName = dsName.replace('\\', '/');
        int posFirst = dsName.lastIndexOf(47);
        if (posFirst < 0) {
            posFirst = 0;
        }
        final int posLast = dsName.indexOf(".", posFirst);
        if (posLast < 0) {
            dsName = dsName.substring(posFirst + 1);
        }
        else {
            dsName = dsName.substring(posFirst + 1, posLast);
        }
        if (dsName.length() != 13) {
            return null;
        }
        final String year = dsName.substring(0, 4);
        final String mon = dsName.substring(4, 6);
        final String day = dsName.substring(6, 8);
        final String hour = dsName.substring(9, 11);
        final String min = dsName.substring(11, 13);
        return "seconds since " + year + "-" + mon + "-" + day + " " + hour + ":" + min + ":0";
    }
    
    private CoordinateAxis makeTimeCoordAxisFromReference(final NetcdfDataset ds, final Variable timeVar, final Array vals) {
        final Variable refVar = ds.findVariable("reftime");
        if (refVar == null) {
            return null;
        }
        double refValue;
        try {
            final Array refArray = refVar.read();
            refValue = refArray.getDouble(refArray.getIndex());
        }
        catch (IOException ioe) {
            return null;
        }
        final Array dvals = Array.factory(Double.TYPE, vals.getShape());
        final IndexIterator diter = dvals.getIndexIterator();
        final IndexIterator iiter = vals.getIndexIterator();
        while (iiter.hasNext()) {
            diter.setDoubleNext(iiter.getDoubleNext() + refValue);
        }
        String units = ds.findAttValueIgnoreCase(refVar, "units", "seconds since 1970-1-1 00:00:00");
        units = this.normalize(units);
        final String desc = "synthesized time coordinate from reftime, valtimeMINUSreftime";
        final CoordinateAxis1D timeCoord = new CoordinateAxis1D(ds, null, "timeCoord", DataType.DOUBLE, "record", units, desc);
        timeCoord.setCachedData(dvals, true);
        this.parseInfo.format("Created Time Coordinate Axis From Reference = ", new Object[0]);
        timeCoord.getNameAndDimensions(this.parseInfo, true, false);
        this.parseInfo.format("\n", new Object[0]);
        return timeCoord;
    }
    
    private double findAttributeDouble(final NetcdfDataset ds, final String attname) {
        final Attribute att = ds.findGlobalAttributeIgnoreCase(attname);
        if (att == null) {
            this.parseInfo.format("ERROR cant find attribute= %s\n", attname);
            return Double.NaN;
        }
        return att.getNumericValue().doubleValue();
    }
}
