// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset;

import ucar.nc2.Attribute;
import java.util.ArrayList;
import java.util.List;
import ucar.nc2.Variable;

class EnhancementsImpl implements Enhancements
{
    private Variable forVar;
    private String desc;
    private String units;
    private List<CoordinateSystem> coordSys;
    
    public EnhancementsImpl(final Variable forVar, final String units, final String desc) {
        this.forVar = forVar;
        this.units = units;
        this.desc = desc;
    }
    
    public EnhancementsImpl(final Variable forVar) {
        this.forVar = forVar;
    }
    
    public List<CoordinateSystem> getCoordinateSystems() {
        return (this.coordSys == null) ? new ArrayList<CoordinateSystem>(0) : this.coordSys;
    }
    
    public void addCoordinateSystem(final CoordinateSystem cs) {
        if (cs == null) {
            throw new RuntimeException("Attempted to add null CoordinateSystem to var " + this.forVar.getName());
        }
        if (this.coordSys == null) {
            this.coordSys = new ArrayList<CoordinateSystem>(5);
        }
        this.coordSys.add(cs);
    }
    
    public void removeCoordinateSystem(final CoordinateSystem p0) {
        if (this.coordSys != null) {
            this.coordSys.remove(p0);
        }
    }
    
    public void setDescription(final String desc) {
        this.desc = desc;
    }
    
    public String getDescription() {
        if (this.desc == null && this.forVar != null) {
            Attribute att = this.forVar.findAttributeIgnoreCase("long_name");
            if (att != null && att.isString()) {
                this.desc = att.getStringValue().trim();
            }
            if (this.desc == null) {
                att = this.forVar.findAttributeIgnoreCase("description");
                if (att != null && att.isString()) {
                    this.desc = att.getStringValue().trim();
                }
            }
            if (this.desc == null) {
                att = this.forVar.findAttributeIgnoreCase("title");
                if (att != null && att.isString()) {
                    this.desc = att.getStringValue().trim();
                }
            }
            if (this.desc == null) {
                att = this.forVar.findAttributeIgnoreCase("standard_name");
                if (att != null && att.isString()) {
                    this.desc = att.getStringValue().trim();
                }
            }
        }
        return (this.desc == null) ? "" : this.desc;
    }
    
    public void setUnitsString(final String units) {
        this.units = units;
        this.forVar.addAttribute(new Attribute("units", units));
    }
    
    public String getUnitsString() {
        if (this.units == null && this.forVar != null) {
            final Attribute att = this.forVar.findAttributeIgnoreCase("units");
            if (att != null && att.isString()) {
                return att.getStringValue().trim();
            }
        }
        return this.units;
    }
}
