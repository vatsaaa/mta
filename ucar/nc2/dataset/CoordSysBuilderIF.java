// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset;

import java.io.IOException;
import ucar.nc2.util.CancelTask;

public interface CoordSysBuilderIF
{
    void setConventionUsed(final String p0);
    
    String getConventionUsed();
    
    String getParseInfo();
    
    String getUserAdvice();
    
    void augmentDataset(final NetcdfDataset p0, final CancelTask p1) throws IOException;
    
    void buildCoordinateSystems(final NetcdfDataset p0);
    
    void addUserAdvice(final String p0);
}
