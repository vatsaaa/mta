// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset;

import java.io.IOException;
import ucar.ma2.Array;

public interface EnhanceScaleMissing
{
    boolean hasMissing();
    
    boolean isMissing(final double p0);
    
    boolean isMissingFast(final double p0);
    
    boolean hasInvalidData();
    
    double getValidMin();
    
    double getValidMax();
    
    boolean isInvalidData(final double p0);
    
    boolean hasFillValue();
    
    boolean isFillValue(final double p0);
    
    boolean hasMissingValue();
    
    boolean isMissingValue(final double p0);
    
    void setFillValueIsMissing(final boolean p0);
    
    void setInvalidDataIsMissing(final boolean p0);
    
    void setMissingDataIsMissing(final boolean p0);
    
    void setUseNaNs(final boolean p0);
    
    boolean getUseNaNs();
    
    boolean hasScaleOffset();
    
    Array convertScaleOffsetMissing(final Array p0) throws IOException;
    
    double convertScaleOffsetMissing(final byte p0);
    
    double convertScaleOffsetMissing(final short p0);
    
    double convertScaleOffsetMissing(final int p0);
    
    double convertScaleOffsetMissing(final long p0);
    
    double convertScaleOffsetMissing(final double p0);
}
