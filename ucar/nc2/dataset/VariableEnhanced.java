// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset;

import java.util.Set;
import ucar.nc2.Variable;
import ucar.nc2.VariableIF;

public interface VariableEnhanced extends VariableIF, Enhancements
{
    Variable getOriginalVariable();
    
    void setOriginalVariable(final Variable p0);
    
    String getOriginalName();
    
    void setUnitsString(final String p0);
    
    void enhance(final Set<NetcdfDataset.Enhance> p0);
    
    void clearCoordinateSystems();
}
