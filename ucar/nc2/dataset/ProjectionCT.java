// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset;

import java.util.List;
import ucar.unidata.util.Parameter;
import ucar.unidata.geoloc.ProjectionImpl;
import net.jcip.annotations.Immutable;

@Immutable
public class ProjectionCT extends CoordinateTransform
{
    private final ProjectionImpl proj;
    
    public ProjectionCT(final String name, final String authority, final ProjectionImpl proj) {
        super(name, authority, TransformType.Projection);
        this.proj = proj;
        final List list = proj.getProjectionParameters();
        for (int i = 0; i < list.size(); ++i) {
            this.addParameter(list.get(i));
        }
    }
    
    public ProjectionImpl getProjection() {
        return this.proj;
    }
}
