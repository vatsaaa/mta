// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset;

import java.util.Iterator;
import java.util.ArrayList;
import ucar.unidata.util.Parameter;
import java.util.List;
import net.jcip.annotations.ThreadSafe;

@ThreadSafe
public class CoordinateTransform implements Comparable
{
    protected final String name;
    protected final String authority;
    protected final TransformType transformType;
    protected List<Parameter> params;
    private volatile int hashCode;
    
    public CoordinateTransform(final String name, final String authority, final TransformType transformType) {
        this.hashCode = 0;
        this.name = name;
        this.authority = authority;
        this.transformType = transformType;
        this.params = new ArrayList<Parameter>();
    }
    
    public void addParameter(final Parameter param) {
        this.params.add(param);
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getAuthority() {
        return this.authority;
    }
    
    public TransformType getTransformType() {
        return this.transformType;
    }
    
    public List<Parameter> getParameters() {
        return this.params;
    }
    
    public Parameter findParameterIgnoreCase(final String name) {
        for (final Parameter a : this.params) {
            if (name.equalsIgnoreCase(a.getName())) {
                return a;
            }
        }
        return null;
    }
    
    @Override
    public boolean equals(final Object oo) {
        if (this == oo) {
            return true;
        }
        if (!(oo instanceof CoordinateTransform)) {
            return false;
        }
        final CoordinateTransform o = (CoordinateTransform)oo;
        if (!this.getName().equals(o.getName())) {
            return false;
        }
        if (!this.getAuthority().equals(o.getAuthority())) {
            return false;
        }
        if (this.getTransformType() != o.getTransformType()) {
            return false;
        }
        final List<Parameter> oparams = o.getParameters();
        if (this.params.size() != oparams.size()) {
            return false;
        }
        for (int i = 0; i < this.params.size(); ++i) {
            final Parameter att = this.params.get(i);
            final Parameter oatt = oparams.get(i);
            if (!att.getName().equals(oatt.getName())) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int result = 17;
            result = 37 * result + this.getName().hashCode();
            result = 37 * result + this.getAuthority().hashCode();
            result = 37 * result + this.getTransformType().hashCode();
            for (final Parameter att : this.params) {
                result = 37 * result + att.getName().hashCode();
            }
            this.hashCode = result;
        }
        return this.hashCode;
    }
    
    @Override
    public String toString() {
        return this.name;
    }
    
    public int compareTo(final Object o) {
        final CoordinateTransform oct = (CoordinateTransform)o;
        return this.name.compareTo(oct.getName());
    }
}
