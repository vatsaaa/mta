// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset;

import ucar.ma2.StructureData;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Array;
import ucar.ma2.Section;
import java.io.IOException;
import ucar.ma2.StructureDataIterator;
import ucar.nc2.Structure;
import ucar.nc2.Group;
import ucar.nc2.Sequence;

public class SequenceDS extends StructureDS
{
    private Sequence orgSeq;
    
    public SequenceDS(final Group g, final Sequence orgSeq) {
        super(g, orgSeq);
        this.orgSeq = orgSeq;
    }
    
    @Override
    public StructureDataIterator getStructureIterator(final int bufferSize) throws IOException {
        return new StructureDataConverter(this, this.orgSeq.getStructureIterator(bufferSize));
    }
    
    @Override
    public Array read(final Section section) throws IOException, InvalidRangeException {
        return this.read();
    }
    
    @Override
    public Array read() throws IOException {
        final Array data = this.orgSeq.read();
        return this.convert(data, null);
    }
    
    private class StructureDataConverter implements StructureDataIterator
    {
        private StructureDataIterator orgIter;
        private SequenceDS newStruct;
        private int count;
        
        StructureDataConverter(final SequenceDS newStruct, final StructureDataIterator orgIter) {
            this.count = 0;
            this.newStruct = newStruct;
            this.orgIter = orgIter;
        }
        
        public boolean hasNext() throws IOException {
            return this.orgIter.hasNext();
        }
        
        public StructureData next() throws IOException {
            final StructureData sdata = this.orgIter.next();
            return this.newStruct.convert(sdata, this.count++);
        }
        
        public void setBufferSize(final int bytes) {
            this.orgIter.setBufferSize(bytes);
        }
        
        public StructureDataIterator reset() {
            this.orgIter.reset();
            return this;
        }
        
        public int getCurrentRecno() {
            return this.orgIter.getCurrentRecno();
        }
    }
}
