// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset;

import ucar.ma2.IndexIterator;
import ucar.ma2.Array;
import ucar.nc2.util.Misc;
import ucar.nc2.Attribute;
import java.util.EnumSet;
import ucar.nc2.Variable;
import ucar.ma2.DataType;

class EnhanceScaleMissingImpl implements EnhanceScaleMissing
{
    private static final byte NC_FILL_BYTE = -127;
    private static final char NC_FILL_CHAR = '\0';
    private static final short NC_FILL_SHORT = -32767;
    private static final int NC_FILL_INT = -2147483647;
    private static final float NC_FILL_FLOAT = 9.96921E36f;
    private static final double NC_FILL_DOUBLE = 9.969209968386869E36;
    private static final String FillValue = "_FillValue";
    private static boolean debug;
    private static boolean debugRead;
    private static boolean debugMissing;
    private DataType convertedDataType;
    private boolean useNaNs;
    private boolean invalidDataIsMissing;
    private boolean fillValueIsMissing;
    private boolean missingDataIsMissing;
    private boolean hasScaleOffset;
    private double scale;
    private double offset;
    private boolean hasValidRange;
    private boolean hasValidMin;
    private boolean hasValidMax;
    private double valid_min;
    private double valid_max;
    private boolean hasFillValue;
    private double fillValue;
    private boolean hasMissingValue;
    private double[] missingValue;
    private boolean isUnsigned;
    
    public EnhanceScaleMissingImpl() {
        this.convertedDataType = null;
        this.useNaNs = false;
        this.invalidDataIsMissing = NetcdfDataset.invalidDataIsMissing;
        this.fillValueIsMissing = NetcdfDataset.fillValueIsMissing;
        this.missingDataIsMissing = NetcdfDataset.missingDataIsMissing;
        this.hasScaleOffset = false;
        this.scale = 1.0;
        this.offset = 0.0;
        this.hasValidRange = false;
        this.hasValidMin = false;
        this.hasValidMax = false;
        this.valid_min = -1.7976931348623157E308;
        this.valid_max = Double.MAX_VALUE;
        this.hasFillValue = false;
        this.hasMissingValue = false;
    }
    
    public EnhanceScaleMissingImpl(final VariableDS forVar) {
        this(forVar, NetcdfDataset.useNaNs, NetcdfDataset.fillValueIsMissing, NetcdfDataset.invalidDataIsMissing, NetcdfDataset.missingDataIsMissing);
    }
    
    public EnhanceScaleMissingImpl(final VariableDS forVar, final boolean useNaNs, final boolean fillValueIsMissing, final boolean invalidDataIsMissing, final boolean missingDataIsMissing) {
        this.convertedDataType = null;
        this.useNaNs = false;
        this.invalidDataIsMissing = NetcdfDataset.invalidDataIsMissing;
        this.fillValueIsMissing = NetcdfDataset.fillValueIsMissing;
        this.missingDataIsMissing = NetcdfDataset.missingDataIsMissing;
        this.hasScaleOffset = false;
        this.scale = 1.0;
        this.offset = 0.0;
        this.hasValidRange = false;
        this.hasValidMin = false;
        this.hasValidMax = false;
        this.valid_min = -1.7976931348623157E308;
        this.valid_max = Double.MAX_VALUE;
        this.hasFillValue = false;
        this.hasMissingValue = false;
        this.fillValueIsMissing = fillValueIsMissing;
        this.invalidDataIsMissing = invalidDataIsMissing;
        this.missingDataIsMissing = missingDataIsMissing;
        final Variable orgVar = forVar.getOriginalVariable();
        if (orgVar instanceof VariableDS) {
            final VariableDS orgVarDS = (VariableDS)orgVar;
            final EnumSet<NetcdfDataset.Enhance> orgEnhanceMode = orgVarDS.getEnhanceMode();
            if (orgEnhanceMode != null && orgEnhanceMode.contains(NetcdfDataset.Enhance.ScaleMissing)) {
                return;
            }
        }
        this.isUnsigned = forVar.isUnsigned();
        this.convertedDataType = forVar.getDataType();
        DataType scaleType = null;
        DataType missType = null;
        DataType validType = null;
        DataType fillType = null;
        if (EnhanceScaleMissingImpl.debug) {
            System.out.println("EnhancementsImpl for Variable = " + forVar.getName());
        }
        Attribute att;
        if (null != (att = forVar.findAttribute("scale_factor")) && !att.isString()) {
            this.scale = att.getNumericValue().doubleValue();
            this.hasScaleOffset = true;
            scaleType = att.getDataType();
            forVar.remove(att);
            if (EnhanceScaleMissingImpl.debug) {
                System.out.println("scale = " + this.scale + " type " + scaleType);
            }
        }
        if (null != (att = forVar.findAttribute("add_offset")) && !att.isString()) {
            this.offset = att.getNumericValue().doubleValue();
            this.hasScaleOffset = true;
            final DataType offType = att.getDataType();
            if (this.rank(offType) > this.rank(scaleType)) {
                scaleType = offType;
            }
            forVar.remove(att);
            if (EnhanceScaleMissingImpl.debug) {
                System.out.println("offset = " + this.offset);
            }
        }
        Attribute validRangeAtt = null;
        if (null != (validRangeAtt = forVar.findAttribute("valid_range")) && !validRangeAtt.isString() && validRangeAtt.getLength() > 1) {
            this.valid_min = validRangeAtt.getNumericValue(0).doubleValue();
            this.valid_max = validRangeAtt.getNumericValue(1).doubleValue();
            this.hasValidRange = true;
            validType = validRangeAtt.getDataType();
            if (this.hasScaleOffset) {
                forVar.remove(validRangeAtt);
            }
            if (EnhanceScaleMissingImpl.debug) {
                System.out.println("valid_range = " + this.valid_min + " " + this.valid_max);
            }
        }
        Attribute validMinAtt = null;
        Attribute validMaxAtt = null;
        if (!this.hasValidRange) {
            if (null != (validMinAtt = forVar.findAttribute("valid_min")) && !validMinAtt.isString()) {
                this.valid_min = validMinAtt.getNumericValue().doubleValue();
                this.hasValidMin = true;
                validType = validMinAtt.getDataType();
                if (this.hasScaleOffset) {
                    forVar.remove(validMinAtt);
                }
                if (EnhanceScaleMissingImpl.debug) {
                    System.out.println("valid_min = " + this.valid_min);
                }
            }
            if (null != (validMaxAtt = forVar.findAttribute("valid_max")) && !validMaxAtt.isString()) {
                this.valid_max = validMaxAtt.getNumericValue().doubleValue();
                this.hasValidMax = true;
                final DataType t = validMaxAtt.getDataType();
                if (this.rank(t) > this.rank(validType)) {
                    validType = t;
                }
                if (this.hasScaleOffset) {
                    forVar.remove(validMaxAtt);
                }
                if (EnhanceScaleMissingImpl.debug) {
                    System.out.println("valid_min = " + this.valid_max);
                }
            }
        }
        final boolean hasValidData = this.hasValidMin || this.hasValidMax || this.hasValidRange;
        if (this.hasValidMin && this.hasValidMax) {
            this.hasValidRange = true;
        }
        if (null != (att = forVar.findAttribute("_FillValue")) && !att.isString()) {
            final double[] values = this.getValueAsDouble(att);
            this.fillValue = values[0];
            this.hasFillValue = true;
            fillType = att.getDataType();
            if (this.hasScaleOffset) {
                forVar.remove(att);
            }
            if (EnhanceScaleMissingImpl.debug) {
                System.out.println("missing_datum from _FillValue = " + this.fillValue);
            }
        }
        if (null != (att = forVar.findAttribute("missing_value"))) {
            final String svalue = att.getStringValue();
            if (att.isString()) {
                if (forVar.getDataType() == DataType.CHAR) {
                    this.missingValue = new double[1];
                    if (svalue.length() == 0) {
                        this.missingValue[0] = 0.0;
                    }
                    else {
                        this.missingValue[0] = svalue.charAt(0);
                    }
                    missType = DataType.CHAR;
                    this.hasMissingValue = true;
                }
                else {
                    try {
                        (this.missingValue = new double[1])[0] = Double.parseDouble(svalue);
                        missType = att.getDataType();
                        this.hasMissingValue = true;
                    }
                    catch (NumberFormatException ex) {
                        if (EnhanceScaleMissingImpl.debug) {
                            System.out.println("String missing_value not parsable as double= " + att.getStringValue());
                        }
                    }
                }
            }
            else {
                this.missingValue = this.getValueAsDouble(att);
                missType = att.getDataType();
                this.hasMissingValue = true;
            }
            if (this.hasScaleOffset) {
                forVar.remove(att);
            }
        }
        final boolean hasMissing = (invalidDataIsMissing && hasValidData) || (fillValueIsMissing && this.hasFillValue) || (missingDataIsMissing && this.hasMissingValue);
        if (this.hasScaleOffset) {
            this.convertedDataType = forVar.getDataType();
            if (hasMissing) {
                if (this.rank(scaleType) > this.rank(this.convertedDataType)) {
                    this.convertedDataType = scaleType;
                }
                if (missingDataIsMissing && this.rank(missType) > this.rank(this.convertedDataType)) {
                    this.convertedDataType = missType;
                }
                if (fillValueIsMissing && this.rank(fillType) > this.rank(this.convertedDataType)) {
                    this.convertedDataType = fillType;
                }
                if (invalidDataIsMissing && this.rank(validType) > this.rank(this.convertedDataType)) {
                    this.convertedDataType = validType;
                }
                if (this.rank(this.convertedDataType) < this.rank(DataType.DOUBLE)) {
                    this.convertedDataType = DataType.FLOAT;
                }
            }
            else if (this.rank(scaleType) > this.rank(this.convertedDataType)) {
                this.convertedDataType = scaleType;
            }
            if (EnhanceScaleMissingImpl.debug) {
                System.out.println("assign dataType = " + this.convertedDataType);
            }
            if (hasValidData) {
                final DataType orgType = forVar.getDataType();
                if (this.rank(validType) != this.rank(scaleType) || this.rank(scaleType) < this.rank(orgType)) {
                    if (validRangeAtt != null) {
                        final double[] values2 = this.getValueAsDouble(validRangeAtt);
                        this.valid_min = values2[0];
                        this.valid_max = values2[1];
                    }
                    else {
                        if (validMinAtt != null) {
                            final double[] values2 = this.getValueAsDouble(validMinAtt);
                            this.valid_min = values2[0];
                        }
                        if (validMaxAtt != null) {
                            final double[] values2 = this.getValueAsDouble(validMaxAtt);
                            this.valid_max = values2[0];
                        }
                    }
                }
            }
        }
        if (hasMissing && (this.convertedDataType == DataType.DOUBLE || this.convertedDataType == DataType.FLOAT)) {
            this.useNaNs = useNaNs;
        }
        if (EnhanceScaleMissingImpl.debug) {
            System.out.println("this.useNaNs = " + this.useNaNs);
        }
    }
    
    private double[] getValueAsDouble(final Attribute att) {
        final int n = att.getLength();
        final double[] value = new double[n];
        if (EnhanceScaleMissingImpl.debugMissing) {
            System.out.printf("missing_data: ", new Object[0]);
        }
        for (int i = 0; i < n; ++i) {
            if (this.isUnsigned && att.getDataType() == DataType.BYTE) {
                value[i] = this.convertScaleOffsetMissing(att.getNumericValue(i).byteValue());
            }
            else if (this.isUnsigned && att.getDataType() == DataType.SHORT) {
                value[i] = this.convertScaleOffsetMissing(att.getNumericValue(i).shortValue());
            }
            else if (this.isUnsigned && att.getDataType() == DataType.INT) {
                value[i] = this.convertScaleOffsetMissing(att.getNumericValue(i).intValue());
            }
            else {
                value[i] = this.scale * att.getNumericValue(i).doubleValue() + this.offset;
            }
            if (EnhanceScaleMissingImpl.debugMissing) {
                System.out.print(" " + value[i]);
            }
        }
        if (EnhanceScaleMissingImpl.debugMissing) {
            System.out.println();
        }
        return value;
    }
    
    private int rank(final DataType c) {
        if (c == DataType.BYTE) {
            return 0;
        }
        if (c == DataType.SHORT) {
            return 1;
        }
        if (c == DataType.INT) {
            return 2;
        }
        if (c == DataType.LONG) {
            return 3;
        }
        if (c == DataType.FLOAT) {
            return 4;
        }
        if (c == DataType.DOUBLE) {
            return 5;
        }
        return -1;
    }
    
    public DataType getConvertedDataType() {
        return this.convertedDataType;
    }
    
    public boolean hasInvalidData() {
        return this.hasValidRange || this.hasValidMin || this.hasValidMax;
    }
    
    public double getValidMin() {
        return this.valid_min;
    }
    
    public double getValidMax() {
        return this.valid_max;
    }
    
    public boolean isInvalidData(final double val) {
        if (this.hasValidRange) {
            return val < this.valid_min || val > this.valid_max;
        }
        if (this.hasValidMin) {
            return val < this.valid_min;
        }
        return this.hasValidMax && val > this.valid_max;
    }
    
    public boolean hasFillValue() {
        return this.hasFillValue;
    }
    
    public boolean isFillValue(final double val) {
        return this.hasFillValue && val == this.fillValue;
    }
    
    public double getFillValue() {
        return this.fillValue;
    }
    
    public boolean hasScaleOffset() {
        return this.hasScaleOffset;
    }
    
    public boolean hasMissingValue() {
        return this.hasMissingValue;
    }
    
    public boolean isMissingValue(final double val) {
        if (!this.hasMissingValue) {
            return false;
        }
        for (int i = 0; i < this.missingValue.length; ++i) {
            if (Misc.closeEnough(val, this.missingValue[i])) {
                return true;
            }
        }
        return false;
    }
    
    public double[] getMissingValues() {
        return this.missingValue;
    }
    
    public void setUseNaNs(final boolean useNaNs) {
        this.useNaNs = useNaNs;
    }
    
    public boolean getUseNaNs() {
        return this.useNaNs;
    }
    
    public void setFillValueIsMissing(final boolean b) {
        this.fillValueIsMissing = b;
    }
    
    public void setInvalidDataIsMissing(final boolean b) {
        this.invalidDataIsMissing = b;
    }
    
    public void setMissingDataIsMissing(final boolean b) {
        this.missingDataIsMissing = b;
    }
    
    public boolean hasMissing() {
        return (this.invalidDataIsMissing && this.hasInvalidData()) || (this.fillValueIsMissing && this.hasFillValue()) || (this.missingDataIsMissing && this.hasMissingValue());
    }
    
    public boolean isMissing(final double val) {
        return Double.isNaN(val) || (this.hasMissing() && this.isMissing_(val));
    }
    
    public boolean isMissingFast(final double val) {
        if (this.useNaNs) {
            return Double.isNaN(val);
        }
        return Double.isNaN(val) || (this.hasMissing() && this.isMissing_(val));
    }
    
    private final boolean isMissing_(final double val) {
        return (this.missingDataIsMissing && this.hasMissingValue && this.isMissingValue(val)) || (this.fillValueIsMissing && this.hasFillValue && this.isFillValue(val)) || (this.invalidDataIsMissing && this.isInvalidData(val));
    }
    
    public Object getFillValue(final DataType dt) {
        final DataType useType = (this.convertedDataType == null) ? dt : this.convertedDataType;
        if (useType == DataType.BYTE || useType == DataType.ENUM1) {
            final byte[] result = { (byte)(this.hasFillValue ? ((byte)this.fillValue) : -127) };
            return result;
        }
        if (useType == DataType.BOOLEAN) {
            final boolean[] result2 = { false };
            return result2;
        }
        if (useType == DataType.CHAR) {
            final char[] result3 = { this.hasFillValue ? ((char)this.fillValue) : '\0' };
            return result3;
        }
        if (useType == DataType.SHORT || useType == DataType.ENUM2) {
            final short[] result4 = { (short)(this.hasFillValue ? ((short)this.fillValue) : -32767) };
            return result4;
        }
        if (useType == DataType.INT || useType == DataType.ENUM4) {
            final int[] result5 = { this.hasFillValue ? ((int)this.fillValue) : -2147483647 };
            return result5;
        }
        if (useType == DataType.LONG) {
            final long[] result6 = { this.hasFillValue ? ((long)this.fillValue) : -2147483647L };
            return result6;
        }
        if (useType == DataType.FLOAT) {
            final float[] result7 = { this.hasFillValue ? ((float)this.fillValue) : 9.96921E36f };
            return result7;
        }
        if (useType == DataType.DOUBLE) {
            final double[] result8 = { this.hasFillValue ? this.fillValue : 9.969209968386869E36 };
            return result8;
        }
        final String[] result9 = { "_FillValue" };
        return result9;
    }
    
    public double convertScaleOffsetMissing(final byte valb) {
        if (!this.hasScaleOffset) {
            return (this.useNaNs && this.isMissing(valb)) ? Double.NaN : valb;
        }
        double convertedValue;
        if (this.isUnsigned) {
            convertedValue = this.scale * DataType.unsignedByteToShort(valb) + this.offset;
        }
        else {
            convertedValue = this.scale * valb + this.offset;
        }
        return (this.useNaNs && this.isMissing(convertedValue)) ? Double.NaN : convertedValue;
    }
    
    public double convertScaleOffsetMissing(final short vals) {
        if (!this.hasScaleOffset) {
            return (this.useNaNs && this.isMissing(vals)) ? Double.NaN : vals;
        }
        double convertedValue;
        if (this.isUnsigned) {
            convertedValue = this.scale * DataType.unsignedShortToInt(vals) + this.offset;
        }
        else {
            convertedValue = this.scale * vals + this.offset;
        }
        return (this.useNaNs && this.isMissing(convertedValue)) ? Double.NaN : convertedValue;
    }
    
    public double convertScaleOffsetMissing(final int vali) {
        if (!this.hasScaleOffset) {
            return (this.useNaNs && this.isMissing(vali)) ? Double.NaN : vali;
        }
        double convertedValue;
        if (this.isUnsigned) {
            convertedValue = this.scale * DataType.unsignedIntToLong(vali) + this.offset;
        }
        else {
            convertedValue = this.scale * vali + this.offset;
        }
        return (this.useNaNs && this.isMissing(convertedValue)) ? Double.NaN : convertedValue;
    }
    
    public double convertScaleOffsetMissing(final long vall) {
        if (!this.hasScaleOffset) {
            return (this.useNaNs && this.isMissing((double)vall)) ? Double.NaN : ((double)vall);
        }
        final double convertedValue = this.scale * vall + this.offset;
        return (this.useNaNs && this.isMissing(convertedValue)) ? Double.NaN : convertedValue;
    }
    
    public double convertScaleOffsetMissing(final double value) {
        if (!this.hasScaleOffset) {
            return (this.useNaNs && this.isMissing(value)) ? Double.NaN : value;
        }
        final double convertedValue = this.scale * value + this.offset;
        return (this.useNaNs && this.isMissing(convertedValue)) ? Double.NaN : convertedValue;
    }
    
    public Array convertScaleOffsetMissing(Array data) {
        if (this.hasScaleOffset()) {
            data = this.convertScaleOffset(data);
        }
        else if (this.hasMissing() && this.getUseNaNs()) {
            data = this.convertMissing(data);
        }
        return data;
    }
    
    private Array convertScaleOffset(final Array in) {
        if (!this.hasScaleOffset) {
            return in;
        }
        if (EnhanceScaleMissingImpl.debugRead) {
            System.out.println("convertScaleOffset ");
        }
        final Array out = Array.factory(this.convertedDataType.getPrimitiveClassType(), in.getShape());
        final IndexIterator iterIn = in.getIndexIterator();
        final IndexIterator iterOut = out.getIndexIterator();
        if (this.isUnsigned && in.getElementType() == Byte.TYPE) {
            this.convertScaleOffsetUnsignedByte(iterIn, iterOut);
        }
        else if (this.isUnsigned && in.getElementType() == Short.TYPE) {
            this.convertScaleOffsetUnsignedShort(iterIn, iterOut);
        }
        else if (this.isUnsigned && in.getElementType() == Integer.TYPE) {
            this.convertScaleOffsetUnsignedInt(iterIn, iterOut);
        }
        else {
            final boolean checkMissing = this.useNaNs && this.hasMissing();
            while (iterIn.hasNext()) {
                final double val = this.scale * iterIn.getDoubleNext() + this.offset;
                iterOut.setDoubleNext((checkMissing && this.isMissing_(val)) ? Double.NaN : val);
            }
        }
        return out;
    }
    
    private void convertScaleOffsetUnsignedByte(final IndexIterator iterIn, final IndexIterator iterOut) {
        final boolean checkMissing = this.useNaNs && this.hasMissing();
        while (iterIn.hasNext()) {
            final byte valb = iterIn.getByteNext();
            final double val = this.scale * DataType.unsignedByteToShort(valb) + this.offset;
            iterOut.setDoubleNext((checkMissing && this.isMissing_(val)) ? Double.NaN : val);
        }
    }
    
    private void convertScaleOffsetUnsignedShort(final IndexIterator iterIn, final IndexIterator iterOut) {
        final boolean checkMissing = this.useNaNs && this.hasMissing();
        while (iterIn.hasNext()) {
            final short valb = iterIn.getShortNext();
            final double val = this.scale * DataType.unsignedShortToInt(valb) + this.offset;
            iterOut.setDoubleNext((checkMissing && this.isMissing_(val)) ? Double.NaN : val);
        }
    }
    
    private void convertScaleOffsetUnsignedInt(final IndexIterator iterIn, final IndexIterator iterOut) {
        final boolean checkMissing = this.useNaNs && this.hasMissing();
        while (iterIn.hasNext()) {
            final int valb = iterIn.getIntNext();
            final double val = this.scale * DataType.unsignedIntToLong(valb) + this.offset;
            iterOut.setDoubleNext((checkMissing && this.isMissing_(val)) ? Double.NaN : val);
        }
    }
    
    private Array convertMissing(final Array in) {
        if (EnhanceScaleMissingImpl.debugRead) {
            System.out.println("convertMissing ");
        }
        final IndexIterator iterIn = in.getIndexIterator();
        if (in.getElementType() == Double.TYPE) {
            while (iterIn.hasNext()) {
                final double val = iterIn.getDoubleNext();
                if (this.isMissing_(val)) {
                    iterIn.setDoubleCurrent(Double.NaN);
                }
            }
        }
        else if (in.getElementType() == Float.TYPE) {
            while (iterIn.hasNext()) {
                final float val2 = iterIn.getFloatNext();
                if (this.isMissing_(val2)) {
                    iterIn.setFloatCurrent(Float.NaN);
                }
            }
        }
        return in;
    }
    
    public float[] setMissingToNaN(final float[] values) {
        if (!this.hasMissing()) {
            return values;
        }
        for (int i = 0; i < values.length; ++i) {
            if (this.isMissing_(values[i])) {
                values[i] = Float.NaN;
            }
        }
        return values;
    }
    
    public static void main(final String[] args) {
        final double d = Double.NaN;
        final float f = (float)d;
        System.out.println(" f=" + f + " " + Float.isNaN(f) + " " + Double.isNaN(f));
    }
    
    static {
        EnhanceScaleMissingImpl.debug = false;
        EnhanceScaleMissingImpl.debugRead = false;
        EnhanceScaleMissingImpl.debugMissing = false;
    }
}
