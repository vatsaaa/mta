// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset;

import java.util.EnumSet;
import org.slf4j.LoggerFactory;
import ucar.ma2.Range;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Section;
import java.io.IOException;
import ucar.ma2.StructureMembers;
import ucar.ma2.ArrayStructureMA;
import ucar.ma2.Array;
import ucar.nc2.util.CancelTask;
import java.util.Iterator;
import ucar.nc2.Attribute;
import java.util.Collection;
import ucar.nc2.Structure;
import ucar.ma2.DataType;
import ucar.nc2.Dimension;
import java.util.ArrayList;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Group;
import ucar.nc2.Variable;
import java.util.List;
import java.util.Set;
import org.slf4j.Logger;

public class StructurePseudoDS extends StructureDS
{
    private static Logger log;
    private static boolean debugRecord;
    protected static final Set<NetcdfDataset.Enhance> enhanceScaleMissing;
    private List<Variable> orgVariables;
    
    protected StructurePseudoDS(final NetcdfDataset ncfile, final Group group, final String shortName) {
        super(ncfile, group, shortName);
        this.orgVariables = new ArrayList<Variable>();
    }
    
    public StructurePseudoDS(final NetcdfDataset ncfile, Group group, final String shortName, List<String> varNames, final Dimension outerDim) {
        super(ncfile, group, shortName);
        this.orgVariables = new ArrayList<Variable>();
        this.setDimensions(outerDim.getName());
        if (group == null) {
            group = ncfile.getRootGroup();
        }
        if (varNames == null) {
            final List<Variable> vars = group.getVariables();
            varNames = new ArrayList<String>(vars.size());
            for (final Variable orgV : vars) {
                if (orgV.getDataType() == DataType.STRUCTURE) {
                    continue;
                }
                final Dimension dim0 = orgV.getDimension(0);
                if (dim0 == null || !dim0.equals(outerDim)) {
                    continue;
                }
                varNames.add(orgV.getShortName());
            }
        }
        for (final String name : varNames) {
            final Variable orgV = group.findVariable(name);
            if (orgV == null) {
                StructurePseudoDS.log.warn("StructurePseudoDS cannot find variable " + name);
            }
            else {
                final Dimension dim0 = orgV.getDimension(0);
                if (!outerDim.equals(dim0)) {
                    throw new IllegalArgumentException("Variable " + orgV.getNameAndDimensions() + " must have outermost dimension=" + outerDim);
                }
                final VariableDS memberV = new VariableDS(ncfile, group, this, orgV.getShortName(), orgV.getDataType(), null, orgV.getUnitsString(), orgV.getDescription());
                memberV.setSPobject(orgV.getSPobject());
                memberV.getAttributes().addAll(orgV.getAttributes());
                final List<Dimension> dims = new ArrayList<Dimension>(orgV.getDimensions());
                dims.remove(0);
                memberV.setDimensions(dims);
                memberV.enhance(StructurePseudoDS.enhanceScaleMissing);
                this.addMemberVariable(memberV);
                this.orgVariables.add(orgV);
            }
        }
        this.calcElementSize();
    }
    
    @Override
    protected Variable copy() {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public Structure select(final List<String> memberNames) {
        final StructurePseudoDS result = new StructurePseudoDS((NetcdfDataset)this.ncfile, this.group, this.getShortName(), memberNames, this.getDimension(0));
        result.isSubset = true;
        return result;
    }
    
    @Override
    public boolean removeMemberVariable(final Variable v) {
        if (super.removeMemberVariable(v)) {
            final java.util.Iterator<Variable> iter = this.orgVariables.iterator();
            while (iter.hasNext()) {
                final Variable mv = iter.next();
                if (mv.getShortName().equals(v.getShortName())) {
                    iter.remove();
                    return true;
                }
            }
        }
        return false;
    }
    
    @Override
    public Array reallyRead(final Variable mainv, final CancelTask cancelTask) throws IOException {
        if (StructurePseudoDS.debugRecord) {
            System.out.println(" read all psuedo records ");
        }
        final StructureMembers smembers = this.makeStructureMembers();
        final ArrayStructureMA asma = new ArrayStructureMA(smembers, this.getShape());
        for (final Variable v : this.orgVariables) {
            final Array data = v.read();
            final StructureMembers.Member m = smembers.findMember(v.getShortName());
            m.setDataArray(data);
        }
        return asma;
    }
    
    @Override
    public Array reallyRead(final Variable mainv, final Section section, final CancelTask cancelTask) throws IOException, InvalidRangeException {
        if (null == section) {
            return this._read();
        }
        if (StructurePseudoDS.debugRecord) {
            System.out.println(" read psuedo records " + section.getRange(0));
        }
        final String err = section.checkInRange(this.getShape());
        if (err != null) {
            throw new InvalidRangeException(err);
        }
        final Range r = section.getRange(0);
        final StructureMembers smembers = this.makeStructureMembers();
        final ArrayStructureMA asma = new ArrayStructureMA(smembers, section.getShape());
        for (final Variable v : this.orgVariables) {
            final List<Range> vsection = new ArrayList<Range>(v.getRanges());
            vsection.set(0, r);
            final Array data = v.read(vsection);
            final StructureMembers.Member m = smembers.findMember(v.getShortName());
            m.setDataArray(data);
        }
        return asma;
    }
    
    static {
        StructurePseudoDS.log = LoggerFactory.getLogger(StructurePseudoDS.class);
        StructurePseudoDS.debugRecord = false;
        enhanceScaleMissing = EnumSet.of(NetcdfDataset.Enhance.ScaleMissing);
    }
}
