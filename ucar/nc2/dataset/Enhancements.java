// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset;

import java.util.List;

public interface Enhancements
{
    String getDescription();
    
    String getUnitsString();
    
    List<CoordinateSystem> getCoordinateSystems();
    
    void addCoordinateSystem(final CoordinateSystem p0);
    
    void removeCoordinateSystem(final CoordinateSystem p0);
}
