// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset;

import org.slf4j.LoggerFactory;
import java.io.IOException;
import ucar.ma2.StructureMembers;
import ucar.ma2.Range;
import ucar.ma2.ArrayStructureMA;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Array;
import ucar.ma2.Section;
import java.util.Iterator;
import ucar.nc2.Attribute;
import java.util.Collection;
import ucar.nc2.Structure;
import ucar.ma2.DataType;
import java.util.ArrayList;
import ucar.nc2.Dimension;
import ucar.nc2.Group;
import ucar.nc2.Variable;
import java.util.List;
import org.slf4j.Logger;

public class StructurePseudo2Dim extends StructurePseudoDS
{
    private static Logger log;
    private List<Variable> orgVariables;
    private boolean debugRecord;
    
    public StructurePseudo2Dim(final NetcdfDataset ncfile, Group group, final String shortName, List<String> varNames, final Dimension outer, final Dimension inner) {
        super(ncfile, group, shortName);
        this.orgVariables = new ArrayList<Variable>();
        this.debugRecord = false;
        this.dataType = DataType.STRUCTURE;
        final ArrayList<Dimension> dims = new ArrayList<Dimension>(2);
        dims.add(outer);
        dims.add(inner);
        this.setDimensions(dims);
        if (group == null) {
            group = ncfile.getRootGroup();
        }
        if (varNames == null) {
            final List<Variable> vars = group.getVariables();
            varNames = new ArrayList<String>(vars.size());
            for (final Variable orgV : vars) {
                if (orgV.getRank() < 2) {
                    continue;
                }
                if (!outer.equals(orgV.getDimension(0)) || !inner.equals(orgV.getDimension(1))) {
                    continue;
                }
                varNames.add(orgV.getShortName());
            }
        }
        for (final String name : varNames) {
            final Variable orgV = group.findVariable(name);
            if (orgV == null) {
                StructurePseudo2Dim.log.warn("StructurePseudo2Dim cannot find variable " + name);
            }
            else {
                if (!outer.equals(orgV.getDimension(0))) {
                    throw new IllegalArgumentException("Variable " + orgV.getNameAndDimensions() + " must have outermost dimension=" + outer);
                }
                if (!inner.equals(orgV.getDimension(1))) {
                    throw new IllegalArgumentException("Variable " + orgV.getNameAndDimensions() + " must have 2nd dimension=" + inner);
                }
                final VariableDS memberV = new VariableDS(ncfile, group, this, orgV.getShortName(), orgV.getDataType(), null, orgV.getUnitsString(), orgV.getDescription());
                memberV.setDataType(orgV.getDataType());
                memberV.setSPobject(orgV.getSPobject());
                memberV.getAttributes().addAll(orgV.getAttributes());
                final List<Dimension> dimList = new ArrayList<Dimension>(orgV.getDimensions());
                memberV.setDimensions(dimList.subList(2, dimList.size()));
                memberV.enhance(StructurePseudo2Dim.enhanceScaleMissing);
                this.addMemberVariable(memberV);
                this.orgVariables.add(orgV);
            }
        }
        this.calcElementSize();
    }
    
    @Override
    public Structure select(final List<String> memberNames) {
        final StructurePseudo2Dim result = new StructurePseudo2Dim((NetcdfDataset)this.ncfile, this.group, this.getShortName(), memberNames, this.getDimension(0), this.getDimension(1));
        result.isSubset = true;
        return result;
    }
    
    @Override
    protected Array _read(final Section section) throws IOException, InvalidRangeException {
        if (null == section) {
            return this._read();
        }
        if (this.debugRecord) {
            System.out.println(" read psuedo records " + section.getRange(0));
        }
        final String err = section.checkInRange(this.getShape());
        if (err != null) {
            throw new InvalidRangeException(err);
        }
        final Range outerRange = section.getRange(0);
        final Range innerRange = section.getRange(1);
        final StructureMembers smembers = this.makeStructureMembers();
        final ArrayStructureMA asma = new ArrayStructureMA(smembers, section.getShape());
        for (final Variable v : this.orgVariables) {
            final List<Range> vsection = new ArrayList<Range>(v.getRanges());
            vsection.set(0, outerRange);
            vsection.set(1, innerRange);
            final Array data = v.read(vsection);
            final StructureMembers.Member m = smembers.findMember(v.getShortName());
            m.setDataArray(data);
        }
        return asma;
    }
    
    static {
        StructurePseudo2Dim.log = LoggerFactory.getLogger(StructurePseudo2Dim.class);
    }
}
