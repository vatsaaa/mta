// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset;

import ucar.nc2.Attribute;
import java.util.Comparator;
import ucar.nc2.dataset.conv.NppConvention;
import ucar.nc2.dataset.conv.AvhrrConvention;
import ucar.nc2.dataset.conv.ModisSatellite;
import ucar.nc2.dataset.conv.FslWindProfiler;
import ucar.nc2.dataset.conv.NsslRadarMosaicConvention;
import ucar.nc2.dataset.conv.Suomi;
import ucar.nc2.dataset.conv.Jason2Convention;
import ucar.nc2.dataset.conv.Cosmic1Convention;
import ucar.nc2.dataset.conv.Nimbus;
import ucar.nc2.dataset.conv.EpicInsitu;
import ucar.nc2.dataset.conv.MADISStation;
import ucar.nc2.dataset.conv.ADASConvention;
import ucar.nc2.dataset.conv.IFPSConvention;
import ucar.nc2.dataset.conv.M3IOConvention;
import ucar.nc2.dataset.conv.M3IOVGGridConvention;
import ucar.nc2.dataset.conv.WRFConvention;
import ucar.nc2.dataset.conv.AWIPSsatConvention;
import ucar.nc2.dataset.conv.AWIPSConvention;
import ucar.nc2.dataset.conv.NUWGConvention;
import ucar.nc2.dataset.conv.IridlConvention;
import ucar.nc2.dataset.conv.GIEFConvention;
import ucar.nc2.dataset.conv.ZebraConvention;
import ucar.nc2.dataset.conv.CEDRICRadarConvention;
import ucar.nc2.dataset.conv.ATDRadarConvention;
import ucar.nc2.dataset.conv.GDVConvention;
import ucar.nc2.dataset.conv.UnidataObsConvention;
import ucar.nc2.dataset.conv.CSMConvention;
import ucar.nc2.dataset.conv.COARDSConvention;
import ucar.nc2.dataset.conv.CF1Convention;
import org.slf4j.LoggerFactory;
import ucar.ma2.DataType;
import ucar.nc2.VariableIF;
import java.util.Collection;
import ucar.nc2.Group;
import ucar.nc2.Structure;
import ucar.nc2.constants.AxisType;
import java.io.IOException;
import java.lang.reflect.Method;
import ucar.nc2.dataset.conv.DefaultConvention;
import ucar.nc2.NetcdfFile;
import ucar.nc2.ncml.NcMLReader;
import ucar.nc2.Variable;
import ucar.nc2.util.CancelTask;
import java.util.StringTokenizer;
import java.util.Iterator;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Formatter;
import ucar.nc2.Dimension;
import java.util.Map;
import java.util.List;
import org.slf4j.Logger;

public class CoordSysBuilder implements CoordSysBuilderIF
{
    public static final String resourcesDir = "resources/nj22/coords/";
    protected static Logger log;
    private static List<Convention> conventionList;
    private static Map<String, String> ncmlHash;
    private static boolean useMaximalCoordSys;
    private static boolean userMode;
    protected String conventionName;
    protected List<VarProcess> varList;
    protected Map<Dimension, List<VarProcess>> coordVarMap;
    protected Formatter parseInfo;
    protected Formatter userAdvice;
    protected boolean debug;
    protected boolean showRejects;
    
    public CoordSysBuilder() {
        this.conventionName = "_Coordinates";
        this.varList = new ArrayList<VarProcess>();
        this.coordVarMap = new HashMap<Dimension, List<VarProcess>>();
        this.parseInfo = new Formatter();
        this.userAdvice = new Formatter();
        this.debug = false;
        this.showRejects = false;
    }
    
    public static void registerNcML(final String conventionName, final String ncmlLocation) {
        CoordSysBuilder.ncmlHash.put(conventionName, ncmlLocation);
    }
    
    public static void registerConvention(final String conventionName, final Class c) {
        registerConvention(conventionName, c, null);
    }
    
    public static void registerConvention(final String conventionName, final Class c, final ConventionNameOk match) {
        if (!CoordSysBuilderIF.class.isAssignableFrom(c)) {
            throw new IllegalArgumentException("CoordSysBuilderIF Class " + c.getName() + " must implement CoordSysBuilderIF");
        }
        try {
            c.newInstance();
        }
        catch (InstantiationException e) {
            throw new IllegalArgumentException("CoordSysBuilderIF Class " + c.getName() + " cannot instantiate, probably need default Constructor");
        }
        catch (IllegalAccessException e2) {
            throw new IllegalArgumentException("CoordSysBuilderIF Class " + c.getName() + " is not accessible");
        }
        if (CoordSysBuilder.userMode) {
            CoordSysBuilder.conventionList.add(0, new Convention(conventionName, c, match));
        }
        else {
            CoordSysBuilder.conventionList.add(new Convention(conventionName, c, match));
        }
    }
    
    private static Class matchConvention(final String convName) {
        for (final Convention c : CoordSysBuilder.conventionList) {
            if (c.match == null && c.convName.equalsIgnoreCase(convName)) {
                return c.convClass;
            }
            if (c.match != null && c.match.isMatch(convName, c.convName)) {
                return c.convClass;
            }
        }
        return null;
    }
    
    public static void registerConvention(final String conventionName, final String className) throws ClassNotFoundException {
        final Class c = Class.forName(className);
        registerConvention(conventionName, c, null);
    }
    
    public static void setUseMaximalCoordSys(final boolean b) {
        CoordSysBuilder.useMaximalCoordSys = b;
    }
    
    public static boolean getUseMaximalCoordSys() {
        return CoordSysBuilder.useMaximalCoordSys;
    }
    
    private static List<String> breakupConventionNames(final String convName) {
        final List<String> names = new ArrayList<String>();
        if (convName.indexOf(44) > 0 || convName.indexOf(59) > 0) {
            final StringTokenizer stoke = new StringTokenizer(convName, ",;");
            while (stoke.hasMoreTokens()) {
                final String name = stoke.nextToken();
                names.add(name.trim());
            }
        }
        else if (convName.indexOf(47) > 0) {
            final StringTokenizer stoke = new StringTokenizer(convName, "/");
            while (stoke.hasMoreTokens()) {
                final String name = stoke.nextToken();
                names.add(name.trim());
            }
        }
        return names;
    }
    
    public static CoordSysBuilderIF factory(final NetcdfDataset ds, final CancelTask cancelTask) throws IOException {
        String convName = ds.findAttValueIgnoreCase(null, "Conventions", null);
        if (convName == null) {
            convName = ds.findAttValueIgnoreCase(null, "Convention", null);
        }
        if (convName != null) {
            convName = convName.trim();
        }
        if (convName != null) {
            final String convNcML = CoordSysBuilder.ncmlHash.get(convName);
            if (convNcML != null) {
                final CoordSysBuilder csb = new CoordSysBuilder();
                NcMLReader.wrapNcML(ds, convNcML, cancelTask);
                return csb;
            }
        }
        Class convClass = null;
        if (convName != null) {
            convClass = matchConvention(convName);
            if (convClass == null) {
                final List<String> names = breakupConventionNames(convName);
                if (names.size() > 0) {
                    for (final Convention conv : CoordSysBuilder.conventionList) {
                        for (final String name : names) {
                            if (name.equalsIgnoreCase(conv.convName)) {
                                convClass = conv.convClass;
                                convName = name;
                            }
                        }
                        if (convClass != null) {
                            break;
                        }
                    }
                }
            }
        }
        if (convClass == null) {
            convName = null;
            for (final Convention conv2 : CoordSysBuilder.conventionList) {
                final Class c = conv2.convClass;
                Method m;
                try {
                    m = c.getMethod("isMine", NetcdfFile.class);
                }
                catch (NoSuchMethodException ex2) {
                    continue;
                }
                try {
                    final Boolean result = (Boolean)m.invoke(null, ds);
                    if (result) {
                        convClass = c;
                        break;
                    }
                    continue;
                }
                catch (Exception ex) {
                    CoordSysBuilder.log.error("ERROR: Class " + c.getName() + " Exception invoking isMine method\n" + ex);
                }
            }
        }
        final boolean usingDefault = convClass == null;
        if (usingDefault) {
            convClass = DefaultConvention.class;
        }
        CoordSysBuilderIF builder;
        try {
            builder = convClass.newInstance();
        }
        catch (Exception e) {
            CoordSysBuilder.log.error("failed on CoordSysBuilderIF for " + convClass.getName(), e);
            return null;
        }
        if (usingDefault) {
            builder.addUserAdvice("No CoordSysBuilder found - using Default Conventions.\n");
        }
        if (convName == null) {
            builder.addUserAdvice("No 'Conventions' global attribute.\n");
        }
        else {
            builder.setConventionUsed(convClass.getName());
        }
        return builder;
    }
    
    public void setConventionUsed(final String convName) {
        this.conventionName = convName;
    }
    
    public String getConventionUsed() {
        return this.conventionName;
    }
    
    public void addUserAdvice(final String advice) {
        this.userAdvice.format("%s", advice);
    }
    
    public String getParseInfo() {
        return this.parseInfo.toString();
    }
    
    public String getUserAdvice() {
        return this.userAdvice.toString();
    }
    
    public void augmentDataset(final NetcdfDataset ncDataset, final CancelTask cancelTask) throws IOException {
    }
    
    protected AxisType getAxisType(final NetcdfDataset ncDataset, final VariableEnhanced v) {
        return null;
    }
    
    public void buildCoordinateSystems(final NetcdfDataset ncDataset) {
        this.parseInfo.format("Parsing with Convention = %s\n", this.conventionName);
        this.addVariables(ncDataset, ncDataset.getVariables(), this.varList);
        this.findCoordinateAxes(ncDataset);
        this.findCoordinateSystems(ncDataset);
        this.findCoordinateTransforms(ncDataset);
        this.makeCoordinateAxes(ncDataset);
        this.makeCoordinateSystems(ncDataset);
        this.assignCoordinateSystemsExplicit(ncDataset);
        this.makeCoordinateSystemsImplicit(ncDataset);
        if (CoordSysBuilder.useMaximalCoordSys) {
            this.makeCoordinateSystemsMaximal(ncDataset);
        }
        this.makeCoordinateTransforms(ncDataset);
        this.assignCoordinateTransforms(ncDataset);
        if (this.debug) {
            System.out.println("parseInfo = \n" + this.parseInfo.toString());
        }
    }
    
    private void addVariables(final NetcdfDataset ncDataset, final List<Variable> varList, final List<VarProcess> varProcessList) {
        for (final Variable v : varList) {
            varProcessList.add(new VarProcess(ncDataset, v));
            if (v instanceof Structure) {
                final List<Variable> nested = ((Structure)v).getVariables();
                this.addVariables(ncDataset, nested, varProcessList);
            }
        }
    }
    
    protected void findCoordinateAxes(final NetcdfDataset ncDataset) {
        for (final VarProcess vp : this.varList) {
            if (vp.coordAxes != null) {
                this.findCoordinateAxes(vp, vp.coordAxes);
            }
            if (vp.coordinates != null) {
                this.findCoordinateAxes(vp, vp.coordinates);
            }
        }
    }
    
    private void findCoordinateAxes(final VarProcess vp, final String coordinates) {
        final StringTokenizer stoker = new StringTokenizer(coordinates);
        while (stoker.hasMoreTokens()) {
            final String vname = stoker.nextToken();
            VarProcess ap = this.findVarProcess(vname);
            if (ap == null) {
                final Group g = vp.v.getParentGroup();
                final Variable v = g.findVariableOrInParent(vname);
                if (v != null) {
                    ap = this.findVarProcess(v.getName());
                }
                else {
                    this.parseInfo.format("***Cant find coordAxis %s referenced from var= %s\n", vname, vp.v.getName());
                    this.userAdvice.format("***Cant find coordAxis %s referenced from var= %s\n", vname, vp.v.getName());
                }
            }
            if (ap != null) {
                if (!ap.isCoordinateAxis) {
                    this.parseInfo.format(" CoordinateAxis = %s added; referenced from var= %s\n", vname, vp.v.getName());
                }
                ap.isCoordinateAxis = true;
            }
            else {
                this.parseInfo.format("***Cant find coordAxis %s referenced from var= %s\n", vname, vp.v.getName());
                this.userAdvice.format("***Cant find coordAxis %s referenced from var= %s\n", vname, vp.v.getName());
            }
        }
    }
    
    protected void findCoordinateSystems(final NetcdfDataset ncDataset) {
        for (final VarProcess vp : this.varList) {
            if (vp.coordSys != null) {
                final StringTokenizer stoker = new StringTokenizer(vp.coordSys);
                while (stoker.hasMoreTokens()) {
                    final String vname = stoker.nextToken();
                    final VarProcess ap = this.findVarProcess(vname);
                    if (ap != null) {
                        if (!ap.isCoordinateSystem) {
                            this.parseInfo.format(" CoordinateSystem = %s added; referenced from var= %s\n", vname, vp.v.getName());
                        }
                        ap.isCoordinateSystem = true;
                    }
                    else {
                        this.parseInfo.format("***Cant find coordSystem %s referenced from var= %s\n", vname, vp.v.getName());
                        this.userAdvice.format("***Cant find coordSystem %s referenced from var= %s\n", vname, vp.v.getName());
                    }
                }
            }
        }
    }
    
    protected void findCoordinateTransforms(final NetcdfDataset ncDataset) {
        for (final VarProcess vp : this.varList) {
            if (vp.coordTransforms != null) {
                final StringTokenizer stoker = new StringTokenizer(vp.coordTransforms);
                while (stoker.hasMoreTokens()) {
                    final String vname = stoker.nextToken();
                    final VarProcess ap = this.findVarProcess(vname);
                    if (ap != null) {
                        if (!ap.isCoordinateTransform) {
                            this.parseInfo.format(" CoordinateTransform = %s added; referenced from var= %s\n", vname, vp.v.getName());
                        }
                        ap.isCoordinateTransform = true;
                    }
                    else {
                        this.parseInfo.format("***Cant find CoordinateTransform %s referenced from var= %s\n", vname, vp.v.getName());
                        this.userAdvice.format("***Cant find CoordinateTransform %s referenced from var= %s\n", vname, vp.v.getName());
                    }
                }
            }
        }
    }
    
    protected void makeCoordinateAxes(final NetcdfDataset ncDataset) {
        for (final VarProcess vp : this.varList) {
            if (vp.isCoordinateAxis || vp.isCoordinateVariable) {
                if (vp.axisType == null) {
                    vp.axisType = this.getAxisType(ncDataset, (VariableEnhanced)vp.v);
                }
                if (vp.axisType == null) {
                    this.userAdvice.format("Coordinate Axis %s does not have an assigned AxisType\n", vp.v.getName());
                }
                vp.makeIntoCoordinateAxis();
            }
        }
    }
    
    protected void makeCoordinateSystems(final NetcdfDataset ncDataset) {
        for (final VarProcess vp : this.varList) {
            if (vp.isCoordinateSystem) {
                vp.makeCoordinateSystem();
            }
        }
    }
    
    protected void assignCoordinateSystemsExplicit(final NetcdfDataset ncDataset) {
        for (final VarProcess vp : this.varList) {
            if (vp.coordSys != null && !vp.isCoordinateTransform) {
                final StringTokenizer stoker = new StringTokenizer(vp.coordSys);
                while (stoker.hasMoreTokens()) {
                    final String vname = stoker.nextToken();
                    final VarProcess ap = this.findVarProcess(vname);
                    if (ap == null) {
                        this.parseInfo.format("***Cant find Coordinate System variable %s referenced from var= %s\n", vname, vp.v.getName());
                        this.userAdvice.format("***Cant find Coordinate System variable %s referenced from var= %s\n", vname, vp.v.getName());
                    }
                    else if (ap.cs == null) {
                        this.parseInfo.format("***Not a Coordinate System variable %s referenced from var= %s\n", vname, vp.v.getName());
                        this.userAdvice.format("***Not a Coordinate System variable %s referenced from var= %s\n", vname, vp.v.getName());
                    }
                    else {
                        final VariableEnhanced ve = (VariableEnhanced)vp.v;
                        ve.addCoordinateSystem(ap.cs);
                    }
                }
            }
        }
        for (final VarProcess csVar : this.varList) {
            if (csVar.isCoordinateSystem) {
                if (csVar.coordSysFor == null) {
                    continue;
                }
                final List<Dimension> dimList = new ArrayList<Dimension>(6);
                final StringTokenizer stoker2 = new StringTokenizer(csVar.coordSysFor);
                while (stoker2.hasMoreTokens()) {
                    final String dname = stoker2.nextToken();
                    final Dimension dim = ncDataset.getRootGroup().findDimension(dname);
                    if (dim == null) {
                        this.parseInfo.format("***Cant find Dimension %s referenced from CoordSys var= %s%n", dname, csVar.v.getName());
                        this.userAdvice.format("***Cant find Dimension %s referenced from CoordSys var= %s%n", dname, csVar.v.getName());
                    }
                    else {
                        dimList.add(dim);
                    }
                }
                for (final VarProcess vp2 : this.varList) {
                    if (!vp2.hasCoordinateSystem() && vp2.isData() && csVar.cs != null) {
                        final VariableEnhanced ve2 = (VariableEnhanced)vp2.v;
                        if (!CoordinateSystem.isSubset(dimList, vp2.v.getDimensionsAll()) || !CoordinateSystem.isSubset(vp2.v.getDimensionsAll(), dimList)) {
                            continue;
                        }
                        ve2.addCoordinateSystem(csVar.cs);
                    }
                }
            }
        }
        for (final VarProcess vp : this.varList) {
            final VariableEnhanced ve3 = (VariableEnhanced)vp.v;
            if (!vp.hasCoordinateSystem() && vp.coordAxes != null && vp.isData()) {
                final List<CoordinateAxis> dataAxesList = this.getAxes(vp.coordAxes, vp.v.getName());
                if (dataAxesList.size() <= 1) {
                    continue;
                }
                final String coordSysName = CoordinateSystem.makeName(dataAxesList);
                final CoordinateSystem cs = ncDataset.findCoordinateSystem(coordSysName);
                if (cs != null) {
                    ve3.addCoordinateSystem(cs);
                    this.parseInfo.format(" assigned explicit CoordSystem '%s' for var= %s\n", cs.getName(), vp.v.getName());
                }
                else {
                    final CoordinateSystem csnew = new CoordinateSystem(ncDataset, dataAxesList, null);
                    ve3.addCoordinateSystem(csnew);
                    ncDataset.addCoordinateSystem(csnew);
                    this.parseInfo.format(" created explicit CoordSystem '%s' for var= %s\n", csnew.getName(), vp.v.getName());
                }
            }
        }
    }
    
    private List<CoordinateAxis> getAxes(final String names, final String varName) {
        final List<CoordinateAxis> axesList = new ArrayList<CoordinateAxis>();
        final StringTokenizer stoker = new StringTokenizer(names);
        while (stoker.hasMoreTokens()) {
            final String vname = stoker.nextToken();
            final VarProcess ap = this.findVarProcess(vname);
            if (ap != null) {
                final CoordinateAxis axis = ap.makeIntoCoordinateAxis();
                if (axesList.contains(axis)) {
                    continue;
                }
                axesList.add(axis);
            }
            else {
                this.parseInfo.format("***Cant find Coordinate Axis %s referenced from var= %s\n", vname, varName);
                this.userAdvice.format("***Cant find Coordinate Axis %s referenced from var= %s\n", vname, varName);
            }
        }
        return axesList;
    }
    
    protected void makeCoordinateSystemsImplicit(final NetcdfDataset ncDataset) {
        for (final VarProcess vp : this.varList) {
            if (!vp.hasCoordinateSystem() && vp.maybeData()) {
                final List<CoordinateAxis> dataAxesList = vp.findCoordinateAxes(true);
                if (dataAxesList.size() < 2) {
                    continue;
                }
                final VariableEnhanced ve = (VariableEnhanced)vp.v;
                final String csName = CoordinateSystem.makeName(dataAxesList);
                final CoordinateSystem cs = ncDataset.findCoordinateSystem(csName);
                if (cs != null && cs.isComplete(vp.v)) {
                    ve.addCoordinateSystem(cs);
                    this.parseInfo.format(" assigned implicit CoordSystem '%s' for var= %s\n", cs.getName(), vp.v.getName());
                }
                else {
                    final CoordinateSystem csnew = new CoordinateSystem(ncDataset, dataAxesList, null);
                    csnew.setImplicit(true);
                    if (!csnew.isComplete(vp.v)) {
                        continue;
                    }
                    ve.addCoordinateSystem(csnew);
                    ncDataset.addCoordinateSystem(csnew);
                    this.parseInfo.format(" created implicit CoordSystem '%s' for var= %s\n", csnew.getName(), vp.v.getName());
                }
            }
        }
    }
    
    protected void makeCoordinateSystemsMaximal(final NetcdfDataset ncDataset) {
        for (final VarProcess vp : this.varList) {
            final VariableEnhanced ve = (VariableEnhanced)vp.v;
            CoordinateSystem implicit = null;
            if (!vp.hasCoordinateSystem()) {
                if (!vp.isData()) {
                    continue;
                }
                CoordinateSystem existing = null;
                if (ve.getCoordinateSystems().size() == 1) {
                    existing = ve.getCoordinateSystems().get(0);
                    if (!existing.isImplicit()) {
                        continue;
                    }
                    if (existing.getRankRange() >= ve.getRank()) {
                        continue;
                    }
                    implicit = existing;
                }
                final List<CoordinateAxis> axisList = new ArrayList<CoordinateAxis>();
                final List<CoordinateAxis> axes = ncDataset.getCoordinateAxes();
                for (final CoordinateAxis axis : axes) {
                    if (this.isCoordinateAxisForVariable(axis, ve)) {
                        axisList.add(axis);
                    }
                }
                if (existing != null && axisList.size() <= existing.getRankRange()) {
                    continue;
                }
                if (axisList.size() < 2) {
                    continue;
                }
                final String csName = CoordinateSystem.makeName(axisList);
                final CoordinateSystem cs = ncDataset.findCoordinateSystem(csName);
                if (cs != null) {
                    if (null != implicit) {
                        ve.removeCoordinateSystem(implicit);
                    }
                    ve.addCoordinateSystem(cs);
                    this.parseInfo.format(" assigned maximal CoordSystem '%s' for var= %s\n", cs.getName(), ve.getName());
                }
                else {
                    final CoordinateSystem csnew = new CoordinateSystem(ncDataset, axisList, null);
                    csnew.setImplicit(true);
                    if (null != implicit) {
                        ve.removeCoordinateSystem(implicit);
                    }
                    ve.addCoordinateSystem(csnew);
                    ncDataset.addCoordinateSystem(csnew);
                    this.parseInfo.format(" created maximal CoordSystem '%s' for var= %s\n", csnew.getName(), ve.getName());
                }
            }
        }
    }
    
    protected boolean isCoordinateAxisForVariable(final Variable axis, final VariableEnhanced v) {
        final List<Dimension> varDims = v.getDimensionsAll();
        int checkDims = axis.getRank();
        if (axis.getDataType() == DataType.CHAR) {
            --checkDims;
        }
        for (int i = 0; i < checkDims; ++i) {
            final Dimension axisDim = axis.getDimension(i);
            if (!varDims.contains(axisDim)) {
                return false;
            }
        }
        return true;
    }
    
    protected boolean hasXY(final List<CoordinateAxis> coordAxes) {
        boolean hasX = false;
        boolean hasY = false;
        boolean hasLat = false;
        boolean hasLon = false;
        for (final CoordinateAxis axis : coordAxes) {
            final AxisType axisType = axis.getAxisType();
            if (axisType == AxisType.GeoX) {
                hasX = true;
            }
            if (axisType == AxisType.GeoY) {
                hasY = true;
            }
            if (axisType == AxisType.Lat) {
                hasLat = true;
            }
            if (axisType == AxisType.Lon) {
                hasLon = true;
            }
        }
        return (hasLat && hasLon) || (hasX && hasY);
    }
    
    protected void makeCoordinateTransforms(final NetcdfDataset ncDataset) {
        for (final VarProcess vp : this.varList) {
            if (vp.isCoordinateTransform && vp.ct == null) {
                vp.ct = CoordTransBuilder.makeCoordinateTransform(vp.ds, vp.v, this.parseInfo, this.userAdvice);
            }
        }
    }
    
    protected CoordinateTransform makeCoordinateTransform(final NetcdfDataset ds, final Variable ctv) {
        return CoordTransBuilder.makeCoordinateTransform(ds, ctv, this.parseInfo, this.userAdvice);
    }
    
    protected void assignCoordinateTransforms(final NetcdfDataset ncDataset) {
        for (final VarProcess vp : this.varList) {
            if (vp.isCoordinateSystem && vp.coordTransforms != null) {
                final StringTokenizer stoker = new StringTokenizer(vp.coordTransforms);
                while (stoker.hasMoreTokens()) {
                    final String vname = stoker.nextToken();
                    final VarProcess ap = this.findVarProcess(vname);
                    if (ap != null) {
                        if (ap.ct != null) {
                            vp.addCoordinateTransform(ap.ct);
                            this.parseInfo.format(" assign explicit coordTransform %s to CoordSys= %s\n", ap.ct, vp.cs);
                        }
                        else {
                            this.parseInfo.format("***Cant find coordTransform in %s referenced from var= %s\n", vname, vp.v.getName());
                            this.userAdvice.format("***Cant find coordTransform in %s referenced from var= %s\n", vname, vp.v.getName());
                        }
                    }
                    else {
                        this.parseInfo.format("***Cant find coordTransform variable= %s referenced from var= %s\n", vname, vp.v.getName());
                        this.userAdvice.format("***Cant find coordTransform variable= %s referenced from var= %s\n", vname, vp.v.getName());
                    }
                }
            }
        }
        for (final VarProcess vp : this.varList) {
            if (vp.isCoordinateTransform && vp.ct != null && vp.coordSys != null) {
                final StringTokenizer stoker = new StringTokenizer(vp.coordSys);
                while (stoker.hasMoreTokens()) {
                    final String vname = stoker.nextToken();
                    final VarProcess vcs = this.findVarProcess(vname);
                    if (vcs == null) {
                        this.parseInfo.format("***Cant find coordSystem variable= %s referenced from var= %s\n", vname, vp.v.getName());
                        this.userAdvice.format("***Cant find coordSystem variable= %s referenced from var= %s\n", vname, vp.v.getName());
                    }
                    else {
                        vcs.addCoordinateTransform(vp.ct);
                        this.parseInfo.format("***assign explicit coordTransform %s to CoordSys=  %s\n", vp.ct, vp.cs);
                    }
                }
            }
        }
        for (final VarProcess vp : this.varList) {
            if (vp.isCoordinateTransform && vp.ct != null && vp.coordAxes != null) {
                final List<CoordinateAxis> dataAxesList = vp.findCoordinateAxes(false);
                if (dataAxesList.size() <= 0) {
                    continue;
                }
                for (final CoordinateSystem cs : ncDataset.getCoordinateSystems()) {
                    if (cs.containsAxes(dataAxesList)) {
                        cs.addCoordinateTransform(vp.ct);
                        this.parseInfo.format("***assign (implicit coordAxes) coordTransform %s to CoordSys=  %s\n", vp.ct, cs);
                    }
                }
            }
        }
        for (final VarProcess vp : this.varList) {
            if (vp.isCoordinateTransform && vp.ct != null && vp.coordAxisTypes != null) {
                final List<AxisType> axisTypesList = new ArrayList<AxisType>();
                final StringTokenizer stoker2 = new StringTokenizer(vp.coordAxisTypes);
                while (stoker2.hasMoreTokens()) {
                    final String name = stoker2.nextToken();
                    final AxisType atype;
                    if (null != (atype = AxisType.getType(name))) {
                        axisTypesList.add(atype);
                    }
                }
                if (axisTypesList.size() <= 0) {
                    continue;
                }
                for (final CoordinateSystem cs2 : ncDataset.getCoordinateSystems()) {
                    if (cs2.containsAxisTypes(axisTypesList)) {
                        cs2.addCoordinateTransform(vp.ct);
                        this.parseInfo.format("***assign (implicit coordAxisType) coordTransform %s to CoordSys=  %s\n", vp.ct, cs2);
                    }
                }
            }
        }
    }
    
    protected VarProcess findVarProcess(final String fullName) {
        if (fullName == null) {
            return null;
        }
        for (final VarProcess vp : this.varList) {
            if (fullName.equals(vp.v.getName())) {
                return vp;
            }
        }
        return null;
    }
    
    protected VarProcess findCoordinateAxis(final String name) {
        if (name == null) {
            return null;
        }
        for (final VarProcess vp : this.varList) {
            if (name.equals(vp.v.getName()) && (vp.isCoordinateVariable || vp.isCoordinateAxis)) {
                return vp;
            }
        }
        return null;
    }
    
    protected void addCoordinateVariable(final Dimension dim, final VarProcess vp) {
        List<VarProcess> list = this.coordVarMap.get(dim);
        if (list == null) {
            list = new ArrayList<VarProcess>();
            this.coordVarMap.put(dim, list);
        }
        if (!list.contains(vp)) {
            list.add(vp);
        }
    }
    
    protected VariableDS makeCoordinateTransformVariable(final NetcdfDataset ds, final CoordinateTransform ct) {
        final VariableDS v = CoordTransBuilder.makeDummyTransformVariable(ds, ct);
        this.parseInfo.format("  made CoordinateTransformVariable: %s\n", ct.getName());
        return v;
    }
    
    @Deprecated
    public static VariableDS makeDummyTransformVariable(final NetcdfDataset ds, final CoordinateTransform ct) {
        return CoordTransBuilder.makeDummyTransformVariable(ds, ct);
    }
    
    static {
        CoordSysBuilder.log = LoggerFactory.getLogger(CoordSysBuilder.class);
        CoordSysBuilder.conventionList = new ArrayList<Convention>();
        CoordSysBuilder.ncmlHash = new HashMap<String, String>();
        CoordSysBuilder.useMaximalCoordSys = true;
        CoordSysBuilder.userMode = false;
        registerConvention("_Coordinates", CoordSysBuilder.class, null);
        registerConvention("CF-1.", CF1Convention.class, new ConventionNameOk() {
            public boolean isMatch(final String convName, final String wantName) {
                if (convName.startsWith(wantName)) {
                    return true;
                }
                final List<String> names = breakupConventionNames(convName);
                for (final String name : names) {
                    if (name.startsWith(wantName)) {
                        return true;
                    }
                }
                return false;
            }
        });
        registerConvention("COARDS", COARDSConvention.class, null);
        registerConvention("NCAR-CSM", CSMConvention.class, null);
        registerConvention("Unidata Observation Dataset v1.0", UnidataObsConvention.class, null);
        registerConvention("GDV", GDVConvention.class, null);
        registerConvention("ATDRadar", ATDRadarConvention.class, null);
        registerConvention("CEDRICRadar", CEDRICRadarConvention.class, null);
        registerConvention("Zebra", ZebraConvention.class, null);
        registerConvention("GIEF/GIEF-F", GIEFConvention.class, null);
        registerConvention("IRIDL", IridlConvention.class, null);
        registerConvention("NUWG", NUWGConvention.class, null);
        registerConvention("AWIPS", AWIPSConvention.class, null);
        registerConvention("AWIPS-Sat", AWIPSsatConvention.class, null);
        registerConvention("WRF", WRFConvention.class, null);
        registerConvention("M3IOVGGrid", M3IOVGGridConvention.class, null);
        registerConvention("M3IO", M3IOConvention.class, null);
        registerConvention("IFPS", IFPSConvention.class, null);
        registerConvention("ARPS/ADAS", ADASConvention.class, null);
        registerConvention("MADIS surface observations, v1.0", MADISStation.class, null);
        registerConvention("epic-insitu-1.0", EpicInsitu.class, null);
        registerConvention("NCAR-RAF/nimbus", Nimbus.class, null);
        registerConvention("Cosmic1Convention", Cosmic1Convention.class, null);
        registerConvention("Jason2Convention", Jason2Convention.class, null);
        registerConvention("Suomi", Suomi.class, null);
        registerConvention("NSSL National Reflectivity Mosaic", NsslRadarMosaicConvention.class, null);
        registerConvention("FslWindProfiler", FslWindProfiler.class, null);
        registerConvention("ModisSatellite", ModisSatellite.class, null);
        registerConvention("AvhrrSatellite", AvhrrConvention.class, null);
        registerConvention("NPP/NPOESS", NppConvention.class, null);
        CoordSysBuilder.userMode = true;
    }
    
    private static class Convention
    {
        String convName;
        Class convClass;
        ConventionNameOk match;
        
        Convention(final String convName, final Class convClass, final ConventionNameOk match) {
            this.convName = convName;
            this.convClass = convClass;
            this.match = match;
        }
    }
    
    private class VarProcessSorter implements Comparator<VarProcess>
    {
        public int compare(final VarProcess o1, final VarProcess o2) {
            return o2.v.getRank() - o1.v.getRank();
        }
    }
    
    public class VarProcess
    {
        public NetcdfDataset ds;
        public Variable v;
        public boolean isCoordinateVariable;
        public boolean isCoordinateAxis;
        public AxisType axisType;
        public String coordAxes;
        public String coordSys;
        public String coordSysFor;
        public String coordVarAlias;
        public String positive;
        public String coordAxisTypes;
        public String coordinates;
        public CoordinateAxis axis;
        public boolean isCoordinateSystem;
        public String coordTransforms;
        public CoordinateSystem cs;
        public boolean isCoordinateTransform;
        public String coordTransformType;
        public CoordinateTransform ct;
        
        private VarProcess(final NetcdfDataset ds, final Variable v) {
            this.ds = ds;
            this.v = v;
            final VariableEnhanced ve = (VariableEnhanced)v;
            this.isCoordinateVariable = v.isCoordinateVariable();
            if (this.isCoordinateVariable) {
                CoordSysBuilder.this.addCoordinateVariable(v.getDimension(0), this);
                CoordSysBuilder.this.parseInfo.format(" Coordinate Variable added = %s for dimension %s\n", v.getName(), v.getDimension(0));
            }
            final Attribute att = v.findAttributeIgnoreCase("_CoordinateAxisType");
            if (att != null) {
                final String axisName = att.getStringValue();
                this.axisType = AxisType.getType(axisName);
                this.isCoordinateAxis = true;
                CoordSysBuilder.this.parseInfo.format(" Coordinate Axis added = %s type= %s\n", v.getName(), axisName);
            }
            this.coordVarAlias = ds.findAttValueIgnoreCase(v, "_CoordinateAliasForDimension", null);
            if (this.coordVarAlias != null) {
                this.coordVarAlias = this.coordVarAlias.trim();
                if (v.getRank() != 1) {
                    CoordSysBuilder.this.parseInfo.format("**ERROR Coordinate Variable Alias %s has rank %d\n", v.getName(), v.getRank());
                    CoordSysBuilder.this.userAdvice.format("**ERROR Coordinate Variable Alias %s has rank %d\n", v.getName(), v.getRank());
                }
                else {
                    final Dimension coordDim = ds.findDimension(this.coordVarAlias);
                    final Dimension vDim = v.getDimension(0);
                    if (!coordDim.equals(vDim)) {
                        CoordSysBuilder.this.parseInfo.format("**ERROR Coordinate Variable Alias %s names wrong dimension %s\n", v.getName(), this.coordVarAlias);
                        CoordSysBuilder.this.userAdvice.format("**ERROR Coordinate Variable Alias %s names wrong dimension %s\n", v.getName(), this.coordVarAlias);
                    }
                    else {
                        this.isCoordinateAxis = true;
                        CoordSysBuilder.this.addCoordinateVariable(coordDim, this);
                        CoordSysBuilder.this.parseInfo.format(" Coordinate Variable Alias added = %s for dimension= %s\n", v.getName(), this.coordVarAlias);
                    }
                }
            }
            this.positive = ds.findAttValueIgnoreCase(v, "_CoordinateZisPositive", null);
            if (this.positive == null) {
                this.positive = ds.findAttValueIgnoreCase(v, "positive", null);
            }
            else {
                this.isCoordinateAxis = true;
                this.positive = this.positive.trim();
                CoordSysBuilder.this.parseInfo.format(" Coordinate Axis added(from positive attribute ) = %s for dimension= %s\n", v.getName(), this.coordVarAlias);
            }
            this.coordAxes = ds.findAttValueIgnoreCase(v, "_CoordinateAxes", null);
            this.coordSys = ds.findAttValueIgnoreCase(v, "_CoordinateSystems", null);
            this.coordSysFor = ds.findAttValueIgnoreCase(v, "_CoordinateSystemFor", null);
            this.coordTransforms = ds.findAttValueIgnoreCase(v, "_CoordinateTransforms", null);
            this.isCoordinateSystem = (this.coordTransforms != null || this.coordSysFor != null);
            this.coordAxisTypes = ds.findAttValueIgnoreCase(v, "_CoordinateAxisTypes", null);
            this.coordTransformType = ds.findAttValueIgnoreCase(v, "_CoordinateTransformType", null);
            this.isCoordinateTransform = (this.coordTransformType != null || this.coordAxisTypes != null);
            if (!this.isCoordinateSystem && !this.isCoordinateTransform && !this.isCoordinateAxis && this.coordAxes != null) {
                final StringTokenizer stoker = new StringTokenizer(this.coordAxes);
                while (stoker.hasMoreTokens()) {
                    final String vname = stoker.nextToken();
                    final Variable axis = ds.findVariable(vname);
                    if (axis != null && !CoordSysBuilder.this.isCoordinateAxisForVariable(axis, ve)) {
                        this.isCoordinateSystem = true;
                    }
                }
            }
        }
        
        public VarProcess(final NetcdfDataset ds) {
            this.ds = ds;
        }
        
        public boolean isData() {
            return !this.isCoordinateVariable && !this.isCoordinateAxis && !this.isCoordinateSystem && !this.isCoordinateTransform;
        }
        
        public boolean maybeData() {
            return !this.isCoordinateVariable && !this.isCoordinateSystem && !this.isCoordinateTransform;
        }
        
        public boolean hasCoordinateSystem() {
            return ((VariableEnhanced)this.v).getCoordinateSystems().size() > 0;
        }
        
        @Override
        public String toString() {
            return this.v.getShortName();
        }
        
        public CoordinateAxis makeIntoCoordinateAxis() {
            if (this.axis != null) {
                return this.axis;
            }
            final CoordinateAxis addCoordinateAxis = this.ds.addCoordinateAxis((VariableDS)this.v);
            this.axis = addCoordinateAxis;
            this.v = addCoordinateAxis;
            if (this.axisType != null) {
                this.axis.setAxisType(this.axisType);
                this.axis.addAttribute(new Attribute("_CoordinateAxisType", this.axisType.toString()));
                if ((this.axisType == AxisType.Height || this.axisType == AxisType.Pressure || this.axisType == AxisType.GeoZ) && this.positive != null) {
                    this.axis.setPositive(this.positive);
                    this.axis.addAttribute(new Attribute("_CoordinateZisPositive", this.positive));
                }
            }
            return this.axis;
        }
        
        public void makeCoordinateSystem() {
            final List<CoordinateAxis> axesList = new ArrayList<CoordinateAxis>();
            if (this.coordAxes != null) {
                final StringTokenizer stoker = new StringTokenizer(this.coordAxes);
                while (stoker.hasMoreTokens()) {
                    final String vname = stoker.nextToken();
                    final VarProcess ap = CoordSysBuilder.this.findVarProcess(vname);
                    if (ap != null) {
                        final CoordinateAxis axis = ap.makeIntoCoordinateAxis();
                        if (axesList.contains(axis)) {
                            continue;
                        }
                        axesList.add(axis);
                    }
                    else {
                        CoordSysBuilder.this.parseInfo.format(" Cant find axes %s for Coordinate System %s\n", vname, this.v.getName());
                        CoordSysBuilder.this.userAdvice.format(" Cant find axes %s for Coordinate System %s\n", vname, this.v.getName());
                    }
                }
            }
            if (axesList.size() == 0) {
                CoordSysBuilder.this.parseInfo.format(" No axes found for Coordinate System %s\n", this.v.getName());
                CoordSysBuilder.this.userAdvice.format(" No axes found for Coordinate System %s\n", this.v.getName());
                return;
            }
            this.cs = new CoordinateSystem(this.ds, axesList, null);
            this.ds.addCoordinateSystem(this.cs);
            CoordSysBuilder.this.parseInfo.format(" Made Coordinate System %s for ", this.cs.getName());
            this.v.getNameAndDimensions(CoordSysBuilder.this.parseInfo, true, false);
            CoordSysBuilder.this.parseInfo.format(" from %s\n", this.coordAxes);
        }
        
        public List<CoordinateAxis> findCoordinateAxes(final boolean addCoordVariables) {
            final List<CoordinateAxis> axesList = new ArrayList<CoordinateAxis>();
            if (this.coordAxes != null) {
                final StringTokenizer stoker = new StringTokenizer(this.coordAxes);
                while (stoker.hasMoreTokens()) {
                    final String vname = stoker.nextToken();
                    final VarProcess ap = CoordSysBuilder.this.findVarProcess(vname);
                    if (ap != null) {
                        final CoordinateAxis axis = ap.makeIntoCoordinateAxis();
                        if (axesList.contains(axis)) {
                            continue;
                        }
                        axesList.add(axis);
                    }
                }
            }
            else if (this.coordinates != null) {
                final StringTokenizer stoker = new StringTokenizer(this.coordinates);
                while (stoker.hasMoreTokens()) {
                    final String vname = stoker.nextToken();
                    final VarProcess ap = CoordSysBuilder.this.findVarProcess(vname);
                    if (ap != null) {
                        final CoordinateAxis axis = ap.makeIntoCoordinateAxis();
                        if (axesList.contains(axis)) {
                            continue;
                        }
                        axesList.add(axis);
                    }
                }
            }
            if (addCoordVariables) {
                for (final Dimension d : this.v.getDimensions()) {
                    final List<VarProcess> coordVars = CoordSysBuilder.this.coordVarMap.get(d);
                    if (coordVars == null) {
                        continue;
                    }
                    for (final VarProcess vp : coordVars) {
                        final CoordinateAxis axis2 = vp.makeIntoCoordinateAxis();
                        if (!axesList.contains(axis2)) {
                            axesList.add(axis2);
                        }
                    }
                }
            }
            return axesList;
        }
        
        void addCoordinateTransform(final CoordinateTransform ct) {
            if (this.cs == null) {
                CoordSysBuilder.this.parseInfo.format("  %s: no CoordinateSystem for CoordinateTransformVariable: %s\n", this.v.getName(), ct.getName());
                return;
            }
            this.cs.addCoordinateTransform(ct);
        }
    }
    
    public interface ConventionNameOk
    {
        boolean isMatch(final String p0, final String p1);
    }
}
