// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset;

import java.util.HashMap;
import ucar.unidata.geoloc.vertical.VerticalTransform;
import ucar.nc2.Dimension;
import java.util.Map;
import net.jcip.annotations.Immutable;

@Immutable
public class VerticalCT extends CoordinateTransform
{
    private final Type type;
    private final CoordTransBuilderIF builder;
    private static Map<String, Type> hash;
    
    public VerticalCT(final String name, final String authority, final Type type, final CoordTransBuilderIF builder) {
        super(name, authority, TransformType.Vertical);
        this.type = type;
        this.builder = builder;
    }
    
    public VerticalCT(final VerticalCT from) {
        super(from.getName(), from.getAuthority(), from.getTransformType());
        this.type = from.getVerticalTransformType();
        this.builder = from.getBuilder();
    }
    
    public Type getVerticalTransformType() {
        return this.type;
    }
    
    public VerticalTransform makeVerticalTransform(final NetcdfDataset ds, final Dimension timeDim) {
        return this.builder.makeMathTransform(ds, timeDim, this);
    }
    
    public CoordTransBuilderIF getBuilder() {
        return this.builder;
    }
    
    @Override
    public String toString() {
        return "VerticalCT {type=" + this.type + ", builder=" + this.builder.getTransformName() + '}';
    }
    
    static {
        VerticalCT.hash = new HashMap<String, Type>(10);
    }
    
    public static class Type
    {
        public static final Type Sigma;
        public static final Type HybridSigmaPressure;
        public static final Type HybridHeight;
        public static final Type Sleve;
        public static final Type OceanSigma;
        public static final Type OceanS;
        public static final Type OceanSigmaZ;
        public static final Type Existing3DField;
        public static final Type WRFEta;
        public static final Type OceanSG1;
        public static final Type OceanSG2;
        private final String name;
        
        public Type(final String s) {
            this.name = s;
            VerticalCT.hash.put(s, this);
        }
        
        public static Type getType(final String name) {
            if (name == null) {
                return null;
            }
            return VerticalCT.hash.get(name);
        }
        
        @Override
        public String toString() {
            return this.name;
        }
        
        static {
            Sigma = new Type("atmosphere_sigma");
            HybridSigmaPressure = new Type("atmosphere_hybrid_sigma_pressure");
            HybridHeight = new Type("atmosphere_hybrid_height");
            Sleve = new Type("atmosphere_sleve");
            OceanSigma = new Type("ocean_sigma");
            OceanS = new Type("ocean_s");
            OceanSigmaZ = new Type("ocean_sigma_z");
            Existing3DField = new Type("Existing3DField");
            WRFEta = new Type("WRFEta");
            OceanSG1 = new Type("ocean_s_g1");
            OceanSG2 = new Type("ocean_s_g2");
        }
    }
}
