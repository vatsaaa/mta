// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset;

import java.util.Formatter;
import ucar.unidata.geoloc.vertical.VerticalTransform;
import ucar.nc2.Dimension;
import ucar.nc2.Variable;

public interface CoordTransBuilderIF
{
    CoordinateTransform makeCoordinateTransform(final NetcdfDataset p0, final Variable p1);
    
    VerticalTransform makeMathTransform(final NetcdfDataset p0, final Dimension p1, final VerticalCT p2);
    
    String getTransformName();
    
    TransformType getTransformType();
    
    void setErrorBuffer(final Formatter p0);
}
