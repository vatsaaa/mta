// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset;

import ucar.nc2.dataset.transform.VOceanSG2;
import ucar.nc2.dataset.transform.VOceanSG1;
import ucar.nc2.dataset.transform.VExplicitField;
import ucar.nc2.dataset.transform.VOceanSigma;
import ucar.nc2.dataset.transform.VOceanS;
import ucar.nc2.dataset.transform.VAtmSigma;
import ucar.nc2.dataset.transform.VAtmHybridSigmaPressure;
import ucar.nc2.dataset.transform.VAtmHybridHeight;
import ucar.nc2.dataset.transform.UTM;
import ucar.nc2.dataset.transform.VerticalPerspective;
import ucar.nc2.dataset.transform.TransverseMercator;
import ucar.nc2.dataset.transform.Stereographic;
import ucar.nc2.dataset.transform.RotatedLatLon;
import ucar.nc2.dataset.transform.RotatedPole;
import ucar.nc2.dataset.transform.PolarStereographic;
import ucar.nc2.dataset.transform.Orthographic;
import ucar.nc2.dataset.transform.MSGnavigation;
import ucar.nc2.dataset.transform.Mercator;
import ucar.nc2.dataset.transform.LambertConformalConic;
import ucar.nc2.dataset.transform.LambertAzimuthal;
import ucar.nc2.dataset.transform.FlatEarth;
import ucar.nc2.dataset.transform.AlbersEqualArea;
import java.util.ArrayList;
import org.slf4j.LoggerFactory;
import ucar.ma2.Array;
import ucar.nc2.Attribute;
import ucar.unidata.util.Parameter;
import ucar.nc2.Structure;
import ucar.nc2.Group;
import ucar.ma2.DataType;
import java.util.Iterator;
import java.util.Formatter;
import ucar.nc2.Variable;
import java.util.List;
import org.slf4j.Logger;

public class CoordTransBuilder
{
    private static Logger log;
    private static List<Transform> transformList;
    private static boolean userMode;
    private static boolean loadWarnings;
    
    public static void registerTransform(final String transformName, final Class c) {
        if (!CoordTransBuilderIF.class.isAssignableFrom(c)) {
            throw new IllegalArgumentException("Class " + c.getName() + " must implement CoordTransBuilderIF");
        }
        try {
            c.newInstance();
        }
        catch (InstantiationException e) {
            throw new IllegalArgumentException("CoordTransBuilderIF Class " + c.getName() + " cannot instantiate, probably need default Constructor");
        }
        catch (IllegalAccessException e2) {
            throw new IllegalArgumentException("CoordTransBuilderIF Class " + c.getName() + " is not accessible");
        }
        if (CoordTransBuilder.userMode) {
            CoordTransBuilder.transformList.add(0, new Transform(transformName, c));
        }
        else {
            CoordTransBuilder.transformList.add(new Transform(transformName, c));
        }
    }
    
    public static void registerTransform(final String transformName, final String className) throws ClassNotFoundException {
        final Class c = Class.forName(className);
        registerTransform(transformName, c);
    }
    
    public static void registerTransformMaybe(final String transformName, final String className) {
        Class c = null;
        try {
            c = Class.forName(className);
        }
        catch (ClassNotFoundException e) {
            CoordTransBuilder.log.warn("Coordinate Transform Class " + className + " not found.");
        }
        registerTransform(transformName, c);
    }
    
    public static CoordinateTransform makeCoordinateTransform(final NetcdfDataset ds, final Variable ctv, final Formatter parseInfo, final Formatter errInfo) {
        String transform_name = ds.findAttValueIgnoreCase(ctv, "transform_name", null);
        if (null == transform_name) {
            transform_name = ds.findAttValueIgnoreCase(ctv, "Projection_Name", null);
        }
        if (null == transform_name) {
            transform_name = ds.findAttValueIgnoreCase(ctv, "grid_mapping_name", null);
        }
        if (null == transform_name) {
            transform_name = ds.findAttValueIgnoreCase(ctv, "standard_name", null);
        }
        if (null == transform_name) {
            parseInfo.format("**Failed to find Coordinate Transform name from Variable= %s\n", ctv);
            return null;
        }
        transform_name = transform_name.trim();
        Class builderClass = null;
        for (final Transform transform : CoordTransBuilder.transformList) {
            if (transform.transName.equals(transform_name)) {
                builderClass = transform.transClass;
                break;
            }
        }
        if (null == builderClass) {
            parseInfo.format("**Failed to find CoordTransBuilder name= %s from Variable= %s\n", transform_name, ctv);
            return null;
        }
        CoordTransBuilderIF builder = null;
        try {
            builder = builderClass.newInstance();
        }
        catch (InstantiationException e) {
            CoordTransBuilder.log.error("Cant instantiate " + builderClass.getName(), e);
        }
        catch (IllegalAccessException e2) {
            CoordTransBuilder.log.error("Cant access " + builderClass.getName(), e2);
        }
        if (null == builder) {
            parseInfo.format("**Failed to build CoordTransBuilder object from class= %s for Variable= %s\n", builderClass.getName(), ctv);
            return null;
        }
        builder.setErrorBuffer(errInfo);
        final CoordinateTransform ct = builder.makeCoordinateTransform(ds, ctv);
        if (ct != null) {
            parseInfo.format(" Made Coordinate transform %s from variable %s: %s\n", transform_name, ctv.getName(), builder);
        }
        return ct;
    }
    
    public static VariableDS makeDummyTransformVariable(final NetcdfDataset ds, final CoordinateTransform ct) {
        final VariableDS v = new VariableDS(ds, null, null, ct.getName(), DataType.CHAR, "", null, null);
        final List<Parameter> params = ct.getParameters();
        for (final Parameter p : params) {
            if (p.isString()) {
                v.addAttribute(new Attribute(p.getName(), p.getStringValue()));
            }
            else {
                final double[] data = p.getNumericValues();
                final Array dataA = Array.factory(Double.TYPE, new int[] { data.length }, data);
                v.addAttribute(new Attribute(p.getName(), dataA));
            }
        }
        v.addAttribute(new Attribute("_CoordinateTransformType", ct.getTransformType().toString()));
        final Array data2 = Array.factory(DataType.CHAR.getPrimitiveClassType(), new int[0], new char[] { ' ' });
        v.setCachedData(data2, true);
        return v;
    }
    
    static {
        CoordTransBuilder.log = LoggerFactory.getLogger(CoordTransBuilder.class);
        CoordTransBuilder.transformList = new ArrayList<Transform>();
        CoordTransBuilder.userMode = false;
        CoordTransBuilder.loadWarnings = false;
        registerTransform("albers_conical_equal_area", AlbersEqualArea.class);
        registerTransform("flat_earth", FlatEarth.class);
        registerTransform("lambert_azimuthal_equal_area", LambertAzimuthal.class);
        registerTransform("lambert_conformal_conic", LambertConformalConic.class);
        registerTransformMaybe("mcidas_area", "ucar.nc2.iosp.mcidas.McIDASAreaTransformBuilder");
        registerTransform("mercator", Mercator.class);
        registerTransform("MSGnavigation", MSGnavigation.class);
        registerTransform("orthographic", Orthographic.class);
        registerTransform("polar_stereographic", PolarStereographic.class);
        registerTransform("rotated_latitude_longitude", RotatedPole.class);
        registerTransform("rotated_latlon_grib", RotatedLatLon.class);
        registerTransform("stereographic", Stereographic.class);
        registerTransform("transverse_mercator", TransverseMercator.class);
        registerTransform("vertical_perspective", VerticalPerspective.class);
        registerTransform("UTM", UTM.class);
        registerTransform("atmosphere_hybrid_height_coordinate", VAtmHybridHeight.class);
        registerTransform("atmosphere_hybrid_sigma_pressure_coordinate", VAtmHybridSigmaPressure.class);
        registerTransform("atmosphere_sigma_coordinate", VAtmSigma.class);
        registerTransform("ocean_s_coordinate", VOceanS.class);
        registerTransform("ocean_sigma_coordinate", VOceanSigma.class);
        registerTransform("explicit_field", VExplicitField.class);
        registerTransform("existing3DField", VExplicitField.class);
        registerTransform("ocean_s_coordinate_g1", VOceanSG1.class);
        registerTransform("ocean_s_coordinate_g2", VOceanSG2.class);
        CoordTransBuilder.userMode = true;
    }
    
    private static class Transform
    {
        String transName;
        Class transClass;
        
        Transform(final String transName, final Class transClass) {
            this.transName = transName;
            this.transClass = transClass;
        }
    }
}
