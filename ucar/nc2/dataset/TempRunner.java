// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset;

import java.util.ArrayList;
import ucar.nc2.NetcdfFile;
import java.io.OutputStream;
import java.io.FileOutputStream;
import ucar.nc2.util.CancelTask;
import ucar.nc2.ncml.NcMLWriter;
import ucar.ma2.InvalidRangeException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Collections;
import java.util.Arrays;
import java.io.File;

public class TempRunner
{
    static void testAllInDir(final File dir, final MClosure closure) throws IOException, InvalidRangeException {
        final File[] fa = dir.listFiles();
        if (fa == null || fa.length == 0) {
            return;
        }
        final List<File> list = Arrays.asList(fa);
        Collections.sort(list);
        for (final File f : list) {
            if (f.isDirectory()) {
                testAllInDir(f, closure);
            }
            else {
                closure.run(f.getPath());
            }
        }
    }
    
    public void testWriteNcml() throws IOException, InvalidRangeException {
        final Average fileAvg = new Average();
        final NcMLWriter writer = new NcMLWriter();
        testAllInDir(new File("C:/data/grib/"), new MClosure() {
            public void run(final String filename) throws IOException, InvalidRangeException {
                if (!filename.endsWith("grib1")) {
                    return;
                }
                final NetcdfFile ncfile = NetcdfDataset.openFile(filename, null);
                final File fileout = new File(filename + ".ncml");
                if (fileout.exists()) {
                    fileout.delete();
                }
                writer.writeXMLexplicit(ncfile, new FileOutputStream(fileout), null);
                System.out.println(" wrote ncml file  =" + fileout);
            }
        });
    }
    
    public void testOpenFile() throws IOException, InvalidRangeException {
        final Average fileAvg = new Average();
        testAllInDir(new File("C:/data/grib/"), new MClosure() {
            public void run(final String filename) throws IOException, InvalidRangeException {
                if (!filename.endsWith("ncml")) {
                    return;
                }
                System.out.println(" open ncml file  =" + filename);
                TempRunner.openFile(filename, fileAvg, true);
            }
        });
        System.out.println(" open ncml file  =" + fileAvg);
    }
    
    static void openFile(final String filename, final Average avg, final boolean enhance) throws IOException, InvalidRangeException {
        try {
            final long start = System.nanoTime();
            final NetcdfFile ncfile = enhance ? NetcdfDataset.openDataset(filename) : NetcdfDataset.openFile(filename, null);
            final long end = System.nanoTime();
            final double took = (end - start) / 1000.0 / 1000.0 / 1000.0;
            ncfile.close();
            if (avg != null) {
                avg.add(took);
            }
        }
        catch (Exception e) {
            System.out.println("BAD " + filename);
            e.printStackTrace();
        }
    }
    
    static void testOpenFile(final String dir, final String suffix) throws IOException, InvalidRangeException {
        final Average fileAvg = new Average();
        testAllInDir(new File(dir), new MClosure() {
            public void run(final String filename) throws IOException, InvalidRangeException {
                if (!filename.endsWith(suffix)) {
                    return;
                }
                TempRunner.openFile(filename, fileAvg, false);
            }
        });
        System.out.println("*** open " + suffix + " files  =" + fileAvg);
    }
    
    static void testOpenDataset(final String dir, final String suffix) throws IOException, InvalidRangeException {
        final Average fileAvg = new Average();
        testAllInDir(new File(dir), new MClosure() {
            public void run(final String filename) throws IOException, InvalidRangeException {
                if (!filename.endsWith(suffix)) {
                    return;
                }
                TempRunner.openFile(filename, fileAvg, true);
            }
        });
        System.out.println("*** open " + suffix + " datasets  =" + fileAvg);
    }
    
    public static void main(final String[] args) throws IOException, InvalidRangeException {
        final String dir = args[0];
        final String suffix = args[1];
        testOpenFile(dir, suffix);
        testOpenDataset(dir, suffix);
    }
    
    private static class Average
    {
        private ArrayList values;
        
        private Average() {
            this.values = new ArrayList();
        }
        
        public void add(final double value) {
            this.values.add(new Double(value));
        }
        
        public double mean() {
            final int elements = this.values.size();
            if (elements == 0) {
                throw new IllegalStateException("No values");
            }
            double sum = 0.0;
            for (int i = 0; i < this.values.size(); ++i) {
                final Double valo = this.values.get(i);
                sum += valo;
            }
            return sum / elements;
        }
        
        public double stddev() {
            final double mean = this.mean();
            double stddevtotal = 0.0;
            for (int i = 0; i < this.values.size(); ++i) {
                final Double valo = this.values.get(i);
                final double dev = valo - mean;
                stddevtotal += dev * dev;
            }
            return Math.sqrt(stddevtotal / this.values.size());
        }
        
        @Override
        public String toString() {
            return " avg= " + this.mean() + " stdev= " + this.stddev() + " count= " + this.values.size();
        }
    }
    
    interface MClosure
    {
        void run(final String p0) throws IOException, InvalidRangeException;
    }
}
