// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset;

import java.util.StringTokenizer;
import ucar.nc2.units.TimeUnit;
import ucar.nc2.util.NamedAnything;
import java.util.ArrayList;
import ucar.nc2.units.DateFormatter;
import ucar.nc2.util.NamedObject;
import ucar.nc2.units.DateRange;
import ucar.ma2.Index;
import ucar.ma2.IndexIterator;
import ucar.nc2.Variable;
import ucar.nc2.Dimension;
import java.util.Iterator;
import java.util.List;
import ucar.ma2.Array;
import ucar.ma2.ArrayObject;
import ucar.ma2.ArrayChar;
import ucar.nc2.Attribute;
import java.io.IOException;
import ucar.ma2.DataType;
import java.util.Formatter;
import ucar.nc2.units.DateUnit;
import java.util.Date;

public class CoordinateAxis1DTime extends CoordinateAxis1D
{
    private Date[] timeDates;
    private DateUnit dateUnit;
    
    public static CoordinateAxis1DTime factory(final NetcdfDataset ncd, final VariableDS org, final Formatter errMessages) throws IOException {
        if (org.getDataType() == DataType.CHAR) {
            return new CoordinateAxis1DTime(ncd, org, errMessages, org.getDimension(0).getName());
        }
        return new CoordinateAxis1DTime(ncd, org, errMessages);
    }
    
    private CoordinateAxis1DTime(final NetcdfDataset ncd, final VariableDS org, final Formatter errMessages, final String dims) throws IOException {
        super(ncd, org.getParentGroup(), org.getShortName(), DataType.STRING, dims, org.getUnitsString(), org.getDescription());
        this.ncd = ncd;
        this.orgName = org.orgName;
        final List<Attribute> atts = org.getAttributes();
        for (final Attribute att : atts) {
            this.addAttribute(att);
        }
        int ncoords = (int)org.getSize();
        final int rank = org.getRank();
        final int strlen = org.getShape(rank - 1);
        ncoords /= strlen;
        this.timeDates = new Date[ncoords];
        final ArrayChar data = (ArrayChar)org.read();
        final ArrayChar.StringIterator ii = data.getStringIterator();
        final ArrayObject.D1 sdata = new ArrayObject.D1(String.class, ncoords);
        for (int i = 0; i < ncoords; ++i) {
            final String coordValue = ii.next();
            final Date d = DateUnit.getStandardOrISO(coordValue);
            if (d == null) {
                if (errMessages != null) {
                    errMessages.format("DateUnit cannot parse String= %s\n", coordValue);
                }
                else {
                    System.out.println("DateUnit cannot parse String= " + coordValue + "\n");
                }
                throw new IllegalArgumentException();
            }
            sdata.set(i, coordValue);
            this.timeDates[i] = d;
        }
        this.setCachedData(sdata, true);
    }
    
    private CoordinateAxis1DTime(final NetcdfDataset ncd, final VariableDS org, final Formatter errMessages) throws IOException {
        super(ncd, org);
        final int ncoords = (int)org.getSize();
        this.timeDates = new Date[ncoords];
        DateUnit dateUnit = null;
        final String units = org.getUnitsString();
        if (units != null) {
            try {
                dateUnit = new DateUnit(units);
            }
            catch (Exception ex) {}
        }
        if (dateUnit != null) {
            final Array data = org.read();
            int count = 0;
            IndexIterator ii = data.getIndexIterator();
            for (int i = 0; i < ncoords; ++i) {
                final double val = ii.getDoubleNext();
                if (!Double.isNaN(val)) {
                    final Date d = dateUnit.makeDate(val);
                    this.timeDates[count++] = d;
                }
            }
            if (count != ncoords) {
                final Dimension localDim = new Dimension(this.getShortName(), count, false);
                this.setDimension(0, localDim);
                final Array shortData = Array.factory(data.getElementType(), new int[] { count });
                final Index ima = shortData.getIndex();
                int count2 = 0;
                ii = data.getIndexIterator();
                for (int j = 0; j < ncoords; ++j) {
                    final double val2 = ii.getDoubleNext();
                    if (!Double.isNaN(val2)) {
                        shortData.setDouble(ima.set0(count2), val2);
                        ++count2;
                    }
                }
                this.cache = new Cache();
                this.setCachedData(shortData, true);
                final Date[] keep = this.timeDates;
                System.arraycopy(keep, 0, this.timeDates = new Date[count], 0, this.timeDates.length);
            }
            return;
        }
        if (org.getDataType() == DataType.STRING) {
            final ArrayObject data2 = (ArrayObject)org.read();
            final IndexIterator ii2 = data2.getIndexIterator();
            for (int k = 0; k < ncoords; ++k) {
                final String coordValue = (String)ii2.getObjectNext();
                final Date d2 = DateUnit.getStandardOrISO(coordValue);
                if (d2 == null) {
                    if (errMessages != null) {
                        errMessages.format("DateUnit cannot parse String= %s\n", coordValue);
                    }
                    else {
                        System.out.println("DateUnit cannot parse String= " + coordValue + "\n");
                    }
                    throw new IllegalArgumentException();
                }
                this.timeDates[k] = d2;
            }
            return;
        }
        if (units != null) {
            try {
                dateUnit = new DateUnit(units + " since 0001-01-01 00:00:00");
            }
            catch (Exception e) {
                try {
                    if (errMessages != null) {
                        errMessages.format("Time Coordinate must be udunits or ISO String: hack since 0001-01-01 00:00:00\n", new Object[0]);
                    }
                    else {
                        System.out.println("Time Coordinate must be udunits or ISO String: hack since 0001-01-01 00:00:00\n");
                    }
                    dateUnit = new DateUnit("secs since 0001-01-01 00:00:00");
                }
                catch (Exception ex2) {}
            }
        }
        final Array data = org.read();
        final IndexIterator ii2 = data.getIndexIterator();
        for (int k = 0; k < ncoords; ++k) {
            final double val3 = ii2.getDoubleNext();
            final Date d3 = dateUnit.makeDate(val3);
            this.timeDates[k] = d3;
        }
    }
    
    private CoordinateAxis1DTime(final NetcdfDataset ncd, final CoordinateAxis1DTime org, final Date[] timeDates) {
        super(ncd, org);
        this.timeDates = timeDates;
        this.dateUnit = org.dateUnit;
    }
    
    @Override
    protected Variable copy() {
        return new CoordinateAxis1DTime(this.ncd, this, this.getTimeDates());
    }
    
    public Date[] getTimeDates() {
        return this.timeDates;
    }
    
    public Date getTimeDate(final int idx) {
        return this.timeDates[idx];
    }
    
    public DateRange getDateRange() {
        return new DateRange(this.timeDates[0], this.timeDates[this.timeDates.length - 1]);
    }
    
    @Override
    public List<NamedObject> getNames() {
        final DateFormatter formatter = new DateFormatter();
        final int n = (int)this.getSize();
        final List<NamedObject> names = new ArrayList<NamedObject>(n);
        for (int i = 0; i < n; ++i) {
            names.add(new NamedAnything(formatter.toDateTimeString(this.getTimeDate(i)), "date/time"));
        }
        return names;
    }
    
    public TimeUnit getTimeResolution() throws Exception {
        final String tUnits = this.getUnitsString();
        final StringTokenizer stoker = new StringTokenizer(tUnits);
        final double tResolution = this.getIncrement();
        return new TimeUnit(tResolution, stoker.nextToken());
    }
    
    public int findTimeIndexFromDate(final Date d) {
        int n;
        long m;
        int index;
        for (n = this.timeDates.length, m = d.getTime(), index = 0; index < n && m >= this.timeDates[index].getTime(); ++index) {}
        return Math.max(0, index - 1);
    }
    
    public boolean hasTime(final Date date) {
        for (final Date timeDate : this.timeDates) {
            if (date.equals(timeDate)) {
                return true;
            }
        }
        return false;
    }
}
