// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset;

import org.slf4j.LoggerFactory;
import ucar.nc2.FileWriter;
import java.io.PrintStream;
import ucar.ma2.DataType;
import ucar.ma2.Array;
import ucar.nc2.iosp.IOServiceProvider;
import java.util.Comparator;
import java.util.Collections;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.Structure;
import ucar.nc2.Sequence;
import ucar.nc2.Attribute;
import ucar.nc2.EnumTypedef;
import ucar.nc2.Group;
import ucar.nc2.ncml.NcMLGWriter;
import ucar.nc2.ncml.NcMLWriter;
import java.io.OutputStream;
import ucar.nc2.Dimension;
import ucar.nc2.util.cache.FileCacheable;
import ucar.nc2.constants.AxisType;
import java.util.ArrayList;
import ucar.nc2.stream.CdmRemote;
import ucar.nc2.ncml.NcMLReader;
import ucar.nc2.dods.DODSNetcdfFile;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.HeadMethod;
import thredds.catalog.ServiceType;
import ucar.nc2.thredds.ThreddsDataFactory;
import java.util.Formatter;
import ucar.unidata.util.StringUtil;
import java.util.Iterator;
import java.util.Collection;
import ucar.nc2.Variable;
import ucar.nc2.util.CancelTask;
import java.io.IOException;
import ucar.nc2.ncml.Aggregation;
import java.util.EnumSet;
import java.util.List;
import org.apache.commons.httpclient.HttpClient;
import ucar.nc2.util.cache.FileFactory;
import ucar.nc2.util.cache.FileCache;
import org.slf4j.Logger;
import java.util.Set;
import ucar.nc2.NetcdfFile;

public class NetcdfDataset extends NetcdfFile
{
    private static Set<Enhance> EnhanceAll;
    private static Set<Enhance> EnhanceNone;
    private static Set<Enhance> defaultEnhanceMode;
    private static Set<Enhance> coordSysEnhanceMode;
    private static Logger log;
    protected static boolean useNaNs;
    protected static boolean fillValueIsMissing;
    protected static boolean invalidDataIsMissing;
    protected static boolean missingDataIsMissing;
    private static FileCache fileCache;
    private static FileFactory defaultNetcdfFileFactory;
    private static HttpClient httpClient;
    private NetcdfFile orgFile;
    private List<CoordinateSystem> coordSys;
    private List<CoordinateAxis> coordAxes;
    private List<CoordinateTransform> coordTransforms;
    private String convUsed;
    private EnumSet<Enhance> enhanceMode;
    private Aggregation agg;
    
    public static Set<Enhance> getEnhanceAll() {
        return NetcdfDataset.EnhanceAll;
    }
    
    public static Set<Enhance> getEnhanceNone() {
        return NetcdfDataset.EnhanceNone;
    }
    
    public static Set<Enhance> getEnhanceDefault() {
        return NetcdfDataset.defaultEnhanceMode;
    }
    
    public static Set<Enhance> getDefaultEnhanceMode() {
        return NetcdfDataset.defaultEnhanceMode;
    }
    
    public static Set<Enhance> parseEnhanceMode(final String enhanceMode) {
        if (enhanceMode == null) {
            return null;
        }
        Set<Enhance> mode = null;
        if (enhanceMode.equalsIgnoreCase("true") || enhanceMode.equalsIgnoreCase("All")) {
            mode = getEnhanceAll();
        }
        else if (enhanceMode.equalsIgnoreCase("AllDefer")) {
            mode = EnumSet.of(Enhance.ScaleMissingDefer, Enhance.CoordSystems, Enhance.ConvertEnums);
        }
        else if (enhanceMode.equalsIgnoreCase("ScaleMissing")) {
            mode = EnumSet.of(Enhance.ScaleMissing);
        }
        else if (enhanceMode.equalsIgnoreCase("ScaleMissingDefer")) {
            mode = EnumSet.of(Enhance.ScaleMissingDefer);
        }
        else if (enhanceMode.equalsIgnoreCase("CoordSystems")) {
            mode = EnumSet.of(Enhance.CoordSystems);
        }
        else if (enhanceMode.equalsIgnoreCase("ConvertEnums")) {
            mode = EnumSet.of(Enhance.ConvertEnums);
        }
        return mode;
    }
    
    public static void setUseNaNs(final boolean b) {
        NetcdfDataset.useNaNs = b;
    }
    
    public static boolean getUseNaNs() {
        return NetcdfDataset.useNaNs;
    }
    
    public static void setFillValueIsMissing(final boolean b) {
        NetcdfDataset.fillValueIsMissing = b;
    }
    
    public static boolean getFillValueIsMissing() {
        return NetcdfDataset.fillValueIsMissing;
    }
    
    public static void setInvalidDataIsMissing(final boolean b) {
        NetcdfDataset.invalidDataIsMissing = b;
    }
    
    public static boolean getInvalidDataIsMissing() {
        return NetcdfDataset.invalidDataIsMissing;
    }
    
    public static void setMissingDataIsMissing(final boolean b) {
        NetcdfDataset.missingDataIsMissing = b;
    }
    
    public static boolean getMissingDataIsMissing() {
        return NetcdfDataset.missingDataIsMissing;
    }
    
    public static void initNetcdfFileCache(final int minElementsInMemory, final int maxElementsInMemory, final int period) {
        NetcdfDataset.fileCache = new FileCache("NetcdfFileCache ", minElementsInMemory, maxElementsInMemory, -1, period);
        NetcdfDataset.defaultNetcdfFileFactory = new MyNetcdfFileFactory();
    }
    
    public static void initNetcdfFileCache(final int minElementsInMemory, final int maxElementsInMemory, final int hardLimit, final int period) {
        NetcdfDataset.fileCache = new FileCache("NetcdfFileCache ", minElementsInMemory, maxElementsInMemory, hardLimit, period);
        NetcdfDataset.defaultNetcdfFileFactory = new MyNetcdfFileFactory();
    }
    
    public static void disableNetcdfFileCache() {
        if (null != NetcdfDataset.fileCache) {
            NetcdfDataset.fileCache.disable();
        }
        NetcdfDataset.fileCache = null;
        shutdown();
    }
    
    public static void shutdown() {
        FileCache.shutdown();
    }
    
    public static FileCache getNetcdfFileCache() {
        return NetcdfDataset.fileCache;
    }
    
    public static NetcdfDataset wrap(final NetcdfFile ncfile, final Set<Enhance> mode) throws IOException {
        if (ncfile instanceof NetcdfDataset) {
            final NetcdfDataset ncd = (NetcdfDataset)ncfile;
            if (!ncd.enhanceNeeded(mode)) {
                return (NetcdfDataset)ncfile;
            }
        }
        return new NetcdfDataset(ncfile, mode);
    }
    
    public static NetcdfDataset openDataset(final String location) throws IOException {
        return openDataset(location, true, null);
    }
    
    public static NetcdfDataset openDataset(final String location, final boolean enhance, final CancelTask cancelTask) throws IOException {
        return openDataset(location, enhance, -1, cancelTask, null);
    }
    
    public static NetcdfDataset openDataset(final String location, final boolean enhance, final int buffer_size, final CancelTask cancelTask, final Object spiObject) throws IOException {
        return openDataset(location, enhance ? NetcdfDataset.defaultEnhanceMode : null, buffer_size, cancelTask, spiObject);
    }
    
    public static NetcdfDataset openDataset(final String location, final Set<Enhance> enhanceMode, final int buffer_size, final CancelTask cancelTask, final Object spiObject) throws IOException {
        final NetcdfFile ncfile = openOrAcquireFile(null, null, null, location, buffer_size, cancelTask, spiObject);
        NetcdfDataset ds;
        if (ncfile instanceof NetcdfDataset) {
            ds = (NetcdfDataset)ncfile;
            enhance(ds, enhanceMode, cancelTask);
        }
        else {
            ds = new NetcdfDataset(ncfile, enhanceMode);
        }
        return ds;
    }
    
    private static CoordSysBuilderIF enhance(final NetcdfDataset ds, final Set<Enhance> mode, final CancelTask cancelTask) throws IOException {
        if (mode == null) {
            return null;
        }
        CoordSysBuilderIF builder = null;
        if (mode.contains(Enhance.CoordSystems) && !ds.enhanceMode.contains(Enhance.CoordSystems)) {
            builder = CoordSysBuilder.factory(ds, cancelTask);
            builder.augmentDataset(ds, cancelTask);
            ds.convUsed = builder.getConventionUsed();
        }
        if ((mode.contains(Enhance.ConvertEnums) && !ds.enhanceMode.contains(Enhance.ConvertEnums)) || (mode.contains(Enhance.ScaleMissing) && !ds.enhanceMode.contains(Enhance.ScaleMissing)) || (mode.contains(Enhance.ScaleMissingDefer) && !ds.enhanceMode.contains(Enhance.ScaleMissingDefer))) {
            for (final Variable v : ds.getVariables()) {
                final VariableEnhanced ve = (VariableEnhanced)v;
                ve.enhance(mode);
                if (cancelTask != null && cancelTask.isCancel()) {
                    return null;
                }
            }
        }
        if (builder != null) {
            builder.buildCoordinateSystems(ds);
        }
        ds.finish();
        ds.enhanceMode.addAll((Collection<?>)mode);
        return builder;
    }
    
    public static NetcdfDataset acquireDataset(final String location, final CancelTask cancelTask) throws IOException {
        return acquireDataset(null, location, NetcdfDataset.defaultEnhanceMode, -1, cancelTask, null);
    }
    
    public static NetcdfDataset acquireDataset(final String location, final boolean enhance, final CancelTask cancelTask) throws IOException {
        return acquireDataset(null, location, enhance ? NetcdfDataset.defaultEnhanceMode : null, -1, cancelTask, null);
    }
    
    public static NetcdfDataset acquireDataset(FileFactory fac, final String location, final Set<Enhance> enhanceMode, final int buffer_size, final CancelTask cancelTask, final Object iospMessage) throws IOException {
        if (NetcdfDataset.fileCache == null) {
            if (fac == null) {
                return openDataset(location, enhanceMode, buffer_size, cancelTask, iospMessage);
            }
            return (NetcdfDataset)fac.open(location, buffer_size, cancelTask, iospMessage);
        }
        else {
            if (fac != null) {
                return (NetcdfDataset)openOrAcquireFile(NetcdfDataset.fileCache, fac, null, location, buffer_size, cancelTask, iospMessage);
            }
            fac = new MyNetcdfDatasetFactory(location, enhanceMode);
            return (NetcdfDataset)openOrAcquireFile(NetcdfDataset.fileCache, fac, fac.hashCode(), location, buffer_size, cancelTask, iospMessage);
        }
    }
    
    public static NetcdfFile openFile(final String location, final CancelTask cancelTask) throws IOException {
        return openOrAcquireFile(null, null, null, location, -1, cancelTask, null);
    }
    
    public static NetcdfFile openFile(final String location, final int buffer_size, final CancelTask cancelTask, final Object spiObject) throws IOException {
        return openOrAcquireFile(null, null, null, location, buffer_size, cancelTask, spiObject);
    }
    
    public static NetcdfFile acquireFile(final String location, final CancelTask cancelTask) throws IOException {
        return acquireFile(null, null, location, -1, cancelTask, null);
    }
    
    public static NetcdfFile acquireFile(final FileFactory factory, final Object hashKey, final String location, final int buffer_size, final CancelTask cancelTask, final Object spiObject) throws IOException {
        if (NetcdfDataset.fileCache == null && factory != null) {
            return (NetcdfFile)factory.open(location, buffer_size, cancelTask, spiObject);
        }
        return openOrAcquireFile(NetcdfDataset.fileCache, factory, hashKey, location, buffer_size, cancelTask, spiObject);
    }
    
    private static NetcdfFile openOrAcquireFile(final FileCache cache, FileFactory factory, final Object hashKey, String location, final int buffer_size, final CancelTask cancelTask, final Object spiObject) throws IOException {
        if (location == null) {
            throw new IOException("NetcdfDataset.openFile: location is null");
        }
        location = location.trim();
        location = StringUtil.replace(location, '\\', "/");
        if (location.startsWith("dods:")) {
            return acquireDODS(cache, factory, hashKey, location, buffer_size, cancelTask, spiObject);
        }
        if (location.startsWith("cdmremote:")) {
            return acquireRemote(cache, factory, hashKey, location, buffer_size, cancelTask, spiObject);
        }
        if (location.startsWith("thredds:")) {
            final Formatter log = new Formatter();
            final ThreddsDataFactory tdf = new ThreddsDataFactory();
            final NetcdfFile ncfile = tdf.openDataset(location, false, cancelTask, log);
            if (ncfile == null) {
                throw new IOException(log.toString());
            }
            return ncfile;
        }
        else {
            if (location.endsWith(".xml") || location.endsWith(".ncml")) {
                if (!location.startsWith("http:") && !location.startsWith("file:")) {
                    location = "file:" + location;
                }
                return acquireNcml(cache, factory, hashKey, location, buffer_size, cancelTask, spiObject);
            }
            if (location.startsWith("http:")) {
                final ServiceType stype = disambiguateHttp(location);
                if (stype == ServiceType.OPENDAP) {
                    return acquireDODS(cache, factory, hashKey, location, buffer_size, cancelTask, spiObject);
                }
                if (stype == ServiceType.CdmRemote) {
                    return acquireRemote(cache, factory, hashKey, location, buffer_size, cancelTask, spiObject);
                }
            }
            if (cache != null) {
                if (factory == null) {
                    factory = NetcdfDataset.defaultNetcdfFileFactory;
                }
                return (NetcdfFile)cache.acquire(factory, hashKey, location, buffer_size, cancelTask, spiObject);
            }
            return NetcdfFile.open(location, buffer_size, cancelTask, spiObject);
        }
    }
    
    private static ServiceType disambiguateHttp(final String location) throws IOException {
        initHttpClient();
        final ServiceType result = checkIfDods(location);
        if (result != null) {
            return result;
        }
        HttpMethod method = null;
        try {
            method = (HttpMethod)new HeadMethod(location);
            method.setFollowRedirects(true);
            final int statusCode = NetcdfDataset.httpClient.executeMethod(method);
            if (statusCode < 300) {
                final Header h = method.getResponseHeader("Content-Description");
                if (h != null && h.getValue() != null) {
                    final String v = h.getValue();
                    if (v.equalsIgnoreCase("ncstream")) {
                        return ServiceType.CdmRemote;
                    }
                }
                return null;
            }
            if (statusCode == 401) {
                throw new IOException("Unauthorized to open dataset " + location);
            }
            throw new IOException(location + " is not a valid URL, return status=" + statusCode);
        }
        finally {
            if (method != null) {
                method.releaseConnection();
            }
        }
    }
    
    private static ServiceType checkIfDods(final String location) throws IOException {
        HttpMethod method = null;
        try {
            method = (HttpMethod)new HeadMethod(location + ".dds");
            method.setFollowRedirects(true);
            final int status = NetcdfDataset.httpClient.executeMethod(method);
            if (status == 200) {
                final Header h = method.getResponseHeader("Content-Description");
                if (h != null && h.getValue() != null) {
                    final String v = h.getValue();
                    if (v.equalsIgnoreCase("dods-dds") || v.equalsIgnoreCase("dods_dds")) {
                        return ServiceType.OPENDAP;
                    }
                    throw new IOException("OPeNDAP Server Error= " + method.getResponseBodyAsString());
                }
            }
            if (status == 401) {
                throw new IOException("Unauthorized to open dataset " + location);
            }
            return null;
        }
        finally {
            if (method != null) {
                method.releaseConnection();
            }
        }
    }
    
    public static void setHttpClient(final HttpClient client) {
        NetcdfDataset.httpClient = client;
    }
    
    private static synchronized void initHttpClient() {
        if (NetcdfDataset.httpClient != null) {
            return;
        }
        final MultiThreadedHttpConnectionManager connectionManager = new MultiThreadedHttpConnectionManager();
        NetcdfDataset.httpClient = new HttpClient((HttpConnectionManager)connectionManager);
    }
    
    private static NetcdfFile acquireDODS(final FileCache cache, FileFactory factory, final Object hashKey, final String location, final int buffer_size, final CancelTask cancelTask, final Object spiObject) throws IOException {
        if (cache == null) {
            return new DODSNetcdfFile(location, cancelTask);
        }
        if (factory == null) {
            factory = new DodsFactory();
        }
        return (NetcdfFile)cache.acquire(factory, hashKey, location, buffer_size, cancelTask, spiObject);
    }
    
    private static NetcdfFile acquireNcml(final FileCache cache, FileFactory factory, final Object hashKey, final String location, final int buffer_size, final CancelTask cancelTask, final Object spiObject) throws IOException {
        if (cache == null) {
            return NcMLReader.readNcML(location, cancelTask);
        }
        if (factory == null) {
            factory = new NcMLFactory();
        }
        return (NetcdfFile)cache.acquire(factory, hashKey, location, buffer_size, cancelTask, spiObject);
    }
    
    private static NetcdfFile acquireRemote(final FileCache cache, FileFactory factory, final Object hashKey, final String location, final int buffer_size, final CancelTask cancelTask, final Object spiObject) throws IOException {
        if (cache == null) {
            return new CdmRemote(location);
        }
        if (factory == null) {
            factory = new RemoteFactory();
        }
        return (NetcdfFile)cache.acquire(factory, hashKey, location, buffer_size, cancelTask, spiObject);
    }
    
    public Aggregation getAggregation() {
        return this.agg;
    }
    
    public void setAggregation(final Aggregation agg) {
        this.agg = agg;
    }
    
    public List<CoordinateSystem> getCoordinateSystems() {
        return this.coordSys;
    }
    
    public String getConventionUsed() {
        return this.convUsed;
    }
    
    public EnumSet<Enhance> getEnhanceMode() {
        return this.enhanceMode;
    }
    
    public List<CoordinateTransform> getCoordinateTransforms() {
        return this.coordTransforms;
    }
    
    public List<CoordinateAxis> getCoordinateAxes() {
        return this.coordAxes;
    }
    
    public void clearCoordinateSystems() {
        this.coordSys = new ArrayList<CoordinateSystem>();
        this.coordAxes = new ArrayList<CoordinateAxis>();
        this.coordTransforms = new ArrayList<CoordinateTransform>();
        for (final Variable v : this.getVariables()) {
            final VariableEnhanced ve = (VariableEnhanced)v;
            ve.clearCoordinateSystems();
        }
        this.enhanceMode.remove(Enhance.CoordSystems);
    }
    
    public CoordinateAxis findCoordinateAxis(final AxisType type) {
        if (type == null) {
            return null;
        }
        for (final CoordinateAxis v : this.coordAxes) {
            if (type == v.getAxisType()) {
                return v;
            }
        }
        return null;
    }
    
    public CoordinateAxis findCoordinateAxis(final String fullName) {
        if (fullName == null) {
            return null;
        }
        for (final CoordinateAxis v : this.coordAxes) {
            final String n = v.getName();
            if (fullName.equals(n)) {
                return v;
            }
        }
        return null;
    }
    
    public CoordinateSystem findCoordinateSystem(final String name) {
        if (name == null) {
            return null;
        }
        for (final CoordinateSystem v : this.coordSys) {
            if (name.equals(v.getName())) {
                return v;
            }
        }
        return null;
    }
    
    public CoordinateTransform findCoordinateTransform(final String name) {
        if (name == null) {
            return null;
        }
        for (final CoordinateTransform v : this.coordTransforms) {
            if (name.equals(v.getName())) {
                return v;
            }
        }
        return null;
    }
    
    @Override
    public synchronized void close() throws IOException {
        if (this.agg != null) {
            this.agg.persistWrite();
        }
        if (this.cache != null) {
            this.unlocked = true;
            this.cache.release(this);
        }
        else {
            if (this.agg != null) {
                this.agg.close();
            }
            this.agg = null;
            if (this.orgFile != null) {
                this.orgFile.close();
            }
            this.orgFile = null;
        }
    }
    
    @Override
    public boolean sync() throws IOException {
        this.unlocked = false;
        if (this.agg != null) {
            return this.agg.sync();
        }
        if (this.orgFile != null && this.orgFile.sync()) {
            this.location = this.orgFile.getLocation();
            this.id = this.orgFile.getId();
            this.title = this.orgFile.getTitle();
            this.empty();
            this.convertGroup(this.getRootGroup(), this.orgFile.getRootGroup());
            this.finish();
            final EnumSet<Enhance> saveMode = this.enhanceMode;
            this.enhanceMode = EnumSet.noneOf(Enhance.class);
            enhance(this, saveMode, null);
            return true;
        }
        return false;
    }
    
    @Override
    public void empty() {
        super.empty();
        this.coordSys = new ArrayList<CoordinateSystem>();
        this.coordAxes = new ArrayList<CoordinateAxis>();
        this.coordTransforms = new ArrayList<CoordinateTransform>();
        this.convUsed = null;
    }
    
    @Override
    public boolean syncExtend() throws IOException {
        this.unlocked = false;
        if (this.agg != null) {
            return this.agg.syncExtend();
        }
        if (this.orgFile != null) {
            final boolean wasExtended = this.orgFile.syncExtend();
            if (wasExtended) {
                final Dimension ndim = this.orgFile.getUnlimitedDimension();
                final int newLength = ndim.getLength();
                final Dimension udim = this.getUnlimitedDimension();
                udim.setLength(newLength);
                for (final Variable v : this.getVariables()) {
                    if (v.isUnlimited()) {
                        v.setDimensions(v.getDimensions());
                    }
                }
                return true;
            }
        }
        return false;
    }
    
    @Override
    public void writeNcML(final OutputStream os, final String uri) throws IOException {
        new NcMLWriter().writeXML(this, os, uri);
    }
    
    public void writeNcMLG(final OutputStream os, final boolean showCoords, final String uri) throws IOException {
        new NcMLGWriter().writeXML(this, os, showCoords, uri);
    }
    
    public NetcdfDataset(final NetcdfFile ncfile) throws IOException {
        this(ncfile, NetcdfDataset.defaultEnhanceMode);
    }
    
    public NetcdfDataset(final NetcdfFile ncfile, final boolean enhance) throws IOException {
        this(ncfile, enhance ? NetcdfDataset.defaultEnhanceMode : null);
    }
    
    public NetcdfDataset(final NetcdfFile ncfile, final Set<Enhance> mode) throws IOException {
        super(ncfile);
        this.orgFile = null;
        this.coordSys = new ArrayList<CoordinateSystem>();
        this.coordAxes = new ArrayList<CoordinateAxis>();
        this.coordTransforms = new ArrayList<CoordinateTransform>();
        this.enhanceMode = EnumSet.noneOf(Enhance.class);
        this.agg = null;
        this.orgFile = ncfile;
        this.spi = null;
        this.convertGroup(this.getRootGroup(), ncfile.getRootGroup());
        this.finish();
        enhance(this, mode, null);
    }
    
    private void convertGroup(final Group g, final Group from) {
        for (final EnumTypedef et : from.getEnumTypedefs()) {
            g.addEnumeration(et);
        }
        for (final Dimension d : from.getDimensions()) {
            g.addDimension(new Dimension(d.getName(), d));
        }
        for (final Attribute a : from.getAttributes()) {
            g.addAttribute(a);
        }
        for (final Variable v : from.getVariables()) {
            g.addVariable(this.convertVariable(g, v));
        }
        for (final Group nested : from.getGroups()) {
            final Group nnested = new Group(this, g, nested.getShortName());
            g.addGroup(nnested);
            this.convertGroup(nnested, nested);
        }
    }
    
    private Variable convertVariable(final Group g, final Variable v) {
        Variable newVar;
        if (v instanceof Sequence) {
            newVar = new SequenceDS(g, (Sequence)v);
        }
        else if (v instanceof Structure) {
            newVar = new StructureDS(g, (Structure)v);
        }
        else {
            newVar = new VariableDS(g, v, false);
        }
        return newVar;
    }
    
    @Override
    protected Boolean makeRecordStructure() {
        if (this.orgFile == null) {
            return false;
        }
        final Boolean hasRecord = (Boolean)this.orgFile.sendIospMessage("AddRecordStructure");
        if (hasRecord == null || !hasRecord) {
            return false;
        }
        final Variable orgV = this.orgFile.getRootGroup().findVariable("record");
        if (orgV == null || !(orgV instanceof Structure)) {
            return false;
        }
        final Structure orgStructure = (Structure)orgV;
        final Dimension udim = this.getUnlimitedDimension();
        if (udim == null) {
            return false;
        }
        final Group root = this.getRootGroup();
        final StructureDS newStructure = new StructureDS(this, root, null, "record", udim.getName(), null, null);
        newStructure.setOriginalVariable(orgStructure);
        for (final Variable v : this.getVariables()) {
            if (!v.isUnlimited()) {
                continue;
            }
            VariableDS memberV;
            try {
                memberV = (VariableDS)v.slice(0, 0);
            }
            catch (InvalidRangeException e) {
                NetcdfDataset.log.error("Cant slice variable " + v);
                return false;
            }
            memberV.setParentStructure(newStructure);
            newStructure.addMemberVariable(memberV);
        }
        root.addVariable(newStructure);
        this.finish();
        return true;
    }
    
    public void sort() {
        Collections.sort(this.variables, new VariableComparator());
        Collections.sort(this.coordAxes, new VariableComparator());
    }
    
    public NetcdfDataset() {
        this.orgFile = null;
        this.coordSys = new ArrayList<CoordinateSystem>();
        this.coordAxes = new ArrayList<CoordinateAxis>();
        this.coordTransforms = new ArrayList<CoordinateTransform>();
        this.enhanceMode = EnumSet.noneOf(Enhance.class);
        this.agg = null;
    }
    
    public NetcdfFile getReferencedFile() {
        return this.orgFile;
    }
    
    @Override
    public IOServiceProvider getIosp() {
        return (this.orgFile == null) ? null : this.orgFile.getIosp();
    }
    
    public void setReferencedFile(final NetcdfFile ncfile) {
        this.orgFile = ncfile;
    }
    
    @Override
    protected String toStringDebug(final Object o) {
        return "";
    }
    
    public void addCoordinateSystem(final CoordinateSystem cs) {
        this.coordSys.add(cs);
    }
    
    public void addCoordinateTransform(final CoordinateTransform ct) {
        if (!this.coordTransforms.contains(ct)) {
            this.coordTransforms.add(ct);
        }
    }
    
    public CoordinateAxis addCoordinateAxis(final VariableDS v) {
        if (v == null) {
            return null;
        }
        final CoordinateAxis oldVar = this.findCoordinateAxis(v.getName());
        if (oldVar != null) {
            this.coordAxes.remove(oldVar);
        }
        final CoordinateAxis ca = (CoordinateAxis)((v instanceof CoordinateAxis) ? v : CoordinateAxis.factory(this, v));
        this.coordAxes.add(ca);
        if (v.isMemberOfStructure()) {
            final Structure parentOrg = v.getParentStructure();
            final Structure parent = (Structure)this.findVariable(parentOrg.getNameEscaped());
            parent.replaceMemberVariable(ca);
        }
        else {
            this.removeVariable(v.getParentGroup(), v.getShortName());
            this.addVariable(ca.getParentGroup(), ca);
        }
        return ca;
    }
    
    @Override
    public Variable addVariable(final Group g, final Variable v) {
        if (!(v instanceof VariableDS) && !(v instanceof StructureDS)) {
            throw new IllegalArgumentException("NetcdfDataset variables must be VariableEnhanced objects");
        }
        return super.addVariable(g, v);
    }
    
    public CoordSysBuilderIF enhance() throws IOException {
        return enhance(this, NetcdfDataset.defaultEnhanceMode, null);
    }
    
    public void enhance(final Set<Enhance> mode) throws IOException {
        enhance(this, mode, null);
    }
    
    public boolean enhanceNeeded(final Set<Enhance> want) throws IOException {
        if (want == null) {
            return false;
        }
        for (final Enhance mode : want) {
            if (!this.enhanceMode.contains(mode)) {
                return true;
            }
        }
        return false;
    }
    
    public void setValues(final Variable v, final int npts, final double start, final double incr) {
        if (npts != v.getSize()) {
            throw new IllegalArgumentException("bad npts = " + npts + " should be " + v.getSize());
        }
        Array data = Array.makeArray(v.getDataType(), npts, start, incr);
        if (v.getRank() != 1) {
            data = data.reshape(v.getShape());
        }
        v.setCachedData(data, true);
    }
    
    public void setValues(final Variable v, final List<String> values) throws IllegalArgumentException {
        Array data = Array.makeArray(v.getDataType(), values);
        if (data.getSize() != v.getSize()) {
            throw new IllegalArgumentException("Incorrect number of values specified for the Variable " + v.getName() + " needed= " + v.getSize() + " given=" + data.getSize());
        }
        if (v.getRank() != 1) {
            data = data.reshape(v.getShape());
        }
        v.setCachedData(data, true);
    }
    
    @Deprecated
    public static Array makeArray(final DataType dtype, final List<String> stringValues) throws NumberFormatException {
        return Array.makeArray(dtype, stringValues);
    }
    
    @Override
    public void getDetailInfo(final Formatter f) {
        f.format("NetcdfDataset location= %s%n", this.getLocation());
        f.format("  title= %s%n", this.getTitle());
        f.format("  id= %s%n", this.getId());
        f.format("  fileType= %s%n", this.getFileTypeId());
        f.format("  fileDesc= %s%n", this.getFileTypeDescription());
        f.format("  class= %s%n", this.getClass().getName());
        if (this.agg == null) {
            f.format("  has no Aggregation element%n", new Object[0]);
        }
        else {
            f.format("%nAggregation:%n", new Object[0]);
            this.agg.getDetailInfo(f);
        }
        if (this.orgFile == null) {
            f.format("  has no referenced NetcdfFile%n", new Object[0]);
            this.showCached(f);
            this.showProxies(f);
        }
        else {
            f.format("%nReferenced File:%n", new Object[0]);
            f.format(this.orgFile.getDetailInfo(), new Object[0]);
        }
    }
    
    void dumpClasses(final Group g, final PrintStream out) {
        out.println("Dimensions:");
        for (final Dimension ds : g.getDimensions()) {
            out.println("  " + ds.getName() + " " + ds.getClass().getName());
        }
        out.println("Atributes:");
        for (final Attribute a : g.getAttributes()) {
            out.println("  " + a.getName() + " " + a.getClass().getName());
        }
        out.println("Variables:");
        this.dumpVariables(g.getVariables(), out);
        out.println("Groups:");
        for (final Group nested : g.getGroups()) {
            out.println("  " + nested.getName() + " " + nested.getClass().getName());
            this.dumpClasses(nested, out);
        }
    }
    
    private void dumpVariables(final List<Variable> vars, final PrintStream out) {
        for (final Variable v : vars) {
            out.print("  " + v.getName() + " " + v.getClass().getName());
            if (v instanceof CoordinateAxis) {
                out.println("  " + ((CoordinateAxis)v).getAxisType());
            }
            else {
                out.println();
            }
            if (v instanceof Structure) {
                this.dumpVariables(((Structure)v).getVariables(), out);
            }
        }
    }
    
    public static void debugDump(final PrintStream out, final NetcdfDataset ncd) {
        final String referencedLocation = (ncd.orgFile == null) ? "(null)" : ncd.orgFile.getLocation();
        out.println("\nNetcdfDataset dump = " + ncd.getLocation() + " uri= " + referencedLocation + "\n");
        ncd.dumpClasses(ncd.getRootGroup(), out);
    }
    
    @Override
    public String getFileTypeId() {
        if (this.orgFile != null) {
            return this.orgFile.getFileTypeId();
        }
        if (this.agg != null) {
            return this.agg.getFileTypeId();
        }
        return "N/A";
    }
    
    @Override
    public String getFileTypeDescription() {
        if (this.orgFile != null) {
            return this.orgFile.getFileTypeDescription();
        }
        if (this.agg != null) {
            return this.agg.getFileTypeDescription();
        }
        return "N/A";
    }
    
    public void check(final Formatter f) {
        for (final Variable v : this.getVariables()) {
            final VariableDS vds = (VariableDS)v;
            if (vds.getOriginalDataType() != vds.getDataType()) {
                f.format("Variable %s has type %s, org = %s%n", vds.getName(), vds.getOriginalDataType(), vds.getDataType());
            }
            if (vds.getOriginalVariable() != null) {
                final Variable orgVar = vds.getOriginalVariable();
                if (orgVar.getRank() == vds.getRank()) {
                    continue;
                }
                f.format("Variable %s has rank %d, org = %d%n", vds.getName(), vds.getRank(), orgVar.getRank());
            }
        }
    }
    
    public static void main(final String[] arg) throws IOException {
        final String usage = "usage: ucar.nc2.dataset.NetcdfDataset -in <fileIn> -out <fileOut> [-isLargeFile]";
        if (arg.length < 4) {
            System.out.println(usage);
            System.exit(0);
        }
        boolean isLargeFile = false;
        String datasetIn = null;
        String datasetOut = null;
        for (int i = 0; i < arg.length; ++i) {
            final String s = arg[i];
            if (s.equalsIgnoreCase("-in")) {
                datasetIn = arg[i + 1];
            }
            if (s.equalsIgnoreCase("-out")) {
                datasetOut = arg[i + 1];
            }
            if (s.equalsIgnoreCase("-isLargeFile")) {
                isLargeFile = true;
            }
        }
        if (datasetIn == null || datasetOut == null) {
            System.out.println(usage);
            System.exit(0);
        }
        final NetcdfFile ncfileIn = openFile(datasetIn, null);
        System.out.println("Read from " + datasetIn + " write to " + datasetOut);
        final NetcdfFile ncfileOut = FileWriter.writeToFile(ncfileIn, datasetOut, false, -1, isLargeFile);
        ncfileIn.close();
        ncfileOut.close();
        System.out.println("NetcdfFile written = " + ncfileOut);
    }
    
    static {
        NetcdfDataset.EnhanceAll = Collections.unmodifiableSet((Set<? extends Enhance>)EnumSet.of(Enhance.ScaleMissing, Enhance.CoordSystems, Enhance.ConvertEnums));
        NetcdfDataset.EnhanceNone = Collections.unmodifiableSet((Set<? extends Enhance>)EnumSet.noneOf(Enhance.class));
        NetcdfDataset.defaultEnhanceMode = NetcdfDataset.EnhanceAll;
        NetcdfDataset.coordSysEnhanceMode = null;
        NetcdfDataset.log = LoggerFactory.getLogger(NetcdfDataset.class);
        NetcdfDataset.useNaNs = true;
        NetcdfDataset.fillValueIsMissing = true;
        NetcdfDataset.invalidDataIsMissing = true;
        NetcdfDataset.missingDataIsMissing = true;
        NetcdfDataset.fileCache = null;
        NetcdfDataset.defaultNetcdfFileFactory = null;
        NetcdfDataset.httpClient = null;
    }
    
    public enum Enhance
    {
        ScaleMissing, 
        ScaleMissingDefer, 
        CoordSystems, 
        ConvertEnums;
    }
    
    private static class MyNetcdfFileFactory implements FileFactory
    {
        public NetcdfFile open(final String location, final int buffer_size, final CancelTask cancelTask, final Object iospMessage) throws IOException {
            return NetcdfDataset.openFile(location, buffer_size, cancelTask, iospMessage);
        }
    }
    
    private static class MyNetcdfDatasetFactory implements FileFactory
    {
        String location;
        EnumSet<Enhance> enhanceMode;
        
        MyNetcdfDatasetFactory(final String location, final Set<Enhance> enhanceMode) {
            this.location = location;
            this.enhanceMode = ((enhanceMode == null) ? EnumSet.noneOf(Enhance.class) : EnumSet.copyOf(enhanceMode));
        }
        
        public NetcdfFile open(final String location, final int buffer_size, final CancelTask cancelTask, final Object iospMessage) throws IOException {
            return NetcdfDataset.openDataset(location, this.enhanceMode, buffer_size, cancelTask, iospMessage);
        }
        
        @Override
        public int hashCode() {
            int result = this.location.hashCode();
            result += 37 * result + this.enhanceMode.hashCode();
            return result;
        }
    }
    
    private static class DodsFactory implements FileFactory
    {
        public NetcdfFile open(final String location, final int buffer_size, final CancelTask cancelTask, final Object spiObject) throws IOException {
            return new DODSNetcdfFile(location, cancelTask);
        }
    }
    
    private static class NcMLFactory implements FileFactory
    {
        public NetcdfFile open(final String location, final int buffer_size, final CancelTask cancelTask, final Object spiObject) throws IOException {
            return NcMLReader.readNcML(location, cancelTask);
        }
    }
    
    private static class RemoteFactory implements FileFactory
    {
        public NetcdfFile open(final String location, final int buffer_size, final CancelTask cancelTask, final Object spiObject) throws IOException {
            return new CdmRemote(location);
        }
    }
    
    private class VariableComparator implements Comparator
    {
        public int compare(final Object o1, final Object o2) {
            final VariableEnhanced v1 = (VariableEnhanced)o1;
            final VariableEnhanced v2 = (VariableEnhanced)o2;
            final List list1 = v1.getCoordinateSystems();
            final String cs1 = (list1.size() > 0) ? list1.get(0).getName() : "";
            final List list2 = v2.getCoordinateSystems();
            final String cs2 = (list2.size() > 0) ? list2.get(0).getName() : "";
            if (cs2.equals(cs1)) {
                return v1.getName().compareToIgnoreCase(v2.getName());
            }
            return cs1.compareToIgnoreCase(cs2);
        }
        
        @Override
        public boolean equals(final Object obj) {
            return this == obj;
        }
    }
}
