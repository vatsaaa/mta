// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset;

import ucar.ma2.StructureDataIterator;
import org.slf4j.LoggerFactory;
import java.util.Set;
import ucar.ma2.StructureDataW;
import ucar.ma2.StructureData;
import ucar.ma2.ArraySequence;
import ucar.ma2.ArrayObject;
import ucar.ma2.ArrayStructureMA;
import ucar.ma2.ArrayStructure;
import ucar.ma2.StructureMembers;
import ucar.ma2.InvalidRangeException;
import java.io.IOException;
import ucar.ma2.Section;
import ucar.ma2.Array;
import ucar.nc2.util.CancelTask;
import ucar.ma2.DataType;
import ucar.nc2.Sequence;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import ucar.nc2.Attribute;
import ucar.nc2.Variable;
import ucar.nc2.Group;
import ucar.nc2.NetcdfFile;
import org.slf4j.Logger;
import ucar.nc2.Structure;

public class StructureDS extends Structure implements VariableEnhanced
{
    private static Logger log;
    private EnhancementsImpl proxy;
    protected Structure orgVar;
    private String orgName;
    
    protected StructureDS(final NetcdfFile ncfile, final Group group, final String shortName) {
        super(ncfile, group, null, shortName);
    }
    
    public StructureDS(final NetcdfDataset ds, final Group group, final Structure parentStructure, final String shortName, final String dims, final String units, final String desc) {
        super(ds, group, parentStructure, shortName);
        this.setDimensions(dims);
        this.proxy = new EnhancementsImpl(this, units, desc);
        if (units != null) {
            this.addAttribute(new Attribute("units", units));
        }
        if (desc != null) {
            this.addAttribute(new Attribute("long_name", desc));
        }
    }
    
    public StructureDS(final Group g, final Structure orgVar) {
        super(orgVar);
        this.group = g;
        this.orgVar = orgVar;
        this.proxy = new EnhancementsImpl(this);
        this.ncfile = null;
        this.spiObject = null;
        this.createNewCache();
        if (orgVar instanceof StructureDS) {
            return;
        }
        final List<Variable> newList = new ArrayList<Variable>(this.members.size());
        for (final Variable v : this.members) {
            final Variable newVar = this.convertVariable(g, v);
            newVar.setParentStructure(this);
            newList.add(newVar);
        }
        this.setMemberVariables(newList);
    }
    
    private Variable convertVariable(final Group g, final Variable v) {
        Variable newVar;
        if (v instanceof Sequence) {
            newVar = new SequenceDS(g, (Sequence)v);
        }
        else if (v instanceof Structure) {
            newVar = new StructureDS(g, (Structure)v);
        }
        else {
            newVar = new VariableDS(g, v, false);
        }
        return newVar;
    }
    
    public StructureDS(final NetcdfDataset ds, final Group group, final Structure parent, final String shortName, final Structure orgVar) {
        super(ds, group, parent, shortName);
        if (orgVar instanceof Structure) {
            throw new IllegalArgumentException("VariableDS must not wrap a Structure; name=" + orgVar.getName());
        }
        this.spiObject = null;
        this.createNewCache();
        this.orgVar = orgVar;
    }
    
    @Override
    protected Variable copy() {
        return new StructureDS(this.group, this);
    }
    
    @Override
    public Structure select(final List<String> memberNames) {
        final StructureDS result = new StructureDS(this.group, this.orgVar);
        final List<Variable> members = new ArrayList<Variable>();
        for (final String name : memberNames) {
            final Variable m = this.findVariable(name);
            if (null != m) {
                members.add(m);
            }
        }
        result.setMemberVariables(members);
        result.isSubset = true;
        return result;
    }
    
    public Variable getOriginalVariable() {
        return this.orgVar;
    }
    
    public void setOriginalVariable(final Variable orgVar) {
        if (!(orgVar instanceof Structure)) {
            throw new IllegalArgumentException("STructureDS must wrap a Structure; name=" + orgVar.getName());
        }
        this.orgVar = (Structure)orgVar;
    }
    
    public DataType getOriginalDataType() {
        return DataType.STRUCTURE;
    }
    
    public String getOriginalName() {
        return this.orgName;
    }
    
    @Override
    public void setName(final String newName) {
        this.orgName = this.shortName;
        super.setName(newName);
    }
    
    @Override
    public Array reallyRead(final Variable client, final CancelTask cancelTask) throws IOException {
        Array result;
        if (this.hasCachedData()) {
            result = super.reallyRead(client, cancelTask);
        }
        else {
            if (this.orgVar == null) {
                throw new IllegalStateException("StructureDS has no way to get data");
            }
            result = this.orgVar.read();
        }
        return this.convert(result, null);
    }
    
    @Override
    public Array reallyRead(final Variable client, final Section section, final CancelTask cancelTask) throws IOException, InvalidRangeException {
        if (section.computeSize() == this.getSize()) {
            return this._read();
        }
        Array result;
        if (this.hasCachedData()) {
            result = super.reallyRead(client, section, cancelTask);
        }
        else {
            if (this.orgVar == null) {
                throw new IllegalStateException("StructureDS has no way to get data");
            }
            result = this.orgVar.read(section);
        }
        return this.convert(result, section);
    }
    
    private boolean convertNeeded(final StructureMembers smData) {
        for (final Variable v : this.getVariables()) {
            if (v instanceof VariableDS) {
                final VariableDS vds = (VariableDS)v;
                if (vds.needConvert()) {
                    return true;
                }
            }
            else if (v instanceof StructureDS) {
                final StructureDS nested = (StructureDS)v;
                if (nested.convertNeeded(null)) {
                    return true;
                }
            }
            if (smData != null && !this.varHasData(v, smData)) {
                return true;
            }
        }
        return false;
    }
    
    protected ArrayStructure convert(final Array data, final Section section) throws IOException {
        final ArrayStructure orgAS = (ArrayStructure)data;
        if (!this.convertNeeded(orgAS.getStructureMembers())) {
            this.convertMemberInfo(orgAS.getStructureMembers());
            return orgAS;
        }
        final ArrayStructure newAS = ArrayStructureMA.factoryMA(orgAS);
        for (final StructureMembers.Member m : newAS.getMembers()) {
            VariableEnhanced v2 = (VariableEnhanced)this.findVariable(m.getName());
            if (v2 == null && this.orgVar != null) {
                v2 = this.findVariableFromOrgName(m.getName());
            }
            if (v2 == null) {
                continue;
            }
            if (v2 instanceof VariableDS) {
                final VariableDS vds = (VariableDS)v2;
                if (!vds.needConvert()) {
                    continue;
                }
                Array mdata = newAS.extractMemberArray(m);
                mdata = vds.convert(mdata);
                newAS.setMemberArray(m, mdata);
            }
            else {
                if (!(v2 instanceof StructureDS)) {
                    continue;
                }
                final StructureDS innerStruct = (StructureDS)v2;
                if (innerStruct.convertNeeded(null)) {
                    if (innerStruct.getDataType() == DataType.SEQUENCE) {
                        final ArrayObject.D1 seqArray = (ArrayObject.D1)newAS.extractMemberArray(m);
                        final ArrayObject.D1 newSeq = new ArrayObject.D1(ArraySequence.class, (int)seqArray.getSize());
                        m.setDataArray(newSeq);
                        for (int i = 0; i < seqArray.getSize(); ++i) {
                            final ArraySequence innerSeq = (ArraySequence)seqArray.get(i);
                            newSeq.set(i, new SequenceConverter(innerStruct, innerSeq));
                        }
                    }
                    else {
                        Array mdata = newAS.extractMemberArray(m);
                        mdata = innerStruct.convert(mdata, null);
                        newAS.setMemberArray(m, mdata);
                    }
                }
                innerStruct.convertMemberInfo(m.getStructureMembers());
            }
        }
        final StructureMembers sm = newAS.getStructureMembers();
        this.convertMemberInfo(sm);
        for (final Variable v3 : this.getVariables()) {
            if (!this.varHasData(v3, sm)) {
                try {
                    final Variable completeVar = this.getParentGroup().findVariable(v3.getShortName());
                    final Array mdata = completeVar.read(section);
                    final StructureMembers.Member j = sm.addMember(v3.getShortName(), v3.getDescription(), v3.getUnitsString(), v3.getDataType(), v3.getShape());
                    newAS.setMemberArray(j, mdata);
                }
                catch (InvalidRangeException e) {
                    throw new IOException(e.getMessage());
                }
            }
        }
        return newAS;
    }
    
    protected StructureData convert(final StructureData sdata, final int recno) throws IOException {
        if (!this.convertNeeded(sdata.getStructureMembers())) {
            this.convertMemberInfo(sdata.getStructureMembers());
            return sdata;
        }
        final StructureMembers smResult = new StructureMembers(sdata.getStructureMembers());
        final StructureDataW result = new StructureDataW(smResult);
        for (final StructureMembers.Member m : sdata.getMembers()) {
            VariableEnhanced v2 = (VariableEnhanced)this.findVariable(m.getName());
            if (v2 == null && this.orgVar != null) {
                v2 = this.findVariableFromOrgName(m.getName());
            }
            if (v2 == null) {
                StructureDS.log.warn("StructureDataDS.convert Cant find member " + m.getName());
            }
            else {
                final StructureMembers.Member mResult = smResult.findMember(m.getName());
                if (v2 instanceof VariableDS) {
                    final VariableDS vds = (VariableDS)v2;
                    Array mdata = sdata.getArray(m);
                    if (vds.needConvert()) {
                        mdata = vds.convert(mdata);
                    }
                    result.setMemberData(mResult, mdata);
                }
                if (!(v2 instanceof StructureDS)) {
                    continue;
                }
                final StructureDS innerStruct = (StructureDS)v2;
                if (innerStruct.getDataType() == DataType.SEQUENCE) {
                    final Array a = sdata.getArray(m);
                    if (a instanceof ArrayObject.D1) {
                        final ArrayObject.D1 seqArray = (ArrayObject.D1)a;
                        final ArrayObject.D1 newSeq = new ArrayObject.D1(ArraySequence.class, (int)seqArray.getSize());
                        mResult.setDataArray(newSeq);
                        for (int i = 0; i < seqArray.getSize(); ++i) {
                            final ArraySequence innerSeq = (ArraySequence)seqArray.get(i);
                            newSeq.set(i, new SequenceConverter(innerStruct, innerSeq));
                        }
                    }
                    else {
                        final ArraySequence seqArray2 = (ArraySequence)a;
                        result.setMemberData(mResult, new SequenceConverter(innerStruct, seqArray2));
                    }
                }
                else {
                    Array mdata = sdata.getArray(m);
                    mdata = innerStruct.convert(mdata, null);
                    result.setMemberData(mResult, mdata);
                }
                innerStruct.convertMemberInfo(mResult.getStructureMembers());
            }
        }
        final StructureMembers sm = result.getStructureMembers();
        this.convertMemberInfo(sm);
        for (final Variable v3 : this.getVariables()) {
            if (!this.varHasData(v3, sm)) {
                try {
                    final Variable completeVar = this.getParentGroup().findVariable(v3.getShortName());
                    final Array mdata2 = completeVar.read(new Section().appendRange(recno, recno));
                    final StructureMembers.Member j = sm.addMember(v3.getShortName(), v3.getDescription(), v3.getUnitsString(), v3.getDataType(), v3.getShape());
                    result.setMemberData(j, mdata2);
                }
                catch (InvalidRangeException e) {
                    throw new IOException(e.getMessage());
                }
            }
        }
        return result;
    }
    
    private void convertMemberInfo(final StructureMembers wrapperSm) {
        for (final StructureMembers.Member m : wrapperSm.getMembers()) {
            Variable v = this.findVariable(m.getName());
            if (v == null && this.orgVar != null) {
                v = (Variable)this.findVariableFromOrgName(m.getName());
            }
            if (v != null) {
                m.setVariableInfo(v.getShortName(), v.getDescription(), v.getUnitsString(), v.getDataType());
            }
            if (v instanceof StructureDS) {
                final StructureDS innerStruct = (StructureDS)v;
                innerStruct.convertMemberInfo(m.getStructureMembers());
            }
        }
    }
    
    private VariableEnhanced findVariableFromOrgName(final String orgName) {
        for (Variable v : this.getVariables()) {
            final Variable vTop = v;
            while (v instanceof VariableEnhanced) {
                final VariableEnhanced ve = (VariableEnhanced)v;
                if (ve.getOriginalName() != null && ve.getOriginalName().equals(orgName)) {
                    return (VariableEnhanced)vTop;
                }
                v = ve.getOriginalVariable();
            }
        }
        return null;
    }
    
    private boolean varHasData(Variable v, final StructureMembers sm) {
        if (sm.findMember(v.getShortName()) != null) {
            return true;
        }
        while (v instanceof VariableEnhanced) {
            final VariableEnhanced ve = (VariableEnhanced)v;
            if (sm.findMember(ve.getOriginalName()) != null) {
                return true;
            }
            v = ve.getOriginalVariable();
        }
        return false;
    }
    
    public void enhance(final Set<NetcdfDataset.Enhance> mode) {
        for (final Variable v : this.getVariables()) {
            final VariableEnhanced ve = (VariableEnhanced)v;
            ve.enhance(mode);
        }
    }
    
    public void clearCoordinateSystems() {
        this.proxy = new EnhancementsImpl(this, this.getUnitsString(), this.getDescription());
    }
    
    public void addCoordinateSystem(final CoordinateSystem p0) {
        this.proxy.addCoordinateSystem(p0);
    }
    
    public void removeCoordinateSystem(final CoordinateSystem p0) {
        this.proxy.removeCoordinateSystem(p0);
    }
    
    public List<CoordinateSystem> getCoordinateSystems() {
        return this.proxy.getCoordinateSystems();
    }
    
    @Override
    public String getDescription() {
        return this.proxy.getDescription();
    }
    
    @Override
    public String getUnitsString() {
        return this.proxy.getUnitsString();
    }
    
    public void setUnitsString(final String units) {
        this.proxy.setUnitsString(units);
    }
    
    static {
        StructureDS.log = LoggerFactory.getLogger(StructureDS.class);
    }
    
    private class SequenceConverter extends ArraySequence
    {
        StructureDS orgStruct;
        ArraySequence orgSeq;
        
        SequenceConverter(final StructureDS orgStruct, final ArraySequence orgSeq) {
            super(orgSeq.getStructureMembers(), orgSeq.getShape());
            this.orgStruct = orgStruct;
            this.orgSeq = orgSeq;
            this.nelems = orgSeq.getStructureDataCount();
            orgStruct.convertMemberInfo(this.members = new StructureMembers(orgSeq.getStructureMembers()));
        }
        
        @Override
        public StructureDataIterator getStructureDataIterator() throws IOException {
            return new StructureDataConverter(this.orgStruct, this.orgSeq.getStructureDataIterator());
        }
    }
    
    private class StructureDataConverter implements StructureDataIterator
    {
        private StructureDataIterator orgIter;
        private StructureDS newStruct;
        private int count;
        
        StructureDataConverter(final StructureDS newStruct, final StructureDataIterator orgIter) {
            this.count = 0;
            this.newStruct = newStruct;
            this.orgIter = orgIter;
        }
        
        public boolean hasNext() throws IOException {
            return this.orgIter.hasNext();
        }
        
        public StructureData next() throws IOException {
            final StructureData sdata = this.orgIter.next();
            return this.newStruct.convert(sdata, this.count++);
        }
        
        public void setBufferSize(final int bytes) {
            this.orgIter.setBufferSize(bytes);
        }
        
        public StructureDataIterator reset() {
            this.orgIter.reset();
            return this;
        }
        
        public int getCurrentRecno() {
            return this.orgIter.getCurrentRecno();
        }
    }
}
