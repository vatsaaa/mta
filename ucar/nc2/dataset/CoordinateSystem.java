// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset;

import ucar.nc2.Variable;
import ucar.nc2.VariableIF;
import ucar.unidata.geoloc.projection.LatLonProjection;
import java.util.Iterator;
import ucar.nc2.constants.AxisType;
import java.util.Comparator;
import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import ucar.unidata.geoloc.ProjectionImpl;
import ucar.nc2.Dimension;
import java.util.List;

public class CoordinateSystem
{
    protected NetcdfDataset ds;
    protected List<CoordinateAxis> coordAxes;
    protected List<CoordinateTransform> coordTrans;
    protected List<Dimension> domain;
    protected String name;
    protected CoordinateAxis xAxis;
    protected CoordinateAxis yAxis;
    protected CoordinateAxis zAxis;
    protected CoordinateAxis tAxis;
    protected CoordinateAxis latAxis;
    protected CoordinateAxis lonAxis;
    protected CoordinateAxis hAxis;
    protected CoordinateAxis pAxis;
    protected CoordinateAxis ensAxis;
    protected CoordinateAxis aziAxis;
    protected CoordinateAxis elevAxis;
    protected CoordinateAxis radialAxis;
    protected boolean isImplicit;
    protected String dataType;
    private ProjectionImpl projection;
    private volatile int hashCode;
    
    public static String makeName(final List<CoordinateAxis> axes) {
        final List<CoordinateAxis> axesSorted = new ArrayList<CoordinateAxis>(axes);
        Collections.sort(axesSorted, new CoordinateAxis.AxisComparator());
        final StringBuilder buff = new StringBuilder();
        for (int i = 0; i < axesSorted.size(); ++i) {
            final CoordinateAxis axis = axesSorted.get(i);
            if (i > 0) {
                buff.append(" ");
            }
            buff.append(axis.getName());
        }
        return buff.toString();
    }
    
    protected CoordinateSystem() {
        this.coordAxes = new ArrayList<CoordinateAxis>();
        this.coordTrans = new ArrayList<CoordinateTransform>();
        this.domain = new ArrayList<Dimension>();
        this.projection = null;
        this.hashCode = 0;
    }
    
    public CoordinateSystem(final NetcdfDataset ds, final Collection<CoordinateAxis> axes, final Collection<CoordinateTransform> coordTrans) {
        this.coordAxes = new ArrayList<CoordinateAxis>();
        this.coordTrans = new ArrayList<CoordinateTransform>();
        this.domain = new ArrayList<Dimension>();
        this.projection = null;
        this.hashCode = 0;
        this.ds = ds;
        this.coordAxes = new ArrayList<CoordinateAxis>(axes);
        this.name = makeName(this.coordAxes);
        if (coordTrans != null) {
            this.coordTrans = new ArrayList<CoordinateTransform>(coordTrans);
        }
        for (final CoordinateAxis axis : this.coordAxes) {
            final AxisType axisType = axis.getAxisType();
            if (axisType != null) {
                if (axisType == AxisType.GeoX) {
                    this.xAxis = this.lesserRank(this.xAxis, axis);
                }
                if (axisType == AxisType.GeoY) {
                    this.yAxis = this.lesserRank(this.yAxis, axis);
                }
                if (axisType == AxisType.GeoZ) {
                    this.zAxis = this.lesserRank(this.zAxis, axis);
                }
                if (axisType == AxisType.Time) {
                    this.tAxis = this.lesserRank(this.tAxis, axis);
                }
                if (axisType == AxisType.Lat) {
                    this.latAxis = this.lesserRank(this.latAxis, axis);
                }
                if (axisType == AxisType.Lon) {
                    this.lonAxis = this.lesserRank(this.lonAxis, axis);
                }
                if (axisType == AxisType.Height) {
                    this.hAxis = this.lesserRank(this.hAxis, axis);
                }
                if (axisType == AxisType.Pressure) {
                    this.pAxis = this.lesserRank(this.pAxis, axis);
                }
                if (axisType == AxisType.Ensemble) {
                    this.ensAxis = this.lesserRank(this.ensAxis, axis);
                }
                if (axisType == AxisType.RadialAzimuth) {
                    this.aziAxis = this.lesserRank(this.aziAxis, axis);
                }
                if (axisType == AxisType.RadialDistance) {
                    this.radialAxis = this.lesserRank(this.radialAxis, axis);
                }
                if (axisType == AxisType.RadialElevation) {
                    this.elevAxis = this.lesserRank(this.elevAxis, axis);
                }
            }
            final List<Dimension> dims = axis.getDimensions();
            for (final Dimension dim : dims) {
                if (!this.domain.contains(dim)) {
                    this.domain.add(dim);
                }
            }
        }
    }
    
    private CoordinateAxis lesserRank(final CoordinateAxis a1, final CoordinateAxis a2) {
        if (a1 == null) {
            return a2;
        }
        return (a1.getRank() <= a2.getRank()) ? a1 : a2;
    }
    
    public void addCoordinateTransform(final CoordinateTransform ct) {
        this.coordTrans.add(ct);
        this.ds.addCoordinateTransform(ct);
    }
    
    public void addCoordinateTransforms(final Collection<CoordinateTransform> ct) {
        if (ct != null) {
            this.coordTrans.addAll(ct);
        }
    }
    
    public List<CoordinateAxis> getCoordinateAxes() {
        return this.coordAxes;
    }
    
    public List<CoordinateTransform> getCoordinateTransforms() {
        return this.coordTrans;
    }
    
    public String getName() {
        return this.name;
    }
    
    public NetcdfDataset getNetcdfDataset() {
        return this.ds;
    }
    
    public List<Dimension> getDomain() {
        return this.domain;
    }
    
    public int getRankDomain() {
        return this.domain.size();
    }
    
    public int getRankRange() {
        return this.coordAxes.size();
    }
    
    public CoordinateAxis findAxis(final AxisType type) {
        CoordinateAxis result = null;
        for (final CoordinateAxis axis : this.coordAxes) {
            final AxisType axisType = axis.getAxisType();
            if (axisType != null && axisType == type) {
                result = this.lesserRank(result, axis);
            }
        }
        return result;
    }
    
    public CoordinateAxis getXaxis() {
        return this.xAxis;
    }
    
    public CoordinateAxis getYaxis() {
        return this.yAxis;
    }
    
    public CoordinateAxis getZaxis() {
        return this.zAxis;
    }
    
    public CoordinateAxis getTaxis() {
        return this.tAxis;
    }
    
    public CoordinateAxis getLatAxis() {
        return this.latAxis;
    }
    
    public CoordinateAxis getLonAxis() {
        return this.lonAxis;
    }
    
    public CoordinateAxis getHeightAxis() {
        return this.hAxis;
    }
    
    public CoordinateAxis getPressureAxis() {
        return this.pAxis;
    }
    
    public CoordinateAxis getEnsembleAxis() {
        return this.ensAxis;
    }
    
    public CoordinateAxis getAzimuthAxis() {
        return this.aziAxis;
    }
    
    public CoordinateAxis getRadialAxis() {
        return this.radialAxis;
    }
    
    public CoordinateAxis getElevationAxis() {
        return this.elevAxis;
    }
    
    public ProjectionCT getProjectionCT() {
        for (final CoordinateTransform ct : this.coordTrans) {
            if (ct instanceof ProjectionCT) {
                return (ProjectionCT)ct;
            }
        }
        return null;
    }
    
    public ProjectionImpl getProjection() {
        if (this.projection == null) {
            if (this.isLatLon()) {
                this.projection = new LatLonProjection();
            }
            final ProjectionCT projCT = this.getProjectionCT();
            if (null != projCT) {
                this.projection = projCT.getProjection();
            }
        }
        return this.projection;
    }
    
    public boolean isGeoXY() {
        return this.xAxis != null && this.yAxis != null && null != this.getProjection() && !(this.projection instanceof LatLonProjection);
    }
    
    public boolean isLatLon() {
        return this.latAxis != null && this.lonAxis != null;
    }
    
    public boolean isRadial() {
        return this.radialAxis != null && this.aziAxis != null;
    }
    
    public boolean isGeoReferencing() {
        return this.isGeoXY() || this.isLatLon();
    }
    
    public boolean isProductSet() {
        for (final CoordinateAxis axis : this.coordAxes) {
            if (!(axis instanceof CoordinateAxis1D)) {
                return false;
            }
        }
        return true;
    }
    
    public boolean isRegular() {
        for (final CoordinateAxis axis : this.coordAxes) {
            if (!(axis instanceof CoordinateAxis1D)) {
                return false;
            }
            if (!((CoordinateAxis1D)axis).isRegular()) {
                return false;
            }
        }
        return true;
    }
    
    public boolean isComplete(final VariableIF v) {
        return isSubset(v.getDimensionsAll(), this.domain);
    }
    
    public boolean isCoordinateSystemFor(final VariableIF v) {
        return isSubset(this.domain, v.getDimensionsAll());
    }
    
    public static boolean isSubset(final List<Dimension> subset, final List<Dimension> set) {
        for (final Dimension d : subset) {
            if (!set.contains(d)) {
                return false;
            }
        }
        return true;
    }
    
    public static List<Dimension> makeDomain(final Variable[] axes) {
        final List<Dimension> domain = new ArrayList<Dimension>(10);
        for (final Variable axis : axes) {
            for (final Dimension dim : axis.getDimensions()) {
                if (!domain.contains(dim)) {
                    domain.add(dim);
                }
            }
        }
        return domain;
    }
    
    public boolean isImplicit() {
        return this.isImplicit;
    }
    
    protected void setImplicit(final boolean isImplicit) {
        this.isImplicit = isImplicit;
    }
    
    public boolean hasVerticalAxis() {
        return this.hAxis != null || this.pAxis != null || this.zAxis != null;
    }
    
    public boolean hasTimeAxis() {
        return this.tAxis != null;
    }
    
    public boolean containsAxes(final List<CoordinateAxis> wantAxes) {
        for (final CoordinateAxis ca : wantAxes) {
            if (!this.containsAxis(ca.getName())) {
                return false;
            }
        }
        return true;
    }
    
    public boolean containsAxis(final String axisName) {
        for (final CoordinateAxis ca : this.coordAxes) {
            if (ca.getName().equals(axisName)) {
                return true;
            }
        }
        return false;
    }
    
    public boolean containsDomain(final List<Dimension> wantDimensions) {
        for (final Dimension d : wantDimensions) {
            if (!this.domain.contains(d)) {
                return false;
            }
        }
        return true;
    }
    
    public boolean containsAxisTypes(final List<AxisType> wantAxes) {
        for (final AxisType wantAxisType : wantAxes) {
            if (!this.containsAxisType(wantAxisType)) {
                return false;
            }
        }
        return true;
    }
    
    public boolean containsAxisType(final AxisType wantAxisType) {
        for (final CoordinateAxis ca : this.coordAxes) {
            if (ca.getAxisType() == wantAxisType) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public boolean equals(final Object oo) {
        if (this == oo) {
            return true;
        }
        if (!(oo instanceof CoordinateSystem)) {
            return false;
        }
        final CoordinateSystem o = (CoordinateSystem)oo;
        if (!this.getName().equals(o.getName())) {
            return false;
        }
        final List<CoordinateAxis> oaxes = o.getCoordinateAxes();
        for (final CoordinateAxis axis : this.getCoordinateAxes()) {
            if (!oaxes.contains(axis)) {
                return false;
            }
        }
        final List<CoordinateTransform> otrans = o.getCoordinateTransforms();
        for (final CoordinateTransform tran : this.getCoordinateTransforms()) {
            if (!otrans.contains(tran)) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int result = 17;
            result = 37 * result + this.getName().hashCode();
            result = 37 * result + this.getCoordinateAxes().hashCode();
            result = 37 * result + this.getCoordinateTransforms().hashCode();
            this.hashCode = result;
        }
        return this.hashCode;
    }
    
    @Override
    public String toString() {
        return this.name;
    }
}
