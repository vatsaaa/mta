// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset;

import ucar.ma2.IndexIterator;
import java.util.Formatter;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Section;
import java.io.IOException;
import ucar.nc2.util.CancelTask;
import java.util.List;
import ucar.ma2.Array;
import java.util.Collection;
import java.util.Set;
import ucar.nc2.Attribute;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Structure;
import ucar.nc2.Group;
import ucar.ma2.DataType;
import java.util.EnumSet;
import ucar.nc2.Variable;

public class VariableDS extends Variable implements VariableEnhanced, EnhanceScaleMissing
{
    private EnhancementsImpl enhanceProxy;
    private EnhanceScaleMissingImpl scaleMissingProxy;
    private EnumSet<NetcdfDataset.Enhance> enhanceMode;
    private boolean needScaleOffsetMissing;
    private boolean needEnumConversion;
    protected Variable orgVar;
    protected DataType orgDataType;
    protected String orgName;
    
    public VariableDS(final NetcdfDataset ds, final Group group, final Structure parentStructure, final String shortName, final DataType dataType, final String dims, final String units, final String desc) {
        super(ds, group, parentStructure, shortName);
        this.needScaleOffsetMissing = false;
        this.needEnumConversion = false;
        this.setDataType(dataType);
        this.setDimensions(dims);
        if (dataType == DataType.STRUCTURE) {
            throw new IllegalArgumentException("VariableDS must not wrap a Structure; name=" + shortName);
        }
        if (units != null) {
            this.addAttribute(new Attribute("units", units));
        }
        if (desc != null) {
            this.addAttribute(new Attribute("long_name", desc));
        }
        this.enhanceProxy = new EnhancementsImpl(this, units, desc);
        this.scaleMissingProxy = new EnhanceScaleMissingImpl();
    }
    
    public VariableDS(final Group group, final Structure parent, final String shortName, final Variable orgVar) {
        super(null, group, parent, shortName);
        this.needScaleOffsetMissing = false;
        this.needEnumConversion = false;
        this.setDimensions(this.getDimensionsString());
        if (orgVar instanceof Structure) {
            throw new IllegalArgumentException("VariableDS must not wrap a Structure; name=" + orgVar.getName());
        }
        this.ncfile = null;
        this.spiObject = null;
        this.createNewCache();
        this.orgVar = orgVar;
        this.orgDataType = orgVar.getDataType();
        this.enhanceProxy = new EnhancementsImpl(this);
        this.scaleMissingProxy = new EnhanceScaleMissingImpl();
    }
    
    public VariableDS(final Group g, final Variable orgVar, final boolean enhance) {
        super(orgVar);
        this.needScaleOffsetMissing = false;
        this.needEnumConversion = false;
        if (g != null) {
            this.group = g;
        }
        this.setDimensions(this.getDimensionsString());
        if (orgVar instanceof Structure) {
            throw new IllegalArgumentException("VariableDS must not wrap a Structure; name=" + orgVar.getName());
        }
        this.ncfile = null;
        this.spiObject = null;
        this.createNewCache();
        this.orgVar = orgVar;
        this.orgDataType = orgVar.getDataType();
        if (orgVar instanceof VariableDS) {
            final VariableDS ncVarDS = (VariableDS)orgVar;
            this.enhanceProxy = ncVarDS.enhanceProxy;
            this.scaleMissingProxy = ncVarDS.scaleMissingProxy;
            this.enhanceMode = ncVarDS.enhanceMode;
        }
        else {
            this.enhanceProxy = new EnhancementsImpl(this);
            if (enhance) {
                this.enhance(NetcdfDataset.getDefaultEnhanceMode());
            }
            else {
                this.scaleMissingProxy = new EnhanceScaleMissingImpl();
            }
        }
    }
    
    protected VariableDS(final VariableDS vds, final boolean isCopy) {
        super(vds);
        this.needScaleOffsetMissing = false;
        this.needEnumConversion = false;
        this.orgVar = vds;
        this.orgDataType = vds.orgDataType;
        this.orgName = vds.orgName;
        this.scaleMissingProxy = vds.scaleMissingProxy;
        this.enhanceProxy = new EnhancementsImpl(this);
        this.enhanceMode = vds.enhanceMode;
        if (!isCopy) {
            this.createNewCache();
        }
    }
    
    @Override
    protected Variable copy() {
        return new VariableDS(this, true);
    }
    
    public void clearCoordinateSystems() {
        this.enhanceProxy = new EnhancementsImpl(this, this.getUnitsString(), this.getDescription());
    }
    
    public void enhance(final Set<NetcdfDataset.Enhance> mode) {
        this.enhanceMode = EnumSet.copyOf(mode);
        boolean alreadyScaleOffsetMissing = false;
        boolean alreadyEnumConversion = false;
        if (this.orgVar != null && this.orgVar instanceof VariableDS) {
            final VariableDS orgVarDS = (VariableDS)this.orgVar;
            final EnumSet<NetcdfDataset.Enhance> orgEnhanceMode = orgVarDS.getEnhanceMode();
            if (orgEnhanceMode != null) {
                if (orgEnhanceMode.contains(NetcdfDataset.Enhance.ScaleMissing)) {
                    alreadyScaleOffsetMissing = true;
                    this.enhanceMode.add(NetcdfDataset.Enhance.ScaleMissing);
                }
                if (orgEnhanceMode.contains(NetcdfDataset.Enhance.ConvertEnums)) {
                    alreadyEnumConversion = true;
                    this.enhanceMode.add(NetcdfDataset.Enhance.ConvertEnums);
                }
            }
        }
        if ((!alreadyScaleOffsetMissing && (this.dataType.isNumeric() || this.dataType == DataType.CHAR) && mode.contains(NetcdfDataset.Enhance.ScaleMissing)) || mode.contains(NetcdfDataset.Enhance.ScaleMissingDefer)) {
            this.scaleMissingProxy = new EnhanceScaleMissingImpl(this);
            if (mode.contains(NetcdfDataset.Enhance.ScaleMissing) && this.scaleMissingProxy.hasScaleOffset() && this.scaleMissingProxy.getConvertedDataType() != this.getDataType()) {
                this.setDataType(this.scaleMissingProxy.getConvertedDataType());
                this.removeAttributeIgnoreCase("_Unsigned");
            }
            this.needScaleOffsetMissing = (mode.contains(NetcdfDataset.Enhance.ScaleMissing) && (this.scaleMissingProxy.hasScaleOffset() || this.scaleMissingProxy.getUseNaNs()));
        }
        if (!alreadyEnumConversion && mode.contains(NetcdfDataset.Enhance.ConvertEnums) && this.dataType.isEnum()) {
            this.needEnumConversion = true;
            this.setDataType(DataType.STRING);
            this.removeAttributeIgnoreCase("_Unsigned");
        }
    }
    
    boolean needConvert() {
        return this.needScaleOffsetMissing || this.needEnumConversion || (this.orgVar != null && this.orgVar instanceof VariableDS && ((VariableDS)this.orgVar).needConvert());
    }
    
    Array convert(final Array data) {
        if (this.needScaleOffsetMissing) {
            return this.convertScaleOffsetMissing(data);
        }
        if (this.needEnumConversion) {
            return this.convertEnums(data);
        }
        if (this.orgVar != null && this.orgVar instanceof VariableDS) {
            return ((VariableDS)this.orgVar).convert(data);
        }
        return data;
    }
    
    public EnumSet<NetcdfDataset.Enhance> getEnhanceMode() {
        return this.enhanceMode;
    }
    
    public void addCoordinateSystem(final CoordinateSystem p0) {
        this.enhanceProxy.addCoordinateSystem(p0);
    }
    
    public void removeCoordinateSystem(final CoordinateSystem p0) {
        this.enhanceProxy.removeCoordinateSystem(p0);
    }
    
    public List<CoordinateSystem> getCoordinateSystems() {
        return this.enhanceProxy.getCoordinateSystems();
    }
    
    @Override
    public String getDescription() {
        return this.enhanceProxy.getDescription();
    }
    
    @Override
    public String getUnitsString() {
        return this.enhanceProxy.getUnitsString();
    }
    
    public void setUnitsString(final String units) {
        this.enhanceProxy.setUnitsString(units);
    }
    
    public Array convertScaleOffsetMissing(final Array data) {
        return this.scaleMissingProxy.convertScaleOffsetMissing(data);
    }
    
    public double getValidMax() {
        return this.scaleMissingProxy.getValidMax();
    }
    
    public double getValidMin() {
        return this.scaleMissingProxy.getValidMin();
    }
    
    public boolean hasFillValue() {
        return this.scaleMissingProxy.hasFillValue();
    }
    
    public boolean hasInvalidData() {
        return this.scaleMissingProxy.hasInvalidData();
    }
    
    public boolean hasMissing() {
        return this.scaleMissingProxy.hasMissing();
    }
    
    public boolean hasMissingValue() {
        return this.scaleMissingProxy.hasMissingValue();
    }
    
    public boolean hasScaleOffset() {
        return this.scaleMissingProxy.hasScaleOffset();
    }
    
    public boolean isFillValue(final double p0) {
        return this.scaleMissingProxy.isFillValue(p0);
    }
    
    public boolean isInvalidData(final double p0) {
        return this.scaleMissingProxy.isInvalidData(p0);
    }
    
    public boolean isMissing(final double val) {
        return this.scaleMissingProxy.isMissing(val);
    }
    
    public boolean isMissingFast(final double val) {
        return this.scaleMissingProxy.isMissingFast(val);
    }
    
    public boolean isMissingValue(final double p0) {
        return this.scaleMissingProxy.isMissingValue(p0);
    }
    
    public void setFillValueIsMissing(final boolean p0) {
        this.scaleMissingProxy.setFillValueIsMissing(p0);
    }
    
    public void setInvalidDataIsMissing(final boolean p0) {
        this.scaleMissingProxy.setInvalidDataIsMissing(p0);
    }
    
    public void setMissingDataIsMissing(final boolean p0) {
        this.scaleMissingProxy.setMissingDataIsMissing(p0);
    }
    
    public void setUseNaNs(final boolean useNaNs) {
        this.scaleMissingProxy.setUseNaNs(useNaNs);
    }
    
    public boolean getUseNaNs() {
        return this.scaleMissingProxy.getUseNaNs();
    }
    
    public double convertScaleOffsetMissing(final byte value) {
        return this.scaleMissingProxy.convertScaleOffsetMissing(value);
    }
    
    public double convertScaleOffsetMissing(final short value) {
        return this.scaleMissingProxy.convertScaleOffsetMissing(value);
    }
    
    public double convertScaleOffsetMissing(final int value) {
        return this.scaleMissingProxy.convertScaleOffsetMissing(value);
    }
    
    public double convertScaleOffsetMissing(final long value) {
        return this.scaleMissingProxy.convertScaleOffsetMissing(value);
    }
    
    public double convertScaleOffsetMissing(final double value) {
        return this.scaleMissingProxy.convertScaleOffsetMissing(value);
    }
    
    public Variable getOriginalVariable() {
        return this.orgVar;
    }
    
    public void setOriginalVariable(final Variable orgVar) {
        if (orgVar instanceof Structure) {
            throw new IllegalArgumentException("VariableDS must not wrap a Structure; name=" + orgVar.getName());
        }
        this.orgVar = orgVar;
    }
    
    public DataType getOriginalDataType() {
        return (this.orgDataType != null) ? this.orgDataType : this.getDataType();
    }
    
    public String getOriginalName() {
        return this.orgName;
    }
    
    @Override
    public String lookupEnumString(final int val) {
        if (this.dataType.isEnum()) {
            return super.lookupEnumString(val);
        }
        return this.orgVar.lookupEnumString(val);
    }
    
    @Override
    public void setName(final String newName) {
        this.orgName = this.shortName;
        super.setName(newName);
    }
    
    @Override
    public String toStringDebug() {
        return (this.orgVar != null) ? this.orgVar.toStringDebug() : "";
    }
    
    public boolean hasCachedDataRecurse() {
        return super.hasCachedData() || (this.orgVar != null && this.orgVar.hasCachedData());
    }
    
    @Override
    protected Array _read() throws IOException {
        Array result;
        if (this.hasCachedData()) {
            result = super._read();
        }
        else {
            result = this.proxyReader.reallyRead(this, null);
        }
        if (this.needScaleOffsetMissing) {
            return this.convertScaleOffsetMissing(result);
        }
        if (this.needEnumConversion) {
            return this.convertEnums(result);
        }
        return result;
    }
    
    @Override
    public Array reallyRead(final Variable client, final CancelTask cancelTask) throws IOException {
        if (this.orgVar == null) {
            return this.getMissingDataArray(this.shape);
        }
        return this.orgVar.read();
    }
    
    @Override
    protected Array _read(final Section section) throws IOException, InvalidRangeException {
        if (null == section || section.computeSize() == this.getSize()) {
            return this._read();
        }
        Array result;
        if (this.hasCachedData()) {
            result = super._read(section);
        }
        else {
            result = this.proxyReader.reallyRead(this, section, null);
        }
        if (this.needScaleOffsetMissing) {
            return this.convertScaleOffsetMissing(result);
        }
        if (this.needEnumConversion) {
            return this.convertEnums(result);
        }
        return result;
    }
    
    @Override
    public Array reallyRead(final Variable client, final Section section, final CancelTask cancelTask) throws IOException, InvalidRangeException {
        if (null == section || section.computeSize() == this.getSize()) {
            return this.reallyRead(client, cancelTask);
        }
        if (this.orgVar == null) {
            return this.getMissingDataArray(section.getShape());
        }
        return this.orgVar.read(section);
    }
    
    public Array getMissingDataArray(final int[] shape) {
        final Object data = this.scaleMissingProxy.getFillValue(this.getDataType());
        return Array.factoryConstant(this.dataType.getPrimitiveClassType(), shape, data);
    }
    
    public void showScaleMissingProxy(final Formatter f) {
        f.format("use NaNs = %s%n", this.scaleMissingProxy.getUseNaNs());
        f.format("has missing = %s%n", this.scaleMissingProxy.hasMissing());
        if (this.scaleMissingProxy.hasMissing()) {
            if (this.scaleMissingProxy.hasMissingValue()) {
                f.format("   missing value(s) = ", new Object[0]);
                for (final double d : this.scaleMissingProxy.getMissingValues()) {
                    f.format(" %f", d);
                }
                f.format("%n", new Object[0]);
            }
            if (this.scaleMissingProxy.hasFillValue()) {
                f.format("   fillValue = %f%n", this.scaleMissingProxy.getFillValue());
            }
            if (this.scaleMissingProxy.hasInvalidData()) {
                f.format("   valid min/max = [%f,%f]%n", this.scaleMissingProxy.getValidMin(), this.scaleMissingProxy.getValidMax());
            }
        }
        final Object mv = this.scaleMissingProxy.getFillValue(this.getDataType());
        final String mvs = (String)((mv instanceof String) ? mv : java.lang.reflect.Array.get(mv, 0).toString());
        f.format("FillValue or default = %s%n", mvs);
        f.format("%nhas scale/offset = %s%n", this.scaleMissingProxy.hasScaleOffset());
        if (this.scaleMissingProxy.hasScaleOffset()) {
            final double offset = this.scaleMissingProxy.convertScaleOffsetMissing(0.0);
            final double scale = this.scaleMissingProxy.convertScaleOffsetMissing(1.0) - offset;
            f.format("   scale_factor = %f add_offset = %f%n", scale, offset);
        }
        f.format("original data type = %s%n", this.getDataType());
        f.format("converted data type = %s%n", this.scaleMissingProxy.getConvertedDataType());
    }
    
    protected Array convertEnums(final Array values) {
        final DataType dt = DataType.getType(values.getElementType());
        if (!dt.isNumeric()) {
            System.out.println("HEY !dt.isNumeric()");
        }
        final Array result = Array.factory(DataType.STRING, values.getShape());
        final IndexIterator ii = result.getIndexIterator();
        values.resetLocalIterator();
        while (values.hasNext()) {
            final String sval = this.lookupEnumString(values.nextInt());
            ii.setObjectNext(sval);
        }
        return result;
    }
}
