// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset;

import ucar.nc2.Variable;

public interface ReplaceVariableCheck
{
    boolean replace(final Variable p0);
}
