// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset;

import org.slf4j.LoggerFactory;
import ucar.ma2.InvalidRangeException;
import java.util.List;
import java.util.ArrayList;
import ucar.ma2.Range;
import ucar.ma2.Array;
import java.io.IOException;
import ucar.nc2.Variable;
import ucar.ma2.ArrayDouble;
import org.slf4j.Logger;

public class CoordinateAxis2D extends CoordinateAxis
{
    private static Logger log;
    private static final boolean debug = false;
    private ArrayDouble.D2 midpoint;
    
    public CoordinateAxis2D(final NetcdfDataset ncd, final VariableDS vds) {
        super(ncd, vds);
        this.midpoint = null;
    }
    
    @Override
    protected Variable copy() {
        return new CoordinateAxis2D(this.ncd, this);
    }
    
    public double getCoordValue(final int i, final int j) {
        if (this.midpoint == null) {
            this.doRead();
        }
        return this.midpoint.get(i, j);
    }
    
    private void doRead() {
        Array data;
        try {
            data = this.read();
        }
        catch (IOException ioe) {
            CoordinateAxis2D.log.error("Error reading coordinate values " + ioe);
            throw new IllegalStateException(ioe);
        }
        data = data.reduce();
        if (data.getRank() != 2) {
            throw new IllegalArgumentException("must be 2D");
        }
        this.midpoint = (ArrayDouble.D2)Array.factory(Double.TYPE, data.getShape(), data.get1DJavaArray(Double.TYPE));
    }
    
    public double[] getCoordValues() {
        if (this.midpoint == null) {
            this.doRead();
        }
        if (!this.isNumeric()) {
            throw new UnsupportedOperationException("CoordinateAxis2D.getCoordValues() on non-numeric");
        }
        return (double[])this.midpoint.get1DJavaArray(Double.TYPE);
    }
    
    public CoordinateAxis2D section(final Range r1, final Range r2) throws InvalidRangeException {
        final List<Range> section = new ArrayList<Range>();
        section.add(r1);
        section.add(r2);
        return (CoordinateAxis2D)this.section(section);
    }
    
    public ArrayDouble.D2 getMidpoints() {
        if (this.midpoint == null) {
            this.doRead();
        }
        return this.midpoint;
    }
    
    public static ArrayDouble.D2 makeXEdges(final ArrayDouble.D2 midx) {
        final int[] shape = midx.getShape();
        final int ny = shape[0];
        final int nx = shape[1];
        final ArrayDouble.D2 edgex = new ArrayDouble.D2(ny + 1, nx + 1);
        for (int y = 0; y < ny - 1; ++y) {
            for (int x = 0; x < nx - 1; ++x) {
                final double xval = (midx.get(y, x) + midx.get(y, x + 1) + midx.get(y + 1, x) + midx.get(y + 1, x + 1)) / 4.0;
                edgex.set(y + 1, x + 1, xval);
            }
            edgex.set(y + 1, 0, edgex.get(y + 1, 1) - (edgex.get(y + 1, 2) - edgex.get(y + 1, 1)));
            edgex.set(y + 1, nx, edgex.get(y + 1, nx - 1) + (edgex.get(y + 1, nx - 1) - edgex.get(y + 1, nx - 2)));
        }
        for (int x2 = 0; x2 < nx + 1; ++x2) {
            edgex.set(0, x2, edgex.get(1, x2) - (edgex.get(2, x2) - edgex.get(1, x2)));
            edgex.set(ny, x2, edgex.get(ny - 1, x2) + (edgex.get(ny - 1, x2) - edgex.get(ny - 2, x2)));
        }
        return edgex;
    }
    
    public static ArrayDouble.D2 makeYEdges(final ArrayDouble.D2 midy) {
        final int[] shape = midy.getShape();
        final int ny = shape[0];
        final int nx = shape[1];
        final ArrayDouble.D2 edgey = new ArrayDouble.D2(ny + 1, nx + 1);
        for (int y = 0; y < ny - 1; ++y) {
            for (int x = 0; x < nx - 1; ++x) {
                final double xval = (midy.get(y, x) + midy.get(y, x + 1) + midy.get(y + 1, x) + midy.get(y + 1, x + 1)) / 4.0;
                edgey.set(y + 1, x + 1, xval);
            }
            edgey.set(y + 1, 0, edgey.get(y + 1, 1) - (edgey.get(y + 1, 2) - edgey.get(y + 1, 1)));
            edgey.set(y + 1, nx, edgey.get(y + 1, nx - 1) + (edgey.get(y + 1, nx - 1) - edgey.get(y + 1, nx - 2)));
        }
        for (int x2 = 0; x2 < nx + 1; ++x2) {
            edgey.set(0, x2, edgey.get(1, x2) - (edgey.get(2, x2) - edgey.get(1, x2)));
            edgey.set(ny, x2, edgey.get(ny - 1, x2) + (edgey.get(ny - 1, x2) - edgey.get(ny - 2, x2)));
        }
        return edgey;
    }
    
    public static ArrayDouble.D2 makeXEdgesRotated(final ArrayDouble.D2 midx) {
        final int[] shape = midx.getShape();
        final int ny = shape[0];
        final int nx = shape[1];
        final ArrayDouble.D2 edgex = new ArrayDouble.D2(ny + 2, nx + 1);
        for (int y = 0; y < ny; ++y) {
            for (int x = 1; x < nx; ++x) {
                final double xval = (midx.get(y, x - 1) + midx.get(y, x)) / 2.0;
                edgex.set(y + 1, x, xval);
            }
            edgex.set(y + 1, 0, midx.get(y, 0) - (edgex.get(y + 1, 1) - midx.get(y, 0)));
            edgex.set(y + 1, nx, midx.get(y, nx - 1) - (edgex.get(y + 1, nx - 1) - midx.get(y, nx - 1)));
        }
        for (int x2 = 0; x2 < nx; ++x2) {
            edgex.set(0, x2, midx.get(0, x2));
        }
        for (int x2 = 0; x2 < nx - 1; ++x2) {
            edgex.set(ny + 1, x2, midx.get(ny - 1, x2));
        }
        return edgex;
    }
    
    public static ArrayDouble.D2 makeYEdgesRotated(final ArrayDouble.D2 midy) {
        final int[] shape = midy.getShape();
        final int ny = shape[0];
        final int nx = shape[1];
        final ArrayDouble.D2 edgey = new ArrayDouble.D2(ny + 2, nx + 1);
        for (int y = 0; y < ny; ++y) {
            for (int x = 1; x < nx; ++x) {
                final double yval = (midy.get(y, x - 1) + midy.get(y, x)) / 2.0;
                edgey.set(y + 1, x, yval);
            }
            edgey.set(y + 1, 0, midy.get(y, 0) - (edgey.get(y + 1, 1) - midy.get(y, 0)));
            edgey.set(y + 1, nx, midy.get(y, nx - 1) - (edgey.get(y + 1, nx - 1) - midy.get(y, nx - 1)));
        }
        for (int x2 = 0; x2 < nx; ++x2) {
            final double pt0 = midy.get(0, x2);
            final double pt2 = edgey.get(2, x2);
            final double diff = pt0 - pt2;
            edgey.set(0, x2, pt0 + diff);
        }
        for (int x2 = 0; x2 < nx - 1; ++x2) {
            final double pt0 = midy.get(ny - 1, x2);
            final double pt2 = edgey.get(ny - 1, x2);
            final double diff = pt0 - pt2;
            edgey.set(ny + 1, x2, pt0 + diff);
        }
        return edgey;
    }
    
    static {
        CoordinateAxis2D.log = LoggerFactory.getLogger(CoordinateAxis2D.class);
    }
}
