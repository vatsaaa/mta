// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset;

import org.slf4j.LoggerFactory;
import ucar.nc2.util.NamedAnything;
import java.util.ArrayList;
import ucar.nc2.util.NamedObject;
import java.util.List;
import ucar.ma2.Index;
import ucar.nc2.Attribute;
import ucar.ma2.ArrayChar;
import ucar.ma2.IndexIterator;
import java.io.IOException;
import ucar.ma2.MAMath;
import ucar.ma2.Array;
import ucar.nc2.constants.AxisType;
import ucar.nc2.util.Misc;
import ucar.unidata.util.Format;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Section;
import ucar.ma2.Range;
import ucar.ma2.DataType;
import ucar.nc2.Group;
import ucar.nc2.Variable;
import org.slf4j.Logger;

public class CoordinateAxis1D extends CoordinateAxis
{
    private static Logger log;
    private boolean wasRead;
    private boolean wasCalcRegular;
    private boolean wasBoundsDone;
    private boolean isInterval;
    private boolean isAscending;
    private double[] midpoint;
    private String[] names;
    private double[] edge;
    private double[] bound1;
    private double[] bound2;
    private boolean isRegular;
    private double start;
    private double increment;
    
    public CoordinateAxis1D(final NetcdfDataset ncd, final VariableDS vds) {
        super(ncd, vds);
        this.wasRead = false;
        this.wasCalcRegular = false;
        this.wasBoundsDone = false;
        this.isInterval = false;
        this.names = null;
        this.isRegular = false;
    }
    
    CoordinateAxis1D(final NetcdfDataset ncd, final CoordinateAxis1D org) {
        super(ncd, org);
        this.wasRead = false;
        this.wasCalcRegular = false;
        this.wasBoundsDone = false;
        this.isInterval = false;
        this.names = null;
        this.isRegular = false;
        this.orgName = org.orgName;
        this.cache = new Cache();
    }
    
    public CoordinateAxis1D(final NetcdfDataset ds, final Group group, final String shortName, final DataType dataType, final String dims, final String units, final String desc) {
        super(ds, group, shortName, dataType, dims, units, desc);
        this.wasRead = false;
        this.wasCalcRegular = false;
        this.wasBoundsDone = false;
        this.isInterval = false;
        this.names = null;
        this.isRegular = false;
    }
    
    public CoordinateAxis1D section(final Range r) throws InvalidRangeException {
        final Section section = new Section().appendRange(r);
        return (CoordinateAxis1D)this.section(section);
    }
    
    @Override
    protected Variable copy() {
        return new CoordinateAxis1D(this.ncd, this);
    }
    
    @Override
    public CoordinateAxis copyNoCache() {
        final CoordinateAxis1D axis = new CoordinateAxis1D(this.ncd, this.getParentGroup(), this.getShortName(), this.getDataType(), this.getDimensionsString(), this.getUnitsString(), this.getDescription());
        axis.axisType = this.axisType;
        axis.boundaryRef = this.boundaryRef;
        axis.isContiguous = this.isContiguous;
        axis.positive = this.positive;
        axis.cache = new Cache();
        return axis;
    }
    
    public String getCoordName(final int index) {
        if (!this.wasRead) {
            this.doRead();
        }
        if (this.isNumeric()) {
            return Format.d(this.getCoordValue(index), 5, 8);
        }
        return this.names[index];
    }
    
    public double getCoordValue(final int index) {
        if (!this.isNumeric()) {
            throw new UnsupportedOperationException("CoordinateAxis1D.getCoordValue() on non-numeric");
        }
        if (!this.wasRead) {
            this.doRead();
        }
        return this.midpoint[index];
    }
    
    @Override
    public double getMinValue() {
        if (!this.isNumeric()) {
            throw new UnsupportedOperationException("CoordinateAxis1D.getCoordValue() on non-numeric");
        }
        if (!this.wasRead) {
            this.doRead();
        }
        return Math.min(this.midpoint[0], this.midpoint[(int)this.getSize() - 1]);
    }
    
    @Override
    public double getMaxValue() {
        if (!this.isNumeric()) {
            throw new UnsupportedOperationException("CoordinateAxis1D.getCoordValue() on non-numeric");
        }
        if (!this.wasRead) {
            this.doRead();
        }
        return Math.max(this.midpoint[0], this.midpoint[(int)this.getSize() - 1]);
    }
    
    public double getCoordEdge(final int index) {
        if (!this.isNumeric()) {
            throw new UnsupportedOperationException("CoordinateAxis1D.getCoordEdge() on non-numeric");
        }
        if (!this.wasBoundsDone) {
            this.makeBounds();
        }
        return this.edge[index];
    }
    
    public double[] getCoordValues() {
        if (!this.isNumeric()) {
            throw new UnsupportedOperationException("CoordinateAxis1D.getCoordValues() on non-numeric");
        }
        if (!this.wasRead) {
            this.doRead();
        }
        return this.midpoint.clone();
    }
    
    public double[] getCoordEdges() {
        if (!this.isNumeric()) {
            throw new UnsupportedOperationException("CoordinateAxis1D.getCoordEdges() on non-numeric");
        }
        if (!this.wasBoundsDone) {
            this.makeBounds();
        }
        return this.edge.clone();
    }
    
    @Override
    public boolean isContiguous() {
        if (!this.wasBoundsDone) {
            this.makeBounds();
        }
        return this.isContiguous;
    }
    
    public boolean isInterval() {
        if (!this.wasBoundsDone) {
            this.makeBounds();
        }
        return this.isInterval;
    }
    
    public double[] getBound1() {
        if (!this.isNumeric()) {
            throw new UnsupportedOperationException("CoordinateAxis1D.getBound1() on non-numeric");
        }
        if (!this.wasBoundsDone) {
            this.makeBounds();
        }
        if (this.bound1 == null) {
            this.makeBoundsFromEdges();
        }
        return this.bound1.clone();
    }
    
    public double[] getBound2() {
        if (!this.isNumeric()) {
            throw new UnsupportedOperationException("CoordinateAxis1D.getBound2() on non-numeric");
        }
        if (!this.wasBoundsDone) {
            this.makeBounds();
        }
        if (this.bound2 == null) {
            this.makeBoundsFromEdges();
        }
        return this.bound2.clone();
    }
    
    public double[] getCoordEdges(final int i) {
        if (!this.wasBoundsDone) {
            this.makeBounds();
        }
        if (!this.isContiguous()) {
            this.makeBoundsFromEdges();
        }
        final double[] e = new double[2];
        if (this.isContiguous()) {
            e[0] = this.getCoordEdge(i);
            e[1] = this.getCoordEdge(i + 1);
        }
        else {
            e[0] = this.bound1[i];
            e[1] = this.bound2[i];
        }
        return e;
    }
    
    public int findCoordElement(final double coordVal) {
        if (!this.isNumeric()) {
            throw new UnsupportedOperationException("CoordinateAxis.findCoordElement() on non-numeric");
        }
        if (this.isRegular()) {
            return this.findCoordElementRegular(coordVal, false);
        }
        if (this.isContiguous()) {
            return this.findCoordElementIrregular(coordVal, false);
        }
        return this.findCoordElementNonContiguous(coordVal, false);
    }
    
    public int findCoordElementBounded(final double coordVal) {
        if (!this.isNumeric()) {
            throw new UnsupportedOperationException("CoordinateAxis.findCoordElementBounded() on non-numeric");
        }
        if (this.isRegular()) {
            return this.findCoordElementRegular(coordVal, true);
        }
        if (this.isContiguous()) {
            return this.findCoordElementIrregular(coordVal, true);
        }
        return this.findCoordElementNonContiguous(coordVal, true);
    }
    
    @Deprecated
    public int findCoordElement(final double coordVal, final int lastIndex) {
        return this.findCoordElement(coordVal);
    }
    
    private int findCoordElementRegular(final double coordValue, final boolean bounded) {
        final int n = (int)this.getSize();
        final double distance = coordValue - this.start;
        final double exactNumSteps = distance / this.increment;
        final int index = (int)Math.round(exactNumSteps);
        if (index < 0) {
            return bounded ? 0 : -1;
        }
        if (index >= n) {
            return bounded ? (n - 1) : -1;
        }
        return index;
    }
    
    private boolean betweenLon(double lon, final double lonBeg, final double lonEnd) {
        while (lon < lonBeg) {
            lon += 360.0;
        }
        return lon >= lonBeg && lon <= lonEnd;
    }
    
    private int findCoordElementIrregular(final double target, final boolean bounded) {
        final int n = (int)this.getSize();
        int low = 0;
        int high = n;
        if (this.isAscending) {
            if (target < this.edge[low]) {
                return bounded ? 0 : -1;
            }
            if (target > this.edge[high]) {
                return bounded ? (n - 1) : -1;
            }
            int mid = low;
            while (high > low + 1) {
                mid = (low + high) / 2;
                final double midVal = this.edge[mid];
                if (midVal == target) {
                    return mid;
                }
                if (midVal < target) {
                    low = mid;
                }
                else {
                    high = mid;
                }
            }
            return low;
        }
        else {
            if (target > this.edge[low]) {
                return bounded ? 0 : -1;
            }
            if (target < this.edge[high]) {
                return bounded ? (n - 1) : -1;
            }
            int mid = low;
            while (high > low + 1) {
                mid = (low + high) / 2;
                final double midVal = this.edge[mid];
                if (midVal == target) {
                    return mid;
                }
                if (midVal < target) {
                    high = mid;
                }
                else {
                    low = mid;
                }
            }
            return high - 1;
        }
    }
    
    private int findCoordElementNonContiguous(final double target, final boolean bounded) {
        final double[] bounds1 = this.getBound1();
        final double[] bounds2 = this.getBound2();
        final int n = bounds1.length;
        if (this.isAscending) {
            if (target < bounds1[0]) {
                return bounded ? 0 : -1;
            }
            if (target > bounds2[n - 1]) {
                return bounded ? (n - 1) : -1;
            }
            int i = 0;
            while (i < n) {
                if (this.bound1[i] <= target && target <= this.bound2[i]) {
                    return i;
                }
                if (this.bound1[i] > target) {
                    if (!bounded) {
                        return -1;
                    }
                    final double d1 = this.bound1[i] - target;
                    final double d2 = target - this.bound1[i - 1];
                    return (d1 > d2) ? (i - 1) : i;
                }
                else {
                    ++i;
                }
            }
            return bounded ? (n - 1) : -1;
        }
        else {
            if (target > bounds1[0]) {
                return bounded ? 0 : -1;
            }
            if (target < bounds2[n - 1]) {
                return bounded ? (n - 1) : -1;
            }
            int i = 0;
            while (i < n) {
                if (this.bound2[i] <= target && target <= this.bound1[i]) {
                    return i;
                }
                if (this.bound2[i] < target) {
                    if (!bounded) {
                        return -1;
                    }
                    final double d1 = this.bound2[i] - target;
                    final double d2 = target - this.bound2[i - 1];
                    return (d1 > d2) ? (i - 1) : i;
                }
                else {
                    ++i;
                }
            }
            return bounded ? (n - 1) : -1;
        }
    }
    
    public double getStart() {
        this.calcIsRegular();
        return this.start;
    }
    
    public double getIncrement() {
        this.calcIsRegular();
        return this.increment;
    }
    
    public boolean isRegular() {
        this.calcIsRegular();
        return this.isRegular;
    }
    
    private void calcIsRegular() {
        if (this.wasCalcRegular) {
            return;
        }
        if (!this.wasRead) {
            this.doRead();
        }
        if (!this.isNumeric()) {
            this.isRegular = false;
        }
        else if (this.getSize() < 2L) {
            this.isRegular = true;
        }
        else {
            this.start = this.getCoordValue(0);
            final int n = (int)this.getSize();
            this.increment = (this.getCoordValue(n - 1) - this.getCoordValue(0)) / (n - 1);
            this.isRegular = true;
            for (int i = 1; i < this.getSize(); ++i) {
                if (!Misc.closeEnough(this.getCoordValue(i) - this.getCoordValue(i - 1), this.increment, 0.005)) {
                    this.isRegular = false;
                    break;
                }
            }
        }
        this.wasCalcRegular = true;
    }
    
    private void doRead() {
        if (this.isNumeric()) {
            this.readValues();
            this.wasRead = true;
            if (this.getSize() < 2L) {
                this.isAscending = true;
            }
            else {
                this.isAscending = (this.getCoordValue(0) < this.getCoordValue(1));
            }
            if (this.axisType == AxisType.Lon) {
                boolean monotonic = true;
                for (int i = 0; i < this.midpoint.length - 1; ++i) {
                    monotonic &= (this.isAscending ? (this.midpoint[i] < this.midpoint[i + 1]) : (this.midpoint[i] > this.midpoint[i + 1]));
                }
                if (!monotonic) {
                    boolean cross = false;
                    if (this.isAscending) {
                        for (int j = 0; j < this.midpoint.length; ++j) {
                            if (cross) {
                                final double[] midpoint = this.midpoint;
                                final int n = j;
                                midpoint[n] += 360.0;
                            }
                            if (!cross && j < this.midpoint.length - 1 && this.midpoint[j] > this.midpoint[j + 1]) {
                                cross = true;
                            }
                        }
                    }
                    else {
                        for (int j = 0; j < this.midpoint.length; ++j) {
                            if (cross) {
                                final double[] midpoint2 = this.midpoint;
                                final int n2 = j;
                                midpoint2[n2] -= 360.0;
                            }
                            if (!cross && j < this.midpoint.length - 1 && this.midpoint[j] < this.midpoint[j + 1]) {
                                cross = true;
                            }
                        }
                    }
                    Array cachedData = Array.factory(DataType.DOUBLE, this.getShape(), this.midpoint);
                    if (this.getDataType() != DataType.DOUBLE) {
                        cachedData = MAMath.convert(cachedData, this.getDataType());
                    }
                    this.setCachedData(cachedData);
                }
            }
        }
        else if (this.getDataType() == DataType.STRING) {
            this.readStringValues();
            this.wasRead = true;
        }
        else {
            this.readCharValues();
            this.wasRead = true;
        }
    }
    
    private void readStringValues() {
        int count = 0;
        Array data;
        try {
            data = this.read();
        }
        catch (IOException ioe) {
            CoordinateAxis1D.log.error("Error reading string coordinate values ", ioe);
            throw new IllegalStateException(ioe);
        }
        this.names = new String[(int)data.getSize()];
        final IndexIterator ii = data.getIndexIterator();
        while (ii.hasNext()) {
            this.names[count++] = (String)ii.getObjectNext();
        }
    }
    
    private void readCharValues() {
        int count = 0;
        ArrayChar data;
        try {
            data = (ArrayChar)this.read();
        }
        catch (IOException ioe) {
            CoordinateAxis1D.log.error("Error reading char coordinate values ", ioe);
            throw new IllegalStateException(ioe);
        }
        final ArrayChar.StringIterator iter = data.getStringIterator();
        this.names = new String[iter.getNumElems()];
        while (iter.hasNext()) {
            this.names[count++] = iter.next();
        }
    }
    
    private void readValues() {
        this.midpoint = new double[(int)this.getSize()];
        int count = 0;
        Array data;
        try {
            this.setUseNaNs(false);
            data = this.read();
        }
        catch (IOException ioe) {
            CoordinateAxis1D.log.error("Error reading coordinate values ", ioe);
            throw new IllegalStateException(ioe);
        }
        final IndexIterator iter = data.getIndexIterator();
        while (iter.hasNext()) {
            this.midpoint[count++] = iter.getDoubleNext();
        }
    }
    
    private void makeBounds() {
        if (!this.wasRead) {
            this.doRead();
        }
        if (this.isNumeric() && !this.makeBoundsFromAux()) {
            this.makeEdges();
        }
        this.wasBoundsDone = true;
    }
    
    private boolean makeBoundsFromAux() {
        final Attribute boundsAtt = this.findAttributeIgnoreCase("bounds");
        if (null == boundsAtt || !boundsAtt.isString()) {
            return false;
        }
        final String boundsVarName = boundsAtt.getStringValue();
        final VariableDS boundsVar = (VariableDS)this.ncd.findVariable(boundsVarName);
        if (null == boundsVar) {
            return false;
        }
        if (2 != boundsVar.getRank()) {
            return false;
        }
        if (this.getDimension(0) != boundsVar.getDimension(0)) {
            return false;
        }
        if (2 != boundsVar.getDimension(1).getLength()) {
            return false;
        }
        Array data;
        try {
            boundsVar.setUseNaNs(false);
            data = boundsVar.read();
        }
        catch (IOException e) {
            CoordinateAxis1D.log.warn("CoordinateAxis1D.hasBounds read failed ", e);
            return false;
        }
        assert data.getRank() == 2 && data.getShape()[1] == 2 : "incorrect shape data for variable " + boundsVar;
        final int n = this.shape[0];
        double[] value1 = new double[n];
        double[] value2 = new double[n];
        final Index ima = data.getIndex();
        for (int i = 0; i < n; ++i) {
            ima.set0(i);
            value1[i] = data.getDouble(ima.set1(0));
            value2[i] = data.getDouble(ima.set1(1));
        }
        final boolean goesUp = n < 2 || value1[n - 1] > value1[0];
        boolean firstLower = true;
        for (int j = 0; j < value1.length; ++j) {
            if (value1[j] != value2[j]) {
                firstLower = (value1[j] < value2[j]);
                break;
            }
        }
        if (goesUp != firstLower) {
            final double[] temp = value1;
            value1 = value2;
            value2 = temp;
        }
        boolean contig = true;
        for (int k = 0; k < n - 1; ++k) {
            if (!Misc.closeEnough(value1[k + 1], value2[k])) {
                contig = false;
            }
        }
        if (contig) {
            (this.edge = new double[n + 1])[0] = value1[0];
            for (int k = 1; k < n + 1; ++k) {
                this.edge[k] = value2[k - 1];
            }
        }
        else {
            (this.edge = new double[n + 1])[0] = value1[0];
            for (int k = 1; k < n; ++k) {
                this.edge[k] = (value1[k] + value2[k - 1]) / 2.0;
            }
            this.edge[n] = value2[n - 1];
            this.isContiguous = false;
        }
        this.bound1 = value1;
        this.bound2 = value2;
        return this.isInterval = true;
    }
    
    private void makeEdges() {
        final int size = (int)this.getSize();
        this.edge = new double[size + 1];
        if (size < 1) {
            return;
        }
        for (int i = 1; i < size; ++i) {
            this.edge[i] = (this.midpoint[i - 1] + this.midpoint[i]) / 2.0;
        }
        this.edge[0] = this.midpoint[0] - (this.edge[1] - this.midpoint[0]);
        this.edge[size] = this.midpoint[size - 1] + (this.midpoint[size - 1] - this.edge[size - 1]);
    }
    
    private void makeMidpoints() {
        final int size = (int)this.getSize();
        this.midpoint = new double[size];
        for (int i = 0; i < size; ++i) {
            this.midpoint[i] = (this.edge[i] + this.edge[i + 1]) / 2.0;
        }
    }
    
    private void makeBoundsFromEdges() {
        final int size = (int)this.getSize();
        if (size == 0) {
            return;
        }
        this.bound1 = new double[size];
        this.bound2 = new double[size];
        for (int i = 0; i < size; ++i) {
            this.bound1[i] = this.edge[i];
            this.bound2[i] = this.edge[i + 1];
        }
        if (this.bound1[0] > this.bound2[0]) {
            final double[] temp = this.bound1;
            this.bound1 = this.bound2;
            this.bound2 = temp;
        }
    }
    
    public List<NamedObject> getNames() {
        final int n = (int)this.getSize();
        final List<NamedObject> names = new ArrayList<NamedObject>(n);
        for (int i = 0; i < n; ++i) {
            names.add(new NamedAnything(this.getCoordName(i), this.getUnitsString()));
        }
        return names;
    }
    
    static {
        CoordinateAxis1D.log = LoggerFactory.getLogger(CoordinateAxis1D.class);
    }
}
