// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset;

import ucar.nc2.units.SimpleUnit;
import ucar.nc2.units.TimeUnit;
import ucar.nc2.units.DateUnit;
import java.util.StringTokenizer;
import ucar.nc2.Variable;
import ucar.unidata.util.Parameter;
import org.jdom.Content;
import org.jdom.Element;
import org.jdom.Document;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Formatter;
import ucar.nc2.dt.grid.GridCoordSys;
import org.jdom.output.XMLOutputter;
import org.jdom.output.Format;
import java.io.IOException;
import ucar.nc2.util.CancelTask;

public class NetcdfDatasetInfo
{
    private NetcdfDataset ds;
    private CoordSysBuilderIF builder;
    
    public NetcdfDatasetInfo(final String location) throws IOException {
        this.ds = NetcdfDataset.openDataset(location, false, null);
        this.builder = this.ds.enhance();
    }
    
    public void close() throws IOException {
        if (this.ds != null) {
            this.ds.close();
        }
    }
    
    public String getParseInfo() {
        return (this.builder == null) ? "" : this.builder.getParseInfo();
    }
    
    public String getUserAdvice() {
        return (this.builder == null) ? "" : this.builder.getUserAdvice();
    }
    
    public String getConventionUsed() {
        return (this.builder == null) ? "None" : this.builder.getConventionUsed();
    }
    
    public String writeXML() {
        final XMLOutputter fmt = new XMLOutputter(Format.getPrettyFormat());
        return fmt.outputString(this.makeDocument());
    }
    
    public GridCoordSys getGridCoordSys(final VariableEnhanced ve) {
        final List<CoordinateSystem> csList = ve.getCoordinateSystems();
        for (final CoordinateSystem cs : csList) {
            if (GridCoordSys.isGridCoordSys(null, cs, ve)) {
                return new GridCoordSys(cs, null);
            }
        }
        return null;
    }
    
    public void writeXML(final OutputStream os) throws IOException {
        final XMLOutputter fmt = new XMLOutputter(Format.getPrettyFormat());
        fmt.output(this.makeDocument(), os);
    }
    
    public Document makeDocument() {
        final Element rootElem = new Element("netcdfDatasetInfo");
        final Document doc = new Document(rootElem);
        rootElem.setAttribute("location", this.ds.getLocation());
        rootElem.addContent(new Element("convention").setAttribute("name", this.getConventionUsed()));
        int nDataVariables = 0;
        int nOtherVariables = 0;
        final List<CoordinateAxis> axes = this.ds.getCoordinateAxes();
        final int nCoordAxes = axes.size();
        for (final CoordinateAxis axis : axes) {
            final Element axisElem = new Element("axis");
            rootElem.addContent(axisElem);
            axisElem.setAttribute("name", axis.getName());
            axisElem.setAttribute("decl", this.getDecl(axis));
            if (axis.getAxisType() != null) {
                axisElem.setAttribute("type", axis.getAxisType().toString());
            }
            if (axis.getUnitsString() != null) {
                axisElem.setAttribute("units", axis.getUnitsString());
                axisElem.setAttribute("udunits", this.isUdunits(axis.getUnitsString()));
            }
            if (axis instanceof CoordinateAxis1D) {
                final CoordinateAxis1D axis1D = (CoordinateAxis1D)axis;
                if (!axis1D.isRegular()) {
                    continue;
                }
                axisElem.setAttribute("regular", ucar.unidata.util.Format.d(axis1D.getIncrement(), 5));
            }
        }
        final List<CoordinateSystem> csList = this.ds.getCoordinateSystems();
        for (final CoordinateSystem cs : csList) {
            Element csElem;
            if (GridCoordSys.isGridCoordSys(null, cs, null)) {
                final GridCoordSys gcs = new GridCoordSys(cs, null);
                csElem = new Element("gridCoordSystem");
                csElem.setAttribute("name", cs.getName());
                csElem.setAttribute("horizX", gcs.getXHorizAxis().getName());
                csElem.setAttribute("horizY", gcs.getYHorizAxis().getName());
                if (gcs.hasVerticalAxis()) {
                    csElem.setAttribute("vertical", gcs.getVerticalAxis().getName());
                }
                if (gcs.hasTimeAxis()) {
                    csElem.setAttribute("time", cs.getTaxis().getName());
                }
            }
            else {
                csElem = new Element("coordSystem");
                csElem.setAttribute("name", cs.getName());
            }
            final List<CoordinateTransform> coordTransforms = cs.getCoordinateTransforms();
            for (final CoordinateTransform ct : coordTransforms) {
                final Element ctElem = new Element("coordTransform");
                csElem.addContent(ctElem);
                ctElem.setAttribute("name", ct.getName());
                ctElem.setAttribute("type", ct.getTransformType().toString());
            }
            rootElem.addContent(csElem);
        }
        final List<CoordinateTransform> coordTransforms2 = this.ds.getCoordinateTransforms();
        for (final CoordinateTransform ct2 : coordTransforms2) {
            final Element ctElem2 = new Element("coordTransform");
            rootElem.addContent(ctElem2);
            ctElem2.setAttribute("name", ct2.getName());
            ctElem2.setAttribute("type", ct2.getTransformType().toString());
            final List<Parameter> params = ct2.getParameters();
            for (final Parameter pp : params) {
                final Element ppElem = new Element("param");
                ctElem2.addContent(ppElem);
                ppElem.setAttribute("name", pp.getName());
                ppElem.setAttribute("value", pp.getStringValue());
            }
        }
        for (final Variable var : this.ds.getVariables()) {
            final VariableEnhanced ve = (VariableEnhanced)var;
            if (ve instanceof CoordinateAxis) {
                continue;
            }
            final GridCoordSys gcs2 = this.getGridCoordSys(ve);
            if (null == gcs2) {
                continue;
            }
            ++nDataVariables;
            final Element gridElem = new Element("grid");
            rootElem.addContent(gridElem);
            gridElem.setAttribute("name", ve.getName());
            gridElem.setAttribute("decl", this.getDecl(ve));
            if (ve.getUnitsString() != null) {
                gridElem.setAttribute("units", ve.getUnitsString());
                gridElem.setAttribute("udunits", this.isUdunits(ve.getUnitsString()));
            }
            gridElem.setAttribute("coordSys", gcs2.getName());
        }
        for (final Variable var : this.ds.getVariables()) {
            final VariableEnhanced ve = (VariableEnhanced)var;
            if (ve instanceof CoordinateAxis) {
                continue;
            }
            final GridCoordSys gcs2 = this.getGridCoordSys(ve);
            if (null != gcs2) {
                continue;
            }
            ++nOtherVariables;
            final Element elem = new Element("variable");
            rootElem.addContent(elem);
            elem.setAttribute("name", ve.getName());
            elem.setAttribute("decl", this.getDecl(ve));
            if (ve.getUnitsString() != null) {
                elem.setAttribute("units", ve.getUnitsString());
                elem.setAttribute("udunits", this.isUdunits(ve.getUnitsString()));
            }
            elem.setAttribute("coordSys", this.getCoordSys(ve));
        }
        if (nDataVariables > 0) {
            rootElem.addContent(new Element("userAdvice").addContent("Dataset contains useable gridded data."));
            if (nOtherVariables > 0) {
                rootElem.addContent(new Element("userAdvice").addContent("Some variables are not gridded fields; check that is what you expect."));
            }
        }
        else if (nCoordAxes == 0) {
            rootElem.addContent(new Element("userAdvice").addContent("No Coordinate Axes were found."));
        }
        else {
            rootElem.addContent(new Element("userAdvice").addContent("No gridded data variables were found."));
        }
        final String userAdvice = this.getUserAdvice();
        if (userAdvice.length() > 0) {
            final StringTokenizer toker = new StringTokenizer(userAdvice, "\n");
            while (toker.hasMoreTokens()) {
                rootElem.addContent(new Element("userAdvice").addContent(toker.nextToken()));
            }
        }
        return doc;
    }
    
    private String getDecl(final VariableEnhanced ve) {
        final Formatter sb = new Formatter();
        sb.format("%s ", ve.getDataType().toString());
        ve.getNameAndDimensions(sb, true, true);
        return sb.toString();
    }
    
    private String getCoordSys(final VariableEnhanced ve) {
        final List csList = ve.getCoordinateSystems();
        if (csList.size() == 1) {
            final CoordinateSystem cs = csList.get(0);
            return cs.getName();
        }
        if (csList.size() > 1) {
            final StringBuilder sb = new StringBuilder();
            for (int i = 0; i < csList.size(); ++i) {
                final CoordinateSystem cs2 = csList.get(i);
                if (i > 0) {
                    sb.append(";");
                }
                sb.append(cs2.getName());
            }
            return sb.toString();
        }
        return " ";
    }
    
    private String isUdunits(final String unit) {
        try {
            new DateUnit(unit);
            return "date";
        }
        catch (Exception e) {
            try {
                new TimeUnit(unit);
                return "time";
            }
            catch (Exception e) {
                final SimpleUnit su = SimpleUnit.factory(unit);
                if (null == su) {
                    return "false";
                }
                return su.getCanonicalString();
            }
        }
    }
    
    public static void main(final String[] args) throws IOException {
        final String url = "C:/data/badmodels/RUC_CONUS_80km_20051211_1900.nc";
        final NetcdfDatasetInfo info = new NetcdfDatasetInfo(url);
        final String infoString = info.writeXML();
        System.out.println(infoString);
    }
}
