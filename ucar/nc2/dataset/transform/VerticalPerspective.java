// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.transform;

import ucar.unidata.geoloc.ProjectionImpl;
import ucar.nc2.dataset.ProjectionCT;
import ucar.unidata.geoloc.projection.VerticalPerspectiveView;
import ucar.unidata.geoloc.Earth;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dataset.TransformType;

public class VerticalPerspective extends AbstractCoordTransBuilder
{
    public String getTransformName() {
        return "vertical_perspective";
    }
    
    public TransformType getTransformType() {
        return TransformType.Projection;
    }
    
    public CoordinateTransform makeCoordinateTransform(final NetcdfDataset ds, final Variable ctv) {
        final double lon0 = this.readAttributeDouble(ctv, "longitude_of_projection_origin", Double.NaN);
        final double lat0 = this.readAttributeDouble(ctv, "latitude_of_projection_origin", Double.NaN);
        final double distance = this.readAttributeDouble(ctv, "height_above_earth", Double.NaN);
        if (Double.isNaN(lon0) || Double.isNaN(lat0) || Double.isNaN(distance)) {
            throw new IllegalArgumentException("Vertical Perspective must have longitude_of_projection_origin, latitude_of_projection_origin, height_above_earth attributes");
        }
        double false_easting = this.readAttributeDouble(ctv, "false_easting", 0.0);
        double false_northing = this.readAttributeDouble(ctv, "false_northing", 0.0);
        if (false_easting != 0.0 || false_northing != 0.0) {
            final double scalef = AbstractCoordTransBuilder.getFalseEastingScaleFactor(ds, ctv);
            false_easting *= scalef;
            false_northing *= scalef;
        }
        final double earth_radius = this.readAttributeDouble(ctv, "earth_radius", Earth.getRadius()) * 0.001;
        final VerticalPerspectiveView proj = new VerticalPerspectiveView(lat0, lon0, earth_radius, distance, false_easting, false_northing);
        return new ProjectionCT(ctv.getShortName(), "FGDC", proj);
    }
}
