// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.transform;

import ucar.nc2.dataset.CoordinateAxis;
import ucar.nc2.NetcdfFile;
import ucar.unidata.geoloc.vertical.WRFEta;
import ucar.unidata.geoloc.vertical.VerticalTransform;
import ucar.nc2.Dimension;
import ucar.nc2.dataset.TransformType;
import ucar.unidata.util.Parameter;
import ucar.nc2.dataset.CoordTransBuilderIF;
import ucar.nc2.dataset.VerticalCT;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dataset.CoordinateSystem;

public class WRFEtaTransformBuilder extends AbstractCoordTransBuilder
{
    private CoordinateSystem cs;
    
    public WRFEtaTransformBuilder() {
    }
    
    public WRFEtaTransformBuilder(final CoordinateSystem cs) {
        this.cs = cs;
    }
    
    public CoordinateTransform makeCoordinateTransform(final NetcdfDataset ds, final Variable v) {
        final VerticalCT.Type type = VerticalCT.Type.WRFEta;
        final VerticalCT ct = new VerticalCT(type.toString(), this.getTransformName(), type, this);
        ct.addParameter(new Parameter("height formula", "height(x,y,z) = (PH(x,y,z) + PHB(x,y,z)) / 9.81"));
        ct.addParameter(new Parameter("perturbation_geopotential", "PH"));
        ct.addParameter(new Parameter("base_geopotential", "PHB"));
        ct.addParameter(new Parameter("pressure formula", "pressure(x,y,z) = P(x,y,z) + PB(x,y,z)"));
        ct.addParameter(new Parameter("perturbation_presure", "P"));
        ct.addParameter(new Parameter("base_presure", "PB"));
        if (this.cs.getXaxis() != null) {
            ct.addParameter(new Parameter("staggered_x", "" + this.isStaggered(this.cs.getXaxis())));
        }
        else {
            ct.addParameter(new Parameter("staggered_x", "" + this.isStaggered2(this.cs.getLonAxis(), 1)));
        }
        if (this.cs.getYaxis() != null) {
            ct.addParameter(new Parameter("staggered_y", "" + this.isStaggered(this.cs.getYaxis())));
        }
        else {
            ct.addParameter(new Parameter("staggered_y", "" + this.isStaggered2(this.cs.getLonAxis(), 0)));
        }
        ct.addParameter(new Parameter("staggered_z", "" + this.isStaggered(this.cs.getZaxis())));
        ct.addParameter(new Parameter("eta", "" + this.cs.getZaxis().getName()));
        return ct;
    }
    
    public String getTransformName() {
        return "WRF_Eta";
    }
    
    public TransformType getTransformType() {
        return TransformType.Vertical;
    }
    
    @Override
    public VerticalTransform makeMathTransform(final NetcdfDataset ds, final Dimension timeDim, final VerticalCT vCT) {
        return new WRFEta(ds, timeDim, vCT.getParameters());
    }
    
    private boolean isStaggered(final CoordinateAxis axis) {
        if (axis == null) {
            return false;
        }
        final String name = axis.getName();
        return name != null && name.endsWith("stag");
    }
    
    private boolean isStaggered2(final CoordinateAxis axis, final int dimIndex) {
        if (axis == null) {
            return false;
        }
        final Dimension dim = axis.getDimension(dimIndex);
        return dim != null && dim.getName().endsWith("stag");
    }
}
