// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.transform;

import ucar.unidata.geoloc.ProjectionImpl;
import ucar.nc2.dataset.ProjectionCT;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dataset.TransformType;

public class MSGnavigation extends AbstractCoordTransBuilder
{
    public String getTransformName() {
        return "MSGnavigation";
    }
    
    public TransformType getTransformType() {
        return TransformType.Projection;
    }
    
    public CoordinateTransform makeCoordinateTransform(final NetcdfDataset ds, final Variable ctv) {
        final double lon0 = this.readAttributeDouble(ctv, "longitude_of_projection_origin", Double.NaN);
        final double lat0 = this.readAttributeDouble(ctv, "latitude_of_projection_origin", Double.NaN);
        final double minor_axis = this.readAttributeDouble(ctv, "semi_minor_axis", Double.NaN);
        final double major_axis = this.readAttributeDouble(ctv, "semi_major_axis", Double.NaN);
        final double height = this.readAttributeDouble(ctv, "height_from_earth_center", Double.NaN);
        final double scale_x = this.readAttributeDouble(ctv, "scale_x", Double.NaN);
        final double scale_y = this.readAttributeDouble(ctv, "scale_y", Double.NaN);
        final ProjectionImpl proj = new ucar.unidata.geoloc.projection.sat.MSGnavigation(lat0, lon0, minor_axis, major_axis, height, scale_x, scale_y);
        return new ProjectionCT(ctv.getShortName(), "FGDC", proj);
    }
}
