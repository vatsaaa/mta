// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.transform;

import ucar.unidata.geoloc.vertical.OceanSigma;
import ucar.unidata.geoloc.vertical.VerticalTransform;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFile;
import ucar.unidata.util.Parameter;
import ucar.nc2.dataset.CoordTransBuilderIF;
import ucar.nc2.dataset.VerticalCT;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dataset.TransformType;

public class VOceanSigma extends AbstractCoordTransBuilder
{
    private String sigma;
    private String eta;
    private String depth;
    
    public String getTransformName() {
        return "ocean_sigma_coordinate";
    }
    
    public TransformType getTransformType() {
        return TransformType.Vertical;
    }
    
    public CoordinateTransform makeCoordinateTransform(final NetcdfDataset ds, final Variable ctv) {
        final String formula_terms = this.getFormula(ds, ctv);
        if (null == formula_terms) {
            return null;
        }
        final String[] values = this.parseFormula(formula_terms, "sigma eta depth");
        if (values == null) {
            return null;
        }
        this.sigma = values[0];
        this.eta = values[1];
        this.depth = values[2];
        final CoordinateTransform rs = new VerticalCT("OceanSigma_Transform_" + ctv.getShortName(), this.getTransformName(), VerticalCT.Type.OceanSigma, this);
        rs.addParameter(new Parameter("standard_name", this.getTransformName()));
        rs.addParameter(new Parameter("formula_terms", formula_terms));
        rs.addParameter(new Parameter("height_formula", "height(x,y,z) = eta(n,j,i) + sigma(k)*(depth(j,i)+eta(n,j,i))"));
        if (!this.addParameter(rs, "Sigma_variableName", ds, this.sigma)) {
            return null;
        }
        if (!this.addParameter(rs, "Eta_variableName", ds, this.eta)) {
            return null;
        }
        if (!this.addParameter(rs, "Depth_variableName", ds, this.depth)) {
            return null;
        }
        return rs;
    }
    
    @Override
    public String toString() {
        return "OceanS: sigma:" + this.sigma + " eta:" + this.eta + " depth:" + this.depth;
    }
    
    @Override
    public VerticalTransform makeMathTransform(final NetcdfDataset ds, final Dimension timeDim, final VerticalCT vCT) {
        return new OceanSigma(ds, timeDim, vCT.getParameters());
    }
}
