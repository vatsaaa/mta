// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.transform;

import org.slf4j.LoggerFactory;
import java.util.Iterator;
import java.util.List;
import ucar.nc2.units.SimpleUnit;
import ucar.nc2.constants.AxisType;
import ucar.nc2.dataset.CoordinateAxis;
import ucar.unidata.util.Parameter;
import ucar.nc2.NetcdfFile;
import ucar.nc2.dataset.CoordinateTransform;
import java.util.StringTokenizer;
import ucar.nc2.Attribute;
import ucar.nc2.Variable;
import ucar.unidata.geoloc.vertical.VerticalTransform;
import ucar.nc2.dataset.VerticalCT;
import ucar.nc2.Dimension;
import ucar.nc2.dataset.NetcdfDataset;
import java.util.Formatter;
import org.slf4j.Logger;
import ucar.nc2.dataset.CoordTransBuilderIF;

public abstract class AbstractCoordTransBuilder implements CoordTransBuilderIF
{
    private static Logger log;
    protected Formatter errBuffer;
    
    public AbstractCoordTransBuilder() {
        this.errBuffer = null;
    }
    
    public void setErrorBuffer(final Formatter errBuffer) {
        this.errBuffer = errBuffer;
    }
    
    public VerticalTransform makeMathTransform(final NetcdfDataset ds, final Dimension timeDim, final VerticalCT vCT) {
        throw new UnsupportedOperationException();
    }
    
    protected double readAttributeDouble(final Variable v, final String attname, final double defValue) {
        final Attribute att = v.findAttributeIgnoreCase(attname);
        if (att == null) {
            return defValue;
        }
        if (att.isString()) {
            return Double.parseDouble(att.getStringValue());
        }
        return att.getNumericValue().doubleValue();
    }
    
    protected double[] readAttributeDouble2(final Attribute att) {
        if (att == null) {
            return null;
        }
        final double[] val = new double[2];
        if (att.isString()) {
            final StringTokenizer stoke = new StringTokenizer(att.getStringValue());
            val[0] = Double.parseDouble(stoke.nextToken());
            val[1] = (stoke.hasMoreTokens() ? Double.parseDouble(stoke.nextToken()) : val[0]);
        }
        else {
            val[0] = att.getNumericValue().doubleValue();
            val[1] = ((att.getLength() > 1) ? att.getNumericValue(1).doubleValue() : val[0]);
        }
        return val;
    }
    
    protected boolean addParameter(final CoordinateTransform rs, final String paramName, final NetcdfFile ds, final String varNameEscaped) {
        if (null == ds.findVariable(varNameEscaped)) {
            if (null != this.errBuffer) {
                this.errBuffer.format("CoordTransBuilder %s: no Variable named %s\n", this.getTransformName(), varNameEscaped);
            }
            return false;
        }
        rs.addParameter(new Parameter(paramName, varNameEscaped));
        return true;
    }
    
    protected String getFormula(final NetcdfDataset ds, final Variable ctv) {
        final String formula = ds.findAttValueIgnoreCase(ctv, "formula_terms", null);
        if (null == formula) {
            if (null != this.errBuffer) {
                this.errBuffer.format("CoordTransBuilder %s: needs attribute 'formula_terms' on Variable %s\n", this.getTransformName(), ctv.getName());
            }
            return null;
        }
        return formula;
    }
    
    public String[] parseFormula(final String formula_terms, final String termString) {
        final String[] formulaTerms = formula_terms.split("[\\s:]+");
        final String[] terms = termString.split("[\\s]+");
        final String[] values = new String[terms.length];
        for (int i = 0; i < terms.length; ++i) {
            for (int j = 0; j < formulaTerms.length; j += 2) {
                if (terms[i].equals(formulaTerms[j])) {
                    values[i] = formulaTerms[j + 1];
                    break;
                }
            }
        }
        boolean ok = true;
        for (int k = 0; k < values.length; ++k) {
            if (values[k] == null) {
                if (null != this.errBuffer) {
                    this.errBuffer.format("Missing term=%s in the formula '%s' for the vertical transform= %s%n", terms[k], formula_terms, this.getTransformName());
                }
                ok = false;
            }
        }
        return (String[])(ok ? values : null);
    }
    
    public static double getFalseEastingScaleFactor(final NetcdfDataset ds, final Variable ctv) {
        String units = ds.findAttValueIgnoreCase(ctv, "units", null);
        if (units == null) {
            final List<CoordinateAxis> axes = ds.getCoordinateAxes();
            for (final CoordinateAxis axis : axes) {
                if (axis.getAxisType() == AxisType.GeoX) {
                    final Variable v = axis.getOriginalVariable();
                    units = v.getUnitsString();
                    break;
                }
            }
        }
        if (units != null) {
            try {
                final SimpleUnit unit = SimpleUnit.factoryWithExceptions(units);
                return unit.convertTo(1.0, SimpleUnit.kmUnit);
            }
            catch (Exception e) {
                AbstractCoordTransBuilder.log.error(units + " not convertible to km");
            }
        }
        return 1.0;
    }
    
    static {
        AbstractCoordTransBuilder.log = LoggerFactory.getLogger(AbstractCoordTransBuilder.class);
    }
}
