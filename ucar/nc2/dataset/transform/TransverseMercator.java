// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.transform;

import ucar.unidata.geoloc.ProjectionImpl;
import ucar.nc2.dataset.ProjectionCT;
import ucar.unidata.geoloc.projection.proj4.TransverseMercatorProjection;
import ucar.unidata.geoloc.Earth;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dataset.TransformType;

public class TransverseMercator extends AbstractCoordTransBuilder
{
    public String getTransformName() {
        return "transverse_mercator";
    }
    
    public TransformType getTransformType() {
        return TransformType.Projection;
    }
    
    public CoordinateTransform makeCoordinateTransform(final NetcdfDataset ds, final Variable ctv) {
        final double scale = this.readAttributeDouble(ctv, "scale_factor_at_central_meridian", Double.NaN);
        final double lon0 = this.readAttributeDouble(ctv, "longitude_of_central_meridian", Double.NaN);
        final double lat0 = this.readAttributeDouble(ctv, "latitude_of_projection_origin", Double.NaN);
        double false_easting = this.readAttributeDouble(ctv, "false_easting", 0.0);
        double false_northing = this.readAttributeDouble(ctv, "false_northing", 0.0);
        if (false_easting != 0.0 || false_northing != 0.0) {
            final double scalef = AbstractCoordTransBuilder.getFalseEastingScaleFactor(ds, ctv);
            false_easting *= scalef;
            false_northing *= scalef;
        }
        final double earth_radius = this.readAttributeDouble(ctv, "earth_radius", Earth.getRadius()) * 0.001;
        final double semi_major_axis = this.readAttributeDouble(ctv, "semi_major_axis", Double.NaN) * 0.001;
        final double semi_minor_axis = this.readAttributeDouble(ctv, "semi_minor_axis", Double.NaN) * 0.001;
        final double inverse_flattening = this.readAttributeDouble(ctv, "inverse_flattening", 0.0);
        ProjectionImpl proj;
        if (!Double.isNaN(semi_major_axis) && (!Double.isNaN(semi_minor_axis) || inverse_flattening != 0.0)) {
            final Earth earth = new Earth(semi_major_axis, semi_minor_axis, inverse_flattening);
            proj = new TransverseMercatorProjection(earth, lon0, lat0, scale, false_easting, false_northing);
        }
        else {
            proj = new ucar.unidata.geoloc.projection.TransverseMercator(lat0, lon0, scale, false_easting, false_northing);
        }
        return new ProjectionCT(ctv.getShortName(), "FGDC", proj);
    }
}
