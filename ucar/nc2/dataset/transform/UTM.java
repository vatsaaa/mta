// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.transform;

import ucar.nc2.Attribute;
import ucar.unidata.geoloc.ProjectionImpl;
import ucar.nc2.dataset.ProjectionCT;
import ucar.unidata.geoloc.projection.UtmProjection;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dataset.TransformType;

public class UTM extends AbstractCoordTransBuilder
{
    public String getTransformName() {
        return "UTM";
    }
    
    public TransformType getTransformType() {
        return TransformType.Projection;
    }
    
    public CoordinateTransform makeCoordinateTransform(final NetcdfDataset ds, final Variable ctv) {
        int zone = (int)this.readAttributeDouble(ctv, "utm_zone_number", Double.NaN);
        final boolean isNorth = zone > 0;
        zone = Math.abs(zone);
        double axis = 0.0;
        double f = 0.0;
        Attribute a;
        if (null != (a = ctv.findAttribute("semimajor_axis"))) {
            axis = a.getNumericValue().doubleValue();
        }
        if (null != (a = ctv.findAttribute("inverse_flattening "))) {
            f = a.getNumericValue().doubleValue();
        }
        final UtmProjection proj = (axis != 0.0) ? new UtmProjection(axis, f, zone, isNorth) : new UtmProjection(zone, isNorth);
        return new ProjectionCT(ctv.getShortName(), "FGDC", proj);
    }
}
