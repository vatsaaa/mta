// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.transform;

import ucar.unidata.geoloc.ProjectionImpl;
import ucar.nc2.dataset.ProjectionCT;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dataset.TransformType;

public class Mercator extends AbstractCoordTransBuilder
{
    public String getTransformName() {
        return "mercator";
    }
    
    public TransformType getTransformType() {
        return TransformType.Projection;
    }
    
    public CoordinateTransform makeCoordinateTransform(final NetcdfDataset ds, final Variable ctv) {
        final double par = this.readAttributeDouble(ctv, "standard_parallel", Double.NaN);
        final double lon0 = this.readAttributeDouble(ctv, "longitude_of_projection_origin", Double.NaN);
        final double lat0 = this.readAttributeDouble(ctv, "latitude_of_projection_origin", Double.NaN);
        double false_easting = this.readAttributeDouble(ctv, "false_easting", 0.0);
        double false_northing = this.readAttributeDouble(ctv, "false_northing", 0.0);
        if (false_easting != 0.0 || false_northing != 0.0) {
            final double scalef = AbstractCoordTransBuilder.getFalseEastingScaleFactor(ds, ctv);
            false_easting *= scalef;
            false_northing *= scalef;
        }
        final ucar.unidata.geoloc.projection.Mercator proj = new ucar.unidata.geoloc.projection.Mercator(lon0, par, false_easting, false_northing);
        return new ProjectionCT(ctv.getShortName(), "FGDC", proj);
    }
}
