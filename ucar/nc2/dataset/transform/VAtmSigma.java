// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.transform;

import ucar.unidata.geoloc.vertical.AtmosSigma;
import ucar.unidata.geoloc.vertical.VerticalTransform;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFile;
import ucar.unidata.util.Parameter;
import ucar.nc2.dataset.CoordTransBuilderIF;
import ucar.nc2.dataset.VerticalCT;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dataset.TransformType;

public class VAtmSigma extends AbstractCoordTransBuilder
{
    private String sigma;
    private String ps;
    private String ptop;
    
    public VAtmSigma() {
        this.sigma = "";
        this.ps = "";
        this.ptop = "";
    }
    
    public String getTransformName() {
        return "atmosphere_sigma_coordinate";
    }
    
    public TransformType getTransformType() {
        return TransformType.Vertical;
    }
    
    public CoordinateTransform makeCoordinateTransform(final NetcdfDataset ds, final Variable ctv) {
        final String formula_terms = this.getFormula(ds, ctv);
        if (null == formula_terms) {
            return null;
        }
        final String[] values = this.parseFormula(formula_terms, "sigma ps ptop");
        if (values == null) {
            return null;
        }
        this.sigma = values[0];
        this.ps = values[1];
        this.ptop = values[2];
        final CoordinateTransform rs = new VerticalCT("AtmSigma_Transform_" + ctv.getShortName(), this.getTransformName(), VerticalCT.Type.Sigma, this);
        rs.addParameter(new Parameter("standard_name", this.getTransformName()));
        rs.addParameter(new Parameter("formula_terms", formula_terms));
        rs.addParameter(new Parameter("formula", "pressure(x,y,z) = ptop + sigma(z)*(surfacePressure(x,y)-ptop)"));
        if (!this.addParameter(rs, "SurfacePressure_variableName", ds, this.ps)) {
            return null;
        }
        if (!this.addParameter(rs, "Sigma_variableName", ds, this.sigma)) {
            return null;
        }
        if (!this.addParameter(rs, "PressureTop_variableName", ds, this.ptop)) {
            return null;
        }
        return rs;
    }
    
    @Override
    public String toString() {
        return "Sigma:sigma:" + this.sigma + " ps:" + this.ps + " ptop:" + this.ptop;
    }
    
    @Override
    public VerticalTransform makeMathTransform(final NetcdfDataset ds, final Dimension timeDim, final VerticalCT vCT) {
        return new AtmosSigma(ds, timeDim, vCT.getParameters());
    }
}
