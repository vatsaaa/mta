// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.transform;

import java.util.List;
import ucar.unidata.geoloc.vertical.OceanSG2;
import ucar.unidata.geoloc.vertical.VerticalTransform;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFile;
import ucar.unidata.util.Parameter;
import ucar.nc2.dataset.CoordTransBuilderIF;
import ucar.nc2.dataset.VerticalCT;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dataset.TransformType;

public class VOceanSG2 extends AbstractCoordTransBuilder
{
    private String s;
    private String eta;
    private String depth;
    private String c;
    private String depth_c;
    
    public VOceanSG2() {
        this.s = "";
        this.eta = "";
        this.depth = "";
        this.c = "";
        this.depth_c = "";
    }
    
    public String getTransformName() {
        return "ocean_s_coordinate_g2";
    }
    
    public TransformType getTransformType() {
        return TransformType.Vertical;
    }
    
    public CoordinateTransform makeCoordinateTransform(final NetcdfDataset ds, final Variable ctv) {
        final String formula_terms = this.getFormula(ds, ctv);
        if (null == formula_terms) {
            return null;
        }
        final String[] values = this.parseFormula(formula_terms, "s C eta depth depth_c");
        if (values == null) {
            return null;
        }
        this.s = values[0];
        this.c = values[1];
        this.eta = values[2];
        this.depth = values[3];
        this.depth_c = values[4];
        final CoordinateTransform rs = new VerticalCT("OceanSG2_Transform_" + ctv.getName(), this.getTransformName(), VerticalCT.Type.OceanSG2, this);
        rs.addParameter(new Parameter("standard_name", this.getTransformName()));
        rs.addParameter(new Parameter("formula_terms", formula_terms));
        rs.addParameter(new Parameter("height_formula", "height(x,y,z) = eta(x,y) + (eta(x,y) + depth([n],x,y)) * ((depth_c*s(z) + depth([n],x,y)*C(z))/(depth_c+depth([n],x,y)))"));
        if (!this.addParameter(rs, "Eta_variableName", ds, this.eta)) {
            return null;
        }
        if (!this.addParameter(rs, "S_variableName", ds, this.s)) {
            return null;
        }
        if (!this.addParameter(rs, "Depth_variableName", ds, this.depth)) {
            return null;
        }
        if (!this.addParameter(rs, "Depth_c_variableName", ds, this.depth_c)) {
            return null;
        }
        if (!this.addParameter(rs, "c_variableName", ds, this.c)) {
            return null;
        }
        return rs;
    }
    
    @Override
    public String toString() {
        return "OceanSG2: s:" + this.s + " c:" + this.c + " eta:" + this.eta + " depth:" + this.depth + " depth_c:" + this.depth_c;
    }
    
    @Override
    public VerticalTransform makeMathTransform(final NetcdfDataset ds, final Dimension timeDim, final VerticalCT vCT) {
        return (VerticalTransform)new OceanSG2((NetcdfFile)ds, timeDim, (List)vCT.getParameters());
    }
}
