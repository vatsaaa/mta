// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.transform;

import ucar.unidata.geoloc.ProjectionImpl;
import ucar.nc2.dataset.ProjectionCT;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dataset.TransformType;

public class RotatedLatLon extends AbstractCoordTransBuilder
{
    public String getTransformName() {
        return "rotated_latlon_grib";
    }
    
    public TransformType getTransformType() {
        return TransformType.Projection;
    }
    
    public CoordinateTransform makeCoordinateTransform(final NetcdfDataset ds, final Variable ctv) {
        final double lon = this.readAttributeDouble(ctv, "grid_south_pole_longitude", Double.NaN);
        final double lat = this.readAttributeDouble(ctv, "grid_south_pole_latitude", Double.NaN);
        final double angle = this.readAttributeDouble(ctv, "grid_south_pole_angle", Double.NaN);
        final ucar.unidata.geoloc.projection.RotatedLatLon proj = new ucar.unidata.geoloc.projection.RotatedLatLon(lat, lon, angle);
        return new ProjectionCT(ctv.getShortName(), "FGDC", proj);
    }
}
