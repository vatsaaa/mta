// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.transform;

import ucar.unidata.geoloc.vertical.HybridSigmaPressure;
import ucar.unidata.geoloc.vertical.VerticalTransform;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFile;
import ucar.unidata.util.Parameter;
import ucar.nc2.dataset.CoordTransBuilderIF;
import ucar.nc2.dataset.VerticalCT;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dataset.TransformType;

public class VAtmHybridSigmaPressure extends AbstractCoordTransBuilder
{
    private boolean useAp;
    private String a;
    private String b;
    private String ps;
    private String p0;
    private String ap;
    
    public String getTransformName() {
        return "atmosphere_hybrid_sigma_pressure_coordinate";
    }
    
    public TransformType getTransformType() {
        return TransformType.Vertical;
    }
    
    public CoordinateTransform makeCoordinateTransform(final NetcdfDataset ds, final Variable ctv) {
        final String formula_terms = this.getFormula(ds, ctv);
        if (null == formula_terms) {
            return null;
        }
        this.useAp = (formula_terms.indexOf("ap:") >= 0);
        final CoordinateTransform rs = new VerticalCT("AtmHybridSigmaPressure_Transform_" + ctv.getShortName(), this.getTransformName(), VerticalCT.Type.HybridSigmaPressure, this);
        rs.addParameter(new Parameter("standard_name", this.getTransformName()));
        rs.addParameter(new Parameter("formula_terms", formula_terms));
        if (this.useAp) {
            final String[] values = this.parseFormula(formula_terms, "ap b ps");
            if (values == null) {
                return null;
            }
            this.ap = values[0];
            this.b = values[1];
            this.ps = values[2];
            rs.addParameter(new Parameter("formula", "pressure(x,y,z) = ap(z) + b(z)*surfacePressure(x,y)"));
            if (!this.addParameter(rs, "SurfacePressure_variableName", ds, this.ps)) {
                return null;
            }
            if (!this.addParameter(rs, "AP_variableName", ds, this.ap)) {
                return null;
            }
            if (!this.addParameter(rs, "B_variableName", ds, this.b)) {
                return null;
            }
        }
        else {
            final String[] values = this.parseFormula(formula_terms, "a b ps p0");
            if (values == null) {
                return null;
            }
            this.a = values[0];
            this.b = values[1];
            this.ps = values[2];
            this.p0 = values[3];
            rs.addParameter(new Parameter("formula", "pressure(x,y,z) = a(z)*p0 + b(z)*surfacePressure(x,y)"));
            if (!this.addParameter(rs, "SurfacePressure_variableName", ds, this.ps)) {
                return null;
            }
            if (!this.addParameter(rs, "A_variableName", ds, this.a)) {
                return null;
            }
            if (!this.addParameter(rs, "B_variableName", ds, this.b)) {
                return null;
            }
            if (!this.addParameter(rs, "P0_variableName", ds, this.p0)) {
                return null;
            }
        }
        return rs;
    }
    
    @Override
    public String toString() {
        return "HybridSigmaPressure:" + (this.useAp ? ("ps:" + this.ps + " p0:" + this.p0 + " a:" + this.a + " b:" + this.b) : ("ps:" + this.ps + " ap:" + this.ap + " b:" + this.b));
    }
    
    @Override
    public VerticalTransform makeMathTransform(final NetcdfDataset ds, final Dimension timeDim, final VerticalCT vCT) {
        return new HybridSigmaPressure(ds, timeDim, vCT.getParameters());
    }
}
