// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.transform;

import ucar.unidata.geoloc.ProjectionImpl;
import ucar.nc2.dataset.ProjectionCT;
import ucar.unidata.geoloc.projection.Stereographic;
import ucar.unidata.geoloc.projection.proj4.StereographicAzimuthalProjection;
import ucar.unidata.geoloc.Earth;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dataset.TransformType;

public class PolarStereographic extends AbstractCoordTransBuilder
{
    public String getTransformName() {
        return "polar_stereographic";
    }
    
    public TransformType getTransformType() {
        return TransformType.Projection;
    }
    
    public CoordinateTransform makeCoordinateTransform(final NetcdfDataset ds, final Variable ctv) {
        double lon0 = this.readAttributeDouble(ctv, "straight_vertical_longitude_from_pole", Double.NaN);
        if (Double.isNaN(lon0)) {
            lon0 = this.readAttributeDouble(ctv, "longitude_of_projection_origin", Double.NaN);
        }
        if (Double.isNaN(lon0)) {
            throw new IllegalArgumentException("No longitude parameter");
        }
        final double lat0 = this.readAttributeDouble(ctv, "latitude_of_projection_origin", 90.0);
        double scale = this.readAttributeDouble(ctv, "scale_factor_at_projection_origin", Double.NaN);
        if (Double.isNaN(scale)) {
            final double stdpar = this.readAttributeDouble(ctv, "standard_parallel", Double.NaN);
            if (!Double.isNaN(stdpar)) {
                final double sin = Math.abs(Math.sin(Math.toRadians(stdpar)));
                scale = (1.0 + sin) / 2.0;
            }
            else {
                scale = 1.0;
            }
        }
        double false_easting = this.readAttributeDouble(ctv, "false_easting", 0.0);
        double false_northing = this.readAttributeDouble(ctv, "false_northing", 0.0);
        if (false_easting != 0.0 || false_northing != 0.0) {
            final double scalef = AbstractCoordTransBuilder.getFalseEastingScaleFactor(ds, ctv);
            false_easting *= scalef;
            false_northing *= scalef;
        }
        final double earth_radius = this.readAttributeDouble(ctv, "earth_radius", Earth.getRadius()) * 0.001;
        final double semi_major_axis = this.readAttributeDouble(ctv, "semi_major_axis", Double.NaN) * 0.001;
        final double semi_minor_axis = this.readAttributeDouble(ctv, "semi_minor_axis", Double.NaN) * 0.001;
        final double inverse_flattening = this.readAttributeDouble(ctv, "inverse_flattening", 0.0);
        ProjectionImpl proj;
        if (!Double.isNaN(semi_major_axis) && (!Double.isNaN(semi_minor_axis) || inverse_flattening != 0.0)) {
            final Earth earth = new Earth(semi_major_axis, semi_minor_axis, inverse_flattening);
            proj = new StereographicAzimuthalProjection(lat0, lon0, scale, 90.0, false_easting, false_northing, earth);
        }
        else {
            proj = new Stereographic(lat0, lon0, scale, false_easting, false_northing);
        }
        return new ProjectionCT(ctv.getShortName(), "FGDC", proj);
    }
    
    public static void main(final String[] arg) {
        final double stdpar = 70.0;
        final double sin = Math.abs(Math.sin(Math.toRadians(stdpar)));
        final double scale = (1.0 + sin) / 2.0;
        System.out.printf("stdpar = %f has scale = %f %n", stdpar, scale);
    }
}
