// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.transform;

import ucar.unidata.geoloc.ProjectionImpl;
import ucar.nc2.dataset.ProjectionCT;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dataset.TransformType;

public class FlatEarth extends AbstractCoordTransBuilder
{
    public String getTransformName() {
        return "flat_earth";
    }
    
    public TransformType getTransformType() {
        return TransformType.Projection;
    }
    
    public CoordinateTransform makeCoordinateTransform(final NetcdfDataset ds, final Variable ctv) {
        final double lon0 = this.readAttributeDouble(ctv, "longitude_of_projection_origin", Double.NaN);
        final double lat0 = this.readAttributeDouble(ctv, "latitude_of_projection_origin", Double.NaN);
        final ucar.unidata.geoloc.projection.FlatEarth proj = new ucar.unidata.geoloc.projection.FlatEarth(lat0, lon0);
        return new ProjectionCT(ctv.getShortName(), "FGDC", proj);
    }
}
