// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.transform;

import ucar.unidata.geoloc.vertical.HybridHeight;
import ucar.unidata.geoloc.vertical.VerticalTransform;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFile;
import ucar.unidata.util.Parameter;
import ucar.nc2.dataset.CoordTransBuilderIF;
import ucar.nc2.dataset.VerticalCT;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dataset.TransformType;

public class VAtmHybridHeight extends AbstractCoordTransBuilder
{
    private String a;
    private String b;
    private String orog;
    
    public String getTransformName() {
        return "atmosphere_hybrid_height_coordinate";
    }
    
    public TransformType getTransformType() {
        return TransformType.Vertical;
    }
    
    public CoordinateTransform makeCoordinateTransform(final NetcdfDataset ds, final Variable ctv) {
        final String formula_terms = this.getFormula(ds, ctv);
        if (null == formula_terms) {
            return null;
        }
        final String[] values = this.parseFormula(formula_terms, "a b orog");
        if (values == null) {
            return null;
        }
        this.a = values[0];
        this.b = values[1];
        this.orog = values[2];
        final CoordinateTransform rs = new VerticalCT("AtmHybridHeight_Transform_" + ctv.getShortName(), this.getTransformName(), VerticalCT.Type.HybridHeight, this);
        rs.addParameter(new Parameter("standard_name", this.getTransformName()));
        rs.addParameter(new Parameter("formula_terms", formula_terms));
        rs.addParameter(new Parameter("formula", "height(x,y,z) = a(z) + b(z)*orog(x,y)"));
        if (!this.addParameter(rs, "A_variableName", ds, this.a)) {
            return null;
        }
        if (!this.addParameter(rs, "B_variableName", ds, this.b)) {
            return null;
        }
        if (!this.addParameter(rs, "Orography_variableName", ds, this.orog)) {
            return null;
        }
        return rs;
    }
    
    @Override
    public String toString() {
        return "HybridHeight:orog:" + this.orog + " a:" + this.a + " b:" + this.b;
    }
    
    @Override
    public VerticalTransform makeMathTransform(final NetcdfDataset ds, final Dimension timeDim, final VerticalCT vCT) {
        return new HybridHeight(ds, timeDim, vCT.getParameters());
    }
}
