// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.transform;

import ucar.nc2.NetcdfFile;
import ucar.unidata.geoloc.vertical.VTfromExistingData;
import ucar.unidata.geoloc.vertical.VerticalTransform;
import ucar.nc2.Dimension;
import ucar.unidata.util.Parameter;
import ucar.nc2.dataset.CoordTransBuilderIF;
import ucar.nc2.dataset.VerticalCT;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dataset.TransformType;

public class VExplicitField extends AbstractCoordTransBuilder
{
    public String getTransformName() {
        return "explicit_field";
    }
    
    public TransformType getTransformType() {
        return TransformType.Vertical;
    }
    
    public CoordinateTransform makeCoordinateTransform(final NetcdfDataset ds, final Variable ctv) {
        final VerticalCT ct = new VerticalCT(ctv.getShortName(), this.getTransformName(), VerticalCT.Type.Existing3DField, this);
        final String fieldName = ds.findAttValueIgnoreCase(ctv, "existingDataField", null);
        if (null == fieldName) {
            throw new IllegalArgumentException("ExplicitField Vertical Transform must have attribute existingDataField");
        }
        ct.addParameter(new Parameter("standard_name", this.getTransformName()));
        ct.addParameter(new Parameter("existingDataField", fieldName));
        return ct;
    }
    
    @Override
    public VerticalTransform makeMathTransform(final NetcdfDataset ds, final Dimension timeDim, final VerticalCT vCT) {
        return new VTfromExistingData(ds, timeDim, vCT.getParameters());
    }
}
