// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.transform;

import ucar.unidata.geoloc.ProjectionImpl;
import ucar.nc2.dataset.ProjectionCT;
import ucar.unidata.geoloc.projection.proj4.AlbersEqualAreaEllipse;
import ucar.unidata.geoloc.Earth;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dataset.TransformType;

public class AlbersEqualArea extends AbstractCoordTransBuilder
{
    public String getTransformName() {
        return "albers_conical_equal_area";
    }
    
    public TransformType getTransformType() {
        return TransformType.Projection;
    }
    
    public CoordinateTransform makeCoordinateTransform(final NetcdfDataset ds, final Variable ctv) {
        final double[] pars = this.readAttributeDouble2(ctv.findAttribute("standard_parallel"));
        if (pars == null) {
            return null;
        }
        final double lon0 = this.readAttributeDouble(ctv, "longitude_of_central_meridian", Double.NaN);
        final double lat0 = this.readAttributeDouble(ctv, "latitude_of_projection_origin", Double.NaN);
        double false_easting = this.readAttributeDouble(ctv, "false_easting", 0.0);
        double false_northing = this.readAttributeDouble(ctv, "false_northing", 0.0);
        if (false_easting != 0.0 || false_northing != 0.0) {
            final double scalef = AbstractCoordTransBuilder.getFalseEastingScaleFactor(ds, ctv);
            false_easting *= scalef;
            false_northing *= scalef;
        }
        final double earth_radius = this.readAttributeDouble(ctv, "earth_radius", Earth.getRadius()) * 0.001;
        final double semi_major_axis = this.readAttributeDouble(ctv, "semi_major_axis", Double.NaN);
        final double semi_minor_axis = this.readAttributeDouble(ctv, "semi_minor_axis", Double.NaN);
        final double inverse_flattening = this.readAttributeDouble(ctv, "inverse_flattening", 0.0);
        ProjectionImpl proj;
        if (!Double.isNaN(semi_major_axis) && (!Double.isNaN(semi_minor_axis) || inverse_flattening != 0.0)) {
            final Earth earth = new Earth(semi_major_axis, semi_minor_axis, inverse_flattening);
            proj = new AlbersEqualAreaEllipse(lat0, lon0, pars[0], pars[1], false_easting, false_northing, earth);
        }
        else {
            proj = new ucar.unidata.geoloc.projection.AlbersEqualArea(lat0, lon0, pars[0], pars[1], false_easting, false_northing, earth_radius);
        }
        return new ProjectionCT(ctv.getShortName(), "FGDC", proj);
    }
}
