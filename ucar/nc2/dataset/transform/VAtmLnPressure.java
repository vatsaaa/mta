// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.transform;

import ucar.unidata.geoloc.vertical.AtmosLnPressure;
import ucar.unidata.geoloc.vertical.VerticalTransform;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFile;
import ucar.unidata.util.Parameter;
import ucar.nc2.dataset.CoordTransBuilderIF;
import ucar.nc2.dataset.VerticalCT;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dataset.TransformType;

public class VAtmLnPressure extends AbstractCoordTransBuilder
{
    private String p0;
    private String lev;
    
    public String getTransformName() {
        return "atmosphere_ln_pressure_coordinate";
    }
    
    public TransformType getTransformType() {
        return TransformType.Vertical;
    }
    
    public CoordinateTransform makeCoordinateTransform(final NetcdfDataset ds, final Variable ctv) {
        final String formula_terms = this.getFormula(ds, ctv);
        if (null == formula_terms) {
            return null;
        }
        final String[] values = this.parseFormula(formula_terms, "p0 lev");
        if (values == null) {
            return null;
        }
        this.p0 = values[0];
        this.lev = values[1];
        final CoordinateTransform rs = new VerticalCT("AtmSigma_Transform_" + ctv.getShortName(), this.getTransformName(), VerticalCT.Type.Sigma, this);
        rs.addParameter(new Parameter("standard_name", this.getTransformName()));
        rs.addParameter(new Parameter("formula_terms", formula_terms));
        rs.addParameter(new Parameter("formula", "pressure(z) = p0 * exp(-lev(k))"));
        if (!this.addParameter(rs, "ReferencePressureVariableName", ds, this.p0)) {
            return null;
        }
        if (!this.addParameter(rs, "VerticalCoordinateVariableName", ds, this.lev)) {
            return null;
        }
        return rs;
    }
    
    @Override
    public String toString() {
        return "AtmLnPressure:p0:" + this.p0 + " lev:" + this.lev;
    }
    
    @Override
    public VerticalTransform makeMathTransform(final NetcdfDataset ds, final Dimension timeDim, final VerticalCT vCT) {
        return new AtmosLnPressure(ds, timeDim, vCT.getParameters());
    }
}
