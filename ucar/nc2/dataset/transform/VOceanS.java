// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset.transform;

import ucar.unidata.geoloc.vertical.OceanS;
import ucar.unidata.geoloc.vertical.VerticalTransform;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFile;
import ucar.unidata.util.Parameter;
import ucar.nc2.dataset.CoordTransBuilderIF;
import ucar.nc2.dataset.VerticalCT;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dataset.TransformType;

public class VOceanS extends AbstractCoordTransBuilder
{
    private String s;
    private String eta;
    private String depth;
    private String a;
    private String b;
    private String depth_c;
    
    public VOceanS() {
        this.s = "";
        this.eta = "";
        this.depth = "";
        this.a = "";
        this.b = "";
        this.depth_c = "";
    }
    
    public String getTransformName() {
        return "ocean_s_coordinate";
    }
    
    public TransformType getTransformType() {
        return TransformType.Vertical;
    }
    
    public CoordinateTransform makeCoordinateTransform(final NetcdfDataset ds, final Variable ctv) {
        final String formula_terms = this.getFormula(ds, ctv);
        if (null == formula_terms) {
            return null;
        }
        final String[] values = this.parseFormula(formula_terms, "s eta depth a b depth_c");
        if (values == null) {
            return null;
        }
        this.s = values[0];
        this.eta = values[1];
        this.depth = values[2];
        this.a = values[3];
        this.b = values[4];
        this.depth_c = values[5];
        final CoordinateTransform rs = new VerticalCT("OceanS_Transform_" + ctv.getShortName(), this.getTransformName(), VerticalCT.Type.OceanS, this);
        rs.addParameter(new Parameter("standard_name", this.getTransformName()));
        rs.addParameter(new Parameter("formula_terms", formula_terms));
        rs.addParameter(new Parameter("height_formula", "height(x,y,z) = depth_c*s(z) + (depth(x,y)-depth_c)*C(z) + eta(x,y) * (1 + (depth_c*s(z) + (depth(x,y)-depth_c)*C(z))/depth(x,y) "));
        rs.addParameter(new Parameter("C_formula", "C(z) = (1-b)*sinh(a*s(z))/sinh(a) + b*(tanh(a*(s(z)+0.5))/(2*tanh(0.5*a))-0.5)"));
        if (!this.addParameter(rs, "Eta_variableName", ds, this.eta)) {
            return null;
        }
        if (!this.addParameter(rs, "S_variableName", ds, this.s)) {
            return null;
        }
        if (!this.addParameter(rs, "Depth_variableName", ds, this.depth)) {
            return null;
        }
        if (!this.addParameter(rs, "Depth_c_variableName", ds, this.depth_c)) {
            return null;
        }
        if (!this.addParameter(rs, "A_variableName", ds, this.a)) {
            return null;
        }
        if (!this.addParameter(rs, "B_variableName", ds, this.b)) {
            return null;
        }
        return rs;
    }
    
    @Override
    public String toString() {
        return "OceanS: s:" + this.s + " eta:" + this.eta + " depth:" + this.depth + " a:" + this.a + " b:" + this.b + " depth_c:" + this.depth_c;
    }
    
    @Override
    public VerticalTransform makeMathTransform(final NetcdfDataset ds, final Dimension timeDim, final VerticalCT vCT) {
        return new OceanS(ds, timeDim, vCT.getParameters());
    }
}
