// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset;

import java.util.List;
import java.util.ArrayList;
import ucar.nc2.Attribute;
import java.util.Iterator;
import ucar.nc2.Structure;
import ucar.nc2.Variable;
import ucar.nc2.Dimension;
import ucar.nc2.Group;
import ucar.nc2.NetcdfFile;

public class DatasetConstructor
{
    private static final String boundsDimName = "bounds_dim";
    
    public static void transferDataset(final NetcdfFile src, final NetcdfDataset target, final ReplaceVariableCheck replaceCheck) {
        transferGroup(src, target, src.getRootGroup(), target.getRootGroup(), replaceCheck);
    }
    
    private static void transferGroup(final NetcdfFile ds, final NetcdfDataset targetDs, final Group src, final Group targetGroup, final ReplaceVariableCheck replaceCheck) {
        final boolean unlimitedOK = true;
        transferGroupAttributes(src, targetGroup);
        for (final Dimension d : src.getDimensions()) {
            if (null == targetGroup.findDimensionLocal(d.getName())) {
                final Dimension newd = new Dimension(d.getName(), d.getLength(), d.isShared(), unlimitedOK && d.isUnlimited(), d.isVariableLength());
                targetGroup.addDimension(newd);
            }
        }
        for (Variable v : src.getVariables()) {
            final Variable targetV = targetGroup.findVariable(v.getShortName());
            final VariableEnhanced targetVe = (VariableEnhanced)targetV;
            final boolean replace = replaceCheck != null && replaceCheck.replace(v);
            if (replace || null == targetV) {
                if (v instanceof Structure && !(v instanceof StructureDS)) {
                    v = new StructureDS(targetGroup, (Structure)v);
                }
                else if (!(v instanceof VariableDS)) {
                    v = new VariableDS(targetGroup, v, false);
                }
                if (null != targetV) {
                    targetGroup.remove(targetV);
                }
                targetGroup.addVariable(v);
                v.resetDimensions();
            }
            else {
                if (targetV.hasCachedData() || targetVe.getOriginalVariable() != null) {
                    continue;
                }
                targetVe.setOriginalVariable(v);
            }
        }
        for (final Group srcNested : src.getGroups()) {
            Group nested = targetGroup.findGroup(srcNested.getShortName());
            if (null == nested) {
                nested = new Group(ds, targetGroup, srcNested.getShortName());
                targetGroup.addGroup(nested);
            }
            transferGroup(ds, targetDs, srcNested, nested, replaceCheck);
        }
    }
    
    public static void transferVariableAttributes(final Variable src, final Variable target) {
        for (final Attribute a : src.getAttributes()) {
            if (null == target.findAttribute(a.getName())) {
                target.addAttribute(a);
            }
        }
    }
    
    public static void transferGroupAttributes(final Group src, final Group target) {
        for (final Attribute a : src.getAttributes()) {
            if (null == target.findAttribute(a.getName())) {
                target.addAttribute(a);
            }
        }
    }
    
    public static Group findGroup(final NetcdfFile newFile, final Group oldGroup) {
        final List<Group> chain = new ArrayList<Group>(5);
        for (Group g = oldGroup; g.getParentGroup() != null; g = g.getParentGroup()) {
            chain.add(0, g);
        }
        Group newg = newFile.getRootGroup();
        for (final Group oldg : chain) {
            newg = newg.findGroup(oldg.getShortName());
            if (newg == null) {
                return null;
            }
        }
        return newg;
    }
    
    public static Dimension getBoundsDimension(final NetcdfFile ncfile) {
        final Group g = ncfile.getRootGroup();
        Dimension d = g.findDimension("bounds_dim");
        if (d == null) {
            d = ncfile.addDimension(g, new Dimension("bounds_dim", 2, true));
        }
        return d;
    }
}
