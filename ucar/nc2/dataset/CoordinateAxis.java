// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dataset;

import java.util.Comparator;
import org.slf4j.LoggerFactory;
import java.util.Formatter;
import ucar.ma2.Array;
import java.io.IOException;
import ucar.nc2.Variable;
import ucar.nc2.Structure;
import ucar.nc2.Group;
import ucar.ma2.DataType;
import ucar.ma2.MAMath;
import ucar.nc2.constants.AxisType;
import org.slf4j.Logger;

public class CoordinateAxis extends VariableDS
{
    private static Logger log;
    private static final int axisSizeToCache = 100000;
    protected NetcdfDataset ncd;
    protected AxisType axisType;
    protected String positive;
    protected String boundaryRef;
    protected boolean isContiguous;
    private MAMath.MinMax minmax;
    private int hashCode;
    
    public static CoordinateAxis factory(final NetcdfDataset ncd, final VariableDS vds) {
        if (vds.getRank() == 1 || (vds.getRank() == 2 && vds.getDataType() == DataType.CHAR)) {
            return new CoordinateAxis1D(ncd, vds);
        }
        if (vds.getRank() == 2) {
            return new CoordinateAxis2D(ncd, vds);
        }
        return new CoordinateAxis(ncd, vds);
    }
    
    protected CoordinateAxis(final NetcdfDataset ncd, final VariableDS vds) {
        super(vds, false);
        this.axisType = null;
        this.positive = null;
        this.boundaryRef = null;
        this.isContiguous = true;
        this.minmax = null;
        this.hashCode = 0;
        this.ncd = ncd;
        if (vds instanceof CoordinateAxis) {
            final CoordinateAxis axis = (CoordinateAxis)vds;
            this.axisType = axis.axisType;
            this.boundaryRef = axis.boundaryRef;
            this.isContiguous = axis.isContiguous;
            this.positive = axis.positive;
        }
        this.setSizeToCache(100000);
    }
    
    public CoordinateAxis(final NetcdfDataset ds, final Group group, final String shortName, final DataType dataType, final String dims, final String units, final String desc) {
        super(ds, group, null, shortName, dataType, dims, units, desc);
        this.axisType = null;
        this.positive = null;
        this.boundaryRef = null;
        this.isContiguous = true;
        this.minmax = null;
        this.hashCode = 0;
        this.ncd = ds;
        this.setSizeToCache(100000);
    }
    
    public CoordinateAxis copyNoCache() {
        final CoordinateAxis axis = new CoordinateAxis(this.ncd, this.getParentGroup(), this.getShortName(), this.getDataType(), this.getDimensionsString(), this.getUnitsString(), this.getDescription());
        axis.axisType = this.axisType;
        axis.boundaryRef = this.boundaryRef;
        axis.isContiguous = this.isContiguous;
        axis.positive = this.positive;
        axis.cache = new Cache();
        return axis;
    }
    
    @Override
    protected Variable copy() {
        return new CoordinateAxis(this.ncd, this);
    }
    
    public AxisType getAxisType() {
        return this.axisType;
    }
    
    public void setAxisType(final AxisType axisType) {
        this.axisType = axisType;
    }
    
    @Override
    public String getUnitsString() {
        final String units = super.getUnitsString();
        return (units == null) ? "" : units;
    }
    
    public boolean isNumeric() {
        return this.getDataType() != DataType.CHAR && this.getDataType() != DataType.STRING && this.getDataType() != DataType.STRUCTURE;
    }
    
    public boolean isContiguous() {
        return this.isContiguous;
    }
    
    public String getPositive() {
        return this.positive;
    }
    
    public void setPositive(final String positive) {
        this.positive = positive;
    }
    
    public String getBoundaryRef() {
        return this.boundaryRef;
    }
    
    public void setBoundaryRef(final String boundaryRef) {
        this.boundaryRef = boundaryRef;
    }
    
    private void init() {
        try {
            final Array data = this.read();
            this.minmax = MAMath.getMinMax(data);
        }
        catch (IOException ioe) {
            CoordinateAxis.log.error("Error reading coordinate values ", ioe);
            throw new IllegalStateException(ioe);
        }
    }
    
    public double getMinValue() {
        if (this.minmax == null) {
            this.init();
        }
        return this.minmax.min;
    }
    
    public double getMaxValue() {
        if (this.minmax == null) {
            this.init();
        }
        return this.minmax.max;
    }
    
    public void getInfo(final Formatter buf) {
        buf.format("%-30s", this.getNameAndDimensions());
        buf.format("%-20s", this.getUnitsString());
        if (this.axisType != null) {
            buf.format("%-10s", this.axisType.toString());
        }
        buf.format("%s", this.getDescription());
    }
    
    @Override
    public boolean equals(final Object oo) {
        if (this == oo) {
            return true;
        }
        if (!(oo instanceof CoordinateAxis)) {
            return false;
        }
        if (!super.equals(oo)) {
            return false;
        }
        final CoordinateAxis o = (CoordinateAxis)oo;
        return (this.getAxisType() == null || this.getAxisType().equals(o.getAxisType())) && (this.getPositive() == null || this.getPositive().equals(o.getPositive()));
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int result = super.hashCode();
            if (this.getAxisType() != null) {
                result = 37 * result + this.getAxisType().hashCode();
            }
            if (this.getPositive() != null) {
                result = 37 * result + this.getPositive().hashCode();
            }
            this.hashCode = result;
        }
        return this.hashCode;
    }
    
    static {
        CoordinateAxis.log = LoggerFactory.getLogger(CoordinateAxis.class);
    }
    
    public static class AxisComparator implements Comparator<CoordinateAxis>
    {
        public int compare(final CoordinateAxis c1, final CoordinateAxis c2) {
            final AxisType t1 = c1.getAxisType();
            final AxisType t2 = c2.getAxisType();
            if (t1 == null && t2 == null) {
                return c1.getName().compareTo(c2.getName());
            }
            if (t1 == null) {
                return -1;
            }
            if (t2 == null) {
                return 1;
            }
            return t1.axisOrder() - t2.axisOrder();
        }
        
        @Override
        public boolean equals(final Object obj) {
            return this == obj;
        }
    }
}
