// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ncml;

import ucar.ma2.Index;
import ucar.nc2.util.cache.FileFactory;
import java.util.EnumSet;
import ucar.ma2.DataType;
import ucar.ma2.Array;
import java.io.IOException;
import ucar.nc2.Group;
import java.util.Iterator;
import ucar.nc2.Structure;
import ucar.nc2.dataset.VariableDS;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;
import ucar.ma2.Range;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.dataset.ReplaceVariableCheck;
import ucar.nc2.dataset.DatasetConstructor;
import ucar.nc2.util.CancelTask;
import java.util.StringTokenizer;
import java.util.ArrayList;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.ma2.Section;
import ucar.nc2.Dimension;
import java.util.List;
import ucar.nc2.ProxyReader;

public class AggregationTiled extends Aggregation implements ProxyReader
{
    private List<String> dimNames;
    private List<Dimension> dims;
    private Section section;
    private boolean debug;
    
    public AggregationTiled(final NetcdfDataset ncd, final String dimName, final String recheckS) {
        super(ncd, dimName, Type.tiled, recheckS);
        this.dimNames = new ArrayList<String>();
        this.dims = new ArrayList<Dimension>();
        this.debug = false;
        final StringTokenizer stoke = new StringTokenizer(dimName);
        while (stoke.hasMoreTokens()) {
            this.dimNames.add(stoke.nextToken());
        }
    }
    
    @Override
    protected void buildNetcdfDataset(final CancelTask cancelTask) throws IOException {
        final Dataset typicalDataset = this.getTypicalDataset();
        final NetcdfFile typical = typicalDataset.acquireFile(null);
        DatasetConstructor.transferDataset(typical, this.ncDataset, null);
        for (final String dimName : this.dimNames) {
            final Dimension dim = this.ncDataset.getRootGroup().findDimension(dimName);
            if (null == dim) {
                throw new IllegalArgumentException("Unknown dimension = " + dimName);
            }
            this.dims.add(dim);
        }
        Section result = null;
        for (final Dataset d : this.datasets) {
            final DatasetTiled dt = (DatasetTiled)d;
            try {
                dt.section = dt.section.addRangeNames(this.dimNames);
                result = ((result == null) ? dt.section : result.union(dt.section));
            }
            catch (InvalidRangeException e) {
                throw new IllegalArgumentException(e);
            }
        }
        assert result != null;
        assert result.getRank() == this.dims.size();
        for (final Range r : result.getRanges()) {
            assert r.first() == 0;
            assert r.stride() == 1;
        }
        this.section = result;
        int count = 0;
        for (final Range r2 : this.section.getRanges()) {
            final Dimension dim2 = this.dims.get(count++);
            dim2.setLength(r2.length());
        }
        for (final Variable v : typical.getVariables()) {
            if (this.isTiled(v)) {
                final Group newGroup = DatasetConstructor.findGroup(this.ncDataset, v.getParentGroup());
                final VariableDS vagg = new VariableDS(this.ncDataset, newGroup, null, v.getShortName(), v.getDataType(), v.getDimensionsString(), null, null);
                vagg.setProxyReader(this);
                DatasetConstructor.transferVariableAttributes(v, vagg);
                newGroup.removeVariable(v.getShortName());
                newGroup.addVariable(vagg);
            }
            if (cancelTask != null && cancelTask.isCancel()) {
                return;
            }
        }
        this.setDatasetAcquireProxy(typicalDataset, this.ncDataset);
        typicalDataset.close(typical);
    }
    
    private boolean isTiled(final Variable v) {
        for (final Dimension d : v.getDimensions()) {
            for (final Range r : this.section.getRanges()) {
                if (d.getName().equals(r.getName())) {
                    return true;
                }
            }
        }
        return false;
    }
    
    @Override
    protected void rebuildDataset() throws IOException {
        this.ncDataset.empty();
        this.dims = new ArrayList<Dimension>();
        this.buildNetcdfDataset(null);
    }
    
    public Array reallyRead(final Variable mainv, final CancelTask cancelTask) throws IOException {
        final DataType dtype = (mainv instanceof VariableDS) ? ((VariableDS)mainv).getOriginalDataType() : mainv.getDataType();
        final Array allData = Array.factory(dtype, mainv.getShape());
        final Section wantSection = mainv.getShapeAsSection();
        if (this.debug) {
            System.out.println("wantSection: " + wantSection + " for var " + mainv.getName());
        }
        final List<Dataset> nestedDatasets = this.getDatasets();
        for (final Dataset vnested : nestedDatasets) {
            final DatasetTiled dtiled = (DatasetTiled)vnested;
            final Section tiledSection = dtiled.makeVarSection(mainv);
            Array varData;
            TileLayout index;
            try {
                varData = dtiled.read(mainv, cancelTask);
                index = new TileLayout(tiledSection, wantSection);
                if (this.debug) {
                    System.out.println(" varData read: " + new Section(varData.getShape()));
                }
            }
            catch (InvalidRangeException e) {
                throw new IllegalArgumentException(e.getMessage());
            }
            while (index.hasNext()) {
                try {
                    Array.arraycopy(varData, index.srcPos, allData, index.resultPos, index.nelems);
                    continue;
                }
                catch (RuntimeException e2) {
                    System.out.println(index.toString());
                    throw e2;
                }
                break;
            }
            if (varData.getSize() == mainv.getSize()) {
                break;
            }
            if (cancelTask != null && cancelTask.isCancel()) {
                return null;
            }
        }
        return allData;
    }
    
    public Array reallyRead(final Variable mainv, final Section wantSection, final CancelTask cancelTask) throws IOException {
        final long size = wantSection.computeSize();
        if (size == mainv.getSize()) {
            return this.reallyRead(mainv, cancelTask);
        }
        final DataType dtype = (mainv instanceof VariableDS) ? ((VariableDS)mainv).getOriginalDataType() : mainv.getDataType();
        final Array allData = Array.factory(dtype, wantSection.getShape());
        if (this.debug) {
            System.out.println(dtype + " allData allocated: " + new Section(allData.getShape()));
        }
        final List<Dataset> nestedDatasets = this.getDatasets();
        for (final Dataset vnested : nestedDatasets) {
            final DatasetTiled dtiled = (DatasetTiled)vnested;
            final Section tiledSection = dtiled.makeVarSection(mainv);
            Array varData;
            TileLayout index;
            try {
                if (!tiledSection.intersects(wantSection)) {
                    continue;
                }
                final Section needToRead = tiledSection.intersect(wantSection);
                if (this.debug) {
                    System.out.println(" tiledSection: " + tiledSection + " from file " + dtiled.getLocation());
                }
                if (this.debug) {
                    System.out.println(" intersection: " + needToRead);
                }
                final Section localNeed = needToRead.shiftOrigin(tiledSection);
                varData = dtiled.read(mainv, cancelTask, localNeed.getRanges());
                index = new TileLayout(needToRead, wantSection);
            }
            catch (InvalidRangeException e) {
                throw new IllegalArgumentException(e.getMessage());
            }
            while (index.hasNext()) {
                try {
                    Array.arraycopy(varData, index.srcPos, allData, index.resultPos, index.nelems);
                    continue;
                }
                catch (RuntimeException e2) {
                    System.out.println(" tiledSection: " + tiledSection);
                    System.out.println(index.toString());
                    throw e2;
                }
                break;
            }
            if (varData.getSize() == mainv.getSize()) {
                break;
            }
            if (cancelTask != null && cancelTask.isCancel()) {
                return null;
            }
        }
        return allData;
    }
    
    @Override
    protected Dataset makeDataset(final String cacheName, final String location, final String id, final String ncoordS, final String coordValueS, final String sectionSpec, final EnumSet<NetcdfDataset.Enhance> enhance, final FileFactory reader) {
        return new DatasetTiled(cacheName, location, id, sectionSpec, enhance, reader);
    }
    
    private class TileLayout
    {
        private int srcPos;
        private int resultPos;
        private int nelems;
        private int total;
        private int startElem;
        Index index;
        boolean first;
        
        TileLayout(final Section localSection, final Section wantSection) throws InvalidRangeException {
            this.srcPos = 0;
            this.first = true;
            final Section dataSection = localSection.compact();
            final Section resultSection = wantSection.compact();
            if (AggregationTiled.this.debug) {
                System.out.println(" resultSection: " + resultSection);
            }
            if (AggregationTiled.this.debug) {
                System.out.println(" dataSection: " + dataSection);
            }
            final int rank = dataSection.getRank();
            this.total = (int)dataSection.computeSize();
            long product = 1L;
            this.startElem = 0;
            for (int ii = rank - 1; ii >= 0; --ii) {
                final int d = dataSection.getOrigin(ii) - resultSection.getOrigin(ii);
                if (d > 0) {
                    this.startElem += (int)(product * d);
                }
                product *= resultSection.getShape(ii);
            }
            this.resultPos = this.startElem;
            this.nelems = localSection.getShape(rank - 1);
            final int[] stride = new int[rank - 1];
            final int[] shape = new int[rank - 1];
            product = resultSection.getShape(rank - 1);
            for (int ii2 = rank - 2; ii2 >= 0; --ii2) {
                stride[ii2] = (int)product;
                shape[ii2] = dataSection.getShape(ii2);
                product *= resultSection.getShape(ii2);
            }
            this.index = new Index(shape, stride);
        }
        
        boolean hasNext() {
            if (this.first) {
                this.first = false;
                return true;
            }
            this.srcPos += this.nelems;
            if (this.srcPos >= this.total) {
                return false;
            }
            this.index.incr();
            this.resultPos = this.startElem + this.index.currentElement();
            return true;
        }
        
        @Override
        public String toString() {
            return "  nElems: " + this.nelems + " srcPos: " + this.srcPos + " resultPos: " + this.resultPos;
        }
    }
    
    class DatasetTiled extends Dataset
    {
        protected String sectionSpec;
        protected Section section;
        
        protected DatasetTiled(final String cacheName, final String location, final String id, final String sectionSpec, final EnumSet<NetcdfDataset.Enhance> enhance, final FileFactory reader) {
            super(cacheName, location, id, enhance, reader);
            this.sectionSpec = sectionSpec;
            try {
                this.section = new Section(sectionSpec);
            }
            catch (InvalidRangeException e) {
                throw new IllegalArgumentException(e);
            }
        }
        
        boolean isNeeded(final Section wantSection) throws InvalidRangeException {
            return this.section.intersects(wantSection);
        }
        
        Section makeVarSection(final Variable mainv) {
            final Section vSection = mainv.getShapeAsSection();
            final Section dataSection = new Section();
            for (final Range r : vSection.getRanges()) {
                final Range rr = this.section.find(r.getName());
                dataSection.appendRange((rr != null) ? rr : r);
            }
            return dataSection;
        }
    }
}
