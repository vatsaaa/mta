// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ncml;

import ucar.nc2.util.cache.FileCacheable;
import org.slf4j.LoggerFactory;
import java.io.FileInputStream;
import ucar.nc2.FileWriter;
import ucar.nc2.util.cache.FileFactory;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Section;
import ucar.ma2.MAMath;
import ucar.nc2.dataset.VariableDS;
import ucar.nc2.dataset.StructureDS;
import ucar.nc2.Structure;
import java.util.Iterator;
import ucar.nc2.Dimension;
import ucar.nc2.Variable;
import java.util.List;
import java.util.StringTokenizer;
import java.util.ArrayList;
import ucar.ma2.DataType;
import ucar.unidata.util.StringUtil;
import ucar.ma2.Array;
import ucar.nc2.Attribute;
import ucar.nc2.util.URLnaming;
import java.io.Reader;
import java.net.URL;
import ucar.nc2.Group;
import java.util.Set;
import org.jdom.Element;
import org.jdom.Document;
import java.io.InputStream;
import ucar.nc2.NetcdfFile;
import org.jdom.output.XMLOutputter;
import org.jdom.JDOMException;
import java.io.IOException;
import org.jdom.input.SAXBuilder;
import ucar.nc2.util.IO;
import java.io.FileNotFoundException;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.util.DebugFlags;
import java.util.Formatter;
import org.slf4j.Logger;
import org.jdom.Namespace;

public class NcMLReader
{
    public static final Namespace ncNS;
    private static Logger log;
    private static boolean debugURL;
    private static boolean debugXML;
    private static boolean showParsedXML;
    private static boolean debugOpen;
    private static boolean debugConstruct;
    private static boolean debugCmd;
    private static boolean debugAggDetail;
    private static boolean validate;
    private String location;
    private boolean explicit;
    private Formatter errlog;
    
    public NcMLReader() {
        this.explicit = false;
        this.errlog = new Formatter();
    }
    
    public static void setDebugFlags(final DebugFlags debugFlag) {
        NcMLReader.debugURL = debugFlag.isSet("NcML/debugURL");
        NcMLReader.debugXML = debugFlag.isSet("NcML/debugXML");
        NcMLReader.showParsedXML = debugFlag.isSet("NcML/showParsedXML");
        NcMLReader.debugCmd = debugFlag.isSet("NcML/debugCmd");
        NcMLReader.debugOpen = debugFlag.isSet("NcML/debugOpen");
        NcMLReader.debugConstruct = debugFlag.isSet("NcML/debugConstruct");
        NcMLReader.debugAggDetail = debugFlag.isSet("NcML/debugAggDetail");
    }
    
    public static void wrapNcMLresource(final NetcdfDataset ncDataset, final String ncmlResourceLocation, final CancelTask cancelTask) throws IOException {
        final ClassLoader cl = ncDataset.getClass().getClassLoader();
        final InputStream is = cl.getResourceAsStream(ncmlResourceLocation);
        if (is == null) {
            throw new FileNotFoundException(ncmlResourceLocation);
        }
        if (NcMLReader.debugXML) {
            System.out.println(" NetcdfDataset URL = <" + ncmlResourceLocation + ">");
            final InputStream is2 = cl.getResourceAsStream(ncmlResourceLocation);
            System.out.println(" contents=\n" + IO.readContents(is2));
        }
        Document doc;
        try {
            final SAXBuilder builder = new SAXBuilder(NcMLReader.validate);
            if (NcMLReader.debugURL) {
                System.out.println(" NetcdfDataset URL = <" + ncmlResourceLocation + ">");
            }
            doc = builder.build(is);
        }
        catch (JDOMException e) {
            throw new IOException(e.getMessage());
        }
        if (NcMLReader.debugXML) {
            System.out.println(" SAXBuilder done");
        }
        if (NcMLReader.showParsedXML) {
            final XMLOutputter xmlOut = new XMLOutputter();
            System.out.println("*** NetcdfDataset/showParsedXML = \n" + xmlOut.outputString(doc) + "\n*******");
        }
        final Element netcdfElem = doc.getRootElement();
        final NcMLReader reader = new NcMLReader();
        reader.readNetcdf(ncDataset.getLocation(), ncDataset, ncDataset, netcdfElem, cancelTask);
        if (NcMLReader.debugOpen) {
            System.out.println("***NcMLReader.wrapNcML result= \n" + ncDataset);
        }
    }
    
    public static void wrapNcML(final NetcdfDataset ncDataset, final String ncmlLocation, final CancelTask cancelTask) throws IOException {
        Document doc;
        try {
            final SAXBuilder builder = new SAXBuilder(NcMLReader.validate);
            if (NcMLReader.debugURL) {
                System.out.println(" NetcdfDataset URL = <" + ncmlLocation + ">");
            }
            doc = builder.build(ncmlLocation);
        }
        catch (JDOMException e) {
            throw new IOException(e.getMessage());
        }
        if (NcMLReader.debugXML) {
            System.out.println(" SAXBuilder done");
        }
        if (NcMLReader.showParsedXML) {
            final XMLOutputter xmlOut = new XMLOutputter();
            System.out.println("*** NetcdfDataset/showParsedXML = \n" + xmlOut.outputString(doc) + "\n*******");
        }
        final Element netcdfElem = doc.getRootElement();
        final NcMLReader reader = new NcMLReader();
        reader.readNetcdf(ncmlLocation, ncDataset, ncDataset, netcdfElem, cancelTask);
        if (NcMLReader.debugOpen) {
            System.out.println("***NcMLReader.wrapNcML result= \n" + ncDataset);
        }
    }
    
    public static NetcdfDataset mergeNcML(final NetcdfFile ref, final Element parentElem) throws IOException {
        final NetcdfDataset targetDS = new NetcdfDataset(ref, null);
        final NcMLReader reader = new NcMLReader();
        reader.readGroup(targetDS, targetDS, null, null, parentElem);
        targetDS.finish();
        return targetDS;
    }
    
    public static NetcdfDataset mergeNcMLdirect(final NetcdfDataset targetDS, final Element parentElem) throws IOException {
        final NcMLReader reader = new NcMLReader();
        reader.readGroup(targetDS, targetDS, null, null, parentElem);
        targetDS.finish();
        return targetDS;
    }
    
    public static NetcdfDataset readNcML(final String ncmlLocation, final CancelTask cancelTask) throws IOException {
        return readNcML(ncmlLocation, (String)null, cancelTask);
    }
    
    public static NetcdfDataset readNcML(final String ncmlLocation, String referencedDatasetUri, final CancelTask cancelTask) throws IOException {
        final URL url = new URL(ncmlLocation);
        if (NcMLReader.debugURL) {
            System.out.println(" NcMLReader open " + ncmlLocation);
            System.out.println("   URL = " + url.toString());
            System.out.println("   external form = " + url.toExternalForm());
            System.out.println("   protocol = " + url.getProtocol());
            System.out.println("   host = " + url.getHost());
            System.out.println("   path = " + url.getPath());
            System.out.println("  file = " + url.getFile());
        }
        Document doc;
        try {
            final SAXBuilder builder = new SAXBuilder(NcMLReader.validate);
            if (NcMLReader.debugURL) {
                System.out.println(" NetcdfDataset URL = <" + url + ">");
            }
            doc = builder.build(url);
        }
        catch (JDOMException e) {
            throw new IOException(e.getMessage());
        }
        if (NcMLReader.debugXML) {
            System.out.println(" SAXBuilder done");
        }
        if (NcMLReader.showParsedXML) {
            final XMLOutputter xmlOut = new XMLOutputter();
            System.out.println("*** NetcdfDataset/showParsedXML = \n" + xmlOut.outputString(doc) + "\n*******");
        }
        final Element netcdfElem = doc.getRootElement();
        if (referencedDatasetUri == null) {
            referencedDatasetUri = netcdfElem.getAttributeValue("location");
            if (referencedDatasetUri == null) {
                referencedDatasetUri = netcdfElem.getAttributeValue("uri");
            }
        }
        final NcMLReader reader = new NcMLReader();
        final NetcdfDataset ncd = reader.readNcML(ncmlLocation, referencedDatasetUri, netcdfElem, cancelTask);
        if (NcMLReader.debugOpen) {
            System.out.println("***NcMLReader.readNcML result= \n" + ncd);
        }
        return ncd;
    }
    
    public static NetcdfDataset readNcML(final InputStream ins, final CancelTask cancelTask) throws IOException {
        Document doc;
        try {
            final SAXBuilder builder = new SAXBuilder(NcMLReader.validate);
            doc = builder.build(ins);
        }
        catch (JDOMException e) {
            throw new IOException(e.getMessage());
        }
        if (NcMLReader.debugXML) {
            System.out.println(" SAXBuilder done");
        }
        if (NcMLReader.showParsedXML) {
            final XMLOutputter xmlOut = new XMLOutputter();
            System.out.println("*** NetcdfDataset/showParsedXML = \n" + xmlOut.outputString(doc) + "\n*******");
        }
        final Element netcdfElem = doc.getRootElement();
        final NetcdfDataset ncd = readNcML(null, netcdfElem, cancelTask);
        if (NcMLReader.debugOpen) {
            System.out.println("***NcMLReader.readNcML (stream) result= \n" + ncd);
        }
        return ncd;
    }
    
    public static NetcdfDataset readNcML(final Reader r, final CancelTask cancelTask) throws IOException {
        return readNcML(r, null, cancelTask);
    }
    
    public static NetcdfDataset readNcML(final Reader r, final String ncmlLocation, final CancelTask cancelTask) throws IOException {
        Document doc;
        try {
            final SAXBuilder builder = new SAXBuilder(NcMLReader.validate);
            doc = builder.build(r);
        }
        catch (JDOMException e) {
            throw new IOException(e.getMessage());
        }
        if (NcMLReader.debugXML) {
            System.out.println(" SAXBuilder done");
        }
        if (NcMLReader.showParsedXML) {
            final XMLOutputter xmlOut = new XMLOutputter();
            System.out.println("*** NetcdfDataset/showParsedXML = \n" + xmlOut.outputString(doc) + "\n*******");
        }
        final Element netcdfElem = doc.getRootElement();
        final NetcdfDataset ncd = readNcML(ncmlLocation, netcdfElem, cancelTask);
        if (NcMLReader.debugOpen) {
            System.out.println("***NcMLReader.readNcML (stream) result= \n" + ncd);
        }
        return ncd;
    }
    
    public static NetcdfDataset readNcML(final String ncmlLocation, final Element netcdfElem, final CancelTask cancelTask) throws IOException {
        String referencedDatasetUri = netcdfElem.getAttributeValue("location");
        if (referencedDatasetUri == null) {
            referencedDatasetUri = netcdfElem.getAttributeValue("uri");
        }
        final NcMLReader reader = new NcMLReader();
        return reader.readNcML(ncmlLocation, referencedDatasetUri, netcdfElem, cancelTask);
    }
    
    private NetcdfDataset readNcML(final String ncmlLocation, String referencedDatasetUri, final Element netcdfElem, final CancelTask cancelTask) throws IOException {
        referencedDatasetUri = URLnaming.resolve(ncmlLocation, referencedDatasetUri);
        if (referencedDatasetUri != null && referencedDatasetUri.equals(ncmlLocation)) {
            throw new IllegalArgumentException("NcML location attribute refers to the NcML document itself" + referencedDatasetUri);
        }
        final String iospS = netcdfElem.getAttributeValue("iosp");
        final String iospParam = netcdfElem.getAttributeValue("iospParam");
        final String bufferSizeS = netcdfElem.getAttributeValue("buffer_size");
        int buffer_size = -1;
        if (bufferSizeS != null) {
            buffer_size = Integer.parseInt(bufferSizeS);
        }
        NetcdfDataset refds = null;
        if (referencedDatasetUri != null) {
            if (iospS != null) {
                NetcdfFile ncfile;
                try {
                    ncfile = new NcMLNetcdfFile(iospS, iospParam, referencedDatasetUri, buffer_size, cancelTask);
                }
                catch (Exception e) {
                    throw new IOException(e.getMessage());
                }
                refds = new NetcdfDataset(ncfile, false);
            }
            else {
                refds = NetcdfDataset.openDataset(referencedDatasetUri, null, buffer_size, cancelTask, iospParam);
            }
        }
        final Element elemE = netcdfElem.getChild("explicit", NcMLReader.ncNS);
        this.explicit = (elemE != null);
        NetcdfDataset targetDS;
        if (this.explicit || refds == null) {
            targetDS = new NetcdfDataset();
            if (refds == null) {
                refds = targetDS;
            }
            else {
                targetDS.setReferencedFile(refds);
            }
        }
        else {
            targetDS = refds;
        }
        this.readNetcdf(ncmlLocation, targetDS, refds, netcdfElem, cancelTask);
        return targetDS;
    }
    
    public void readNetcdf(final String ncmlLocation, final NetcdfDataset targetDS, final NetcdfFile refds, final Element netcdfElem, final CancelTask cancelTask) throws IOException {
        this.location = ncmlLocation;
        if (NcMLReader.debugOpen) {
            System.out.println("NcMLReader.readNetcdf ncml= " + ncmlLocation + " referencedDatasetUri= " + refds.getLocation());
        }
        final Namespace use = netcdfElem.getNamespace();
        if (!use.equals(NcMLReader.ncNS)) {
            throw new IllegalArgumentException("Incorrect namespace specified in NcML= " + use.getURI() + "\n   must be=" + NcMLReader.ncNS.getURI());
        }
        if (ncmlLocation != null) {
            targetDS.setLocation(ncmlLocation);
        }
        targetDS.setId(netcdfElem.getAttributeValue("id"));
        targetDS.setTitle(netcdfElem.getAttributeValue("title"));
        final Element aggElem = netcdfElem.getChild("aggregation", NcMLReader.ncNS);
        if (aggElem != null) {
            final Aggregation agg = this.readAgg(aggElem, ncmlLocation, targetDS, cancelTask);
            targetDS.setAggregation(agg);
            agg.finish(cancelTask);
        }
        this.readGroup(targetDS, refds, null, null, netcdfElem);
        final String errors = this.errlog.toString();
        if (errors.length() > 0) {
            throw new IllegalArgumentException("NcML had fatal errors:" + errors);
        }
        targetDS.finish();
        final Set<NetcdfDataset.Enhance> mode = NetcdfDataset.parseEnhanceMode(netcdfElem.getAttributeValue("enhance"));
        targetDS.enhance(mode);
        final String addRecords = netcdfElem.getAttributeValue("addRecords");
        if (addRecords != null && addRecords.equalsIgnoreCase("true")) {
            targetDS.sendIospMessage("AddRecordStructure");
        }
    }
    
    private void readAtt(final Object parent, final Object refParent, final Element attElem) {
        final String name = attElem.getAttributeValue("name");
        if (name == null) {
            this.errlog.format("NcML Attribute name is required (%s)%n", attElem);
            return;
        }
        String nameInFile = attElem.getAttributeValue("orgName");
        final boolean newName = nameInFile != null && !nameInFile.equals(name);
        if (nameInFile == null) {
            nameInFile = name;
        }
        else if (null == this.findAttribute(refParent, nameInFile)) {
            this.errlog.format("NcML attribute orgName '%s' doesnt exist. att=%s in=%s%n", nameInFile, name, parent);
            return;
        }
        final Attribute att = this.findAttribute(refParent, nameInFile);
        if (att == null) {
            if (NcMLReader.debugConstruct) {
                System.out.println(" add new att = " + name);
            }
            try {
                final Array values = readAttributeValues(attElem);
                this.addAttribute(parent, new Attribute(name, values));
            }
            catch (RuntimeException e) {
                this.errlog.format("NcML new Attribute Exception: %s att=%s in=%s%n", e.getMessage(), name, parent);
            }
        }
        else {
            if (NcMLReader.debugConstruct) {
                System.out.println(" modify existing att = " + name);
            }
            final boolean hasValue = attElem.getAttribute("value") != null;
            Label_0352: {
                if (hasValue) {
                    try {
                        final Array values2 = readAttributeValues(attElem);
                        this.addAttribute(parent, new Attribute(name, values2));
                        break Label_0352;
                    }
                    catch (RuntimeException e2) {
                        this.errlog.format("NcML existing Attribute Exception: %s att=%s in=%s%n", e2.getMessage(), name, parent);
                        return;
                    }
                }
                this.addAttribute(parent, new Attribute(name, att.getValues()));
            }
            if (newName && !this.explicit) {
                this.removeAttribute(parent, att);
                if (NcMLReader.debugConstruct) {
                    System.out.println(" remove old att = " + nameInFile);
                }
            }
        }
    }
    
    public static Array readAttributeValues(final Element s) throws IllegalArgumentException {
        String valString = s.getAttributeValue("value");
        if (valString == null) {
            throw new IllegalArgumentException("No value specified");
        }
        valString = StringUtil.unquoteXmlAttribute(valString);
        final String type = s.getAttributeValue("type");
        DataType dtype = (type == null) ? DataType.STRING : DataType.getType(type);
        if (dtype == DataType.CHAR) {
            dtype = DataType.STRING;
        }
        String sep = s.getAttributeValue("separator");
        if (sep == null && dtype == DataType.STRING) {
            final List<String> list = new ArrayList<String>();
            list.add(valString);
            return Array.makeArray(dtype, list);
        }
        if (sep == null) {
            sep = " ";
        }
        final List<String> stringValues = new ArrayList<String>();
        final StringTokenizer tokn = new StringTokenizer(valString, sep);
        while (tokn.hasMoreTokens()) {
            stringValues.add(tokn.nextToken());
        }
        return Array.makeArray(dtype, stringValues);
    }
    
    private Attribute findAttribute(final Object parent, final String name) {
        if (parent == null) {
            return null;
        }
        if (parent instanceof Group) {
            return ((Group)parent).findAttribute(name);
        }
        if (parent instanceof Variable) {
            return ((Variable)parent).findAttribute(name);
        }
        return null;
    }
    
    private void addAttribute(final Object parent, final Attribute att) {
        if (parent instanceof Group) {
            ((Group)parent).addAttribute(att);
        }
        else if (parent instanceof Variable) {
            ((Variable)parent).addAttribute(att);
        }
    }
    
    private void removeAttribute(final Object parent, final Attribute att) {
        if (parent instanceof Group) {
            ((Group)parent).remove(att);
        }
        else if (parent instanceof Variable) {
            ((Variable)parent).remove(att);
        }
    }
    
    private void readDim(final Group g, final Group refg, final Element dimElem) {
        final String name = dimElem.getAttributeValue("name");
        if (name == null) {
            this.errlog.format("NcML Dimension name is required (%s)%n", dimElem);
            return;
        }
        String nameInFile = dimElem.getAttributeValue("orgName");
        if (nameInFile == null) {
            nameInFile = name;
        }
        final Dimension dim = (refg == null) ? null : refg.findDimension(nameInFile);
        if (dim == null) {
            final String lengthS = dimElem.getAttributeValue("length");
            final String isUnlimitedS = dimElem.getAttributeValue("isUnlimited");
            final String isSharedS = dimElem.getAttributeValue("isShared");
            final String isUnknownS = dimElem.getAttributeValue("isVariableLength");
            final boolean isUnlimited = isUnlimitedS != null && isUnlimitedS.equalsIgnoreCase("true");
            final boolean isUnknown = isUnknownS != null && isUnknownS.equalsIgnoreCase("true");
            boolean isShared = true;
            if (isSharedS != null && isSharedS.equalsIgnoreCase("false")) {
                isShared = false;
            }
            int len = Integer.parseInt(lengthS);
            if (isUnknownS != null && isUnknownS.equalsIgnoreCase("false")) {
                len = Dimension.VLEN.getLength();
            }
            if (NcMLReader.debugConstruct) {
                System.out.println(" add new dim = " + name);
            }
            g.addDimension(new Dimension(name, len, isShared, isUnlimited, isUnknown));
        }
        else {
            dim.setName(name);
            final String lengthS = dimElem.getAttributeValue("length");
            final String isUnlimitedS = dimElem.getAttributeValue("isUnlimited");
            final String isSharedS = dimElem.getAttributeValue("isShared");
            final String isUnknownS = dimElem.getAttributeValue("isVariableLength");
            if (isUnlimitedS != null) {
                dim.setUnlimited(isUnlimitedS.equalsIgnoreCase("true"));
            }
            if (isSharedS != null) {
                dim.setShared(!isSharedS.equalsIgnoreCase("false"));
            }
            if (isUnknownS != null) {
                dim.setVariableLength(isUnknownS.equalsIgnoreCase("true"));
            }
            if (lengthS != null && !dim.isVariableLength()) {
                final int len2 = Integer.parseInt(lengthS);
                dim.setLength(len2);
            }
            if (NcMLReader.debugConstruct) {
                System.out.println(" modify existing dim = " + name);
            }
            if (g != refg) {
                g.addDimension(dim);
            }
        }
    }
    
    private void readGroup(final NetcdfDataset newds, final NetcdfFile refds, final Group parent, final Group refParent, final Element groupElem) throws IOException {
        Group refg = null;
        Group g;
        if (parent == null) {
            g = newds.getRootGroup();
            refg = refds.getRootGroup();
            if (NcMLReader.debugConstruct) {
                System.out.println(" root group ");
            }
        }
        else {
            final String name = groupElem.getAttributeValue("name");
            if (name == null) {
                this.errlog.format("NcML Group name is required (%s)%n", groupElem);
                return;
            }
            String nameInFile = groupElem.getAttributeValue("orgName");
            if (nameInFile == null) {
                nameInFile = name;
            }
            if (refParent != null) {
                refg = refParent.findGroup(nameInFile);
            }
            if (refg == null) {
                g = new Group(newds, parent, name);
                parent.addGroup(g);
                if (NcMLReader.debugConstruct) {
                    System.out.println(" add new group = " + name);
                }
            }
            else if (parent != refParent) {
                g = new Group(newds, parent, name);
                parent.addGroup(g);
                if (NcMLReader.debugConstruct) {
                    System.out.println(" transfer existing group = " + name);
                }
            }
            else {
                g = refg;
                if (!nameInFile.equals(name)) {
                    g.setName(name);
                }
                if (NcMLReader.debugConstruct) {
                    System.out.println(" modify existing group = " + name);
                }
            }
        }
        final List<Element> attList = (List<Element>)groupElem.getChildren("attribute", NcMLReader.ncNS);
        for (final Element attElem : attList) {
            this.readAtt(g, refg, attElem);
        }
        final List<Element> dimList = (List<Element>)groupElem.getChildren("dimension", NcMLReader.ncNS);
        for (final Element dimElem : dimList) {
            this.readDim(g, refg, dimElem);
        }
        final List<Element> varList = (List<Element>)groupElem.getChildren("variable", NcMLReader.ncNS);
        for (final Element varElem : varList) {
            this.readVariable(newds, g, refg, varElem);
        }
        final List<Element> removeList = (List<Element>)groupElem.getChildren("remove", NcMLReader.ncNS);
        for (final Element e : removeList) {
            this.cmdRemove(g, e.getAttributeValue("type"), e.getAttributeValue("name"));
        }
        final List<Element> groupList = (List<Element>)groupElem.getChildren("group", NcMLReader.ncNS);
        for (final Element gElem : groupList) {
            this.readGroup(newds, refds, g, refg, gElem);
            if (NcMLReader.debugConstruct) {
                System.out.println(" add group = " + g.getName());
            }
        }
    }
    
    private void readVariable(final NetcdfDataset ds, final Group g, final Group refg, final Element varElem) throws IOException {
        final String name = varElem.getAttributeValue("name");
        if (name == null) {
            this.errlog.format("NcML Variable name is required (%s)%n", varElem);
            return;
        }
        String nameInFile = varElem.getAttributeValue("orgName");
        if (nameInFile == null) {
            nameInFile = name;
        }
        final Variable refv = (refg == null) ? null : refg.findVariable(nameInFile);
        if (refv == null) {
            if (NcMLReader.debugConstruct) {
                System.out.println(" add new var = " + name);
            }
            g.addVariable(this.readVariableNew(ds, g, null, varElem));
            return;
        }
        DataType dtype = null;
        final String typeS = varElem.getAttributeValue("type");
        if (typeS != null) {
            dtype = DataType.getType(typeS);
        }
        else {
            dtype = refv.getDataType();
        }
        final String shape = varElem.getAttributeValue("shape");
        Variable v;
        if (refg == g) {
            v = refv;
            v.setName(name);
            v.setDataType(dtype);
            if (shape != null) {
                v.setDimensions(shape);
            }
            if (NcMLReader.debugConstruct) {
                System.out.println(" modify existing var = " + nameInFile);
            }
        }
        else {
            if (refv instanceof Structure) {
                v = new StructureDS(ds, g, null, name, (Structure)refv);
                v.setDimensions(shape);
            }
            else {
                v = new VariableDS(g, null, name, refv);
                v.setDataType(dtype);
                v.setDimensions(shape);
            }
            if (NcMLReader.debugConstruct) {
                System.out.println(" modify explicit var = " + nameInFile);
            }
            g.addVariable(v);
        }
        final List<Element> attList = (List<Element>)varElem.getChildren("attribute", NcMLReader.ncNS);
        for (final Element attElem : attList) {
            this.readAtt(v, refv, attElem);
        }
        final List<Element> removeList = (List<Element>)varElem.getChildren("remove", NcMLReader.ncNS);
        for (final Element remElem : removeList) {
            this.cmdRemove(v, remElem.getAttributeValue("type"), remElem.getAttributeValue("name"));
        }
        if (v.getDataType() == DataType.STRUCTURE) {
            final StructureDS s = (StructureDS)v;
            final StructureDS refS = (StructureDS)refv;
            final List<Element> varList = (List<Element>)varElem.getChildren("variable", NcMLReader.ncNS);
            for (final Element vElem : varList) {
                this.readVariableNested(ds, s, refS, vElem);
            }
        }
        else {
            final Element valueElem = varElem.getChild("values", NcMLReader.ncNS);
            if (valueElem != null) {
                this.readValues(ds, v, varElem, valueElem);
            }
            else if (v.hasCachedData()) {
                Array data;
                try {
                    data = v.read();
                }
                catch (IOException e) {
                    throw new IllegalStateException(e.getMessage());
                }
                if (data.getClass() != v.getDataType().getPrimitiveClassType()) {
                    final Array newData = Array.factory(v.getDataType(), v.getShape());
                    MAMath.copy(newData, data);
                    v.setCachedData(newData, false);
                }
            }
        }
        Element viewElem = varElem.getChild("logicalSection", NcMLReader.ncNS);
        if (null != viewElem) {
            final String sectionSpec = viewElem.getAttributeValue("section");
            if (sectionSpec != null) {
                try {
                    final Section s2 = new Section(sectionSpec);
                    final Section viewSection = Section.fill(s2, v.getShape());
                    if (!v.getShapeAsSection().contains(viewSection)) {
                        this.errlog.format("Invalid logicalSection on variable=%s section =(%s) original=(%s) %n", v.getName(), sectionSpec, v.getShapeAsSection());
                        return;
                    }
                    final Variable view = v.section(viewSection);
                    g.removeVariable(v.getShortName());
                    g.addVariable(view);
                }
                catch (InvalidRangeException e2) {
                    this.errlog.format("Invalid logicalSection on variable=%s section=(%s) error=%s %n", v.getName(), sectionSpec, e2.getMessage());
                    return;
                }
            }
        }
        viewElem = varElem.getChild("logicalSlice", NcMLReader.ncNS);
        if (null != viewElem) {
            final String dimName = viewElem.getAttributeValue("dimName");
            if (null == dimName) {
                this.errlog.format("NcML logicalSlice: dimName is required, variable=%s %n", v.getName());
                return;
            }
            final int dim = v.findDimensionIndex(dimName);
            if (dim < 0) {
                this.errlog.format("NcML logicalSlice: cant find dimension %s in variable=%s %n", dimName, v.getName());
                return;
            }
            final String indexS = viewElem.getAttributeValue("index");
            int index = -1;
            if (null == indexS) {
                this.errlog.format("NcML logicalSlice: index is required, variable=%s %n", v.getName());
                return;
            }
            try {
                index = Integer.parseInt(indexS);
            }
            catch (NumberFormatException e4) {
                this.errlog.format("NcML logicalSlice: index=%s must be integer, variable=%s %n", indexS, v.getName());
                return;
            }
            try {
                final Variable view2 = v.slice(dim, index);
                g.removeVariable(v.getShortName());
                g.addVariable(view2);
            }
            catch (InvalidRangeException e3) {
                this.errlog.format("Invalid logicalSlice (%d,%d) on variable=%s error=%s %n", dim, index, v.getName(), e3.getMessage());
            }
        }
    }
    
    private Variable readVariableNew(final NetcdfDataset ds, final Group g, final Structure parentS, final Element varElem) {
        final String name = varElem.getAttributeValue("name");
        if (name == null) {
            this.errlog.format("NcML Variable name is required (%s)%n", varElem);
            return null;
        }
        final String type = varElem.getAttributeValue("type");
        if (type == null) {
            throw new IllegalArgumentException("New variable (" + name + ") must have datatype attribute");
        }
        final DataType dtype = DataType.getType(type);
        String shape = varElem.getAttributeValue("shape");
        if (shape == null) {
            shape = "";
        }
        Variable v;
        if (dtype == DataType.STRUCTURE) {
            final StructureDS s = (StructureDS)(v = new StructureDS(ds, g, parentS, name, shape, null, null));
            final List<Element> varList = (List<Element>)varElem.getChildren("variable", NcMLReader.ncNS);
            for (final Element vElem : varList) {
                this.readVariableNested(ds, s, s, vElem);
            }
        }
        else {
            v = new VariableDS(ds, g, parentS, name, dtype, shape, null, null);
            final Element valueElem = varElem.getChild("values", NcMLReader.ncNS);
            if (valueElem != null) {
                this.readValues(ds, v, varElem, valueElem);
            }
        }
        final List<Element> attList = (List<Element>)varElem.getChildren("attribute", NcMLReader.ncNS);
        for (final Element attElem : attList) {
            this.readAtt(v, null, attElem);
        }
        return v;
    }
    
    private void readVariableNested(final NetcdfDataset ds, final Structure parentS, final Structure refStruct, final Element varElem) {
        final String name = varElem.getAttributeValue("name");
        if (name == null) {
            this.errlog.format("NcML Variable name is required (%s)%n", varElem);
            return;
        }
        String nameInFile = varElem.getAttributeValue("orgName");
        if (nameInFile == null) {
            nameInFile = name;
        }
        final Variable refv = refStruct.findVariable(nameInFile);
        if (refv == null) {
            if (NcMLReader.debugConstruct) {
                System.out.println(" add new var = " + name);
            }
            final Variable nested = this.readVariableNew(ds, parentS.getParentGroup(), parentS, varElem);
            parentS.addMemberVariable(nested);
            return;
        }
        Variable v;
        if (parentS == refStruct) {
            v = refv;
            v.setName(name);
        }
        else {
            if (refv instanceof Structure) {
                v = new StructureDS(parentS.getParentGroup(), (Structure)refv);
                v.setName(name);
                v.setParentStructure(parentS);
            }
            else {
                v = new VariableDS(parentS.getParentGroup(), refv, false);
                v.setName(name);
                v.setParentStructure(parentS);
            }
            parentS.addMemberVariable(v);
        }
        if (NcMLReader.debugConstruct) {
            System.out.println(" modify existing var = " + nameInFile);
        }
        final String typeS = varElem.getAttributeValue("type");
        if (typeS != null) {
            final DataType dtype = DataType.getType(typeS);
            v.setDataType(dtype);
        }
        final String shape = varElem.getAttributeValue("shape");
        if (shape != null) {
            v.setDimensions(shape);
        }
        final List<Element> attList = (List<Element>)varElem.getChildren("attribute", NcMLReader.ncNS);
        for (final Element attElem : attList) {
            this.readAtt(v, refv, attElem);
        }
        final List<Element> removeList = (List<Element>)varElem.getChildren("remove", NcMLReader.ncNS);
        for (final Element remElem : removeList) {
            this.cmdRemove(v, remElem.getAttributeValue("type"), remElem.getAttributeValue("name"));
        }
        if (v.getDataType() == DataType.STRUCTURE || v.getDataType() == DataType.SEQUENCE) {
            final StructureDS s = (StructureDS)v;
            final StructureDS refS = (StructureDS)refv;
            final List<Element> varList = (List<Element>)varElem.getChildren("variable", NcMLReader.ncNS);
            for (final Element vElem : varList) {
                this.readVariableNested(ds, s, refS, vElem);
            }
        }
        else {
            final Element valueElem = varElem.getChild("values", NcMLReader.ncNS);
            if (valueElem != null) {
                this.readValues(ds, v, varElem, valueElem);
            }
        }
    }
    
    private void readValues(final NetcdfDataset ds, final Variable v, final Element varElem, final Element valuesElem) {
        final String fromAttribute = valuesElem.getAttributeValue("fromAttribute");
        if (fromAttribute != null) {
            Attribute att = null;
            final int pos = fromAttribute.indexOf(64);
            if (pos > 0) {
                final String varName = fromAttribute.substring(0, pos);
                final String attName = fromAttribute.substring(pos + 1);
                final Variable vFrom = ds.getRootGroup().findVariable(varName);
                if (vFrom == null) {
                    this.errlog.format("Cant find variable %s %n", fromAttribute);
                    return;
                }
                att = vFrom.findAttribute(attName);
            }
            else {
                final String attName2 = (pos == 0) ? fromAttribute.substring(1) : fromAttribute;
                att = ds.getRootGroup().findAttribute(attName2);
            }
            if (att == null) {
                this.errlog.format("Cant find attribute %s %n", fromAttribute);
                return;
            }
            final Array data = att.getValues();
            v.setCachedData(data, true);
        }
        else {
            final String startS = valuesElem.getAttributeValue("start");
            final String incrS = valuesElem.getAttributeValue("increment");
            final String nptsS = valuesElem.getAttributeValue("npts");
            final int npts = (nptsS == null) ? ((int)v.getSize()) : Integer.parseInt(nptsS);
            if (startS != null && incrS != null) {
                final double start = Double.parseDouble(startS);
                final double incr = Double.parseDouble(incrS);
                ds.setValues(v, npts, start, incr);
                return;
            }
            final String values = varElem.getChildText("values", NcMLReader.ncNS);
            String sep = valuesElem.getAttributeValue("separator");
            if (sep == null) {
                sep = " ";
            }
            if (v.getDataType() == DataType.CHAR) {
                final int nhave = values.length();
                final int nwant = (int)v.getSize();
                final char[] data2 = new char[nwant];
                for (int min = Math.min(nhave, nwant), i = 0; i < min; ++i) {
                    data2[i] = values.charAt(i);
                }
                final Array dataArray = Array.factory(DataType.CHAR.getPrimitiveClassType(), v.getShape(), data2);
                v.setCachedData(dataArray, true);
            }
            else {
                final List<String> valList = new ArrayList<String>();
                final StringTokenizer tokn = new StringTokenizer(values, sep);
                while (tokn.hasMoreTokens()) {
                    valList.add(tokn.nextToken());
                }
                ds.setValues(v, valList);
            }
        }
    }
    
    private Aggregation readAgg(final Element aggElem, final String ncmlLocation, final NetcdfDataset newds, final CancelTask cancelTask) throws IOException {
        final String dimName = aggElem.getAttributeValue("dimName");
        final String type = aggElem.getAttributeValue("type");
        final String recheck = aggElem.getAttributeValue("recheckEvery");
        Aggregation agg;
        if (type.equals("joinExisting")) {
            agg = new AggregationExisting(newds, dimName, recheck);
        }
        else if (type.equals("joinNew")) {
            agg = new AggregationNew(newds, dimName, recheck);
        }
        else if (type.equals("tiled")) {
            agg = new AggregationTiled(newds, dimName, recheck);
        }
        else if (type.equals("union")) {
            agg = new AggregationUnion(newds, dimName, recheck);
        }
        else {
            if (!type.equals("forecastModelRunCollection") && !type.equals("forecastModelRunSingleCollection")) {
                throw new IllegalArgumentException("Unknown aggregation type=" + type);
            }
            final AggregationFmrc aggc = (AggregationFmrc)(agg = new AggregationFmrc(newds, dimName, recheck));
            final List<Element> scan2List = (List<Element>)aggElem.getChildren("scanFmrc", NcMLReader.ncNS);
            for (final Element scanElem : scan2List) {
                String dirLocation = scanElem.getAttributeValue("location");
                final String regexpPatternString = scanElem.getAttributeValue("regExp");
                final String suffix = scanElem.getAttributeValue("suffix");
                final String subdirs = scanElem.getAttributeValue("subdirs");
                final String olderS = scanElem.getAttributeValue("olderThan");
                final String runMatcher = scanElem.getAttributeValue("runDateMatcher");
                final String forecastMatcher = scanElem.getAttributeValue("forecastDateMatcher");
                final String offsetMatcher = scanElem.getAttributeValue("forecastOffsetMatcher");
                dirLocation = URLnaming.resolve(ncmlLocation, dirLocation);
                aggc.addDirectoryScanFmrc(dirLocation, suffix, regexpPatternString, subdirs, olderS, runMatcher, forecastMatcher, offsetMatcher);
                if (cancelTask != null && cancelTask.isCancel()) {
                    return null;
                }
                if (!NcMLReader.debugAggDetail) {
                    continue;
                }
                System.out.println(" debugAgg: nested dirLocation = " + dirLocation);
            }
        }
        if (agg instanceof AggregationOuterDimension) {
            final AggregationOuterDimension aggo = (AggregationOuterDimension)agg;
            final String timeUnitsChange = aggElem.getAttributeValue("timeUnitsChange");
            if (timeUnitsChange != null) {
                aggo.setTimeUnitsChange(timeUnitsChange.equalsIgnoreCase("true"));
            }
            List<Element> list = (List<Element>)aggElem.getChildren("variableAgg", NcMLReader.ncNS);
            for (final Element vaggElem : list) {
                final String varName = vaggElem.getAttributeValue("name");
                aggo.addVariable(varName);
            }
            list = (List<Element>)aggElem.getChildren("promoteGlobalAttribute", NcMLReader.ncNS);
            for (final Element gattElem : list) {
                final String varName = gattElem.getAttributeValue("name");
                final String orgName = gattElem.getAttributeValue("orgName");
                aggo.addVariableFromGlobalAttribute(varName, orgName);
            }
            list = (List<Element>)aggElem.getChildren("promoteGlobalAttributeCompose", NcMLReader.ncNS);
            for (final Element gattElem : list) {
                final String varName = gattElem.getAttributeValue("name");
                final String format = gattElem.getAttributeValue("format");
                final String orgName2 = gattElem.getAttributeValue("orgName");
                aggo.addVariableFromGlobalAttributeCompose(varName, format, orgName2);
            }
            list = (List<Element>)aggElem.getChildren("cacheVariable", NcMLReader.ncNS);
            for (final Element gattElem : list) {
                final String varName = gattElem.getAttributeValue("name");
                aggo.addCacheVariable(varName, null);
            }
        }
        final List<Element> ncList = (List<Element>)aggElem.getChildren("netcdf", NcMLReader.ncNS);
        for (final Element netcdfElemNested : ncList) {
            String location = netcdfElemNested.getAttributeValue("location");
            if (location == null) {
                location = netcdfElemNested.getAttributeValue("uri");
            }
            final String id = netcdfElemNested.getAttributeValue("id");
            final String ncoords = netcdfElemNested.getAttributeValue("ncoords");
            final String coordValueS = netcdfElemNested.getAttributeValue("coordValue");
            final String sectionSpec = netcdfElemNested.getAttributeValue("section");
            final NcmlElementReader reader = new NcmlElementReader(ncmlLocation, location, netcdfElemNested);
            final String cacheName = ncmlLocation + "#" + Integer.toString(netcdfElemNested.hashCode());
            agg.addExplicitDataset(cacheName, location, id, ncoords, coordValueS, sectionSpec, reader);
            if (cancelTask != null && cancelTask.isCancel()) {
                return null;
            }
            if (!NcMLReader.debugAggDetail) {
                continue;
            }
            System.out.println(" debugAgg: nested dataset = " + location);
        }
        final List<Element> dirList = (List<Element>)aggElem.getChildren("scan", NcMLReader.ncNS);
        for (final Element scanElem : dirList) {
            String dirLocation = scanElem.getAttributeValue("location");
            final String regexpPatternString = scanElem.getAttributeValue("regExp");
            final String suffix = scanElem.getAttributeValue("suffix");
            final String subdirs = scanElem.getAttributeValue("subdirs");
            final String olderS = scanElem.getAttributeValue("olderThan");
            final String dateFormatMark = scanElem.getAttributeValue("dateFormatMark");
            final Set<NetcdfDataset.Enhance> enhanceMode = NetcdfDataset.parseEnhanceMode(scanElem.getAttributeValue("enhance"));
            dirLocation = URLnaming.resolve(ncmlLocation, dirLocation);
            final Element cdElement = scanElem.getChild("crawlableDatasetImpl", NcMLReader.ncNS);
            agg.addDatasetScan(cdElement, dirLocation, suffix, regexpPatternString, dateFormatMark, enhanceMode, subdirs, olderS);
            if (cancelTask != null && cancelTask.isCancel()) {
                return null;
            }
            if (!NcMLReader.debugAggDetail) {
                continue;
            }
            System.out.println(" debugAgg: nested dirLocation = " + dirLocation);
        }
        final Element collElem = aggElem.getChild("collection", NcMLReader.ncNS);
        if (collElem != null) {
            agg.addCollection(collElem.getAttributeValue("spec"), collElem.getAttributeValue("olderThan"));
        }
        boolean needMerge = aggElem.getChildren("attribute", NcMLReader.ncNS).size() > 0;
        if (!needMerge) {
            needMerge = (aggElem.getChildren("variable", NcMLReader.ncNS).size() > 0);
        }
        if (!needMerge) {
            needMerge = (aggElem.getChildren("dimension", NcMLReader.ncNS).size() > 0);
        }
        if (!needMerge) {
            needMerge = (aggElem.getChildren("group", NcMLReader.ncNS).size() > 0);
        }
        if (!needMerge) {
            needMerge = (aggElem.getChildren("remove", NcMLReader.ncNS).size() > 0);
        }
        if (needMerge) {
            agg.setModifications(aggElem);
        }
        return agg;
    }
    
    private void cmdRemove(final Group g, final String type, final String name) {
        boolean err = false;
        if (type.equals("dimension")) {
            final Dimension dim = g.findDimension(name);
            if (dim != null) {
                g.remove(dim);
                if (NcMLReader.debugCmd) {
                    System.out.println("CMD remove " + type + " " + name);
                }
            }
            else {
                err = true;
            }
        }
        else if (type.equals("variable")) {
            final Variable v = g.findVariable(name);
            if (v != null) {
                g.remove(v);
                if (NcMLReader.debugCmd) {
                    System.out.println("CMD remove " + type + " " + name);
                }
            }
            else {
                err = true;
            }
        }
        else if (type.equals("attribute")) {
            final Attribute a = g.findAttribute(name);
            if (a != null) {
                g.remove(a);
                if (NcMLReader.debugCmd) {
                    System.out.println("CMD remove " + type + " " + name);
                }
            }
            else {
                err = true;
            }
        }
        if (err) {
            final Formatter f = new Formatter();
            f.format("CMD remove %s CANT find %s location %s%n", type, name, this.location);
            NcMLReader.log.info(f.toString());
        }
    }
    
    private void cmdRemove(final Variable v, final String type, final String name) {
        boolean err = false;
        if (type.equals("attribute")) {
            final Attribute a = v.findAttribute(name);
            if (a != null) {
                v.remove(a);
                if (NcMLReader.debugCmd) {
                    System.out.println("CMD remove " + type + " " + name);
                }
            }
            else {
                err = true;
            }
        }
        else if (type.equals("variable") && v instanceof Structure) {
            final Structure s = (Structure)v;
            final Variable nested = s.findVariable(name);
            if (nested != null) {
                s.removeMemberVariable(nested);
                if (NcMLReader.debugCmd) {
                    System.out.println("CMD remove " + type + " " + name);
                }
            }
            else {
                err = true;
            }
        }
        if (err) {
            final Formatter f = new Formatter();
            f.format("CMD remove %s CANT find %s location %s%n", type, name, this.location);
            NcMLReader.log.info(f.toString());
        }
    }
    
    public static void writeNcMLToFile(final String ncmlLocation, final String fileOutName) throws IOException {
        final NetcdfFile ncd = NetcdfDataset.acquireFile(ncmlLocation, null);
        final NetcdfFile ncdnew = FileWriter.writeToFile(ncd, fileOutName);
        ncd.close();
        ncdnew.close();
    }
    
    public static void writeNcMLToFile(final InputStream ncml, final String fileOutName) throws IOException {
        final NetcdfDataset ncd = readNcML(ncml, null);
        final NetcdfFile ncdnew = FileWriter.writeToFile(ncd, fileOutName, true);
        ncd.close();
        ncdnew.close();
    }
    
    public static void main(final String[] arg) {
        final String ncmlFile = "C:/data/AStest/oots/test.ncml";
        final String ncmlFileOut = "C:/TEMP/testNcmlOut.nc";
        try {
            final InputStream in = new FileInputStream(ncmlFile);
            writeNcMLToFile(in, ncmlFileOut);
        }
        catch (Exception ioe) {
            System.out.println("error = " + ncmlFile);
            ioe.printStackTrace();
        }
    }
    
    static {
        ncNS = Namespace.getNamespace("nc", "http://www.unidata.ucar.edu/namespaces/netcdf/ncml-2.2");
        NcMLReader.log = LoggerFactory.getLogger(NcMLReader.class);
        NcMLReader.debugURL = false;
        NcMLReader.debugXML = false;
        NcMLReader.showParsedXML = false;
        NcMLReader.debugOpen = false;
        NcMLReader.debugConstruct = false;
        NcMLReader.debugCmd = false;
        NcMLReader.debugAggDetail = false;
        NcMLReader.validate = false;
    }
    
    private static class NcMLNetcdfFile extends NetcdfFile
    {
        NcMLNetcdfFile(final String iospClassName, final String iospParam, final String location, final int buffer_size, final CancelTask cancelTask) throws IOException, IllegalAccessException, ClassNotFoundException, InstantiationException {
            super(iospClassName, iospParam, location, buffer_size, cancelTask);
        }
    }
    
    private class NcmlElementReader implements FileFactory
    {
        private Element netcdfElem;
        private String ncmlLocation;
        private String location;
        
        NcmlElementReader(final String ncmlLocation, final String location, final Element netcdfElem) {
            this.ncmlLocation = ncmlLocation;
            this.location = location;
            this.netcdfElem = netcdfElem;
        }
        
        public NetcdfFile open(final String cacheName, final int buffer_size, final CancelTask cancelTask, final Object spiObject) throws IOException {
            if (NcMLReader.debugAggDetail) {
                System.out.println(" NcmlElementReader open nested dataset " + cacheName);
            }
            final NetcdfFile result = NcMLReader.this.readNcML(this.ncmlLocation, this.location, this.netcdfElem, cancelTask);
            result.setLocation(this.ncmlLocation + "#" + this.location);
            return result;
        }
    }
}
