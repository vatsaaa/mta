// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ncml;

import java.util.Map;
import org.jdom.Element;
import java.util.HashMap;
import ucar.nc2.util.xml.Parse;
import java.nio.channels.FileLock;
import java.nio.channels.FileChannel;
import java.io.File;
import ucar.nc2.NCdumpW;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.channels.OverlappingFileLockException;
import java.io.FileOutputStream;
import ucar.ma2.IndexIterator;
import java.util.List;
import ucar.nc2.units.DateUnit;
import ucar.ma2.Array;
import java.util.Collection;
import java.util.Arrays;
import java.util.Formatter;
import ucar.nc2.dataset.CoordinateAxis1DTime;
import java.util.Date;
import java.util.ArrayList;
import java.io.IOException;
import java.util.Iterator;
import ucar.nc2.Attribute;
import ucar.nc2.ProxyReader;
import ucar.nc2.Structure;
import ucar.nc2.dataset.VariableDS;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;
import ucar.nc2.Group;
import ucar.nc2.Dimension;
import ucar.ma2.DataType;
import ucar.nc2.dataset.ReplaceVariableCheck;
import ucar.nc2.dataset.DatasetConstructor;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;

public class AggregationExisting extends AggregationOuterDimension
{
    public AggregationExisting(final NetcdfDataset ncd, final String dimName, final String recheckS) {
        super(ncd, dimName, Type.joinExisting, recheckS);
    }
    
    @Override
    protected void buildNetcdfDataset(final CancelTask cancelTask) throws IOException {
        final Dataset typicalDataset = this.getTypicalDataset();
        final NetcdfFile typical = typicalDataset.acquireFile(null);
        DatasetConstructor.transferDataset(typical, this.ncDataset, null);
        final String dimName = this.getDimensionName();
        CacheVar coordCacheVar;
        if (this.type != Type.joinExistingOne) {
            final Variable tcv = typical.findVariable(dimName);
            if (tcv == null) {
                throw new IllegalArgumentException("AggregationExisting: no coordinate variable for agg dimension= " + dimName);
            }
            coordCacheVar = new CoordValueVar(dimName, tcv.getDataType(), tcv.getUnitsString());
        }
        else {
            coordCacheVar = new CoordValueVar(dimName, DataType.STRING, "");
        }
        this.cacheList.add(coordCacheVar);
        this.persistRead();
        this.buildCoords(cancelTask);
        final Dimension aggDim = new Dimension(dimName, this.getTotalCoords());
        this.ncDataset.removeDimension(null, dimName);
        this.ncDataset.addDimension(null, aggDim);
        this.promoteGlobalAttributes((DatasetOuterDimension)typicalDataset);
        for (final Variable v : typical.getVariables()) {
            if (v.getRank() < 1) {
                continue;
            }
            final Dimension d = v.getDimension(0);
            if (!dimName.equals(d.getName())) {
                continue;
            }
            final Group newGroup = DatasetConstructor.findGroup(this.ncDataset, v.getParentGroup());
            final VariableDS vagg = new VariableDS(this.ncDataset, newGroup, null, v.getShortName(), v.getDataType(), v.getDimensionsString(), null, null);
            vagg.setProxyReader(this);
            DatasetConstructor.transferVariableAttributes(v, vagg);
            newGroup.removeVariable(v.getShortName());
            newGroup.addVariable(vagg);
            this.aggVars.add(vagg);
            if (cancelTask != null && cancelTask.isCancel()) {
                return;
            }
        }
        VariableDS joinAggCoord = (VariableDS)this.ncDataset.getRootGroup().findVariable(dimName);
        if (joinAggCoord == null && this.type == Type.joinExisting) {
            typicalDataset.close(typical);
            throw new IllegalArgumentException("No existing coordinate variable for joinExisting on " + this.getLocation());
        }
        if (this.type == Type.joinExistingOne) {
            if (joinAggCoord != null) {
                this.ncDataset.getRootGroup().removeVariable(joinAggCoord.getShortName());
            }
            joinAggCoord = new VariableDS(this.ncDataset, null, null, dimName, DataType.STRING, dimName, null, null);
            joinAggCoord.setProxyReader(this);
            this.ncDataset.getRootGroup().addVariable(joinAggCoord);
            this.aggVars.add(joinAggCoord);
            joinAggCoord.addAttribute(new Attribute("_CoordinateAxisType", "Time"));
            joinAggCoord.addAttribute(new Attribute("long_name", "time coordinate"));
            joinAggCoord.addAttribute(new Attribute("standard_name", "time"));
        }
        if (this.timeUnitsChange) {
            this.readTimeCoordinates(joinAggCoord, cancelTask);
        }
        joinAggCoord.setSPobject(coordCacheVar);
        this.persistRead();
        this.setDatasetAcquireProxy(typicalDataset, this.ncDataset);
        typicalDataset.close(typical);
        if (AggregationExisting.debugInvocation) {
            System.out.println(this.ncDataset.getLocation() + " invocation count = " + AggregationOuterDimension.invocation);
        }
    }
    
    @Override
    protected void rebuildDataset() throws IOException {
        super.rebuildDataset();
        if (this.timeUnitsChange) {
            final VariableDS joinAggCoord = (VariableDS)this.ncDataset.getRootGroup().findVariable(this.dimName);
            this.readTimeCoordinates(joinAggCoord, null);
        }
    }
    
    protected void readTimeCoordinates(final VariableDS timeAxis, final CancelTask cancelTask) throws IOException {
        final List<Date> dateList = new ArrayList<Date>();
        String timeUnits = null;
        for (final Dataset dataset : this.getDatasets()) {
            NetcdfFile ncfile = null;
            try {
                ncfile = dataset.acquireFile(cancelTask);
                final Variable v = ncfile.findVariable(timeAxis.getName());
                if (v == null) {
                    AggregationExisting.logger.warn("readTimeCoordinates: variable = " + timeAxis.getName() + " not found in file " + dataset.getLocation());
                    return;
                }
                final VariableDS vds = (VariableDS)((v instanceof VariableDS) ? v : new VariableDS(null, v, true));
                final CoordinateAxis1DTime timeCoordVar = CoordinateAxis1DTime.factory(this.ncDataset, vds, null);
                final Date[] dates = timeCoordVar.getTimeDates();
                dateList.addAll(Arrays.asList(dates));
                if (timeUnits == null) {
                    timeUnits = v.getUnitsString();
                }
            }
            finally {
                dataset.close(ncfile);
            }
            if (cancelTask != null && cancelTask.isCancel()) {
                return;
            }
        }
        final int[] shape = timeAxis.getShape();
        final int ntimes = shape[0];
        assert ntimes == dateList.size();
        final DataType coordType = (timeAxis.getDataType() == DataType.STRING) ? DataType.STRING : DataType.DOUBLE;
        final Array timeCoordVals = Array.factory(coordType, shape);
        final IndexIterator ii = timeCoordVals.getIndexIterator();
        if (timeAxis.getDataType() == DataType.STRING) {
            for (final Date date : dateList) {
                ii.setObjectNext(this.dateFormatter.toDateTimeStringISO(date));
            }
        }
        else {
            timeAxis.setDataType(DataType.DOUBLE);
            DateUnit du;
            try {
                du = new DateUnit(timeUnits);
            }
            catch (Exception e) {
                throw new IOException(e.getMessage());
            }
            timeAxis.addAttribute(new Attribute("units", timeUnits));
            for (final Date date2 : dateList) {
                final double val = du.makeValue(date2);
                ii.setDoubleNext(val);
            }
        }
        timeAxis.setCachedData(timeCoordVals, false);
    }
    
    @Override
    public void persistWrite() throws IOException {
        if (AggregationExisting.diskCache2 == null) {
            return;
        }
        final String cacheName = this.getCacheName();
        if (cacheName == null) {
            return;
        }
        final File cacheFile = AggregationExisting.diskCache2.getCacheFile(cacheName);
        if (!this.cacheDirty && cacheFile.exists()) {
            return;
        }
        FileChannel channel = null;
        try {
            if (!cacheFile.exists()) {
                final File dir = cacheFile.getParentFile();
                if (!dir.mkdirs()) {
                    AggregationExisting.logger.error("Cant make cache directory= " + cacheFile);
                }
            }
            final FileOutputStream fos = new FileOutputStream(cacheFile);
            channel = fos.getChannel();
            FileLock lock;
            try {
                lock = channel.tryLock();
            }
            catch (OverlappingFileLockException e) {
                return;
            }
            if (lock == null) {
                return;
            }
            final PrintWriter out = new PrintWriter(fos);
            out.print("<?xml version='1.0' encoding='UTF-8'?>\n");
            out.print("<aggregation xmlns='http://www.unidata.ucar.edu/namespaces/netcdf/ncml-2.2' version='3' ");
            out.print("type='" + this.type + "' ");
            if (this.dimName != null) {
                out.print("dimName='" + this.dimName + "' ");
            }
            if (this.datasetManager.getRecheck() != null) {
                out.print("recheckEvery='" + this.datasetManager.getRecheck() + "' ");
            }
            out.print(">\n");
            final List<Dataset> nestedDatasets = this.getDatasets();
            for (final Dataset dataset : nestedDatasets) {
                final DatasetOuterDimension dod = (DatasetOuterDimension)dataset;
                if (dod.getId() == null) {
                    AggregationExisting.logger.warn("id is null");
                }
                out.print("  <netcdf id='" + dod.getId() + "' ");
                out.print("ncoords='" + dod.getNcoords(null) + "' >\n");
                for (final CacheVar pv : this.cacheList) {
                    final Array data = pv.getData(dod.getId());
                    if (data != null) {
                        out.print("    <cache varName='" + pv.varName + "' >");
                        NCdumpW.printArray(data, out);
                        out.print("</cache>\n");
                        if (!AggregationExisting.logger.isDebugEnabled()) {
                            continue;
                        }
                        AggregationExisting.logger.debug(" wrote array = " + pv.varName + " nelems= " + data.getSize() + " for " + dataset.getLocation());
                    }
                }
                out.print("  </netcdf>\n");
            }
            out.print("</aggregation>\n");
            out.close();
            cacheFile.setLastModified(this.datasetManager.getLastScanned());
            this.cacheDirty = false;
            if (AggregationExisting.logger.isDebugEnabled()) {
                AggregationExisting.logger.debug("Aggregation persisted = " + cacheFile.getPath() + " lastModified= " + new Date(this.datasetManager.getLastScanned()));
            }
        }
        finally {
            if (channel != null) {
                channel.close();
            }
        }
    }
    
    @Override
    protected void persistRead() {
        if (AggregationExisting.diskCache2 == null) {
            return;
        }
        final String cacheName = this.getCacheName();
        if (cacheName == null) {
            return;
        }
        final File cacheFile = AggregationExisting.diskCache2.getCacheFile(cacheName);
        if (!cacheFile.exists()) {
            return;
        }
        if (AggregationExisting.logger.isDebugEnabled()) {
            AggregationExisting.logger.debug(" Try to Read cache " + cacheFile.getPath());
        }
        Element aggElem;
        try {
            aggElem = Parse.readRootElement("file:" + cacheFile.getPath());
        }
        catch (IOException e) {
            if (AggregationExisting.debugCache) {
                System.out.println(" No cache for " + cacheName + " - " + e.getMessage());
            }
            return;
        }
        final String version = aggElem.getAttributeValue("version");
        if (version == null || !version.equals("3")) {
            return;
        }
        final Map<String, Dataset> map = new HashMap<String, Dataset>();
        for (final Dataset ds : this.getDatasets()) {
            map.put(ds.getId(), ds);
        }
        final List<Element> ncList = (List<Element>)aggElem.getChildren("netcdf", NcMLReader.ncNS);
        for (final Element netcdfElemNested : ncList) {
            final String id = netcdfElemNested.getAttributeValue("id");
            final DatasetOuterDimension dod = map.get(id);
            if (null == dod) {
                if (!AggregationExisting.logger.isDebugEnabled()) {
                    continue;
                }
                AggregationExisting.logger.debug(" have cache but no dataset= " + id);
            }
            else {
                if (AggregationExisting.logger.isDebugEnabled()) {
                    AggregationExisting.logger.debug(" use cache for dataset= " + id);
                }
                if (dod.ncoord == 0) {
                    final String ncoordsS = netcdfElemNested.getAttributeValue("ncoords");
                    try {
                        dod.ncoord = Integer.parseInt(ncoordsS);
                        if (AggregationExisting.logger.isDebugEnabled()) {
                            AggregationExisting.logger.debug(" Read the cache; ncoords = " + dod.ncoord);
                        }
                    }
                    catch (NumberFormatException e3) {
                        AggregationExisting.logger.error("bad ncoord attribute on dataset=" + id);
                    }
                }
                final List<Element> cacheElemList = (List<Element>)netcdfElemNested.getChildren("cache", NcMLReader.ncNS);
                for (final Element cacheElemNested : cacheElemList) {
                    final String varName = cacheElemNested.getAttributeValue("varName");
                    final CacheVar pv = this.findCacheVariable(varName);
                    if (pv != null) {
                        final String sdata = cacheElemNested.getText();
                        if (sdata.length() == 0) {
                            continue;
                        }
                        if (AggregationExisting.logger.isDebugEnabled()) {
                            AggregationExisting.logger.debug(" read data for var = " + varName + " size= " + sdata.length());
                        }
                        final String[] vals = sdata.split(" ");
                        try {
                            final Array data = Array.makeArray(pv.dtype, vals);
                            pv.putData(id, data);
                        }
                        catch (Exception e2) {
                            AggregationExisting.logger.warn("Error reading cached data ", e2);
                        }
                    }
                    else {
                        AggregationExisting.logger.warn("not a cache var=" + varName);
                    }
                }
            }
        }
    }
    
    private String getCacheName() {
        String cacheName = this.ncDataset.getLocation();
        if (cacheName == null) {
            cacheName = this.ncDataset.getCacheName();
        }
        return cacheName;
    }
}
