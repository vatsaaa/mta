// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ncml;

import java.util.Date;
import ucar.ma2.Array;
import ucar.nc2.dataset.CoordinateAxis;
import ucar.nc2.dataset.CoordinateAxis1DTime;
import ucar.nc2.dt.GridCoordSystem;
import ucar.nc2.dt.GridDatatype;
import ucar.nc2.NCdump;
import ucar.nc2.dt.grid.GridDataset;
import thredds.inventory.CollectionManager;
import java.util.Formatter;
import java.util.Iterator;
import ucar.nc2.util.CancelTask;
import java.io.IOException;
import thredds.inventory.DateExtractor;
import thredds.inventory.DateExtractorFromName;
import ucar.nc2.ft.fmrc.Fmrc;
import ucar.nc2.dataset.NetcdfDataset;
import java.util.Set;

public class AggregationFmrc extends AggregationOuterDimension
{
    protected static Set<NetcdfDataset.Enhance> fmrcEnhanceMode;
    private boolean debug;
    private Fmrc fmrc;
    private String runMatcher;
    
    public AggregationFmrc(final NetcdfDataset ncd, final String dimName, final String recheckS) {
        super(ncd, dimName, Type.forecastModelRunCollection, recheckS);
        this.debug = false;
    }
    
    public void addDirectoryScanFmrc(final String dirName, final String suffix, final String regexpPatternString, final String subdirs, final String olderThan, final String runMatcher, final String forecastMatcher, final String offsetMatcher) throws IOException {
        this.runMatcher = runMatcher;
        this.isDate = true;
        this.datasetManager.addDirectoryScan(dirName, suffix, regexpPatternString, subdirs, olderThan, null);
        if (runMatcher != null) {
            final DateExtractor dateExtractor = new DateExtractorFromName(runMatcher, false);
            this.datasetManager.setDateExtractor(dateExtractor);
        }
    }
    
    @Override
    protected void makeDatasets(final CancelTask cancelTask) throws IOException {
        super.makeDatasets(cancelTask);
        for (final Dataset ds : this.datasets) {
            ds.enhance = AggregationFmrc.fmrcEnhanceMode;
        }
    }
    
    @Override
    public void getDetailInfo(final Formatter f) {
        super.getDetailInfo(f);
        if (this.runMatcher != null) {
            f.format("  runMatcher=%s%n", this.runMatcher);
        }
    }
    
    @Override
    protected void buildNetcdfDataset(final CancelTask cancelTask) throws IOException {
        DateExtractor dateExtractor = null;
        if (this.runMatcher != null) {
            dateExtractor = new DateExtractorFromName(this.runMatcher, false);
        }
        if (dateExtractor == null && this.dateFormatMark != null) {
            dateExtractor = new DateExtractorFromName(this.dateFormatMark, true);
        }
        (this.fmrc = new Fmrc(this.datasetManager)).getDataset2D(this.ncDataset);
    }
    
    @Override
    protected void rebuildDataset() throws IOException {
        throw new UnsupportedOperationException();
    }
    
    public static void main(final String[] arg) throws IOException {
        final String defaultFilename = "C:/data/rap/fmrc.xml";
        final String filename = (arg.length > 0) ? arg[0] : defaultFilename;
        final ucar.nc2.dt.GridDataset gds = GridDataset.open(filename);
        final GridDatatype gg = gds.findGridDatatype("T");
        final GridCoordSystem gsys = gg.getCoordinateSystem();
        final CoordinateAxis1DTime rtaxis = gsys.getRunTimeAxis();
        final CoordinateAxis taxis2D = gsys.getTimeAxis();
        final Array data = taxis2D.read();
        NCdump.printArray(data, "2D time array", System.out, null);
        System.out.println("Run Time, Valid Times");
        final Date[] runtimes = rtaxis.getTimeDates();
        for (int i = 0; i < runtimes.length; ++i) {
            System.out.println("\n" + runtimes[i]);
            final CoordinateAxis1DTime taxis = gsys.getTimeAxisForRun(i);
            final Date[] times = taxis.getTimeDates();
            for (int j = 0; j < times.length; ++j) {
                System.out.println("   " + times[j]);
            }
        }
    }
    
    static {
        AggregationFmrc.fmrcEnhanceMode = NetcdfDataset.getDefaultEnhanceMode();
    }
}
