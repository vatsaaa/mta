// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ncml;

import ucar.ma2.IndexIterator;
import ucar.ma2.Array;
import ucar.nc2.dataset.CoordinateAxis1D;
import ucar.ma2.ArrayChar;
import ucar.unidata.util.Parameter;
import ucar.ma2.DataType;
import org.jdom.Verifier;
import java.util.List;
import java.util.Iterator;
import ucar.nc2.dataset.CoordinateTransform;
import java.util.ArrayList;
import ucar.nc2.dataset.CoordinateSystem;
import ucar.nc2.dataset.VariableDS;
import ucar.nc2.dataset.CoordinateAxis;
import ucar.nc2.Variable;
import ucar.nc2.Attribute;
import org.jdom.Content;
import ucar.nc2.Dimension;
import thredds.catalog.XMLEntityResolver;
import org.jdom.Element;
import org.jdom.Document;
import java.io.IOException;
import org.jdom.output.XMLOutputter;
import org.jdom.output.Format;
import java.io.OutputStream;
import ucar.nc2.dataset.NetcdfDataset;
import org.jdom.Namespace;

public class NcMLGWriter
{
    protected static final Namespace ncNS;
    protected static final String schemaLocation = "http://www.unidata.ucar.edu/schemas/netcdf-cs.xsd";
    
    public void writeXML(final NetcdfDataset ncd, final OutputStream os, final boolean showCoords, final String uri) throws IOException {
        final XMLOutputter fmt = new XMLOutputter(Format.getPrettyFormat());
        fmt.output(this.makeDocument(ncd, showCoords, uri), os);
    }
    
    Document makeDocument(final NetcdfDataset ncd, final boolean showCoords, final String uri) {
        final Element rootElem = new Element("netcdf", NcMLGWriter.ncNS);
        final Document doc = new Document(rootElem);
        rootElem.addNamespaceDeclaration(NcMLGWriter.ncNS);
        rootElem.addNamespaceDeclaration(XMLEntityResolver.xsiNS);
        rootElem.setAttribute("schemaLocation", NcMLGWriter.ncNS.getURI() + " " + "http://www.unidata.ucar.edu/schemas/netcdf-cs.xsd", XMLEntityResolver.xsiNS);
        if (null != ncd.getId()) {
            rootElem.setAttribute("id", ncd.getId());
        }
        if (null != uri) {
            rootElem.setAttribute("uri", uri);
        }
        else {
            rootElem.setAttribute("uri", ncd.getLocation());
        }
        for (final Dimension dim : ncd.getDimensions()) {
            rootElem.addContent(this.makeDim(dim));
        }
        for (final Attribute att : ncd.getGlobalAttributes()) {
            rootElem.addContent(this.makeAttribute(att, "attribute"));
        }
        for (final Variable var : ncd.getVariables()) {
            if (var instanceof CoordinateAxis) {
                rootElem.addContent(this.makeCoordinateAxis((CoordinateAxis)var, showCoords));
            }
        }
        for (final Variable var : ncd.getVariables()) {
            if (!(var instanceof CoordinateAxis)) {
                rootElem.addContent(this.makeVariable((VariableDS)var));
            }
        }
        for (final CoordinateSystem cs : ncd.getCoordinateSystems()) {
            rootElem.addContent(this.makeCoordSys(cs));
        }
        final List<CoordinateTransform> coordTrans = new ArrayList<CoordinateTransform>();
        for (final CoordinateSystem cs2 : ncd.getCoordinateSystems()) {
            final List<CoordinateTransform> ctList = cs2.getCoordinateTransforms();
            if (ctList != null) {
                for (final CoordinateTransform ct : ctList) {
                    if (!coordTrans.contains(ct)) {
                        coordTrans.add(ct);
                    }
                }
            }
        }
        for (final CoordinateTransform coordTran : coordTrans) {
            rootElem.addContent(this.makeCoordTransform(coordTran));
        }
        return doc;
    }
    
    private Element makeAttribute(final Attribute att, final String elementName) {
        final Element attElem = new Element(elementName, NcMLGWriter.ncNS);
        attElem.setAttribute("name", att.getName());
        final DataType dt = att.getDataType();
        if (dt != null) {
            attElem.setAttribute("type", dt.toString());
        }
        if (att.isString()) {
            String value = att.getStringValue();
            final String err = Verifier.checkCharacterData(value);
            if (err != null) {
                value = "NcMLWriter invalid attribute value, err= " + err;
                System.out.println(value);
            }
            attElem.setAttribute("value", value);
        }
        else {
            final StringBuffer buff = new StringBuffer();
            for (int i = 0; i < att.getLength(); ++i) {
                final Number val = att.getNumericValue(i);
                if (i > 0) {
                    buff.append(" ");
                }
                buff.append(val.toString());
            }
            attElem.setAttribute("value", buff.toString());
        }
        return attElem;
    }
    
    private Element makeAttribute(final Parameter att, final String elementName) {
        final Element attElem = new Element(elementName, NcMLGWriter.ncNS);
        attElem.setAttribute("name", att.getName());
        if (att.isString()) {
            String value = att.getStringValue();
            final String err = Verifier.checkCharacterData(value);
            if (err != null) {
                value = "NcMLWriter invalid attribute value, err= " + err;
                System.out.println(value);
            }
            attElem.setAttribute("value", value);
        }
        else {
            attElem.setAttribute("type", "double");
            final StringBuffer buff = new StringBuffer();
            for (int i = 0; i < att.getLength(); ++i) {
                final double val = att.getNumericValue(i);
                if (i > 0) {
                    buff.append(" ");
                }
                buff.append(Double.toString(val));
            }
            attElem.setAttribute("value", buff.toString());
        }
        return attElem;
    }
    
    private Element makeCoordinateAxis(final CoordinateAxis var, final boolean showCoords) {
        final Element varElem = new Element("coordinateAxis", NcMLGWriter.ncNS);
        varElem.setAttribute("name", var.getName());
        final StringBuffer buff = new StringBuffer();
        final List dims = var.getDimensions();
        for (int i = 0; i < dims.size(); ++i) {
            final Dimension dim = dims.get(i);
            if (i > 0) {
                buff.append(" ");
            }
            buff.append(dim.getName());
        }
        if (buff.length() > 0) {
            varElem.setAttribute("shape", buff.toString());
        }
        final DataType dt = var.getDataType();
        varElem.setAttribute("type", dt.toString());
        for (final Attribute att : var.getAttributes()) {
            varElem.addContent(this.makeAttribute(att, "attribute"));
        }
        if (var.isMetadata() || (showCoords && var.getRank() <= 1)) {
            varElem.addContent(this.makeValues(var));
        }
        varElem.setAttribute("units", var.getUnitsString());
        if (var.getAxisType() != null) {
            varElem.setAttribute("axisType", var.getAxisType().toString());
        }
        final String positive = var.getPositive();
        if (positive != null) {
            varElem.setAttribute("positive", positive);
        }
        final String boundaryRef = var.getBoundaryRef();
        if (boundaryRef != null) {
            varElem.setAttribute("boundaryRef", boundaryRef);
        }
        final List csys = var.getCoordinateSystems();
        if (csys.size() > 0) {
            buff.setLength(0);
            for (int j = 0; j < csys.size(); ++j) {
                final CoordinateSystem cs = csys.get(j);
                if (j > 0) {
                    buff.append(" ");
                }
                buff.append(cs.getName());
            }
            varElem.setAttribute("coordinateSystems", buff.toString());
        }
        return varElem;
    }
    
    private Element makeCoordSys(final CoordinateSystem cs) {
        final Element csElem = new Element("coordinateSystem", NcMLGWriter.ncNS);
        csElem.setAttribute("name", cs.getName());
        for (final CoordinateAxis axis : cs.getCoordinateAxes()) {
            final Element axisElem = new Element("coordinateAxisRef", NcMLGWriter.ncNS);
            axisElem.setAttribute("ref", axis.getName());
            csElem.addContent(axisElem);
        }
        final List<CoordinateTransform> transforms = cs.getCoordinateTransforms();
        if (transforms != null) {
            for (final CoordinateTransform ct : transforms) {
                if (ct == null) {
                    continue;
                }
                final Element tElem = new Element("coordinateTransformRef", NcMLGWriter.ncNS);
                tElem.setAttribute("ref", ct.getName());
                csElem.addContent(tElem);
            }
        }
        return csElem;
    }
    
    private Element makeCoordTransform(final CoordinateTransform coordTransform) {
        final Element elem = new Element("coordinateTransform", NcMLGWriter.ncNS);
        elem.setAttribute("name", coordTransform.getName());
        elem.setAttribute("authority", coordTransform.getAuthority());
        if (coordTransform.getTransformType() != null) {
            elem.setAttribute("transformType", coordTransform.getTransformType().toString());
        }
        final List<Parameter> params = coordTransform.getParameters();
        for (final Parameter p : params) {
            elem.addContent(this.makeAttribute(p, "parameter"));
        }
        return elem;
    }
    
    private Element makeDim(final Dimension dim) {
        final Element dimElem = new Element("dimension", NcMLGWriter.ncNS);
        dimElem.setAttribute("name", dim.getName());
        dimElem.setAttribute("length", Integer.toString(dim.getLength()));
        if (dim.isUnlimited()) {
            dimElem.setAttribute("isUnlimited", "true");
        }
        return dimElem;
    }
    
    private Element makeVariable(final VariableDS var) {
        final Element varElem = new Element("variable", NcMLGWriter.ncNS);
        varElem.setAttribute("name", var.getName());
        final StringBuffer buff = new StringBuffer();
        final List dims = var.getDimensions();
        for (int i = 0; i < dims.size(); ++i) {
            final Dimension dim = dims.get(i);
            if (i > 0) {
                buff.append(" ");
            }
            buff.append(dim.getName());
        }
        if (buff.length() > 0) {
            varElem.setAttribute("shape", buff.toString());
        }
        final DataType dt = var.getDataType();
        if (dt != null) {
            varElem.setAttribute("type", dt.toString());
        }
        for (final Attribute att : var.getAttributes()) {
            varElem.addContent(this.makeAttribute(att, "attribute"));
        }
        if (var.isMetadata()) {
            varElem.addContent(this.makeValues(var));
        }
        final List csys = var.getCoordinateSystems();
        if (csys.size() > 0) {
            buff.setLength(0);
            for (int j = 0; j < csys.size(); ++j) {
                final CoordinateSystem cs = csys.get(j);
                if (j > 0) {
                    buff.append(" ");
                }
                buff.append(cs.getName());
            }
            varElem.setAttribute("coordinateSystems", buff.toString());
        }
        return varElem;
    }
    
    private Element makeValues(final VariableDS v) {
        final Element elem = new Element("values", NcMLGWriter.ncNS);
        final StringBuffer buff = new StringBuffer();
        Array a;
        try {
            a = v.read();
        }
        catch (IOException ioe) {
            return elem;
        }
        if (a instanceof ArrayChar) {
            final ArrayChar dataC = (ArrayChar)a;
            for (int i = 0; i < dataC.getShape()[0]; ++i) {
                if (i > 0) {
                    buff.append(" ");
                }
                buff.append("\"").append(dataC.getString(i)).append("\"");
            }
            elem.setText(buff.toString());
        }
        else if (v instanceof CoordinateAxis1D && ((CoordinateAxis1D)v).isRegular()) {
            final CoordinateAxis1D axis = (CoordinateAxis1D)v;
            elem.setAttribute("start", Double.toString(axis.getStart()));
            elem.setAttribute("increment", Double.toString(axis.getIncrement()));
            elem.setAttribute("npts", Long.toString(v.getSize()));
        }
        else {
            final boolean isDouble = v.getDataType() == DataType.DOUBLE;
            final boolean isFloat = v.getDataType() == DataType.FLOAT;
            final IndexIterator iter = a.getIndexIterator();
            while (iter.hasNext()) {
                if (isDouble) {
                    buff.append(iter.getDoubleNext());
                }
                else if (isFloat) {
                    buff.append(iter.getFloatNext());
                }
                else {
                    buff.append(iter.getIntNext());
                }
                buff.append(" ");
            }
            elem.setText(buff.toString());
        }
        return elem;
    }
    
    public static void main(final String[] arg) {
        final String urls = "C:/data/galeon/RUC.nc";
        try {
            final NetcdfDataset df = NetcdfDataset.openDataset(urls);
            final NcMLGWriter ncFactory = new NcMLGWriter();
            System.out.println("NetcdfDataset = " + urls + "\n" + df);
            System.out.println("-----------");
            ncFactory.writeXML(df, System.out, true, null);
        }
        catch (Exception ioe) {
            System.out.println("error = " + urls);
            ioe.printStackTrace();
        }
    }
    
    static {
        ncNS = Namespace.getNamespace("http://www.ucar.edu/schemas/netcdf");
    }
}
