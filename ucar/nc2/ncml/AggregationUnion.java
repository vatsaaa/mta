// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ncml;

import java.io.IOException;
import java.util.Iterator;
import ucar.nc2.dataset.ReplaceVariableCheck;
import ucar.nc2.dataset.DatasetConstructor;
import ucar.nc2.util.CancelTask;
import java.util.ArrayList;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.NetcdfFile;
import java.util.List;

public class AggregationUnion extends Aggregation
{
    private List<NetcdfFile> openDatasets;
    
    public AggregationUnion(final NetcdfDataset ncd, final String dimName, final String recheckS) {
        super(ncd, dimName, Type.union, recheckS);
        this.openDatasets = new ArrayList<NetcdfFile>();
    }
    
    @Override
    protected void buildNetcdfDataset(final CancelTask cancelTask) throws IOException {
        final List<Dataset> nestedDatasets = this.getDatasets();
        for (final Dataset vnested : nestedDatasets) {
            final NetcdfFile ncfile = vnested.acquireFile(cancelTask);
            DatasetConstructor.transferDataset(ncfile, this.ncDataset, null);
            this.openDatasets.add(ncfile);
        }
    }
    
    @Override
    protected void rebuildDataset() throws IOException {
        this.ncDataset.empty();
        this.buildNetcdfDataset(null);
    }
    
    @Override
    protected void closeDatasets() throws IOException {
        for (final NetcdfFile ncfile : this.openDatasets) {
            try {
                ncfile.close();
            }
            catch (IOException ex) {}
        }
        super.closeDatasets();
    }
}
