// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ncml;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import ucar.ma2.DataType;
import ucar.nc2.NetcdfFile;
import ucar.nc2.dataset.CoordinateAxis;
import ucar.nc2.Attribute;
import ucar.nc2.ProxyReader;
import ucar.nc2.Variable;
import ucar.nc2.Structure;
import ucar.nc2.dataset.VariableDS;
import ucar.nc2.Group;
import ucar.nc2.Dimension;
import ucar.nc2.dataset.ReplaceVariableCheck;
import ucar.nc2.dataset.DatasetConstructor;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;

public class AggregationNew extends AggregationOuterDimension
{
    public AggregationNew(final NetcdfDataset ncd, final String dimName, final String recheckS) {
        super(ncd, dimName, Type.joinNew, recheckS);
    }
    
    @Override
    protected void buildNetcdfDataset(final CancelTask cancelTask) throws IOException {
        this.buildCoords(cancelTask);
        final Dataset typicalDataset = this.getTypicalDataset();
        final NetcdfFile typical = typicalDataset.acquireFile(null);
        DatasetConstructor.transferDataset(typical, this.ncDataset, null);
        final String dimName = this.getDimensionName();
        final Dimension aggDim = new Dimension(dimName, this.getTotalCoords());
        this.ncDataset.removeDimension(null, dimName);
        this.ncDataset.addDimension(null, aggDim);
        this.promoteGlobalAttributes((DatasetOuterDimension)typicalDataset);
        final DataType coordType = this.getCoordinateType();
        final VariableDS joinAggCoord = new VariableDS(this.ncDataset, null, null, dimName, coordType, dimName, null, null);
        this.ncDataset.addVariable(null, joinAggCoord);
        joinAggCoord.setProxyReader(this);
        if (this.isDate) {
            joinAggCoord.addAttribute(new Attribute("_CoordinateAxisType", "Time"));
        }
        final CacheVar cv = new CoordValueVar(joinAggCoord.getName(), joinAggCoord.getDataType(), joinAggCoord.getUnitsString());
        joinAggCoord.setSPobject(cv);
        this.cacheList.add(cv);
        final List<String> aggVarNames = this.getAggVariableNames();
        if (aggVarNames.size() == 0) {
            for (final Variable v : typical.getVariables()) {
                if (!(v instanceof CoordinateAxis)) {
                    aggVarNames.add(v.getShortName());
                }
            }
        }
        for (final String varname : aggVarNames) {
            final Variable aggVar = this.ncDataset.findVariable(varname);
            if (aggVar == null) {
                AggregationNew.logger.error(this.ncDataset.getLocation() + " aggNewDimension cant find variable " + varname);
            }
            else {
                final Group newGroup = DatasetConstructor.findGroup(this.ncDataset, aggVar.getParentGroup());
                final VariableDS vagg = new VariableDS(this.ncDataset, newGroup, null, aggVar.getShortName(), aggVar.getDataType(), dimName + " " + aggVar.getDimensionsString(), null, null);
                vagg.setProxyReader(this);
                DatasetConstructor.transferVariableAttributes(aggVar, vagg);
                final Attribute att = vagg.findAttribute("_CoordinateAxes");
                if (att != null) {
                    final String axes = dimName + " " + att.getStringValue();
                    vagg.addAttribute(new Attribute("_CoordinateAxes", axes));
                }
                newGroup.removeVariable(aggVar.getShortName());
                newGroup.addVariable(vagg);
                this.aggVars.add(vagg);
                if (cancelTask != null && cancelTask.isCancel()) {
                    return;
                }
                continue;
            }
        }
        this.setDatasetAcquireProxy(typicalDataset, this.ncDataset);
        typicalDataset.close(typical);
    }
    
    private DataType getCoordinateType() {
        final List<Dataset> nestedDatasets = this.getDatasets();
        final DatasetOuterDimension first = nestedDatasets.get(0);
        return first.isStringValued ? DataType.STRING : DataType.DOUBLE;
    }
}
