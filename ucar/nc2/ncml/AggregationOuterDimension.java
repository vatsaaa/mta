// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ncml;

import ucar.nc2.Attribute;
import ucar.ma2.IndexIterator;
import ucar.nc2.units.DateUnit;
import ucar.ma2.Index;
import java.util.HashMap;
import java.util.Map;
import ucar.nc2.NetcdfFile;
import ucar.nc2.units.DateFromString;
import java.util.StringTokenizer;
import java.util.Date;
import java.util.Formatter;
import thredds.inventory.MFile;
import ucar.nc2.util.cache.FileFactory;
import java.util.EnumSet;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorCompletionService;
import ucar.ma2.InvalidRangeException;
import java.util.Collection;
import ucar.ma2.Range;
import ucar.ma2.MAMath;
import ucar.ma2.Section;
import ucar.nc2.Dimension;
import ucar.ma2.Array;
import ucar.nc2.Variable;
import ucar.nc2.Structure;
import ucar.nc2.Group;
import java.io.IOException;
import ucar.nc2.util.CancelTask;
import java.util.Iterator;
import ucar.ma2.DataType;
import java.util.ArrayList;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dataset.VariableDS;
import java.util.List;
import ucar.nc2.ProxyReader;

public abstract class AggregationOuterDimension extends Aggregation implements ProxyReader
{
    protected static boolean debugCache;
    protected static boolean debugInvocation;
    protected static boolean debugStride;
    public static int invocation;
    protected List<String> aggVarNames;
    protected List<VariableDS> aggVars;
    private int totalCoords;
    protected List<CacheVar> cacheList;
    protected boolean timeUnitsChange;
    
    protected AggregationOuterDimension(final NetcdfDataset ncd, final String dimName, final Type type, final String recheckS) {
        super(ncd, dimName, type, recheckS);
        this.aggVarNames = new ArrayList<String>();
        this.aggVars = new ArrayList<VariableDS>();
        this.totalCoords = 0;
        this.cacheList = new ArrayList<CacheVar>();
        this.timeUnitsChange = false;
    }
    
    void setTimeUnitsChange(final boolean timeUnitsChange) {
        this.timeUnitsChange = timeUnitsChange;
        if (timeUnitsChange) {
            this.isDate = true;
        }
    }
    
    public void addVariable(final String varName) {
        this.aggVarNames.add(varName);
    }
    
    void addVariableFromGlobalAttribute(final String varName, final String orgName) {
        this.cacheList.add(new PromoteVar(varName, orgName));
    }
    
    void addVariableFromGlobalAttributeCompose(final String varName, final String format, final String gattNames) {
        this.cacheList.add(new PromoteVarCompose(varName, format, gattNames));
    }
    
    void addCacheVariable(final String varName, final DataType dtype) {
        if (this.findCacheVariable(varName) != null) {
            return;
        }
        this.cacheList.add(new CacheVar(varName, dtype));
    }
    
    CacheVar findCacheVariable(final String varName) {
        for (final CacheVar cv : this.cacheList) {
            if (cv.varName.equals(varName)) {
                return cv;
            }
        }
        return null;
    }
    
    List<String> getAggVariableNames() {
        return this.aggVarNames;
    }
    
    protected void buildCoords(final CancelTask cancelTask) throws IOException {
        final List<Dataset> nestedDatasets = this.getDatasets();
        if (this.type == Type.forecastModelRunCollection) {
            for (final Dataset nested : nestedDatasets) {
                final DatasetOuterDimension dod = (DatasetOuterDimension)nested;
                dod.ncoord = 1;
            }
        }
        this.totalCoords = 0;
        for (final Dataset nested : nestedDatasets) {
            final DatasetOuterDimension dod = (DatasetOuterDimension)nested;
            this.totalCoords += dod.setStartEnd(this.totalCoords, cancelTask);
        }
    }
    
    protected int getTotalCoords() {
        return this.totalCoords;
    }
    
    protected void promoteGlobalAttributes(final DatasetOuterDimension typicalDataset) throws IOException {
        for (final CacheVar cv : this.cacheList) {
            if (!(cv instanceof PromoteVar)) {
                continue;
            }
            final PromoteVar pv = (PromoteVar)cv;
            final Array data = pv.read(typicalDataset);
            pv.dtype = DataType.getType(data.getElementType());
            final VariableDS promotedVar = new VariableDS(this.ncDataset, null, null, pv.varName, pv.dtype, this.dimName, null, null);
            this.ncDataset.addVariable(null, promotedVar);
            promotedVar.setProxyReader(this);
            promotedVar.setSPobject(pv);
        }
    }
    
    @Override
    protected void rebuildDataset() throws IOException {
        this.buildCoords(null);
        final Dimension aggDim = this.ncDataset.findDimension(this.dimName);
        aggDim.setLength(this.getTotalCoords());
        final VariableDS joinAggCoord = (VariableDS)this.ncDataset.getRootGroup().findVariable(this.dimName);
        joinAggCoord.setDimensions(this.dimName);
        joinAggCoord.invalidateCache();
        for (final Variable aggVar : this.aggVars) {
            aggVar.resetDimensions();
            aggVar.invalidateCache();
        }
        final Dataset typicalDataset = this.getTypicalDataset();
        for (final Variable var : this.ncDataset.getRootGroup().getVariables()) {
            final VariableDS varDS = (VariableDS)var;
            if (!this.aggVars.contains(varDS)) {
                if (this.dimName.equals(var.getName())) {
                    continue;
                }
                final DatasetProxyReader proxy = new DatasetProxyReader(typicalDataset);
                var.setProxyReader(proxy);
            }
        }
        for (final CacheVar cv : this.cacheList) {
            cv.reset();
        }
    }
    
    public Array reallyRead(final Variable mainv, final Section section, final CancelTask cancelTask) throws IOException, InvalidRangeException {
        if (AggregationOuterDimension.debugConvert && mainv instanceof VariableDS) {
            final DataType dtype = ((VariableDS)mainv).getOriginalDataType();
            if (dtype != null && dtype != mainv.getDataType()) {
                System.out.printf("Original type = %s mainv type= %s%n", dtype, mainv.getDataType());
            }
        }
        final long size = section.computeSize();
        if (size == mainv.getSize()) {
            return this.reallyRead(mainv, cancelTask);
        }
        final DataType dtype2 = (mainv instanceof VariableDS) ? ((VariableDS)mainv).getOriginalDataType() : mainv.getDataType();
        final Object spObj = mainv.getSPobject();
        if (spObj != null && spObj instanceof CacheVar) {
            final CacheVar pv = (CacheVar)spObj;
            final Array cacheArray = pv.read(section, cancelTask);
            return MAMath.convert(cacheArray, dtype2);
        }
        final Array sectionData = Array.factory(dtype2, section.getShape());
        int destPos = 0;
        final List<Range> ranges = section.getRanges();
        final Range joinRange = section.getRange(0);
        final List<Range> nestedSection = new ArrayList<Range>(ranges);
        final List<Range> innerSection = ranges.subList(1, ranges.size());
        if (AggregationOuterDimension.debug) {
            System.out.println("   agg wants range=" + mainv.getName() + "(" + joinRange + ")");
        }
        final List<Dataset> nestedDatasets = this.getDatasets();
        for (final Dataset nested : nestedDatasets) {
            final DatasetOuterDimension dod = (DatasetOuterDimension)nested;
            final Range nestedJoinRange = dod.getNestedJoinRange(joinRange);
            if (nestedJoinRange == null) {
                continue;
            }
            Array varData;
            if (this.type == Type.joinNew || this.type == Type.forecastModelRunCollection) {
                varData = dod.read(mainv, cancelTask, innerSection);
            }
            else {
                nestedSection.set(0, nestedJoinRange);
                varData = dod.read(mainv, cancelTask, nestedSection);
            }
            if (cancelTask != null && cancelTask.isCancel()) {
                return null;
            }
            varData = MAMath.convert(varData, dtype2);
            Array.arraycopy(varData, 0, sectionData, destPos, (int)varData.getSize());
            destPos += (int)varData.getSize();
        }
        return sectionData;
    }
    
    public Array reallyRead(final Variable mainv, final CancelTask cancelTask) throws IOException {
        if (AggregationOuterDimension.debugConvert && mainv instanceof VariableDS) {
            final DataType dtype = ((VariableDS)mainv).getOriginalDataType();
            if (dtype != null && dtype != mainv.getDataType()) {
                System.out.printf("Original type = %s mainv type= %s%n", dtype, mainv.getDataType());
            }
        }
        final DataType dtype = (mainv instanceof VariableDS) ? ((VariableDS)mainv).getOriginalDataType() : mainv.getDataType();
        final Object spObj = mainv.getSPobject();
        if (spObj != null && spObj instanceof CacheVar) {
            final CacheVar pv = (CacheVar)spObj;
            try {
                final Array cacheArray = pv.read(mainv.getShapeAsSection(), cancelTask);
                return MAMath.convert(cacheArray, dtype);
            }
            catch (InvalidRangeException e) {
                AggregationOuterDimension.logger.error("readAgg " + this.getLocation(), e);
                throw new IllegalArgumentException("readAgg " + this.getLocation(), e);
            }
        }
        final Array allData = Array.factory(dtype, mainv.getShape());
        int destPos = 0;
        final List<Dataset> nestedDatasets = this.getDatasets();
        if (AggregationOuterDimension.executor != null) {
            final CompletionService<Result> completionService = new ExecutorCompletionService<Result>(AggregationOuterDimension.executor);
            int count = 0;
            for (final Dataset vnested : nestedDatasets) {
                completionService.submit(new ReaderTask(vnested, mainv, cancelTask, count++));
            }
            try {
                for (int n = nestedDatasets.size(), i = 0; i < n; ++i) {
                    final Result r = completionService.take().get();
                    r.data = MAMath.convert(r.data, dtype);
                    if (r != null) {
                        final int size = (int)r.data.getSize();
                        Array.arraycopy(r.data, 0, allData, size * r.index, size);
                    }
                }
            }
            catch (InterruptedException e3) {
                Thread.currentThread().interrupt();
            }
            catch (ExecutionException e2) {
                throw new IOException(e2.getMessage());
            }
        }
        else {
            for (final Dataset vnested2 : nestedDatasets) {
                Array varData = vnested2.read(mainv, cancelTask);
                if (cancelTask != null && cancelTask.isCancel()) {
                    return null;
                }
                varData = MAMath.convert(varData, dtype);
                Array.arraycopy(varData, 0, allData, destPos, (int)varData.getSize());
                destPos += (int)varData.getSize();
            }
        }
        return allData;
    }
    
    @Override
    protected Dataset makeDataset(final String cacheName, final String location, final String id, final String ncoordS, final String coordValueS, final String sectionSpec, final EnumSet<NetcdfDataset.Enhance> enhance, final FileFactory reader) {
        return new DatasetOuterDimension(cacheName, location, id, ncoordS, coordValueS, enhance, reader);
    }
    
    @Override
    protected Dataset makeDataset(final MFile dset) {
        return new DatasetOuterDimension(dset);
    }
    
    @Override
    public void getDetailInfo(final Formatter f) {
        super.getDetailInfo(f);
        f.format("  timeUnitsChange=%s%n", this.timeUnitsChange);
        f.format("  totalCoords=%d%n", this.totalCoords);
        if (this.aggVarNames.size() > 0) {
            f.format("  Aggregation Variables specified in NcML%n", new Object[0]);
            for (final String vname : this.aggVarNames) {
                f.format("   %s%n", vname);
            }
        }
        f.format("%nAggregation Variables%n", new Object[0]);
        for (final VariableDS vds : this.aggVars) {
            f.format("   ", new Object[0]);
            vds.getNameAndDimensions(f, true, false);
            f.format("%n", new Object[0]);
        }
        if (this.cacheList.size() > 0) {
            f.format("%nCache Variables%n", new Object[0]);
            for (final CacheVar cv : this.cacheList) {
                f.format("   %s%n", cv);
            }
        }
        f.format("%nVariable Proxies%n", new Object[0]);
        for (final Variable v : this.ncDataset.getVariables()) {
            if (v.hasCachedData()) {
                f.format("   %20s cached%n", v.getShortName());
            }
            else {
                f.format("   %20s proxy %s%n", v.getShortName(), v.getProxyReader().getClass().getName());
            }
        }
    }
    
    public static void main(final String[] args) throws IOException {
        final String format = "%04d-%02d-%02dT%02d:%02d:%02.0f";
        final Formatter f = new Formatter();
        final Object[] vals = { new Integer(2002), new Integer(10), new Integer(20), new Integer(23), new Integer(0), new Float(2.1) };
        f.format(format, vals);
        System.out.println(f);
    }
    
    static {
        AggregationOuterDimension.debugCache = false;
        AggregationOuterDimension.debugInvocation = false;
        AggregationOuterDimension.debugStride = false;
        AggregationOuterDimension.invocation = 0;
    }
    
    private class ReaderTask implements Callable<Result>
    {
        Dataset ds;
        Variable mainv;
        CancelTask cancelTask;
        int index;
        
        ReaderTask(final Dataset ds, final Variable mainv, final CancelTask cancelTask, final int index) {
            this.ds = ds;
            this.mainv = mainv;
            this.cancelTask = cancelTask;
            this.index = index;
        }
        
        public Result call() throws Exception {
            final Array data = this.ds.read(this.mainv, this.cancelTask);
            return new Result(data, this.index);
        }
    }
    
    private class Result
    {
        Array data;
        int index;
        
        Result(final Array data, final int index) {
            this.data = data;
            this.index = index;
        }
    }
    
    class DatasetOuterDimension extends Dataset
    {
        protected int ncoord;
        protected String coordValue;
        protected Date coordValueDate;
        protected boolean isStringValued;
        private int aggStart;
        private int aggEnd;
        
        protected DatasetOuterDimension(final String location) {
            super(location);
            this.isStringValued = false;
            this.aggStart = 0;
            this.aggEnd = 0;
        }
        
        protected DatasetOuterDimension(final String cacheName, final String location, final String id, final String ncoordS, final String coordValueS, final EnumSet<NetcdfDataset.Enhance> enhance, final FileFactory reader) {
            super(cacheName, location, id, enhance, reader);
            this.isStringValued = false;
            this.aggStart = 0;
            this.aggEnd = 0;
            this.coordValue = coordValueS;
            if (AggregationOuterDimension.this.type == Type.joinNew || AggregationOuterDimension.this.type == Type.joinExistingOne) {
                this.ncoord = 1;
            }
            else if (ncoordS != null) {
                try {
                    this.ncoord = Integer.parseInt(ncoordS);
                }
                catch (NumberFormatException e) {
                    Aggregation.logger.error("bad ncoord attribute on dataset=" + location);
                }
            }
            if (AggregationOuterDimension.this.type == Type.joinNew || AggregationOuterDimension.this.type == Type.joinExistingOne || AggregationOuterDimension.this.type == Type.forecastModelRunCollection) {
                if (coordValueS == null) {
                    final int pos = this.location.lastIndexOf("/");
                    this.coordValue = ((pos < 0) ? this.location : this.location.substring(pos + 1));
                    this.isStringValued = true;
                }
                else {
                    try {
                        Double.parseDouble(coordValueS);
                    }
                    catch (NumberFormatException e) {
                        this.isStringValued = true;
                    }
                }
            }
            if (AggregationOuterDimension.this.type == Type.joinExisting && coordValueS != null) {
                final StringTokenizer stoker = new StringTokenizer(coordValueS, " ,");
                this.ncoord = stoker.countTokens();
            }
        }
        
        DatasetOuterDimension(final MFile cd) {
            super(cd);
            this.isStringValued = false;
            this.aggStart = 0;
            this.aggEnd = 0;
            if (AggregationOuterDimension.this.type == Type.joinNew || AggregationOuterDimension.this.type == Type.joinExistingOne) {
                this.ncoord = 1;
            }
            if (AggregationOuterDimension.this.type == Type.joinNew || AggregationOuterDimension.this.type == Type.joinExistingOne || AggregationOuterDimension.this.type == Type.forecastModelRunCollection) {
                final int pos = this.location.lastIndexOf("/");
                this.coordValue = ((pos < 0) ? this.location : this.location.substring(pos + 1));
                this.isStringValued = true;
            }
            if (null != AggregationOuterDimension.this.dateFormatMark) {
                final String filename = cd.getName();
                this.coordValueDate = DateFromString.getDateUsingDemarkatedCount(filename, AggregationOuterDimension.this.dateFormatMark, '#');
                this.coordValue = AggregationOuterDimension.this.dateFormatter.toDateTimeStringISO(this.coordValueDate);
                if (Aggregation.debugDateParse) {
                    System.out.println("  adding " + cd.getPath() + " date= " + this.coordValue);
                }
            }
            else if (Aggregation.debugDateParse) {
                System.out.println("  adding " + cd.getPath());
            }
            if (this.coordValue == null && AggregationOuterDimension.this.type == Type.joinNew) {
                this.coordValue = cd.getName();
            }
        }
        
        public String getCoordValueString() {
            return this.coordValue;
        }
        
        public Date getCoordValueDate() {
            return this.coordValueDate;
        }
        
        @Override
        public void show(final Formatter f) {
            f.format("   %s", this.location);
            if (this.coordValue != null) {
                f.format(" coordValue='%s'", this.coordValue);
            }
            if (this.coordValueDate != null) {
                f.format(" coordValueDate='%s'", AggregationOuterDimension.this.dateFormatter.toDateTimeString(this.coordValueDate));
            }
            f.format(" range=[%d:%d) (%d)%n", this.aggStart, this.aggEnd, this.ncoord);
        }
        
        public int getNcoords(final CancelTask cancelTask) throws IOException {
            if (this.ncoord <= 0) {
                NetcdfFile ncd = null;
                try {
                    ncd = this.acquireFile(cancelTask);
                    if (cancelTask != null && cancelTask.isCancel()) {
                        return 0;
                    }
                    final Dimension d = ncd.getRootGroup().findDimension(AggregationOuterDimension.this.dimName);
                    if (d == null) {
                        throw new IllegalArgumentException("Dimension not found= " + AggregationOuterDimension.this.dimName);
                    }
                    this.ncoord = d.getLength();
                }
                finally {
                    this.close(ncd);
                }
            }
            return this.ncoord;
        }
        
        protected int setStartEnd(final int aggStart, final CancelTask cancelTask) throws IOException {
            this.aggStart = aggStart;
            this.aggEnd = aggStart + this.getNcoords(cancelTask);
            return this.ncoord;
        }
        
        protected Range getNestedJoinRange(final Range totalRange) throws InvalidRangeException {
            final int wantStart = totalRange.first();
            final int wantStop = totalRange.last() + 1;
            if (!this.isNeeded(wantStart, wantStop)) {
                return null;
            }
            final int firstInInterval = totalRange.getFirstInInterval(this.aggStart);
            if (firstInInterval < 0 || firstInInterval >= this.aggEnd) {
                return null;
            }
            final int start = Math.max(firstInInterval, wantStart) - this.aggStart;
            final int stop = Math.min(this.aggEnd, wantStop) - this.aggStart;
            return new Range(start, stop - 1, totalRange.stride());
        }
        
        protected boolean isNeeded(final Range totalRange) {
            final int wantStart = totalRange.first();
            final int wantStop = totalRange.last() + 1;
            return this.isNeeded(wantStart, wantStop);
        }
        
        private boolean isNeeded(final int wantStart, final int wantStop) {
            return wantStart < wantStop && wantStart < this.aggEnd && wantStop > this.aggStart;
        }
        
        @Override
        protected void cacheVariables(final NetcdfFile ncfile) throws IOException {
            for (final CacheVar pv : AggregationOuterDimension.this.cacheList) {
                pv.read(this, ncfile);
            }
        }
        
        @Override
        protected Array read(final Variable mainv, final CancelTask cancelTask, List<Range> section) throws IOException, InvalidRangeException {
            NetcdfFile ncd = null;
            try {
                ncd = this.acquireFile(cancelTask);
                if (cancelTask != null && cancelTask.isCancel()) {
                    return null;
                }
                final Variable v = this.findVariable(ncd, mainv);
                if (v == null) {
                    Aggregation.logger.error("AggOuterDimension cant find " + mainv.getName() + " in " + ncd.getLocation() + "; return all zeroes!!!");
                    return Array.factory(mainv.getDataType(), new Section(section).getShape());
                }
                if (Aggregation.debugRead) {
                    final Section want = new Section(section);
                    System.out.printf("AggOuter.read(%s) %s from %s in %s%n", want, mainv.getNameAndDimensions(), v.getNameAndDimensions(), this.getLocation());
                }
                final Range fullRange = v.getRanges().get(0);
                final Range want2 = section.get(0);
                if (fullRange.last() < want2.last()) {
                    final Range limitRange = new Range(want2.first(), fullRange.last(), want2.stride());
                    section = new ArrayList<Range>(section);
                    section.set(0, limitRange);
                }
                return v.read(section);
            }
            finally {
                this.close(ncd);
            }
        }
        
        @Override
        public int compareTo(final Object o) {
            if (this.coordValueDate == null) {
                return super.compareTo(o);
            }
            final DatasetOuterDimension other = (DatasetOuterDimension)o;
            return this.coordValueDate.compareTo(other.coordValueDate);
        }
    }
    
    class CacheVar
    {
        String varName;
        DataType dtype;
        private Map<String, Array> dataMap;
        
        CacheVar(final String varName, final DataType dtype) {
            this.dataMap = new HashMap<String, Array>();
            this.varName = varName;
            this.dtype = dtype;
            if (varName == null) {
                throw new IllegalArgumentException("Missing variable name on cache var");
            }
        }
        
        @Override
        public String toString() {
            return this.varName + " (" + this.getClass().getName() + ")";
        }
        
        void reset() {
            final Map<String, Array> newMap = new HashMap<String, Array>();
            for (final Dataset ds : AggregationOuterDimension.this.datasets) {
                final String id = ds.getId();
                final Array data = this.dataMap.get(id);
                if (data != null) {
                    newMap.put(id, data);
                }
            }
            this.dataMap = newMap;
        }
        
        Array read(final Section section, final CancelTask cancelTask) throws IOException, InvalidRangeException {
            if (AggregationOuterDimension.debugCache) {
                System.out.println("caching " + this.varName + " section= " + section);
            }
            Array allData = null;
            final List<Range> ranges = section.getRanges();
            final Range joinRange = section.getRange(0);
            Section innerSection = null;
            if (section.getRank() > 1) {
                innerSection = new Section(ranges.subList(1, ranges.size()));
            }
            int resultPos = 0;
            final List<Dataset> nestedDatasets = AggregationOuterDimension.this.getDatasets();
            for (final Dataset vnested : nestedDatasets) {
                final DatasetOuterDimension dod = (DatasetOuterDimension)vnested;
                final Range nestedJoinRange = dod.getNestedJoinRange(joinRange);
                if (nestedJoinRange == null) {
                    continue;
                }
                if (AggregationOuterDimension.debugStride) {
                    System.out.printf("%d: %s [%d,%d) (%d) %f for %s%n", resultPos, nestedJoinRange, dod.aggStart, dod.aggEnd, dod.ncoord, dod.aggStart / 8.0, vnested.getLocation());
                }
                Array varData = this.read(dod);
                if ((AggregationOuterDimension.this.type == Type.joinNew || AggregationOuterDimension.this.type == Type.forecastModelRunCollection) && innerSection != null && varData.getSize() != innerSection.computeSize()) {
                    varData = varData.section(innerSection.getRanges());
                }
                else if (innerSection == null && varData.getSize() != nestedJoinRange.length()) {
                    final List<Range> nestedSection = new ArrayList<Range>(ranges);
                    nestedSection.set(0, nestedJoinRange);
                    varData = varData.section(nestedSection);
                }
                if (this.dtype == null) {
                    this.dtype = DataType.getType(varData.getElementType());
                }
                if (allData == null) {
                    allData = Array.factory(this.dtype, section.getShape());
                    if (AggregationOuterDimension.debugStride) {
                        System.out.printf("total result section = %s (%d)%n", section, Index.computeSize(section.getShape()));
                    }
                }
                final int nelems = (int)varData.getSize();
                Array.arraycopy(varData, 0, allData, resultPos, nelems);
                resultPos += nelems;
                if (cancelTask != null && cancelTask.isCancel()) {
                    return null;
                }
            }
            return allData;
        }
        
        protected void putData(final String id, final Array data) {
            this.dataMap.put(id, data);
        }
        
        protected Array getData(final String id) {
            return this.dataMap.get(id);
        }
        
        protected Array read(final DatasetOuterDimension dset) throws IOException {
            final Array data = this.getData(dset.getId());
            if (data != null) {
                return data;
            }
            if (AggregationOuterDimension.this.type == Type.joinNew) {
                return null;
            }
            NetcdfFile ncfile = null;
            try {
                ncfile = dset.acquireFile(null);
                return this.read(dset, ncfile);
            }
            finally {
                if (ncfile != null) {
                    ncfile.close();
                }
            }
        }
        
        protected Array read(final DatasetOuterDimension dset, final NetcdfFile ncfile) throws IOException {
            ++AggregationOuterDimension.invocation;
            Array data = this.getData(dset.getId());
            if (data != null) {
                return data;
            }
            if (AggregationOuterDimension.this.type == Type.joinNew) {
                return null;
            }
            final Variable v = ncfile.findVariable(this.varName);
            data = v.read();
            this.putData(dset.getId(), data);
            if (AggregationOuterDimension.debugCache) {
                System.out.println("caching " + this.varName + " complete data");
            }
            return data;
        }
    }
    
    class CoordValueVar extends CacheVar
    {
        String units;
        DateUnit du;
        
        CoordValueVar(final String varName, final DataType dtype, final String units) {
            super(varName, dtype);
            this.units = units;
            try {
                this.du = new DateUnit(units);
            }
            catch (Exception ex) {}
        }
        
        @Override
        protected Array read(final DatasetOuterDimension dset) throws IOException {
            final Array data = this.readCached(dset);
            if (data != null) {
                return data;
            }
            return super.read(dset);
        }
        
        @Override
        protected Array read(final DatasetOuterDimension dset, final NetcdfFile ncfile) throws IOException {
            final Array data = this.readCached(dset);
            if (data != null) {
                return data;
            }
            return super.read(dset, ncfile);
        }
        
        private Array readCached(final DatasetOuterDimension dset) throws IOException {
            Array data = this.getData(dset.getId());
            if (data != null) {
                return data;
            }
            data = Array.factory(this.dtype, new int[] { dset.ncoord });
            final IndexIterator ii = data.getIndexIterator();
            if (dset.coordValueDate != null) {
                if (this.dtype == DataType.STRING) {
                    ii.setObjectNext(dset.coordValue);
                }
                else if (this.du != null) {
                    final double val = this.du.makeValue(dset.coordValueDate);
                    ii.setDoubleNext(val);
                }
                this.putData(dset.getId(), data);
                return data;
            }
            if (dset.coordValue != null) {
                if (dset.ncoord == 1) {
                    if (this.dtype == DataType.STRING) {
                        ii.setObjectNext(dset.coordValue);
                    }
                    else {
                        final double val = Double.parseDouble(dset.coordValue);
                        ii.setDoubleNext(val);
                    }
                }
                else {
                    int count = 0;
                    final StringTokenizer stoker = new StringTokenizer(dset.coordValue, " ,");
                    while (stoker.hasMoreTokens()) {
                        final String toke = stoker.nextToken();
                        if (this.dtype == DataType.STRING) {
                            ii.setObjectNext(toke);
                        }
                        else {
                            final double val2 = Double.parseDouble(toke);
                            ii.setDoubleNext(val2);
                        }
                        ++count;
                    }
                    if (count != dset.ncoord) {
                        Aggregation.logger.error("readAggCoord incorrect number of coordinates dataset=" + dset.getLocation());
                        throw new IllegalArgumentException("readAggCoord incorrect number of coordinates dataset=" + dset.getLocation());
                    }
                }
                this.putData(dset.getId(), data);
                return data;
            }
            return null;
        }
    }
    
    class PromoteVar extends CacheVar
    {
        String gattName;
        
        protected PromoteVar(final String varName, final DataType dtype) {
            super(varName, dtype);
        }
        
        PromoteVar(final String varName, final String gattName) {
            super(varName, null);
            this.gattName = ((gattName != null) ? gattName : varName);
        }
        
        @Override
        protected Array read(final DatasetOuterDimension dset, final NetcdfFile ncfile) throws IOException {
            Array data = this.getData(dset.getId());
            if (data != null) {
                return data;
            }
            final Attribute att = ncfile.findGlobalAttribute(this.gattName);
            if (att == null) {
                throw new IllegalArgumentException("Unknown attribute name= " + this.gattName);
            }
            data = att.getValues();
            if (this.dtype == null) {
                this.dtype = DataType.getType(data.getElementType());
            }
            if (dset.ncoord == 1) {
                this.putData(dset.getId(), data);
            }
            else {
                final Array allData = Array.factory(this.dtype, new int[] { dset.ncoord });
                for (int i = 0; i < dset.ncoord; ++i) {
                    Array.arraycopy(data, 0, allData, i, 1);
                }
                this.putData(dset.getId(), allData);
                data = allData;
            }
            return data;
        }
    }
    
    class PromoteVarCompose extends PromoteVar
    {
        String varName;
        String format;
        String[] gattNames;
        
        PromoteVarCompose(final String varName, final String format, final String gattNames) {
            super(varName, DataType.STRING);
            this.format = format;
            this.gattNames = gattNames.split(" ");
            if (format == null) {
                throw new IllegalArgumentException("Missing format string (java.util.Formatter)");
            }
        }
        
        @Override
        protected Array read(final DatasetOuterDimension dset, final NetcdfFile ncfile) throws IOException {
            final Array data = this.getData(dset.getId());
            if (data != null) {
                return data;
            }
            final List<Object> vals = new ArrayList<Object>();
            for (final String gattName : this.gattNames) {
                final Attribute att = ncfile.findGlobalAttribute(gattName);
                if (att == null) {
                    throw new IllegalArgumentException("Unknown attribute name= " + gattName);
                }
                vals.add(att.getValue(0));
            }
            final Formatter f = new Formatter();
            f.format(this.format, vals.toArray());
            final String result = f.toString();
            final Array allData = Array.factory(this.dtype, new int[] { dset.ncoord });
            for (int i = 0; i < dset.ncoord; ++i) {
                allData.setObject(i, result);
            }
            this.putData(dset.getId(), allData);
            return allData;
        }
    }
}
