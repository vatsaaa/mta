// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ncml;

import ucar.ma2.InvalidRangeException;
import ucar.ma2.Section;
import ucar.ma2.Range;
import ucar.ma2.Array;
import ucar.unidata.util.StringUtil;
import org.slf4j.LoggerFactory;
import ucar.nc2.dataset.VariableEnhanced;
import ucar.nc2.ProxyReader;
import ucar.nc2.Variable;
import java.util.Random;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Collections;
import thredds.inventory.MFile;
import java.util.Iterator;
import ucar.nc2.NetcdfFile;
import ucar.nc2.util.CancelTask;
import java.io.IOException;
import java.util.Formatter;
import thredds.inventory.DateExtractor;
import thredds.inventory.DateExtractorFromName;
import java.util.Set;
import java.util.EnumSet;
import ucar.nc2.util.cache.FileFactory;
import java.util.ArrayList;
import ucar.nc2.units.DateFormatter;
import org.jdom.Element;
import thredds.inventory.DatasetCollectionManager;
import java.util.List;
import ucar.nc2.dataset.NetcdfDataset;
import java.util.concurrent.Executor;
import ucar.nc2.util.DiskCache2;
import org.slf4j.Logger;

public abstract class Aggregation
{
    protected static TypicalDataset typicalDatasetMode;
    protected static Logger logger;
    protected static DiskCache2 diskCache2;
    protected static Executor executor;
    protected static boolean debug;
    protected static boolean debugOpenFile;
    protected static boolean debugSyncDetail;
    protected static boolean debugProxy;
    protected static boolean debugRead;
    protected static boolean debugDateParse;
    protected static boolean debugConvert;
    protected NetcdfDataset ncDataset;
    protected Type type;
    protected Object spiObject;
    protected List<Dataset> explicitDatasets;
    protected List<Dataset> datasets;
    protected DatasetCollectionManager datasetManager;
    protected boolean cacheDirty;
    protected String dimName;
    private Element mergeNcml;
    protected String dateFormatMark;
    protected boolean isDate;
    protected DateFormatter dateFormatter;
    
    public static void setPersistenceCache(final DiskCache2 dc) {
        Aggregation.diskCache2 = dc;
    }
    
    public static void setExecutor(final Executor exec) {
        Aggregation.executor = exec;
    }
    
    public static void setTypicalDatasetMode(final String mode) {
        if (mode.equalsIgnoreCase("random")) {
            Aggregation.typicalDatasetMode = TypicalDataset.RANDOM;
        }
        else if (mode.equalsIgnoreCase("latest")) {
            Aggregation.typicalDatasetMode = TypicalDataset.LATEST;
        }
        else if (mode.equalsIgnoreCase("penultimate")) {
            Aggregation.typicalDatasetMode = TypicalDataset.PENULTIMATE;
        }
        else if (mode.equalsIgnoreCase("first")) {
            Aggregation.typicalDatasetMode = TypicalDataset.FIRST;
        }
        else {
            Aggregation.logger.error("Unknown setTypicalDatasetMode= " + mode);
        }
    }
    
    protected Aggregation(final NetcdfDataset ncd, final String dimName, final Type type, final String recheckS) {
        this.explicitDatasets = new ArrayList<Dataset>();
        this.datasets = new ArrayList<Dataset>();
        this.cacheDirty = true;
        this.mergeNcml = null;
        this.isDate = false;
        this.dateFormatter = new DateFormatter();
        this.ncDataset = ncd;
        this.dimName = dimName;
        this.type = type;
        this.datasetManager = new DatasetCollectionManager(recheckS);
    }
    
    public void addExplicitDataset(final String cacheName, final String location, final String id, final String ncoordS, final String coordValueS, final String sectionSpec, final FileFactory reader) {
        final Dataset nested = this.makeDataset(cacheName, location, id, ncoordS, coordValueS, sectionSpec, null, reader);
        this.explicitDatasets.add(nested);
    }
    
    public void addDataset(final Dataset nested) {
        this.explicitDatasets.add(nested);
    }
    
    public void addDatasetScan(final Element crawlableDatasetElement, final String dirName, final String suffix, final String regexpPatternString, final String dateFormatMark, final Set<NetcdfDataset.Enhance> enhanceMode, final String subdirs, final String olderThan) {
        this.dateFormatMark = dateFormatMark;
        this.datasetManager.addDirectoryScan(dirName, suffix, regexpPatternString, subdirs, olderThan, enhanceMode);
        if (dateFormatMark != null) {
            this.isDate = true;
            if (this.type == Type.joinExisting) {
                this.type = Type.joinExistingOne;
            }
            final DateExtractor dateExtractor = (dateFormatMark == null) ? null : new DateExtractorFromName(dateFormatMark, true);
            this.datasetManager.setDateExtractor(dateExtractor);
        }
    }
    
    public void addCollection(final String spec, final String olderThan) throws IOException {
        this.datasetManager = DatasetCollectionManager.open(spec, olderThan, null);
    }
    
    public void setModifications(final Element ncmlMods) {
        this.mergeNcml = ncmlMods;
    }
    
    public Type getType() {
        return this.type;
    }
    
    public String getDimensionName() {
        return this.dimName;
    }
    
    protected String getLocation() {
        return this.ncDataset.getLocation();
    }
    
    public void close() throws IOException {
        this.persistWrite();
        this.closeDatasets();
    }
    
    public synchronized boolean syncExtend() throws IOException {
        return this.datasetManager.isRescanNeeded() && this._sync();
    }
    
    public synchronized boolean sync() throws IOException {
        return this.datasetManager.isRescanNeeded() && this._sync();
    }
    
    private boolean _sync() throws IOException {
        if (!this.datasetManager.rescan()) {
            return false;
        }
        this.cacheDirty = true;
        this.closeDatasets();
        this.makeDatasets(null);
        this.rebuildDataset();
        this.ncDataset.finish();
        if (this.ncDataset.getEnhanceMode().contains(NetcdfDataset.Enhance.CoordSystems)) {
            this.ncDataset.clearCoordinateSystems();
            this.ncDataset.enhance(this.ncDataset.getEnhanceMode());
            this.ncDataset.finish();
        }
        return true;
    }
    
    public String getFileTypeId() {
        Dataset ds = null;
        NetcdfFile ncfile = null;
        try {
            ds = this.getTypicalDataset();
            ncfile = ds.acquireFile(null);
            return ncfile.getFileTypeId();
        }
        catch (Exception e) {
            Aggregation.logger.error("failed to open " + ds);
            if (ds != null) {
                try {
                    ds.close(ncfile);
                }
                catch (IOException e2) {
                    Aggregation.logger.error("failed to close " + ds);
                }
            }
        }
        finally {
            if (ds != null) {
                try {
                    ds.close(ncfile);
                }
                catch (IOException e3) {
                    Aggregation.logger.error("failed to close " + ds);
                }
            }
        }
        return "N/A";
    }
    
    public String getFileTypeDescription() {
        Dataset ds = null;
        NetcdfFile ncfile = null;
        try {
            ds = this.getTypicalDataset();
            ncfile = ds.acquireFile(null);
            return ncfile.getFileTypeDescription();
        }
        catch (Exception e) {
            Aggregation.logger.error("failed to open " + ds);
            if (ds != null) {
                try {
                    ds.close(ncfile);
                }
                catch (IOException e2) {
                    Aggregation.logger.error("failed to close " + ds);
                }
            }
        }
        finally {
            if (ds != null) {
                try {
                    ds.close(ncfile);
                }
                catch (IOException e3) {
                    Aggregation.logger.error("failed to close " + ds);
                }
            }
        }
        return "N/A";
    }
    
    protected abstract void buildNetcdfDataset(final CancelTask p0) throws IOException;
    
    protected abstract void rebuildDataset() throws IOException;
    
    public void persistWrite() throws IOException {
    }
    
    protected void persistRead() {
    }
    
    protected void closeDatasets() throws IOException {
    }
    
    public void getDetailInfo(final Formatter f) {
        f.format("  Type=%s%n", this.type);
        f.format("  dimName=%s%n", this.dimName);
        f.format("  Datasets%n", new Object[0]);
        for (final Dataset ds : this.datasets) {
            ds.show(f);
        }
    }
    
    public void finish(final CancelTask cancelTask) throws IOException {
        this.datasetManager.scan(cancelTask);
        this.cacheDirty = true;
        this.closeDatasets();
        this.makeDatasets(cancelTask);
        this.buildNetcdfDataset(cancelTask);
    }
    
    public List<Dataset> getDatasets() {
        return this.datasets;
    }
    
    protected void makeDatasets(final CancelTask cancelTask) throws IOException {
        this.datasets = new ArrayList<Dataset>();
        for (final MFile cd : this.datasetManager.getFiles()) {
            this.datasets.add(this.makeDataset(cd));
        }
        Collections.sort(this.datasets);
        for (final Dataset dataset : this.explicitDatasets) {
            this.datasets.add(dataset);
        }
        final Set<String> dset = new HashSet<String>(2 * this.datasets.size());
        for (final Dataset dataset2 : this.datasets) {
            if (dset.contains(dataset2.cacheLocation)) {
                Aggregation.logger.warn("Duplicate dataset in aggregation = " + dataset2.cacheLocation);
            }
            dset.add(dataset2.cacheLocation);
        }
        if (this.datasets.size() == 0) {
            throw new IllegalStateException("There are no datasets in the aggregation");
        }
    }
    
    protected Dataset getTypicalDataset() throws IOException {
        final List<Dataset> nestedDatasets = this.getDatasets();
        final int n = nestedDatasets.size();
        if (n == 0) {
            throw new FileNotFoundException("No datasets in this aggregation");
        }
        int select;
        if (Aggregation.typicalDatasetMode == TypicalDataset.LATEST) {
            select = n - 1;
        }
        else if (Aggregation.typicalDatasetMode == TypicalDataset.PENULTIMATE) {
            select = ((n < 2) ? 0 : (n - 2));
        }
        else if (Aggregation.typicalDatasetMode == TypicalDataset.FIRST) {
            select = 0;
        }
        else {
            select = ((n < 2) ? 0 : new Random().nextInt(n));
        }
        return nestedDatasets.get(select);
    }
    
    protected Dataset makeDataset(final String cacheName, final String location, final String id, final String ncoordS, final String coordValueS, final String sectionSpec, final EnumSet<NetcdfDataset.Enhance> enhance, final FileFactory reader) {
        return new Dataset(cacheName, location, id, enhance, reader);
    }
    
    protected Dataset makeDataset(final MFile dset) {
        return new Dataset(dset);
    }
    
    protected void setDatasetAcquireProxy(final Dataset typicalDataset, final NetcdfDataset newds) throws IOException {
        final DatasetProxyReader proxy = new DatasetProxyReader(typicalDataset);
        final List<Variable> allVars = newds.getRootGroup().getVariables();
        for (final Variable v : allVars) {
            if (v.getProxyReader() != v) {
                if (!Aggregation.debugProxy) {
                    continue;
                }
                System.out.println(" debugProxy: hasProxyReader " + v.getName());
            }
            else if (v.isCaching()) {
                v.setCachedData(v.read());
            }
            else {
                v.setProxyReader(proxy);
                if (!Aggregation.debugProxy) {
                    continue;
                }
                System.out.println(" debugProxy: set proxy on " + v.getName());
            }
        }
    }
    
    protected Variable findVariable(final NetcdfFile ncfile, final Variable mainV) {
        Variable v = ncfile.findVariable(mainV.getName());
        if (v == null) {
            final VariableEnhanced ve = (VariableEnhanced)mainV;
            v = ncfile.findVariable(ve.getOriginalName());
        }
        return v;
    }
    
    static {
        Aggregation.logger = LoggerFactory.getLogger(Aggregation.class);
        Aggregation.diskCache2 = null;
        Aggregation.debug = false;
        Aggregation.debugOpenFile = false;
        Aggregation.debugSyncDetail = false;
        Aggregation.debugProxy = false;
        Aggregation.debugRead = false;
        Aggregation.debugDateParse = false;
        Aggregation.debugConvert = false;
    }
    
    protected enum Type
    {
        forecastModelRunCollection, 
        forecastModelRunSingleCollection, 
        joinExisting, 
        joinExistingOne, 
        joinNew, 
        tiled, 
        union;
    }
    
    protected enum TypicalDataset
    {
        FIRST, 
        RANDOM, 
        LATEST, 
        PENULTIMATE;
    }
    
    public class Dataset implements Comparable
    {
        protected String location;
        protected String id;
        protected String cacheLocation;
        protected FileFactory reader;
        protected Set<NetcdfDataset.Enhance> enhance;
        protected Object extraInfo;
        
        protected Dataset(final String location) {
            this.location = ((location == null) ? null : StringUtil.substitute(location, "\\", "/"));
        }
        
        protected Dataset(final Aggregation aggregation, final MFile mfile) {
            this(aggregation, mfile.getPath());
            this.cacheLocation = this.location;
            this.enhance = (Set<NetcdfDataset.Enhance>)mfile.getAuxInfo();
        }
        
        protected Dataset(final Aggregation aggregation, final String cacheLocation, final String location, final String id, final EnumSet<NetcdfDataset.Enhance> enhance, final FileFactory reader) {
            this(aggregation, location);
            this.cacheLocation = cacheLocation;
            this.id = id;
            this.reader = reader;
        }
        
        public String getLocation() {
            return this.location;
        }
        
        public String getCacheLocation() {
            return this.cacheLocation;
        }
        
        public String getId() {
            if (this.id != null) {
                return this.id;
            }
            if (this.location != null) {
                return this.location;
            }
            return Integer.toString(this.hashCode());
        }
        
        public NetcdfFile acquireFile(final CancelTask cancelTask) throws IOException {
            if (Aggregation.debugOpenFile) {
                System.out.println(" try to acquire " + this.cacheLocation);
            }
            final long start = System.currentTimeMillis();
            NetcdfFile ncfile = NetcdfDataset.acquireFile(this.reader, null, this.cacheLocation, -1, cancelTask, Aggregation.this.spiObject);
            if (Aggregation.this.mergeNcml != null) {
                ncfile = NcMLReader.mergeNcML(ncfile, Aggregation.this.mergeNcml);
            }
            if (this.enhance == null || this.enhance.isEmpty()) {
                if (Aggregation.debugOpenFile) {
                    System.out.println(" acquire (no enhance) " + this.cacheLocation + " took " + (System.currentTimeMillis() - start));
                }
                return ncfile;
            }
            NetcdfDataset ds;
            if (ncfile instanceof NetcdfDataset) {
                ds = (NetcdfDataset)ncfile;
                ds.enhance(this.enhance);
            }
            else {
                ds = new NetcdfDataset(ncfile, this.enhance);
            }
            if (Aggregation.debugOpenFile) {
                System.out.println(" acquire (enhance) " + this.cacheLocation + " took " + (System.currentTimeMillis() - start));
            }
            return ds;
        }
        
        protected void close(final NetcdfFile ncfile) throws IOException {
            if (ncfile == null) {
                return;
            }
            this.cacheVariables(ncfile);
            ncfile.close();
        }
        
        protected void cacheVariables(final NetcdfFile ncfile) throws IOException {
        }
        
        public void show(final Formatter f) {
            f.format("   %s%n", this.location);
        }
        
        protected Array read(final Variable mainv, final CancelTask cancelTask) throws IOException {
            NetcdfFile ncd = null;
            try {
                ncd = this.acquireFile(cancelTask);
                if (cancelTask != null && cancelTask.isCancel()) {
                    return null;
                }
                final Variable v = this.findVariable(ncd, mainv);
                if (mainv == null || v == null) {
                    System.out.println("HEY (mainv == null)");
                }
                if (Aggregation.debugRead) {
                    System.out.printf("Agg.read %s from %s in %s%n", mainv.getNameAndDimensions(), v.getNameAndDimensions(), this.getLocation());
                }
                return v.read();
            }
            finally {
                this.close(ncd);
            }
        }
        
        protected Array read(final Variable mainv, final CancelTask cancelTask, final List<Range> section) throws IOException, InvalidRangeException {
            NetcdfFile ncd = null;
            try {
                ncd = this.acquireFile(cancelTask);
                if (cancelTask != null && cancelTask.isCancel()) {
                    return null;
                }
                final Variable v = this.findVariable(ncd, mainv);
                if (Aggregation.debugRead) {
                    final Section want = new Section(section);
                    System.out.printf("Agg.read(%s) %s from %s in %s%n", want, mainv.getNameAndDimensions(), v.getNameAndDimensions(), this.getLocation());
                }
                return v.read(section);
            }
            finally {
                this.close(ncd);
            }
        }
        
        protected Variable findVariable(final NetcdfFile ncfile, final Variable mainV) {
            Variable v = ncfile.findVariable(mainV.getName());
            if (v == null) {
                final VariableEnhanced ve = (VariableEnhanced)mainV;
                v = ncfile.findVariable(ve.getOriginalName());
            }
            return v;
        }
        
        @Override
        public boolean equals(final Object oo) {
            if (this == oo) {
                return true;
            }
            if (!(oo instanceof Dataset)) {
                return false;
            }
            final Dataset other = (Dataset)oo;
            if (this.location != null) {
                return this.location.equals(other.location);
            }
            return super.equals(oo);
        }
        
        @Override
        public int hashCode() {
            return (this.location != null) ? this.location.hashCode() : super.hashCode();
        }
        
        public int compareTo(final Object o) {
            final Dataset other = (Dataset)o;
            return this.location.compareTo(other.location);
        }
    }
    
    protected class DatasetProxyReader implements ProxyReader
    {
        Dataset dataset;
        
        DatasetProxyReader(final Dataset dataset) {
            this.dataset = dataset;
        }
        
        public Array reallyRead(final Variable mainV, final CancelTask cancelTask) throws IOException {
            NetcdfFile ncfile = null;
            try {
                ncfile = this.dataset.acquireFile(cancelTask);
                if (cancelTask != null && cancelTask.isCancel()) {
                    return null;
                }
                final Variable proxyV = Aggregation.this.findVariable(ncfile, mainV);
                return proxyV.read();
            }
            finally {
                this.dataset.close(ncfile);
            }
        }
        
        public Array reallyRead(final Variable mainV, final Section section, final CancelTask cancelTask) throws IOException, InvalidRangeException {
            NetcdfFile ncfile = null;
            try {
                ncfile = this.dataset.acquireFile(cancelTask);
                final Variable proxyV = Aggregation.this.findVariable(ncfile, mainV);
                if (cancelTask != null && cancelTask.isCancel()) {
                    return null;
                }
                return proxyV.read(section);
            }
            finally {
                this.dataset.close(ncfile);
            }
        }
    }
}
