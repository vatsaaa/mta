// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.ncml;

import org.slf4j.LoggerFactory;
import ucar.ma2.Index;
import ucar.ma2.IndexIterator;
import ucar.ma2.Array;
import ucar.nc2.util.Misc;
import java.util.List;
import ucar.nc2.Structure;
import java.util.Iterator;
import ucar.nc2.dataset.VariableEnhanced;
import ucar.nc2.Dimension;
import ucar.ma2.DataType;
import ucar.nc2.Attribute;
import ucar.nc2.Group;
import ucar.nc2.util.URLnaming;
import org.jdom.Document;
import org.jdom.Content;
import org.jdom.Element;
import java.io.OutputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import org.jdom.output.Format;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;
import org.jdom.output.XMLOutputter;
import ucar.nc2.dataset.NetcdfDataset;
import org.slf4j.Logger;
import org.jdom.Namespace;

public class NcMLWriter
{
    protected static final Namespace ncNS;
    protected static final Namespace xsiNS;
    private static Logger log;
    private NetcdfDataset ncd;
    private XMLOutputter fmt;
    private Variable aggCoord;
    
    public String writeXML(final NetcdfFile ncfile) throws IOException {
        if (ncfile instanceof NetcdfDataset) {
            this.ncd = (NetcdfDataset)ncfile;
        }
        else {
            this.ncd = new NetcdfDataset(ncfile, false);
        }
        this.fmt = new XMLOutputter(Format.getPrettyFormat());
        return this.fmt.outputString(this.makeDocument(null));
    }
    
    public String writeXML(final Variable v) throws IOException {
        this.fmt = new XMLOutputter(Format.getPrettyFormat());
        return this.fmt.outputString(this.makeDocument(null));
    }
    
    public void writeXML(final NetcdfFile ncfile, final String filenameOut) throws IOException {
        final OutputStream out = new BufferedOutputStream(new FileOutputStream(filenameOut, false));
        this.writeXML(ncfile, out, null);
        out.close();
    }
    
    public void writeXML(final NetcdfFile ncfile, final OutputStream os, final String location) throws IOException {
        if (ncfile instanceof NetcdfDataset) {
            this.ncd = (NetcdfDataset)ncfile;
        }
        else {
            this.ncd = new NetcdfDataset(ncfile, false);
        }
        (this.fmt = new XMLOutputter(Format.getPrettyFormat())).output(this.makeDocument(location), os);
    }
    
    public void writeXMLexplicit(final NetcdfFile ncfile, final OutputStream os, final String location) throws IOException {
        if (ncfile instanceof NetcdfDataset) {
            this.ncd = (NetcdfDataset)ncfile;
        }
        else {
            this.ncd = new NetcdfDataset(ncfile, false);
        }
        this.fmt = new XMLOutputter(Format.getPrettyFormat());
        final Document doc = this.makeDocument(location);
        final Element root = doc.getRootElement();
        root.addContent(new Element("explicit", NcMLWriter.ncNS));
        this.fmt.output(doc, os);
    }
    
    private Document makeDocument(String location) throws IOException {
        final Element rootElem = new Element("netcdf", NcMLWriter.ncNS);
        final Document doc = new Document(rootElem);
        rootElem.addNamespaceDeclaration(NcMLWriter.ncNS);
        if (null == location) {
            location = this.ncd.getLocation();
        }
        if (null != location) {
            rootElem.setAttribute("location", URLnaming.canonicalizeWrite(location));
        }
        if (null != this.ncd.getId()) {
            rootElem.setAttribute("id", this.ncd.getId());
        }
        if (null != this.ncd.getTitle()) {
            rootElem.setAttribute("title", this.ncd.getTitle());
        }
        final Aggregation agg = this.ncd.getAggregation();
        if (agg != null) {
            final String aggDimensionName = agg.getDimensionName();
            this.aggCoord = this.ncd.findVariable(aggDimensionName);
        }
        final Group rootGroup = this.ncd.getRootGroup();
        this.writeGroup(rootElem, rootGroup);
        return doc;
    }
    
    public static Element writeAttribute(final Attribute att, final String elementName, final Namespace ns) {
        final Element attElem = new Element(elementName, ns);
        attElem.setAttribute("name", att.getName());
        final DataType dt = att.getDataType();
        if (dt != null && dt != DataType.STRING) {
            attElem.setAttribute("type", dt.toString());
        }
        if (att.isString()) {
            final String value = att.getStringValue();
            attElem.setAttribute("value", value);
        }
        else {
            final StringBuilder buff = new StringBuilder();
            for (int i = 0; i < att.getLength(); ++i) {
                final Number val = att.getNumericValue(i);
                if (i > 0) {
                    buff.append(" ");
                }
                buff.append(val.toString());
            }
            attElem.setAttribute("value", buff.toString());
        }
        return attElem;
    }
    
    public static Element writeDimension(final Dimension dim, final Namespace ns) {
        final Element dimElem = new Element("dimension", ns);
        dimElem.setAttribute("name", dim.getName());
        dimElem.setAttribute("length", Integer.toString(dim.getLength()));
        if (dim.isUnlimited()) {
            dimElem.setAttribute("isUnlimited", "true");
        }
        if (dim.isVariableLength()) {
            dimElem.setAttribute("isVariableLength", "true");
        }
        return dimElem;
    }
    
    private Element writeGroup(final Element elem, final Group group) {
        for (final Dimension dim : group.getDimensions()) {
            elem.addContent(writeDimension(dim, NcMLWriter.ncNS));
        }
        for (final Attribute att : group.getAttributes()) {
            elem.addContent(writeAttribute(att, "attribute", NcMLWriter.ncNS));
        }
        for (final Variable var : group.getVariables()) {
            try {
                elem.addContent(this.writeVariable((VariableEnhanced)var));
            }
            catch (ClassCastException e) {
                NcMLWriter.log.error("var not instanceof VariableEnhanced = " + var.getName(), e);
            }
        }
        for (final Group g : group.getGroups()) {
            final Element groupElem = new Element("group", NcMLWriter.ncNS);
            groupElem.setAttribute("name", g.getShortName());
            elem.addContent(this.writeGroup(groupElem, g));
        }
        return elem;
    }
    
    private Element writeVariable(final VariableEnhanced var) {
        final boolean isStructure = var instanceof Structure;
        final Element varElem = new Element("variable", NcMLWriter.ncNS);
        varElem.setAttribute("name", var.getShortName());
        final StringBuilder buff = new StringBuilder();
        final List dims = var.getDimensions();
        for (int i = 0; i < dims.size(); ++i) {
            final Dimension dim = dims.get(i);
            if (i > 0) {
                buff.append(" ");
            }
            if (dim.isShared()) {
                buff.append(dim.getName());
            }
            else {
                buff.append(dim.getLength());
            }
        }
        varElem.setAttribute("shape", buff.toString());
        final DataType dt = var.getDataType();
        if (dt != null) {
            varElem.setAttribute("type", dt.toString());
        }
        for (final Attribute att : var.getAttributes()) {
            varElem.addContent(writeAttribute(att, "attribute", NcMLWriter.ncNS));
        }
        if (var.isMetadata() || var == this.aggCoord) {
            varElem.addContent(writeValues(var, NcMLWriter.ncNS, true));
        }
        if (isStructure) {
            final Structure s = (Structure)var;
            for (final Variable variable : s.getVariables()) {
                final VariableEnhanced nestedV = (VariableEnhanced)variable;
                varElem.addContent(this.writeVariable(nestedV));
            }
        }
        return varElem;
    }
    
    public static Element writeValues(final VariableEnhanced v, final Namespace ns, final boolean allowRegular) {
        final Element elem = new Element("values", ns);
        final StringBuilder buff = new StringBuilder();
        Array a;
        try {
            a = v.read();
        }
        catch (IOException ioe) {
            return elem;
        }
        if (v.getDataType() == DataType.CHAR) {
            final char[] data = (char[])a.getStorage();
            elem.setText(new String(data));
        }
        else if (v.getDataType() == DataType.STRING) {
            final IndexIterator iter = a.getIndexIterator();
            int count = 0;
            while (iter.hasNext()) {
                final String s = (String)iter.getObjectNext();
                if (count++ > 0) {
                    buff.append(" ");
                }
                buff.append("\"").append(s).append("\"");
            }
            elem.setText(buff.toString());
        }
        else {
            if (allowRegular && a.getRank() == 1 && a.getSize() > 2L) {
                final Index ima = a.getIndex();
                final double start = a.getDouble(ima.set(0));
                final double incr = a.getDouble(ima.set(1)) - start;
                boolean isRegular = true;
                for (int i = 2; i < a.getSize(); ++i) {
                    final double v2 = a.getDouble(ima.set(i));
                    final double v3 = a.getDouble(ima.set(i - 1));
                    if (!Misc.closeEnough(v2 - v3, incr)) {
                        isRegular = false;
                    }
                }
                if (isRegular) {
                    elem.setAttribute("start", Double.toString(start));
                    elem.setAttribute("increment", Double.toString(incr));
                    elem.setAttribute("npts", Long.toString(v.getSize()));
                    return elem;
                }
            }
            final boolean isRealType = v.getDataType() == DataType.DOUBLE || v.getDataType() == DataType.FLOAT;
            final IndexIterator iter2 = a.getIndexIterator();
            buff.append(isRealType ? iter2.getDoubleNext() : iter2.getIntNext());
            while (iter2.hasNext()) {
                buff.append(" ");
                buff.append(isRealType ? iter2.getDoubleNext() : iter2.getIntNext());
            }
            elem.setText(buff.toString());
        }
        return elem;
    }
    
    public static void main(final String[] arg) {
        final String test = "C:/data/atd/rgg.20020411.000000.lel.ll.nc";
        final String urls = (arg.length == 0) ? test : arg[0];
        try {
            final NetcdfDataset df = NetcdfDataset.openDataset(urls);
            final NcMLWriter writer = new NcMLWriter();
            System.out.println("NetcdfDataset = " + urls + "\n" + df);
            System.out.println("-----------");
            writer.writeXML(df, System.out, null);
        }
        catch (Exception ioe) {
            System.out.println("error = " + urls);
            ioe.printStackTrace();
        }
    }
    
    static {
        ncNS = Namespace.getNamespace("http://www.unidata.ucar.edu/namespaces/netcdf/ncml-2.2");
        xsiNS = Namespace.getNamespace("xsi", "http://www.w3.org/2001/XMLSchema");
        NcMLWriter.log = LoggerFactory.getLogger(NcMLWriter.class);
    }
}
