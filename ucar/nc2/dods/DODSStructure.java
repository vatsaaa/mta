// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dods;

import ucar.ma2.StructureData;
import ucar.ma2.ArrayStructure;
import ucar.ma2.StructureDataIterator;
import ucar.nc2.Variable;
import java.util.List;
import opendap.dap.DArray;
import java.io.IOException;
import java.util.Iterator;
import ucar.nc2.Dimension;
import opendap.dap.DSequence;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Group;
import opendap.dap.DConstructor;
import ucar.nc2.Structure;

public class DODSStructure extends Structure
{
    private DConstructor ds;
    protected DODSNetcdfFile dodsfile;
    protected String dodsShortName;
    
    DODSStructure(final DODSNetcdfFile dodsfile, final Group parentGroup, final Structure parentStructure, final String dodsShortName, final DodsV dodsV) throws IOException {
        super(dodsfile, parentGroup, parentStructure, DODSNetcdfFile.makeNetcdfName(dodsShortName));
        this.dodsfile = dodsfile;
        this.ds = (DConstructor)dodsV.bt;
        this.dodsShortName = dodsShortName;
        if (this.ds instanceof DSequence) {
            this.dimensions.add(Dimension.VLEN);
            this.shape = new int[1];
        }
        else {
            this.shape = new int[0];
        }
        for (final DodsV nested : dodsV.children) {
            dodsfile.addVariable(parentGroup, this, nested);
        }
        if (this.ds instanceof DSequence) {
            this.isVariableLength = true;
        }
        this.setSPobject(dodsV);
    }
    
    DODSStructure(final DODSNetcdfFile dodsfile, final Group parentGroup, final Structure parentStructure, final String shortName, final DArray dodsArray, final DodsV dodsV) throws IOException {
        this(dodsfile, parentGroup, parentStructure, shortName, dodsV);
        final List<Dimension> dims = dodsfile.constructDimensions(parentGroup, dodsArray);
        this.setDimensions(dims);
        this.setSPobject(dodsV);
    }
    
    private DODSStructure(final DODSStructure from) {
        super(from);
        this.dodsfile = from.dodsfile;
        this.dodsShortName = from.dodsShortName;
        this.ds = from.ds;
    }
    
    @Override
    protected Variable copy() {
        return new DODSStructure(this);
    }
    
    DConstructor getDConstructor() {
        return this.ds;
    }
    
    protected String getDODSshortName() {
        return this.dodsShortName;
    }
    
    public StructureDataIterator getStructureIterator(final String CE) throws IOException {
        return new SequenceIterator(CE);
    }
    
    private class SequenceIterator implements StructureDataIterator
    {
        private int nrows;
        private int row;
        private ArrayStructure structArray;
        
        SequenceIterator(final String CE) throws IOException {
            this.row = 0;
            this.structArray = (ArrayStructure)DODSStructure.this.read();
            this.nrows = (int)this.structArray.getSize();
        }
        
        public boolean hasNext() {
            return this.row < this.nrows;
        }
        
        public StructureData next() {
            return this.structArray.getStructureData(this.row++);
        }
        
        public void setBufferSize(final int bytes) {
        }
        
        public StructureDataIterator reset() {
            this.row = 0;
            return this;
        }
        
        public int getCurrentRecno() {
            return this.row - 1;
        }
    }
}
