// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dods;

import org.slf4j.LoggerFactory;
import java.util.Enumeration;
import opendap.dap.DArrayDimension;
import ucar.ma2.Index;
import opendap.dap.DArray;
import opendap.dap.BytePrimitiveVector;
import opendap.dap.Int16PrimitiveVector;
import opendap.dap.Int32PrimitiveVector;
import opendap.dap.Float64PrimitiveVector;
import opendap.dap.Float32PrimitiveVector;
import opendap.dap.UInt16PrimitiveVector;
import opendap.dap.UInt32PrimitiveVector;
import opendap.dap.DByte;
import opendap.dap.DInt16;
import opendap.dap.DInt32;
import opendap.dap.DFloat64;
import opendap.dap.DFloat32;
import opendap.dap.DUInt16;
import opendap.dap.DUInt32;
import ucar.ma2.StructureData;
import opendap.dap.BaseTypePrimitiveVector;
import java.util.Vector;
import opendap.dap.BaseType;
import java.util.Iterator;
import ucar.ma2.ArrayStructureMA;
import opendap.dap.PrimitiveVector;
import ucar.ma2.IndexIterator;
import opendap.dap.DVector;
import opendap.dap.DSequence;
import opendap.dap.DGrid;
import opendap.dap.DConstructor;
import opendap.dap.DStructure;
import ucar.ma2.DataType;
import opendap.dap.DString;
import opendap.dap.DAP2Exception;
import java.io.IOException;
import ucar.ma2.StructureMembers;
import ucar.ma2.ArraySequenceNested;
import java.util.ArrayList;
import ucar.ma2.ArrayStructure;
import ucar.ma2.Array;
import ucar.ma2.Range;
import java.util.List;
import ucar.nc2.Variable;
import org.slf4j.Logger;

public class ConvertD2N
{
    private static Logger logger;
    
    public Array convertNestedVariable(final Variable v, final List<Range> section, final DodsV dataV, final boolean flatten) throws IOException, DAP2Exception {
        final Array data = this.convertTopVariable(v, section, dataV);
        if (!flatten) {
            return data;
        }
        final ArrayStructure as = (ArrayStructure)data;
        final List<String> names = new ArrayList<String>();
        for (Variable nested = v; nested.isMemberOfStructure(); nested = nested.getParentStructure()) {
            names.add(0, nested.getShortName());
        }
        final StructureMembers.Member m = this.findNested(as, names, v.getShortName());
        final Array mdata = m.getDataArray();
        if (mdata instanceof ArraySequenceNested) {
            final ArraySequenceNested arraySeq = (ArraySequenceNested)mdata;
            return arraySeq.flatten();
        }
        return mdata;
    }
    
    private StructureMembers.Member findNested(final ArrayStructure as, final List<String> names, final String want) {
        final String name = names.get(0);
        final StructureMembers sm = as.getStructureMembers();
        final StructureMembers.Member m = sm.findMember(name);
        if (name.equals(want)) {
            return m;
        }
        final ArrayStructure nested = (ArrayStructure)m.getDataArray();
        names.remove(0);
        return this.findNested(nested, names, want);
    }
    
    public Array convertTopVariable(final Variable v, final List<Range> section, final DodsV dataV) throws IOException, DAP2Exception {
        final Array data = this.convert(dataV);
        if (dataV.darray != null && dataV.bt instanceof DString) {
            if (v.getDataType() == DataType.STRING) {
                return this.convertStringArray(data, v);
            }
            if (v.getDataType() == DataType.CHAR) {
                return this.convertStringArrayToChar(dataV.darray, v, section);
            }
            final String mess = "DODSVariable convertArray String invalid dataType= " + v.getDataType();
            ConvertD2N.logger.error(mess);
            throw new IllegalArgumentException(mess);
        }
        else {
            if (dataV.bt instanceof DString && v.getDataType() == DataType.CHAR) {
                return this.convertStringToChar(data, v);
            }
            return data;
        }
    }
    
    public Array convert(final DodsV dataV) throws IOException, DAP2Exception {
        if (dataV.darray == null) {
            if (dataV.bt instanceof DStructure) {
                final ArrayStructure structArray = this.makeArrayStructure(dataV);
                this.iconvertDataStructure((DConstructor)dataV.bt, structArray.getStructureMembers());
                return structArray;
            }
            if (dataV.bt instanceof DGrid) {
                throw new IllegalStateException("DGrid without a darray");
            }
            if (dataV.bt instanceof DSequence) {
                final ArrayStructure structArray = this.makeArrayStructure(dataV);
                this.iconvertDataSequenceArray((DSequence)dataV.bt, structArray.getStructureMembers());
                return structArray;
            }
            final DataType dtype = DODSNetcdfFile.convertToNCType(dataV.elemType);
            final Array scalarData = Array.factory(dtype, new int[0]);
            final IndexIterator scalarIndex = scalarData.getIndexIterator();
            this.iconvertDataPrimitiveScalar(dataV.bt, scalarIndex);
            return scalarData;
        }
        else {
            if (dataV.darray == null) {
                final String mess = "Unknown baseType " + dataV.bt.getClass().getName() + " name=" + dataV.getName();
                ConvertD2N.logger.error(mess);
                throw new IllegalStateException(mess);
            }
            if (dataV.bt instanceof DStructure) {
                final ArrayStructure structArray = this.makeArrayStructure(dataV);
                this.iconvertDataStructureArray((DVector)dataV.darray, structArray.getStructureMembers());
                return structArray;
            }
            if (dataV.bt instanceof DString) {
                return this.convertStringArray(dataV.darray);
            }
            final PrimitiveVector pv = dataV.darray.getPrimitiveVector();
            final Object storage = pv.getInternalStorage();
            final DataType dtype2 = DODSNetcdfFile.convertToNCType(dataV.elemType);
            return Array.factory(dtype2.getPrimitiveClassType(), this.makeShape(dataV.darray), storage);
        }
    }
    
    private ArrayStructure makeArrayStructure(final DodsV dataV) {
        final StructureMembers members = new StructureMembers(dataV.getNetcdfShortName());
        for (final DodsV dodsV : dataV.children) {
            final StructureMembers.Member m = members.addMember(dodsV.getNetcdfShortName(), null, null, dodsV.getDataType(), dodsV.getShape());
            Array data;
            if (dodsV.bt instanceof DStructure || dodsV.bt instanceof DGrid) {
                data = this.makeArrayStructure(dodsV);
            }
            else if (dodsV.bt instanceof DSequence) {
                data = this.makeArrayNestedSequence(dodsV);
                m.setShape(data.getShape());
            }
            else {
                data = Array.factory(dodsV.getDataType(), dodsV.getShapeAll());
            }
            m.setDataArray(data);
            m.setDataObject(data.getIndexIterator());
        }
        return new ArrayStructureMA(members, dataV.getShapeAll());
    }
    
    private ArrayStructure makeArrayNestedSequence(final DodsV dataV) {
        final StructureMembers members = new StructureMembers(dataV.getName());
        for (final DodsV dodsV : dataV.children) {
            members.addMember(dodsV.getNetcdfShortName(), null, null, dodsV.getDataType(), dodsV.getShape());
        }
        final DSequence outerSeq = (DSequence)dataV.parent.bt;
        final int outerLength = outerSeq.getRowCount();
        final ArraySequenceNested aseq = new ArraySequenceNested(members, outerLength);
        final String name = dataV.getName();
        for (int row = 0; row < outerLength; ++row) {
            final Vector dv = outerSeq.getRow(row);
            for (int j = 0; j < dv.size(); ++j) {
                final BaseType bt = dv.elementAt(j);
                if (bt.getName().equals(name)) {
                    final DSequence innerSeq = (DSequence)bt;
                    final int innerLength = innerSeq.getRowCount();
                    aseq.setSequenceLength(row, innerLength);
                }
            }
        }
        aseq.finish();
        final List<StructureMembers.Member> memberList = members.getMembers();
        for (final StructureMembers.Member m : memberList) {
            final Array data = m.getDataArray();
            m.setDataObject(data.getIndexIterator());
        }
        return aseq;
    }
    
    private void iconvertDataStructureArray(final DVector darray, final StructureMembers members) throws DAP2Exception {
        final List<StructureMembers.Member> mlist = members.getMembers();
        for (final StructureMembers.Member member : mlist) {
            final String name = member.getName();
            final IndexIterator ii = (IndexIterator)member.getDataObject();
            final BaseTypePrimitiveVector pv = (BaseTypePrimitiveVector)darray.getPrimitiveVector();
            for (int row = 0; row < pv.getLength(); ++row) {
                final DStructure ds_data = (DStructure)pv.getValue(row);
                final BaseType member_data = ds_data.getVariable(name);
                this.iconvertData(member_data, ii);
            }
        }
    }
    
    private void iconvertDataSequenceArray(final DSequence dseq, final StructureMembers members) throws DAP2Exception {
        for (int row = 0; row < dseq.getRowCount(); ++row) {
            final Vector dv = dseq.getRow(row);
            for (int j = 0; j < dv.size(); ++j) {
                final BaseType member_data = dv.elementAt(j);
                final StructureMembers.Member member = members.findMember(member_data.getName());
                final IndexIterator ii = (IndexIterator)member.getDataObject();
                this.iconvertData(member_data, ii);
            }
        }
    }
    
    private void iconvertDataStructure(final DConstructor ds, final StructureMembers members) throws DAP2Exception {
        final List<StructureMembers.Member> mlist = members.getMembers();
        for (final StructureMembers.Member member : mlist) {
            final IndexIterator ii = (IndexIterator)member.getDataObject();
            final String dodsName;
            final String name = dodsName = member.getName();
            if (dodsName == null) {
                throw new DAP2Exception("Cant find dodsName for member variable " + name);
            }
            final BaseType bt = ds.getVariable(dodsName);
            this.iconvertData(bt, ii);
        }
    }
    
    private void iconvertData(final BaseType dodsVar, final IndexIterator ii) throws DAP2Exception {
        if (dodsVar instanceof DSequence) {
            final DSequence dseq = (DSequence)dodsVar;
            final StructureData sd = (StructureData)ii.getObjectNext();
            this.iconvertDataSequenceArray(dseq, sd.getStructureMembers());
        }
        else if (dodsVar instanceof DStructure || dodsVar instanceof DGrid) {
            final DConstructor ds = (DConstructor)dodsVar;
            final StructureData sd = (StructureData)ii.getObjectNext();
            this.iconvertDataStructure(ds, sd.getStructureMembers());
        }
        else if (dodsVar instanceof DVector) {
            final DVector da = (DVector)dodsVar;
            final BaseType bt = da.getPrimitiveVector().getTemplate();
            if (bt instanceof DStructure) {
                final StructureData sd2 = (StructureData)ii.getObjectNext();
                this.iconvertDataStructureArray(da, sd2.getStructureMembers());
            }
            else {
                if (bt instanceof DGrid) {
                    throw new UnsupportedOperationException();
                }
                if (bt instanceof DSequence) {
                    throw new UnsupportedOperationException();
                }
                this.iconvertDataPrimitiveArray(da.getPrimitiveVector(), ii);
            }
        }
        else {
            this.iconvertDataPrimitiveScalar(dodsVar, ii);
        }
    }
    
    private void iconvertDataPrimitiveScalar(final BaseType dodsScalar, final IndexIterator ii) {
        if (dodsScalar instanceof DString) {
            final String sval = ((DString)dodsScalar).getValue();
            ii.setObjectNext(sval);
        }
        else if (dodsScalar instanceof DUInt32) {
            final int ival = ((DUInt32)dodsScalar).getValue();
            final long lval = DataType.unsignedIntToLong(ival);
            ii.setLongNext(lval);
        }
        else if (dodsScalar instanceof DUInt16) {
            final short sval2 = ((DUInt16)dodsScalar).getValue();
            final int ival2 = DataType.unsignedShortToInt(sval2);
            ii.setIntNext(ival2);
        }
        else if (dodsScalar instanceof DFloat32) {
            ii.setFloatNext(((DFloat32)dodsScalar).getValue());
        }
        else if (dodsScalar instanceof DFloat64) {
            ii.setDoubleNext(((DFloat64)dodsScalar).getValue());
        }
        else if (dodsScalar instanceof DInt32) {
            ii.setIntNext(((DInt32)dodsScalar).getValue());
        }
        else if (dodsScalar instanceof DInt16) {
            ii.setShortNext(((DInt16)dodsScalar).getValue());
        }
        else {
            if (!(dodsScalar instanceof DByte)) {
                throw new IllegalArgumentException("DODSVariable extractScalar invalid dataType= " + dodsScalar.getClass().getName());
            }
            ii.setByteNext(((DByte)dodsScalar).getValue());
        }
    }
    
    private void iconvertDataPrimitiveArray(final PrimitiveVector pv, final IndexIterator ii) {
        final BaseType bt = pv.getTemplate();
        if (bt instanceof DString) {
            final BaseTypePrimitiveVector bpv = (BaseTypePrimitiveVector)pv;
            for (int row = 0; row < bpv.getLength(); ++row) {
                final DString ds = (DString)bpv.getValue(row);
                ii.setObjectNext(ds.getValue());
            }
        }
        else if (bt instanceof DUInt32) {
            final UInt32PrimitiveVector bpv2 = (UInt32PrimitiveVector)pv;
            for (int row = 0; row < bpv2.getLength(); ++row) {
                final int ival = bpv2.getValue(row);
                final long lval = DataType.unsignedIntToLong(ival);
                ii.setLongNext(lval);
            }
        }
        else if (bt instanceof DUInt16) {
            final UInt16PrimitiveVector bpv3 = (UInt16PrimitiveVector)pv;
            for (int row = 0; row < bpv3.getLength(); ++row) {
                final short sval = bpv3.getValue(row);
                final int ival2 = DataType.unsignedShortToInt(sval);
                ii.setIntNext(ival2);
            }
        }
        else if (bt instanceof DFloat32) {
            final Float32PrimitiveVector bpv4 = (Float32PrimitiveVector)pv;
            for (int row = 0; row < bpv4.getLength(); ++row) {
                ii.setFloatNext(bpv4.getValue(row));
            }
        }
        else if (bt instanceof DFloat64) {
            final Float64PrimitiveVector bpv5 = (Float64PrimitiveVector)pv;
            for (int row = 0; row < bpv5.getLength(); ++row) {
                ii.setDoubleNext(bpv5.getValue(row));
            }
        }
        else if (bt instanceof DInt32) {
            final Int32PrimitiveVector bpv6 = (Int32PrimitiveVector)pv;
            for (int row = 0; row < bpv6.getLength(); ++row) {
                ii.setIntNext(bpv6.getValue(row));
            }
        }
        else if (bt instanceof DInt16) {
            final Int16PrimitiveVector bpv7 = (Int16PrimitiveVector)pv;
            for (int row = 0; row < bpv7.getLength(); ++row) {
                ii.setShortNext(bpv7.getValue(row));
            }
        }
        else {
            if (!(bt instanceof DByte)) {
                throw new IllegalArgumentException("DODSVariable extractScalar invalid dataType= " + bt.getClass().getName());
            }
            final BytePrimitiveVector bpv8 = (BytePrimitiveVector)pv;
            for (int row = 0; row < bpv8.getLength(); ++row) {
                ii.setByteNext(bpv8.getValue(row));
            }
        }
    }
    
    private Array convertStringArray(final DArray dv) {
        final PrimitiveVector pv = dv.getPrimitiveVector();
        final BaseTypePrimitiveVector btpv = (BaseTypePrimitiveVector)pv;
        final int nStrings = btpv.getLength();
        final String[] storage = new String[nStrings];
        for (int i = 0; i < nStrings; ++i) {
            final DString bb = (DString)btpv.getValue(i);
            storage[i] = bb.getValue();
        }
        return Array.factory(DataType.STRING.getPrimitiveClassType(), this.makeShape(dv), storage);
    }
    
    private Array convertStringArray(final Array data, final Variable ncVar) {
        final String[] storage = (String[])data.getStorage();
        int max_len = 0;
        for (final String s : storage) {
            max_len = Math.max(max_len, s.length());
        }
        if (max_len > 1) {
            return data;
        }
        int count = 0;
        final int n = (int)data.getSize();
        final char[] charStorage = new char[n];
        for (final String s2 : storage) {
            if (s2.length() > 0) {
                charStorage[count++] = s2.charAt(0);
            }
        }
        ncVar.setDataType(DataType.CHAR);
        return Array.factory(DataType.CHAR.getPrimitiveClassType(), data.getShape(), charStorage);
    }
    
    private Array convertStringArrayToChar(final DArray dv, final Variable ncVar, List<Range> section) {
        final PrimitiveVector pv = dv.getPrimitiveVector();
        final BaseTypePrimitiveVector btpv = (BaseTypePrimitiveVector)pv;
        final int nStrings = btpv.getLength();
        if (section == null) {
            section = ncVar.getRanges();
        }
        final int[] shape = Range.getShape(section);
        final int total = (int)Index.computeSize(shape);
        int max_len = 0;
        for (int i = 0; i < nStrings; ++i) {
            final DString bb = (DString)btpv.getValue(i);
            final String val = bb.getValue();
            max_len = Math.max(max_len, val.length());
        }
        if (max_len == 1) {
            final char[] charStorage = new char[total];
            for (int j = 0; j < nStrings; ++j) {
                final DString bb2 = (DString)btpv.getValue(j);
                final String val2 = bb2.getValue();
                charStorage[j] = ((val2.length() > 0) ? val2.charAt(0) : '\0');
            }
            return Array.factory(DataType.CHAR.getPrimitiveClassType(), shape, charStorage);
        }
        final int[] varShape = ncVar.getShape();
        final int strLen = varShape[ncVar.getRank() - 1];
        final char[] storage = new char[total];
        int pos = 0;
        for (int k = 0; k < nStrings; ++k) {
            final DString bb3 = (DString)btpv.getValue(k);
            final String val3 = bb3.getValue();
            for (int len = Math.min(val3.length(), strLen), l = 0; l < len; ++l) {
                storage[pos + l] = val3.charAt(l);
            }
            pos += strLen;
        }
        return Array.factory(DataType.CHAR.getPrimitiveClassType(), shape, storage);
    }
    
    private Array convertStringToChar(final Array data, final Variable ncVar) {
        final String s = (String)data.getObject(Index.scalarIndexImmutable);
        final int total = (int)ncVar.getSize();
        final char[] storage = new char[total];
        for (int len = Math.min(s.length(), total), k = 0; k < len; ++k) {
            storage[k] = s.charAt(k);
        }
        return Array.factory(DataType.CHAR.getPrimitiveClassType(), ncVar.getShape(), storage);
    }
    
    private int[] makeShape(final DArray dodsArray) {
        int count = 0;
        Enumeration enumerate = dodsArray.getDimensions();
        while (enumerate.hasMoreElements()) {
            ++count;
            enumerate.nextElement();
        }
        final int[] shape = new int[count];
        enumerate = dodsArray.getDimensions();
        count = 0;
        while (enumerate.hasMoreElements()) {
            final DArrayDimension dad = enumerate.nextElement();
            shape[count++] = dad.getSize();
        }
        return shape;
    }
    
    static {
        ConvertD2N.logger = LoggerFactory.getLogger(DODSNetcdfFile.class);
    }
}
