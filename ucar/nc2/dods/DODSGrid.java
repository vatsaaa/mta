// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dods;

import org.slf4j.LoggerFactory;
import ucar.nc2.Variable;
import java.io.IOException;
import java.util.List;
import ucar.nc2.Attribute;
import java.util.Formatter;
import ucar.nc2.Dimension;
import java.util.ArrayList;
import ucar.nc2.Structure;
import ucar.nc2.Group;
import org.slf4j.Logger;

public class DODSGrid extends DODSVariable
{
    private static Logger logger;
    
    DODSGrid(final DODSNetcdfFile dodsfile, final Group parentGroup, final Structure parentStructure, final String dodsShortName, final DodsV dodsV) throws IOException {
        super(dodsfile, parentGroup, parentStructure, DODSNetcdfFile.makeNetcdfName(dodsShortName), dodsV);
        this.dodsShortName = dodsShortName;
        final DodsV array = dodsV.children.get(0);
        final List<Dimension> dims = new ArrayList<Dimension>();
        final Formatter sbuff = new Formatter();
        for (int i = 1; i < dodsV.children.size(); ++i) {
            final DodsV map = dodsV.children.get(i);
            final String name = DODSNetcdfFile.makeNetcdfName(map.bt.getName());
            final Dimension dim = parentGroup.findDimension(name);
            if (dim == null) {
                DODSGrid.logger.warn("DODSGrid cant find dimension = <" + name + ">");
            }
            else {
                dims.add(dim);
                sbuff.format("%s ", name);
            }
        }
        this.setDimensions(dims);
        this.setDataType(DODSNetcdfFile.convertToNCType(array.bt));
        if (DODSNetcdfFile.isUnsigned(array.bt)) {
            this.addAttribute(new Attribute("_Unsigned", "true"));
        }
        final DODSAttribute att = new DODSAttribute("_CoordinateAxes", sbuff.toString());
        this.addAttribute(att);
    }
    
    @Override
    protected Variable copy() {
        return new DODSGrid(this);
    }
    
    private DODSGrid(final DODSGrid from) {
        super(from);
    }
    
    static {
        DODSGrid.logger = LoggerFactory.getLogger(DODSGrid.class);
    }
}
