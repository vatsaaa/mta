// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dods;

import org.slf4j.LoggerFactory;
import opendap.dap.parser.ParseException;
import opendap.dap.DAP2Exception;
import java.io.OutputStream;
import opendap.dap.DConnect;
import java.util.StringTokenizer;
import opendap.dap.Attribute;
import java.io.IOException;
import opendap.dap.AttributeTable;
import opendap.dap.DAS;
import java.util.Collection;
import ucar.ma2.DataType;
import java.util.Iterator;
import java.io.PrintStream;
import java.util.ArrayList;
import opendap.dap.PrimitiveVector;
import opendap.dap.BaseTypePrimitiveVector;
import opendap.dap.NoSuchVariableException;
import opendap.dap.DataDDS;
import opendap.dap.DStructure;
import opendap.dap.DSequence;
import opendap.dap.DGrid;
import opendap.dap.DConstructor;
import opendap.dap.DList;
import java.util.Enumeration;
import opendap.dap.DDS;
import ucar.ma2.Array;
import opendap.dap.DArrayDimension;
import opendap.dap.DArray;
import java.util.List;
import opendap.dap.BaseType;
import org.slf4j.Logger;

class DodsV implements Comparable
{
    private static Logger logger;
    private static boolean debugAttributes;
    DodsV parent;
    BaseType bt;
    BaseType elemType;
    List<DodsV> children;
    DArray darray;
    List<DArrayDimension> dimensions;
    List<DArrayDimension> dimensionsAll;
    List<DODSAttribute> attributes;
    Array data;
    boolean isDone;
    int seq;
    private int nextInSequence;
    
    static DodsV parseDDS(final DDS dds) {
        final DodsV root = new DodsV(null, null);
        final Enumeration variables = dds.getVariables();
        parseVariables(root, variables);
        root.assignSequence(root);
        return root;
    }
    
    private static void parseVariables(final DodsV parent, final Enumeration children) {
        while (children.hasMoreElements()) {
            final BaseType bt = children.nextElement();
            if (bt instanceof DList) {
                final String mess = "Variables of type " + bt.getClass().getName() + " are not supported.";
                DodsV.logger.warn(mess);
            }
            else {
                final DodsV dodsV = new DodsV(parent, bt);
                if (bt instanceof DConstructor) {
                    final DConstructor dcon = (DConstructor)bt;
                    final Enumeration enumerate2 = dcon.getVariables();
                    parseVariables(dodsV, enumerate2);
                }
                else if (bt instanceof DArray) {
                    final DArray da = (DArray)bt;
                    final BaseType elemType = da.getPrimitiveVector().getTemplate();
                    dodsV.bt = elemType;
                    dodsV.darray = da;
                    if (elemType instanceof DGrid || elemType instanceof DSequence || elemType instanceof DList) {
                        final String mess2 = "Arrays of type " + elemType.getClass().getName() + " are not supported.";
                        DodsV.logger.warn(mess2);
                        continue;
                    }
                    if (elemType instanceof DStructure) {
                        final DConstructor dcon2 = (DConstructor)elemType;
                        final Enumeration nestedVariables = dcon2.getVariables();
                        parseVariables(dodsV, nestedVariables);
                    }
                }
                parent.children.add(dodsV);
            }
        }
    }
    
    static DodsV parseDataDDS(final DataDDS dds) throws NoSuchVariableException {
        final DodsV root = new DodsV(null, null);
        final Enumeration variables = dds.getVariables();
        parseDataVariables(root, variables);
        root.assignSequence(root);
        return root;
    }
    
    private static void parseDataVariables(final DodsV parent, final Enumeration children) throws NoSuchVariableException {
        while (children.hasMoreElements()) {
            final BaseType bt = children.nextElement();
            final DodsV dodsV = new DodsV(parent, bt);
            parent.children.add(dodsV);
            if (bt instanceof DGrid) {
                final DGrid dgrid = (DGrid)bt;
                if (dodsV.parent.bt == null) {
                    dodsV.darray = (DArray)dgrid.getVar(0);
                    processDArray(dodsV);
                }
                else {
                    dodsV.makeAllDimensions();
                }
                final Enumeration enumerate2 = dgrid.getVariables();
                parseDataVariables(dodsV, enumerate2);
            }
            else if (bt instanceof DSequence) {
                final DSequence dseq = (DSequence)bt;
                final int seqlen = dseq.getRowCount();
                if (seqlen > 0) {
                    final DArrayDimension ddim = new DArrayDimension(seqlen, (String)null);
                    dodsV.dimensions.add(ddim);
                }
                dodsV.makeAllDimensions();
                final Enumeration enumerate3 = dseq.getVariables();
                parseDataVariables(dodsV, enumerate3);
            }
            else if (bt instanceof DConstructor) {
                final DStructure dcon = (DStructure)bt;
                dodsV.makeAllDimensions();
                final Enumeration enumerate2 = dcon.getVariables();
                parseDataVariables(dodsV, enumerate2);
            }
            else if (bt instanceof DArray) {
                dodsV.darray = (DArray)bt;
                processDArray(dodsV);
                dodsV.bt = dodsV.elemType;
                if (!(dodsV.elemType instanceof DStructure)) {
                    continue;
                }
                final DStructure dcon = (DStructure)dodsV.elemType;
                final Enumeration nestedVariables = dcon.getVariables();
                parseDataVariables(dodsV, nestedVariables);
            }
            else {
                dodsV.makeAllDimensions();
            }
        }
    }
    
    private static void processDArray(final DodsV dodsV) {
        final DArray da = dodsV.darray;
        final Enumeration dims = da.getDimensions();
        while (dims.hasMoreElements()) {
            final DArrayDimension dim = dims.nextElement();
            dodsV.dimensions.add(dim);
        }
        dodsV.makeAllDimensions();
        final PrimitiveVector pv = da.getPrimitiveVector();
        BaseType elemType;
        if (pv instanceof BaseTypePrimitiveVector) {
            final BaseTypePrimitiveVector bpv = (BaseTypePrimitiveVector)pv;
            elemType = bpv.getValue(0);
        }
        else {
            elemType = da.getPrimitiveVector().getTemplate();
        }
        dodsV.elemType = elemType;
        if (dodsV.elemType instanceof DGrid || dodsV.elemType instanceof DSequence || dodsV.elemType instanceof DList) {
            final String mess = "Arrays of type " + dodsV.bt.getClass().getName() + " are not supported.";
            DodsV.logger.error(mess);
            throw new IllegalArgumentException(mess);
        }
    }
    
    DodsV(final DodsV parent, final BaseType bt) {
        this.children = new ArrayList<DodsV>();
        this.dimensions = new ArrayList<DArrayDimension>();
        this.dimensionsAll = new ArrayList<DArrayDimension>();
        this.attributes = new ArrayList<DODSAttribute>();
        this.nextInSequence = 0;
        this.parent = parent;
        this.bt = bt;
        this.elemType = bt;
    }
    
    public int compareTo(final Object o) {
        return this.seq - ((DodsV)o).seq;
    }
    
    void show(final PrintStream out, final String space) {
        out.print(space + "DodsV.show " + this.getName() + " " + this.getType());
        out.print("(");
        int count = 0;
        for (final DArrayDimension dim : this.dimensionsAll) {
            final String name = (dim.getName() == null) ? "" : (dim.getName() + "=");
            if (count > 0) {
                out.print(",");
            }
            out.print(name + dim.getSize());
            ++count;
        }
        out.println(")");
        for (final DodsV dodsV : this.children) {
            dodsV.show(out, space + "  ");
        }
    }
    
    String getName() {
        return (this.bt == null) ? " root" : this.bt.getName();
    }
    
    String getType() {
        return (this.bt == null) ? "" : this.bt.getTypeName();
    }
    
    DataType getDataType() {
        if (this.bt == null) {
            return null;
        }
        if (this.bt instanceof DGrid) {
            DODSNetcdfFile.convertToNCType(this.elemType);
        }
        return DODSNetcdfFile.convertToNCType(this.bt);
    }
    
    int[] getShape() {
        final int[] shape = new int[this.dimensions.size()];
        for (int i = 0; i < this.dimensions.size(); ++i) {
            final DArrayDimension dim = this.dimensions.get(i);
            shape[i] = dim.getSize();
        }
        return shape;
    }
    
    int[] getShapeAll() {
        if (this.bt instanceof DSequence) {
            final DSequence dseq = (DSequence)this.bt;
            final int seqlen = dseq.getRowCount();
            return new int[] { seqlen };
        }
        final int[] shape = new int[this.dimensionsAll.size()];
        for (int i = 0; i < this.dimensionsAll.size(); ++i) {
            final DArrayDimension dim = this.dimensionsAll.get(i);
            shape[i] = dim.getSize();
        }
        return shape;
    }
    
    void addAttribute(final DODSAttribute att) {
        this.attributes.add(att);
    }
    
    void makeAllDimensions() {
        this.dimensionsAll = new ArrayList<DArrayDimension>();
        if (this.parent != null) {
            this.dimensionsAll.addAll(this.parent.dimensionsAll);
        }
        this.dimensionsAll.addAll(this.dimensions);
    }
    
    String getFullName() {
        if (this.parent != null && this.parent.bt != null) {
            return this.parent.getFullName() + "." + this.bt.getName();
        }
        return (this.bt == null) ? "root" : this.bt.getName();
    }
    
    String getNetcdfShortName() {
        return DODSNetcdfFile.makeNetcdfName(this.getName());
    }
    
    private void assignSequence(final DodsV root) {
        for (final DodsV nested : this.children) {
            nested.assignSequence(root);
            nested.seq = root.nextInSequence;
            ++this.nextInSequence;
        }
    }
    
    void parseDAS(final DAS das) throws IOException {
        final Enumeration tableNames = das.getNames();
        while (tableNames.hasMoreElements()) {
            final String tableName = tableNames.nextElement();
            final AttributeTable attTable = das.getAttributeTableN(tableName);
            if (tableName.equals("NC_GLOBAL") || tableName.equals("HDF_GLOBAL")) {
                this.addAttributeTable(this, attTable, tableName, true);
            }
            else {
                if (tableName.equals("DODS_EXTRA")) {
                    continue;
                }
                if (tableName.equals("EXTRA_DIMENSION")) {
                    continue;
                }
                DodsV dodsV = this.findDodsV(tableName, false);
                if (dodsV != null) {
                    this.addAttributeTable(dodsV, attTable, tableName, true);
                }
                else {
                    dodsV = this.findTableDotDelimited(tableName);
                    if (dodsV != null) {
                        this.addAttributeTable(dodsV, attTable, tableName, true);
                    }
                    else {
                        if (DodsV.debugAttributes) {
                            System.out.println("DODSNetcdf getAttributes CANT find <" + tableName + "> add to globals");
                        }
                        this.addAttributeTable(this, attTable, tableName, false);
                    }
                }
            }
        }
    }
    
    private void addAttributeTable(final DodsV dodsV, final AttributeTable attTable, final String fullName, final boolean match) {
        if (attTable == null) {
            return;
        }
        final Enumeration attNames = attTable.getNames();
        while (attNames.hasMoreElements()) {
            final String attName = attNames.nextElement();
            final Attribute att = attTable.getAttribute(attName);
            if (att == null) {
                DodsV.logger.error("Attribute not found=" + attName + " in table=" + attTable.getName());
            }
            else {
                this.addAttribute(dodsV, att, fullName, match);
            }
        }
    }
    
    private void addAttribute(final DodsV dodsV, final Attribute att, String fullName, final boolean match) {
        if (att == null) {
            return;
        }
        fullName = fullName + "." + att.getName();
        if (!att.isContainer()) {
            final DODSAttribute ncatt = new DODSAttribute(match ? att.getName() : fullName, att);
            dodsV.addAttribute(ncatt);
            if (DodsV.debugAttributes) {
                System.out.println(" addAttribute " + ncatt.getName() + " to " + dodsV.getFullName());
            }
        }
        else if (att.getName() == null) {
            DodsV.logger.info("DODS attribute name is null = " + att);
        }
        else {
            final DodsV child = dodsV.findDodsV(att.getName(), false);
            if (child != null) {
                this.addAttributeTable(child, att.getContainerN(), fullName, match);
            }
            else {
                if (att.getName().equals("DODS")) {
                    return;
                }
                if (DodsV.debugAttributes) {
                    System.out.println(" Cant find nested Variable " + att.getName() + " in " + dodsV.getFullName());
                }
                this.addAttributeTable(this, att.getContainerN(), fullName, false);
            }
        }
    }
    
    DodsV findDodsV(final String name, final boolean useDone) {
        for (final DodsV dodsV : this.children) {
            if (useDone && dodsV.isDone) {
                continue;
            }
            if (name == null || dodsV == null || dodsV.bt == null) {
                DodsV.logger.warn("Corrupted structure");
            }
            else {
                if (name.equals(dodsV.bt.getName())) {
                    return dodsV;
                }
                continue;
            }
        }
        return null;
    }
    
    DodsV findByNetcdfShortName(final String ncname) {
        for (final DodsV child : this.children) {
            if (ncname.equals(child.getNetcdfShortName())) {
                return child;
            }
        }
        return null;
    }
    
    DodsV findByDodsShortName(final String dodsname) {
        for (final DodsV child : this.children) {
            if (dodsname.equals(child.getName())) {
                return child;
            }
        }
        for (final DodsV child : this.children) {
            final DodsV d = child.findByDodsShortName(dodsname);
            if (null != d) {
                return d;
            }
        }
        return null;
    }
    
    DodsV findDataV(final DodsV ddsV) {
        if (ddsV.parent.bt == null) {
            final DodsV dataV = this.findDodsV(ddsV.bt.getName(), true);
            return dataV;
        }
        final DodsV parentV = this.findDataV(ddsV.parent);
        if (parentV == null) {
            return this.findDodsV(ddsV.bt.getName(), true);
        }
        return parentV.findDodsV(ddsV.bt.getName(), true);
    }
    
    DodsV findTableDotDelimited(final String tableName) {
        DodsV dodsV = this;
        final StringTokenizer toker = new StringTokenizer(tableName, ".");
        while (toker.hasMoreTokens()) {
            final String name = toker.nextToken();
            dodsV = dodsV.findDodsV(name, false);
            if (dodsV == null) {
                return null;
            }
        }
        return dodsV;
    }
    
    private static void doit(final String urlName) throws IOException, DAP2Exception, ParseException {
        System.out.println("DODSV read =" + urlName);
        final DConnect dodsConnection = new DConnect(urlName, true);
        final DDS dds = dodsConnection.getDDS();
        dds.print((OutputStream)System.out);
        final DodsV root = parseDDS(dds);
        final DAS das = dodsConnection.getDAS();
        das.print((OutputStream)System.out);
        root.parseDAS(das);
        root.show(System.out, "");
    }
    
    public static void main(final String[] args) throws IOException, ParseException, DAP2Exception {
        doit("http://iridl.ldeo.columbia.edu/SOURCES/.CAYAN/dods");
    }
    
    static {
        DodsV.logger = LoggerFactory.getLogger(DodsV.class);
        DodsV.debugAttributes = false;
    }
}
