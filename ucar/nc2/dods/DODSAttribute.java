// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dods;

import ucar.unidata.util.StringUtil;
import ucar.ma2.Index;
import java.util.Iterator;
import ucar.ma2.Array;
import ucar.ma2.DataType;
import net.jcip.annotations.Immutable;
import ucar.nc2.Attribute;

@Immutable
public class DODSAttribute extends Attribute
{
    private static String[] escapeAttributeStrings;
    private static String[] substAttributeStrings;
    
    public DODSAttribute(final String dodsName, final opendap.dap.Attribute att) {
        super(DODSNetcdfFile.makeNetcdfName(dodsName));
        final DataType ncType = DODSNetcdfFile.convertToNCType(att.getType());
        int nvals = 0;
        Iterator iter = att.getValuesIterator();
        while (iter.hasNext()) {
            iter.next();
            ++nvals;
        }
        final String[] vals = new String[nvals];
        iter = att.getValuesIterator();
        int count = 0;
        while (iter.hasNext()) {
            vals[count++] = iter.next();
        }
        Array data = null;
        if (ncType == DataType.STRING) {
            data = Array.factory(ncType.getPrimitiveClassType(), new int[] { nvals }, vals);
        }
        else {
            try {
                data = Array.factory(ncType.getPrimitiveClassType(), new int[] { nvals });
                final Index ima = data.getIndex();
                for (int i = 0; i < nvals; ++i) {
                    final double dval = Double.parseDouble(vals[i]);
                    data.setDouble(ima.set(i), dval);
                }
            }
            catch (NumberFormatException e) {
                throw new IllegalArgumentException("Illegal Numeric Value for Attribute Value for " + dodsName);
            }
        }
        this.setValues(data);
    }
    
    protected DODSAttribute(final String dodsName, final String val) {
        super(DODSNetcdfFile.makeNetcdfName(dodsName), val);
    }
    
    private String unescapeAttributeStringValues(final String value) {
        return StringUtil.substitute(value, DODSAttribute.substAttributeStrings, DODSAttribute.escapeAttributeStrings);
    }
    
    static {
        DODSAttribute.escapeAttributeStrings = new String[] { "\\", "\"" };
        DODSAttribute.substAttributeStrings = new String[] { "\\\\", "\\\"" };
    }
}
