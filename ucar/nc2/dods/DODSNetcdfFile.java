// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dods;

import org.slf4j.LoggerFactory;
import java.io.ByteArrayOutputStream;
import java.util.Formatter;
import ucar.nc2.iosp.IospHelper;
import java.nio.channels.WritableByteChannel;
import ucar.ma2.Section;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.ParsedSectionSpec;
import java.util.Map;
import ucar.ma2.Range;
import java.util.HashMap;
import java.util.Collections;
import opendap.dap.StatusUI;
import opendap.dap.DataDDS;
import ucar.unidata.util.StringUtil;
import opendap.dap.DArrayDimension;
import opendap.dap.DArray;
import ucar.ma2.Array;
import opendap.dap.BaseType;
import opendap.dap.DStructure;
import opendap.dap.DSequence;
import opendap.dap.DUInt32;
import opendap.dap.DUInt16;
import opendap.dap.DInt32;
import opendap.dap.DInt16;
import opendap.dap.DFloat64;
import opendap.dap.DFloat32;
import opendap.dap.DByte;
import opendap.dap.DString;
import opendap.dap.DGrid;
import ucar.nc2.Structure;
import opendap.dap.DConstructor;
import opendap.dap.AttributeTable;
import java.util.Enumeration;
import ucar.nc2.Group;
import ucar.nc2.Dimension;
import ucar.nc2.Attribute;
import ucar.nc2.util.cache.FileCacheable;
import java.util.Iterator;
import java.util.List;
import ucar.ma2.DataType;
import ucar.nc2.Variable;
import java.util.ArrayList;
import java.util.StringTokenizer;
import opendap.dap.DAP2Exception;
import java.io.FileNotFoundException;
import opendap.dap.DDSException;
import opendap.dap.DASException;
import opendap.dap.parser.ParseException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.io.IOException;
import ucar.nc2.util.CancelTask;
import ucar.nc2.util.DebugFlags;
import opendap.dap.DAS;
import opendap.dap.DDS;
import opendap.dap.DConnect2;
import org.slf4j.Logger;
import ucar.nc2.NetcdfFile;

public class DODSNetcdfFile extends NetcdfFile
{
    public static boolean debugCE;
    public static boolean debugServerCall;
    public static boolean debugOpenResult;
    public static boolean debugDataResult;
    public static boolean debugCharArray;
    public static boolean debugConvertData;
    public static boolean debugConstruct;
    public static boolean debugPreload;
    public static boolean debugTime;
    public static boolean showNCfile;
    public static boolean debugAttributes;
    public static boolean debugCached;
    private static boolean accept_compress;
    private static boolean preload;
    private static boolean useGroups;
    private static int preloadCoordVarSize;
    private static Logger logger;
    private ConvertD2N convertD2N;
    private DConnect2 dodsConnection;
    private DDS dds;
    private DAS das;
    
    public static void setAllowSessions(final boolean b) {
        DConnect2.setAllowSessions(b);
    }
    
    @Deprecated
    public static void setAllowDeflate(final boolean b) {
        DODSNetcdfFile.accept_compress = b;
    }
    
    public static void setAllowCompression(final boolean b) {
        DODSNetcdfFile.accept_compress = b;
    }
    
    public static void setDebugFlags(final DebugFlags debugFlag) {
        DODSNetcdfFile.debugCE = debugFlag.isSet("DODS/constraintExpression");
        DODSNetcdfFile.debugServerCall = debugFlag.isSet("DODS/serverCall");
        DODSNetcdfFile.debugOpenResult = debugFlag.isSet("DODS/debugOpenResult");
        DODSNetcdfFile.debugDataResult = debugFlag.isSet("DODS/debugDataResult");
        DODSNetcdfFile.debugCharArray = debugFlag.isSet("DODS/charArray");
        DODSNetcdfFile.debugConstruct = debugFlag.isSet("DODS/constructNetcdf");
        DODSNetcdfFile.debugPreload = debugFlag.isSet("DODS/preload");
        DODSNetcdfFile.debugTime = debugFlag.isSet("DODS/timeCalls");
        DODSNetcdfFile.showNCfile = debugFlag.isSet("DODS/showNCfile");
        DODSNetcdfFile.debugAttributes = debugFlag.isSet("DODS/attributes");
        DODSNetcdfFile.debugCached = debugFlag.isSet("DODS/cache");
    }
    
    public static void setPreload(final boolean b) {
        DODSNetcdfFile.preload = b;
    }
    
    public static void setCoordinateVariablePreloadSize(final int size) {
        DODSNetcdfFile.preloadCoordVarSize = size;
    }
    
    public static String canonicalURL(final String urlName) {
        if (urlName.startsWith("http:")) {
            return "dods:" + urlName.substring(5);
        }
        return urlName;
    }
    
    public DODSNetcdfFile(final String datasetURL) throws IOException {
        this(datasetURL, null);
    }
    
    public DODSNetcdfFile(final String datasetURL, final CancelTask cancelTask) throws IOException {
        this.convertD2N = new ConvertD2N();
        this.dodsConnection = null;
        String urlName = datasetURL;
        this.location = datasetURL;
        if (datasetURL.startsWith("dods:")) {
            urlName = "http:" + datasetURL.substring(5);
        }
        else {
            if (!datasetURL.startsWith("http:")) {
                throw new MalformedURLException(datasetURL + " must start with dods: or http:");
            }
            this.location = "dods:" + datasetURL.substring(5);
        }
        if (DODSNetcdfFile.debugServerCall) {
            System.out.println("DConnect to = <" + urlName + ">");
        }
        this.dodsConnection = new DConnect2(urlName, DODSNetcdfFile.accept_compress);
        if (cancelTask != null && cancelTask.isCancel()) {
            return;
        }
        try {
            this.dds = this.dodsConnection.getDDS();
            if (DODSNetcdfFile.debugServerCall) {
                System.out.println("DODSNetcdfFile readDDS");
            }
            if (DODSNetcdfFile.debugOpenResult) {
                System.out.println("DDS = ");
                this.dds.print((OutputStream)System.out);
            }
            if (cancelTask != null && cancelTask.isCancel()) {
                return;
            }
            this.das = this.dodsConnection.getDAS();
            if (DODSNetcdfFile.debugServerCall) {
                System.out.println("DODSNetcdfFile readDAS");
            }
            if (DODSNetcdfFile.debugOpenResult) {
                System.out.println("DAS = ");
                this.das.print((OutputStream)System.out);
            }
            if (cancelTask != null && cancelTask.isCancel()) {
                return;
            }
            if (DODSNetcdfFile.debugOpenResult) {
                System.out.println("dodsVersion = " + this.dodsConnection.getServerVersion());
            }
        }
        catch (ParseException e) {
            DODSNetcdfFile.logger.info("DODSNetcdfFile " + datasetURL, (Throwable)e);
            if (DODSNetcdfFile.debugOpenResult) {
                System.out.println("open failure = " + e.getMessage());
            }
            throw new IOException(e.getMessage());
        }
        catch (DASException e2) {
            DODSNetcdfFile.logger.info("DODSNetcdfFile " + datasetURL, (Throwable)e2);
            if (DODSNetcdfFile.debugOpenResult) {
                System.out.println("open failure = " + e2.getClass().getName() + ": " + e2.getMessage());
            }
            throw new IOException(e2.getClass().getName() + ": " + e2.getMessage());
        }
        catch (DDSException e3) {
            DODSNetcdfFile.logger.info("DODSNetcdfFile " + datasetURL, (Throwable)e3);
            if (DODSNetcdfFile.debugOpenResult) {
                System.out.println("open failure = " + e3.getClass().getName() + ": " + e3.getMessage());
            }
            throw new IOException(e3.getClass().getName() + ": " + e3.getMessage());
        }
        catch (DAP2Exception dodsE) {
            if (dodsE.getErrorCode() == 1) {
                throw new FileNotFoundException(dodsE.getMessage());
            }
            dodsE.printStackTrace(System.err);
            throw new IOException((Throwable)dodsE);
        }
        catch (Exception e4) {
            DODSNetcdfFile.logger.info("DODSNetcdfFile " + datasetURL, e4);
            if (DODSNetcdfFile.debugOpenResult) {
                System.out.println("open failure = " + e4.getClass().getName() + ": " + e4.getMessage());
            }
            throw new IOException(e4.getClass().getName() + ": " + e4.getMessage());
        }
        final DodsV rootDodsV = DodsV.parseDDS(this.dds);
        rootDodsV.parseDAS(this.das);
        if (cancelTask != null && cancelTask.isCancel()) {
            return;
        }
        this.constructTopVariables(rootDodsV, cancelTask);
        if (cancelTask != null && cancelTask.isCancel()) {
            return;
        }
        this.constructConstructors(rootDodsV, cancelTask);
        if (cancelTask != null && cancelTask.isCancel()) {
            return;
        }
        this.finish();
        this.parseGlobalAttributes(this.das, rootDodsV);
        if (cancelTask != null && cancelTask.isCancel()) {
            return;
        }
        final int pos;
        if (0 <= (pos = urlName.indexOf(63))) {
            final String datasetName = urlName.substring(0, pos);
            if (DODSNetcdfFile.debugServerCall) {
                System.out.println(" reconnect to = <" + datasetName + ">");
            }
            this.dodsConnection = new DConnect2(datasetName, DODSNetcdfFile.accept_compress);
            final String CE = urlName.substring(pos + 1);
            final StringTokenizer stoke = new StringTokenizer(CE, " ,");
            while (stoke.hasMoreTokens()) {
                final String proj = stoke.nextToken();
                final int subsetPos = proj.indexOf(91);
                if (DODSNetcdfFile.debugCE) {
                    System.out.println(" CE = " + proj + " " + subsetPos);
                }
                if (subsetPos > 0) {
                    final String vname = proj.substring(0, subsetPos);
                    final String vCE = proj.substring(subsetPos);
                    if (DODSNetcdfFile.debugCE) {
                        System.out.println(" vCE = <" + vname + "><" + vCE + ">");
                    }
                    final DODSVariable dodsVar = (DODSVariable)this.findVariable(vname);
                    dodsVar.setCE(vCE);
                    dodsVar.setCaching(true);
                }
            }
        }
        if (DODSNetcdfFile.preload) {
            final List<Variable> preloadList = new ArrayList<Variable>();
            for (final Variable dodsVar2 : this.variables) {
                final long size = dodsVar2.getSize() * dodsVar2.getElementSize();
                if ((dodsVar2.isCoordinateVariable() && size < DODSNetcdfFile.preloadCoordVarSize) || dodsVar2.isCaching() || dodsVar2.getDataType() == DataType.STRING) {
                    dodsVar2.setCaching(true);
                    preloadList.add(dodsVar2);
                    if (!DODSNetcdfFile.debugPreload) {
                        continue;
                    }
                    System.out.println("  preload" + dodsVar2);
                }
            }
            if (cancelTask != null && cancelTask.isCancel()) {
                return;
            }
            this.readArrays(preloadList);
        }
        this.finish();
        if (DODSNetcdfFile.showNCfile) {
            System.out.println("DODS nc file = " + this);
        }
    }
    
    @Override
    public synchronized void close() throws IOException {
        if (null != this.dodsConnection) {
            this.dodsConnection.closeSession();
        }
        if (this.cache != null) {
            this.unlocked = true;
            this.cache.release(this);
        }
        else {
            this.dodsConnection = null;
        }
    }
    
    @Override
    public boolean sync() throws IOException {
        this.unlocked = false;
        return this.spi != null && this.spi.sync();
    }
    
    private void parseGlobalAttributes(final DAS das, final DodsV root) {
        final List<DODSAttribute> atts = root.attributes;
        for (final DODSAttribute ncatt : atts) {
            this.rootGroup.addAttribute(ncatt);
        }
        final Enumeration tableNames = das.getNames();
        while (tableNames.hasMoreElements()) {
            final String tableName = tableNames.nextElement();
            final AttributeTable attTable = das.getAttributeTableN(tableName);
            if (tableName.equals("DODS_EXTRA")) {
                final Enumeration attNames = attTable.getNames();
                while (attNames.hasMoreElements()) {
                    final String attName = attNames.nextElement();
                    if (attName.equals("Unlimited_Dimension")) {
                        final opendap.dap.Attribute att = attTable.getAttribute(attName);
                        final DODSAttribute ncatt2 = new DODSAttribute(attName, att);
                        this.setUnlimited(ncatt2.getStringValue());
                    }
                    else {
                        DODSNetcdfFile.logger.warn(" Unknown DODS_EXTRA attribute = " + attName + " " + this.location);
                    }
                }
            }
            else {
                if (!tableName.equals("EXTRA_DIMENSION")) {
                    continue;
                }
                final Enumeration attNames = attTable.getNames();
                while (attNames.hasMoreElements()) {
                    final String attName = attNames.nextElement();
                    final opendap.dap.Attribute att = attTable.getAttribute(attName);
                    final DODSAttribute ncatt2 = new DODSAttribute(attName, att);
                    final int length = ncatt2.getNumericValue().intValue();
                    final Dimension extraDim = new Dimension(attName, length);
                    this.addDimension(null, extraDim);
                }
            }
        }
    }
    
    private void constructTopVariables(final DodsV rootDodsV, final CancelTask cancelTask) throws IOException {
        final List<DodsV> topVariables = rootDodsV.children;
        for (final DodsV dodsV : topVariables) {
            if (dodsV.bt instanceof DConstructor) {
                continue;
            }
            this.addVariable(this.rootGroup, null, dodsV);
            if (cancelTask != null && cancelTask.isCancel()) {
                return;
            }
        }
    }
    
    private void constructConstructors(final DodsV rootDodsV, final CancelTask cancelTask) throws IOException {
        final List<DodsV> topVariables = rootDodsV.children;
        for (final DodsV dodsV : topVariables) {
            if (dodsV.isDone) {
                continue;
            }
            if (dodsV.bt instanceof DGrid) {
                continue;
            }
            this.addVariable(this.rootGroup, null, dodsV);
            if (cancelTask != null && cancelTask.isCancel()) {
                return;
            }
        }
        for (final DodsV dodsV : topVariables) {
            if (dodsV.isDone) {
                continue;
            }
            this.addVariable(this.rootGroup, null, dodsV);
            if (cancelTask != null && cancelTask.isCancel()) {
                return;
            }
        }
    }
    
    Variable addVariable(final Group parentGroup, final Structure parentStructure, final DodsV dodsV) throws IOException {
        final Variable v = this.makeVariable(parentGroup, parentStructure, dodsV);
        if (v != null) {
            this.addAttributes(v, dodsV);
            if (parentStructure == null) {
                parentGroup.addVariable(v);
            }
            else {
                parentStructure.addMemberVariable(v);
            }
            dodsV.isDone = true;
        }
        return v;
    }
    
    private Variable makeVariable(final Group parentGroup, final Structure parentStructure, final DodsV dodsV) throws IOException {
        final BaseType dodsBT = dodsV.bt;
        final String dodsShortName = dodsBT.getName();
        if (DODSNetcdfFile.debugConstruct) {
            System.out.print("DODSNetcdf makeVariable try to init <" + dodsShortName + "> :");
        }
        if (dodsBT instanceof DString) {
            if (dodsV.darray == null) {
                if (DODSNetcdfFile.debugConstruct) {
                    System.out.println("  assigned to DString: name = " + dodsShortName);
                }
                return new DODSVariable(this, parentGroup, parentStructure, dodsShortName, dodsBT, dodsV);
            }
            if (DODSNetcdfFile.debugConstruct) {
                System.out.println("  assigned to Array of Strings: name = " + dodsShortName);
            }
            return new DODSVariable(this, parentGroup, parentStructure, dodsShortName, dodsV.darray, dodsBT, dodsV);
        }
        else if (dodsBT instanceof DByte || dodsBT instanceof DFloat32 || dodsBT instanceof DFloat64 || dodsBT instanceof DInt16 || dodsBT instanceof DInt32 || dodsBT instanceof DUInt16 || dodsBT instanceof DUInt32) {
            if (dodsV.darray == null) {
                if (DODSNetcdfFile.debugConstruct) {
                    System.out.println("  assigned to scalar " + dodsBT.getTypeName() + ": name = " + dodsShortName);
                }
                return new DODSVariable(this, parentGroup, parentStructure, dodsShortName, dodsBT, dodsV);
            }
            if (DODSNetcdfFile.debugConstruct) {
                System.out.println("  assigned to array of type " + dodsBT.getClass().getName() + ": name = " + dodsShortName);
            }
            return new DODSVariable(this, parentGroup, parentStructure, dodsShortName, dodsV.darray, dodsBT, dodsV);
        }
        else if (dodsBT instanceof DGrid) {
            if (dodsV.darray == null) {
                if (DODSNetcdfFile.debugConstruct) {
                    System.out.println(" assigned to DGrid <" + dodsShortName + ">");
                }
                for (int i = 1; i < dodsV.children.size(); ++i) {
                    final DodsV map = dodsV.children.get(i);
                    final String shortName = makeNetcdfName(map.bt.getName());
                    Variable mapV = parentGroup.findVariable(shortName);
                    if (mapV == null) {
                        mapV = this.addVariable(parentGroup, parentStructure, map);
                        this.makeCoordinateVariable(parentGroup, mapV, map.data);
                    }
                    else if (!mapV.isCoordinateVariable()) {
                        this.makeCoordinateVariable(parentGroup, mapV, map.data);
                    }
                }
                return new DODSGrid(this, parentGroup, parentStructure, dodsShortName, dodsV);
            }
            if (DODSNetcdfFile.debugConstruct) {
                System.out.println(" ERROR! array of DGrid <" + dodsShortName + ">");
            }
            return null;
        }
        else if (dodsBT instanceof DSequence) {
            if (dodsV.darray == null) {
                if (DODSNetcdfFile.debugConstruct) {
                    System.out.println(" assigned to DSequence <" + dodsShortName + ">");
                }
                return new DODSStructure(this, parentGroup, parentStructure, dodsShortName, dodsV);
            }
            if (DODSNetcdfFile.debugConstruct) {
                System.out.println(" ERROR! array of DSequence <" + dodsShortName + ">");
            }
            return null;
        }
        else {
            if (!(dodsBT instanceof DStructure)) {
                DODSNetcdfFile.logger.warn("DODSNetcdf " + this.location + " didnt process basetype <" + dodsBT.getTypeName() + "> variable = " + dodsShortName);
                return null;
            }
            final DStructure dstruct = (DStructure)dodsBT;
            if (dodsV.darray != null) {
                if (DODSNetcdfFile.debugConstruct) {
                    System.out.println(" assigned to Array of DStructure <" + dodsShortName + "> ");
                }
                return new DODSStructure(this, parentGroup, parentStructure, dodsShortName, dodsV.darray, dodsV);
            }
            if (DODSNetcdfFile.useGroups && parentStructure == null && this.isGroup(dstruct)) {
                if (DODSNetcdfFile.debugConstruct) {
                    System.out.println(" assigned to Group <" + dodsShortName + ">");
                }
                final Group g = new Group(this, parentGroup, makeNetcdfName(dodsShortName));
                this.addAttributes(g, dodsV);
                parentGroup.addGroup(g);
                for (final DodsV nested : dodsV.children) {
                    this.addVariable(g, null, nested);
                }
                return null;
            }
            if (DODSNetcdfFile.debugConstruct) {
                System.out.println(" assigned to DStructure <" + dodsShortName + ">");
            }
            return new DODSStructure(this, parentGroup, parentStructure, dodsShortName, dodsV);
        }
    }
    
    private void makeCoordinateVariable(final Group parentGroup, final Variable v, final Array data) {
        final String name = v.getShortName();
        final Dimension oldDimension = v.getDimension(0);
        final Dimension newDimension = new Dimension(name, oldDimension.getLength());
        v.setDimension(0, newDimension);
        final Dimension old = parentGroup.findDimension(name);
        parentGroup.remove(old);
        parentGroup.addDimension(newDimension);
        if (data != null) {
            v.setCachedData(data);
            if (DODSNetcdfFile.debugCached) {
                System.out.println(" cache for <" + name + "> length =" + data.getSize());
            }
        }
    }
    
    private boolean isGroup(final DStructure dstruct) {
        final BaseType parent = dstruct.getParent();
        return parent == null || !(parent instanceof DStructure) || this.isGroup((DStructure)parent);
    }
    
    private void addAttributes(final Variable v, final DodsV dodsV) {
        final List<DODSAttribute> atts = dodsV.attributes;
        for (final DODSAttribute ncatt : atts) {
            v.addAttribute(ncatt);
        }
        final Attribute axes = v.findAttribute("coordinates");
        final Attribute _axes = v.findAttribute("_CoordinateAxes");
        if (null != axes && null != _axes) {
            v.addAttribute(new Attribute("_CoordinateAxes", axes.getStringValue() + " " + _axes.getStringValue()));
        }
    }
    
    private void addAttributes(final Group g, final DodsV dodsV) {
        final List<DODSAttribute> atts = dodsV.attributes;
        for (final DODSAttribute ncatt : atts) {
            g.addAttribute(ncatt);
        }
    }
    
    Dimension getNetcdfStrlenDim(final DODSVariable v) {
        final AttributeTable table = this.das.getAttributeTableN(v.getName());
        if (table == null) {
            return null;
        }
        final opendap.dap.Attribute dodsAtt = table.getAttribute("DODS");
        if (dodsAtt == null) {
            return null;
        }
        final AttributeTable dodsTable = dodsAtt.getContainerN();
        if (dodsTable == null) {
            return null;
        }
        final opendap.dap.Attribute att = dodsTable.getAttribute("strlen");
        if (att == null) {
            return null;
        }
        final String strlen = att.getValueAtN(0);
        final opendap.dap.Attribute att2 = dodsTable.getAttribute("dimName");
        final String dimName = (att2 == null) ? null : att2.getValueAtN(0);
        if (DODSNetcdfFile.debugCharArray) {
            System.out.println(v.getName() + " has strlen= " + strlen + " dimName= " + dimName);
        }
        int dimLength;
        try {
            dimLength = Integer.parseInt(strlen);
        }
        catch (NumberFormatException e) {
            DODSNetcdfFile.logger.warn("DODSNetcdfFile " + this.location + " var = " + v.getName() + " error on strlen attribute = " + strlen);
            return null;
        }
        if (dimLength <= 0) {
            return null;
        }
        return new Dimension(dimName, dimLength, dimName != null);
    }
    
    Dimension getSharedDimension(Group group, final Dimension d) {
        if (d.getName() == null) {
            return d;
        }
        if (group == null) {
            group = this.rootGroup;
        }
        for (final Dimension sd : group.getDimensions()) {
            if (sd.getName().equals(d.getName()) && sd.getLength() == d.getLength()) {
                return sd;
            }
        }
        d.setShared(true);
        group.addDimension(d);
        return d;
    }
    
    List<Dimension> constructDimensions(Group group, final DArray dodsArray) {
        if (group == null) {
            group = this.rootGroup;
        }
        final List<Dimension> dims = new ArrayList<Dimension>();
        final Enumeration enumerate = dodsArray.getDimensions();
        while (enumerate.hasMoreElements()) {
            final DArrayDimension dad = enumerate.nextElement();
            String name = dad.getName();
            if (name != null) {
                name = StringUtil.unescape(dad.getName());
            }
            Dimension myd;
            if (name == null) {
                myd = new Dimension("", dad.getSize(), false);
            }
            else {
                myd = group.findDimension(name);
                if (myd == null) {
                    myd = new Dimension(name, dad.getSize());
                    group.addDimension(myd);
                }
                else if (myd.getLength() != dad.getSize()) {
                    myd = new Dimension(name, dad.getSize(), false);
                }
            }
            dims.add(myd);
        }
        return dims;
    }
    
    private void setUnlimited(final String dimName) {
        final Dimension dim = this.rootGroup.findDimension(dimName);
        if (dim != null) {
            dim.setUnlimited(true);
        }
        else {
            DODSNetcdfFile.logger.error(" DODS Unlimited_Dimension = " + dimName + " not found on " + this.location);
        }
    }
    
    protected int[] makeShape(final DArray dodsArray) {
        int count = 0;
        Enumeration enumerate = dodsArray.getDimensions();
        while (enumerate.hasMoreElements()) {
            ++count;
            enumerate.nextElement();
        }
        final int[] shape = new int[count];
        enumerate = dodsArray.getDimensions();
        count = 0;
        while (enumerate.hasMoreElements()) {
            final DArrayDimension dad = enumerate.nextElement();
            shape[count++] = dad.getSize();
        }
        return shape;
    }
    
    public static String getDODSshortName(final Variable var) {
        if (var instanceof DODSVariable) {
            return ((DODSVariable)var).getDODSshortName();
        }
        if (var instanceof DODSStructure) {
            return ((DODSStructure)var).getDODSshortName();
        }
        return null;
    }
    
    private String makeDODSname(final DodsV dodsV) {
        final DodsV parent = dodsV.parent;
        if (parent.bt != null) {
            return this.makeDODSname(parent) + "." + dodsV.bt.getName();
        }
        return dodsV.bt.getName();
    }
    
    static String makeNetcdfName(final String name) {
        return StringUtil.unescape(name);
    }
    
    public static int convertToDODSType(final DataType dataType, final boolean isUnsigned) {
        if (dataType == DataType.STRING) {
            return 10;
        }
        if (dataType == DataType.BYTE) {
            return 3;
        }
        if (dataType == DataType.FLOAT) {
            return 8;
        }
        if (dataType == DataType.DOUBLE) {
            return 9;
        }
        if (dataType == DataType.SHORT) {
            return isUnsigned ? 5 : 4;
        }
        if (dataType == DataType.INT) {
            return isUnsigned ? 7 : 6;
        }
        if (dataType == DataType.BOOLEAN) {
            return 3;
        }
        if (dataType == DataType.LONG) {
            return 6;
        }
        return 10;
    }
    
    public static DataType convertToNCType(final int dodsDataType) {
        switch (dodsDataType) {
            case 3: {
                return DataType.BYTE;
            }
            case 8: {
                return DataType.FLOAT;
            }
            case 9: {
                return DataType.DOUBLE;
            }
            case 4: {
                return DataType.SHORT;
            }
            case 5: {
                return DataType.SHORT;
            }
            case 6: {
                return DataType.INT;
            }
            case 7: {
                return DataType.INT;
            }
            default: {
                return DataType.STRING;
            }
        }
    }
    
    public static boolean isUnsigned(final int dodsDataType) {
        return dodsDataType == 3 || dodsDataType == 5 || dodsDataType == 7;
    }
    
    public static DataType convertToNCType(final BaseType dtype) {
        if (dtype instanceof DString) {
            return DataType.STRING;
        }
        if (dtype instanceof DStructure || dtype instanceof DSequence || dtype instanceof DGrid) {
            return DataType.STRUCTURE;
        }
        if (dtype instanceof DFloat32) {
            return DataType.FLOAT;
        }
        if (dtype instanceof DFloat64) {
            return DataType.DOUBLE;
        }
        if (dtype instanceof DUInt32) {
            return DataType.INT;
        }
        if (dtype instanceof DUInt16) {
            return DataType.SHORT;
        }
        if (dtype instanceof DInt32) {
            return DataType.INT;
        }
        if (dtype instanceof DInt16) {
            return DataType.SHORT;
        }
        if (dtype instanceof DByte) {
            return DataType.BYTE;
        }
        throw new IllegalArgumentException("DODSVariable illegal type = " + dtype.getTypeName());
    }
    
    public static boolean isUnsigned(final BaseType dtype) {
        return dtype instanceof DByte || dtype instanceof DUInt16 || dtype instanceof DUInt32;
    }
    
    DataDDS readDataDDSfromServer(String CE) throws IOException, ParseException, DAP2Exception {
        if (DODSNetcdfFile.debugServerCall) {
            System.out.println("DODSNetcdfFile.readDataDDSfromServer = <" + CE + ">");
        }
        long start = 0L;
        if (DODSNetcdfFile.debugTime) {
            start = System.currentTimeMillis();
        }
        if (!CE.startsWith("?")) {
            CE = "?" + CE;
        }
        final DataDDS data = this.dodsConnection.getData(CE, (StatusUI)null);
        if (DODSNetcdfFile.debugTime) {
            System.out.println("DODSNetcdfFile.readDataDDSfromServer took = " + (System.currentTimeMillis() - start) / 1000.0);
        }
        if (DODSNetcdfFile.debugDataResult) {
            System.out.println(" dataDDS return:");
            data.print((OutputStream)System.out);
        }
        return data;
    }
    
    @Override
    public List<Array> readArrays(final List<Variable> preloadVariables) throws IOException {
        if (preloadVariables.size() == 0) {
            return new ArrayList<Array>();
        }
        final List<DodsV> reqDodsVlist = new ArrayList<DodsV>();
        for (final Variable var : preloadVariables) {
            if (var.hasCachedData()) {
                continue;
            }
            reqDodsVlist.add((DodsV)var.getSPobject());
        }
        Collections.sort(reqDodsVlist);
        final Map<DodsV, DodsV> map = new HashMap<DodsV, DodsV>(2 * reqDodsVlist.size() + 1);
        if (reqDodsVlist.size() > 0) {
            final StringBuilder requestString = new StringBuilder();
            for (int i = 0; i < reqDodsVlist.size(); ++i) {
                final DodsV dodsV = reqDodsVlist.get(i);
                requestString.append((i == 0) ? "?" : ",");
                requestString.append(this.makeDODSname(dodsV));
            }
            DodsV root;
            try {
                final DataDDS dataDDS = this.readDataDDSfromServer(requestString.toString());
                root = DodsV.parseDataDDS(dataDDS);
            }
            catch (Exception exc) {
                DODSNetcdfFile.logger.error("ERROR readDataDDSfromServer on " + (Object)requestString, exc);
                throw new IOException(exc.getMessage());
            }
            for (final DodsV ddsV : reqDodsVlist) {
                final DodsV dataV = root.findDataV(ddsV);
                if (dataV != null) {
                    if (DODSNetcdfFile.debugConvertData) {
                        System.out.println("readArray found dataV= " + this.makeDODSname(ddsV));
                    }
                    dataV.isDone = true;
                    map.put(ddsV, dataV);
                }
                else {
                    DODSNetcdfFile.logger.error("ERROR findDataV cant find " + this.makeDODSname(ddsV) + " on " + this.location);
                }
            }
        }
        final List<Array> result = new ArrayList<Array>();
        for (final Variable var2 : preloadVariables) {
            if (var2.hasCachedData()) {
                result.add(var2.read());
            }
            else {
                Array data = null;
                final DodsV ddsV2 = (DodsV)var2.getSPobject();
                DodsV dataV2 = map.get(ddsV2);
                if (dataV2 == null) {
                    DODSNetcdfFile.logger.error("DODSNetcdfFile.readArrays cant find " + this.makeDODSname(ddsV2) + " in dataDDS; " + this.location);
                }
                else {
                    if (DODSNetcdfFile.debugConvertData) {
                        System.out.println("readArray converting " + this.makeDODSname(ddsV2));
                    }
                    dataV2.isDone = true;
                    try {
                        if (var2.isMemberOfStructure()) {
                            while (dataV2.parent != null && dataV2.parent.bt != null) {
                                dataV2 = dataV2.parent;
                            }
                            data = this.convertD2N.convertNestedVariable(var2, null, dataV2, true);
                        }
                        else {
                            data = this.convertD2N.convertTopVariable(var2, null, dataV2);
                        }
                    }
                    catch (DAP2Exception de) {
                        DODSNetcdfFile.logger.error("ERROR convertVariable on " + var2.getName(), (Throwable)de);
                        throw new IOException(de.getMessage());
                    }
                    if (var2.isCaching()) {
                        var2.setCachedData(data);
                        if (DODSNetcdfFile.debugCached) {
                            System.out.println(" cache for <" + var2.getName() + "> length =" + data.getSize());
                        }
                    }
                }
                result.add(data);
            }
        }
        return result;
    }
    
    @Override
    public Array readSection(final String variableSection) throws IOException, InvalidRangeException {
        final ParsedSectionSpec cer = ParsedSectionSpec.parseVariableSection(this, variableSection);
        if (this.unlocked) {
            throw new IllegalStateException("File is unlocked - cannot use");
        }
        return this.readData(cer.v, cer.section);
    }
    
    @Override
    protected Array readData(final Variable v, final Section section) throws IOException, InvalidRangeException {
        if (this.unlocked) {
            throw new IllegalStateException("File is unlocked - cannot use");
        }
        final StringBuilder buff = new StringBuilder(100);
        buff.setLength(0);
        buff.append(getDODSshortName(v));
        if (!v.isVariableLength()) {
            List<Range> dodsSection = section.getRanges();
            if (v.getDataType() == DataType.CHAR) {
                final int n = section.getRank();
                if (n == v.getRank()) {
                    dodsSection = dodsSection.subList(0, n - 1);
                }
            }
            this.makeSelector(buff, dodsSection);
        }
        Array dataArray;
        try {
            final DataDDS dataDDS = this.readDataDDSfromServer(buff.toString());
            final DodsV root = DodsV.parseDataDDS(dataDDS);
            final DodsV want = root.children.get(0);
            dataArray = this.convertD2N.convertTopVariable(v, section.getRanges(), want);
        }
        catch (DAP2Exception ex) {
            ex.printStackTrace();
            throw new IOException(ex.getMessage());
        }
        catch (ParseException ex2) {
            ex2.printStackTrace();
            throw new IOException(ex2.getMessage());
        }
        return dataArray;
    }
    
    public long readToByteChannel(final Variable v, final Section section, final WritableByteChannel channel) throws IOException, InvalidRangeException {
        if (this.unlocked) {
            throw new IllegalStateException("File is unlocked - cannot use");
        }
        final Array result = this.readData(v, section);
        return IospHelper.transferData(result, channel);
    }
    
    public Array readWithCE(final Variable v, final String CE) throws IOException {
        Array dataArray;
        try {
            final DataDDS dataDDS = this.readDataDDSfromServer(CE);
            final DodsV root = DodsV.parseDataDDS(dataDDS);
            final DodsV want = root.children.get(0);
            if (v.isMemberOfStructure()) {
                dataArray = this.convertD2N.convertNestedVariable(v, null, want, true);
            }
            else {
                dataArray = this.convertD2N.convertTopVariable(v, null, want);
            }
        }
        catch (DAP2Exception ex) {
            ex.printStackTrace();
            throw new IOException(ex.getMessage());
        }
        catch (ParseException ex2) {
            ex2.printStackTrace();
            throw new IOException(ex2.getMessage());
        }
        return dataArray;
    }
    
    private int addParents(final StringBuilder buff, final Variable s, final List<Range> section, int start) {
        final Structure parent = s.getParentStructure();
        if (parent != null) {
            start = this.addParents(buff, parent, section, start);
            buff.append(".");
        }
        final List<Range> subSection = section.subList(start, start + s.getRank());
        buff.append(getDODSshortName(s));
        if (!s.isVariableLength()) {
            this.makeSelector(buff, subSection);
        }
        return start + s.getRank();
    }
    
    private void makeSelector(final StringBuilder buff, final List<Range> section) {
        for (final Range r : section) {
            buff.append("[");
            buff.append(r.first());
            buff.append(':');
            buff.append(r.stride());
            buff.append(':');
            buff.append(r.last());
            buff.append("]");
        }
    }
    
    @Override
    public void getDetailInfo(final Formatter f) {
        super.getDetailInfo(f);
        f.format("DDS = %n", new Object[0]);
        ByteArrayOutputStream buffOS = new ByteArrayOutputStream(8000);
        this.dds.print((OutputStream)buffOS);
        f.format("%s%n", buffOS.toString());
        f.format("%nDAS = %n", new Object[0]);
        buffOS = new ByteArrayOutputStream(8000);
        this.das.print((OutputStream)buffOS);
        f.format("%s%n", buffOS.toString());
    }
    
    @Override
    public String getFileTypeId() {
        return "OPeNDAP";
    }
    
    @Override
    public String getFileTypeDescription() {
        return "Open-source Project for a Network Data Access Protocol";
    }
    
    public static void main(final String[] arg) {
        final String url = "http://localhost:8080/thredds/dodsC/testContent/testData.nc.ascii?reftime[0:1:0]";
        try {
            final DODSNetcdfFile df = new DODSNetcdfFile(url, null);
            System.out.println("dods file = " + url + "\n" + df);
        }
        catch (Exception ioe) {
            System.out.println("error = " + url);
            ioe.printStackTrace();
        }
    }
    
    static {
        DODSNetcdfFile.debugCE = false;
        DODSNetcdfFile.debugServerCall = false;
        DODSNetcdfFile.debugOpenResult = false;
        DODSNetcdfFile.debugDataResult = false;
        DODSNetcdfFile.debugCharArray = false;
        DODSNetcdfFile.debugConvertData = false;
        DODSNetcdfFile.debugConstruct = false;
        DODSNetcdfFile.debugPreload = false;
        DODSNetcdfFile.debugTime = false;
        DODSNetcdfFile.showNCfile = false;
        DODSNetcdfFile.debugAttributes = false;
        DODSNetcdfFile.debugCached = false;
        DODSNetcdfFile.accept_compress = false;
        DODSNetcdfFile.preload = true;
        DODSNetcdfFile.useGroups = false;
        DODSNetcdfFile.preloadCoordVarSize = 50000;
        DODSNetcdfFile.logger = LoggerFactory.getLogger(DODSNetcdfFile.class);
    }
}
