// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dods;

import opendap.dap.DArray;
import java.util.List;
import ucar.nc2.Dimension;
import java.util.ArrayList;
import ucar.ma2.DataType;
import ucar.nc2.Attribute;
import opendap.dap.BaseType;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Structure;
import ucar.nc2.Group;
import ucar.nc2.Variable;

public class DODSVariable extends Variable
{
    protected String CE;
    protected DODSNetcdfFile dodsfile;
    protected String dodsShortName;
    
    DODSVariable(final DODSNetcdfFile dodsfile, final Group parentGroup, final Structure parentStructure, final String dodsShortName, final DodsV dodsV) {
        super(dodsfile, parentGroup, parentStructure, DODSNetcdfFile.makeNetcdfName(dodsShortName));
        this.dodsfile = dodsfile;
        this.dodsShortName = dodsShortName;
        this.setSPobject(dodsV);
    }
    
    DODSVariable(final DODSNetcdfFile dodsfile, final Group parentGroup, final Structure parentStructure, final String dodsShortName, final BaseType dodsScalar, final DodsV dodsV) {
        super(dodsfile, parentGroup, parentStructure, DODSNetcdfFile.makeNetcdfName(dodsShortName));
        this.dodsfile = dodsfile;
        this.dodsShortName = dodsShortName;
        this.setDataType(DODSNetcdfFile.convertToNCType(dodsScalar));
        if (DODSNetcdfFile.isUnsigned(dodsScalar)) {
            this.addAttribute(new Attribute("_Unsigned", "true"));
        }
        final Dimension strlenDim;
        if (this.dataType == DataType.STRING && null != (strlenDim = dodsfile.getNetcdfStrlenDim(this))) {
            final List<Dimension> dims = new ArrayList<Dimension>();
            if (strlenDim.getLength() != 0) {
                dims.add(dodsfile.getSharedDimension(parentGroup, strlenDim));
            }
            this.setDimensions(dims);
            this.setDataType(DataType.CHAR);
        }
        else {
            this.shape = new int[0];
        }
        this.setSPobject(dodsV);
    }
    
    DODSVariable(final DODSNetcdfFile dodsfile, final Group parentGroup, final Structure parentStructure, final String dodsShortName, final DArray dodsArray, final BaseType elemType, final DodsV dodsV) {
        super(dodsfile, parentGroup, parentStructure, DODSNetcdfFile.makeNetcdfName(dodsShortName));
        this.dodsfile = dodsfile;
        this.dodsShortName = dodsShortName;
        this.setDataType(DODSNetcdfFile.convertToNCType(elemType));
        if (DODSNetcdfFile.isUnsigned(elemType)) {
            this.addAttribute(new Attribute("_Unsigned", "true"));
        }
        final List<Dimension> dims = dodsfile.constructDimensions(parentGroup, dodsArray);
        final Dimension strlenDim;
        if (this.dataType == DataType.STRING && null != (strlenDim = dodsfile.getNetcdfStrlenDim(this))) {
            if (strlenDim.getLength() != 0) {
                dims.add(dodsfile.getSharedDimension(parentGroup, strlenDim));
            }
            this.setDataType(DataType.CHAR);
        }
        this.setDimensions(dims);
        this.setSPobject(dodsV);
    }
    
    @Override
    protected Variable copy() {
        return new DODSVariable(this);
    }
    
    protected DODSVariable(final DODSVariable from) {
        super(from);
        this.dodsfile = from.dodsfile;
        this.dodsShortName = from.dodsShortName;
        this.CE = from.CE;
    }
    
    protected void setCE(final String CE) {
        this.CE = CE;
    }
    
    protected boolean hasCE() {
        return this.CE != null;
    }
    
    protected String nameWithCE() {
        return this.hasCE() ? (this.getShortName() + this.CE) : this.getShortName();
    }
    
    protected String getDODSshortName() {
        return this.dodsShortName;
    }
}
