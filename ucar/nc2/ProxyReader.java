// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2;

import ucar.ma2.InvalidRangeException;
import ucar.ma2.Section;
import java.io.IOException;
import ucar.ma2.Array;
import ucar.nc2.util.CancelTask;

public interface ProxyReader
{
    Array reallyRead(final Variable p0, final CancelTask p1) throws IOException;
    
    Array reallyRead(final Variable p0, final Section p1, final CancelTask p2) throws IOException, InvalidRangeException;
}
