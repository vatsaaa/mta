// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2;

import ucar.nc2.util.URLnaming;
import ucar.unidata.util.StringUtil;
import java.util.Iterator;
import ucar.ma2.StructureMembers;
import ucar.ma2.IndexIterator;
import ucar.ma2.Index;
import ucar.ma2.StructureData;
import ucar.ma2.ArrayObject;
import ucar.ma2.ArrayChar;
import ucar.nc2.util.Indent;
import ucar.ma2.Array;
import java.io.PrintStream;
import java.io.ByteArrayOutputStream;
import java.util.Collection;
import ucar.ma2.Section;
import ucar.ma2.InvalidRangeException;
import java.util.List;
import ucar.ma2.Range;
import java.util.ArrayList;
import java.io.Writer;
import java.io.PrintWriter;
import java.io.OutputStreamWriter;
import java.io.FileNotFoundException;
import java.util.StringTokenizer;
import java.io.IOException;
import ucar.nc2.util.CancelTask;
import java.io.OutputStream;

public class NCdump
{
    private static boolean debugSelector;
    private static String usage;
    private static int totalWidth;
    private static char[] org;
    private static String[] replace;
    
    public static boolean printHeader(final String fileName, final OutputStream out) throws IOException {
        return print(fileName, out, false, false, false, false, null, null);
    }
    
    public static boolean printNcML(final String fileName, final OutputStream out) throws IOException {
        return print(fileName, out, false, true, true, false, null, null);
    }
    
    public static boolean print(final String command, final OutputStream out) throws IOException {
        return print(command, out, null);
    }
    
    public static boolean print(String command, final OutputStream out, final CancelTask ct) throws IOException {
        final StringTokenizer stoke = new StringTokenizer(command);
        if (!stoke.hasMoreTokens()) {
            out.write(NCdump.usage.getBytes());
            return false;
        }
        final String filename = stoke.nextToken();
        NetcdfFile nc = null;
        try {
            nc = NetcdfFile.open(filename, ct);
            final int pos = command.indexOf(filename);
            command = command.substring(pos + filename.length());
            return print(nc, command, out, ct);
        }
        catch (FileNotFoundException e) {
            final String mess = "file not found= " + filename;
            out.write(mess.getBytes());
            return false;
        }
        finally {
            if (nc != null) {
                nc.close();
            }
        }
    }
    
    public static boolean print(final NetcdfFile nc, final String command, final OutputStream out, final CancelTask ct) throws IOException {
        boolean showAll = false;
        boolean showCoords = false;
        boolean ncml = false;
        boolean strict = false;
        String varNames = null;
        if (command != null) {
            final StringTokenizer stoke = new StringTokenizer(command);
            while (stoke.hasMoreTokens()) {
                final String toke = stoke.nextToken();
                if (toke.equalsIgnoreCase("-help")) {
                    out.write(NCdump.usage.getBytes());
                    out.write(10);
                    return true;
                }
                if (toke.equalsIgnoreCase("-vall")) {
                    showAll = true;
                }
                if (toke.equalsIgnoreCase("-c")) {
                    showCoords = true;
                }
                if (toke.equalsIgnoreCase("-ncml")) {
                    ncml = true;
                }
                if (toke.equalsIgnoreCase("-cdl")) {
                    strict = true;
                }
                if (!toke.equalsIgnoreCase("-v") || !stoke.hasMoreTokens()) {
                    continue;
                }
                varNames = stoke.nextToken();
            }
        }
        return print(nc, out, showAll, showCoords, ncml, strict, varNames, ct);
    }
    
    public static boolean print(final String fileName, final OutputStream out, final boolean showAll, final boolean showCoords, final boolean ncml, final boolean strict, final String varNames, final CancelTask ct) throws IOException {
        NetcdfFile nc = null;
        try {
            nc = NetcdfFile.open(fileName, ct);
            return print(nc, out, showAll, showCoords, ncml, strict, varNames, ct);
        }
        catch (FileNotFoundException e) {
            final String mess = "file not found= " + fileName;
            out.write(mess.getBytes());
            return false;
        }
        finally {
            if (nc != null) {
                nc.close();
            }
        }
    }
    
    public static boolean print(final NetcdfFile nc, final OutputStream out, final boolean showAll, final boolean showCoords, final boolean ncml, final boolean strict, final String varNames, final CancelTask ct) throws IOException {
        final PrintWriter pw = new PrintWriter(new OutputStreamWriter(out));
        return NCdumpW.print(nc, pw, showAll, showCoords, ncml, strict, varNames, ct);
    }
    
    private static CEresult parseVariableSection(final NetcdfFile ncfile, final String variableSection) throws InvalidRangeException {
        final StringTokenizer stoke = new StringTokenizer(variableSection, ".");
        String selector = stoke.nextToken();
        if (selector == null) {
            throw new IllegalArgumentException("empty sectionSpec = " + variableSection);
        }
        boolean hasInner = false;
        final List<Range> fullSelection = new ArrayList<Range>();
        Variable innerV = parseVariableSelector(ncfile, selector, fullSelection);
        while (stoke.hasMoreTokens()) {
            selector = stoke.nextToken();
            innerV = parseVariableSelector(innerV, selector, fullSelection);
            hasInner = true;
        }
        return new CEresult(innerV, fullSelection, hasInner);
    }
    
    private static Variable parseVariableSelector(final Object parent, final String selector, final List<Range> fullSelection) throws InvalidRangeException {
        String indexSelect = null;
        final int pos1 = selector.indexOf(40);
        String varName;
        if (pos1 < 0) {
            varName = selector;
        }
        else {
            varName = selector.substring(0, pos1);
            indexSelect = selector.substring(pos1);
        }
        if (NCdump.debugSelector) {
            System.out.println(" parseVariableSection <" + selector + "> = <" + varName + ">, <" + indexSelect + ">");
        }
        Variable v = null;
        if (parent instanceof NetcdfFile) {
            final NetcdfFile ncfile = (NetcdfFile)parent;
            v = ncfile.findVariable(varName);
        }
        else if (parent instanceof Structure) {
            final Structure s = (Structure)parent;
            v = s.findVariable(varName);
        }
        if (v == null) {
            throw new IllegalArgumentException(" cant find variable: " + varName + " in selector=" + selector);
        }
        Section section;
        if (indexSelect != null) {
            section = new Section(indexSelect);
            section.setDefaults(v.getShape());
        }
        else {
            section = new Section(v.getShape());
        }
        fullSelection.addAll(section.getRanges());
        return v;
    }
    
    public static String makeSectionString(final VariableIF v, final List<Range> ranges) throws InvalidRangeException {
        final StringBuilder sb = new StringBuilder();
        makeSpec(sb, v, ranges);
        return sb.toString();
    }
    
    private static List<Range> makeSpec(final StringBuilder sb, final VariableIF v, List<Range> orgRanges) throws InvalidRangeException {
        if (v.isMemberOfStructure()) {
            orgRanges = makeSpec(sb, v.getParentStructure(), orgRanges);
            sb.append('.');
        }
        final List<Range> ranges = (orgRanges == null) ? v.getRanges() : orgRanges;
        sb.append(v.isMemberOfStructure() ? v.getShortName() : v.getNameEscaped());
        if (!v.isVariableLength() && !v.isScalar()) {
            sb.append('(');
            for (int count = 0; count < v.getRank(); ++count) {
                Range r = ranges.get(count);
                if (r == null) {
                    r = new Range(0, v.getDimension(count).getLength());
                }
                if (count > 0) {
                    sb.append(", ");
                }
                sb.append(r.first());
                sb.append(':');
                sb.append(r.last());
                sb.append(':');
                sb.append(r.stride());
            }
            sb.append(')');
        }
        return (orgRanges == null) ? null : ranges.subList(v.getRank(), ranges.size());
    }
    
    public static String printVariableData(final VariableIF v, final CancelTask ct) throws IOException {
        final Array data = v.read();
        final ByteArrayOutputStream bos = new ByteArrayOutputStream();
        printArray(data, v.getName(), new PrintStream(bos), ct);
        return bos.toString();
    }
    
    public static String printVariableDataSection(final VariableIF v, final String sectionSpec, final CancelTask ct) throws IOException, InvalidRangeException {
        final Array data = v.read(sectionSpec);
        final ByteArrayOutputStream bos = new ByteArrayOutputStream();
        printArray(data, v.getName(), new PrintStream(bos), ct);
        return bos.toString();
    }
    
    public static void printArray(final Array array, final String name, final PrintStream out, final CancelTask ct) {
        printArray(array, name, null, out, new Indent(2), ct);
    }
    
    private static void printArray(final Array array, final String name, final String units, final PrintStream out, final Indent ilev, final CancelTask ct) {
        if (ct != null && ct.isCancel()) {
            return;
        }
        if (name != null) {
            out.print(ilev + name + " =");
        }
        ilev.incr();
        if (array == null) {
            throw new IllegalArgumentException("null array");
        }
        if (array instanceof ArrayChar && array.getRank() > 0) {
            printStringArray(out, (ArrayChar)array, ilev, ct);
        }
        else if (array.getElementType() == String.class) {
            printStringArray(out, (ArrayObject)array, ilev, ct);
        }
        else if (array.getElementType() == StructureData.class) {
            if (array.getSize() == 1L) {
                printStructureData(out, (StructureData)array.getObject(array.getIndex()), ilev, ct);
            }
            else {
                printStructureDataArray(out, array, ilev, ct);
            }
        }
        else {
            printArray(array, out, ilev, ct);
        }
        if (units != null) {
            out.print(" " + units);
        }
        out.print("\n");
        ilev.decr();
    }
    
    private static void printArray(final Array ma, final PrintStream out, final Indent indent, final CancelTask ct) {
        if (ct != null && ct.isCancel()) {
            return;
        }
        final int rank = ma.getRank();
        final Index ima = ma.getIndex();
        if (rank == 0) {
            out.print(ma.getObject(ima).toString());
            return;
        }
        final int[] dims = ma.getShape();
        final int last = dims[0];
        out.print("\n" + indent + "{");
        if (rank == 1 && ma.getElementType() != StructureData.class) {
            for (int ii = 0; ii < last; ++ii) {
                out.print(ma.getObject(ima.set(ii)).toString());
                if (ii != last - 1) {
                    out.print(", ");
                }
                if (ct != null && ct.isCancel()) {
                    return;
                }
            }
            out.print("}");
            return;
        }
        indent.incr();
        for (int ii = 0; ii < last; ++ii) {
            final Array slice = ma.slice(0, ii);
            printArray(slice, out, indent, ct);
            if (ii != last - 1) {
                out.print(",");
            }
            if (ct != null && ct.isCancel()) {
                return;
            }
        }
        indent.decr();
        out.print("\n" + indent + "}");
    }
    
    static void printStringArray(final PrintStream out, final ArrayChar ma, final Indent indent, final CancelTask ct) {
        if (ct != null && ct.isCancel()) {
            return;
        }
        final int rank = ma.getRank();
        if (rank == 1) {
            out.print("  \"" + ma.getString() + "\"");
            return;
        }
        if (rank == 2) {
            boolean first = true;
            final ArrayChar.StringIterator iter = ma.getStringIterator();
            while (iter.hasNext()) {
                if (!first) {
                    out.print(", ");
                }
                out.print("\"" + iter.next() + "\"");
                first = false;
                if (ct != null && ct.isCancel()) {
                    return;
                }
            }
            return;
        }
        final int[] dims = ma.getShape();
        final int last = dims[0];
        out.print("\n" + indent + "{");
        indent.incr();
        for (int ii = 0; ii < last; ++ii) {
            final ArrayChar slice = (ArrayChar)ma.slice(0, ii);
            printStringArray(out, slice, indent, ct);
            if (ii != last - 1) {
                out.print(",");
            }
            if (ct != null && ct.isCancel()) {
                return;
            }
        }
        indent.decr();
        out.print("\n" + indent + "}");
    }
    
    static void printStringArray(final PrintStream out, final ArrayObject ma, final Indent indent, final CancelTask ct) {
        if (ct != null && ct.isCancel()) {
            return;
        }
        final int rank = ma.getRank();
        final Index ima = ma.getIndex();
        if (rank == 0) {
            out.print("  \"" + ma.getObject(ima) + "\"");
            return;
        }
        if (rank == 1) {
            boolean first = true;
            for (int i = 0; i < ma.getSize(); ++i) {
                if (!first) {
                    out.print(", ");
                }
                out.print("  \"" + ma.getObject(ima.set(i)) + "\"");
                first = false;
            }
            return;
        }
        final int[] dims = ma.getShape();
        final int last = dims[0];
        out.print("\n" + indent + "{");
        indent.incr();
        for (int ii = 0; ii < last; ++ii) {
            final ArrayObject slice = (ArrayObject)ma.slice(0, ii);
            printStringArray(out, slice, indent, ct);
            if (ii != last - 1) {
                out.print(",");
            }
        }
        indent.decr();
        out.print("\n" + indent + "}");
    }
    
    private static void printStructureDataArray(final PrintStream out, final Array array, final Indent indent, final CancelTask ct) {
        final IndexIterator ii = array.getIndexIterator();
        while (ii.hasNext()) {
            final StructureData sdata = (StructureData)ii.next();
            out.println("\n" + indent + "{");
            printStructureData(out, sdata, indent, ct);
            out.print(indent + "} " + sdata.getName() + "(" + ii + ")");
            if (ct != null && ct.isCancel()) {
                return;
            }
        }
    }
    
    public static void printStructureData(final PrintStream out, final StructureData sdata) {
        printStructureData(out, sdata, new Indent(2), null);
    }
    
    private static void printStructureData(final PrintStream out, final StructureData sdata, final Indent indent, final CancelTask ct) {
        indent.incr();
        for (final StructureMembers.Member m : sdata.getMembers()) {
            final Array sdataArray = sdata.getArray(m);
            printArray(sdataArray, m.getName(), m.getUnitsString(), out, indent, ct);
            if (ct != null && ct.isCancel()) {
                return;
            }
        }
        indent.decr();
    }
    
    public static void writeNcML(final NetcdfFile ncfile, final OutputStream os, final boolean showCoords, final String uri) throws IOException {
        final PrintStream out = new PrintStream(os);
        out.print("<?xml version='1.0' encoding='UTF-8'?>\n");
        out.print("<netcdf xmlns='http://www.unidata.ucar.edu/namespaces/netcdf/ncml-2.2'\n");
        if (uri != null) {
            out.print("    location='" + StringUtil.quoteXmlAttribute(uri) + "' >\n\n");
        }
        else {
            out.print("    location='" + StringUtil.quoteXmlAttribute(URLnaming.canonicalizeWrite(ncfile.getLocation())) + "' >\n\n");
        }
        if (ncfile.getId() != null) {
            out.print("    id='" + StringUtil.quoteXmlAttribute(ncfile.getId()) + "' >\n");
        }
        if (ncfile.getTitle() != null) {
            out.print("    title='" + StringUtil.quoteXmlAttribute(ncfile.getTitle()) + "' >\n");
        }
        writeNcMLGroup(ncfile, ncfile.getRootGroup(), out, new Indent(2), showCoords);
        out.print("</netcdf>\n");
        out.flush();
    }
    
    private static void writeNcMLGroup(final NetcdfFile ncfile, final Group g, final PrintStream out, final Indent indent, final boolean showCoords) throws IOException {
        if (g != ncfile.getRootGroup()) {
            out.print(indent);
            out.print("<group name='" + StringUtil.quoteXmlAttribute(g.getShortName()) + "' >\n");
        }
        indent.incr();
        final List<Dimension> dimList = g.getDimensions();
        for (final Dimension dim : dimList) {
            out.print(indent);
            out.print("<dimension name='" + StringUtil.quoteXmlAttribute(dim.getName()) + "' length='" + dim.getLength() + "'");
            if (dim.isUnlimited()) {
                out.print(" isUnlimited='true'");
            }
            out.print(" />\n");
        }
        if (dimList.size() > 0) {
            out.print("\n");
        }
        final List<Attribute> attList = g.getAttributes();
        for (final Attribute att : attList) {
            writeNcMLAtt(att, out, indent);
        }
        if (attList.size() > 0) {
            out.print("\n");
        }
        for (final Variable v : g.getVariables()) {
            if (v instanceof Structure) {
                writeNcMLStructure((Structure)v, out, indent);
            }
            else {
                writeNcMLVariable(v, out, indent, showCoords);
            }
        }
        final List groupList = g.getGroups();
        for (int i = 0; i < groupList.size(); ++i) {
            if (i > 0) {
                out.print("\n");
            }
            final Group nested = groupList.get(i);
            writeNcMLGroup(ncfile, nested, out, indent, showCoords);
        }
        indent.decr();
        if (g != ncfile.getRootGroup()) {
            out.print(indent);
            out.print("</group>\n");
        }
    }
    
    private static void writeNcMLStructure(final Structure s, final PrintStream out, final Indent indent) throws IOException {
        out.print(indent);
        out.print("<structure name='" + StringUtil.quoteXmlAttribute(s.getShortName()));
        if (s.getRank() > 0) {
            writeNcMLDimension(s, out);
        }
        out.print(">\n");
        indent.incr();
        final List<Attribute> attList = s.getAttributes();
        for (final Attribute att : attList) {
            writeNcMLAtt(att, out, indent);
        }
        if (attList.size() > 0) {
            out.print("\n");
        }
        final List<Variable> varList = s.getVariables();
        for (final Variable v : varList) {
            writeNcMLVariable(v, out, indent, false);
        }
        indent.decr();
        out.print(indent);
        out.print("</structure>\n");
    }
    
    private static void writeNcMLVariable(final Variable v, final PrintStream out, final Indent indent, final boolean showCoords) throws IOException {
        out.print(indent);
        out.print("<variable name='" + StringUtil.quoteXmlAttribute(v.getShortName()) + "' type='" + v.getDataType() + "'");
        if (v.getRank() > 0) {
            writeNcMLDimension(v, out);
        }
        indent.incr();
        boolean closed = false;
        final List<Attribute> atts = v.getAttributes();
        if (atts.size() > 0) {
            out.print(" >\n");
            closed = true;
            for (final Attribute att : atts) {
                writeNcMLAtt(att, out, indent);
            }
        }
        if (showCoords && v.isCoordinateVariable()) {
            if (!closed) {
                out.print(" >\n");
                closed = true;
            }
            writeNcMLValues(v, out, indent);
        }
        indent.decr();
        if (!closed) {
            out.print(" />\n");
        }
        else {
            out.print(indent);
            out.print("</variable>\n");
        }
    }
    
    private static void writeNcMLDimension(final Variable v, final PrintStream out) {
        out.print(" shape='");
        final List<Dimension> dims = v.getDimensions();
        for (int j = 0; j < dims.size(); ++j) {
            final Dimension dim = dims.get(j);
            if (j != 0) {
                out.print(" ");
            }
            if (dim.isShared()) {
                out.print(StringUtil.quoteXmlAttribute(dim.getName()));
            }
            else {
                out.print(dim.getLength());
            }
        }
        out.print("'");
    }
    
    private static void writeNcMLAtt(final Attribute att, final PrintStream out, final Indent indent) {
        out.print(indent);
        out.print("<attribute name='" + StringUtil.quoteXmlAttribute(att.getName()) + "' value='");
        if (att.isString()) {
            for (int i = 0; i < att.getLength(); ++i) {
                if (i > 0) {
                    out.print("\\, ");
                }
                out.print(StringUtil.quoteXmlAttribute(att.getStringValue(i)));
            }
        }
        else {
            for (int i = 0; i < att.getLength(); ++i) {
                if (i > 0) {
                    out.print(" ");
                }
                out.print(att.getNumericValue(i) + " ");
            }
            out.print("' type='" + att.getDataType());
        }
        out.print("' />\n");
    }
    
    private static void writeNcMLValues(final Variable v, final PrintStream out, final Indent indent) throws IOException {
        final Array data = v.read();
        int width = formatValues(indent + "<values>", out, 0, indent);
        final IndexIterator ii = data.getIndexIterator();
        while (ii.hasNext()) {
            width = formatValues(ii.next() + " ", out, width, indent);
        }
        formatValues("</values>\n", out, width, indent);
    }
    
    private static int formatValues(final String s, final PrintStream out, int width, final Indent indent) {
        final int len = s.length();
        if (len + width > NCdump.totalWidth) {
            out.print("\n");
            out.print(indent);
            width = indent.toString().length();
        }
        out.print(s);
        width += len;
        return width;
    }
    
    public static String encodeString(final String s) {
        return StringUtil.replace(s, NCdump.org, NCdump.replace);
    }
    
    public static void main(final String[] args) {
        if (args.length == 0) {
            System.out.println(NCdump.usage);
            return;
        }
        final StringBuilder sbuff = new StringBuilder();
        for (final String arg : args) {
            sbuff.append(arg);
            sbuff.append(" ");
        }
        try {
            print(sbuff.toString(), System.out, null);
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
    
    static {
        NCdump.debugSelector = false;
        NCdump.usage = "usage: NCdump <filename> [-cdl | -ncml] [-c | -vall] [-v varName1;varName2;..] [-v varName(0:1,:,12)]\n";
        NCdump.totalWidth = 80;
        NCdump.org = new char[] { '\b', '\f', '\n', '\r', '\t', '\\', '\'', '\"' };
        NCdump.replace = new String[] { "\\b", "\\f", "\\n", "\\r", "\\t", "\\\\", "\\'", "\\\"" };
    }
    
    private static class CEresult
    {
        public Variable v;
        public List<Range> ranges;
        public boolean hasInner;
        
        CEresult(final Variable v, final List<Range> ranges, final boolean hasInner) {
            this.v = v;
            this.ranges = ranges;
            this.hasInner = hasInner;
        }
    }
}
