// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2;

import java.util.Iterator;
import java.util.Arrays;
import java.util.Collections;
import java.util.Collection;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import net.jcip.annotations.Immutable;

@Immutable
public class EnumTypedef
{
    private final String name;
    private final Map<Integer, String> map;
    private ArrayList<String> enumStrings;
    
    public EnumTypedef(final String name, final Map<Integer, String> map) {
        this.name = name;
        this.map = map;
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getShortName() {
        return this.name;
    }
    
    public List<String> getEnumStrings() {
        if (this.enumStrings != null) {
            Collections.sort(this.enumStrings = new ArrayList<String>(this.map.values()));
        }
        return this.enumStrings;
    }
    
    public Map<Integer, String> getMap() {
        return this.map;
    }
    
    public String lookupEnumString(final int e) {
        final String result = this.map.get(e);
        return (result == null) ? ("Unknown enum value= " + e) : result;
    }
    
    public String writeCDL(final boolean strict) {
        final StringBuilder buff = new StringBuilder();
        final String name = strict ? NetcdfFile.escapeName(this.getName()) : this.getName();
        buff.append("  enum ").append(name).append(" { ");
        int count = 0;
        final List<Object> keyset = Arrays.asList(this.map.keySet().toArray());
        for (final Object key : keyset) {
            final String s = this.map.get(key);
            if (0 < count++) {
                buff.append(", ");
            }
            if (strict) {
                buff.append(NetcdfFile.escapeName(s)).append(" = ").append(key);
            }
            else {
                buff.append("'").append(s).append("' = ").append(key);
            }
        }
        buff.append("};");
        return buff.toString();
    }
}
