// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2;

import java.util.ArrayList;
import ucar.nc2.iosp.AbstractIOServiceProvider;
import java.util.Iterator;
import java.util.List;

public class Dimension implements Comparable
{
    public static Dimension VLEN;
    private boolean isUnlimited;
    private boolean isVariableLength;
    private boolean isShared;
    private String name;
    private int length;
    private boolean immutable;
    private Group g;
    private int hashCode;
    
    public static String makeDimensionList(final List<Dimension> dimList) {
        final StringBuilder out = new StringBuilder();
        for (final Dimension dim : dimList) {
            out.append(dim.getName()).append(" ");
        }
        return out.toString();
    }
    
    public String getName() {
        return this.name;
    }
    
    public int getLength() {
        return this.length;
    }
    
    public boolean isUnlimited() {
        return this.isUnlimited;
    }
    
    public boolean isVariableLength() {
        return this.isVariableLength;
    }
    
    public boolean isShared() {
        return this.isShared;
    }
    
    public Group getGroup() {
        return this.g;
    }
    
    @Override
    public boolean equals(final Object oo) {
        if (this == oo) {
            return true;
        }
        if (!(oo instanceof Dimension)) {
            return false;
        }
        final Dimension other = (Dimension)oo;
        return (this.g == null || this.g.equals(other.getGroup())) && (this.getName() != null || other.getName() == null) && (this.getName() == null || this.getName().equals(other.getName())) && this.getLength() == other.getLength() && this.isUnlimited() == other.isUnlimited() && this.isVariableLength() == other.isVariableLength() && this.isShared() == other.isShared();
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int result = 17;
            if (this.g != null) {
                result += 37 * result + this.g.hashCode();
            }
            if (null != this.getName()) {
                result += 37 * result + this.getName().hashCode();
            }
            result += 37 * result + this.getLength();
            result += 37 * result + (this.isUnlimited() ? 0 : 1);
            result += 37 * result + (this.isVariableLength() ? 0 : 1);
            result += 37 * result + (this.isShared() ? 0 : 1);
            this.hashCode = result;
        }
        return this.hashCode;
    }
    
    @Override
    public String toString() {
        return this.writeCDL(false);
    }
    
    public int compareTo(final Object o) {
        final Dimension odim = (Dimension)o;
        return this.name.compareTo(odim.getName());
    }
    
    public String writeCDL(final boolean strict) {
        final StringBuilder buff = new StringBuilder();
        final String name = strict ? NetcdfFile.escapeName(this.getName()) : this.getName();
        buff.append("   ").append(name);
        if (this.isUnlimited()) {
            buff.append(" = UNLIMITED;   // (").append(this.getLength()).append(" currently)");
        }
        else if (this.isVariableLength()) {
            buff.append(" = UNKNOWN;");
        }
        else {
            buff.append(" = ").append(this.getLength()).append(";");
        }
        return buff.toString();
    }
    
    public Dimension(final String name, final int length) {
        this(name, length, true, false, false);
    }
    
    public Dimension(final String name, final int length, final boolean isShared) {
        this(name, length, isShared, false, false);
    }
    
    public Dimension(final String name, final int length, final boolean isShared, final boolean isUnlimited, final boolean isVariableLength) {
        this.isUnlimited = false;
        this.isVariableLength = false;
        this.isShared = true;
        this.immutable = false;
        this.hashCode = 0;
        this.setName(name);
        this.isShared = isShared;
        this.isUnlimited = isUnlimited;
        this.isVariableLength = isVariableLength;
        this.setLength(length);
        assert !this.isShared;
    }
    
    public Dimension(final String name, final Dimension from) {
        this.isUnlimited = false;
        this.isVariableLength = false;
        this.isShared = true;
        this.immutable = false;
        this.hashCode = 0;
        this.setName(name);
        this.length = from.length;
        this.isUnlimited = from.isUnlimited;
        this.isVariableLength = from.isVariableLength;
        this.isShared = from.isShared;
    }
    
    public void setUnlimited(final boolean b) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        this.isUnlimited = b;
        this.setLength(this.length);
    }
    
    public void setVariableLength(final boolean b) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        this.isVariableLength = b;
        this.setLength(this.length);
    }
    
    public void setShared(final boolean b) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        this.isShared = b;
        this.hashCode = 0;
    }
    
    public void setLength(final int n) {
        if (this.immutable && !this.isUnlimited) {
            throw new IllegalStateException("Cant modify");
        }
        if (this.isVariableLength) {
            if (n != -1) {
                throw new IllegalArgumentException("VariableLength Dimension length =" + n + " must be -1");
            }
        }
        else if (this.isUnlimited) {
            if (n < 0) {
                throw new IllegalArgumentException("Unlimited Dimension length =" + n + " must >= 0");
            }
        }
        else if (n < 1) {
            throw new IllegalArgumentException("Dimension length =" + n + " must be > 0");
        }
        this.length = n;
        this.hashCode = 0;
    }
    
    public void setName(final String name) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        this.name = ((name == null || name.length() == 0) ? null : AbstractIOServiceProvider.createValidNetcdfObjectName(name));
        this.hashCode = 0;
    }
    
    public void setGroup(final Group g) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        this.g = g;
        this.hashCode = 0;
    }
    
    public Dimension setImmutable() {
        this.immutable = true;
        return this;
    }
    
    @Deprecated
    public synchronized void addCoordinateVariable(final Variable v) {
    }
    
    @Deprecated
    public List<Variable> getCoordinateVariables() {
        return new ArrayList<Variable>();
    }
    
    static {
        Dimension.VLEN = new Dimension("*", -1, true, false, true).setImmutable();
    }
}
