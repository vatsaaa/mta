// 
// Decompiled by Procyon v0.5.36
// 

package ucar.util.prefs;

import java.util.List;
import java.util.prefs.PreferenceChangeListener;

public interface PersistenceManager
{
    void addPreferenceChangeListener(final PreferenceChangeListener p0);
    
    String get(final String p0, final String p1);
    
    void put(final String p0, final String p1);
    
    boolean getBoolean(final String p0, final boolean p1);
    
    void putBoolean(final String p0, final boolean p1);
    
    double getDouble(final String p0, final double p1);
    
    void putDouble(final String p0, final double p1);
    
    int getInt(final String p0, final int p1);
    
    void putInt(final String p0, final int p1);
    
    List getList(final String p0, final List p1);
    
    void putList(final String p0, final List p1);
    
    Object getObject(final String p0);
    
    void putObject(final String p0, final Object p1);
    
    long getLong(final String p0, final long p1);
    
    void putLong(final String p0, final long p1);
}
