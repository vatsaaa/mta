// 
// Decompiled by Procyon v0.5.36
// 

package ucar.util.prefs;

import java.io.BufferedOutputStream;
import org.xml.sax.Attributes;
import java.util.Stack;
import java.util.Iterator;
import java.util.prefs.BackingStoreException;
import java.beans.XMLEncoder;
import java.io.PrintWriter;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.beans.ExceptionListener;
import java.beans.XMLDecoder;
import javax.xml.parsers.SAXParser;
import org.xml.sax.SAXException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.helpers.DefaultHandler;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.File;

public class XMLStore
{
    private static boolean debugRead;
    private static boolean debugReadValues;
    private static boolean debugWhichStore;
    private static boolean debugWriteNested;
    private static boolean debugWriteBean;
    private File prefsFile;
    private PreferencesExt rootPrefs;
    private boolean showDecoderExceptions;
    private static final int BUFF_SIZE = 1024;
    private static final String prologue = "<?xml version='1.0' encoding='UTF-8'?>\n";
    private static final String header = "<?xml version='1.0' encoding='UTF-8'?>\n<java version='1.4.1_01' class='java.beans.XMLDecoder'>\n";
    private static final String trailer = "</java>\n";
    private String outputExceptionMessage;
    private static char[] replaceChar;
    private static String[] replaceWith;
    
    public static XMLStore createFromFile(final String fileName, final XMLStore storedDefaults) throws IOException {
        final File prefsFile = new File(fileName);
        InputStream primIS = null;
        InputStream objIS = null;
        if (prefsFile.exists()) {
            primIS = new BufferedInputStream(new FileInputStream(prefsFile));
            objIS = new BufferedInputStream(new FileInputStream(prefsFile));
        }
        if (XMLStore.debugWhichStore) {
            System.out.println("XMLStore read from file " + fileName);
        }
        final XMLStore store = new XMLStore(primIS, objIS, storedDefaults);
        store.prefsFile = prefsFile;
        return store;
    }
    
    public static XMLStore createFromInputStream(final InputStream is1, final InputStream is2, final XMLStore storedDefaults) throws IOException {
        if (XMLStore.debugWhichStore) {
            System.out.println("XMLStore read from input stream " + is1);
        }
        return new XMLStore(is1, is2, storedDefaults);
    }
    
    public static XMLStore createFromResource(final String resourceName, final XMLStore storedDefaults) throws IOException {
        final Class c = XMLStore.class;
        final InputStream primIS = c.getResourceAsStream(resourceName);
        final InputStream objIS = c.getResourceAsStream(resourceName);
        if (primIS == null) {
            throw new IOException("XMLStore.createFromResource cant find <" + resourceName + ">");
        }
        if (XMLStore.debugWhichStore) {
            System.out.println("XMLStore read from resource " + resourceName);
        }
        return new XMLStore(primIS, objIS, storedDefaults);
    }
    
    public XMLStore() {
        this.prefsFile = null;
        this.rootPrefs = new PreferencesExt(null, "");
        this.showDecoderExceptions = false;
    }
    
    private XMLStore(final InputStream primIS, final InputStream objIS, final XMLStore storedDefaults) throws IOException {
        this.prefsFile = null;
        this.rootPrefs = new PreferencesExt(null, "");
        this.showDecoderExceptions = false;
        if (null != primIS) {
            final SAXParserFactory factory = SAXParserFactory.newInstance();
            try {
                final SAXParser saxParser = factory.newSAXParser();
                final MySaxHandler handler = new MySaxHandler(objIS);
                saxParser.parse(primIS, handler);
            }
            catch (ParserConfigurationException e) {
                e.printStackTrace();
            }
            catch (SAXException se) {
                System.out.println("SAXException = " + se.getMessage());
                se.printStackTrace();
                final Exception see = se.getException();
                if (see != null) {
                    System.out.println("from = " + see.getMessage());
                    see.printStackTrace();
                }
            }
            catch (IOException ioe) {
                ioe.printStackTrace();
            }
            primIS.close();
            objIS.close();
        }
        if (storedDefaults != null) {
            this.rootPrefs.setStoredDefaults(storedDefaults.getPreferences());
        }
    }
    
    private XMLDecoder openBeanDecoder(final InputStream objIS) {
        try {
            final InputMunger im = new InputMunger(objIS);
            final XMLDecoder beanDecoder = new XMLDecoder(im, null, new ExceptionListener() {
                public void exceptionThrown(final Exception e) {
                    if (XMLStore.this.showDecoderExceptions) {
                        System.out.println("***XMLStore.read() got Exception= " + e.getClass().getName() + " " + e.getMessage());
                    }
                }
            });
            return beanDecoder;
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
            return null;
        }
    }
    
    public static String makeStandardFilename(final String appName, final String storeName) {
        String userHome = null;
        try {
            userHome = System.getProperty("user.home");
        }
        catch (Exception e) {
            System.out.println("XMLStore.makeStandardFilename: error System.getProperty(user.home) " + e);
        }
        if (null == userHome) {
            userHome = ".";
        }
        final String dirFilename = userHome + "/" + appName;
        final File f = new File(dirFilename);
        if (!f.exists()) {
            f.mkdirs();
        }
        return dirFilename + "/" + storeName;
    }
    
    public PreferencesExt getPreferences() {
        return this.rootPrefs;
    }
    
    public void save() throws IOException {
        if (this.prefsFile == null) {
            throw new UnsupportedOperationException("XMLStore is read-only");
        }
        final String parentFilename = this.prefsFile.getParent();
        File prefTemp;
        if (parentFilename == null) {
            prefTemp = File.createTempFile("pref", ".xml");
        }
        else {
            final File parentFile = new File(parentFilename);
            prefTemp = File.createTempFile("pref", ".xml", parentFile);
        }
        prefTemp.deleteOnExit();
        final FileOutputStream fos = new FileOutputStream(prefTemp, false);
        this.save(fos);
        fos.close();
        final File xmlBackup = new File(this.prefsFile.getAbsolutePath() + ".bak");
        if (xmlBackup.exists()) {
            xmlBackup.delete();
        }
        this.prefsFile.renameTo(xmlBackup);
        prefTemp.renameTo(this.prefsFile);
        prefTemp.delete();
    }
    
    public void save(final OutputStream out) throws IOException {
        this.outputExceptionMessage = null;
        final OutputMunger bos = new OutputMunger(out);
        final PrintWriter pw = new PrintWriter(bos);
        final XMLEncoder beanEncoder = new XMLEncoder(bos);
        beanEncoder.setExceptionListener(new ExceptionListener() {
            public void exceptionThrown(final Exception exception) {
                System.out.println("XMLStore.save() got Exception: abort saving the preferences!");
                exception.printStackTrace();
                XMLStore.this.outputExceptionMessage = exception.getMessage();
            }
        });
        pw.print("<?xml version='1.0' encoding='UTF-8'?>\n");
        pw.print("<preferences EXTERNAL_XML_VERSION='1.0'>\n");
        if (!this.rootPrefs.isUserNode()) {
            pw.print("  <root type='system'>\n");
        }
        else {
            pw.print("  <root type='user'>\n");
        }
        this.writeXmlNode(bos, pw, this.rootPrefs, beanEncoder, "  ");
        if (this.outputExceptionMessage != null) {
            throw new IOException(this.outputExceptionMessage);
        }
        pw.print("  </root>\n");
        pw.print("</preferences>\n");
        pw.flush();
    }
    
    private void writeXmlNode(final OutputMunger bos, final PrintWriter out, final PreferencesExt prefs, final XMLEncoder beanEncoder, String m) throws IOException {
        m += "  ";
        if (XMLStore.debugWriteNested) {
            System.out.println(" writeXmlNode " + prefs);
        }
        if (XMLStore.debugWriteBean) {
            final ClassLoader l = Thread.currentThread().getContextClassLoader();
            System.out.println("  ClassLoader " + l.getClass().getName());
        }
        try {
            final String[] keys = prefs.keysNoDefaults();
            if (keys.length == 0) {
                out.println(m + "<map/>");
            }
            else {
                out.println(m + "<map>");
                for (int i = 0; i < keys.length; ++i) {
                    final Object value = prefs.getObjectNoDefaults(keys[i]);
                    if (value instanceof String) {
                        if (XMLStore.debugWriteNested) {
                            System.out.println("  write entry " + keys[i] + " " + value);
                        }
                        out.println(m + "  <entry key='" + keys[i] + "' value='" + quote((String)value) + "' />");
                    }
                    else if (value instanceof Bean.Collection) {
                        final Bean.Collection bean = (Bean.Collection)value;
                        if (XMLStore.debugWriteNested) {
                            System.out.println("  write bean collection " + keys[i]);
                        }
                        if (!bean.getCollection().isEmpty()) {
                            out.println(m + "  <beanCollection key='" + keys[i] + "' class='" + bean.getBeanClass().getName() + "'>");
                            for (final Object o : bean.getCollection()) {
                                out.print(m + "    <bean ");
                                bean.writeProperties(out, o);
                                out.println("/>");
                            }
                            out.println(m + "  </beanCollection>");
                        }
                    }
                    else if (value instanceof Bean) {
                        final Bean bean2 = (Bean)value;
                        if (XMLStore.debugWriteNested) {
                            System.out.println("  write bean " + keys[i] + " " + value);
                        }
                        out.print(m + "  <bean key='" + keys[i] + "' class='" + bean2.getBeanClass().getName() + "' ");
                        bean2.writeProperties(out);
                        out.println("/>");
                    }
                    else {
                        out.println(m + "  <beanObject key='" + keys[i] + "' >");
                        out.flush();
                        bos.enterBeanStream();
                        try {
                            if (XMLStore.debugWriteNested || XMLStore.debugWriteBean) {
                                System.out.println("  write beanObject " + keys[i] + " " + value + " " + value.getClass().getName());
                            }
                            beanEncoder.writeObject(value);
                            if (XMLStore.debugWriteBean) {
                                System.out.println("  write bean done ");
                            }
                        }
                        catch (Exception e) {
                            System.out.println("Exception beanEncoder: " + e);
                            e.printStackTrace();
                            throw new IOException(e.getMessage());
                        }
                        beanEncoder.flush();
                        bos.exitBeanStream();
                        out.println(m + "  </beanObject>");
                    }
                }
                out.println(m + "</map>");
            }
            final String[] kidName = prefs.childrenNames();
            for (int j = 0; j < kidName.length; ++j) {
                final PreferencesExt pkid = (PreferencesExt)prefs.node(kidName[j]);
                out.println(m + "<node name='" + pkid.name() + "' >");
                this.writeXmlNode(bos, out, pkid, beanEncoder, m);
                out.println(m + "</node>");
            }
        }
        catch (BackingStoreException e2) {
            e2.printStackTrace();
            throw new IOException(e2.getMessage());
        }
    }
    
    static String quote(final String x) {
        boolean ok = true;
        for (int i = 0; i < XMLStore.replaceChar.length; ++i) {
            final int pos = x.indexOf(XMLStore.replaceChar[i]);
            ok &= (pos < 0);
        }
        if (ok) {
            return x;
        }
        final StringBuffer result = new StringBuffer(x);
        for (int j = 0; j < XMLStore.replaceChar.length; ++j) {
            final int pos2 = x.indexOf(XMLStore.replaceChar[j]);
            if (pos2 >= 0) {
                replace(result, XMLStore.replaceChar[j], XMLStore.replaceWith[j]);
            }
        }
        return result.toString();
    }
    
    private static void replace(final StringBuffer sb, final char out, final String in) {
        for (int i = 0; i < sb.length(); ++i) {
            if (sb.charAt(i) == out) {
                sb.replace(i, i + 1, in);
                i += in.length();
            }
        }
    }
    
    static {
        XMLStore.debugRead = false;
        XMLStore.debugReadValues = false;
        XMLStore.debugWhichStore = false;
        XMLStore.debugWriteNested = false;
        XMLStore.debugWriteBean = false;
        XMLStore.replaceChar = new char[] { '&', '<', '>', '\'', '\"', '\r', '\n' };
        XMLStore.replaceWith = new String[] { "&amp;", "&lt;", "&gt;", "&apos;", "&quot;", "&#13;", "&#10;" };
    }
    
    private class MySaxHandler extends DefaultHandler
    {
        private boolean debug;
        private boolean debugDetail;
        private InputStream objIS;
        private XMLDecoder beanDecoder;
        private Bean.Collection currentBeanCollection;
        private Stack stack;
        private PreferencesExt current;
        
        MySaxHandler(final InputStream objIS) throws IOException {
            this.debug = false;
            this.debugDetail = false;
            this.beanDecoder = null;
            this.currentBeanCollection = null;
            this.objIS = objIS;
        }
        
        @Override
        public void startElement(final String uri, final String localName, final String qName, final Attributes attributes) throws SAXException {
            if (qName.equalsIgnoreCase("root")) {
                this.startRoot(attributes);
            }
            else if (qName.equalsIgnoreCase("map")) {
                this.startMap(attributes);
            }
            else if (qName.equalsIgnoreCase("node")) {
                this.startNode(attributes);
            }
            else if (qName.equalsIgnoreCase("entry")) {
                this.startEntry(attributes);
            }
            else if (qName.equalsIgnoreCase("bean")) {
                this.startBean(attributes);
            }
            else if (qName.equalsIgnoreCase("beanCollection")) {
                this.startBeanCollection(attributes);
            }
            else if (qName.equalsIgnoreCase("beanObject")) {
                this.startBeanObject(attributes);
            }
            else if (this.debugDetail) {
                System.out.println(" unprocessed startElement = " + qName);
            }
        }
        
        @Override
        public void endElement(final String uri, final String localName, final String qName) throws SAXException {
            if (qName.equalsIgnoreCase("node")) {
                this.endNode();
            }
            if (qName.equalsIgnoreCase("beanCollection")) {
                this.endBeanCollection();
            }
            else if (this.debugDetail) {
                System.out.println(" unprocessed endElement = " + qName);
            }
        }
        
        private void startRoot(final Attributes attributes) {
            if (this.debugDetail) {
                System.out.println(" root ");
            }
            this.stack = new Stack();
            this.current = XMLStore.this.rootPrefs;
        }
        
        private void startMap(final Attributes attributes) {
            if (this.debugDetail) {
                System.out.println(" map ");
            }
        }
        
        private void startNode(final Attributes attributes) {
            final String name = attributes.getValue("name");
            if (this.debug) {
                System.out.println(" node = " + name);
            }
            if (name.equals("dumpPane") && this.debug) {
                System.out.println("");
            }
            this.stack.push(this.current);
            this.current = (PreferencesExt)this.current.node(name);
        }
        
        private void startEntry(final Attributes attributes) {
            final String key = attributes.getValue("key");
            final String values = attributes.getValue("value");
            if (this.debug) {
                System.out.println(" entry = " + key + " " + values);
            }
            this.current.put(key, values);
        }
        
        private void startBean(final Attributes attributes) {
            final String key = attributes.getValue("key");
            try {
                if (this.currentBeanCollection != null) {
                    final Object value = this.currentBeanCollection.readProperties(attributes);
                    if (this.debug) {
                        System.out.println(" bean(collection) = " + key + " value= " + value);
                    }
                }
                else {
                    final Object value = new Bean(attributes);
                    if (this.debug) {
                        System.out.println(" bean = " + key + " value= " + value);
                    }
                    this.current.putObject(key, value);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        private void startBeanCollection(final Attributes attributes) {
            final String key = attributes.getValue("key");
            try {
                this.currentBeanCollection = new Bean.Collection(attributes);
                if (this.debug) {
                    System.out.println(" beanCollection = " + key);
                }
                this.current.putObject(key, this.currentBeanCollection);
            }
            catch (Exception ex) {}
        }
        
        private void startBeanObject(final Attributes attributes) {
            final String key = attributes.getValue("key");
            if (this.beanDecoder == null) {
                this.beanDecoder = XMLStore.this.openBeanDecoder(this.objIS);
            }
            try {
                if (this.debug) {
                    System.out.print(" beanObject = " + key + " ");
                }
                final Object value = this.beanDecoder.readObject();
                if (this.debug) {
                    System.out.println(" value= " + value);
                }
                this.current.putObject(key, value);
            }
            catch (Exception e) {
                System.out.println("#ERROR beanDecoder; beanObject key = " + key);
                e.printStackTrace();
            }
        }
        
        private void endBeanCollection() {
            this.currentBeanCollection = null;
        }
        
        private void endNode() {
            this.current = this.stack.pop();
        }
    }
    
    class InputMunger extends BufferedInputStream
    {
        private byte[] buf;
        private int count;
        private int pos;
        boolean isHeader;
        int countHeader;
        int sizeHeader;
        boolean isTrailer;
        int countTrailer;
        int sizeTrailer;
        
        InputMunger(final XMLStore xmlStore, final InputStream in) throws IOException {
            this(xmlStore, in, 1024);
        }
        
        InputMunger(final InputStream in, final int size) throws IOException {
            super(in);
            this.count = 0;
            this.pos = 0;
            this.isHeader = true;
            this.countHeader = 0;
            this.sizeHeader = "<?xml version='1.0' encoding='UTF-8'?>\n<java version='1.4.1_01' class='java.beans.XMLDecoder'>\n".length();
            this.isTrailer = false;
            this.countTrailer = 0;
            this.sizeTrailer = "</java>\n".length();
            this.buf = new byte[size];
            this.fill(0);
            this.pos = "<?xml version='1.0' encoding='UTF-8'?>\n".length();
        }
        
        @Override
        public int read(final byte[] b, final int off, final int len) throws IOException {
            for (int i = 0; i < len; ++i) {
                final int bval = this.read();
                if (bval < 0) {
                    return (i > 0) ? i : -1;
                }
                b[off + i] = (byte)bval;
            }
            return len;
        }
        
        @Override
        public int read() throws IOException {
            if (this.isHeader) {
                this.isHeader = (this.countHeader + 1 < this.sizeHeader);
                return "<?xml version='1.0' encoding='UTF-8'?>\n<java version='1.4.1_01' class='java.beans.XMLDecoder'>\n".charAt(this.countHeader++);
            }
            if (this.isTrailer) {
                return (this.countTrailer < this.sizeTrailer) ? "</java>\n".charAt(this.countTrailer++) : -1;
            }
            return this.read1();
        }
        
        private int read1() throws IOException {
            if (this.pos >= this.count) {
                this.fill(0);
                if (this.pos >= this.count) {
                    this.isTrailer = true;
                    return this.read();
                }
            }
            return this.buf[this.pos++] & 0xFF;
        }
        
        private int fill(final int save) throws IOException {
            final int start = this.count - save;
            if (save > 0) {
                System.arraycopy(this.buf, start, this.buf, 0, save);
            }
            this.pos = 0;
            this.count = save;
            final int n = this.in.read(this.buf, save, this.buf.length - save);
            if (n > 0) {
                this.count += n;
            }
            return n;
        }
    }
    
    private class OutputMunger extends BufferedOutputStream
    {
        boolean done;
        boolean bean;
        int countNL;
        
        OutputMunger(final OutputStream out) {
            super(out, 1024);
            this.done = false;
            this.bean = false;
            this.countNL = 0;
        }
        
        void enterBeanStream() {
            this.bean = true;
        }
        
        void exitBeanStream() {
            this.bean = false;
        }
        
        @Override
        public void write(final int b) throws IOException {
            if (this.done || !this.bean) {
                super.write(b);
            }
            else {
                if (b == 10) {
                    ++this.countNL;
                }
                if (this.countNL == 2) {
                    this.done = true;
                }
            }
        }
        
        @Override
        public void write(final byte[] b, final int off, final int len) throws IOException {
            if (this.done || !this.bean) {
                super.write(b, off, len);
            }
            else {
                for (int i = 0; i < len; ++i) {
                    this.write(b[off + i]);
                }
            }
        }
    }
}
