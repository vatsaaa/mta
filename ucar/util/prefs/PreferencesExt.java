// 
// Decompiled by Procyon v0.5.36
// 

package ucar.util.prefs;

import java.util.Iterator;
import java.util.prefs.BackingStoreException;
import java.util.Collections;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Collection;
import java.util.HashMap;
import java.util.prefs.Preferences;
import java.util.prefs.AbstractPreferences;

public class PreferencesExt extends AbstractPreferences implements PersistenceManager
{
    static Preferences userRoot;
    static Preferences systemRoot;
    private boolean isBackingStoreAvailable;
    private PreferencesExt parent;
    private HashMap keyValues;
    private HashMap children;
    private PreferencesExt storedDefaults;
    
    public static void setUserRoot(final PreferencesExt prefs) {
        PreferencesExt.userRoot = prefs;
    }
    
    public static void setSystemRoot(final PreferencesExt prefs) {
        PreferencesExt.systemRoot = prefs;
    }
    
    public PreferencesExt(final PreferencesExt parent, final String name) {
        super(parent, name);
        this.isBackingStoreAvailable = true;
        this.storedDefaults = null;
        this.parent = parent;
        this.keyValues = new HashMap(20);
        this.children = new HashMap(10);
    }
    
    void setStoredDefaults(final PreferencesExt storedDefaults) {
        this.storedDefaults = storedDefaults;
    }
    
    private PreferencesExt getStoredDefaults() {
        return (this.parent == null) ? this.storedDefaults : this.parent.getStoredDefaults();
    }
    
    @Override
    public boolean isUserNode() {
        return this != PreferencesExt.systemRoot;
    }
    
    public Object getBean(final String key, final Object def) {
        if (key == null) {
            throw new NullPointerException("Null key");
        }
        if (this.isRemoved()) {
            throw new IllegalStateException("Node has been removed.");
        }
        synchronized (this.lock) {
            Object result = null;
            try {
                result = this._getObject(key);
                if (result != null) {
                    if (result instanceof Bean.Collection) {
                        result = ((Bean.Collection)result).getCollection();
                    }
                    else if (result instanceof Bean) {
                        result = ((Bean)result).getObject();
                    }
                }
            }
            catch (Exception ex) {}
            return (result == null) ? def : result;
        }
    }
    
    public void putBean(final String key, final Object newValue) {
        final Object oldValue = this.getBean(key, null);
        if (oldValue == null || !oldValue.equals(newValue)) {
            this.keyValues.put(key, new Bean(newValue));
        }
    }
    
    public void putBeanCollection(final String key, final Collection newValue) {
        final Object oldValue = this.getBean(key, null);
        if (oldValue == null || !oldValue.equals(newValue)) {
            this.keyValues.put(key, new Bean.Collection(newValue));
        }
    }
    
    public void putBeanObject(final String key, final Object newValue) {
        final Object oldValue = this.getBean(key, null);
        if (oldValue == null || !oldValue.equals(newValue)) {
            this.keyValues.put(key, newValue);
        }
    }
    
    public List getList(final String key, final List def) {
        try {
            final Object bean = this.getBean(key, def);
            return (List)bean;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public void putList(final String key, final List newValue) {
        this.putBeanObject(key, newValue);
    }
    
    @Override
    protected String[] childrenNamesSpi() {
        final HashSet allKids = new HashSet(this.children.keySet());
        final PreferencesExt sd = this.getStoredDefaults();
        if (sd != null) {
            allKids.addAll(sd.childrenNamesSpi(this.absolutePath()));
        }
        final ArrayList list = new ArrayList(allKids);
        Collections.sort((List<Comparable>)list);
        final String[] result = new String[list.size()];
        for (int i = 0; i < list.size(); ++i) {
            result[i] = list.get(i).toString();
        }
        return result;
    }
    
    protected Collection childrenNamesSpi(final String nodePath) {
        final HashSet allKids = new HashSet();
        try {
            if (this.nodeExists(nodePath)) {
                final PreferencesExt node = (PreferencesExt)this.node(nodePath);
                allKids.addAll(node.children.keySet());
            }
        }
        catch (BackingStoreException ex) {}
        final PreferencesExt sd = this.getStoredDefaults();
        if (sd != null) {
            allKids.addAll(sd.childrenNamesSpi(nodePath));
        }
        return allKids;
    }
    
    String[] keysNoDefaults() throws BackingStoreException {
        final HashSet allKeys = new HashSet(this.keyValues.keySet());
        final ArrayList list = new ArrayList(allKeys);
        Collections.sort((List<Comparable>)list);
        final String[] result = new String[list.size()];
        for (int i = 0; i < list.size(); ++i) {
            result[i] = list.get(i).toString();
        }
        return result;
    }
    
    @Override
    protected String[] keysSpi() throws BackingStoreException {
        final HashSet allKeys = new HashSet(this.keyValues.keySet());
        final PreferencesExt sd = this.getStoredDefaults();
        if (sd != null) {
            allKeys.addAll(sd.keysSpi(this.absolutePath()));
        }
        final ArrayList list = new ArrayList(allKeys);
        Collections.sort((List<Comparable>)list);
        final String[] result = new String[list.size()];
        for (int i = 0; i < list.size(); ++i) {
            result[i] = list.get(i).toString();
        }
        return result;
    }
    
    protected Collection keysSpi(final String nodePath) {
        final HashSet allKeys = new HashSet();
        try {
            if (this.nodeExists(nodePath)) {
                final PreferencesExt node = (PreferencesExt)this.node(nodePath);
                allKeys.addAll(node.keyValues.keySet());
            }
        }
        catch (BackingStoreException ex) {}
        final PreferencesExt sd = this.getStoredDefaults();
        if (sd != null) {
            allKeys.addAll(sd.keysSpi(nodePath));
        }
        return allKeys;
    }
    
    void dump() throws BackingStoreException {
        final String[] keys = this.keys();
        for (int i = 0; i < keys.length; ++i) {
            System.out.println("key = " + keys[i] + " value= " + this.get(keys[i], null));
        }
        final String[] kidNames = this.childrenNames();
        for (int j = 0; j < kidNames.length; ++j) {
            ((PreferencesExt)this.node(kidNames[j])).dump();
        }
    }
    
    void show(final String what, final Collection c) {
        try {
            System.out.println("---" + what + ":");
            for (final Object o : c) {
                System.out.println("  " + o.toString() + " " + o.getClass().getName());
            }
        }
        catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        }
        System.out.println("***");
    }
    
    @Override
    protected AbstractPreferences childSpi(final String name) {
        PreferencesExt child;
        if (null != (child = this.children.get(name))) {
            return child;
        }
        child = new PreferencesExt(this, name);
        this.children.put(name, child);
        child.newNode = true;
        return child;
    }
    
    @Override
    protected void flushSpi() throws BackingStoreException {
    }
    
    @Override
    protected String getSpi(final String keyName) {
        final Object o = this._getObject(keyName);
        return (o == null) ? null : o.toString();
    }
    
    @Override
    protected void putSpi(final String key, final String newValue) {
        final String oldValue = this.getSpi(key);
        if (oldValue == null || !oldValue.equals(newValue)) {
            this.keyValues.put(key, newValue);
        }
    }
    
    @Override
    protected void removeNodeSpi() throws BackingStoreException {
        if (this.parent != null && null == this.parent.children.remove(this.name())) {
            System.out.println("ERROR PreferencesExt.removeNodeSpi :" + this.name());
        }
    }
    
    @Override
    protected void removeSpi(final String key) {
        this.keyValues.remove(key);
    }
    
    @Override
    protected void syncSpi() throws BackingStoreException {
    }
    
    Object getObjectNoDefaults(final String keyName) {
        return this.keyValues.get(keyName);
    }
    
    public void putObject(final String keyName, final Object value) {
        if (keyName == null) {
            throw new IllegalArgumentException("PreferencesExt try to store null keyname");
        }
        this.keyValues.put(keyName, value);
    }
    
    public Object getObject(final String key) {
        synchronized (this.lock) {
            Object result = null;
            try {
                result = this._getObject(key);
            }
            catch (Exception ex) {}
            return result;
        }
    }
    
    private Object _getObject(final String keyName) {
        Object result = null;
        try {
            result = this.keyValues.get(keyName);
            if (result == null) {
                final PreferencesExt sd = this.getStoredDefaults();
                if (sd != null) {
                    result = sd.getObjectFromNode(this.absolutePath(), keyName);
                }
            }
        }
        catch (Exception ex) {}
        return result;
    }
    
    private Object getObjectFromNode(final String nodePath, final String keyName) {
        Object result = null;
        try {
            if (this.nodeExists(nodePath)) {
                final PreferencesExt node = (PreferencesExt)this.node(nodePath);
                synchronized (node) {
                    result = node._getObject(keyName);
                }
            }
        }
        catch (BackingStoreException ex) {}
        final PreferencesExt sd = this.getStoredDefaults();
        if (result == null && sd != null) {
            synchronized (sd) {
                result = sd.getObjectFromNode(nodePath, keyName);
            }
        }
        return result;
    }
    
    static {
        PreferencesExt.userRoot = new PreferencesExt(null, "");
        PreferencesExt.systemRoot = new PreferencesExt(null, "");
    }
}
