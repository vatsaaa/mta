// 
// Decompiled by Procyon v0.5.36
// 

package ucar.util.prefs;

import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.beans.PropertyDescriptor;
import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.util.TreeMap;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Collection;
import java.io.IOException;
import java.io.PrintWriter;
import org.xml.sax.Attributes;

class Bean
{
    private Object o;
    private BeanParser p;
    
    public Bean(final Object o) {
        this.p = null;
        this.o = o;
    }
    
    public Bean(final Attributes atts) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        this.p = null;
        final String className = atts.getValue("class");
        final Class c = Class.forName(className);
        this.o = c.newInstance();
        (this.p = BeanParser.getParser(c)).readProperties(this.o, atts);
    }
    
    public void writeProperties(final PrintWriter out) throws IOException {
        if (this.p == null) {
            this.p = BeanParser.getParser(this.o.getClass());
        }
        this.p.writeProperties(this.o, out);
    }
    
    public Object getObject() {
        return this.o;
    }
    
    public Class getBeanClass() {
        return this.o.getClass();
    }
    
    static class Collection
    {
        private java.util.Collection collect;
        private Class beanClass;
        private BeanParser p;
        
        Collection(final java.util.Collection collect) {
            this.p = null;
            this.collect = collect;
            if (!collect.isEmpty()) {
                final Iterator iter = collect.iterator();
                this.beanClass = iter.next().getClass();
                this.p = BeanParser.getParser(this.beanClass);
            }
        }
        
        Collection(final Attributes atts) throws ClassNotFoundException {
            this.p = null;
            final String className = atts.getValue("class");
            this.beanClass = Class.forName(className);
            this.p = BeanParser.getParser(this.beanClass);
            this.collect = new ArrayList();
        }
        
        public void writeProperties(final PrintWriter out, final Object thiso) throws IOException {
            this.p.writeProperties(thiso, out);
        }
        
        public java.util.Collection getCollection() {
            return this.collect;
        }
        
        public Class getBeanClass() {
            return this.beanClass;
        }
        
        public Object readProperties(final Attributes atts) throws InstantiationException, IllegalAccessException {
            final Object o = this.beanClass.newInstance();
            this.p.readProperties(o, atts);
            this.collect.add(o);
            return o;
        }
    }
    
    private static class BeanParser
    {
        private static boolean debugBean;
        private static HashMap parsers;
        private TreeMap properties;
        private Object[] args;
        
        static BeanParser getParser(final Class beanClass) {
            BeanParser parser;
            if (null == (parser = BeanParser.parsers.get(beanClass))) {
                parser = new BeanParser(beanClass);
                BeanParser.parsers.put(beanClass, parser);
            }
            return parser;
        }
        
        BeanParser(final Class beanClass) {
            this.properties = new TreeMap();
            this.args = new Object[1];
            BeanInfo info = null;
            try {
                info = Introspector.getBeanInfo(beanClass, Object.class);
            }
            catch (IntrospectionException e) {
                e.printStackTrace();
            }
            if (BeanParser.debugBean) {
                System.out.println("Bean " + beanClass.getName());
            }
            final PropertyDescriptor[] pds = info.getPropertyDescriptors();
            for (int i = 0; i < pds.length; ++i) {
                if (pds[i].getReadMethod() != null && pds[i].getWriteMethod() != null) {
                    this.properties.put(pds[i].getName(), pds[i]);
                    if (BeanParser.debugBean) {
                        System.out.println(" property " + pds[i].getName());
                    }
                }
            }
        }
        
        void writeProperties(final Object bean, final PrintWriter out) throws IOException {
            for (final PropertyDescriptor pds : this.properties.values()) {
                final Method getter = pds.getReadMethod();
                try {
                    Object value = getter.invoke(bean, (Object[])null);
                    if (value == null) {
                        continue;
                    }
                    if (value instanceof String) {
                        value = XMLStore.quote((String)value);
                    }
                    else if (value instanceof Date) {
                        value = new Long(((Date)value).getTime());
                    }
                    out.print(pds.getName() + "='" + value + "' ");
                    if (!BeanParser.debugBean) {
                        continue;
                    }
                    System.out.println(" property get " + pds.getName() + "='" + value + "' ");
                }
                catch (InvocationTargetException e) {}
                catch (IllegalAccessException ex) {}
            }
        }
        
        void readProperties(final Object bean, final Attributes atts) {
            for (final PropertyDescriptor pds : this.properties.values()) {
                final Method setter = pds.getWriteMethod();
                try {
                    final String sArg = atts.getValue(pds.getName());
                    final Object arg = this.getArgument(pds.getPropertyType(), sArg);
                    if (BeanParser.debugBean) {
                        System.out.println(" property set " + pds.getName() + "=" + sArg + " == " + arg);
                    }
                    if (arg == null) {
                        return;
                    }
                    this.args[0] = arg;
                    setter.invoke(bean, this.args);
                }
                catch (NumberFormatException e) {}
                catch (InvocationTargetException e2) {}
                catch (IllegalAccessException ex) {}
            }
        }
        
        private Object getArgument(final Class c, final String value) {
            if (c == String.class) {
                return value;
            }
            if (c == Integer.TYPE) {
                return Integer.valueOf(value);
            }
            if (c == Double.TYPE) {
                return Double.valueOf(value);
            }
            if (c == Boolean.TYPE) {
                return Boolean.valueOf(value);
            }
            if (c == Float.TYPE) {
                return Float.valueOf(value);
            }
            if (c == Short.TYPE) {
                return Short.valueOf(value);
            }
            if (c == Long.TYPE) {
                return Long.valueOf(value);
            }
            if (c == Byte.TYPE) {
                return Byte.valueOf(value);
            }
            if (c == Date.class) {
                final long time = Long.parseLong(value);
                return new Date(time);
            }
            return null;
        }
        
        static {
            BeanParser.debugBean = false;
            BeanParser.parsers = new HashMap();
        }
    }
}
