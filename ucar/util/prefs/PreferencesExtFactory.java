// 
// Decompiled by Procyon v0.5.36
// 

package ucar.util.prefs;

import java.util.prefs.Preferences;
import java.util.prefs.PreferencesFactory;

public class PreferencesExtFactory implements PreferencesFactory
{
    public Preferences userRoot() {
        return PreferencesExt.userRoot;
    }
    
    public Preferences systemRoot() {
        return PreferencesExt.systemRoot;
    }
}
