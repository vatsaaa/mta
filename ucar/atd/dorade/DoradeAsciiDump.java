// 
// Decompiled by Procyon v0.5.36
// 

package ucar.atd.dorade;

import java.io.FileOutputStream;
import java.io.File;

public class DoradeAsciiDump
{
    public static void main(final String[] args) throws Exception {
        if (args.length < 2) {
            System.err.println("Error: usage: java ucar.unidata.data.radar.DoradeDump <parameter (e.g., VR,ZDR,DBZ)> <spol filename>");
            System.exit(1);
        }
        final String paramName = args[0];
        for (int i = 2; i < args.length; ++i) {
            final String filename = args[i];
            final File sourceFile = new File(filename);
            final File destFile = new File(sourceFile.getName() + ".txt");
            final DoradeSweep sweep = new DoradeSweep(filename);
            if (sweep.getScanMode() != ScanMode.MODE_SUR) {
                System.err.println("Skipping:" + sourceFile);
            }
            else {
                final int nRays = sweep.getNRays();
                final int nCells = sweep.getNCells(0);
                final DoradePARM param = sweep.lookupParamIgnoreCase(paramName);
                if (param == null) {
                    System.err.println("Error: Could not find given paramter:" + paramName);
                    System.exit(1);
                }
                final float[] azimuths = sweep.getAzimuths();
                final float[] elevations = sweep.getElevations();
                final StringBuffer sb = new StringBuffer();
                System.err.println("File:" + sourceFile + " #rays:" + nRays + " #cells:" + nCells);
                for (int rayIdx = 0; rayIdx < nRays; ++rayIdx) {
                    sb.append("ray:" + rayIdx + " " + elevations[rayIdx] + " " + azimuths[rayIdx] + "\n");
                    final float[] rayValues = sweep.getRayData(param, rayIdx);
                    for (int cellIdx = 0; cellIdx < rayValues.length; ++cellIdx) {
                        if (cellIdx > 0) {
                            sb.append(",");
                        }
                        sb.append("" + rayValues[cellIdx]);
                    }
                    sb.append("\n");
                }
                final FileOutputStream out = new FileOutputStream(destFile);
                out.write(sb.toString().getBytes());
                out.flush();
                out.close();
            }
        }
    }
}
