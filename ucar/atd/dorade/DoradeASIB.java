// 
// Decompiled by Procyon v0.5.36
// 

package ucar.atd.dorade;

import java.io.RandomAccessFile;

class DoradeASIB extends DoradeDescriptor
{
    private float longitude;
    private float latitude;
    private float altitudeMSL;
    private float altitudeAGL;
    private float groundSpeedEW;
    private float groundSpeedNS;
    private float verticalVelocity;
    private float antennaHeading;
    private float rollAngle;
    private float pitchAngle;
    private float yawAngle;
    private float antennaScanAngle;
    private float antennaFixedAngle;
    private float uWind;
    private float vWind;
    private float wWind;
    private float headingChangeRate;
    private float pitchChangeRate;
    
    public DoradeASIB(final RandomAccessFile file, final boolean littleEndianData) throws DescriptorException {
        final byte[] data = this.readDescriptor(file, littleEndianData, "ASIB");
        this.longitude = this.grabFloat(data, 8);
        this.latitude = this.grabFloat(data, 12);
        this.altitudeMSL = this.grabFloat(data, 16);
        this.altitudeAGL = this.grabFloat(data, 20);
        this.groundSpeedEW = this.grabFloat(data, 24);
        this.groundSpeedNS = this.grabFloat(data, 28);
        this.verticalVelocity = this.grabFloat(data, 32);
        this.antennaHeading = this.grabFloat(data, 36);
        this.rollAngle = this.grabFloat(data, 40);
        this.pitchAngle = this.grabFloat(data, 44);
        this.yawAngle = this.grabFloat(data, 48);
        this.antennaScanAngle = this.grabFloat(data, 52);
        this.antennaFixedAngle = this.grabFloat(data, 56);
        this.uWind = this.grabFloat(data, 60);
        this.vWind = this.grabFloat(data, 64);
        this.wWind = this.grabFloat(data, 68);
        this.headingChangeRate = this.grabFloat(data, 72);
        this.pitchChangeRate = this.grabFloat(data, 76);
        if (this.verbose) {
            System.out.println(this);
        }
    }
    
    @Override
    public String toString() {
        String s = "ASIB\n";
        s = s + "  longitude: " + this.longitude + "\n";
        s = s + "  latitude: " + this.latitude + "\n";
        s = s + "  altitude (MSL): " + this.altitudeMSL + "\n";
        s = s + "  altitude (AGL): " + this.altitudeAGL + "\n";
        s = s + "  EW ground speed: " + this.groundSpeedEW + "\n";
        s = s + "  NS ground speed: " + this.groundSpeedNS + "\n";
        s = s + "  vertical velocity: " + this.verticalVelocity + "\n";
        s = s + "  antenna heading: " + this.antennaHeading + "\n";
        s = s + "  roll: " + this.rollAngle + "\n";
        s = s + "  pitch: " + this.pitchAngle + "\n";
        s = s + "  yaw: " + this.yawAngle + "\n";
        s = s + "  scan angle: " + this.antennaScanAngle + "\n";
        s = s + "  fixed angle: " + this.antennaFixedAngle + "\n";
        s = s + "  u wind: " + this.uWind + "\n";
        s = s + "  v wind: " + this.vWind + "\n";
        s = s + "  w wind: " + this.wWind + "\n";
        s = s + "  heading change rate: " + this.headingChangeRate + "\n";
        s = s + "  pitch change rate: " + this.pitchChangeRate;
        return s;
    }
    
    public float getLatitude() {
        return this.latitude;
    }
    
    public float getLongitude() {
        return this.longitude;
    }
    
    public float getAltitude() {
        return this.altitudeMSL;
    }
}
