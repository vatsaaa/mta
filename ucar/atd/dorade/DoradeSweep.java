// 
// Decompiled by Procyon v0.5.36
// 

package ucar.atd.dorade;

import java.io.IOException;
import java.util.Date;
import java.io.FileNotFoundException;
import java.io.RandomAccessFile;

public class DoradeSweep
{
    public static final float BAD_VALUE = Float.MAX_VALUE;
    DoradeSSWB mySSWB;
    DoradeVOLD myVOLD;
    DoradeSWIB mySWIB;
    boolean littleEndian;
    
    public DoradeSweep(final String filename) throws DoradeSweepException, FileNotFoundException {
        final RandomAccessFile file = new RandomAccessFile(filename, "r");
        try {
            this.littleEndian = DoradeDescriptor.sweepfileIsLittleEndian(file);
            this.mySSWB = new DoradeSSWB(file, this.littleEndian);
            this.myVOLD = new DoradeVOLD(file, this.littleEndian);
            this.mySWIB = new DoradeSWIB(file, this.littleEndian, this.myVOLD);
        }
        catch (Exception ex) {
            throw new DoradeSweepException(ex);
        }
    }
    
    public DoradeSweep(final RandomAccessFile file) throws DoradeSweepException, FileNotFoundException {
        try {
            this.littleEndian = DoradeDescriptor.sweepfileIsLittleEndian(file);
            this.mySSWB = new DoradeSSWB(file, this.littleEndian);
            this.myVOLD = new DoradeVOLD(file, this.littleEndian);
            this.mySWIB = new DoradeSWIB(file, this.littleEndian, this.myVOLD);
        }
        catch (Exception ex) {
            throw new DoradeSweepException(ex);
        }
    }
    
    public boolean isLittleEndian() {
        return this.littleEndian;
    }
    
    public DoradePARM[] getParamList() {
        return this.myVOLD.getParamList();
    }
    
    public DoradePARM lookupParamIgnoreCase(final String name) {
        final DoradePARM[] list = this.getParamList();
        for (int i = 0; i < list.length; ++i) {
            if (list[i].getName().equalsIgnoreCase(name)) {
                return list[i];
            }
        }
        return null;
    }
    
    public int getNRays() {
        return this.mySWIB.getNRays();
    }
    
    public int getNSensors() {
        return this.myVOLD.getNSensors();
    }
    
    public String getSensorName(final int which) {
        return this.myVOLD.getSensorName(which);
    }
    
    public boolean sensorIsMoving(final int which) throws DoradeSweepException {
        try {
            this.getLatitude(which);
            this.getLongitude(which);
            this.getAltitude(which);
        }
        catch (MovingSensorException mpex) {
            return true;
        }
        return false;
    }
    
    public float getLatitude(final int which) throws MovingSensorException {
        final float[] lats = this.mySWIB.getLatitudes();
        if (lats == null) {
            return this.myVOLD.getRADD(which).getLatitude();
        }
        for (int r = 1; r < lats.length; ++r) {
            if (lats[r] != lats[0]) {
                throw new MovingSensorException("sensor is not static");
            }
        }
        return lats[0];
    }
    
    public float getLongitude(final int which) throws MovingSensorException {
        final float[] lons = this.mySWIB.getLongitudes();
        if (lons == null) {
            return this.myVOLD.getRADD(which).getLongitude();
        }
        for (int r = 1; r < lons.length; ++r) {
            if (lons[r] != lons[0]) {
                throw new MovingSensorException("sensor is not static");
            }
        }
        return lons[0];
    }
    
    public float getAltitude(final int which) throws MovingSensorException {
        final float[] alts = this.mySWIB.getAltitudes();
        if (alts == null) {
            return this.myVOLD.getRADD(which).getAltitude();
        }
        for (int r = 1; r < alts.length; ++r) {
            if (alts[r] != alts[0]) {
                throw new MovingSensorException("sensor is not static");
            }
        }
        return alts[0];
    }
    
    public float[] getLatitudes(final int which) {
        float[] lats = this.mySWIB.getLatitudes();
        if (lats == null) {
            final float fixedLat = this.myVOLD.getRADD(which).getLatitude();
            lats = new float[this.getNRays()];
            for (int r = 0; r < this.getNRays(); ++r) {
                lats[r] = fixedLat;
            }
        }
        return lats;
    }
    
    public float[] getLongitudes(final int which) {
        float[] lons = this.mySWIB.getLongitudes();
        if (lons == null) {
            final float fixedLon = this.myVOLD.getRADD(which).getLongitude();
            lons = new float[this.getNRays()];
            for (int r = 0; r < this.getNRays(); ++r) {
                lons[r] = fixedLon;
            }
        }
        return lons;
    }
    
    public float[] getAltitudes(final int which) {
        float[] alts = this.mySWIB.getAltitudes();
        if (alts == null) {
            final float fixedAlt = this.myVOLD.getRADD(which).getAltitude();
            alts = new float[this.getNRays()];
            for (int r = 0; r < this.getNRays(); ++r) {
                alts[r] = fixedAlt;
            }
        }
        return alts;
    }
    
    public float getFixedAngle() {
        return this.mySWIB.getFixedAngle();
    }
    
    public int getSweepNumber() {
        return this.mySWIB.getSweepNumber();
    }
    
    public Date getTime() {
        return this.mySSWB.getStartTime();
    }
    
    public Date[] getTimes() {
        return this.mySWIB.getTimes();
    }
    
    public Date getRayTime(final int ray) {
        return this.mySWIB.getRayTime(ray);
    }
    
    public float[] getRayData(final DoradePARM param, final int ray) throws DoradeSweepException {
        return this.getRayData(param, ray, null);
    }
    
    public float[] getRayData(final DoradePARM param, final int ray, final float[] workingArray) throws DoradeSweepException {
        try {
            return this.mySWIB.getRayData(param, ray, workingArray);
        }
        catch (DoradeDescriptor.DescriptorException ex) {
            throw new DoradeSweepException(ex);
        }
    }
    
    public float getRangeToFirstCell(final int which) {
        return this.myVOLD.getRADD(which).getRangeToFirstCell();
    }
    
    public float getCellSpacing(final int which) {
        try {
            return this.myVOLD.getRADD(which).getCellSpacing();
        }
        catch (DoradeDescriptor.DescriptorException ex) {
            return -1.0f;
        }
    }
    
    public int getNCells(final int which) {
        return this.myVOLD.getRADD(which).getNCells();
    }
    
    public short getVolumnNumber() {
        return this.myVOLD.getVolumeNumber();
    }
    
    public String getProjectName() {
        return this.myVOLD.getProjectName();
    }
    
    public float[] getAzimuths() {
        return this.mySWIB.getAzimuths();
    }
    
    public float[] getElevations() {
        return this.mySWIB.getElevations();
    }
    
    public static String formatDate(final Date date) {
        return DoradeDescriptor.formatDate(date);
    }
    
    public static void main(final String[] args) {
        try {
            if (args.length == 0) {
                System.err.println("Usage: DoradeSweep <filename>");
                System.exit(1);
            }
            final DoradeSweep sweepfile = new DoradeSweep(args[0]);
            final DoradePARM[] params = sweepfile.getParamList();
            System.out.println(params.length + " params in file");
            for (int p = 0; p < params.length; ++p) {
                mainGetParam(sweepfile, params[p]);
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private static void mainGetParam(final DoradeSweep sweepfile, final DoradePARM param) throws DoradeSweepException {
        System.out.print("getting " + param.getName());
        for (int r = 0; r < sweepfile.getNRays(); ++r) {
            final float[] vals = sweepfile.getRayData(param, r);
            final int nCells = vals.length;
            if (r == 0) {
                System.out.println(" (" + nCells + " cells x " + sweepfile.getNRays() + " rays)");
            }
        }
    }
    
    public ScanMode getScanMode() {
        return this.myVOLD.getRADD(0).getScanMode();
    }
    
    public static boolean isDoradeSweep(final RandomAccessFile file) throws DoradeSweepException {
        try {
            if (findName(file, "SSWB")) {
                return true;
            }
        }
        catch (Exception ex) {
            throw new DoradeSweepException(ex);
        }
        return false;
    }
    
    private static boolean findName(final RandomAccessFile file, final String expectedName) throws IOException {
        final byte[] nameBytes = new byte[4];
        try {
            final long filepos = file.getFilePointer();
            file.seek(0L);
            if (file.read(nameBytes, 0, 4) == -1) {
                return false;
            }
            file.seek(filepos);
        }
        catch (Exception ex) {
            throw new IOException();
        }
        return expectedName.equals(new String(nameBytes));
    }
    
    public ScanMode getScanMode(final int which) {
        return this.myVOLD.getRADD(which).getScanMode();
    }
    
    public float getUnambiguousVelocity(final int which) {
        return this.myVOLD.getRADD(which).getUnambiguousVelocity();
    }
    
    public float getunambiguousRange(final int which) {
        return this.myVOLD.getRADD(which).getunambiguousRange();
    }
    
    public float getradarConstant(final int which) {
        return this.myVOLD.getRADD(which).getradarConstant();
    }
    
    public float getrcvrGain(final int which) {
        return this.myVOLD.getRADD(which).getrcvrGain();
    }
    
    public float getantennaGain(final int which) {
        return this.myVOLD.getRADD(which).getantennaGain();
    }
    
    public float getsystemGain(final int which) {
        return this.myVOLD.getRADD(which).getsystemGain();
    }
    
    public float gethBeamWidth(final int which) {
        return this.myVOLD.getRADD(which).gethBeamWidth();
    }
    
    public float getpeakPower(final int which) {
        return this.myVOLD.getRADD(which).getpeakPower();
    }
    
    public float getnoisePower(final int which) {
        return this.myVOLD.getRADD(which).getnoisePower();
    }
    
    public static class DoradeSweepException extends Exception
    {
        protected DoradeSweepException(final String message) {
            super(message);
        }
        
        protected DoradeSweepException(final Exception ex) {
            super(ex);
        }
    }
    
    public static class MovingSensorException extends Exception
    {
        protected MovingSensorException(final String message) {
            super(message);
        }
        
        protected MovingSensorException(final Exception ex) {
            super(ex);
        }
    }
}
