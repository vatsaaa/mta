// 
// Decompiled by Procyon v0.5.36
// 

package ucar.atd.dorade;

public class ScanMode
{
    public static final ScanMode MODE_PPI;
    public static final ScanMode MODE_RHI;
    public static final ScanMode MODE_SUR;
    public static final ScanMode MODE_coplane;
    public static final ScanMode MODE_calibration;
    public static final ScanMode MODE_vertical;
    public static final ScanMode MODE_idle;
    public static final ScanMode MODE_target;
    public static final ScanMode MODE_manual;
    public static final ScanMode MODE_air;
    public static final ScanMode MODE_horizontal;
    private String modeName;
    
    private ScanMode(final String modeName) {
        this.modeName = modeName;
    }
    
    public String getName() {
        return this.modeName;
    }
    
    @Override
    public String toString() {
        return this.modeName;
    }
    
    static {
        MODE_PPI = new ScanMode("PPI");
        MODE_RHI = new ScanMode("RHI");
        MODE_SUR = new ScanMode("SUR");
        MODE_coplane = new ScanMode("coplane");
        MODE_calibration = new ScanMode("calibration");
        MODE_vertical = new ScanMode("vertical");
        MODE_idle = new ScanMode("idle");
        MODE_target = new ScanMode("target");
        MODE_manual = new ScanMode("manual");
        MODE_air = new ScanMode("air");
        MODE_horizontal = new ScanMode("horizontal");
    }
}
