// 
// Decompiled by Procyon v0.5.36
// 

package ucar.atd.dorade;

import java.io.RandomAccessFile;

class DoradeCFAC extends DoradeDescriptor
{
    private float azimCorrection;
    private float elevCorrection;
    private float rangeCorrection;
    private float longitudeCorrection;
    private float latitudeCorrection;
    private float presAltCorrection;
    private float radarAltCorrection;
    private float uSpeedCorrection;
    private float vSpeedCorrection;
    private float wSpeedCorrection;
    private float headingCorrection;
    private float rollCorrection;
    private float pitchCorrection;
    private float driftCorrection;
    private float rotationAngleCorrection;
    private float tiltAngleCorrection;
    
    public DoradeCFAC(final RandomAccessFile file, final boolean littleEndianData) throws DescriptorException {
        final byte[] data = this.readDescriptor(file, littleEndianData, "CFAC");
        this.azimCorrection = this.grabFloat(data, 8);
        this.elevCorrection = this.grabFloat(data, 12);
        this.rangeCorrection = this.grabFloat(data, 16);
        this.longitudeCorrection = this.grabFloat(data, 20);
        this.latitudeCorrection = this.grabFloat(data, 24);
        this.presAltCorrection = this.grabFloat(data, 28);
        this.radarAltCorrection = this.grabFloat(data, 32);
        this.uSpeedCorrection = this.grabFloat(data, 36);
        this.vSpeedCorrection = this.grabFloat(data, 40);
        this.wSpeedCorrection = this.grabFloat(data, 44);
        this.headingCorrection = this.grabFloat(data, 48);
        this.rollCorrection = this.grabFloat(data, 52);
        this.pitchCorrection = this.grabFloat(data, 56);
        this.driftCorrection = this.grabFloat(data, 60);
        this.rotationAngleCorrection = this.grabFloat(data, 64);
        this.tiltAngleCorrection = this.grabFloat(data, 68);
        if (this.verbose) {
            System.out.println(this);
        }
    }
    
    @Override
    public String toString() {
        String s = "CFAC\n";
        s = s + "  azimuth correction: " + this.azimCorrection + "\n";
        s = s + "  elevation correction: " + this.elevCorrection + "\n";
        s = s + "  range correction: " + this.rangeCorrection + "\n";
        s = s + "  longitude correction: " + this.longitudeCorrection + "\n";
        s = s + "  latitude correction: " + this.latitudeCorrection + "\n";
        s = s + "  pressure altitude correction: " + this.presAltCorrection + "\n";
        s = s + "  radar altitude correction: " + this.radarAltCorrection + "\n";
        s = s + "  u speed correction: " + this.uSpeedCorrection + "\n";
        s = s + "  v speed correction: " + this.vSpeedCorrection + "\n";
        s = s + "  w speed correction: " + this.wSpeedCorrection + "\n";
        s = s + "  heading correction: " + this.headingCorrection + "\n";
        s = s + "  roll correction: " + this.rollCorrection + "\n";
        s = s + "  pitch correction: " + this.pitchCorrection + "\n";
        s = s + "  drift correction: " + this.driftCorrection + "\n";
        s = s + "  rotation angle correction: " + this.rotationAngleCorrection + "\n";
        s = s + "  tilt angle correction: " + this.tiltAngleCorrection;
        return s;
    }
}
