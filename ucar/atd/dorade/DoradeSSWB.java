// 
// Decompiled by Procyon v0.5.36
// 

package ucar.atd.dorade;

import java.io.RandomAccessFile;
import java.util.Date;

class DoradeSSWB extends DoradeDescriptor
{
    private Date lastUseTime;
    private Date startTime;
    private Date endTime;
    private int fileSize;
    private int compressionFlag;
    private Date volumeTime;
    private int nParams;
    private String radarName;
    private int version;
    private int status;
    private int nKeyTables;
    private KeyTable[] keyTables;
    
    public DoradeSSWB(final RandomAccessFile file, final boolean littleEndianData) throws DescriptorException {
        final byte[] data = this.readDescriptor(file, littleEndianData, "SSWB");
        if (data == null) {
            System.out.println("SSWB data null");
            System.exit(1);
        }
        int intTime = this.grabInt(data, 8);
        this.lastUseTime = new Date(intTime * 1000L);
        intTime = this.grabInt(data, 12);
        this.startTime = new Date(intTime * 1000L);
        intTime = this.grabInt(data, 16);
        this.endTime = new Date(intTime * 1000L);
        this.fileSize = this.grabInt(data, 20);
        this.compressionFlag = this.grabInt(data, 24);
        intTime = this.grabInt(data, 28);
        this.volumeTime = new Date(intTime * 1000L);
        this.nParams = this.grabInt(data, 32);
        if (data.length == 36) {
            this.radarName = "";
            this.version = 0;
            this.status = 0;
            this.nKeyTables = 0;
            this.keyTables = null;
        }
        else {
            this.radarName = new String(data, 36, 8).trim();
            final int optOffset = (data.length == 200) ? 4 : 0;
            double doubleTime = this.grabDouble(data, 44 + optOffset);
            if (doubleTime != 0.0) {
                this.startTime.setTime((long)(doubleTime * 1000.0));
            }
            doubleTime = this.grabDouble(data, 52 + optOffset);
            if (doubleTime != 0.0) {
                this.endTime.setTime((long)(doubleTime * 1000.0));
            }
            this.version = this.grabInt(data, 60 + optOffset);
            this.nKeyTables = this.grabInt(data, 64 + optOffset);
            this.status = this.grabInt(data, 68 + optOffset);
            this.keyTables = (KeyTable[])((this.nKeyTables > 0) ? new KeyTable[this.nKeyTables] : null);
            for (int i = 0; i < this.nKeyTables; ++i) {
                final int entrystart = 100 + 12 * i + optOffset;
                this.keyTables[i] = new KeyTable(this.grabInt(data, entrystart), this.grabInt(data, entrystart + 4), this.grabInt(data, entrystart + 8));
            }
        }
        if (this.verbose) {
            System.out.println(this);
        }
    }
    
    @Override
    public String toString() {
        String s = "SSWB\n";
        s = s + "  last use: " + DoradeDescriptor.formatDate(this.lastUseTime) + "\n";
        s = s + "  start time: " + DoradeDescriptor.formatDate(this.startTime) + "\n";
        s = s + "  end time: " + DoradeDescriptor.formatDate(this.endTime) + "\n";
        s = s + "  file size: " + this.fileSize + "\n";
        s = s + "  compression flag: " + this.compressionFlag + "\n";
        s = s + "  volume time: " + DoradeDescriptor.formatDate(this.volumeTime) + "\n";
        s = s + "  number of params: " + this.nParams + "\n";
        s = s + "  radar name: " + this.radarName + "\n";
        s = s + "  SSWB version: " + this.version + "\n";
        s = s + "  status: " + this.status + "\n";
        s = s + "  number of key tables: " + this.nKeyTables;
        return s;
    }
    
    public Date getStartTime() {
        return this.startTime;
    }
    
    public Date getEndTime() {
        return this.endTime;
    }
    
    private class KeyTable
    {
        public int offset;
        public int size;
        public int type;
        public final int KEYED_BY_TIME = 1;
        public final int KEYED_BY_ROT_ANG = 2;
        public final int SOLO_EDIT_SUMMARY = 3;
        
        public KeyTable(final int offset, final int size, final int type) {
            this.offset = offset;
            this.size = size;
            this.type = type;
        }
    }
}
