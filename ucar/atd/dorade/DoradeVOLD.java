// 
// Decompiled by Procyon v0.5.36
// 

package ucar.atd.dorade;

import java.util.Calendar;
import java.io.RandomAccessFile;
import java.util.Date;

class DoradeVOLD extends DoradeDescriptor
{
    private short formatRev;
    private short volNumber;
    private int maxRecLen;
    private String projectName;
    private Date dataTime;
    private String flightId;
    private String facilityName;
    private Date fileGenTime;
    private short nSensors;
    private DoradeRADD[] myRADDs;
    
    public DoradeVOLD(final RandomAccessFile file, final boolean littleEndianData) throws DescriptorException {
        final byte[] data = this.readDescriptor(file, littleEndianData, "VOLD");
        this.formatRev = this.grabShort(data, 8);
        this.volNumber = this.grabShort(data, 10);
        this.maxRecLen = this.grabInt(data, 12);
        this.projectName = new String(data, 16, 20).trim();
        final Calendar calendar = Calendar.getInstance(DoradeVOLD.TZ_UTC);
        short year = this.grabShort(data, 36);
        short month = this.grabShort(data, 38);
        short day = this.grabShort(data, 40);
        final short hour = this.grabShort(data, 42);
        final short minute = this.grabShort(data, 44);
        final short second = this.grabShort(data, 46);
        calendar.clear();
        calendar.set(year, month - 1, day, hour, minute, second);
        this.dataTime = calendar.getTime();
        this.flightId = new String(data, 48, 8).trim();
        this.facilityName = new String(data, 56, 8).trim();
        year = this.grabShort(data, 64);
        month = this.grabShort(data, 66);
        day = this.grabShort(data, 68);
        calendar.clear();
        calendar.set(year, month - 1, day);
        this.fileGenTime = calendar.getTime();
        this.nSensors = this.grabShort(data, 70);
        if (this.verbose) {
            System.out.println(this);
        }
        this.myRADDs = new DoradeRADD[this.nSensors];
        for (int i = 0; i < this.nSensors; ++i) {
            this.myRADDs[i] = new DoradeRADD(file, littleEndianData);
        }
    }
    
    @Override
    public String toString() {
        String s = "VOLD\n";
        s = s + "  format: " + this.formatRev + "\n";
        s = s + "  volume: " + this.volNumber + "\n";
        s = s + "  maxRecLen: " + this.maxRecLen + "\n";
        s = s + "  project: " + this.projectName + "\n";
        s = s + "  data time: " + DoradeDescriptor.formatDate(this.dataTime) + "\n";
        s = s + "  flight id: " + this.flightId + "\n";
        s = s + "  facility name: " + this.facilityName + "\n";
        s = s + "  file made: " + DoradeDescriptor.formatDate(this.fileGenTime) + "\n";
        s = s + "  num sensors: " + this.nSensors;
        return s;
    }
    
    public int getNSensors() {
        return this.nSensors;
    }
    
    public String getSensorName(final int n) {
        return this.myRADDs[n].getRadarName();
    }
    
    public DoradeRADD getRADD(final int n) {
        return this.myRADDs[n];
    }
    
    public Date getVolumeTime() {
        return this.dataTime;
    }
    
    public short getVolumeNumber() {
        return this.volNumber;
    }
    
    public String getProjectName() {
        return this.projectName;
    }
    
    public DoradePARM[] getParamList() {
        int paramCount = 0;
        for (int i = 0; i < this.nSensors; ++i) {
            paramCount += this.myRADDs[i].getNParams();
        }
        final DoradePARM[] list = new DoradePARM[paramCount];
        int next = 0;
        for (int j = 0; j < this.nSensors; ++j) {
            final int nParams = this.myRADDs[j].getNParams();
            System.arraycopy(this.myRADDs[j].getParamList(), 0, list, next, nParams);
            next += nParams;
        }
        return list;
    }
}
