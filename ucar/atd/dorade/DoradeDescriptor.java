// 
// Decompiled by Procyon v0.5.36
// 

package ucar.atd.dorade;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.InputStream;
import java.io.DataInputStream;
import java.io.ByteArrayInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.util.HashMap;
import java.util.TimeZone;
import java.io.RandomAccessFile;

abstract class DoradeDescriptor
{
    protected String descName;
    protected String expectedName;
    protected RandomAccessFile file;
    protected boolean littleEndianData;
    protected boolean verbose;
    protected static final TimeZone TZ_UTC;
    private static boolean defaultVerboseState;
    private static HashMap classVerboseStates;
    
    protected byte[] readDescriptor(final RandomAccessFile file, final boolean littleEndianData, final String expectedName) throws DescriptorException {
        this.file = file;
        this.littleEndianData = littleEndianData;
        this.expectedName = expectedName;
        this.verbose = getDefaultVerboseState(expectedName);
        byte[] data = null;
        try {
            this.findNext(file);
            final long startpos = file.getFilePointer();
            final byte[] header = new byte[8];
            file.read(header);
            this.descName = new String(header, 0, 4);
            final int size = this.grabInt(header, 4);
            file.seek(startpos);
            data = new byte[size];
            file.read(data);
        }
        catch (IOException ex) {
            throw new DescriptorException(ex);
        }
        if (!this.descName.equals(expectedName)) {
            throw new DescriptorException("Got descriptor name '" + this.descName + "' when expecting name '" + expectedName + "'");
        }
        return data;
    }
    
    protected static void skipDescriptor(final RandomAccessFile file, final boolean littleEndianData) throws DescriptorException, IOException {
        try {
            file.read(new byte[4]);
            final byte[] lenBytes = new byte[4];
            file.read(lenBytes);
            final int descLen = grabInt(lenBytes, 0, littleEndianData);
            file.read(new byte[descLen - 8]);
        }
        catch (EOFException eofex) {}
        catch (Exception ex) {
            throw new DescriptorException(ex);
        }
    }
    
    protected static String peekName(final RandomAccessFile file) throws DescriptorException {
        try {
            final long filepos = file.getFilePointer();
            final byte[] nameBytes = new byte[4];
            if (file.read(nameBytes) == -1) {
                return null;
            }
            file.seek(filepos);
            return new String(nameBytes);
        }
        catch (Exception ex) {
            throw new DescriptorException(ex);
        }
    }
    
    public static boolean sweepfileIsLittleEndian(final RandomAccessFile file) throws DescriptorException {
        int descLen;
        try {
            file.seek(0L);
            final byte[] bytes = new byte[4];
            file.read(bytes);
            descLen = file.readInt();
            file.seek(0L);
        }
        catch (Exception ex) {
            throw new DescriptorException(ex);
        }
        return descLen < 0 || descLen > 16777215;
    }
    
    protected short grabShort(final byte[] bytes, final int offset) {
        final int ndx0 = offset + (this.littleEndianData ? 1 : 0);
        final int ndx2 = offset + (this.littleEndianData ? 0 : 1);
        return (short)(bytes[ndx0] << 8 | (bytes[ndx2] & 0xFF));
    }
    
    protected static int grabInt(final byte[] bytes, final int offset, final boolean littleEndianData) {
        final int ndx0 = offset + (littleEndianData ? 3 : 0);
        final int ndx2 = offset + (littleEndianData ? 2 : 1);
        final int ndx3 = offset + (littleEndianData ? 1 : 2);
        final int ndx4 = offset + (littleEndianData ? 0 : 3);
        return bytes[ndx0] << 24 | (bytes[ndx2] & 0xFF) << 16 | (bytes[ndx3] & 0xFF) << 8 | (bytes[ndx4] & 0xFF);
    }
    
    protected int grabInt(final byte[] bytes, final int offset) {
        return grabInt(bytes, offset, this.littleEndianData);
    }
    
    protected float grabFloat(final byte[] bytes, int offset) throws DescriptorException {
        try {
            byte[] src;
            if (this.littleEndianData) {
                src = new byte[] { bytes[offset + 3], bytes[offset + 2], bytes[offset + 1], bytes[offset + 0] };
                offset = 0;
            }
            else {
                src = bytes;
            }
            final DataInputStream stream = new DataInputStream(new ByteArrayInputStream(src, offset, 4));
            return stream.readFloat();
        }
        catch (Exception ex) {
            throw new DescriptorException(ex);
        }
    }
    
    protected double grabDouble(final byte[] bytes, int offset) throws DescriptorException {
        try {
            byte[] src;
            if (this.littleEndianData) {
                src = new byte[] { bytes[offset + 7], bytes[offset + 6], bytes[offset + 5], bytes[offset + 4], bytes[offset + 3], bytes[offset + 2], bytes[offset + 1], bytes[offset + 0] };
                offset = 0;
            }
            else {
                src = bytes;
            }
            final DataInputStream stream = new DataInputStream(new ByteArrayInputStream(src, offset, 8));
            return stream.readDouble();
        }
        catch (Exception ex) {
            throw new DescriptorException(ex);
        }
    }
    
    protected static long findNextWithName(final String expectedName, final RandomAccessFile file, final boolean littleEndianData) throws DescriptorException, IOException {
        String descName;
        while ((descName = peekName(file)) != null) {
            if (descName.equals(expectedName)) {
                try {
                    return file.getFilePointer();
                }
                catch (IOException ex) {
                    throw new DescriptorException(ex);
                }
            }
            skipDescriptor(file, littleEndianData);
        }
        throw new DescriptorException("Expected " + expectedName + " descriptor not found!");
    }
    
    protected long findNext(final RandomAccessFile file) throws DescriptorException, IOException {
        return findNextWithName(this.expectedName, file, this.littleEndianData);
    }
    
    public static String formatDate(final Date date) {
        final SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS z");
        df.setTimeZone(DoradeDescriptor.TZ_UTC);
        return df.format(date);
    }
    
    public static boolean getDefaultVerboseState() {
        return DoradeDescriptor.defaultVerboseState;
    }
    
    public static void setDefaultVerboseState(final boolean verbose) {
        DoradeDescriptor.defaultVerboseState = verbose;
        DoradeDescriptor.classVerboseStates.clear();
    }
    
    public static boolean getDefaultVerboseState(final String descriptorName) {
        final Boolean classVerboseState = DoradeDescriptor.classVerboseStates.get(descriptorName.toUpperCase());
        if (classVerboseState != null) {
            return classVerboseState;
        }
        return DoradeDescriptor.defaultVerboseState;
    }
    
    public static void setDefaultVerboseState(final String descriptorName, final boolean verbose) {
        DoradeDescriptor.classVerboseStates.put(descriptorName.toUpperCase(), new Boolean(verbose));
    }
    
    static {
        TZ_UTC = TimeZone.getTimeZone("UTC");
        DoradeDescriptor.defaultVerboseState = false;
        DoradeDescriptor.classVerboseStates = new HashMap();
    }
    
    static class DescriptorException extends Exception
    {
        protected DescriptorException(final String message) {
            super(message);
        }
        
        protected DescriptorException(final Throwable cause) {
            super(cause);
        }
    }
}
