// 
// Decompiled by Procyon v0.5.36
// 

package ucar.atd.dorade;

import java.util.Date;
import java.io.IOException;
import java.io.RandomAccessFile;

class DoradeSWIB extends DoradeDescriptor
{
    private String comment;
    private int sweepNumber;
    private int nRays;
    private float startAngle;
    private float endAngle;
    private float fixedAngle;
    private int filterFlag;
    private long[] rayDataOffsets;
    private DoradeRYIB[] myRYIBs;
    private DoradeASIB[] myASIBs;
    private DoradeVOLD myVOLD;
    private float[] azimuths;
    private float[] elevations;
    
    public DoradeSWIB(final RandomAccessFile file, final boolean littleEndianData, final DoradeVOLD vold) throws DescriptorException {
        this.azimuths = null;
        this.elevations = null;
        final byte[] data = this.readDescriptor(file, littleEndianData, "SWIB");
        this.myVOLD = vold;
        this.comment = new String(data, 8, 8).trim();
        this.sweepNumber = this.grabInt(data, 16);
        this.nRays = this.grabInt(data, 20);
        this.startAngle = this.grabFloat(data, 24);
        this.endAngle = this.grabFloat(data, 28);
        this.fixedAngle = this.grabFloat(data, 32);
        this.filterFlag = this.grabInt(data, 36);
        if (this.verbose) {
            System.out.println(this);
        }
        this.myRYIBs = new DoradeRYIB[this.nRays];
        this.myASIBs = new DoradeASIB[this.nRays];
        this.rayDataOffsets = new long[this.nRays];
        boolean haveASIBs = false;
        for (int i = 0; i < this.nRays; ++i) {
            this.myRYIBs[i] = new DoradeRYIB(file, littleEndianData, this.myVOLD);
            try {
                this.rayDataOffsets[i] = file.getFilePointer();
            }
            catch (IOException ex) {
                throw new DescriptorException(ex);
            }
            if (i != 0) {
                if (!haveASIBs) {
                    continue;
                }
            }
            try {
                this.myASIBs[i] = new DoradeASIB(file, littleEndianData);
                haveASIBs = true;
            }
            catch (DescriptorException ex2) {
                if (i == 0) {
                    haveASIBs = false;
                    this.myASIBs = null;
                    try {
                        file.seek(this.rayDataOffsets[i]);
                        continue;
                    }
                    catch (IOException ioex) {
                        throw new DescriptorException(ioex);
                    }
                }
                throw new DescriptorException("not enough ASIBs");
            }
        }
    }
    
    @Override
    public String toString() {
        String s = "SWIB\n";
        s = s + "  sweep number: " + this.sweepNumber + "\n";
        s = s + "  number of rays: " + this.nRays + "\n";
        s = s + "  start angle: " + this.startAngle + "\n";
        s = s + "  end angle: " + this.endAngle + "\n";
        s = s + "  fixed angle: " + this.fixedAngle + "\n";
        s = s + "  filter flag: " + this.filterFlag;
        return s;
    }
    
    public float[] getRayData(final DoradePARM parm, final int ray) throws DescriptorException {
        return this.getRayData(parm, ray, null);
    }
    
    public float[] getRayData(final DoradePARM parm, final int ray, final float[] workingArray) throws DescriptorException {
        try {
            this.file.seek(this.rayDataOffsets[ray]);
            final DoradeRDAT rdat = DoradeRDAT.getNextOf(parm, this.file, this.littleEndianData);
            return parm.getParamValues(rdat, workingArray);
        }
        catch (IOException ex) {
            throw new DescriptorException(ex);
        }
    }
    
    public int getNRays() {
        return this.nRays;
    }
    
    public Date[] getTimes() {
        if (this.myRYIBs == null) {
            return null;
        }
        final Date[] times = new Date[this.nRays];
        for (int i = 0; i < this.nRays; ++i) {
            times[i] = this.myRYIBs[i].getRayTime();
        }
        return times;
    }
    
    public Date getRayTime(final int ray) {
        return this.myRYIBs[ray].getRayTime();
    }
    
    public float[] getLatitudes() {
        if (this.myASIBs == null) {
            return null;
        }
        final float[] lats = new float[this.nRays];
        for (int i = 0; i < this.nRays; ++i) {
            lats[i] = this.myASIBs[i].getLatitude();
        }
        return lats;
    }
    
    public float[] getLongitudes() {
        if (this.myASIBs == null) {
            return null;
        }
        final float[] lons = new float[this.nRays];
        for (int i = 0; i < this.nRays; ++i) {
            lons[i] = this.myASIBs[i].getLongitude();
        }
        return lons;
    }
    
    public float[] getAltitudes() {
        if (this.myASIBs == null) {
            return null;
        }
        final float[] alts = new float[this.nRays];
        for (int i = 0; i < this.nRays; ++i) {
            alts[i] = this.myASIBs[i].getAltitude();
        }
        return alts;
    }
    
    public float[] getAzimuths() {
        if (this.azimuths == null) {
            this.azimuths = new float[this.nRays];
            for (int r = 0; r < this.nRays; ++r) {
                this.azimuths[r] = this.myRYIBs[r].getAzimuth();
            }
        }
        return this.azimuths;
    }
    
    public float[] getElevations() {
        if (this.elevations == null) {
            this.elevations = new float[this.nRays];
            for (int r = 0; r < this.nRays; ++r) {
                this.elevations[r] = this.myRYIBs[r].getElevation();
            }
        }
        return this.elevations;
    }
    
    public float getFixedAngle() {
        return this.fixedAngle;
    }
    
    public int getSweepNumber() {
        return this.sweepNumber;
    }
}
