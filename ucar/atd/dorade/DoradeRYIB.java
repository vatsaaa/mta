// 
// Decompiled by Procyon v0.5.36
// 

package ucar.atd.dorade;

import java.util.Calendar;
import java.io.RandomAccessFile;
import java.util.Date;

class DoradeRYIB extends DoradeDescriptor
{
    private int sweepNumber;
    private Date rayTime;
    private float azimuth;
    private float elevation;
    private float lastPeakPower;
    private float scanRate;
    private int rayStatus;
    private DoradeVOLD myVOLD;
    
    public DoradeRYIB(final RandomAccessFile file, final boolean littleEndianData, final DoradeVOLD vold) throws DescriptorException {
        final byte[] data = this.readDescriptor(file, littleEndianData, "RYIB");
        this.myVOLD = vold;
        this.sweepNumber = this.grabInt(data, 8);
        final int julianDay = this.grabInt(data, 12);
        final int hour = this.grabShort(data, 16);
        final int minute = this.grabShort(data, 18);
        final int second = this.grabShort(data, 20);
        final int milliSecond = this.grabShort(data, 22);
        this.azimuth = this.grabFloat(data, 24);
        this.elevation = this.grabFloat(data, 28);
        this.lastPeakPower = this.grabFloat(data, 32);
        this.scanRate = this.grabFloat(data, 36);
        this.rayStatus = this.grabInt(data, 40);
        final Date volumeTime = this.myVOLD.getVolumeTime();
        final Calendar volumeCalendar = Calendar.getInstance(DoradeRYIB.TZ_UTC);
        volumeCalendar.setTime(volumeTime);
        final Calendar rayTimeCalendar = (Calendar)volumeCalendar.clone();
        rayTimeCalendar.set(11, hour);
        rayTimeCalendar.set(12, minute);
        rayTimeCalendar.set(13, second);
        rayTimeCalendar.set(14, milliSecond);
        if (rayTimeCalendar.before(volumeCalendar)) {
            rayTimeCalendar.add(5, 1);
        }
        this.rayTime = rayTimeCalendar.getTime();
        if (this.verbose) {
            System.out.println(this);
        }
    }
    
    @Override
    public String toString() {
        String s = "RYIB\n";
        s = s + "  sweep number: " + this.sweepNumber + "\n";
        s = s + "  ray time: " + DoradeDescriptor.formatDate(this.rayTime) + "\n";
        s = s + "  azimuth: " + this.azimuth + "\n";
        s = s + "  elevation: " + this.elevation + "\n";
        s = s + "  last peak transmitted power: " + this.lastPeakPower + "\n";
        s = s + "  scan rate: " + this.scanRate + "\n";
        s = s + "  ray status: " + this.rayStatus;
        return s;
    }
    
    public float getAzimuth() {
        return this.azimuth;
    }
    
    public float getElevation() {
        return this.elevation;
    }
    
    public Date getRayTime() {
        return this.rayTime;
    }
}
