// 
// Decompiled by Procyon v0.5.36
// 

package ucar.atd.dorade;

import java.io.IOException;
import java.io.RandomAccessFile;

class DoradeRADD extends DoradeDescriptor
{
    public static final RadarType TYPE_GROUND;
    public static final RadarType TYPE_AIR_FORE;
    public static final RadarType TYPE_AIR_AFT;
    public static final RadarType TYPE_AIR_TAIL;
    public static final RadarType TYPE_AIR_LF;
    public static final RadarType TYPE_SHIP;
    public static final RadarType TYPE_AIR_NOSE;
    public static final RadarType TYPE_SATELLITE;
    public static final RadarType TYPE_FIXED_LIDAR;
    public static final RadarType TYPE_MOVING_LIDAR;
    private static RadarType[] radarTypes;
    private static ScanMode[] scanModeTable;
    private String radarName;
    private float radarConstant;
    private float peakPower;
    private float noisePower;
    private float rcvrGain;
    private float antennaGain;
    private float systemGain;
    private float hBeamWidth;
    private float vBeamWidth;
    private short radarTypeNdx;
    private ScanMode scanMode;
    private float rotVelocity;
    private float scanParam0;
    private float scanParam1;
    private short nParams;
    private short nAdditionalDescriptors;
    private short compressionScheme;
    public static final int COMPRESSION_NONE = 0;
    public static final int COMPRESSION_HRD = 1;
    private short dataReductionMethod;
    public static final int DATA_REDUCTION_NONE = 1;
    public static final int DATA_REDUCTION_BY_AZIMUTH = 2;
    public static final int DATA_REDUCTION_BY_RANGE = 3;
    public static final int DATA_REDUCTION_BY_ALTITUDE = 4;
    private float reductionBound0;
    private float reductionBound1;
    private float longitude;
    private float latitude;
    private float altitude;
    private float unambiguousVelocity;
    private float unambiguousRange;
    private short nFrequencies;
    private short nPRTs;
    private float[] frequencies;
    private float[] PRTs;
    private DoradePARM[] myPARMs;
    private DoradeCELV myCELV;
    private DoradeCFAC myCFAC;
    private int nCells;
    
    public DoradeRADD(final RandomAccessFile file, final boolean littleEndianData) throws DescriptorException {
        final byte[] data = this.readDescriptor(file, littleEndianData, "RADD");
        this.radarName = new String(data, 8, 8).trim();
        this.radarConstant = this.grabFloat(data, 16);
        this.peakPower = this.grabFloat(data, 20);
        this.noisePower = this.grabFloat(data, 24);
        this.rcvrGain = this.grabFloat(data, 28);
        this.antennaGain = this.grabFloat(data, 32);
        this.systemGain = this.grabFloat(data, 36);
        this.hBeamWidth = this.grabFloat(data, 40);
        this.vBeamWidth = this.grabFloat(data, 44);
        this.radarTypeNdx = this.grabShort(data, 48);
        this.scanMode = DoradeRADD.scanModeTable[this.grabShort(data, 50)];
        this.rotVelocity = this.grabFloat(data, 52);
        this.scanParam0 = this.grabFloat(data, 56);
        this.scanParam1 = this.grabFloat(data, 60);
        this.nParams = this.grabShort(data, 64);
        this.nAdditionalDescriptors = this.grabShort(data, 66);
        this.compressionScheme = this.grabShort(data, 68);
        this.dataReductionMethod = this.grabShort(data, 70);
        this.reductionBound0 = this.grabFloat(data, 72);
        this.reductionBound1 = this.grabFloat(data, 76);
        this.longitude = this.grabFloat(data, 80);
        this.latitude = this.grabFloat(data, 84);
        this.altitude = this.grabFloat(data, 88);
        this.unambiguousVelocity = this.grabFloat(data, 92);
        this.unambiguousRange = this.grabFloat(data, 96);
        this.nFrequencies = this.grabShort(data, 100);
        this.nPRTs = this.grabShort(data, 102);
        this.frequencies = new float[5];
        for (int i = 0; i < 5; ++i) {
            this.frequencies[i] = this.grabFloat(data, 104 + 4 * i);
        }
        this.PRTs = new float[5];
        for (int i = 0; i < 5; ++i) {
            this.PRTs[i] = this.grabFloat(data, 124 + 4 * i);
        }
        if (this.verbose) {
            System.out.println(this);
        }
        this.myPARMs = new DoradePARM[this.nParams];
        for (int i = 0; i < this.nParams; ++i) {
            this.myPARMs[i] = new DoradePARM(file, littleEndianData, this);
        }
        try {
            final long startpos = file.getFilePointer();
            final String dName = DoradeDescriptor.peekName(file);
            if (dName.equals("CELV")) {
                this.myCELV = new DoradeCELV(file, littleEndianData);
            }
            else {
                if (!dName.equals("CSFD")) {
                    throw new DescriptorException("Expected " + dName + " descriptor not found!");
                }
                file.seek(startpos);
                this.myCELV = new DoradeCSFD(file, littleEndianData);
            }
        }
        catch (IOException ioex) {
            throw new DescriptorException(ioex);
        }
        this.nCells = this.myCELV.getNCells();
        if (this.nAdditionalDescriptors > this.nParams + 1) {
            this.myCFAC = new DoradeCFAC(file, littleEndianData);
        }
        else {
            this.myCFAC = null;
        }
    }
    
    @Override
    public String toString() {
        String s = "RADD\n";
        s = s + "  radar name: " + this.radarName + "\n";
        s = s + "  radar constant: " + this.radarConstant + "\n";
        s = s + "  peak power: " + this.peakPower + "\n";
        s = s + "  noise power: " + this.noisePower + "\n";
        s = s + "  receiver gain: " + this.rcvrGain + "\n";
        s = s + "  antenna gain: " + this.antennaGain + "\n";
        s = s + "  system gain: " + this.systemGain + "\n";
        s = s + "  beam width: " + this.hBeamWidth + "(H), " + this.vBeamWidth + "(V)\n";
        s = s + "  radar type: " + DoradeRADD.radarTypes[this.radarTypeNdx].getName() + "\n";
        s = s + "  scan mode: " + this.scanMode + "\n";
        s = s + "  rotation velocity: " + this.rotVelocity + "\n";
        s = s + "  scan params: " + this.scanParam0 + ", " + this.scanParam1 + "\n";
        s = s + "  number of parameters: " + this.nParams + "\n";
        s = s + "  additional descriptors: " + this.nAdditionalDescriptors + "\n";
        s = s + "  compression scheme: " + this.compressionScheme + "\n";
        s = s + "  data reduction method: " + this.dataReductionMethod + "\n";
        s = s + "  reduction bounds: " + this.reductionBound0 + ", " + this.reductionBound1 + "\n";
        s = s + "  location: " + this.longitude + "/" + this.latitude + " @ " + this.altitude + " km (MSL)\n";
        s = s + "  unambiguous velocity: " + this.unambiguousVelocity + "\n";
        s = s + "  unambiguous range: " + this.unambiguousRange + "\n";
        s += "  frequencies: ";
        for (int i = 0; i < this.nFrequencies; ++i) {
            s = s + this.frequencies[i] + " ";
        }
        s += "\n";
        s += "  PRTs: ";
        for (int i = 0; i < this.nPRTs; ++i) {
            s = s + this.PRTs[i] + " ";
        }
        return s;
    }
    
    public int getCompressionScheme() {
        return this.compressionScheme;
    }
    
    public int getNCells() {
        return this.nCells;
    }
    
    public DoradePARM[] getParamList() {
        return this.myPARMs;
    }
    
    public int getNParams() {
        return this.nParams;
    }
    
    public String getRadarName() {
        return this.radarName;
    }
    
    public float getLatitude() {
        return this.latitude;
    }
    
    public float getLongitude() {
        return this.longitude;
    }
    
    public float getAltitude() {
        return this.altitude;
    }
    
    public float getRangeToFirstCell() {
        return this.myCELV.getCellRanges()[0];
    }
    
    public float getCellSpacing() throws DescriptorException {
        final float[] cellRanges = this.myCELV.getCellRanges();
        final float cellSpacing = cellRanges[1] - cellRanges[0];
        for (int i = 2; i < cellRanges.length; ++i) {
            final float space = cellRanges[i] - cellRanges[i - 1];
            if (space != cellSpacing && Math.abs(space / cellSpacing - 1.0) > 0.01) {
                throw new DescriptorException("variable cell spacing");
            }
        }
        return cellSpacing;
    }
    
    public ScanMode getScanMode() {
        return this.scanMode;
    }
    
    public float getUnambiguousVelocity() {
        return this.unambiguousVelocity;
    }
    
    public float getunambiguousRange() {
        return this.unambiguousRange;
    }
    
    public float getradarConstant() {
        return this.radarConstant;
    }
    
    public float getrcvrGain() {
        return this.rcvrGain;
    }
    
    public float getantennaGain() {
        return this.antennaGain;
    }
    
    public float getsystemGain() {
        return this.systemGain;
    }
    
    public float gethBeamWidth() {
        return this.hBeamWidth;
    }
    
    public float getpeakPower() {
        return this.peakPower;
    }
    
    public float getnoisePower() {
        return this.noisePower;
    }
    
    static {
        TYPE_GROUND = new RadarType("ground");
        TYPE_AIR_FORE = new RadarType("airborne (fore)");
        TYPE_AIR_AFT = new RadarType("airborne (aft)");
        TYPE_AIR_TAIL = new RadarType("airborne (tail)");
        TYPE_AIR_LF = new RadarType("airborne (lower fuselage)");
        TYPE_SHIP = new RadarType("shipborne");
        TYPE_AIR_NOSE = new RadarType("airborne (nose)");
        TYPE_SATELLITE = new RadarType("satellite");
        TYPE_FIXED_LIDAR = new RadarType("fixed lidar");
        TYPE_MOVING_LIDAR = new RadarType("moving lidar");
        DoradeRADD.radarTypes = new RadarType[] { DoradeRADD.TYPE_GROUND, DoradeRADD.TYPE_AIR_FORE, DoradeRADD.TYPE_AIR_AFT, DoradeRADD.TYPE_AIR_LF, DoradeRADD.TYPE_AIR_TAIL, DoradeRADD.TYPE_SHIP, DoradeRADD.TYPE_AIR_NOSE, DoradeRADD.TYPE_SATELLITE, DoradeRADD.TYPE_MOVING_LIDAR, DoradeRADD.TYPE_FIXED_LIDAR };
        DoradeRADD.scanModeTable = new ScanMode[] { ScanMode.MODE_calibration, ScanMode.MODE_PPI, ScanMode.MODE_coplane, ScanMode.MODE_RHI, ScanMode.MODE_vertical, ScanMode.MODE_target, ScanMode.MODE_manual, ScanMode.MODE_idle, ScanMode.MODE_SUR, ScanMode.MODE_air, ScanMode.MODE_horizontal };
    }
    
    static class RadarType
    {
        private String name;
        
        protected RadarType(final String name) {
            this.name = name;
        }
        
        public String getName() {
            return this.name;
        }
    }
}
