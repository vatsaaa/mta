// 
// Decompiled by Procyon v0.5.36
// 

package ucar.atd.dorade;

import java.io.IOException;
import java.io.RandomAccessFile;

class DoradeRDAT extends DoradeDescriptor
{
    private String paramName;
    private byte[] paramData;
    
    public DoradeRDAT(final RandomAccessFile file, final boolean littleEndianData) throws DescriptorException {
        final byte[] data = this.readDescriptor(file, littleEndianData, "RDAT");
        this.paramName = new String(data, 8, 8).trim();
        System.arraycopy(data, 16, this.paramData = new byte[data.length - 16], 0, data.length - 16);
        if (this.verbose) {
            System.out.println(this);
        }
    }
    
    @Override
    public String toString() {
        String s = "RDAT\n";
        s = s + "  param name: " + this.paramName + "\n";
        s = s + "  data length: " + this.paramData.length;
        return s;
    }
    
    public String getParamName() {
        return this.paramName;
    }
    
    public byte[] getRawData() {
        return this.paramData;
    }
    
    public static DoradeRDAT getNextOf(final DoradePARM parm, final RandomAccessFile file, final boolean littleEndianData) throws DescriptorException, IOException {
        while (true) {
            final long pos = DoradeDescriptor.findNextWithName("RDAT", file, littleEndianData);
            if (peekParamName(file).equals(parm.getName())) {
                break;
            }
            DoradeDescriptor.skipDescriptor(file, littleEndianData);
        }
        return new DoradeRDAT(file, littleEndianData);
    }
    
    private static String peekParamName(final RandomAccessFile file) throws DescriptorException {
        try {
            final long filepos = file.getFilePointer();
            file.skipBytes(8);
            final byte[] nameBytes = new byte[8];
            if (file.read(nameBytes) == -1) {
                throw new DescriptorException("unexpected EOF");
            }
            file.seek(filepos);
            return new String(nameBytes).trim();
        }
        catch (Exception ex) {
            throw new DescriptorException(ex);
        }
    }
}
