// 
// Decompiled by Procyon v0.5.36
// 

package ucar.atd.dorade;

import java.io.RandomAccessFile;

class DoradeCSFD extends DoradeCELV
{
    protected int nSegments;
    protected float rangeToFirstCell;
    protected float[] segCellSpacing;
    protected short[] segNCells;
    
    public DoradeCSFD(final RandomAccessFile file, final boolean littleEndianData) throws DescriptorException {
        final byte[] data = this.readDescriptor(file, littleEndianData, "CSFD");
        this.nSegments = this.grabInt(data, 8);
        this.rangeToFirstCell = this.grabFloat(data, 12);
        this.segCellSpacing = new float[this.nSegments];
        this.segNCells = new short[this.nSegments];
        this.nCells = 0;
        for (int seg = 0; seg < this.nSegments; ++seg) {
            this.segCellSpacing[seg] = this.grabFloat(data, 16 + 4 * seg);
            this.segNCells[seg] = this.grabShort(data, 48 + 2 * seg);
            this.nCells += this.segNCells[seg];
        }
        this.ranges = new float[this.nCells];
        int cell = 0;
        float endOfPrevCell = 0.0f;
        for (int seg2 = 0; seg2 < this.nSegments; ++seg2) {
            for (int segCell = 0; segCell < this.segNCells[seg2]; ++segCell) {
                if (cell == 0) {
                    this.ranges[cell++] = this.rangeToFirstCell;
                    endOfPrevCell = this.rangeToFirstCell + this.segCellSpacing[seg2] / 2.0f;
                }
                else {
                    this.ranges[cell++] = endOfPrevCell + this.segCellSpacing[seg2] / 2.0f;
                    endOfPrevCell += this.segCellSpacing[seg2];
                }
            }
        }
        if (this.verbose) {
            System.out.println(this);
        }
    }
    
    @Override
    public String toString() {
        String s = "CSFD\n";
        s = s + "  number of segments: " + this.nSegments;
        for (int seg = 0; seg < this.nSegments; ++seg) {
            s += "\n";
            s = s + "  segment " + seg + "\n";
            s = s + "    # of cells: " + this.segNCells[seg] + "\n";
            s = s + "    cell spacing: " + this.segCellSpacing[seg] + " m";
        }
        return s;
    }
}
