// 
// Decompiled by Procyon v0.5.36
// 

package ucar.atd.dorade;

import java.io.RandomAccessFile;

class DoradeCELV extends DoradeDescriptor
{
    protected int nCells;
    protected float[] ranges;
    
    public DoradeCELV(final RandomAccessFile file, final boolean littleEndianData) throws DescriptorException {
        final byte[] data = this.readDescriptor(file, littleEndianData, "CELV");
        this.nCells = this.grabInt(data, 8);
        this.ranges = new float[this.nCells];
        for (int i = 0; i < this.nCells; ++i) {
            this.ranges[i] = this.grabFloat(data, 12 + 4 * i);
        }
        if (this.verbose) {
            System.out.println(this);
        }
    }
    
    protected DoradeCELV() {
    }
    
    @Override
    public String toString() {
        String s = "CELV\n";
        s = s + "  number of cells: " + this.nCells + "\n";
        s = s + "  ranges: " + this.ranges[0] + ", " + this.ranges[1] + ", ..., " + this.ranges[this.nCells - 1];
        return s;
    }
    
    public int getNCells() {
        return this.nCells;
    }
    
    public float[] getCellRanges() {
        return this.ranges;
    }
}
