// 
// Decompiled by Procyon v0.5.36
// 

package ucar.atd.dorade;

import java.io.RandomAccessFile;

public class DoradePARM extends DoradeDescriptor
{
    public static final float BAD_VALUE = Float.MAX_VALUE;
    private String paramName;
    private String paramDescription;
    private String unitName;
    private short usedPRTs;
    private short usedFrequencies;
    private float rcvrBandwidth;
    private short pulseWidth;
    private short polarization;
    private short nSamples;
    private short binaryFormat;
    public static final int FORMAT_8BIT_INT = 1;
    public static final int FORMAT_16BIT_INT = 2;
    public static final int FORMAT_32BIT_INT = 3;
    public static final int FORMAT_32BIT_FLOAT = 4;
    public static final int FORMAT_16BIT_FLOAT = 5;
    private String thresholdParamName;
    private float thresholdValue;
    private float scale;
    private float bias;
    private int badDataFlag;
    private DoradeRADD myRADD;
    
    public DoradePARM(final RandomAccessFile file, final boolean littleEndianData, final DoradeRADD radd) throws DescriptorException {
        final byte[] data = this.readDescriptor(file, littleEndianData, "PARM");
        this.myRADD = radd;
        this.paramName = new String(data, 8, 8).trim();
        this.paramDescription = new String(data, 16, 40).trim();
        this.unitName = new String(data, 56, 8).trim();
        this.usedPRTs = this.grabShort(data, 64);
        this.usedFrequencies = this.grabShort(data, 66);
        this.rcvrBandwidth = this.grabFloat(data, 68);
        this.pulseWidth = this.grabShort(data, 72);
        this.polarization = this.grabShort(data, 74);
        this.nSamples = this.grabShort(data, 76);
        this.binaryFormat = this.grabShort(data, 78);
        this.thresholdParamName = new String(data, 80, 8).trim();
        this.thresholdValue = this.grabFloat(data, 88);
        this.scale = this.grabFloat(data, 92);
        this.bias = this.grabFloat(data, 96);
        this.badDataFlag = this.grabInt(data, 100);
        if (this.verbose) {
            System.out.println(this);
        }
    }
    
    @Override
    public String toString() {
        String s = "PARM\n";
        s = s + "  param name: " + this.paramName + "\n";
        s = s + "  param description: " + this.paramDescription + "\n";
        s = s + "  unit name: " + this.unitName + "\n";
        s = s + "  used PRTs: " + this.usedPRTs + "\n";
        s = s + "  used frequencies: " + this.usedFrequencies + "\n";
        s = s + "  receiver bandwidth: " + this.rcvrBandwidth + "\n";
        s = s + "  pulse width: " + this.pulseWidth + "\n";
        s = s + "  polarization: " + this.polarization + "\n";
        s = s + "  number of samples: " + this.nSamples + "\n";
        s = s + "  binary format: " + this.binaryFormat + "\n";
        s = s + "  threshold parameter: " + this.thresholdParamName + "\n";
        s = s + "  threshold value: " + this.thresholdValue + "\n";
        s = s + "  scale: " + this.scale + "\n";
        s = s + "  bias: " + this.bias + "\n";
        s = s + "  bad data flag: " + this.badDataFlag;
        return s;
    }
    
    public String getName() {
        return this.paramName;
    }
    
    public int getBadDataFlag() {
        return this.badDataFlag;
    }
    
    public float getThresholdValue() {
        return this.thresholdValue;
    }
    
    public int getPolarization() {
        return this.polarization;
    }
    
    public float getScale() {
        return this.scale;
    }
    
    public String getUnitName() {
        return this.unitName;
    }
    
    public int getusedPRTs() {
        return this.usedPRTs;
    }
    
    public int getusedFrequencies() {
        return this.usedFrequencies;
    }
    
    public int getnSamples() {
        return this.nSamples;
    }
    
    public String getthresholdParamName() {
        return this.thresholdParamName;
    }
    
    public String getUnits() {
        return this.unitName;
    }
    
    public String getDescription() {
        return this.paramDescription;
    }
    
    public int getBinaryFormat() {
        return this.binaryFormat;
    }
    
    public int getNCells() {
        return this.myRADD.getNCells();
    }
    
    public float getCellSpacing() throws DescriptorException {
        return this.myRADD.getCellSpacing();
    }
    
    public float[] getParamValues(final DoradeRDAT rdat) throws DescriptorException {
        return this.getParamValues(rdat, null);
    }
    
    public float[] getParamValues(final DoradeRDAT rdat, final float[] workingArray) throws DescriptorException {
        if (!this.paramName.equals(rdat.getParamName())) {
            throw new DescriptorException("parameter name mismatch");
        }
        final byte[] paramData = rdat.getRawData();
        final int nCells = this.myRADD.getNCells();
        float[] values;
        if (workingArray != null && workingArray.length == nCells) {
            values = workingArray;
        }
        else {
            values = new float[nCells];
        }
        short[] svalues = null;
        if (this.myRADD.getCompressionScheme() == 1) {
            if (this.binaryFormat != 2) {
                throw new DescriptorException("Cannot unpack compressed data with binary format " + this.binaryFormat);
            }
            svalues = this.uncompressHRD(paramData, nCells);
        }
        for (int cell = 0; cell < nCells; ++cell) {
            switch (this.binaryFormat) {
                case 1: {
                    final byte bval = paramData[cell];
                    values[cell] = ((bval == this.badDataFlag) ? Float.MAX_VALUE : ((bval - this.bias) / this.scale));
                    break;
                }
                case 2: {
                    final short sval = (svalues != null) ? svalues[cell] : this.grabShort(paramData, 2 * cell);
                    values[cell] = ((sval == this.badDataFlag) ? Float.MAX_VALUE : ((sval - this.bias) / this.scale));
                    break;
                }
                case 3: {
                    final int ival = this.grabInt(paramData, 4 * cell);
                    values[cell] = ((ival == this.badDataFlag) ? Float.MAX_VALUE : ((ival - this.bias) / this.scale));
                    break;
                }
                case 4: {
                    final float fval = this.grabFloat(paramData, 4 * cell);
                    values[cell] = ((fval == this.badDataFlag) ? Float.MAX_VALUE : ((fval - this.bias) / this.scale));
                    break;
                }
                case 5: {
                    throw new DescriptorException("can't unpack 16-bit float data yet");
                }
                default: {
                    throw new DescriptorException("bad binary format (" + this.binaryFormat + ")");
                }
            }
        }
        return values;
    }
    
    private short[] uncompressHRD(final byte[] compressedData, final int nCells) throws DescriptorException {
        final short[] svalues = new short[nCells];
        int cPos = 0;
        int nextCell = 0;
        while (true) {
            final short runDescriptor = this.grabShort(compressedData, cPos);
            cPos += 2;
            final boolean runHasGoodValues = (runDescriptor & 0x8000) != 0x0;
            final int runLength = runDescriptor & 0x7FFF;
            if (runLength == 1) {
                for (int cell = nextCell; cell < nCells; ++cell) {
                    svalues[cell] = (short)this.badDataFlag;
                }
                return svalues;
            }
            if (nextCell + runLength > nCells) {
                throw new DescriptorException("attempt to unpack too many cells");
            }
            for (int cell2 = nextCell; cell2 < nextCell + runLength; ++cell2) {
                if (runHasGoodValues) {
                    svalues[cell2] = this.grabShort(compressedData, cPos);
                    cPos += 2;
                }
                else {
                    svalues[cell2] = (short)this.badDataFlag;
                }
            }
            nextCell += runLength;
        }
    }
}
