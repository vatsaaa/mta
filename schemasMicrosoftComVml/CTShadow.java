// 
// Decompiled by Procyon v0.5.36
// 

package schemasMicrosoftComVml;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlString;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTShadow extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTShadow.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctshadowdfdetype");
    
    String getId();
    
    XmlString xgetId();
    
    boolean isSetId();
    
    void setId(final String p0);
    
    void xsetId(final XmlString p0);
    
    void unsetId();
    
    STTrueFalse.Enum getOn();
    
    STTrueFalse xgetOn();
    
    boolean isSetOn();
    
    void setOn(final STTrueFalse.Enum p0);
    
    void xsetOn(final STTrueFalse p0);
    
    void unsetOn();
    
    STShadowType.Enum getType();
    
    STShadowType xgetType();
    
    boolean isSetType();
    
    void setType(final STShadowType.Enum p0);
    
    void xsetType(final STShadowType p0);
    
    void unsetType();
    
    STTrueFalse.Enum getObscured();
    
    STTrueFalse xgetObscured();
    
    boolean isSetObscured();
    
    void setObscured(final STTrueFalse.Enum p0);
    
    void xsetObscured(final STTrueFalse p0);
    
    void unsetObscured();
    
    String getColor();
    
    STColorType xgetColor();
    
    boolean isSetColor();
    
    void setColor(final String p0);
    
    void xsetColor(final STColorType p0);
    
    void unsetColor();
    
    String getOpacity();
    
    XmlString xgetOpacity();
    
    boolean isSetOpacity();
    
    void setOpacity(final String p0);
    
    void xsetOpacity(final XmlString p0);
    
    void unsetOpacity();
    
    String getOffset();
    
    XmlString xgetOffset();
    
    boolean isSetOffset();
    
    void setOffset(final String p0);
    
    void xsetOffset(final XmlString p0);
    
    void unsetOffset();
    
    String getColor2();
    
    STColorType xgetColor2();
    
    boolean isSetColor2();
    
    void setColor2(final String p0);
    
    void xsetColor2(final STColorType p0);
    
    void unsetColor2();
    
    String getOffset2();
    
    XmlString xgetOffset2();
    
    boolean isSetOffset2();
    
    void setOffset2(final String p0);
    
    void xsetOffset2(final XmlString p0);
    
    void unsetOffset2();
    
    String getOrigin();
    
    XmlString xgetOrigin();
    
    boolean isSetOrigin();
    
    void setOrigin(final String p0);
    
    void xsetOrigin(final XmlString p0);
    
    void unsetOrigin();
    
    String getMatrix();
    
    XmlString xgetMatrix();
    
    boolean isSetMatrix();
    
    void setMatrix(final String p0);
    
    void xsetMatrix(final XmlString p0);
    
    void unsetMatrix();
    
    public static final class Factory
    {
        public static CTShadow newInstance() {
            return (CTShadow)XmlBeans.getContextTypeLoader().newInstance(CTShadow.type, null);
        }
        
        public static CTShadow newInstance(final XmlOptions xmlOptions) {
            return (CTShadow)XmlBeans.getContextTypeLoader().newInstance(CTShadow.type, xmlOptions);
        }
        
        public static CTShadow parse(final String s) throws XmlException {
            return (CTShadow)XmlBeans.getContextTypeLoader().parse(s, CTShadow.type, null);
        }
        
        public static CTShadow parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTShadow)XmlBeans.getContextTypeLoader().parse(s, CTShadow.type, xmlOptions);
        }
        
        public static CTShadow parse(final File file) throws XmlException, IOException {
            return (CTShadow)XmlBeans.getContextTypeLoader().parse(file, CTShadow.type, null);
        }
        
        public static CTShadow parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTShadow)XmlBeans.getContextTypeLoader().parse(file, CTShadow.type, xmlOptions);
        }
        
        public static CTShadow parse(final URL url) throws XmlException, IOException {
            return (CTShadow)XmlBeans.getContextTypeLoader().parse(url, CTShadow.type, null);
        }
        
        public static CTShadow parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTShadow)XmlBeans.getContextTypeLoader().parse(url, CTShadow.type, xmlOptions);
        }
        
        public static CTShadow parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTShadow)XmlBeans.getContextTypeLoader().parse(inputStream, CTShadow.type, null);
        }
        
        public static CTShadow parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTShadow)XmlBeans.getContextTypeLoader().parse(inputStream, CTShadow.type, xmlOptions);
        }
        
        public static CTShadow parse(final Reader reader) throws XmlException, IOException {
            return (CTShadow)XmlBeans.getContextTypeLoader().parse(reader, CTShadow.type, null);
        }
        
        public static CTShadow parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTShadow)XmlBeans.getContextTypeLoader().parse(reader, CTShadow.type, xmlOptions);
        }
        
        public static CTShadow parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTShadow)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTShadow.type, null);
        }
        
        public static CTShadow parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTShadow)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTShadow.type, xmlOptions);
        }
        
        public static CTShadow parse(final Node node) throws XmlException {
            return (CTShadow)XmlBeans.getContextTypeLoader().parse(node, CTShadow.type, null);
        }
        
        public static CTShadow parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTShadow)XmlBeans.getContextTypeLoader().parse(node, CTShadow.type, xmlOptions);
        }
        
        @Deprecated
        public static CTShadow parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTShadow)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTShadow.type, null);
        }
        
        @Deprecated
        public static CTShadow parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTShadow)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTShadow.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTShadow.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTShadow.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
