// 
// Decompiled by Procyon v0.5.36
// 

package schemasMicrosoftComVml;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import schemasMicrosoftComOfficeOffice.STConnectType;
import org.apache.xmlbeans.XmlString;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTPath extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTPath.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctpath5963type");
    
    String getId();
    
    XmlString xgetId();
    
    boolean isSetId();
    
    void setId(final String p0);
    
    void xsetId(final XmlString p0);
    
    void unsetId();
    
    String getV();
    
    XmlString xgetV();
    
    boolean isSetV();
    
    void setV(final String p0);
    
    void xsetV(final XmlString p0);
    
    void unsetV();
    
    String getLimo();
    
    XmlString xgetLimo();
    
    boolean isSetLimo();
    
    void setLimo(final String p0);
    
    void xsetLimo(final XmlString p0);
    
    void unsetLimo();
    
    String getTextboxrect();
    
    XmlString xgetTextboxrect();
    
    boolean isSetTextboxrect();
    
    void setTextboxrect(final String p0);
    
    void xsetTextboxrect(final XmlString p0);
    
    void unsetTextboxrect();
    
    STTrueFalse.Enum getFillok();
    
    STTrueFalse xgetFillok();
    
    boolean isSetFillok();
    
    void setFillok(final STTrueFalse.Enum p0);
    
    void xsetFillok(final STTrueFalse p0);
    
    void unsetFillok();
    
    STTrueFalse.Enum getStrokeok();
    
    STTrueFalse xgetStrokeok();
    
    boolean isSetStrokeok();
    
    void setStrokeok(final STTrueFalse.Enum p0);
    
    void xsetStrokeok(final STTrueFalse p0);
    
    void unsetStrokeok();
    
    STTrueFalse.Enum getShadowok();
    
    STTrueFalse xgetShadowok();
    
    boolean isSetShadowok();
    
    void setShadowok(final STTrueFalse.Enum p0);
    
    void xsetShadowok(final STTrueFalse p0);
    
    void unsetShadowok();
    
    STTrueFalse.Enum getArrowok();
    
    STTrueFalse xgetArrowok();
    
    boolean isSetArrowok();
    
    void setArrowok(final STTrueFalse.Enum p0);
    
    void xsetArrowok(final STTrueFalse p0);
    
    void unsetArrowok();
    
    STTrueFalse.Enum getGradientshapeok();
    
    STTrueFalse xgetGradientshapeok();
    
    boolean isSetGradientshapeok();
    
    void setGradientshapeok(final STTrueFalse.Enum p0);
    
    void xsetGradientshapeok(final STTrueFalse p0);
    
    void unsetGradientshapeok();
    
    STTrueFalse.Enum getTextpathok();
    
    STTrueFalse xgetTextpathok();
    
    boolean isSetTextpathok();
    
    void setTextpathok(final STTrueFalse.Enum p0);
    
    void xsetTextpathok(final STTrueFalse p0);
    
    void unsetTextpathok();
    
    STTrueFalse.Enum getInsetpenok();
    
    STTrueFalse xgetInsetpenok();
    
    boolean isSetInsetpenok();
    
    void setInsetpenok(final STTrueFalse.Enum p0);
    
    void xsetInsetpenok(final STTrueFalse p0);
    
    void unsetInsetpenok();
    
    STConnectType.Enum getConnecttype();
    
    STConnectType xgetConnecttype();
    
    boolean isSetConnecttype();
    
    void setConnecttype(final STConnectType.Enum p0);
    
    void xsetConnecttype(final STConnectType p0);
    
    void unsetConnecttype();
    
    String getConnectlocs();
    
    XmlString xgetConnectlocs();
    
    boolean isSetConnectlocs();
    
    void setConnectlocs(final String p0);
    
    void xsetConnectlocs(final XmlString p0);
    
    void unsetConnectlocs();
    
    String getConnectangles();
    
    XmlString xgetConnectangles();
    
    boolean isSetConnectangles();
    
    void setConnectangles(final String p0);
    
    void xsetConnectangles(final XmlString p0);
    
    void unsetConnectangles();
    
    schemasMicrosoftComOfficeOffice.STTrueFalse.Enum getExtrusionok();
    
    schemasMicrosoftComOfficeOffice.STTrueFalse xgetExtrusionok();
    
    boolean isSetExtrusionok();
    
    void setExtrusionok(final schemasMicrosoftComOfficeOffice.STTrueFalse.Enum p0);
    
    void xsetExtrusionok(final schemasMicrosoftComOfficeOffice.STTrueFalse p0);
    
    void unsetExtrusionok();
    
    public static final class Factory
    {
        public static CTPath newInstance() {
            return (CTPath)XmlBeans.getContextTypeLoader().newInstance(CTPath.type, null);
        }
        
        public static CTPath newInstance(final XmlOptions xmlOptions) {
            return (CTPath)XmlBeans.getContextTypeLoader().newInstance(CTPath.type, xmlOptions);
        }
        
        public static CTPath parse(final String s) throws XmlException {
            return (CTPath)XmlBeans.getContextTypeLoader().parse(s, CTPath.type, null);
        }
        
        public static CTPath parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTPath)XmlBeans.getContextTypeLoader().parse(s, CTPath.type, xmlOptions);
        }
        
        public static CTPath parse(final File file) throws XmlException, IOException {
            return (CTPath)XmlBeans.getContextTypeLoader().parse(file, CTPath.type, null);
        }
        
        public static CTPath parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPath)XmlBeans.getContextTypeLoader().parse(file, CTPath.type, xmlOptions);
        }
        
        public static CTPath parse(final URL url) throws XmlException, IOException {
            return (CTPath)XmlBeans.getContextTypeLoader().parse(url, CTPath.type, null);
        }
        
        public static CTPath parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPath)XmlBeans.getContextTypeLoader().parse(url, CTPath.type, xmlOptions);
        }
        
        public static CTPath parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTPath)XmlBeans.getContextTypeLoader().parse(inputStream, CTPath.type, null);
        }
        
        public static CTPath parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPath)XmlBeans.getContextTypeLoader().parse(inputStream, CTPath.type, xmlOptions);
        }
        
        public static CTPath parse(final Reader reader) throws XmlException, IOException {
            return (CTPath)XmlBeans.getContextTypeLoader().parse(reader, CTPath.type, null);
        }
        
        public static CTPath parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPath)XmlBeans.getContextTypeLoader().parse(reader, CTPath.type, xmlOptions);
        }
        
        public static CTPath parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTPath)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTPath.type, null);
        }
        
        public static CTPath parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTPath)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTPath.type, xmlOptions);
        }
        
        public static CTPath parse(final Node node) throws XmlException {
            return (CTPath)XmlBeans.getContextTypeLoader().parse(node, CTPath.type, null);
        }
        
        public static CTPath parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTPath)XmlBeans.getContextTypeLoader().parse(node, CTPath.type, xmlOptions);
        }
        
        @Deprecated
        public static CTPath parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTPath)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTPath.type, null);
        }
        
        @Deprecated
        public static CTPath parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTPath)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTPath.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTPath.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTPath.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
