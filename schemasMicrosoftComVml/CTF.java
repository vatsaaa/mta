// 
// Decompiled by Procyon v0.5.36
// 

package schemasMicrosoftComVml;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlString;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTF extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTF.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctfbc3atype");
    
    String getEqn();
    
    XmlString xgetEqn();
    
    boolean isSetEqn();
    
    void setEqn(final String p0);
    
    void xsetEqn(final XmlString p0);
    
    void unsetEqn();
    
    public static final class Factory
    {
        public static CTF newInstance() {
            return (CTF)XmlBeans.getContextTypeLoader().newInstance(CTF.type, null);
        }
        
        public static CTF newInstance(final XmlOptions xmlOptions) {
            return (CTF)XmlBeans.getContextTypeLoader().newInstance(CTF.type, xmlOptions);
        }
        
        public static CTF parse(final String s) throws XmlException {
            return (CTF)XmlBeans.getContextTypeLoader().parse(s, CTF.type, null);
        }
        
        public static CTF parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTF)XmlBeans.getContextTypeLoader().parse(s, CTF.type, xmlOptions);
        }
        
        public static CTF parse(final File file) throws XmlException, IOException {
            return (CTF)XmlBeans.getContextTypeLoader().parse(file, CTF.type, null);
        }
        
        public static CTF parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTF)XmlBeans.getContextTypeLoader().parse(file, CTF.type, xmlOptions);
        }
        
        public static CTF parse(final URL url) throws XmlException, IOException {
            return (CTF)XmlBeans.getContextTypeLoader().parse(url, CTF.type, null);
        }
        
        public static CTF parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTF)XmlBeans.getContextTypeLoader().parse(url, CTF.type, xmlOptions);
        }
        
        public static CTF parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTF)XmlBeans.getContextTypeLoader().parse(inputStream, CTF.type, null);
        }
        
        public static CTF parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTF)XmlBeans.getContextTypeLoader().parse(inputStream, CTF.type, xmlOptions);
        }
        
        public static CTF parse(final Reader reader) throws XmlException, IOException {
            return (CTF)XmlBeans.getContextTypeLoader().parse(reader, CTF.type, null);
        }
        
        public static CTF parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTF)XmlBeans.getContextTypeLoader().parse(reader, CTF.type, xmlOptions);
        }
        
        public static CTF parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTF)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTF.type, null);
        }
        
        public static CTF parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTF)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTF.type, xmlOptions);
        }
        
        public static CTF parse(final Node node) throws XmlException {
            return (CTF)XmlBeans.getContextTypeLoader().parse(node, CTF.type, null);
        }
        
        public static CTF parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTF)XmlBeans.getContextTypeLoader().parse(node, CTF.type, xmlOptions);
        }
        
        @Deprecated
        public static CTF parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTF)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTF.type, null);
        }
        
        @Deprecated
        public static CTF parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTF)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTF.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTF.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTF.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
