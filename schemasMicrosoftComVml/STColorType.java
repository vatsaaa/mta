// 
// Decompiled by Procyon v0.5.36
// 

package schemasMicrosoftComVml;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlString;

public interface STColorType extends XmlString
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STColorType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stcolortype99c1type");
    
    public static final class Factory
    {
        public static STColorType newValue(final Object o) {
            return (STColorType)STColorType.type.newValue(o);
        }
        
        public static STColorType newInstance() {
            return (STColorType)XmlBeans.getContextTypeLoader().newInstance(STColorType.type, null);
        }
        
        public static STColorType newInstance(final XmlOptions xmlOptions) {
            return (STColorType)XmlBeans.getContextTypeLoader().newInstance(STColorType.type, xmlOptions);
        }
        
        public static STColorType parse(final String s) throws XmlException {
            return (STColorType)XmlBeans.getContextTypeLoader().parse(s, STColorType.type, null);
        }
        
        public static STColorType parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STColorType)XmlBeans.getContextTypeLoader().parse(s, STColorType.type, xmlOptions);
        }
        
        public static STColorType parse(final File file) throws XmlException, IOException {
            return (STColorType)XmlBeans.getContextTypeLoader().parse(file, STColorType.type, null);
        }
        
        public static STColorType parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STColorType)XmlBeans.getContextTypeLoader().parse(file, STColorType.type, xmlOptions);
        }
        
        public static STColorType parse(final URL url) throws XmlException, IOException {
            return (STColorType)XmlBeans.getContextTypeLoader().parse(url, STColorType.type, null);
        }
        
        public static STColorType parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STColorType)XmlBeans.getContextTypeLoader().parse(url, STColorType.type, xmlOptions);
        }
        
        public static STColorType parse(final InputStream inputStream) throws XmlException, IOException {
            return (STColorType)XmlBeans.getContextTypeLoader().parse(inputStream, STColorType.type, null);
        }
        
        public static STColorType parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STColorType)XmlBeans.getContextTypeLoader().parse(inputStream, STColorType.type, xmlOptions);
        }
        
        public static STColorType parse(final Reader reader) throws XmlException, IOException {
            return (STColorType)XmlBeans.getContextTypeLoader().parse(reader, STColorType.type, null);
        }
        
        public static STColorType parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STColorType)XmlBeans.getContextTypeLoader().parse(reader, STColorType.type, xmlOptions);
        }
        
        public static STColorType parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STColorType)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STColorType.type, null);
        }
        
        public static STColorType parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STColorType)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STColorType.type, xmlOptions);
        }
        
        public static STColorType parse(final Node node) throws XmlException {
            return (STColorType)XmlBeans.getContextTypeLoader().parse(node, STColorType.type, null);
        }
        
        public static STColorType parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STColorType)XmlBeans.getContextTypeLoader().parse(node, STColorType.type, xmlOptions);
        }
        
        @Deprecated
        public static STColorType parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STColorType)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STColorType.type, null);
        }
        
        @Deprecated
        public static STColorType parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STColorType)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STColorType.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STColorType.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STColorType.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
