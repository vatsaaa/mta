// 
// Decompiled by Procyon v0.5.36
// 

package schemasMicrosoftComVml.impl;

import org.apache.xmlbeans.SchemaType;
import schemasMicrosoftComVml.STStrokeJoinStyle;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STStrokeJoinStyleImpl extends JavaStringEnumerationHolderEx implements STStrokeJoinStyle
{
    public STStrokeJoinStyleImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STStrokeJoinStyleImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
