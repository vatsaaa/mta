// 
// Decompiled by Procyon v0.5.36
// 

package schemasMicrosoftComVml.impl;

import org.apache.xmlbeans.XmlObject;
import java.util.ArrayList;
import schemasMicrosoftComVml.CTH;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import schemasMicrosoftComVml.CTHandles;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTHandlesImpl extends XmlComplexContentImpl implements CTHandles
{
    private static final QName H$0;
    
    public CTHandlesImpl(final SchemaType type) {
        super(type);
    }
    
    public List<CTH> getHList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTH>)new CTHandlesImpl.HList(this);
        }
    }
    
    public CTH[] getHArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTHandlesImpl.H$0, list);
            final CTH[] array = new CTH[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTH getHArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTH cth = (CTH)this.get_store().find_element_user(CTHandlesImpl.H$0, n);
            if (cth == null) {
                throw new IndexOutOfBoundsException();
            }
            return cth;
        }
    }
    
    public int sizeOfHArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTHandlesImpl.H$0);
        }
    }
    
    public void setHArray(final CTH[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTHandlesImpl.H$0);
        }
    }
    
    public void setHArray(final int n, final CTH cth) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTH cth2 = (CTH)this.get_store().find_element_user(CTHandlesImpl.H$0, n);
            if (cth2 == null) {
                throw new IndexOutOfBoundsException();
            }
            cth2.set(cth);
        }
    }
    
    public CTH insertNewH(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTH)this.get_store().insert_element_user(CTHandlesImpl.H$0, n);
        }
    }
    
    public CTH addNewH() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTH)this.get_store().add_element_user(CTHandlesImpl.H$0);
        }
    }
    
    public void removeH(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTHandlesImpl.H$0, n);
        }
    }
    
    static {
        H$0 = new QName("urn:schemas-microsoft-com:vml", "h");
    }
}
