// 
// Decompiled by Procyon v0.5.36
// 

package schemasMicrosoftComVml.impl;

import org.apache.xmlbeans.SchemaType;
import schemasMicrosoftComVml.STColorType;
import org.apache.xmlbeans.impl.values.JavaStringHolderEx;

public class STColorTypeImpl extends JavaStringHolderEx implements STColorType
{
    public STColorTypeImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STColorTypeImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
