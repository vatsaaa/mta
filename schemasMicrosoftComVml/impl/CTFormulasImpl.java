// 
// Decompiled by Procyon v0.5.36
// 

package schemasMicrosoftComVml.impl;

import org.apache.xmlbeans.XmlObject;
import java.util.ArrayList;
import schemasMicrosoftComVml.CTF;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import schemasMicrosoftComVml.CTFormulas;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTFormulasImpl extends XmlComplexContentImpl implements CTFormulas
{
    private static final QName F$0;
    
    public CTFormulasImpl(final SchemaType type) {
        super(type);
    }
    
    public List<CTF> getFList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTF>)new CTFormulasImpl.FList(this);
        }
    }
    
    public CTF[] getFArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTFormulasImpl.F$0, list);
            final CTF[] array = new CTF[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTF getFArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTF ctf = (CTF)this.get_store().find_element_user(CTFormulasImpl.F$0, n);
            if (ctf == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctf;
        }
    }
    
    public int sizeOfFArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTFormulasImpl.F$0);
        }
    }
    
    public void setFArray(final CTF[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTFormulasImpl.F$0);
        }
    }
    
    public void setFArray(final int n, final CTF ctf) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTF ctf2 = (CTF)this.get_store().find_element_user(CTFormulasImpl.F$0, n);
            if (ctf2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctf2.set(ctf);
        }
    }
    
    public CTF insertNewF(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTF)this.get_store().insert_element_user(CTFormulasImpl.F$0, n);
        }
    }
    
    public CTF addNewF() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTF)this.get_store().add_element_user(CTFormulasImpl.F$0);
        }
    }
    
    public void removeF(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTFormulasImpl.F$0, n);
        }
    }
    
    static {
        F$0 = new QName("urn:schemas-microsoft-com:vml", "f");
    }
}
