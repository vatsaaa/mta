// 
// Decompiled by Procyon v0.5.36
// 

package schemasMicrosoftComVml.impl;

import org.apache.xmlbeans.SchemaType;
import schemasMicrosoftComVml.STExt;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STExtImpl extends JavaStringEnumerationHolderEx implements STExt
{
    public STExtImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STExtImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
