// 
// Decompiled by Procyon v0.5.36
// 

package schemasMicrosoftComVml.impl;

import org.apache.xmlbeans.SchemaType;
import schemasMicrosoftComVml.STTrueFalse;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STTrueFalseImpl extends JavaStringEnumerationHolderEx implements STTrueFalse
{
    public STTrueFalseImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STTrueFalseImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
