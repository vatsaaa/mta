// 
// Decompiled by Procyon v0.5.36
// 

package schemasMicrosoftComVml.impl;

import schemasMicrosoftComVml.STEditAs;
import schemasMicrosoftComVml.STColorType;
import schemasMicrosoftComOfficeOffice.STInsetMode;
import schemasMicrosoftComOfficeOffice.STHrAlign;
import org.apache.xmlbeans.XmlFloat;
import org.apache.xmlbeans.XmlInteger;
import java.math.BigInteger;
import org.apache.xmlbeans.StringEnumAbstractBase;
import schemasMicrosoftComVml.STTrueFalse;
import org.apache.xmlbeans.XmlString;
import org.apache.xmlbeans.SimpleValue;
import schemasMicrosoftComOfficeOffice.CTDiagram;
import schemasMicrosoftComVml.CTRoundRect;
import schemasMicrosoftComVml.CTRect;
import schemasMicrosoftComVml.CTPolyLine;
import schemasMicrosoftComVml.CTOval;
import schemasMicrosoftComVml.CTLine;
import schemasMicrosoftComVml.CTImage;
import schemasMicrosoftComVml.CTCurve;
import schemasMicrosoftComVml.CTArc;
import schemasMicrosoftComVml.CTShapetype;
import schemasMicrosoftComVml.CTShape;
import schemasMicrosoftComOfficePowerpoint.CTRel;
import schemasMicrosoftComOfficeExcel.CTClientData;
import schemasMicrosoftComOfficeWord.CTBorder;
import schemasMicrosoftComOfficeWord.CTAnchorLock;
import schemasMicrosoftComOfficeWord.CTWrap;
import schemasMicrosoftComOfficeOffice.CTSignatureLine;
import schemasMicrosoftComOfficeOffice.CTClipPath;
import schemasMicrosoftComOfficeOffice.CTLock;
import schemasMicrosoftComOfficeOffice.CTCallout;
import schemasMicrosoftComOfficeOffice.CTExtrusion;
import schemasMicrosoftComOfficeOffice.CTSkew;
import schemasMicrosoftComVml.CTImageData;
import schemasMicrosoftComVml.CTTextPath;
import schemasMicrosoftComVml.CTTextbox;
import schemasMicrosoftComVml.CTShadow;
import schemasMicrosoftComVml.CTStroke;
import schemasMicrosoftComVml.CTFill;
import schemasMicrosoftComVml.CTHandles;
import schemasMicrosoftComVml.CTFormulas;
import org.apache.xmlbeans.XmlObject;
import java.util.ArrayList;
import schemasMicrosoftComVml.CTPath;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import schemasMicrosoftComVml.CTGroup;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTGroupImpl extends XmlComplexContentImpl implements CTGroup
{
    private static final QName PATH$0;
    private static final QName FORMULAS$2;
    private static final QName HANDLES$4;
    private static final QName FILL$6;
    private static final QName STROKE$8;
    private static final QName SHADOW$10;
    private static final QName TEXTBOX$12;
    private static final QName TEXTPATH$14;
    private static final QName IMAGEDATA$16;
    private static final QName SKEW$18;
    private static final QName EXTRUSION$20;
    private static final QName CALLOUT$22;
    private static final QName LOCK$24;
    private static final QName CLIPPATH$26;
    private static final QName SIGNATURELINE$28;
    private static final QName WRAP$30;
    private static final QName ANCHORLOCK$32;
    private static final QName BORDERTOP$34;
    private static final QName BORDERBOTTOM$36;
    private static final QName BORDERLEFT$38;
    private static final QName BORDERRIGHT$40;
    private static final QName CLIENTDATA$42;
    private static final QName TEXTDATA$44;
    private static final QName GROUP$46;
    private static final QName SHAPE$48;
    private static final QName SHAPETYPE$50;
    private static final QName ARC$52;
    private static final QName CURVE$54;
    private static final QName IMAGE$56;
    private static final QName LINE$58;
    private static final QName OVAL$60;
    private static final QName POLYLINE$62;
    private static final QName RECT$64;
    private static final QName ROUNDRECT$66;
    private static final QName DIAGRAM$68;
    private static final QName ID$70;
    private static final QName STYLE$72;
    private static final QName HREF$74;
    private static final QName TARGET$76;
    private static final QName CLASS1$78;
    private static final QName TITLE$80;
    private static final QName ALT$82;
    private static final QName COORDSIZE$84;
    private static final QName COORDORIGIN$86;
    private static final QName WRAPCOORDS$88;
    private static final QName PRINT$90;
    private static final QName SPID$92;
    private static final QName ONED$94;
    private static final QName REGROUPID$96;
    private static final QName DOUBLECLICKNOTIFY$98;
    private static final QName BUTTON$100;
    private static final QName USERHIDDEN$102;
    private static final QName BULLET$104;
    private static final QName HR$106;
    private static final QName HRSTD$108;
    private static final QName HRNOSHADE$110;
    private static final QName HRPCT$112;
    private static final QName HRALIGN$114;
    private static final QName ALLOWINCELL$116;
    private static final QName ALLOWOVERLAP$118;
    private static final QName USERDRAWN$120;
    private static final QName BORDERTOPCOLOR$122;
    private static final QName BORDERLEFTCOLOR$124;
    private static final QName BORDERBOTTOMCOLOR$126;
    private static final QName BORDERRIGHTCOLOR$128;
    private static final QName DGMLAYOUT$130;
    private static final QName DGMNODEKIND$132;
    private static final QName DGMLAYOUTMRU$134;
    private static final QName INSETMODE$136;
    private static final QName FILLED$138;
    private static final QName FILLCOLOR$140;
    private static final QName EDITAS$142;
    private static final QName TABLEPROPERTIES$144;
    private static final QName TABLELIMITS$146;
    
    public CTGroupImpl(final SchemaType type) {
        super(type);
    }
    
    public List<CTPath> getPathList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPath>)new CTGroupImpl.PathList(this);
        }
    }
    
    public CTPath[] getPathArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.PATH$0, list);
            final CTPath[] array = new CTPath[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPath getPathArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPath ctPath = (CTPath)this.get_store().find_element_user(CTGroupImpl.PATH$0, n);
            if (ctPath == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPath;
        }
    }
    
    public int sizeOfPathArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.PATH$0);
        }
    }
    
    public void setPathArray(final CTPath[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTGroupImpl.PATH$0);
        }
    }
    
    public void setPathArray(final int n, final CTPath ctPath) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPath ctPath2 = (CTPath)this.get_store().find_element_user(CTGroupImpl.PATH$0, n);
            if (ctPath2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPath2.set(ctPath);
        }
    }
    
    public CTPath insertNewPath(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPath)this.get_store().insert_element_user(CTGroupImpl.PATH$0, n);
        }
    }
    
    public CTPath addNewPath() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPath)this.get_store().add_element_user(CTGroupImpl.PATH$0);
        }
    }
    
    public void removePath(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.PATH$0, n);
        }
    }
    
    public List<CTFormulas> getFormulasList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTFormulas>)new CTGroupImpl.FormulasList(this);
        }
    }
    
    public CTFormulas[] getFormulasArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.FORMULAS$2, list);
            final CTFormulas[] array = new CTFormulas[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTFormulas getFormulasArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTFormulas ctFormulas = (CTFormulas)this.get_store().find_element_user(CTGroupImpl.FORMULAS$2, n);
            if (ctFormulas == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctFormulas;
        }
    }
    
    public int sizeOfFormulasArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.FORMULAS$2);
        }
    }
    
    public void setFormulasArray(final CTFormulas[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTGroupImpl.FORMULAS$2);
        }
    }
    
    public void setFormulasArray(final int n, final CTFormulas ctFormulas) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTFormulas ctFormulas2 = (CTFormulas)this.get_store().find_element_user(CTGroupImpl.FORMULAS$2, n);
            if (ctFormulas2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctFormulas2.set(ctFormulas);
        }
    }
    
    public CTFormulas insertNewFormulas(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTFormulas)this.get_store().insert_element_user(CTGroupImpl.FORMULAS$2, n);
        }
    }
    
    public CTFormulas addNewFormulas() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTFormulas)this.get_store().add_element_user(CTGroupImpl.FORMULAS$2);
        }
    }
    
    public void removeFormulas(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.FORMULAS$2, n);
        }
    }
    
    public List<CTHandles> getHandlesList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTHandles>)new CTGroupImpl.HandlesList(this);
        }
    }
    
    public CTHandles[] getHandlesArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.HANDLES$4, list);
            final CTHandles[] array = new CTHandles[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTHandles getHandlesArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTHandles ctHandles = (CTHandles)this.get_store().find_element_user(CTGroupImpl.HANDLES$4, n);
            if (ctHandles == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctHandles;
        }
    }
    
    public int sizeOfHandlesArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.HANDLES$4);
        }
    }
    
    public void setHandlesArray(final CTHandles[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTGroupImpl.HANDLES$4);
        }
    }
    
    public void setHandlesArray(final int n, final CTHandles ctHandles) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTHandles ctHandles2 = (CTHandles)this.get_store().find_element_user(CTGroupImpl.HANDLES$4, n);
            if (ctHandles2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctHandles2.set(ctHandles);
        }
    }
    
    public CTHandles insertNewHandles(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTHandles)this.get_store().insert_element_user(CTGroupImpl.HANDLES$4, n);
        }
    }
    
    public CTHandles addNewHandles() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTHandles)this.get_store().add_element_user(CTGroupImpl.HANDLES$4);
        }
    }
    
    public void removeHandles(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.HANDLES$4, n);
        }
    }
    
    public List<CTFill> getFillList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTFill>)new CTGroupImpl.FillList(this);
        }
    }
    
    public CTFill[] getFillArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.FILL$6, list);
            final CTFill[] array = new CTFill[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTFill getFillArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTFill ctFill = (CTFill)this.get_store().find_element_user(CTGroupImpl.FILL$6, n);
            if (ctFill == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctFill;
        }
    }
    
    public int sizeOfFillArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.FILL$6);
        }
    }
    
    public void setFillArray(final CTFill[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTGroupImpl.FILL$6);
        }
    }
    
    public void setFillArray(final int n, final CTFill ctFill) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTFill ctFill2 = (CTFill)this.get_store().find_element_user(CTGroupImpl.FILL$6, n);
            if (ctFill2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctFill2.set(ctFill);
        }
    }
    
    public CTFill insertNewFill(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTFill)this.get_store().insert_element_user(CTGroupImpl.FILL$6, n);
        }
    }
    
    public CTFill addNewFill() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTFill)this.get_store().add_element_user(CTGroupImpl.FILL$6);
        }
    }
    
    public void removeFill(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.FILL$6, n);
        }
    }
    
    public List<CTStroke> getStrokeList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTStroke>)new CTGroupImpl.StrokeList(this);
        }
    }
    
    public CTStroke[] getStrokeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.STROKE$8, list);
            final CTStroke[] array = new CTStroke[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTStroke getStrokeArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTStroke ctStroke = (CTStroke)this.get_store().find_element_user(CTGroupImpl.STROKE$8, n);
            if (ctStroke == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctStroke;
        }
    }
    
    public int sizeOfStrokeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.STROKE$8);
        }
    }
    
    public void setStrokeArray(final CTStroke[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTGroupImpl.STROKE$8);
        }
    }
    
    public void setStrokeArray(final int n, final CTStroke ctStroke) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTStroke ctStroke2 = (CTStroke)this.get_store().find_element_user(CTGroupImpl.STROKE$8, n);
            if (ctStroke2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctStroke2.set(ctStroke);
        }
    }
    
    public CTStroke insertNewStroke(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTStroke)this.get_store().insert_element_user(CTGroupImpl.STROKE$8, n);
        }
    }
    
    public CTStroke addNewStroke() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTStroke)this.get_store().add_element_user(CTGroupImpl.STROKE$8);
        }
    }
    
    public void removeStroke(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.STROKE$8, n);
        }
    }
    
    public List<CTShadow> getShadowList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTShadow>)new CTGroupImpl.ShadowList(this);
        }
    }
    
    public CTShadow[] getShadowArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.SHADOW$10, list);
            final CTShadow[] array = new CTShadow[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTShadow getShadowArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTShadow ctShadow = (CTShadow)this.get_store().find_element_user(CTGroupImpl.SHADOW$10, n);
            if (ctShadow == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctShadow;
        }
    }
    
    public int sizeOfShadowArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.SHADOW$10);
        }
    }
    
    public void setShadowArray(final CTShadow[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTGroupImpl.SHADOW$10);
        }
    }
    
    public void setShadowArray(final int n, final CTShadow ctShadow) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTShadow ctShadow2 = (CTShadow)this.get_store().find_element_user(CTGroupImpl.SHADOW$10, n);
            if (ctShadow2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctShadow2.set(ctShadow);
        }
    }
    
    public CTShadow insertNewShadow(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTShadow)this.get_store().insert_element_user(CTGroupImpl.SHADOW$10, n);
        }
    }
    
    public CTShadow addNewShadow() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTShadow)this.get_store().add_element_user(CTGroupImpl.SHADOW$10);
        }
    }
    
    public void removeShadow(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.SHADOW$10, n);
        }
    }
    
    public List<CTTextbox> getTextboxList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTTextbox>)new CTGroupImpl.TextboxList(this);
        }
    }
    
    public CTTextbox[] getTextboxArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.TEXTBOX$12, list);
            final CTTextbox[] array = new CTTextbox[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTTextbox getTextboxArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTextbox ctTextbox = (CTTextbox)this.get_store().find_element_user(CTGroupImpl.TEXTBOX$12, n);
            if (ctTextbox == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctTextbox;
        }
    }
    
    public int sizeOfTextboxArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.TEXTBOX$12);
        }
    }
    
    public void setTextboxArray(final CTTextbox[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTGroupImpl.TEXTBOX$12);
        }
    }
    
    public void setTextboxArray(final int n, final CTTextbox ctTextbox) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTextbox ctTextbox2 = (CTTextbox)this.get_store().find_element_user(CTGroupImpl.TEXTBOX$12, n);
            if (ctTextbox2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctTextbox2.set(ctTextbox);
        }
    }
    
    public CTTextbox insertNewTextbox(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTextbox)this.get_store().insert_element_user(CTGroupImpl.TEXTBOX$12, n);
        }
    }
    
    public CTTextbox addNewTextbox() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTextbox)this.get_store().add_element_user(CTGroupImpl.TEXTBOX$12);
        }
    }
    
    public void removeTextbox(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.TEXTBOX$12, n);
        }
    }
    
    public List<CTTextPath> getTextpathList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTTextPath>)new CTGroupImpl.TextpathList(this);
        }
    }
    
    public CTTextPath[] getTextpathArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.TEXTPATH$14, list);
            final CTTextPath[] array = new CTTextPath[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTTextPath getTextpathArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTextPath ctTextPath = (CTTextPath)this.get_store().find_element_user(CTGroupImpl.TEXTPATH$14, n);
            if (ctTextPath == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctTextPath;
        }
    }
    
    public int sizeOfTextpathArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.TEXTPATH$14);
        }
    }
    
    public void setTextpathArray(final CTTextPath[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTGroupImpl.TEXTPATH$14);
        }
    }
    
    public void setTextpathArray(final int n, final CTTextPath ctTextPath) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTextPath ctTextPath2 = (CTTextPath)this.get_store().find_element_user(CTGroupImpl.TEXTPATH$14, n);
            if (ctTextPath2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctTextPath2.set(ctTextPath);
        }
    }
    
    public CTTextPath insertNewTextpath(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTextPath)this.get_store().insert_element_user(CTGroupImpl.TEXTPATH$14, n);
        }
    }
    
    public CTTextPath addNewTextpath() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTextPath)this.get_store().add_element_user(CTGroupImpl.TEXTPATH$14);
        }
    }
    
    public void removeTextpath(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.TEXTPATH$14, n);
        }
    }
    
    public List<CTImageData> getImagedataList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTImageData>)new CTGroupImpl.ImagedataList(this);
        }
    }
    
    public CTImageData[] getImagedataArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.IMAGEDATA$16, list);
            final CTImageData[] array = new CTImageData[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTImageData getImagedataArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTImageData ctImageData = (CTImageData)this.get_store().find_element_user(CTGroupImpl.IMAGEDATA$16, n);
            if (ctImageData == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctImageData;
        }
    }
    
    public int sizeOfImagedataArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.IMAGEDATA$16);
        }
    }
    
    public void setImagedataArray(final CTImageData[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTGroupImpl.IMAGEDATA$16);
        }
    }
    
    public void setImagedataArray(final int n, final CTImageData ctImageData) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTImageData ctImageData2 = (CTImageData)this.get_store().find_element_user(CTGroupImpl.IMAGEDATA$16, n);
            if (ctImageData2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctImageData2.set((XmlObject)ctImageData);
        }
    }
    
    public CTImageData insertNewImagedata(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTImageData)this.get_store().insert_element_user(CTGroupImpl.IMAGEDATA$16, n);
        }
    }
    
    public CTImageData addNewImagedata() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTImageData)this.get_store().add_element_user(CTGroupImpl.IMAGEDATA$16);
        }
    }
    
    public void removeImagedata(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.IMAGEDATA$16, n);
        }
    }
    
    public List<CTSkew> getSkewList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTSkew>)new CTGroupImpl.SkewList(this);
        }
    }
    
    public CTSkew[] getSkewArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.SKEW$18, list);
            final CTSkew[] array = new CTSkew[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTSkew getSkewArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSkew ctSkew = (CTSkew)this.get_store().find_element_user(CTGroupImpl.SKEW$18, n);
            if (ctSkew == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctSkew;
        }
    }
    
    public int sizeOfSkewArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.SKEW$18);
        }
    }
    
    public void setSkewArray(final CTSkew[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTGroupImpl.SKEW$18);
        }
    }
    
    public void setSkewArray(final int n, final CTSkew ctSkew) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSkew ctSkew2 = (CTSkew)this.get_store().find_element_user(CTGroupImpl.SKEW$18, n);
            if (ctSkew2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctSkew2.set((XmlObject)ctSkew);
        }
    }
    
    public CTSkew insertNewSkew(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSkew)this.get_store().insert_element_user(CTGroupImpl.SKEW$18, n);
        }
    }
    
    public CTSkew addNewSkew() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSkew)this.get_store().add_element_user(CTGroupImpl.SKEW$18);
        }
    }
    
    public void removeSkew(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.SKEW$18, n);
        }
    }
    
    public List<CTExtrusion> getExtrusionList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTExtrusion>)new CTGroupImpl.ExtrusionList(this);
        }
    }
    
    public CTExtrusion[] getExtrusionArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.EXTRUSION$20, list);
            final CTExtrusion[] array = new CTExtrusion[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTExtrusion getExtrusionArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTExtrusion ctExtrusion = (CTExtrusion)this.get_store().find_element_user(CTGroupImpl.EXTRUSION$20, n);
            if (ctExtrusion == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctExtrusion;
        }
    }
    
    public int sizeOfExtrusionArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.EXTRUSION$20);
        }
    }
    
    public void setExtrusionArray(final CTExtrusion[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTGroupImpl.EXTRUSION$20);
        }
    }
    
    public void setExtrusionArray(final int n, final CTExtrusion ctExtrusion) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTExtrusion ctExtrusion2 = (CTExtrusion)this.get_store().find_element_user(CTGroupImpl.EXTRUSION$20, n);
            if (ctExtrusion2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctExtrusion2.set((XmlObject)ctExtrusion);
        }
    }
    
    public CTExtrusion insertNewExtrusion(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTExtrusion)this.get_store().insert_element_user(CTGroupImpl.EXTRUSION$20, n);
        }
    }
    
    public CTExtrusion addNewExtrusion() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTExtrusion)this.get_store().add_element_user(CTGroupImpl.EXTRUSION$20);
        }
    }
    
    public void removeExtrusion(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.EXTRUSION$20, n);
        }
    }
    
    public List<CTCallout> getCalloutList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTCallout>)new CTGroupImpl.CalloutList(this);
        }
    }
    
    public CTCallout[] getCalloutArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.CALLOUT$22, list);
            final CTCallout[] array = new CTCallout[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTCallout getCalloutArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTCallout ctCallout = (CTCallout)this.get_store().find_element_user(CTGroupImpl.CALLOUT$22, n);
            if (ctCallout == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctCallout;
        }
    }
    
    public int sizeOfCalloutArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.CALLOUT$22);
        }
    }
    
    public void setCalloutArray(final CTCallout[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTGroupImpl.CALLOUT$22);
        }
    }
    
    public void setCalloutArray(final int n, final CTCallout ctCallout) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTCallout ctCallout2 = (CTCallout)this.get_store().find_element_user(CTGroupImpl.CALLOUT$22, n);
            if (ctCallout2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctCallout2.set((XmlObject)ctCallout);
        }
    }
    
    public CTCallout insertNewCallout(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTCallout)this.get_store().insert_element_user(CTGroupImpl.CALLOUT$22, n);
        }
    }
    
    public CTCallout addNewCallout() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTCallout)this.get_store().add_element_user(CTGroupImpl.CALLOUT$22);
        }
    }
    
    public void removeCallout(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.CALLOUT$22, n);
        }
    }
    
    public List<CTLock> getLockList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTLock>)new CTGroupImpl.LockList(this);
        }
    }
    
    public CTLock[] getLockArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.LOCK$24, list);
            final CTLock[] array = new CTLock[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTLock getLockArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTLock ctLock = (CTLock)this.get_store().find_element_user(CTGroupImpl.LOCK$24, n);
            if (ctLock == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctLock;
        }
    }
    
    public int sizeOfLockArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.LOCK$24);
        }
    }
    
    public void setLockArray(final CTLock[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTGroupImpl.LOCK$24);
        }
    }
    
    public void setLockArray(final int n, final CTLock ctLock) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTLock ctLock2 = (CTLock)this.get_store().find_element_user(CTGroupImpl.LOCK$24, n);
            if (ctLock2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctLock2.set(ctLock);
        }
    }
    
    public CTLock insertNewLock(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTLock)this.get_store().insert_element_user(CTGroupImpl.LOCK$24, n);
        }
    }
    
    public CTLock addNewLock() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTLock)this.get_store().add_element_user(CTGroupImpl.LOCK$24);
        }
    }
    
    public void removeLock(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.LOCK$24, n);
        }
    }
    
    public List<CTClipPath> getClippathList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTClipPath>)new CTGroupImpl.ClippathList(this);
        }
    }
    
    public CTClipPath[] getClippathArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.CLIPPATH$26, list);
            final CTClipPath[] array = new CTClipPath[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTClipPath getClippathArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTClipPath ctClipPath = (CTClipPath)this.get_store().find_element_user(CTGroupImpl.CLIPPATH$26, n);
            if (ctClipPath == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctClipPath;
        }
    }
    
    public int sizeOfClippathArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.CLIPPATH$26);
        }
    }
    
    public void setClippathArray(final CTClipPath[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTGroupImpl.CLIPPATH$26);
        }
    }
    
    public void setClippathArray(final int n, final CTClipPath ctClipPath) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTClipPath ctClipPath2 = (CTClipPath)this.get_store().find_element_user(CTGroupImpl.CLIPPATH$26, n);
            if (ctClipPath2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctClipPath2.set((XmlObject)ctClipPath);
        }
    }
    
    public CTClipPath insertNewClippath(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTClipPath)this.get_store().insert_element_user(CTGroupImpl.CLIPPATH$26, n);
        }
    }
    
    public CTClipPath addNewClippath() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTClipPath)this.get_store().add_element_user(CTGroupImpl.CLIPPATH$26);
        }
    }
    
    public void removeClippath(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.CLIPPATH$26, n);
        }
    }
    
    public List<CTSignatureLine> getSignaturelineList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTSignatureLine>)new CTGroupImpl.SignaturelineList(this);
        }
    }
    
    public CTSignatureLine[] getSignaturelineArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.SIGNATURELINE$28, list);
            final CTSignatureLine[] array = new CTSignatureLine[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTSignatureLine getSignaturelineArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSignatureLine ctSignatureLine = (CTSignatureLine)this.get_store().find_element_user(CTGroupImpl.SIGNATURELINE$28, n);
            if (ctSignatureLine == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctSignatureLine;
        }
    }
    
    public int sizeOfSignaturelineArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.SIGNATURELINE$28);
        }
    }
    
    public void setSignaturelineArray(final CTSignatureLine[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTGroupImpl.SIGNATURELINE$28);
        }
    }
    
    public void setSignaturelineArray(final int n, final CTSignatureLine ctSignatureLine) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSignatureLine ctSignatureLine2 = (CTSignatureLine)this.get_store().find_element_user(CTGroupImpl.SIGNATURELINE$28, n);
            if (ctSignatureLine2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctSignatureLine2.set((XmlObject)ctSignatureLine);
        }
    }
    
    public CTSignatureLine insertNewSignatureline(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSignatureLine)this.get_store().insert_element_user(CTGroupImpl.SIGNATURELINE$28, n);
        }
    }
    
    public CTSignatureLine addNewSignatureline() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSignatureLine)this.get_store().add_element_user(CTGroupImpl.SIGNATURELINE$28);
        }
    }
    
    public void removeSignatureline(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.SIGNATURELINE$28, n);
        }
    }
    
    public List<CTWrap> getWrapList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTWrap>)new CTGroupImpl.WrapList(this);
        }
    }
    
    public CTWrap[] getWrapArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.WRAP$30, list);
            final CTWrap[] array = new CTWrap[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTWrap getWrapArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTWrap ctWrap = (CTWrap)this.get_store().find_element_user(CTGroupImpl.WRAP$30, n);
            if (ctWrap == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctWrap;
        }
    }
    
    public int sizeOfWrapArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.WRAP$30);
        }
    }
    
    public void setWrapArray(final CTWrap[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTGroupImpl.WRAP$30);
        }
    }
    
    public void setWrapArray(final int n, final CTWrap ctWrap) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTWrap ctWrap2 = (CTWrap)this.get_store().find_element_user(CTGroupImpl.WRAP$30, n);
            if (ctWrap2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctWrap2.set((XmlObject)ctWrap);
        }
    }
    
    public CTWrap insertNewWrap(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTWrap)this.get_store().insert_element_user(CTGroupImpl.WRAP$30, n);
        }
    }
    
    public CTWrap addNewWrap() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTWrap)this.get_store().add_element_user(CTGroupImpl.WRAP$30);
        }
    }
    
    public void removeWrap(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.WRAP$30, n);
        }
    }
    
    public List<CTAnchorLock> getAnchorlockList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTAnchorLock>)new CTGroupImpl.AnchorlockList(this);
        }
    }
    
    public CTAnchorLock[] getAnchorlockArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.ANCHORLOCK$32, list);
            final CTAnchorLock[] array = new CTAnchorLock[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTAnchorLock getAnchorlockArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTAnchorLock ctAnchorLock = (CTAnchorLock)this.get_store().find_element_user(CTGroupImpl.ANCHORLOCK$32, n);
            if (ctAnchorLock == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctAnchorLock;
        }
    }
    
    public int sizeOfAnchorlockArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.ANCHORLOCK$32);
        }
    }
    
    public void setAnchorlockArray(final CTAnchorLock[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTGroupImpl.ANCHORLOCK$32);
        }
    }
    
    public void setAnchorlockArray(final int n, final CTAnchorLock ctAnchorLock) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTAnchorLock ctAnchorLock2 = (CTAnchorLock)this.get_store().find_element_user(CTGroupImpl.ANCHORLOCK$32, n);
            if (ctAnchorLock2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctAnchorLock2.set((XmlObject)ctAnchorLock);
        }
    }
    
    public CTAnchorLock insertNewAnchorlock(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTAnchorLock)this.get_store().insert_element_user(CTGroupImpl.ANCHORLOCK$32, n);
        }
    }
    
    public CTAnchorLock addNewAnchorlock() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTAnchorLock)this.get_store().add_element_user(CTGroupImpl.ANCHORLOCK$32);
        }
    }
    
    public void removeAnchorlock(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.ANCHORLOCK$32, n);
        }
    }
    
    public List<CTBorder> getBordertopList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTBorder>)new CTGroupImpl.BordertopList(this);
        }
    }
    
    public CTBorder[] getBordertopArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.BORDERTOP$34, list);
            final CTBorder[] array = new CTBorder[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTBorder getBordertopArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBorder ctBorder = (CTBorder)this.get_store().find_element_user(CTGroupImpl.BORDERTOP$34, n);
            if (ctBorder == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctBorder;
        }
    }
    
    public int sizeOfBordertopArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.BORDERTOP$34);
        }
    }
    
    public void setBordertopArray(final CTBorder[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTGroupImpl.BORDERTOP$34);
        }
    }
    
    public void setBordertopArray(final int n, final CTBorder ctBorder) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBorder ctBorder2 = (CTBorder)this.get_store().find_element_user(CTGroupImpl.BORDERTOP$34, n);
            if (ctBorder2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctBorder2.set((XmlObject)ctBorder);
        }
    }
    
    public CTBorder insertNewBordertop(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBorder)this.get_store().insert_element_user(CTGroupImpl.BORDERTOP$34, n);
        }
    }
    
    public CTBorder addNewBordertop() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBorder)this.get_store().add_element_user(CTGroupImpl.BORDERTOP$34);
        }
    }
    
    public void removeBordertop(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.BORDERTOP$34, n);
        }
    }
    
    public List<CTBorder> getBorderbottomList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTBorder>)new CTGroupImpl.BorderbottomList(this);
        }
    }
    
    public CTBorder[] getBorderbottomArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.BORDERBOTTOM$36, list);
            final CTBorder[] array = new CTBorder[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTBorder getBorderbottomArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBorder ctBorder = (CTBorder)this.get_store().find_element_user(CTGroupImpl.BORDERBOTTOM$36, n);
            if (ctBorder == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctBorder;
        }
    }
    
    public int sizeOfBorderbottomArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.BORDERBOTTOM$36);
        }
    }
    
    public void setBorderbottomArray(final CTBorder[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTGroupImpl.BORDERBOTTOM$36);
        }
    }
    
    public void setBorderbottomArray(final int n, final CTBorder ctBorder) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBorder ctBorder2 = (CTBorder)this.get_store().find_element_user(CTGroupImpl.BORDERBOTTOM$36, n);
            if (ctBorder2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctBorder2.set((XmlObject)ctBorder);
        }
    }
    
    public CTBorder insertNewBorderbottom(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBorder)this.get_store().insert_element_user(CTGroupImpl.BORDERBOTTOM$36, n);
        }
    }
    
    public CTBorder addNewBorderbottom() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBorder)this.get_store().add_element_user(CTGroupImpl.BORDERBOTTOM$36);
        }
    }
    
    public void removeBorderbottom(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.BORDERBOTTOM$36, n);
        }
    }
    
    public List<CTBorder> getBorderleftList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTBorder>)new CTGroupImpl.BorderleftList(this);
        }
    }
    
    public CTBorder[] getBorderleftArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.BORDERLEFT$38, list);
            final CTBorder[] array = new CTBorder[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTBorder getBorderleftArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBorder ctBorder = (CTBorder)this.get_store().find_element_user(CTGroupImpl.BORDERLEFT$38, n);
            if (ctBorder == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctBorder;
        }
    }
    
    public int sizeOfBorderleftArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.BORDERLEFT$38);
        }
    }
    
    public void setBorderleftArray(final CTBorder[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTGroupImpl.BORDERLEFT$38);
        }
    }
    
    public void setBorderleftArray(final int n, final CTBorder ctBorder) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBorder ctBorder2 = (CTBorder)this.get_store().find_element_user(CTGroupImpl.BORDERLEFT$38, n);
            if (ctBorder2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctBorder2.set((XmlObject)ctBorder);
        }
    }
    
    public CTBorder insertNewBorderleft(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBorder)this.get_store().insert_element_user(CTGroupImpl.BORDERLEFT$38, n);
        }
    }
    
    public CTBorder addNewBorderleft() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBorder)this.get_store().add_element_user(CTGroupImpl.BORDERLEFT$38);
        }
    }
    
    public void removeBorderleft(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.BORDERLEFT$38, n);
        }
    }
    
    public List<CTBorder> getBorderrightList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTBorder>)new CTGroupImpl.BorderrightList(this);
        }
    }
    
    public CTBorder[] getBorderrightArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.BORDERRIGHT$40, list);
            final CTBorder[] array = new CTBorder[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTBorder getBorderrightArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBorder ctBorder = (CTBorder)this.get_store().find_element_user(CTGroupImpl.BORDERRIGHT$40, n);
            if (ctBorder == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctBorder;
        }
    }
    
    public int sizeOfBorderrightArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.BORDERRIGHT$40);
        }
    }
    
    public void setBorderrightArray(final CTBorder[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTGroupImpl.BORDERRIGHT$40);
        }
    }
    
    public void setBorderrightArray(final int n, final CTBorder ctBorder) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBorder ctBorder2 = (CTBorder)this.get_store().find_element_user(CTGroupImpl.BORDERRIGHT$40, n);
            if (ctBorder2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctBorder2.set((XmlObject)ctBorder);
        }
    }
    
    public CTBorder insertNewBorderright(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBorder)this.get_store().insert_element_user(CTGroupImpl.BORDERRIGHT$40, n);
        }
    }
    
    public CTBorder addNewBorderright() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBorder)this.get_store().add_element_user(CTGroupImpl.BORDERRIGHT$40);
        }
    }
    
    public void removeBorderright(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.BORDERRIGHT$40, n);
        }
    }
    
    public List<CTClientData> getClientDataList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTClientData>)new CTGroupImpl.ClientDataList(this);
        }
    }
    
    public CTClientData[] getClientDataArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.CLIENTDATA$42, list);
            final CTClientData[] array = new CTClientData[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTClientData getClientDataArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTClientData ctClientData = (CTClientData)this.get_store().find_element_user(CTGroupImpl.CLIENTDATA$42, n);
            if (ctClientData == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctClientData;
        }
    }
    
    public int sizeOfClientDataArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.CLIENTDATA$42);
        }
    }
    
    public void setClientDataArray(final CTClientData[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTGroupImpl.CLIENTDATA$42);
        }
    }
    
    public void setClientDataArray(final int n, final CTClientData ctClientData) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTClientData ctClientData2 = (CTClientData)this.get_store().find_element_user(CTGroupImpl.CLIENTDATA$42, n);
            if (ctClientData2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctClientData2.set(ctClientData);
        }
    }
    
    public CTClientData insertNewClientData(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTClientData)this.get_store().insert_element_user(CTGroupImpl.CLIENTDATA$42, n);
        }
    }
    
    public CTClientData addNewClientData() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTClientData)this.get_store().add_element_user(CTGroupImpl.CLIENTDATA$42);
        }
    }
    
    public void removeClientData(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.CLIENTDATA$42, n);
        }
    }
    
    public List<CTRel> getTextdataList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTRel>)new CTGroupImpl.TextdataList(this);
        }
    }
    
    public CTRel[] getTextdataArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.TEXTDATA$44, list);
            final CTRel[] array = new CTRel[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTRel getTextdataArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRel ctRel = (CTRel)this.get_store().find_element_user(CTGroupImpl.TEXTDATA$44, n);
            if (ctRel == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctRel;
        }
    }
    
    public int sizeOfTextdataArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.TEXTDATA$44);
        }
    }
    
    public void setTextdataArray(final CTRel[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTGroupImpl.TEXTDATA$44);
        }
    }
    
    public void setTextdataArray(final int n, final CTRel ctRel) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRel ctRel2 = (CTRel)this.get_store().find_element_user(CTGroupImpl.TEXTDATA$44, n);
            if (ctRel2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctRel2.set((XmlObject)ctRel);
        }
    }
    
    public CTRel insertNewTextdata(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRel)this.get_store().insert_element_user(CTGroupImpl.TEXTDATA$44, n);
        }
    }
    
    public CTRel addNewTextdata() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRel)this.get_store().add_element_user(CTGroupImpl.TEXTDATA$44);
        }
    }
    
    public void removeTextdata(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.TEXTDATA$44, n);
        }
    }
    
    public List<CTGroup> getGroupList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTGroup>)new CTGroupImpl.GroupList(this);
        }
    }
    
    public CTGroup[] getGroupArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.GROUP$46, list);
            final CTGroup[] array = new CTGroup[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTGroup getGroupArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTGroup ctGroup = (CTGroup)this.get_store().find_element_user(CTGroupImpl.GROUP$46, n);
            if (ctGroup == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctGroup;
        }
    }
    
    public int sizeOfGroupArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.GROUP$46);
        }
    }
    
    public void setGroupArray(final CTGroup[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTGroupImpl.GROUP$46);
        }
    }
    
    public void setGroupArray(final int n, final CTGroup ctGroup) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTGroup ctGroup2 = (CTGroup)this.get_store().find_element_user(CTGroupImpl.GROUP$46, n);
            if (ctGroup2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctGroup2.set(ctGroup);
        }
    }
    
    public CTGroup insertNewGroup(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTGroup)this.get_store().insert_element_user(CTGroupImpl.GROUP$46, n);
        }
    }
    
    public CTGroup addNewGroup() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTGroup)this.get_store().add_element_user(CTGroupImpl.GROUP$46);
        }
    }
    
    public void removeGroup(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.GROUP$46, n);
        }
    }
    
    public List<CTShape> getShapeList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTShape>)new CTGroupImpl.ShapeList(this);
        }
    }
    
    public CTShape[] getShapeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.SHAPE$48, list);
            final CTShape[] array = new CTShape[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTShape getShapeArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTShape ctShape = (CTShape)this.get_store().find_element_user(CTGroupImpl.SHAPE$48, n);
            if (ctShape == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctShape;
        }
    }
    
    public int sizeOfShapeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.SHAPE$48);
        }
    }
    
    public void setShapeArray(final CTShape[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTGroupImpl.SHAPE$48);
        }
    }
    
    public void setShapeArray(final int n, final CTShape ctShape) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTShape ctShape2 = (CTShape)this.get_store().find_element_user(CTGroupImpl.SHAPE$48, n);
            if (ctShape2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctShape2.set(ctShape);
        }
    }
    
    public CTShape insertNewShape(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTShape)this.get_store().insert_element_user(CTGroupImpl.SHAPE$48, n);
        }
    }
    
    public CTShape addNewShape() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTShape)this.get_store().add_element_user(CTGroupImpl.SHAPE$48);
        }
    }
    
    public void removeShape(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.SHAPE$48, n);
        }
    }
    
    public List<CTShapetype> getShapetypeList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTShapetype>)new CTGroupImpl.ShapetypeList(this);
        }
    }
    
    public CTShapetype[] getShapetypeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.SHAPETYPE$50, list);
            final CTShapetype[] array = new CTShapetype[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTShapetype getShapetypeArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTShapetype ctShapetype = (CTShapetype)this.get_store().find_element_user(CTGroupImpl.SHAPETYPE$50, n);
            if (ctShapetype == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctShapetype;
        }
    }
    
    public int sizeOfShapetypeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.SHAPETYPE$50);
        }
    }
    
    public void setShapetypeArray(final CTShapetype[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTGroupImpl.SHAPETYPE$50);
        }
    }
    
    public void setShapetypeArray(final int n, final CTShapetype ctShapetype) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTShapetype ctShapetype2 = (CTShapetype)this.get_store().find_element_user(CTGroupImpl.SHAPETYPE$50, n);
            if (ctShapetype2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctShapetype2.set(ctShapetype);
        }
    }
    
    public CTShapetype insertNewShapetype(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTShapetype)this.get_store().insert_element_user(CTGroupImpl.SHAPETYPE$50, n);
        }
    }
    
    public CTShapetype addNewShapetype() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTShapetype)this.get_store().add_element_user(CTGroupImpl.SHAPETYPE$50);
        }
    }
    
    public void removeShapetype(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.SHAPETYPE$50, n);
        }
    }
    
    public List<CTArc> getArcList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTArc>)new CTGroupImpl.ArcList(this);
        }
    }
    
    public CTArc[] getArcArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.ARC$52, list);
            final CTArc[] array = new CTArc[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTArc getArcArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTArc ctArc = (CTArc)this.get_store().find_element_user(CTGroupImpl.ARC$52, n);
            if (ctArc == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctArc;
        }
    }
    
    public int sizeOfArcArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.ARC$52);
        }
    }
    
    public void setArcArray(final CTArc[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTGroupImpl.ARC$52);
        }
    }
    
    public void setArcArray(final int n, final CTArc ctArc) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTArc ctArc2 = (CTArc)this.get_store().find_element_user(CTGroupImpl.ARC$52, n);
            if (ctArc2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctArc2.set((XmlObject)ctArc);
        }
    }
    
    public CTArc insertNewArc(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTArc)this.get_store().insert_element_user(CTGroupImpl.ARC$52, n);
        }
    }
    
    public CTArc addNewArc() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTArc)this.get_store().add_element_user(CTGroupImpl.ARC$52);
        }
    }
    
    public void removeArc(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.ARC$52, n);
        }
    }
    
    public List<CTCurve> getCurveList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTCurve>)new CTGroupImpl.CurveList(this);
        }
    }
    
    public CTCurve[] getCurveArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.CURVE$54, list);
            final CTCurve[] array = new CTCurve[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTCurve getCurveArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTCurve ctCurve = (CTCurve)this.get_store().find_element_user(CTGroupImpl.CURVE$54, n);
            if (ctCurve == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctCurve;
        }
    }
    
    public int sizeOfCurveArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.CURVE$54);
        }
    }
    
    public void setCurveArray(final CTCurve[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTGroupImpl.CURVE$54);
        }
    }
    
    public void setCurveArray(final int n, final CTCurve ctCurve) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTCurve ctCurve2 = (CTCurve)this.get_store().find_element_user(CTGroupImpl.CURVE$54, n);
            if (ctCurve2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctCurve2.set((XmlObject)ctCurve);
        }
    }
    
    public CTCurve insertNewCurve(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTCurve)this.get_store().insert_element_user(CTGroupImpl.CURVE$54, n);
        }
    }
    
    public CTCurve addNewCurve() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTCurve)this.get_store().add_element_user(CTGroupImpl.CURVE$54);
        }
    }
    
    public void removeCurve(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.CURVE$54, n);
        }
    }
    
    public List<CTImage> getImageList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTImage>)new CTGroupImpl.ImageList(this);
        }
    }
    
    public CTImage[] getImageArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.IMAGE$56, list);
            final CTImage[] array = new CTImage[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTImage getImageArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTImage ctImage = (CTImage)this.get_store().find_element_user(CTGroupImpl.IMAGE$56, n);
            if (ctImage == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctImage;
        }
    }
    
    public int sizeOfImageArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.IMAGE$56);
        }
    }
    
    public void setImageArray(final CTImage[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTGroupImpl.IMAGE$56);
        }
    }
    
    public void setImageArray(final int n, final CTImage ctImage) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTImage ctImage2 = (CTImage)this.get_store().find_element_user(CTGroupImpl.IMAGE$56, n);
            if (ctImage2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctImage2.set((XmlObject)ctImage);
        }
    }
    
    public CTImage insertNewImage(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTImage)this.get_store().insert_element_user(CTGroupImpl.IMAGE$56, n);
        }
    }
    
    public CTImage addNewImage() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTImage)this.get_store().add_element_user(CTGroupImpl.IMAGE$56);
        }
    }
    
    public void removeImage(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.IMAGE$56, n);
        }
    }
    
    public List<CTLine> getLineList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTLine>)new CTGroupImpl.LineList(this);
        }
    }
    
    public CTLine[] getLineArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.LINE$58, list);
            final CTLine[] array = new CTLine[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTLine getLineArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTLine ctLine = (CTLine)this.get_store().find_element_user(CTGroupImpl.LINE$58, n);
            if (ctLine == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctLine;
        }
    }
    
    public int sizeOfLineArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.LINE$58);
        }
    }
    
    public void setLineArray(final CTLine[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTGroupImpl.LINE$58);
        }
    }
    
    public void setLineArray(final int n, final CTLine ctLine) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTLine ctLine2 = (CTLine)this.get_store().find_element_user(CTGroupImpl.LINE$58, n);
            if (ctLine2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctLine2.set((XmlObject)ctLine);
        }
    }
    
    public CTLine insertNewLine(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTLine)this.get_store().insert_element_user(CTGroupImpl.LINE$58, n);
        }
    }
    
    public CTLine addNewLine() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTLine)this.get_store().add_element_user(CTGroupImpl.LINE$58);
        }
    }
    
    public void removeLine(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.LINE$58, n);
        }
    }
    
    public List<CTOval> getOvalList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTOval>)new CTGroupImpl.OvalList(this);
        }
    }
    
    public CTOval[] getOvalArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.OVAL$60, list);
            final CTOval[] array = new CTOval[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTOval getOvalArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTOval ctOval = (CTOval)this.get_store().find_element_user(CTGroupImpl.OVAL$60, n);
            if (ctOval == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctOval;
        }
    }
    
    public int sizeOfOvalArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.OVAL$60);
        }
    }
    
    public void setOvalArray(final CTOval[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTGroupImpl.OVAL$60);
        }
    }
    
    public void setOvalArray(final int n, final CTOval ctOval) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTOval ctOval2 = (CTOval)this.get_store().find_element_user(CTGroupImpl.OVAL$60, n);
            if (ctOval2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctOval2.set((XmlObject)ctOval);
        }
    }
    
    public CTOval insertNewOval(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTOval)this.get_store().insert_element_user(CTGroupImpl.OVAL$60, n);
        }
    }
    
    public CTOval addNewOval() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTOval)this.get_store().add_element_user(CTGroupImpl.OVAL$60);
        }
    }
    
    public void removeOval(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.OVAL$60, n);
        }
    }
    
    public List<CTPolyLine> getPolylineList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPolyLine>)new CTGroupImpl.PolylineList(this);
        }
    }
    
    public CTPolyLine[] getPolylineArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.POLYLINE$62, list);
            final CTPolyLine[] array = new CTPolyLine[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPolyLine getPolylineArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPolyLine ctPolyLine = (CTPolyLine)this.get_store().find_element_user(CTGroupImpl.POLYLINE$62, n);
            if (ctPolyLine == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPolyLine;
        }
    }
    
    public int sizeOfPolylineArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.POLYLINE$62);
        }
    }
    
    public void setPolylineArray(final CTPolyLine[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTGroupImpl.POLYLINE$62);
        }
    }
    
    public void setPolylineArray(final int n, final CTPolyLine ctPolyLine) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPolyLine ctPolyLine2 = (CTPolyLine)this.get_store().find_element_user(CTGroupImpl.POLYLINE$62, n);
            if (ctPolyLine2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPolyLine2.set((XmlObject)ctPolyLine);
        }
    }
    
    public CTPolyLine insertNewPolyline(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPolyLine)this.get_store().insert_element_user(CTGroupImpl.POLYLINE$62, n);
        }
    }
    
    public CTPolyLine addNewPolyline() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPolyLine)this.get_store().add_element_user(CTGroupImpl.POLYLINE$62);
        }
    }
    
    public void removePolyline(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.POLYLINE$62, n);
        }
    }
    
    public List<CTRect> getRectList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTRect>)new CTGroupImpl.RectList(this);
        }
    }
    
    public CTRect[] getRectArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.RECT$64, list);
            final CTRect[] array = new CTRect[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTRect getRectArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRect ctRect = (CTRect)this.get_store().find_element_user(CTGroupImpl.RECT$64, n);
            if (ctRect == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctRect;
        }
    }
    
    public int sizeOfRectArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.RECT$64);
        }
    }
    
    public void setRectArray(final CTRect[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTGroupImpl.RECT$64);
        }
    }
    
    public void setRectArray(final int n, final CTRect ctRect) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRect ctRect2 = (CTRect)this.get_store().find_element_user(CTGroupImpl.RECT$64, n);
            if (ctRect2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctRect2.set((XmlObject)ctRect);
        }
    }
    
    public CTRect insertNewRect(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRect)this.get_store().insert_element_user(CTGroupImpl.RECT$64, n);
        }
    }
    
    public CTRect addNewRect() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRect)this.get_store().add_element_user(CTGroupImpl.RECT$64);
        }
    }
    
    public void removeRect(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.RECT$64, n);
        }
    }
    
    public List<CTRoundRect> getRoundrectList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTRoundRect>)new CTGroupImpl.RoundrectList(this);
        }
    }
    
    public CTRoundRect[] getRoundrectArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.ROUNDRECT$66, list);
            final CTRoundRect[] array = new CTRoundRect[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTRoundRect getRoundrectArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRoundRect ctRoundRect = (CTRoundRect)this.get_store().find_element_user(CTGroupImpl.ROUNDRECT$66, n);
            if (ctRoundRect == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctRoundRect;
        }
    }
    
    public int sizeOfRoundrectArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.ROUNDRECT$66);
        }
    }
    
    public void setRoundrectArray(final CTRoundRect[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTGroupImpl.ROUNDRECT$66);
        }
    }
    
    public void setRoundrectArray(final int n, final CTRoundRect ctRoundRect) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRoundRect ctRoundRect2 = (CTRoundRect)this.get_store().find_element_user(CTGroupImpl.ROUNDRECT$66, n);
            if (ctRoundRect2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctRoundRect2.set((XmlObject)ctRoundRect);
        }
    }
    
    public CTRoundRect insertNewRoundrect(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRoundRect)this.get_store().insert_element_user(CTGroupImpl.ROUNDRECT$66, n);
        }
    }
    
    public CTRoundRect addNewRoundrect() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRoundRect)this.get_store().add_element_user(CTGroupImpl.ROUNDRECT$66);
        }
    }
    
    public void removeRoundrect(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.ROUNDRECT$66, n);
        }
    }
    
    public List<CTDiagram> getDiagramList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTDiagram>)new CTGroupImpl.DiagramList(this);
        }
    }
    
    public CTDiagram[] getDiagramArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupImpl.DIAGRAM$68, list);
            final CTDiagram[] array = new CTDiagram[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTDiagram getDiagramArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTDiagram ctDiagram = (CTDiagram)this.get_store().find_element_user(CTGroupImpl.DIAGRAM$68, n);
            if (ctDiagram == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctDiagram;
        }
    }
    
    public int sizeOfDiagramArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupImpl.DIAGRAM$68);
        }
    }
    
    public void setDiagramArray(final CTDiagram[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTGroupImpl.DIAGRAM$68);
        }
    }
    
    public void setDiagramArray(final int n, final CTDiagram ctDiagram) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTDiagram ctDiagram2 = (CTDiagram)this.get_store().find_element_user(CTGroupImpl.DIAGRAM$68, n);
            if (ctDiagram2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctDiagram2.set((XmlObject)ctDiagram);
        }
    }
    
    public CTDiagram insertNewDiagram(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTDiagram)this.get_store().insert_element_user(CTGroupImpl.DIAGRAM$68, n);
        }
    }
    
    public CTDiagram addNewDiagram() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTDiagram)this.get_store().add_element_user(CTGroupImpl.DIAGRAM$68);
        }
    }
    
    public void removeDiagram(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupImpl.DIAGRAM$68, n);
        }
    }
    
    public String getId() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.ID$70);
            if (simpleValue == null) {
                return null;
            }
            return simpleValue.getStringValue();
        }
    }
    
    public XmlString xgetId() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().find_attribute_user(CTGroupImpl.ID$70);
        }
    }
    
    public boolean isSetId() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.ID$70) != null;
        }
    }
    
    public void setId(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.ID$70);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.ID$70);
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetId(final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            XmlString xmlString2 = (XmlString)this.get_store().find_attribute_user(CTGroupImpl.ID$70);
            if (xmlString2 == null) {
                xmlString2 = (XmlString)this.get_store().add_attribute_user(CTGroupImpl.ID$70);
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void unsetId() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.ID$70);
        }
    }
    
    public String getStyle() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.STYLE$72);
            if (simpleValue == null) {
                return null;
            }
            return simpleValue.getStringValue();
        }
    }
    
    public XmlString xgetStyle() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().find_attribute_user(CTGroupImpl.STYLE$72);
        }
    }
    
    public boolean isSetStyle() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.STYLE$72) != null;
        }
    }
    
    public void setStyle(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.STYLE$72);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.STYLE$72);
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetStyle(final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            XmlString xmlString2 = (XmlString)this.get_store().find_attribute_user(CTGroupImpl.STYLE$72);
            if (xmlString2 == null) {
                xmlString2 = (XmlString)this.get_store().add_attribute_user(CTGroupImpl.STYLE$72);
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void unsetStyle() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.STYLE$72);
        }
    }
    
    public String getHref() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.HREF$74);
            if (simpleValue == null) {
                return null;
            }
            return simpleValue.getStringValue();
        }
    }
    
    public XmlString xgetHref() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().find_attribute_user(CTGroupImpl.HREF$74);
        }
    }
    
    public boolean isSetHref() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.HREF$74) != null;
        }
    }
    
    public void setHref(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.HREF$74);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.HREF$74);
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetHref(final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            XmlString xmlString2 = (XmlString)this.get_store().find_attribute_user(CTGroupImpl.HREF$74);
            if (xmlString2 == null) {
                xmlString2 = (XmlString)this.get_store().add_attribute_user(CTGroupImpl.HREF$74);
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void unsetHref() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.HREF$74);
        }
    }
    
    public String getTarget() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.TARGET$76);
            if (simpleValue == null) {
                return null;
            }
            return simpleValue.getStringValue();
        }
    }
    
    public XmlString xgetTarget() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().find_attribute_user(CTGroupImpl.TARGET$76);
        }
    }
    
    public boolean isSetTarget() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.TARGET$76) != null;
        }
    }
    
    public void setTarget(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.TARGET$76);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.TARGET$76);
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetTarget(final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            XmlString xmlString2 = (XmlString)this.get_store().find_attribute_user(CTGroupImpl.TARGET$76);
            if (xmlString2 == null) {
                xmlString2 = (XmlString)this.get_store().add_attribute_user(CTGroupImpl.TARGET$76);
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void unsetTarget() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.TARGET$76);
        }
    }
    
    public String getClass1() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.CLASS1$78);
            if (simpleValue == null) {
                return null;
            }
            return simpleValue.getStringValue();
        }
    }
    
    public XmlString xgetClass1() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().find_attribute_user(CTGroupImpl.CLASS1$78);
        }
    }
    
    public boolean isSetClass1() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.CLASS1$78) != null;
        }
    }
    
    public void setClass1(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.CLASS1$78);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.CLASS1$78);
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetClass1(final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            XmlString xmlString2 = (XmlString)this.get_store().find_attribute_user(CTGroupImpl.CLASS1$78);
            if (xmlString2 == null) {
                xmlString2 = (XmlString)this.get_store().add_attribute_user(CTGroupImpl.CLASS1$78);
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void unsetClass1() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.CLASS1$78);
        }
    }
    
    public String getTitle() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.TITLE$80);
            if (simpleValue == null) {
                return null;
            }
            return simpleValue.getStringValue();
        }
    }
    
    public XmlString xgetTitle() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().find_attribute_user(CTGroupImpl.TITLE$80);
        }
    }
    
    public boolean isSetTitle() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.TITLE$80) != null;
        }
    }
    
    public void setTitle(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.TITLE$80);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.TITLE$80);
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetTitle(final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            XmlString xmlString2 = (XmlString)this.get_store().find_attribute_user(CTGroupImpl.TITLE$80);
            if (xmlString2 == null) {
                xmlString2 = (XmlString)this.get_store().add_attribute_user(CTGroupImpl.TITLE$80);
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void unsetTitle() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.TITLE$80);
        }
    }
    
    public String getAlt() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.ALT$82);
            if (simpleValue == null) {
                return null;
            }
            return simpleValue.getStringValue();
        }
    }
    
    public XmlString xgetAlt() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().find_attribute_user(CTGroupImpl.ALT$82);
        }
    }
    
    public boolean isSetAlt() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.ALT$82) != null;
        }
    }
    
    public void setAlt(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.ALT$82);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.ALT$82);
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetAlt(final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            XmlString xmlString2 = (XmlString)this.get_store().find_attribute_user(CTGroupImpl.ALT$82);
            if (xmlString2 == null) {
                xmlString2 = (XmlString)this.get_store().add_attribute_user(CTGroupImpl.ALT$82);
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void unsetAlt() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.ALT$82);
        }
    }
    
    public String getCoordsize() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.COORDSIZE$84);
            if (simpleValue == null) {
                return null;
            }
            return simpleValue.getStringValue();
        }
    }
    
    public XmlString xgetCoordsize() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().find_attribute_user(CTGroupImpl.COORDSIZE$84);
        }
    }
    
    public boolean isSetCoordsize() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.COORDSIZE$84) != null;
        }
    }
    
    public void setCoordsize(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.COORDSIZE$84);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.COORDSIZE$84);
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetCoordsize(final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            XmlString xmlString2 = (XmlString)this.get_store().find_attribute_user(CTGroupImpl.COORDSIZE$84);
            if (xmlString2 == null) {
                xmlString2 = (XmlString)this.get_store().add_attribute_user(CTGroupImpl.COORDSIZE$84);
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void unsetCoordsize() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.COORDSIZE$84);
        }
    }
    
    public String getCoordorigin() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.COORDORIGIN$86);
            if (simpleValue == null) {
                return null;
            }
            return simpleValue.getStringValue();
        }
    }
    
    public XmlString xgetCoordorigin() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().find_attribute_user(CTGroupImpl.COORDORIGIN$86);
        }
    }
    
    public boolean isSetCoordorigin() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.COORDORIGIN$86) != null;
        }
    }
    
    public void setCoordorigin(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.COORDORIGIN$86);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.COORDORIGIN$86);
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetCoordorigin(final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            XmlString xmlString2 = (XmlString)this.get_store().find_attribute_user(CTGroupImpl.COORDORIGIN$86);
            if (xmlString2 == null) {
                xmlString2 = (XmlString)this.get_store().add_attribute_user(CTGroupImpl.COORDORIGIN$86);
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void unsetCoordorigin() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.COORDORIGIN$86);
        }
    }
    
    public String getWrapcoords() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.WRAPCOORDS$88);
            if (simpleValue == null) {
                return null;
            }
            return simpleValue.getStringValue();
        }
    }
    
    public XmlString xgetWrapcoords() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().find_attribute_user(CTGroupImpl.WRAPCOORDS$88);
        }
    }
    
    public boolean isSetWrapcoords() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.WRAPCOORDS$88) != null;
        }
    }
    
    public void setWrapcoords(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.WRAPCOORDS$88);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.WRAPCOORDS$88);
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetWrapcoords(final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            XmlString xmlString2 = (XmlString)this.get_store().find_attribute_user(CTGroupImpl.WRAPCOORDS$88);
            if (xmlString2 == null) {
                xmlString2 = (XmlString)this.get_store().add_attribute_user(CTGroupImpl.WRAPCOORDS$88);
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void unsetWrapcoords() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.WRAPCOORDS$88);
        }
    }
    
    public STTrueFalse.Enum getPrint() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.PRINT$90);
            if (simpleValue == null) {
                return null;
            }
            return (STTrueFalse.Enum)simpleValue.getEnumValue();
        }
    }
    
    public STTrueFalse xgetPrint() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalse)this.get_store().find_attribute_user(CTGroupImpl.PRINT$90);
        }
    }
    
    public boolean isSetPrint() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.PRINT$90) != null;
        }
    }
    
    public void setPrint(final STTrueFalse.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.PRINT$90);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.PRINT$90);
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetPrint(final STTrueFalse stTrueFalse) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            STTrueFalse stTrueFalse2 = (STTrueFalse)this.get_store().find_attribute_user(CTGroupImpl.PRINT$90);
            if (stTrueFalse2 == null) {
                stTrueFalse2 = (STTrueFalse)this.get_store().add_attribute_user(CTGroupImpl.PRINT$90);
            }
            stTrueFalse2.set(stTrueFalse);
        }
    }
    
    public void unsetPrint() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.PRINT$90);
        }
    }
    
    public String getSpid() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.SPID$92);
            if (simpleValue == null) {
                return null;
            }
            return simpleValue.getStringValue();
        }
    }
    
    public XmlString xgetSpid() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().find_attribute_user(CTGroupImpl.SPID$92);
        }
    }
    
    public boolean isSetSpid() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.SPID$92) != null;
        }
    }
    
    public void setSpid(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.SPID$92);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.SPID$92);
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetSpid(final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            XmlString xmlString2 = (XmlString)this.get_store().find_attribute_user(CTGroupImpl.SPID$92);
            if (xmlString2 == null) {
                xmlString2 = (XmlString)this.get_store().add_attribute_user(CTGroupImpl.SPID$92);
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void unsetSpid() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.SPID$92);
        }
    }
    
    public schemasMicrosoftComOfficeOffice.STTrueFalse.Enum getOned() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.ONED$94);
            if (simpleValue == null) {
                return null;
            }
            return (schemasMicrosoftComOfficeOffice.STTrueFalse.Enum)simpleValue.getEnumValue();
        }
    }
    
    public schemasMicrosoftComOfficeOffice.STTrueFalse xgetOned() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().find_attribute_user(CTGroupImpl.ONED$94);
        }
    }
    
    public boolean isSetOned() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.ONED$94) != null;
        }
    }
    
    public void setOned(final schemasMicrosoftComOfficeOffice.STTrueFalse.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.ONED$94);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.ONED$94);
            }
            simpleValue.setEnumValue((StringEnumAbstractBase)enumValue);
        }
    }
    
    public void xsetOned(final schemasMicrosoftComOfficeOffice.STTrueFalse stTrueFalse) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            schemasMicrosoftComOfficeOffice.STTrueFalse stTrueFalse2 = (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().find_attribute_user(CTGroupImpl.ONED$94);
            if (stTrueFalse2 == null) {
                stTrueFalse2 = (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().add_attribute_user(CTGroupImpl.ONED$94);
            }
            stTrueFalse2.set((XmlObject)stTrueFalse);
        }
    }
    
    public void unsetOned() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.ONED$94);
        }
    }
    
    public BigInteger getRegroupid() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.REGROUPID$96);
            if (simpleValue == null) {
                return null;
            }
            return simpleValue.getBigIntegerValue();
        }
    }
    
    public XmlInteger xgetRegroupid() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().find_attribute_user(CTGroupImpl.REGROUPID$96);
        }
    }
    
    public boolean isSetRegroupid() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.REGROUPID$96) != null;
        }
    }
    
    public void setRegroupid(final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.REGROUPID$96);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.REGROUPID$96);
            }
            simpleValue.setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void xsetRegroupid(final XmlInteger xmlInteger) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            XmlInteger xmlInteger2 = (XmlInteger)this.get_store().find_attribute_user(CTGroupImpl.REGROUPID$96);
            if (xmlInteger2 == null) {
                xmlInteger2 = (XmlInteger)this.get_store().add_attribute_user(CTGroupImpl.REGROUPID$96);
            }
            xmlInteger2.set(xmlInteger);
        }
    }
    
    public void unsetRegroupid() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.REGROUPID$96);
        }
    }
    
    public schemasMicrosoftComOfficeOffice.STTrueFalse.Enum getDoubleclicknotify() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.DOUBLECLICKNOTIFY$98);
            if (simpleValue == null) {
                return null;
            }
            return (schemasMicrosoftComOfficeOffice.STTrueFalse.Enum)simpleValue.getEnumValue();
        }
    }
    
    public schemasMicrosoftComOfficeOffice.STTrueFalse xgetDoubleclicknotify() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().find_attribute_user(CTGroupImpl.DOUBLECLICKNOTIFY$98);
        }
    }
    
    public boolean isSetDoubleclicknotify() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.DOUBLECLICKNOTIFY$98) != null;
        }
    }
    
    public void setDoubleclicknotify(final schemasMicrosoftComOfficeOffice.STTrueFalse.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.DOUBLECLICKNOTIFY$98);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.DOUBLECLICKNOTIFY$98);
            }
            simpleValue.setEnumValue((StringEnumAbstractBase)enumValue);
        }
    }
    
    public void xsetDoubleclicknotify(final schemasMicrosoftComOfficeOffice.STTrueFalse stTrueFalse) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            schemasMicrosoftComOfficeOffice.STTrueFalse stTrueFalse2 = (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().find_attribute_user(CTGroupImpl.DOUBLECLICKNOTIFY$98);
            if (stTrueFalse2 == null) {
                stTrueFalse2 = (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().add_attribute_user(CTGroupImpl.DOUBLECLICKNOTIFY$98);
            }
            stTrueFalse2.set((XmlObject)stTrueFalse);
        }
    }
    
    public void unsetDoubleclicknotify() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.DOUBLECLICKNOTIFY$98);
        }
    }
    
    public schemasMicrosoftComOfficeOffice.STTrueFalse.Enum getButton() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.BUTTON$100);
            if (simpleValue == null) {
                return null;
            }
            return (schemasMicrosoftComOfficeOffice.STTrueFalse.Enum)simpleValue.getEnumValue();
        }
    }
    
    public schemasMicrosoftComOfficeOffice.STTrueFalse xgetButton() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().find_attribute_user(CTGroupImpl.BUTTON$100);
        }
    }
    
    public boolean isSetButton() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.BUTTON$100) != null;
        }
    }
    
    public void setButton(final schemasMicrosoftComOfficeOffice.STTrueFalse.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.BUTTON$100);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.BUTTON$100);
            }
            simpleValue.setEnumValue((StringEnumAbstractBase)enumValue);
        }
    }
    
    public void xsetButton(final schemasMicrosoftComOfficeOffice.STTrueFalse stTrueFalse) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            schemasMicrosoftComOfficeOffice.STTrueFalse stTrueFalse2 = (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().find_attribute_user(CTGroupImpl.BUTTON$100);
            if (stTrueFalse2 == null) {
                stTrueFalse2 = (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().add_attribute_user(CTGroupImpl.BUTTON$100);
            }
            stTrueFalse2.set((XmlObject)stTrueFalse);
        }
    }
    
    public void unsetButton() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.BUTTON$100);
        }
    }
    
    public schemasMicrosoftComOfficeOffice.STTrueFalse.Enum getUserhidden() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.USERHIDDEN$102);
            if (simpleValue == null) {
                return null;
            }
            return (schemasMicrosoftComOfficeOffice.STTrueFalse.Enum)simpleValue.getEnumValue();
        }
    }
    
    public schemasMicrosoftComOfficeOffice.STTrueFalse xgetUserhidden() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().find_attribute_user(CTGroupImpl.USERHIDDEN$102);
        }
    }
    
    public boolean isSetUserhidden() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.USERHIDDEN$102) != null;
        }
    }
    
    public void setUserhidden(final schemasMicrosoftComOfficeOffice.STTrueFalse.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.USERHIDDEN$102);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.USERHIDDEN$102);
            }
            simpleValue.setEnumValue((StringEnumAbstractBase)enumValue);
        }
    }
    
    public void xsetUserhidden(final schemasMicrosoftComOfficeOffice.STTrueFalse stTrueFalse) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            schemasMicrosoftComOfficeOffice.STTrueFalse stTrueFalse2 = (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().find_attribute_user(CTGroupImpl.USERHIDDEN$102);
            if (stTrueFalse2 == null) {
                stTrueFalse2 = (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().add_attribute_user(CTGroupImpl.USERHIDDEN$102);
            }
            stTrueFalse2.set((XmlObject)stTrueFalse);
        }
    }
    
    public void unsetUserhidden() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.USERHIDDEN$102);
        }
    }
    
    public schemasMicrosoftComOfficeOffice.STTrueFalse.Enum getBullet() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.BULLET$104);
            if (simpleValue == null) {
                return null;
            }
            return (schemasMicrosoftComOfficeOffice.STTrueFalse.Enum)simpleValue.getEnumValue();
        }
    }
    
    public schemasMicrosoftComOfficeOffice.STTrueFalse xgetBullet() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().find_attribute_user(CTGroupImpl.BULLET$104);
        }
    }
    
    public boolean isSetBullet() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.BULLET$104) != null;
        }
    }
    
    public void setBullet(final schemasMicrosoftComOfficeOffice.STTrueFalse.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.BULLET$104);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.BULLET$104);
            }
            simpleValue.setEnumValue((StringEnumAbstractBase)enumValue);
        }
    }
    
    public void xsetBullet(final schemasMicrosoftComOfficeOffice.STTrueFalse stTrueFalse) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            schemasMicrosoftComOfficeOffice.STTrueFalse stTrueFalse2 = (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().find_attribute_user(CTGroupImpl.BULLET$104);
            if (stTrueFalse2 == null) {
                stTrueFalse2 = (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().add_attribute_user(CTGroupImpl.BULLET$104);
            }
            stTrueFalse2.set((XmlObject)stTrueFalse);
        }
    }
    
    public void unsetBullet() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.BULLET$104);
        }
    }
    
    public schemasMicrosoftComOfficeOffice.STTrueFalse.Enum getHr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.HR$106);
            if (simpleValue == null) {
                return null;
            }
            return (schemasMicrosoftComOfficeOffice.STTrueFalse.Enum)simpleValue.getEnumValue();
        }
    }
    
    public schemasMicrosoftComOfficeOffice.STTrueFalse xgetHr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().find_attribute_user(CTGroupImpl.HR$106);
        }
    }
    
    public boolean isSetHr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.HR$106) != null;
        }
    }
    
    public void setHr(final schemasMicrosoftComOfficeOffice.STTrueFalse.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.HR$106);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.HR$106);
            }
            simpleValue.setEnumValue((StringEnumAbstractBase)enumValue);
        }
    }
    
    public void xsetHr(final schemasMicrosoftComOfficeOffice.STTrueFalse stTrueFalse) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            schemasMicrosoftComOfficeOffice.STTrueFalse stTrueFalse2 = (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().find_attribute_user(CTGroupImpl.HR$106);
            if (stTrueFalse2 == null) {
                stTrueFalse2 = (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().add_attribute_user(CTGroupImpl.HR$106);
            }
            stTrueFalse2.set((XmlObject)stTrueFalse);
        }
    }
    
    public void unsetHr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.HR$106);
        }
    }
    
    public schemasMicrosoftComOfficeOffice.STTrueFalse.Enum getHrstd() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.HRSTD$108);
            if (simpleValue == null) {
                return null;
            }
            return (schemasMicrosoftComOfficeOffice.STTrueFalse.Enum)simpleValue.getEnumValue();
        }
    }
    
    public schemasMicrosoftComOfficeOffice.STTrueFalse xgetHrstd() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().find_attribute_user(CTGroupImpl.HRSTD$108);
        }
    }
    
    public boolean isSetHrstd() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.HRSTD$108) != null;
        }
    }
    
    public void setHrstd(final schemasMicrosoftComOfficeOffice.STTrueFalse.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.HRSTD$108);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.HRSTD$108);
            }
            simpleValue.setEnumValue((StringEnumAbstractBase)enumValue);
        }
    }
    
    public void xsetHrstd(final schemasMicrosoftComOfficeOffice.STTrueFalse stTrueFalse) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            schemasMicrosoftComOfficeOffice.STTrueFalse stTrueFalse2 = (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().find_attribute_user(CTGroupImpl.HRSTD$108);
            if (stTrueFalse2 == null) {
                stTrueFalse2 = (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().add_attribute_user(CTGroupImpl.HRSTD$108);
            }
            stTrueFalse2.set((XmlObject)stTrueFalse);
        }
    }
    
    public void unsetHrstd() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.HRSTD$108);
        }
    }
    
    public schemasMicrosoftComOfficeOffice.STTrueFalse.Enum getHrnoshade() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.HRNOSHADE$110);
            if (simpleValue == null) {
                return null;
            }
            return (schemasMicrosoftComOfficeOffice.STTrueFalse.Enum)simpleValue.getEnumValue();
        }
    }
    
    public schemasMicrosoftComOfficeOffice.STTrueFalse xgetHrnoshade() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().find_attribute_user(CTGroupImpl.HRNOSHADE$110);
        }
    }
    
    public boolean isSetHrnoshade() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.HRNOSHADE$110) != null;
        }
    }
    
    public void setHrnoshade(final schemasMicrosoftComOfficeOffice.STTrueFalse.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.HRNOSHADE$110);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.HRNOSHADE$110);
            }
            simpleValue.setEnumValue((StringEnumAbstractBase)enumValue);
        }
    }
    
    public void xsetHrnoshade(final schemasMicrosoftComOfficeOffice.STTrueFalse stTrueFalse) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            schemasMicrosoftComOfficeOffice.STTrueFalse stTrueFalse2 = (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().find_attribute_user(CTGroupImpl.HRNOSHADE$110);
            if (stTrueFalse2 == null) {
                stTrueFalse2 = (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().add_attribute_user(CTGroupImpl.HRNOSHADE$110);
            }
            stTrueFalse2.set((XmlObject)stTrueFalse);
        }
    }
    
    public void unsetHrnoshade() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.HRNOSHADE$110);
        }
    }
    
    public float getHrpct() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.HRPCT$112);
            if (simpleValue == null) {
                return 0.0f;
            }
            return simpleValue.getFloatValue();
        }
    }
    
    public XmlFloat xgetHrpct() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlFloat)this.get_store().find_attribute_user(CTGroupImpl.HRPCT$112);
        }
    }
    
    public boolean isSetHrpct() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.HRPCT$112) != null;
        }
    }
    
    public void setHrpct(final float floatValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.HRPCT$112);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.HRPCT$112);
            }
            simpleValue.setFloatValue(floatValue);
        }
    }
    
    public void xsetHrpct(final XmlFloat xmlFloat) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            XmlFloat xmlFloat2 = (XmlFloat)this.get_store().find_attribute_user(CTGroupImpl.HRPCT$112);
            if (xmlFloat2 == null) {
                xmlFloat2 = (XmlFloat)this.get_store().add_attribute_user(CTGroupImpl.HRPCT$112);
            }
            xmlFloat2.set(xmlFloat);
        }
    }
    
    public void unsetHrpct() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.HRPCT$112);
        }
    }
    
    public STHrAlign.Enum getHralign() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.HRALIGN$114);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_default_attribute_value(CTGroupImpl.HRALIGN$114);
            }
            if (simpleValue == null) {
                return null;
            }
            return (STHrAlign.Enum)simpleValue.getEnumValue();
        }
    }
    
    public STHrAlign xgetHralign() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            STHrAlign stHrAlign = (STHrAlign)this.get_store().find_attribute_user(CTGroupImpl.HRALIGN$114);
            if (stHrAlign == null) {
                stHrAlign = (STHrAlign)this.get_default_attribute_value(CTGroupImpl.HRALIGN$114);
            }
            return stHrAlign;
        }
    }
    
    public boolean isSetHralign() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.HRALIGN$114) != null;
        }
    }
    
    public void setHralign(final STHrAlign.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.HRALIGN$114);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.HRALIGN$114);
            }
            simpleValue.setEnumValue((StringEnumAbstractBase)enumValue);
        }
    }
    
    public void xsetHralign(final STHrAlign stHrAlign) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            STHrAlign stHrAlign2 = (STHrAlign)this.get_store().find_attribute_user(CTGroupImpl.HRALIGN$114);
            if (stHrAlign2 == null) {
                stHrAlign2 = (STHrAlign)this.get_store().add_attribute_user(CTGroupImpl.HRALIGN$114);
            }
            stHrAlign2.set((XmlObject)stHrAlign);
        }
    }
    
    public void unsetHralign() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.HRALIGN$114);
        }
    }
    
    public schemasMicrosoftComOfficeOffice.STTrueFalse.Enum getAllowincell() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.ALLOWINCELL$116);
            if (simpleValue == null) {
                return null;
            }
            return (schemasMicrosoftComOfficeOffice.STTrueFalse.Enum)simpleValue.getEnumValue();
        }
    }
    
    public schemasMicrosoftComOfficeOffice.STTrueFalse xgetAllowincell() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().find_attribute_user(CTGroupImpl.ALLOWINCELL$116);
        }
    }
    
    public boolean isSetAllowincell() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.ALLOWINCELL$116) != null;
        }
    }
    
    public void setAllowincell(final schemasMicrosoftComOfficeOffice.STTrueFalse.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.ALLOWINCELL$116);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.ALLOWINCELL$116);
            }
            simpleValue.setEnumValue((StringEnumAbstractBase)enumValue);
        }
    }
    
    public void xsetAllowincell(final schemasMicrosoftComOfficeOffice.STTrueFalse stTrueFalse) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            schemasMicrosoftComOfficeOffice.STTrueFalse stTrueFalse2 = (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().find_attribute_user(CTGroupImpl.ALLOWINCELL$116);
            if (stTrueFalse2 == null) {
                stTrueFalse2 = (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().add_attribute_user(CTGroupImpl.ALLOWINCELL$116);
            }
            stTrueFalse2.set((XmlObject)stTrueFalse);
        }
    }
    
    public void unsetAllowincell() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.ALLOWINCELL$116);
        }
    }
    
    public schemasMicrosoftComOfficeOffice.STTrueFalse.Enum getAllowoverlap() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.ALLOWOVERLAP$118);
            if (simpleValue == null) {
                return null;
            }
            return (schemasMicrosoftComOfficeOffice.STTrueFalse.Enum)simpleValue.getEnumValue();
        }
    }
    
    public schemasMicrosoftComOfficeOffice.STTrueFalse xgetAllowoverlap() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().find_attribute_user(CTGroupImpl.ALLOWOVERLAP$118);
        }
    }
    
    public boolean isSetAllowoverlap() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.ALLOWOVERLAP$118) != null;
        }
    }
    
    public void setAllowoverlap(final schemasMicrosoftComOfficeOffice.STTrueFalse.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.ALLOWOVERLAP$118);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.ALLOWOVERLAP$118);
            }
            simpleValue.setEnumValue((StringEnumAbstractBase)enumValue);
        }
    }
    
    public void xsetAllowoverlap(final schemasMicrosoftComOfficeOffice.STTrueFalse stTrueFalse) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            schemasMicrosoftComOfficeOffice.STTrueFalse stTrueFalse2 = (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().find_attribute_user(CTGroupImpl.ALLOWOVERLAP$118);
            if (stTrueFalse2 == null) {
                stTrueFalse2 = (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().add_attribute_user(CTGroupImpl.ALLOWOVERLAP$118);
            }
            stTrueFalse2.set((XmlObject)stTrueFalse);
        }
    }
    
    public void unsetAllowoverlap() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.ALLOWOVERLAP$118);
        }
    }
    
    public schemasMicrosoftComOfficeOffice.STTrueFalse.Enum getUserdrawn() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.USERDRAWN$120);
            if (simpleValue == null) {
                return null;
            }
            return (schemasMicrosoftComOfficeOffice.STTrueFalse.Enum)simpleValue.getEnumValue();
        }
    }
    
    public schemasMicrosoftComOfficeOffice.STTrueFalse xgetUserdrawn() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().find_attribute_user(CTGroupImpl.USERDRAWN$120);
        }
    }
    
    public boolean isSetUserdrawn() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.USERDRAWN$120) != null;
        }
    }
    
    public void setUserdrawn(final schemasMicrosoftComOfficeOffice.STTrueFalse.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.USERDRAWN$120);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.USERDRAWN$120);
            }
            simpleValue.setEnumValue((StringEnumAbstractBase)enumValue);
        }
    }
    
    public void xsetUserdrawn(final schemasMicrosoftComOfficeOffice.STTrueFalse stTrueFalse) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            schemasMicrosoftComOfficeOffice.STTrueFalse stTrueFalse2 = (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().find_attribute_user(CTGroupImpl.USERDRAWN$120);
            if (stTrueFalse2 == null) {
                stTrueFalse2 = (schemasMicrosoftComOfficeOffice.STTrueFalse)this.get_store().add_attribute_user(CTGroupImpl.USERDRAWN$120);
            }
            stTrueFalse2.set((XmlObject)stTrueFalse);
        }
    }
    
    public void unsetUserdrawn() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.USERDRAWN$120);
        }
    }
    
    public String getBordertopcolor() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.BORDERTOPCOLOR$122);
            if (simpleValue == null) {
                return null;
            }
            return simpleValue.getStringValue();
        }
    }
    
    public XmlString xgetBordertopcolor() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().find_attribute_user(CTGroupImpl.BORDERTOPCOLOR$122);
        }
    }
    
    public boolean isSetBordertopcolor() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.BORDERTOPCOLOR$122) != null;
        }
    }
    
    public void setBordertopcolor(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.BORDERTOPCOLOR$122);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.BORDERTOPCOLOR$122);
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetBordertopcolor(final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            XmlString xmlString2 = (XmlString)this.get_store().find_attribute_user(CTGroupImpl.BORDERTOPCOLOR$122);
            if (xmlString2 == null) {
                xmlString2 = (XmlString)this.get_store().add_attribute_user(CTGroupImpl.BORDERTOPCOLOR$122);
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void unsetBordertopcolor() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.BORDERTOPCOLOR$122);
        }
    }
    
    public String getBorderleftcolor() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.BORDERLEFTCOLOR$124);
            if (simpleValue == null) {
                return null;
            }
            return simpleValue.getStringValue();
        }
    }
    
    public XmlString xgetBorderleftcolor() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().find_attribute_user(CTGroupImpl.BORDERLEFTCOLOR$124);
        }
    }
    
    public boolean isSetBorderleftcolor() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.BORDERLEFTCOLOR$124) != null;
        }
    }
    
    public void setBorderleftcolor(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.BORDERLEFTCOLOR$124);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.BORDERLEFTCOLOR$124);
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetBorderleftcolor(final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            XmlString xmlString2 = (XmlString)this.get_store().find_attribute_user(CTGroupImpl.BORDERLEFTCOLOR$124);
            if (xmlString2 == null) {
                xmlString2 = (XmlString)this.get_store().add_attribute_user(CTGroupImpl.BORDERLEFTCOLOR$124);
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void unsetBorderleftcolor() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.BORDERLEFTCOLOR$124);
        }
    }
    
    public String getBorderbottomcolor() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.BORDERBOTTOMCOLOR$126);
            if (simpleValue == null) {
                return null;
            }
            return simpleValue.getStringValue();
        }
    }
    
    public XmlString xgetBorderbottomcolor() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().find_attribute_user(CTGroupImpl.BORDERBOTTOMCOLOR$126);
        }
    }
    
    public boolean isSetBorderbottomcolor() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.BORDERBOTTOMCOLOR$126) != null;
        }
    }
    
    public void setBorderbottomcolor(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.BORDERBOTTOMCOLOR$126);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.BORDERBOTTOMCOLOR$126);
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetBorderbottomcolor(final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            XmlString xmlString2 = (XmlString)this.get_store().find_attribute_user(CTGroupImpl.BORDERBOTTOMCOLOR$126);
            if (xmlString2 == null) {
                xmlString2 = (XmlString)this.get_store().add_attribute_user(CTGroupImpl.BORDERBOTTOMCOLOR$126);
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void unsetBorderbottomcolor() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.BORDERBOTTOMCOLOR$126);
        }
    }
    
    public String getBorderrightcolor() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.BORDERRIGHTCOLOR$128);
            if (simpleValue == null) {
                return null;
            }
            return simpleValue.getStringValue();
        }
    }
    
    public XmlString xgetBorderrightcolor() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().find_attribute_user(CTGroupImpl.BORDERRIGHTCOLOR$128);
        }
    }
    
    public boolean isSetBorderrightcolor() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.BORDERRIGHTCOLOR$128) != null;
        }
    }
    
    public void setBorderrightcolor(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.BORDERRIGHTCOLOR$128);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.BORDERRIGHTCOLOR$128);
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetBorderrightcolor(final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            XmlString xmlString2 = (XmlString)this.get_store().find_attribute_user(CTGroupImpl.BORDERRIGHTCOLOR$128);
            if (xmlString2 == null) {
                xmlString2 = (XmlString)this.get_store().add_attribute_user(CTGroupImpl.BORDERRIGHTCOLOR$128);
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void unsetBorderrightcolor() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.BORDERRIGHTCOLOR$128);
        }
    }
    
    public BigInteger getDgmlayout() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.DGMLAYOUT$130);
            if (simpleValue == null) {
                return null;
            }
            return simpleValue.getBigIntegerValue();
        }
    }
    
    public XmlInteger xgetDgmlayout() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().find_attribute_user(CTGroupImpl.DGMLAYOUT$130);
        }
    }
    
    public boolean isSetDgmlayout() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.DGMLAYOUT$130) != null;
        }
    }
    
    public void setDgmlayout(final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.DGMLAYOUT$130);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.DGMLAYOUT$130);
            }
            simpleValue.setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void xsetDgmlayout(final XmlInteger xmlInteger) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            XmlInteger xmlInteger2 = (XmlInteger)this.get_store().find_attribute_user(CTGroupImpl.DGMLAYOUT$130);
            if (xmlInteger2 == null) {
                xmlInteger2 = (XmlInteger)this.get_store().add_attribute_user(CTGroupImpl.DGMLAYOUT$130);
            }
            xmlInteger2.set(xmlInteger);
        }
    }
    
    public void unsetDgmlayout() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.DGMLAYOUT$130);
        }
    }
    
    public BigInteger getDgmnodekind() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.DGMNODEKIND$132);
            if (simpleValue == null) {
                return null;
            }
            return simpleValue.getBigIntegerValue();
        }
    }
    
    public XmlInteger xgetDgmnodekind() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().find_attribute_user(CTGroupImpl.DGMNODEKIND$132);
        }
    }
    
    public boolean isSetDgmnodekind() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.DGMNODEKIND$132) != null;
        }
    }
    
    public void setDgmnodekind(final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.DGMNODEKIND$132);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.DGMNODEKIND$132);
            }
            simpleValue.setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void xsetDgmnodekind(final XmlInteger xmlInteger) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            XmlInteger xmlInteger2 = (XmlInteger)this.get_store().find_attribute_user(CTGroupImpl.DGMNODEKIND$132);
            if (xmlInteger2 == null) {
                xmlInteger2 = (XmlInteger)this.get_store().add_attribute_user(CTGroupImpl.DGMNODEKIND$132);
            }
            xmlInteger2.set(xmlInteger);
        }
    }
    
    public void unsetDgmnodekind() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.DGMNODEKIND$132);
        }
    }
    
    public BigInteger getDgmlayoutmru() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.DGMLAYOUTMRU$134);
            if (simpleValue == null) {
                return null;
            }
            return simpleValue.getBigIntegerValue();
        }
    }
    
    public XmlInteger xgetDgmlayoutmru() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().find_attribute_user(CTGroupImpl.DGMLAYOUTMRU$134);
        }
    }
    
    public boolean isSetDgmlayoutmru() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.DGMLAYOUTMRU$134) != null;
        }
    }
    
    public void setDgmlayoutmru(final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.DGMLAYOUTMRU$134);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.DGMLAYOUTMRU$134);
            }
            simpleValue.setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void xsetDgmlayoutmru(final XmlInteger xmlInteger) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            XmlInteger xmlInteger2 = (XmlInteger)this.get_store().find_attribute_user(CTGroupImpl.DGMLAYOUTMRU$134);
            if (xmlInteger2 == null) {
                xmlInteger2 = (XmlInteger)this.get_store().add_attribute_user(CTGroupImpl.DGMLAYOUTMRU$134);
            }
            xmlInteger2.set(xmlInteger);
        }
    }
    
    public void unsetDgmlayoutmru() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.DGMLAYOUTMRU$134);
        }
    }
    
    public STInsetMode.Enum getInsetmode() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.INSETMODE$136);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_default_attribute_value(CTGroupImpl.INSETMODE$136);
            }
            if (simpleValue == null) {
                return null;
            }
            return (STInsetMode.Enum)simpleValue.getEnumValue();
        }
    }
    
    public STInsetMode xgetInsetmode() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            STInsetMode stInsetMode = (STInsetMode)this.get_store().find_attribute_user(CTGroupImpl.INSETMODE$136);
            if (stInsetMode == null) {
                stInsetMode = (STInsetMode)this.get_default_attribute_value(CTGroupImpl.INSETMODE$136);
            }
            return stInsetMode;
        }
    }
    
    public boolean isSetInsetmode() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.INSETMODE$136) != null;
        }
    }
    
    public void setInsetmode(final STInsetMode.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.INSETMODE$136);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.INSETMODE$136);
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetInsetmode(final STInsetMode stInsetMode) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            STInsetMode stInsetMode2 = (STInsetMode)this.get_store().find_attribute_user(CTGroupImpl.INSETMODE$136);
            if (stInsetMode2 == null) {
                stInsetMode2 = (STInsetMode)this.get_store().add_attribute_user(CTGroupImpl.INSETMODE$136);
            }
            stInsetMode2.set(stInsetMode);
        }
    }
    
    public void unsetInsetmode() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.INSETMODE$136);
        }
    }
    
    public STTrueFalse.Enum getFilled() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.FILLED$138);
            if (simpleValue == null) {
                return null;
            }
            return (STTrueFalse.Enum)simpleValue.getEnumValue();
        }
    }
    
    public STTrueFalse xgetFilled() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalse)this.get_store().find_attribute_user(CTGroupImpl.FILLED$138);
        }
    }
    
    public boolean isSetFilled() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.FILLED$138) != null;
        }
    }
    
    public void setFilled(final STTrueFalse.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.FILLED$138);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.FILLED$138);
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetFilled(final STTrueFalse stTrueFalse) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            STTrueFalse stTrueFalse2 = (STTrueFalse)this.get_store().find_attribute_user(CTGroupImpl.FILLED$138);
            if (stTrueFalse2 == null) {
                stTrueFalse2 = (STTrueFalse)this.get_store().add_attribute_user(CTGroupImpl.FILLED$138);
            }
            stTrueFalse2.set(stTrueFalse);
        }
    }
    
    public void unsetFilled() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.FILLED$138);
        }
    }
    
    public String getFillcolor() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.FILLCOLOR$140);
            if (simpleValue == null) {
                return null;
            }
            return simpleValue.getStringValue();
        }
    }
    
    public STColorType xgetFillcolor() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STColorType)this.get_store().find_attribute_user(CTGroupImpl.FILLCOLOR$140);
        }
    }
    
    public boolean isSetFillcolor() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.FILLCOLOR$140) != null;
        }
    }
    
    public void setFillcolor(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.FILLCOLOR$140);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.FILLCOLOR$140);
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetFillcolor(final STColorType stColorType) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            STColorType stColorType2 = (STColorType)this.get_store().find_attribute_user(CTGroupImpl.FILLCOLOR$140);
            if (stColorType2 == null) {
                stColorType2 = (STColorType)this.get_store().add_attribute_user(CTGroupImpl.FILLCOLOR$140);
            }
            stColorType2.set(stColorType);
        }
    }
    
    public void unsetFillcolor() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.FILLCOLOR$140);
        }
    }
    
    public STEditAs.Enum getEditas() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.EDITAS$142);
            if (simpleValue == null) {
                return null;
            }
            return (STEditAs.Enum)simpleValue.getEnumValue();
        }
    }
    
    public STEditAs xgetEditas() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STEditAs)this.get_store().find_attribute_user(CTGroupImpl.EDITAS$142);
        }
    }
    
    public boolean isSetEditas() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.EDITAS$142) != null;
        }
    }
    
    public void setEditas(final STEditAs.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.EDITAS$142);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.EDITAS$142);
            }
            simpleValue.setEnumValue((StringEnumAbstractBase)enumValue);
        }
    }
    
    public void xsetEditas(final STEditAs stEditAs) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            STEditAs stEditAs2 = (STEditAs)this.get_store().find_attribute_user(CTGroupImpl.EDITAS$142);
            if (stEditAs2 == null) {
                stEditAs2 = (STEditAs)this.get_store().add_attribute_user(CTGroupImpl.EDITAS$142);
            }
            stEditAs2.set((XmlObject)stEditAs);
        }
    }
    
    public void unsetEditas() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.EDITAS$142);
        }
    }
    
    public String getTableproperties() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.TABLEPROPERTIES$144);
            if (simpleValue == null) {
                return null;
            }
            return simpleValue.getStringValue();
        }
    }
    
    public XmlString xgetTableproperties() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().find_attribute_user(CTGroupImpl.TABLEPROPERTIES$144);
        }
    }
    
    public boolean isSetTableproperties() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.TABLEPROPERTIES$144) != null;
        }
    }
    
    public void setTableproperties(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.TABLEPROPERTIES$144);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.TABLEPROPERTIES$144);
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetTableproperties(final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            XmlString xmlString2 = (XmlString)this.get_store().find_attribute_user(CTGroupImpl.TABLEPROPERTIES$144);
            if (xmlString2 == null) {
                xmlString2 = (XmlString)this.get_store().add_attribute_user(CTGroupImpl.TABLEPROPERTIES$144);
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void unsetTableproperties() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.TABLEPROPERTIES$144);
        }
    }
    
    public String getTablelimits() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.TABLELIMITS$146);
            if (simpleValue == null) {
                return null;
            }
            return simpleValue.getStringValue();
        }
    }
    
    public XmlString xgetTablelimits() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().find_attribute_user(CTGroupImpl.TABLELIMITS$146);
        }
    }
    
    public boolean isSetTablelimits() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTGroupImpl.TABLELIMITS$146) != null;
        }
    }
    
    public void setTablelimits(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGroupImpl.TABLELIMITS$146);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGroupImpl.TABLELIMITS$146);
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetTablelimits(final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            XmlString xmlString2 = (XmlString)this.get_store().find_attribute_user(CTGroupImpl.TABLELIMITS$146);
            if (xmlString2 == null) {
                xmlString2 = (XmlString)this.get_store().add_attribute_user(CTGroupImpl.TABLELIMITS$146);
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void unsetTablelimits() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTGroupImpl.TABLELIMITS$146);
        }
    }
    
    static {
        PATH$0 = new QName("urn:schemas-microsoft-com:vml", "path");
        FORMULAS$2 = new QName("urn:schemas-microsoft-com:vml", "formulas");
        HANDLES$4 = new QName("urn:schemas-microsoft-com:vml", "handles");
        FILL$6 = new QName("urn:schemas-microsoft-com:vml", "fill");
        STROKE$8 = new QName("urn:schemas-microsoft-com:vml", "stroke");
        SHADOW$10 = new QName("urn:schemas-microsoft-com:vml", "shadow");
        TEXTBOX$12 = new QName("urn:schemas-microsoft-com:vml", "textbox");
        TEXTPATH$14 = new QName("urn:schemas-microsoft-com:vml", "textpath");
        IMAGEDATA$16 = new QName("urn:schemas-microsoft-com:vml", "imagedata");
        SKEW$18 = new QName("urn:schemas-microsoft-com:office:office", "skew");
        EXTRUSION$20 = new QName("urn:schemas-microsoft-com:office:office", "extrusion");
        CALLOUT$22 = new QName("urn:schemas-microsoft-com:office:office", "callout");
        LOCK$24 = new QName("urn:schemas-microsoft-com:office:office", "lock");
        CLIPPATH$26 = new QName("urn:schemas-microsoft-com:office:office", "clippath");
        SIGNATURELINE$28 = new QName("urn:schemas-microsoft-com:office:office", "signatureline");
        WRAP$30 = new QName("urn:schemas-microsoft-com:office:word", "wrap");
        ANCHORLOCK$32 = new QName("urn:schemas-microsoft-com:office:word", "anchorlock");
        BORDERTOP$34 = new QName("urn:schemas-microsoft-com:office:word", "bordertop");
        BORDERBOTTOM$36 = new QName("urn:schemas-microsoft-com:office:word", "borderbottom");
        BORDERLEFT$38 = new QName("urn:schemas-microsoft-com:office:word", "borderleft");
        BORDERRIGHT$40 = new QName("urn:schemas-microsoft-com:office:word", "borderright");
        CLIENTDATA$42 = new QName("urn:schemas-microsoft-com:office:excel", "ClientData");
        TEXTDATA$44 = new QName("urn:schemas-microsoft-com:office:powerpoint", "textdata");
        GROUP$46 = new QName("urn:schemas-microsoft-com:vml", "group");
        SHAPE$48 = new QName("urn:schemas-microsoft-com:vml", "shape");
        SHAPETYPE$50 = new QName("urn:schemas-microsoft-com:vml", "shapetype");
        ARC$52 = new QName("urn:schemas-microsoft-com:vml", "arc");
        CURVE$54 = new QName("urn:schemas-microsoft-com:vml", "curve");
        IMAGE$56 = new QName("urn:schemas-microsoft-com:vml", "image");
        LINE$58 = new QName("urn:schemas-microsoft-com:vml", "line");
        OVAL$60 = new QName("urn:schemas-microsoft-com:vml", "oval");
        POLYLINE$62 = new QName("urn:schemas-microsoft-com:vml", "polyline");
        RECT$64 = new QName("urn:schemas-microsoft-com:vml", "rect");
        ROUNDRECT$66 = new QName("urn:schemas-microsoft-com:vml", "roundrect");
        DIAGRAM$68 = new QName("urn:schemas-microsoft-com:office:office", "diagram");
        ID$70 = new QName("", "id");
        STYLE$72 = new QName("", "style");
        HREF$74 = new QName("", "href");
        TARGET$76 = new QName("", "target");
        CLASS1$78 = new QName("", "class");
        TITLE$80 = new QName("", "title");
        ALT$82 = new QName("", "alt");
        COORDSIZE$84 = new QName("", "coordsize");
        COORDORIGIN$86 = new QName("", "coordorigin");
        WRAPCOORDS$88 = new QName("", "wrapcoords");
        PRINT$90 = new QName("", "print");
        SPID$92 = new QName("urn:schemas-microsoft-com:office:office", "spid");
        ONED$94 = new QName("urn:schemas-microsoft-com:office:office", "oned");
        REGROUPID$96 = new QName("urn:schemas-microsoft-com:office:office", "regroupid");
        DOUBLECLICKNOTIFY$98 = new QName("urn:schemas-microsoft-com:office:office", "doubleclicknotify");
        BUTTON$100 = new QName("urn:schemas-microsoft-com:office:office", "button");
        USERHIDDEN$102 = new QName("urn:schemas-microsoft-com:office:office", "userhidden");
        BULLET$104 = new QName("urn:schemas-microsoft-com:office:office", "bullet");
        HR$106 = new QName("urn:schemas-microsoft-com:office:office", "hr");
        HRSTD$108 = new QName("urn:schemas-microsoft-com:office:office", "hrstd");
        HRNOSHADE$110 = new QName("urn:schemas-microsoft-com:office:office", "hrnoshade");
        HRPCT$112 = new QName("urn:schemas-microsoft-com:office:office", "hrpct");
        HRALIGN$114 = new QName("urn:schemas-microsoft-com:office:office", "hralign");
        ALLOWINCELL$116 = new QName("urn:schemas-microsoft-com:office:office", "allowincell");
        ALLOWOVERLAP$118 = new QName("urn:schemas-microsoft-com:office:office", "allowoverlap");
        USERDRAWN$120 = new QName("urn:schemas-microsoft-com:office:office", "userdrawn");
        BORDERTOPCOLOR$122 = new QName("urn:schemas-microsoft-com:office:office", "bordertopcolor");
        BORDERLEFTCOLOR$124 = new QName("urn:schemas-microsoft-com:office:office", "borderleftcolor");
        BORDERBOTTOMCOLOR$126 = new QName("urn:schemas-microsoft-com:office:office", "borderbottomcolor");
        BORDERRIGHTCOLOR$128 = new QName("urn:schemas-microsoft-com:office:office", "borderrightcolor");
        DGMLAYOUT$130 = new QName("urn:schemas-microsoft-com:office:office", "dgmlayout");
        DGMNODEKIND$132 = new QName("urn:schemas-microsoft-com:office:office", "dgmnodekind");
        DGMLAYOUTMRU$134 = new QName("urn:schemas-microsoft-com:office:office", "dgmlayoutmru");
        INSETMODE$136 = new QName("urn:schemas-microsoft-com:office:office", "insetmode");
        FILLED$138 = new QName("", "filled");
        FILLCOLOR$140 = new QName("", "fillcolor");
        EDITAS$142 = new QName("", "editas");
        TABLEPROPERTIES$144 = new QName("urn:schemas-microsoft-com:office:office", "tableproperties");
        TABLELIMITS$146 = new QName("urn:schemas-microsoft-com:office:office", "tablelimits");
    }
}
