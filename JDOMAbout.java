import org.jdom.Document;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import org.jdom.Element;
import java.util.LinkedList;
import java.io.FileNotFoundException;
import java.util.jar.JarFile;
import java.util.StringTokenizer;
import org.jdom.input.SAXBuilder;
import java.util.List;
import java.util.Iterator;

// 
// Decompiled by Procyon v0.5.36
// 

public class JDOMAbout
{
    public static void main(final String[] args) throws Exception {
        final Info info = new JDOMAbout().new Info();
        final String title = info.title;
        System.out.println(String.valueOf(title) + " version " + info.version);
        System.out.println("Copyright " + info.copyright);
        System.out.println();
        System.out.println(info.description);
        System.out.println();
        System.out.println("Authors:");
        for (final Author author : info.authors) {
            System.out.print("  " + author.name);
            if (author.email == null) {
                System.out.println();
            }
            else {
                System.out.println(" <" + author.email + ">");
            }
        }
        System.out.println();
        System.out.println(String.valueOf(title) + " license:");
        System.out.println(info.license);
        System.out.println();
        System.out.println(String.valueOf(title) + " support:");
        System.out.println(info.support);
        System.out.println();
        System.out.println(String.valueOf(title) + " web site: " + info.website);
        System.out.println();
    }
    
    private class Info
    {
        String title;
        String version;
        String copyright;
        String description;
        List authors;
        String license;
        String support;
        String website;
        
        Info() throws Exception {
            final String INFO_FILENAME = "META-INF/info.xml";
            final SAXBuilder builder = new SAXBuilder();
            JarFile jarFile = null;
            ZipEntry zipEntry = null;
            final String classpath = System.getProperty("java.class.path");
            final StringTokenizer tokenizer = new StringTokenizer(classpath, ";:");
            while (tokenizer.hasMoreTokens() && zipEntry == null) {
                final String token = tokenizer.nextToken();
                try {
                    jarFile = new JarFile(token);
                    zipEntry = jarFile.getEntry("META-INF/info.xml");
                }
                catch (Exception ex) {}
            }
            if (zipEntry == null) {
                throw new FileNotFoundException("META-INF/info.xml not found; it should be within the JDOM JAR but isn't");
            }
            final InputStream in = jarFile.getInputStream(zipEntry);
            final Document doc = builder.build(in);
            final Element root = doc.getRootElement();
            this.title = root.getChildTextTrim("title");
            this.version = root.getChildTextTrim("version");
            this.copyright = root.getChildTextTrim("copyright");
            this.description = root.getChildTextTrim("description");
            this.license = root.getChildTextTrim("license");
            this.support = root.getChildTextTrim("support");
            this.website = root.getChildTextTrim("web-site");
            final List authorElements = root.getChildren("author");
            this.authors = new LinkedList();
            for (final Element element : authorElements) {
                final Author author = new Author();
                author.name = element.getChildTextTrim("name");
                author.email = element.getChildTextTrim("e-mail");
                this.authors.add(author);
            }
        }
    }
    
    private class Author
    {
        String name;
        String email;
        
        Author() {
        }
    }
}
