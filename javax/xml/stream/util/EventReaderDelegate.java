// 
// Decompiled by Procyon v0.5.36
// 

package javax.xml.stream.util;

import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLEventReader;

public class EventReaderDelegate implements XMLEventReader
{
    private XMLEventReader reader;
    
    public EventReaderDelegate() {
    }
    
    public EventReaderDelegate(final XMLEventReader reader) {
        this.reader = reader;
    }
    
    public void close() throws XMLStreamException {
        this.reader.close();
    }
    
    public String getElementText() throws XMLStreamException {
        return this.reader.getElementText();
    }
    
    public XMLEventReader getParent() {
        return this.reader;
    }
    
    public Object getProperty(final String name) throws IllegalArgumentException {
        return this.reader.getProperty(name);
    }
    
    public boolean hasNext() {
        return this.reader.hasNext();
    }
    
    public Object next() {
        return this.reader.next();
    }
    
    public XMLEvent nextEvent() throws XMLStreamException {
        return this.reader.nextEvent();
    }
    
    public XMLEvent nextTag() throws XMLStreamException {
        return this.reader.nextTag();
    }
    
    public XMLEvent peek() throws XMLStreamException {
        return this.reader.peek();
    }
    
    public void remove() {
        this.reader.remove();
    }
    
    public void setParent(final XMLEventReader reader) {
        this.reader = reader;
    }
}
