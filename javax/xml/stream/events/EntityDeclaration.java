// 
// Decompiled by Procyon v0.5.36
// 

package javax.xml.stream.events;

public interface EntityDeclaration extends XMLEvent
{
    String getBaseURI();
    
    String getName();
    
    String getNotationName();
    
    String getPublicId();
    
    String getReplacementText();
    
    String getSystemId();
}
