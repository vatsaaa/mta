// 
// Decompiled by Procyon v0.5.36
// 

package javax.xml.stream.events;

public interface StartDocument extends XMLEvent
{
    boolean encodingSet();
    
    String getCharacterEncodingScheme();
    
    String getSystemId();
    
    String getVersion();
    
    boolean isStandalone();
    
    boolean standaloneSet();
}
