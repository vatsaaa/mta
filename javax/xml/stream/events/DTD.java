// 
// Decompiled by Procyon v0.5.36
// 

package javax.xml.stream.events;

import java.util.List;

public interface DTD extends XMLEvent
{
    String getDocumentTypeDeclaration();
    
    List getEntities();
    
    List getNotations();
    
    Object getProcessedDTD();
}
