// 
// Decompiled by Procyon v0.5.36
// 

package javax.xml.stream;

import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.FileInputStream;
import java.util.Properties;
import java.io.File;

class FactoryLocator
{
    static Object locate(final String factoryId) throws FactoryConfigurationError {
        return locate(factoryId, null);
    }
    
    static Object locate(final String factoryId, final String altClassName) throws FactoryConfigurationError {
        return locate(factoryId, altClassName, Thread.currentThread().getContextClassLoader());
    }
    
    static Object locate(final String factoryId, final String altClassName, final ClassLoader classLoader) throws FactoryConfigurationError {
        try {
            final String prop = System.getProperty(factoryId);
            if (prop != null) {
                return loadFactory(prop, classLoader);
            }
        }
        catch (Exception ex) {}
        try {
            final String configFile = System.getProperty("java.home") + File.separator + "lib" + File.separator + "stax.properties";
            final File f = new File(configFile);
            if (f.exists()) {
                final Properties props = new Properties();
                props.load(new FileInputStream(f));
                final String factoryClassName = props.getProperty(factoryId);
                return loadFactory(factoryClassName, classLoader);
            }
        }
        catch (Exception ex2) {}
        final String serviceId = "META-INF/services/" + factoryId;
        try {
            InputStream is = null;
            if (classLoader == null) {
                is = ClassLoader.getSystemResourceAsStream(serviceId);
            }
            else {
                is = classLoader.getResourceAsStream(serviceId);
            }
            if (is != null) {
                final BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                final String factoryClassName = br.readLine();
                br.close();
                if (factoryClassName != null && !"".equals(factoryClassName)) {
                    return loadFactory(factoryClassName, classLoader);
                }
            }
        }
        catch (Exception ex3) {}
        if (altClassName == null) {
            throw new FactoryConfigurationError("Unable to locate factory for " + factoryId + ".", null);
        }
        return loadFactory(altClassName, classLoader);
    }
    
    private static Object loadFactory(final String className, final ClassLoader classLoader) throws FactoryConfigurationError {
        try {
            final Class factoryClass = (classLoader == null) ? Class.forName(className) : classLoader.loadClass(className);
            return factoryClass.newInstance();
        }
        catch (ClassNotFoundException x) {
            throw new FactoryConfigurationError("Requested factory " + className + " cannot be located.  Classloader =" + classLoader.toString(), x);
        }
        catch (Exception x2) {
            throw new FactoryConfigurationError("Requested factory " + className + " could not be instantiated: " + x2, x2);
        }
    }
}
