// 
// Decompiled by Procyon v0.5.36
// 

package javax.xml.stream;

public interface Location
{
    int getCharacterOffset();
    
    int getColumnNumber();
    
    int getLineNumber();
    
    String getPublicId();
    
    String getSystemId();
}
